//===- unittests/Lex/LexerTest.cpp ------ Lexer tests ---------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//

#include "clang/Lex/Lexer.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/LangOptions.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Basic/TargetOptions.h"
#include "clang/Lex/HeaderSearch.h"
#include "clang/Lex/HeaderSearchOptions.h"
#include "clang/Lex/ModuleLoader.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Lex/PreprocessorOptions.h"
#include "gtest/gtest.h"

using namespace llvm;
using namespace clang;

namespace {

class VoidModuleLoader : public ModuleLoader {
  ModuleLoadResult loadModule(SourceLocation ImportLoc, 
                              ModuleIdPath Path,
                              Module::NameVisibilityKind Visibility,
                              bool IsInclusionDirective) override {
    return ModuleLoadResult();
  }

  void makeModuleVisible(Module *Mod,
                         Module::NameVisibilityKind Visibility,
                         SourceLocation ImportLoc) override { }

  GlobalModuleIndex *loadGlobalModuleIndex(SourceLocation TriggerLoc) override
    { return nullptr; }
  bool lookupMissingImports(StringRef Name, SourceLocation TriggerLoc) override
    { return 0; }
};

// The test fixture.
class LexerTest : public ::testing::Test {
protected:
  LexerTest()
    : FileMgr(FileMgrOpts),
      DiagID(new DiagnosticIDs()),
      Diags(DiagID, new DiagnosticOptions, new IgnoringDiagConsumer()),
      SourceMgr(Diags, FileMgr),
      TargetOpts(new TargetOptions)
  {
    TargetOpts->Triple = "x86_64-apple-darwin11.1.0";
    Target = TargetInfo::CreateTargetInfo(Diags, TargetOpts);
    LangOpts.PHP = true;
  }

  std::vector<Token> CheckLex(StringRef Source,
                              ArrayRef<tok::TokenKind> ExpectedTokens) {
    std::unique_ptr<MemoryBuffer> Buf = MemoryBuffer::getMemBuffer(Source);
    SourceMgr.setMainFileID(SourceMgr.createFileID(std::move(Buf)));

    VoidModuleLoader ModLoader;
    HeaderSearch HeaderInfo(new HeaderSearchOptions, SourceMgr, Diags, LangOpts,
                            Target.get());
    Preprocessor *PProc = new Preprocessor(new PreprocessorOptions(), Diags,
                    LangOpts, SourceMgr,
                    HeaderInfo, ModLoader, /*IILookup =*/nullptr,
                    /*OwnsHeaderSearch =*/false);
    PP.reset(PProc);
    PP->Initialize(*Target);
    PP->EnterMainSourceFile();

    std::vector<Token> toks;
    while (1) {
      Token tok;
      PP->Lex(tok);
      if (tok.is(tok::eof))
        break;
      toks.push_back(tok);
    }

    EXPECT_EQ(ExpectedTokens.size(), toks.size());
    for (unsigned i = 0, e = ExpectedTokens.size(); i != e; ++i) {
      EXPECT_EQ(ExpectedTokens[i], toks[i].getKind());
    }

    return toks;
  }


  std::vector<Token> CheckLexInterpolator(StringRef Source,
    ArrayRef<tok::TokenKind> ExpectedTokens) {
    std::unique_ptr<MemoryBuffer> Buf = MemoryBuffer::getMemBuffer(Source);
    SourceMgr.setMainFileID(SourceMgr.createFileID(std::move(Buf)));

    VoidModuleLoader ModLoader;
    HeaderSearch HeaderInfo(new HeaderSearchOptions, SourceMgr, Diags, LangOpts,
      Target.get());
    Preprocessor *PProc = new Preprocessor(new PreprocessorOptions(), Diags,
      LangOpts, SourceMgr,
      HeaderInfo, ModLoader, /*IILookup =*/nullptr,
      /*OwnsHeaderSearch =*/false);
    PP.reset(PProc);
    PP->Initialize(*Target);
    PP->EnterMainSourceFile();

    std::vector<Token> toks;
    bool interpolation = false;
    while (1) {
      Token tok;
      PP->Lex(tok);
      if (interpolation && tok.isNot(tok::string_literal)) {
        if (tok.is(tok::r_square) || tok.is(tok::r_brace)) { PP->setInterpolateMode(); }
        if (tok.is(tok::identifier)) {
          Token ID = tok;
          PP->Lex(tok);
          if (tok.isNot(tok::arrow) && tok.isNot(tok::l_square)) {
            tok = ID;
            PP->restoreInterpolationMode();
          }
          else {
            toks.push_back(ID);
          }
        }
      }
      if (tok.is(tok::quote)) interpolation = true;
      if (tok.is(tok::closing_quote)) interpolation = false;
      if (tok.is(tok::eof))
        break;
      toks.push_back(tok);
    }

    EXPECT_EQ(ExpectedTokens.size(), toks.size());
    for (unsigned i = 0, e = ExpectedTokens.size(); i != e; ++i) {
      EXPECT_EQ(ExpectedTokens[i], toks[i].getKind());
    }

    return toks;
  }

  std::string getText(Token tok) {
    return PP->getSpelling(tok);
  }

  FileSystemOptions FileMgrOpts;
  FileManager FileMgr;
  IntrusiveRefCntPtr<DiagnosticIDs> DiagID;
  DiagnosticsEngine Diags;
  SourceManager SourceMgr;
  LangOptions LangOpts;
  std::shared_ptr<TargetOptions> TargetOpts;
  IntrusiveRefCntPtr<TargetInfo> Target;
  std::unique_ptr<Preprocessor> PP;
};


TEST_F(LexerTest, Empty) {
  std::vector<tok::TokenKind> ExpectedTokens;

  std::vector<Token> toks = CheckLex("",
                                     ExpectedTokens);
}


TEST_F(LexerTest, OnlyHTML) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex("abcd",
                                     ExpectedTokens);

  EXPECT_EQ("abcd", getText(toks[0]));
}


TEST_F(LexerTest, HTMLWithNull) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex("abcd\0efgh",
                                     ExpectedTokens);

  EXPECT_EQ("abcd\0efgh", getText(toks[0]));
}


TEST_F(LexerTest, OnlyTag) {
  std::vector<tok::TokenKind> ExpectedTokens;

  std::vector<Token> toks = CheckLex("<?php",
                                     ExpectedTokens);
}


TEST_F(LexerTest, BadTag) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex("<?phpa",
                                     ExpectedTokens);

  EXPECT_EQ("<?phpa", getText(toks[0]));
}


TEST_F(LexerTest, HTMLPlusEmpty) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex("abcd<?php",
                                     ExpectedTokens);

  EXPECT_EQ("abcd", getText(toks[0]));
}


TEST_F(LexerTest, TagEmpty) {
  std::vector<tok::TokenKind> ExpectedTokens;

  std::vector<Token> toks = CheckLex("<?php ?>",
                                     ExpectedTokens);
}


TEST_F(LexerTest, TagEmpty2) {
  std::vector<tok::TokenKind> ExpectedTokens;

  std::vector<Token> toks = CheckLex("<?php?>",
                                     ExpectedTokens);
}


TEST_F(LexerTest, HTMLPlusEmptyTag) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex("abcd<?php ?>",
                                     ExpectedTokens);

  EXPECT_EQ("abcd", getText(toks[0]));
}


TEST_F(LexerTest, HTMLPlusEmptyTag2) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex("abcd<?php ?>efgh",
                                     ExpectedTokens);

  EXPECT_EQ("abcd", getText(toks[0]));
  EXPECT_EQ("efgh", getText(toks[1]));
}


TEST_F(LexerTest, HTMLPlusEmptyTag3) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);
  ExpectedTokens.push_back(tok::html_data);
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex("abcd<?php ?><?php?>efgh",
                                     ExpectedTokens);

  EXPECT_EQ("abcd", getText(toks[0]));
  EXPECT_EQ("",     getText(toks[1]));
  EXPECT_EQ("efgh", getText(toks[2]));
}


TEST_F(LexerTest, NotEmpty) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::numeric_constant);
  ExpectedTokens.push_back(tok::identifier);

  std::vector<Token> toks = CheckLex("<?php 111 abcd?>",
                                     ExpectedTokens);

  EXPECT_EQ("111", getText(toks[0]));
  EXPECT_EQ("abcd", getText(toks[1]));
}


TEST_F(LexerTest, SpecTokens) {
   std::vector<tok::TokenKind> ExpectedTokens;
   ExpectedTokens.push_back(tok::periodequal);
   ExpectedTokens.push_back(tok::less_greater);
   ExpectedTokens.push_back(tok::dollar);
   ExpectedTokens.push_back(tok::star_star);
   ExpectedTokens.push_back(tok::backslash);
   ExpectedTokens.push_back(tok::equal_equal_equal);
   ExpectedTokens.push_back(tok::equal_greater);
   ExpectedTokens.push_back(tok::exclaim_equal_equal);
   ExpectedTokens.push_back(tok::less_equal_greater);
   ExpectedTokens.push_back(tok::question_question);
   ExpectedTokens.push_back(tok::star_star_equal);
   ExpectedTokens.push_back(tok::at);
 
   std::vector<Token> toks = CheckLex(
        "<?php .= <> $ ** \\ === => !== <=> ?? **= @ ?>", ExpectedTokens);
 }


TEST_F(LexerTest, ShortTag) {
  std::vector<tok::TokenKind> ExpectedTokens;
  std::vector<Token> toks = CheckLex("<?", ExpectedTokens);
}

TEST_F(LexerTest, ShortTag0) {
  std::vector<tok::TokenKind> ExpectedTokens;
  // escape trigraph since there is no option to disable it in gcc
  std::vector<Token> toks = CheckLex("<?\?>", ExpectedTokens);
}

TEST_F(LexerTest, ShortTag1) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::greater);

  std::vector<Token> toks = CheckLex("<?>", ExpectedTokens);
}

TEST_F(LexerTest, ShortTag2) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::numeric_constant);
  ExpectedTokens.push_back(tok::plus);
  ExpectedTokens.push_back(tok::string_literal);

  std::vector<Token> toks = CheckLex("<?11+'a'?>", ExpectedTokens);
  EXPECT_EQ("11", getText(toks[0]));
  EXPECT_EQ("'a'", getText(toks[2]));
}

TEST_F(LexerTest, AspTag) {
  std::vector<tok::TokenKind> ExpectedTokens;
  std::vector<Token> toks = CheckLex("<%", ExpectedTokens);
}

TEST_F(LexerTest, AspTag1) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::greater);

  std::vector<Token> toks = CheckLex("<%>", ExpectedTokens);
}

TEST_F(LexerTest, AspTag2) {
  std::vector<tok::TokenKind> ExpectedTokens;

  std::vector<Token> toks = CheckLex("<%%>", ExpectedTokens);
}

TEST_F(LexerTest, AspTag3) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::percent);

  std::vector<Token> toks = CheckLex("<%%%>", ExpectedTokens);
}

TEST_F(LexerTest, AspTag4) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::numeric_constant);

  std::vector<Token> toks = CheckLex("<%22%>", ExpectedTokens);
  EXPECT_EQ("22", getText(toks[0]));
}

TEST_F(LexerTest, EchoTag) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::kw_echo);
  std::vector<Token> toks = CheckLex("<?=", ExpectedTokens);
}

TEST_F(LexerTest, EchoTag1) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::kw_echo);
  ExpectedTokens.push_back(tok::numeric_constant);

  std::vector<Token> toks = CheckLex("<?=20", ExpectedTokens);
  EXPECT_EQ("20", getText(toks[1]));
}

TEST_F(LexerTest, EchoTag2) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::kw_echo);
  ExpectedTokens.push_back(tok::numeric_constant);

  std::vector<Token> toks = CheckLex("<?=20?>", ExpectedTokens);
  EXPECT_EQ("20", getText(toks[1]));
}

TEST_F(LexerTest, ScriptTag) {
  std::vector<tok::TokenKind> ExpectedTokens;
  std::vector<Token> toks = CheckLex(
      "<script    language    =    'php'> </script>", ExpectedTokens);
}

TEST_F(LexerTest, ScriptTag0) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::numeric_constant);

  std::vector<Token> toks = CheckLex(
      "<script language  =  'php'> 22 </script>", ExpectedTokens);
  EXPECT_EQ("22", getText(toks[0]));
}

TEST_F(LexerTest, ScriptTag1) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::numeric_constant);

  std::vector<Token> toks = CheckLex(
      "<script language=php> 22 </script>", ExpectedTokens);
  EXPECT_EQ("22", getText(toks[0]));
}

TEST_F(LexerTest, ScriptTag2) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);
  ExpectedTokens.push_back(tok::numeric_constant);
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex(
    "QQQ<script language=php> 22 </script>LLL", ExpectedTokens);
  EXPECT_EQ("QQQ", getText(toks[0]));
  EXPECT_EQ("22", getText(toks[1]));
  EXPECT_EQ("LLL", getText(toks[2]));
}

TEST_F(LexerTest, ScriptTag3) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::numeric_constant);

  std::vector<Token> toks = CheckLex(
    "<script language=  \"php\"  > 22 </script>", ExpectedTokens);
  EXPECT_EQ("22", getText(toks[0]));
}

TEST_F(LexerTest, ScriptTag4) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex(
    "<script language \"php\"  > 22 </script>", ExpectedTokens);
}

TEST_F(LexerTest, ScriptTag5) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex(
    "<script language =\"php\'  > 22 </script>", ExpectedTokens);
}

TEST_F(LexerTest, ScriptTag6) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::html_data);

  std::vector<Token> toks = CheckLex(
    "<script language =\"php\' attr='nothing' > 22 </script>", ExpectedTokens);
}

TEST_F(LexerTest, MultiLineStringLiteral) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::string_literal);
  std::vector<Token> toks = CheckLex("<?'abc\n"
                                     "123456\n"
                                     "abcde\n' ?>", ExpectedTokens);
  EXPECT_EQ("'abc\n"
            "123456\n"
            "abcde\n'", getText(toks[0]));
}

TEST_F(LexerTest, SingleQuoteLiteral) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::string_literal);
  std::vector<Token> toks = CheckLex("<? '' ?>", ExpectedTokens);
  EXPECT_EQ("''", getText(toks[0]));
}

TEST_F(LexerTest, StringInterPolation0) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::quote);
  ExpectedTokens.push_back(tok::closing_quote);
  std::vector<Token> toks = CheckLex("<? \"\" ", ExpectedTokens);
}

TEST_F(LexerTest, StringInterPolation1) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::quote);
  ExpectedTokens.push_back(tok::string_literal);
  ExpectedTokens.push_back(tok::dollar);
  ExpectedTokens.push_back(tok::identifier);
  ExpectedTokens.push_back(tok::closing_quote);
  std::vector<Token> toks = CheckLex("<? \"aaaa$a\"", ExpectedTokens);
}

TEST_F(LexerTest, StringInterPolation2) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::quote);
  ExpectedTokens.push_back(tok::string_literal);
  ExpectedTokens.push_back(tok::dollar);
  ExpectedTokens.push_back(tok::identifier);
  ExpectedTokens.push_back(tok::string_literal);
  ExpectedTokens.push_back(tok::closing_quote);
  std::vector<Token> toks = CheckLexInterpolator("<? \"aaaa $a bbbbb\"", ExpectedTokens);
}

TEST_F(LexerTest, StringInterPolation3) {
  std::vector<tok::TokenKind> ExpectedTokens;
  ExpectedTokens.push_back(tok::quote);
  ExpectedTokens.push_back(tok::string_literal);
  ExpectedTokens.push_back(tok::dollar);
  ExpectedTokens.push_back(tok::identifier);
  ExpectedTokens.push_back(tok::l_square);
  ExpectedTokens.push_back(tok::r_square);
  ExpectedTokens.push_back(tok::string_literal);
  ExpectedTokens.push_back(tok::closing_quote);
  std::vector<Token> toks = CheckLexInterpolator("<? \"aaaa $a[] bbbbb\"", ExpectedTokens);
}

} // anonymous namespace
