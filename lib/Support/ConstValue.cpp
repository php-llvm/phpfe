//===--- ConstValue.cpp ---------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Class that represents a value in constant folding calculations.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/SmallString.h"
#include "phpfe/Support/ConstValue.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Support/Utilities.h"
#include "conversion/number_recognizer.h"
//------------------------------------------------------------------------------

//------ Global data -----------------------------------------------------------
#define DEBUG_TYPE "constval"

STATISTIC(NumValues, "Number of values");
//------------------------------------------------------------------------------


namespace phpfe {

static ConstValue::ConversionStatus transform_status(conversion::Status x) {
  switch (x) {
  case conversion::OK:
  case conversion::DoubleToInt:
  case conversion::IntToDouble:
  case conversion::DoubleUnderflow:
    return ConstValue::OK;

  case conversion::NaN:
    return ConstValue::NaN;

  case conversion::NegativeLimit:
  case conversion::IntOverflowNegative:
    return ConstValue::IntTooLargeNegative;

  case conversion::IntOverflow:
    return ConstValue::IntTooLarge;

  case conversion::DoubleOverflow:
    return ConstValue::FloatTooLarge;
  }
  llvm_unreachable("invalid conversion status");
}


//------ Implementation of ConstValue ------------------------------------------

ConstValue::ConstValue()
  : Kind(NotAConstant), FloatVal(0.0), Key(nullptr) {
  ++NumValues;
}


ConstValue::ConstValue(bool X)
  : Kind(Bool), BoolVal(X), FloatVal(0.0), Key(nullptr) {
  ++NumValues;
}


void ConstValue::set(const ArrayConstValue &Val) {
  Kind = Array;
  ArrayVal = Val;
}


ConstValue *ConstValue::takeKey() {
  assert(hasKey());
  ConstValue *Result = Key;
  Key = nullptr;
  return Result;
}


template <typename T>
static CompareResult three_way_compare(T A, T B) {
  if (A == B)
    return CompareResult::Equal;
  if (A < B)
    return CompareResult::Less;
  return CompareResult::Greater;
}

template <>
CompareResult three_way_compare(APInt A, APInt B) {
  if (A.eq(B))
    return CompareResult::Equal;
  if (A.slt(B))
    return CompareResult::Less;
  return CompareResult::Greater;
}

template <>
CompareResult three_way_compare(APFloat A, APFloat B) {
  switch (A.compare(B)) {
  case APFloat::cmpLessThan:    return CompareResult::Less;
  case APFloat::cmpEqual:       return CompareResult::Equal;
  case APFloat::cmpGreaterThan: return CompareResult::Greater;
  default:
    llvm_unreachable("incomparable float values");
  }
}

CompareResult ConstValue::compare(const ConstValue *V) const {
  ConversionStatus Status;

  // Boolean argument is promoted to integer or float depending on the second
  // argument.

  if (isBool()) {
    if (V->isFloat())
      return three_way_compare(getAsFloat(Status), V->getFloat());
    return three_way_compare(getAsInt(Status), V->getAsInt(Status));
  }

  if (V->isBool()) {
    if (isFloat())
      return three_way_compare(getFloat(), V->getAsFloat(Status));
    return three_way_compare(getAsInt(Status), V->getAsInt(Status));
  }

  // Null argument is converted to zero or empty string depending on the second
  // argument.

  if (isNull()) {
    if (V->isString())
      return three_way_compare((size_t)0, V->getString().size());
    if (V->isFloat())
      return three_way_compare(APFloat(0.0), V->getFloat());
    return three_way_compare(APInt(Runtime::getIntValueWidth(), 0),
                             V->getAsInt(Status));
  }

  if (V->isNull()) {
    if (isString())
      return three_way_compare(getString().size(), (size_t)0);
    if (isFloat())
      return three_way_compare(getFloat(), APFloat(0.0));
    return three_way_compare(getAsInt(Status),
                             APInt(Runtime::getIntValueWidth(), 0));
  }

  // Arrays compare to array by special algorithm, in all other cases array
  // is greater.
  if (isArray()) {
    if (V->isArray()) {
      CompareResult Result = getArray().compare(V->getArray());
      return static_cast<CompareResult>(Result);
    }
    return CompareResult::Greater;
  }

  if (V->isArray())
    return CompareResult::Less;

  // If both arguments are strings and they both are convertible to numbers,
  // convert them to numbers and then compare. Otherwise use lexicographical
  // comparison.
  if (isString() && V->isString()) {
    StringRef LHS = getString();
    StringRef RHS = V->getString();
    conversion::NumberRecognizer<> LRecog;
    LRecog.recognize(LHS.data(), LHS.size());
    if (LRecog.success()) {
      conversion::NumberRecognizer<> RRecog;
      RRecog.recognize(RHS.data(), RHS.size());
      if (RRecog.success()) {
        if (!LRecog.is_float() && !RRecog.is_float()) {
          APInt LVal, RVal;
          if (getIntFromRecognizer(LVal, LRecog) &&
              getIntFromRecognizer(RVal, RRecog)) {
            return three_way_compare(LVal, RVal);
          }
        }
        return three_way_compare(LRecog.to<double>(), RRecog.to<double>());
      }
    }

    return three_way_compare(LHS.compare(RHS), 0);
  }

  // If one argument is a float, convert the other to float then compare.
  if (isFloat()) {
    if (V->isFloat())
      return three_way_compare(getFloat(), V->getFloat());
    if (V->isInt()) {
      APFloat Val = getFloat();
      Val.convertFromAPInt(V->getInt(), true, APFloat::rmTowardZero);
      return three_way_compare(getFloat(), Val);
    }
    return three_way_compare(getFloat(), V->getAsFloat(Status));
  }

  if (V->isFloat())
    return three_way_compare(getAsFloat(Status), V->getFloat());

  // If one argument is an integer, convert the other to integer then compare.
  if (isInt()) {
    if (V->isInt())
      return three_way_compare(getInt(), V->getInt());
    return three_way_compare(getInt(), V->getAsInt(Status));
  }

  return three_way_compare(getAsFloat(Status), V->getAsFloat(Status));
}


bool ConstValue::getAsBool(ConversionStatus &Status) const {
  Status = OK;
  switch (getKind()) {
  case Null:
    return false;

  case Bool:
    return getBool();

  case Int:
    return getInt() != 0;

  case Float:
    return !getFloat().isZero();

  case String:
    return !getString().empty() && !getString().equals("0");

  case Array:
    return !getArray().empty();
  
  default:
    llvm_unreachable("unknown constant value");
  }
  return false;
}


bool ConstValue::equals_strict(const ConstValue *V) const {
  if (getKind() != V->getKind())
    return false;

  switch (getKind()) {
    case Null:
      return true;

    case Bool:
      return getBool() == V->getBool();

    case Float:
      return getFloat().compare(V->getFloat()) == APFloat::cmpEqual;

    case Array:
      return getArray().equals_strict(V->getArray());

    case Int:
      return getInt() == V->getInt();

    case String:
      return getString() == V->getString();

    default:
      llvm_unreachable("unexpected const value type");
      break;
  }
  return false;
}


APInt ConstValue::getAsInt(ConversionStatus &Status, long base) const {
  Status = OK;
  switch (getKind()) {
  case Null:
    return APInt(Runtime::getIntValueWidth(), 0, true);

  case Bool:
    return APInt(Runtime::getIntValueWidth(), getBool(), true);

  case Float: {
    double Val = getFloat().convertToDouble();
    if (Val > Runtime::getMaxInteger().signedRoundToDouble()) {
      Status = IntTooLarge;
      return Runtime::getMaxInteger();
    }
    if (Val < Runtime::getMinInteger().signedRoundToDouble()) {
      Status = IntTooLargeNegative;
      return Runtime::getMinInteger();
    }
    APInt Result(Runtime::getIntValueWidth(), (uint64_t)Val, true);
    return Result;
  }

  case Array:
    Status = ArrayConv;
    return getArray().empty()
      ? Runtime::getZeroInteger()
      : Runtime::getOneInteger();

  case Int:
    return getInt();

  case String: {
    conversion::NumberRecognizer<> Recog(0U);
    Recog.recognize(getString().data(), getString().size(), base);
    if (Recog.failure()) {
      Status = NaN;
      return Runtime::getZeroInteger();
    }
    if (!Recog.is_float()) {
      APInt IVal;
      if (getIntFromRecognizer(IVal, Recog))
        return IVal;
    }
    switch (Recog.get_status()) {
    case conversion::OK:
    case conversion::DoubleToInt:
      break;
    case conversion::DoubleOverflow:
      Status = FloatTooLarge;
      break;
    case conversion::IntOverflow:
      Status = IntTooLarge;
      break;
    case conversion::IntOverflowNegative:
    case conversion::NegativeLimit:
      Status = IntTooLargeNegative;
      break;
    default:
      Status = NaN;
      return Runtime::getZeroInteger();
    }
    if (Recog.is_negative())
      return RuntimeTypes::getMinInteger();
    return RuntimeTypes::getMaxInteger();
  }

  default:
    llvm_unreachable("unexpected const value type");
  }
}


APFloat ConstValue::getAsFloat(ConversionStatus &Status) const {
  Status = OK;
  switch (getKind()) {
    case NotAConstant:
      Status = NaN;
      break;

    case Null:
      return APFloat(0.0);

    case Bool:
      return APFloat((double)getBool());

    case Int:
      return APFloat((double)getInt().getSExtValue());

    case Float:
      return getFloat();

    case String:
      break;

    case Array:
      Status = ArrayConv;
      return APFloat(getArray().size() ? 1.0 : 0.0);
  }

  if (!isString()) {
    assert(Status != OK);
    return APFloat(0.0);
  }

  conversion::NumberRecognizer<> Recog;
  StringRef Str = getString();
  Recog.recognize(Str.data(), Str.size());
  double Val = Recog.to<double>();
  Status = transform_status(Recog.get_status());
  return APFloat(Val);
}


std::string ConstValue::getAsString(ConversionStatus &Status) const {
  Status = OK;
  switch (getKind()) {
  case NotAConstant:
    Status = NaN;
    return "";

  case Null:
    return "";

  case Bool:
    return getBool() ? "1" : "";

  case Int:
    return getInt().toString(10, true);

  case Float: {
    SmallString<32> Str;
    getFloat().toString(Str, DoubleFormatPrecision);
    return Str.str();
  }

  case String:
    return getString();

  case Array:
    return "Array";
  }
  llvm_unreachable("Unexpected constant type");
  return "";
}


bool ConstValue::isStringNumeric(bool &HasExtra) const {
  if (!isString())
    return false;
  conversion::NumberRecognizer<> Recog;
  StringRef Str = getString();
  Recog.recognize(Str.data(), Str.size());
  HasExtra = Recog.partial();
  if (Recog.failure() || Recog.get_status() == conversion::NaN)
    return false;
  return !Recog.failure();
}


bool ConstValue::isStringFloat(bool &HasExtra) const {
  if (!isString())
    return false;
  conversion::NumberRecognizer<> Recog;
  StringRef Str = getString();
  Recog.recognize(Str.data(), Str.size());
  HasExtra = Recog.partial();
  if (Recog.failure() || Recog.get_status() == conversion::NaN)
    return false;
  return Recog.is_float();
}


bool ConstValue::isStringInteger(bool &HasExtra) const {
  if (!isString())
    return false;
  conversion::NumberRecognizer<> Recog;
  StringRef Str = getString();
  Recog.recognize(Str.data(), Str.size());
  if (Recog.failure() || Recog.get_status() == conversion::NaN)
    return false;
  return !Recog.is_float();
}


bool ConstValue::isZero() const {
  ConversionStatus Status;
  return isNull() ||
         (isBool() && !getBool()) ||
         (isInt() && getInt() == 0) ||
         (isFloat() && getFloat().isZero()) ||
         (isString() && getAsFloat(Status).isZero());
}


bool ConstValue::isOne() const {
  ConstValue::ConversionStatus Status;
  bool HasExtra;
  return (isBool() && getBool()) ||
         (isInt() && getInt() == 1) ||
         (isFloat() && getFloat().compare(APFloat(1.0)) == APFloat::cmpEqual) ||
         (isStringInteger(HasExtra) &&
          (getAsFloat(Status).compare(APFloat(1.0)) == APFloat::cmpEqual) &&
          Status == OK) ||
         (isStringFloat(HasExtra) &&
          (getAsFloat(Status).compare(APFloat(1.0)) == APFloat::cmpEqual) &&
          Status == OK);
}


//------ Implementation of ArrayConstValue -------------------------------------

void ArrayConstValue::set(const SmallVectorImpl<ArrayItem> &X,
                          ConstantPool &Pool) {
  assert(Content.empty() && "Content already set");
  APInt Index = Runtime::getZeroInteger();  // For implicit keys
  bool HasIndex = false;                    // Numeric key has been seen
  bool IsMap = false;                       // Cannot be vector anymore
  for (ArrayItem Item : X) {
    if (const ConstValue *Key = Item.getKey()) {
      if (Key->isString()) {
        IsMap = true;
      } else {
        assert(Key->isInt());
        if (HasIndex) {
          if (Key->getInt() != Index)
            IsMap = true;
          if (Key->getInt().sgt(Index)) {
            Index = Key->getInt();
            Index++;
          }
        } else {
          Index = Key->getInt();
          HasIndex = true;
          Index++;
        }
      }
    } else {
      HasIndex = true;
      Item.setKey(Pool.get(Index));
      Index++;
    }
    Content.push_back(Item);
  }
  IsVector = !IsMap;
}


bool ArrayConstValue::equals(const ArrayConstValue &X) const {
  return IsVector == X.IsVector && Content == X.Content;
}


bool ArrayConstValue::less(const ArrayConstValue &X) const {
  if (IsVector > X.IsVector)
    return false;
  if (IsVector < X.IsVector)
    return true;
  return Content < X.Content;
}


bool ArrayConstValue::hasKey(const ConstValue *Key) const {
  for (auto Val : Content) {
    if (Val.getKey()->compare(Key) == CompareResult::Equal)
      return true;
  }
  return false;
}


CompareResult ArrayConstValue::compare(const ArrayConstValue &X) const {
  if (size() < X.size())
    return CompareResult::Less;
  if (size() > X.size())
    return CompareResult::Greater;

  for (size_t I = 0; I < size(); ++I) {
    if (!X.hasKey(getKey(I)))
      return CompareResult::Greater;

    auto CmpResult = getValue(I)->compare(X.getValue(I));
    if (CmpResult != CompareResult::Equal)
      return CmpResult == CompareResult::Less
        ? CompareResult::Less
        : CompareResult::Greater;
  }
  return CompareResult::Equal;
}


bool ArrayConstValue::equals_strict(const ArrayConstValue &X) const {
  if (size() != X.size())
    return false;

  for (size_t I = 0; I < size(); ++I) {
    if (!X.hasKey(getKey(I)))
      return false;

    if (!getValue(I)->equals_strict(X.getValue(I)))
      return false;
  }
  return true;
}


//------ Implementation of ConstantPool ----------------------------------------

ConstantPool::ConstantPool()
  : ZeroConstant(nullptr), OneConstant(nullptr), MinusOneConstant(nullptr),
    FloatZeroConstant(nullptr), EmptyStringConstant(nullptr) {
}


ConstantPool::~ConstantPool() {
}


void ConstantPool::init() {
  NullConstant.reset(new ConstValue());
  NullConstant->setNull();
  FalseConstant.reset(new ConstValue(false));
  TrueConstant.reset(new ConstValue(true));
  ZeroConstant = new ConstValue();
  ZeroConstant->set(Runtime::getZeroInteger());
  IntConstants[Runtime::getZeroInteger()] = ZeroConstant;
  OneConstant = new ConstValue();
  OneConstant->set(Runtime::getOneInteger());
  IntConstants[Runtime::getOneInteger()] = OneConstant;
  MinusOneConstant = new ConstValue();
  MinusOneConstant->set(Runtime::getMinusOneInteger());
  EmptyStringConstant = new ConstValue();
  EmptyStringConstant->set(std::string());
  IntConstants[Runtime::getMinusOneInteger()] = MinusOneConstant;
  FloatZeroConstant = new ConstValue();
  FloatZeroConstant->set(APFloat(APFloat::IEEEdouble, 0));
  FloatConstants[APFloat(APFloat::IEEEdouble, 0)] = FloatZeroConstant;
  StringConstants[std::string()] = EmptyStringConstant;
}


ConstValue *ConstantPool::get(const APInt &X) {
  assert(X.getBitWidth() == Runtime::getIntValueWidth());
  auto Ptr = IntConstants.find(X);
  if (Ptr != IntConstants.end()) {
    assert(Ptr->second);
    return Ptr->second;
  }
  ConstValue *NewVal = new ConstValue();
  NewVal->set(X);
  IntConstants[X] = NewVal;
  return NewVal;
}


ConstValue *ConstantPool::get(const APFloat &X) {
  auto Ptr = FloatConstants.find(X);
  if (Ptr != FloatConstants.end()) {
    assert(Ptr->second);
    return Ptr->second;
  }
  ConstValue *NewVal = new ConstValue();
  NewVal->set(X);
  FloatConstants[X] = NewVal;
  return NewVal;
}


ConstValue *ConstantPool::get(std::string X) {
  auto Ptr = StringConstants.find(X);
  if (Ptr != StringConstants.end()) {
    assert(Ptr->second);
    return Ptr->second;
  }
  ConstValue *NewVal = new ConstValue();
  NewVal->set(X);
  StringConstants[X] = NewVal;
  return NewVal;
}


ConstValue *ConstantPool::get(const SmallVectorImpl<ArrayItem> &X) {
  ArrayConstValue NewArray;
  NewArray.set(X, *this);
  auto Ptr = ArrayConstants.find(NewArray);
  if (Ptr != ArrayConstants.end()) {
    assert(Ptr->second);
    return Ptr->second;
  }
  ConstValue *NewVal = new ConstValue;
  NewVal->set(NewArray);
  ArrayConstants[NewArray] = NewVal;
  return NewVal;
}


bool ConstantPool::has(ConstValue *V) {
  assert(V);
  if (V->isNotConstant())
    return false;
  if (V->isNull()) {
    assert(V == NullConstant.get());
    return true;
  }
  if (V->isBool()) {
    assert(V == FalseConstant.get() || V == TrueConstant.get());
    return true;
  }
  if (V->isInt())
    return IntConstants.find(V->getInt()) != IntConstants.end();
  if (V->isFloat())
    return FloatConstants.find(V->getFloat()) != FloatConstants.end();
  if (V->isString())
    return StringConstants.find(V->getString()) != StringConstants.end();
  if (V->isArray())
    return ArrayConstants.find(V->getArray()) != ArrayConstants.end();
  llvm_unreachable("Bad constant type");
  return false;
}


void ConstantPool::add(ConstValue *V) {
  assert(V);
  assert(!V->isNotConstant());
  if (!has(V)) {
    if (V->isInt()) {
      IntConstants[V->getInt()] = V;
    } else if (V->isFloat()) {
      FloatConstants[V->getFloat()] = V;
    } else if (V->isString()) {
      StringConstants[V->getString()] = V;
    } else if (V->isArray()) {
      ArrayConstants[V->getArray()] = V;
    }
    llvm_unreachable("Bad constant type");
  }
}

}
