//===--- LegalizePhpNodes.cpp - AST Transformation Component ----*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Transformation that makes AST valid from viewpoint of Clang Code Generator.
// These methods legalize specific PHP nodes.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Transformer/Legalizer.h"
#include "phpfe/Sema/Runtime.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Stmt *Legalizer::VisitPhpEchoStmt(PhpEchoStmt *S) {
  Expr *Result = nullptr;
  for (Expr *Arg : S->arguments())
    if (Expr *Output = LegalizeEchoArgument(Arg))
      Result = Trans.createChainExpression(Arg->getLocStart(), Result, Output);
  if (!Result)
    return new(Context) NullStmt(S->getEchoLoc());
  return Result;
}


Expr *Legalizer::LegalizeEchoArgument(Expr *Arg) {
  SmallVector<Expr*, 4> CallArgs;
  Expr *CallExpr;

  Expr *ArgExpr = cast<Expr>(Visit(Arg));
  const Type *ArgT = ArgExpr->getType().getTypePtr();

  if (const ConstantArrayType* ArrayT = dyn_cast<ConstantArrayType>(ArgT)) {
    QualType ElementT = ArrayT->getElementType();
    ElementT.removeLocalCVRQualifiers(Qualifiers::CVRMask);
    if (ElementT == Context.CharTy) {
      // String literal.
      IntegerLiteral* SzExpr = IntegerLiteral::Create(Context,
          ArrayT->getSize() - 1, Context.getSizeType(), Arg->getLocStart());
      CallArgs.push_back(ArgExpr);
      CallArgs.push_back(SzExpr);
      CallExpr = Trans.createRuntimeFunctionCall("echo_txt", CallArgs);
    }
  } else if (ArgT->isVoidType()) {
    // Void value does not produce any output but it might have side effects.
    return ArgExpr;
  } else if (ArgT->isBooleanType()) {
    CallArgs.push_back(ArgExpr);
    CallExpr = Trans.createRuntimeFunctionCall("echo_bool", CallArgs);
  } else if (ArgT->isIntegerType()) {
    CallArgs.push_back(ArgExpr);
    CallExpr = Trans.createRuntimeFunctionCall("echo_integer", CallArgs);
  } else if (ArgT->isFloatingType()) {
    CallArgs.push_back(ArgExpr);
    CallExpr = Trans.createRuntimeFunctionCall("echo_double", CallArgs);
  } else if (RuntimeTypes::isBoxedType(ArgExpr->getType())) {
    CallArgs.push_back(ArgExpr);
    CallExpr = Trans.createRuntimeFunctionCall("echo_box", CallArgs);
  } else {
    // TODO: handle string and universal types
    llvm_unreachable("Unknown type of echo argument");
  }
  return CallExpr;
}


Expr *Legalizer::VisitPhpArrayExpr(PhpArrayExpr *E) {
  if (ConstValue *Val = Semantics.getConstantValue(E))
    return Semantics.BuildArrayInitVariable(E->getExprLoc(), Val->getArray());

  llvm_unreachable("non const arrays are not supported yet");
  return nullptr;
}

}
