//===--- ReducePhpNodes.cpp - AST Transformation Component ------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Implementation of class Reducer, PHP specific nodes.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Transformer/Reducer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Expr *Reducer::VisitPhpArrayExpr(PhpArrayExpr *E) {
  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return Semantics.getConstantExpr(V, E->getLocStart(), true);

  if (EV.seenError())
    return E;

  for (unsigned I = 0; I < E->getNumElements(); ++I) {
    Expr *Item = E->getElement(I);
    Expr *NewItem = cast<Expr>(Visit(Item));
    assert(NewItem);
    if (Item != NewItem) {
      E->setElement(I, NewItem);
      Changed = true;
    }
  }
  return E;
}


Expr *Reducer::VisitPhpMapExpr(PhpMapExpr *E) {
  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return Semantics.getConstantExpr(V, E->getLocStart(), true);

  if (EV.seenError())
    return E;

  Expr *Item = E->getLHS();
  Expr *NewItem = cast<Expr>(Visit(Item));
  if (Item != NewItem) {
    E->setLHS(NewItem);
    Changed = true;
  }
  Item = E->getLHS();
  NewItem = cast<Expr>(Visit(Item));
  if (Item != NewItem) {
    E->setRHS(NewItem);
    Changed = true;
  }
  return E;
}


Stmt *Reducer::VisitPhpEchoStmt(PhpEchoStmt *S) {
  if (S->getNumArgs() == 0) {
    Changed = true;
    return new(Context) NullStmt(S->getEchoLoc());
  }

  if (S->getNumArgs() == 1) {
    Expr *E = cast<Expr>(Visit(S->getArg(0)));
    if (E != S->getArg(0)) {
      Changed = true;
      S->setArgs(Context, &E, 1);
    }
    return S;
  }

  //TODO: multiple arguments
  return S;
}


}
