//===--- Legalizer.cpp - AST Transformation Component -----------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Transformation that makes AST valid from viewpoint of Clang Code Generator.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Transformer/Legalizer.h"
#include "phpfe/Sema/Runtime.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Legalizer::Legalizer(Transformer &T) : Trans(T), Semantics(T.getSema()),
  Context(T.getASTContext()), CurrentFunction(nullptr) {
}


Legalizer::~Legalizer() {
}


bool Legalizer::LegalizeFunction(FunctionDecl *FDecl) {
  // Skip non-PHP functions.
  //TODO: do we need this check?
  if (!Semantics.isPhpFunction(FDecl) && FDecl != Semantics.getUnitFunction())
    return true;

  // Declarations without definition do not need legalization.
  if (!FDecl->hasBody())
    return true;

  // Transform function body.
  CurrentFunction = FDecl;
  DeclContext *PrevContext = Semantics.CurContext;
  Semantics.CurContext = FDecl;
  Stmt *Body = FDecl->getBody();
  Stmt *NewBody = Visit(Body);
  if (Body != NewBody)
    FDecl->setBody(NewBody);
  Semantics.CurContext = PrevContext;
  CurrentFunction = nullptr;

  //TODO: If errors were emitted, stop legalization.
  return true;
}


bool Legalizer::LegalizeClass(CXXRecordDecl *CD) {
  return true;
}


Stmt *Legalizer::VisitStmt(Stmt* S) {
  // S->dump();
  // llvm_unreachable("unexpected statement in Legalizer!");
  return S;
}


Stmt *Legalizer::VisitCompoundStmt(CompoundStmt *S) {
  SmallVector<Stmt*, 8> NewStatements;
  for (Stmt *St : S->body()) {
    Stmt *Res = Visit(St);
    if (!Res)
      continue;
//     if (isa<Expr>(Res))
//       Res = Semantics.MaybeBuildExprWithCleaunups(cast<Expr>(Res));
    NewStatements.push_back(Res);
  }
  return new (Context) CompoundStmt(Context, NewStatements,
                                    S->getLocStart(), S->getLocEnd());
}


Stmt *Legalizer::VisitIfStmt(IfStmt *S) {
  Expr *Cond = legalizeBooleanExpr(S->getCond());
  //TODO: cond is full expr
  S->setCond(Cond);
  S->setThen(Visit(S->getThen()));
  if (S->getElse())
    S->setElse(Visit(S->getElse()));
  return S;
}

Stmt *Legalizer::VisitWhileStmt(WhileStmt *S) {
  Expr *Cond = legalizeBooleanExpr(S->getCond());
  //TODO: cond is full expr
  S->setCond(Cond);
  S->setBody(Visit(S->getBody()));
  return S;
}


Stmt *Legalizer::VisitReturnStmt(ReturnStmt *S) {
  QualType RetTy = CurrentFunction->getReturnType();
  if (Expr *RetValue = S->getRetValue()) {
    Expr *RetExpr = cast<Expr>(Visit(RetValue));
    if (RetTy->isVoidType()) {
      // Allow returning of void values in void functions.
      //
      // @code
      //     function f() : void {...}
      //     function g() : void { return f(); }
      // @endcode
      //
      // In other cases we must not see void type here. If the function was
      // declared as returning void but contains return statement that returns
      // something non-void, it should has been detected by parser.
      assert(RetTy->isVoidType());

      // Although return value is ignored, we cannot throw away the return
      // expression, as it may produce side effects. They must be collected in
      // comma expression.
      if (BinaryOperator *BO = dyn_cast<BinaryOperator>(RetExpr)) {
        assert(BO->getOpcode() == BO_Comma);
        assert(isa<PhpNullLiteral>(BO->getRHS()));
        // LHS of comma operation must contain side effects.
        return BO->getLHS();
      }
      if (isa<PhpNullLiteral>(RetExpr))
        return Semantics.createNullStmt(RetExpr->getLocStart());
      llvm_unreachable("unexpected void node");
    }

    Expr *NewRetValue = LegalizeReturnExpr(CurrentFunction, RetExpr);
    if (NewRetValue != RetValue)
      S->setRetValue(NewRetValue);
  }
  return S;
}


Stmt *Legalizer::VisitDeclStmt(DeclStmt *S) {
  SmallVector<Stmt *, 16> InitStatements;
  if (CurrentFunction == Semantics.getUnitFunction()) {
    // Declaration of global objects.
    for (Decl *D : S->getDeclGroup()) {
      if (VarDecl *VD = dyn_cast<VarDecl>(D)) {
        if (Semantics.isPhpConstant(VD)) {
          // PHP constant
          assert(VD->getInit());
          // Make initializing call.
          SmallVector<Expr *, 2> Args;
          Expr *VarRef = DeclRefExpr::Create(Context, NestedNameSpecifierLoc(),
                                             SourceLocation(), VD, false,
                      DeclarationNameInfo(VD->getDeclName(), VD->getLocation()),
                                             getValueInfoType(), VK_LValue);
          Args.push_back(VarRef);
          Args.push_back(cast<Expr>(Visit(VD->getInit())));
          Expr *ICall = getRuntime().makeRuntimeFunctionCall("init_constant",
                                                             Args);
          InitStatements.push_back(ICall);
        } else if (Semantics.isPhpVariable(VD)) {
          // PHP variable
          //TODO:
        } else {
          llvm_unreachable("wrong PHP entity");
        }
      }
    }
  } else {
    // Declaration of local objects.
    //TODO:
  }
  if (InitStatements.empty())
    return nullptr;
  CompoundStmt *Res = new(Context) CompoundStmt(Context, InitStatements,
     InitStatements.front()->getLocStart(), InitStatements.back()->getLocEnd());
  return Res;
}


Expr *Legalizer::VisitPhpNullLiteral(PhpNullLiteral *E) {
  return Semantics.getRuntime().createNullBox(E->getNullLoc());
}


Expr *Legalizer::VisitCXXMemberCallExpr(CXXMemberCallExpr *E) {
  return E;
}


Expr *Legalizer::VisitParenExpr(ParenExpr *E) {
  E->setSubExpr(cast<Expr>(Visit(E->getSubExpr())));
  // It is necessery to set correct type after legalizing ParenExpr
  // otherwise it can lead to unnecessary casts or even absense of
  // required casts
  E->setType(E->getSubExpr()->getType());
  return E;
}


// Legalizes reading subscript, write access should be handled by different
// method.
Expr *Legalizer::VisitArraySubscriptExpr(ArraySubscriptExpr *E) {
  // If this is a correct node from C++ viewpoint, do not legalize it. Use base
  // type for that, the node type is not reliable as it may be deduced
  // (string[x] is string, array(1,2,3)[x] is int).
  QualType BaseTy = E->getLHS()->getType();
  if (BaseTy->isArrayType())
    return E;

  Expr *Base = cast<Expr>(Visit(E->getLHS()));
  Expr *Key = cast<Expr>(Visit(E->getRHS()));

  Expr *Result = getRuntime().generateSubscriptRead(Base, Key);
  assert(Result);
  return Result;
}


Expr *Legalizer::LegalizeCastExpression(QualType CastType, Expr *SubExpr) {
  QualType ExprType = SubExpr->getType();
  if (CastType == ExprType)
    return SubExpr;

  if (isBoxedType(ExprType)) {
    if (CastType == getBoolType())
      return getRuntime().generateToBoolNode(SubExpr);

    if (CastType == getIntValueType())
      return getRuntime().generateToIntegerNode(SubExpr);

    if (CastType == getDoubleType())
      return getRuntime().generateToDoubleNode(SubExpr);

    if (CastType == getNumberBoxType())
      return getRuntime().generateToNumberNode(SubExpr);

    if (isStringType(CastType))
      return getRuntime().generateToStringNode(SubExpr);

    if (isArrayType(CastType))
      return getRuntime().generateToArrayNode(SubExpr);

    if (CastType == getObjectType()) {
      //TODO: add cast to object
      llvm_unreachable("cast to object is not implemented yet");
    }
    llvm_unreachable("unexpected cast type");
  }

  if (CastType == getBoolType()) {
    if (isArrayType(ExprType) || isStringType(ExprType)) {
      Expr *BoxExpr = getSema().getRuntime().createExprBox(SubExpr);
      return getRuntime().generateToBoolNode(BoxExpr);
    }
    if (ExprType == getObjectType()) {
      llvm_unreachable("cast from object is not implemented yet");
    }
  }

  if (CastType == getIntValueType()) {
    if (isArrayType(ExprType) || isStringType(ExprType)) {
      Expr *BoxExpr = getSema().getRuntime().createExprBox(SubExpr);
      return getRuntime().generateToIntegerNode(BoxExpr);
    }
    if (ExprType == getObjectType()) {
      llvm_unreachable("cast from object is not implemented yet");
    }
  }

  if (CastType == getDoubleType()) {
    if (isArrayType(ExprType) || isStringType(ExprType)) {
      Expr *BoxExpr = getSema().getRuntime().createExprBox(SubExpr);
      return getRuntime().generateToDoubleNode(BoxExpr);
    }
    if (ExprType == getObjectType()) {
      llvm_unreachable("cast from object is not implemented yet");
    }
  }

  if (isStringType(CastType)) {
    Expr *BoxExpr = getSema().getRuntime().createExprBox(SubExpr);
    return getRuntime().generateToStringNode(BoxExpr);
  }

  if (isArrayType(CastType)) {
    Expr *BoxExpr = getSema().getRuntime().createExprBox(SubExpr);
    return getRuntime().generateToArrayNode(BoxExpr);
  }

  if (isObjectType(CastType)) {
    //TODO: implement cast
    llvm_unreachable("cast to object is not implemented yet");
  }

  return nullptr;
}


Expr *Legalizer::VisitCStyleCastExpr(CStyleCastExpr *E) {
  Expr *SubExpr = cast<Expr>(Visit(E->getSubExpr()));
  QualType CastType = E->getType();
  
  if (Expr *CastExpr = LegalizeCastExpression(CastType, SubExpr))
    return CastExpr;

  assert(E->getCastKind() != CK_Dependent);
  if (E->getSubExpr() != SubExpr)
    E->setSubExpr(SubExpr);
  return E;
}


Expr *Legalizer::VisitImplicitCastExpr(ImplicitCastExpr *E) {
  Expr *SubExpr = cast<Expr>(Visit(E->getSubExpr()));
  QualType CastType = E->getType();

  if (E->getCastKind() == CK_ToVoid)
    return E;

  if (Expr *CastExpr = LegalizeCastExpression(CastType, SubExpr))
    return CastExpr;

  assert(E->getCastKind() != CK_Dependent);
  if (E->getSubExpr() != SubExpr)
    E->setSubExpr(SubExpr);
  return E;
}


Expr *Legalizer::LegalizeAccess(Expr *E) {
  //TODO: implement access;
  assert(E->isLValue());
  return cast<Expr>(Visit(E));
}


/// \brief Casts expression to the specified type.
///
Expr *Legalizer::castBetweenBoxed(QualType TargetType, Expr *E) {
  QualType ExprType = E->getType();
  assert(isRefBoxType(TargetType) || isBoxedType(TargetType));
  assert(isRefBoxType(ExprType) || isBoxedType(ExprType));

  if (ExprType == TargetType)
    return E;

  // Cast argument to base type if necessary.
  if (ExprType.getLocalUnqualifiedType() != getPlainBoxType()) {
    CXXCastPath CastPath;
    if (isRefBoxType(ExprType))
      CastPath.push_back(getBaseOfRefBox());
    else if (isBoxedType(ExprType))
      CastPath.push_back(getBaseOfBoxed());
    else
      llvm_unreachable("boxed type expected");

    QualType BaseType = getPlainBoxType();
    if (ExprType.isConstQualified())
      BaseType.addConst();

    E = ImplicitCastExpr::Create(Context, BaseType, CK_DerivedToBase, E,
                                 &CastPath, E->getValueKind());
  } else {
    assert(E->getValueKind() == VK_LValue);
  }

  // Cast base type to another box type.
  if (TargetType.getLocalUnqualifiedType() != getPlainBoxType()) {
    CXXCastPath CastPath;

    if (TargetType.getLocalUnqualifiedType() == getClassBoxType()) {
      CastPath.push_back(getBaseOfBox());
    } else if (isRefBoxType(TargetType)) {
      CastPath.push_back(getBaseOfBox());
      CastPath.push_back(getBaseOfRefBox());
    } else if (isBoxedType(TargetType)) {
      CastPath.push_back(getBaseOfBox());
      CastPath.push_back(getBaseOfBoxed());
    } else
      llvm_unreachable("boxed type expected");

    E = ImplicitCastExpr::Create(Context, TargetType, CK_BaseToDerived,
                                 E, &CastPath, E->getValueKind());
  }

  return E;
}


// Logical ! operator
Expr *Legalizer::VisitUnaryLNot(UnaryOperator *E) {
  Expr *SubExpr = cast<Expr>(Visit(E->getSubExpr()));
  E->setSubExpr(SubExpr);
  return E;
}


static const char *getIncDecRTFunction(UnaryOperatorKind Kind) {
  switch (Kind) {
  case UO_PostInc:  return "postfix_increment";
  case UO_PreInc:   return "prefix_increment";
  case UO_PostDec:  return "postfix_decrement";
  case UO_PreDec:   return "prefix_decrement";
  default:
    break;
  }
  llvm_unreachable("unary operator kind is not increment nor decremenet");
  return "";
}


Expr *Legalizer::LegalizeUnaryIncDec(UnaryOperator *E) {
  auto Kind = E->getOpcode();
  assert(Kind == UO_PostInc ||
         Kind == UO_PreInc  ||
         Kind == UO_PostDec ||
         Kind == UO_PreDec);

  Expr *Base = E->getSubExpr();
  QualType BaseType = Base->getType();

  if (Runtime::isObjectType(BaseType)) {
    llvm_unreachable("increment for objects is not implemented yet");
  }

  //TODO: handle object properties

  assert(!BaseType->isBooleanType());
  if (BaseType->isIntegerType()) {
    Expr *NewBase = cast<Expr>(Visit(Base));
    if (NewBase != Base) {
      assert(NewBase->getType() == BaseType);
      E->setSubExpr(NewBase);
    }
    return E;
  }

  assert(Runtime::isUniversalType(BaseType) ||
         Runtime::isStringType(BaseType) ||
         BaseType->isFloatingType());

  Expr *Access = LegalizeAccess(Base);
  SmallVector<Expr*, 1> Args;
  Args.push_back(Access);
  std::string FName = getIncDecRTFunction(Kind);
  if (BaseType->isFloatingType())
    FName += "_double";
  return Trans.createRuntimeFunctionCall(FName, Args);
}


Expr *Legalizer::VisitUnaryPostInc(UnaryOperator *E) {
  assert(E->getOpcode() == UO_PostInc);
  return LegalizeUnaryIncDec(E);
}


Expr *Legalizer::VisitUnaryPreInc(UnaryOperator *E) {
  assert(E->getOpcode() == UO_PreInc);
  return LegalizeUnaryIncDec(E);
}


Expr *Legalizer::VisitUnaryPostDec(UnaryOperator *E) {
  assert(E->getOpcode() == UO_PostDec);
  return LegalizeUnaryIncDec(E);
}


Expr *Legalizer::VisitUnaryPreDec(UnaryOperator *E) {
  assert(E->getOpcode() == UO_PreDec);
  return LegalizeUnaryIncDec(E);
}


Expr *Legalizer::VisitOpaqueValueExpr(OpaqueValueExpr *E) {
  return cast<Expr>(Visit(E->getSourceExpr()));
}

}
