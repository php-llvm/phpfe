//===--- Eliminator.cpp - AST Transformation Component ----------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Transformation that eliminated unnecessary operations.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/Expr.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Transformer/Eliminator.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


class DiagnosticSuppress {
  Eliminator &Host;
  bool SavedFlag;
public:
  DiagnosticSuppress(Eliminator &X, bool V = true) : Host(X) {
    SavedFlag = X.isDiagnosticSuppressed();
    X.suppressDiagnostic(V);
  }
  ~DiagnosticSuppress() {
    Host.suppressDiagnostic(SavedFlag);
  }
};


/// \brief Removes unused subexpressions from the given expression.
/// \param E Expression result of which is unused;
/// \returns Expression that must be retained. It is a sequence of
/// subexpressions having side effect concatenated by comma operation.
///
Expr *Eliminator::eliminateUnused(Expr *E, bool SuppressDiagnostic) {
  DiagnosticSuppressed = SuppressDiagnostic;
  return Visit(E);
}


Expr *Eliminator::VisitExpr(Expr *E) {
  // By default expression is retained.
  return E;
}


Expr *Eliminator::VisitBinaryOperator(BinaryOperator *E) {
  if (E->isAssignmentOp(E->getOpcode()))
    return E;

  if (!DiagnosticSuppressed)
    Semantics.Diag(E->getExprLoc(), diag::warn_php_unused_expression)
      << E->getLHS()->getSourceRange() << E->getRHS()->getSourceRange();
  DiagnosticSuppress X(*this);

  Expr *LHS = Visit(E->getLHS());
  Expr *RHS = Visit(E->getRHS());

  return Trans.createChainExpression(E->getExprLoc(), LHS, RHS);
}


Expr *Eliminator::VisitUnaryOperator(UnaryOperator *E) {
  switch (E->getOpcode()) {
  case UO_PreInc:
  case UO_PreDec:
  case UO_PostInc:
  case UO_PostDec:
    return E;
  default:
    break;
  }
  if (!DiagnosticSuppressed)
    Semantics.Diag(E->getExprLoc(), diag::warn_php_unused_expression)
    << E->getSubExpr()->getSourceRange();
  DiagnosticSuppress X(*this);
  return Visit(E->getSubExpr());
}


Expr *Eliminator::VisitBinComma(BinaryOperator *E) {
  DiagnosticSuppress X(*this, true);
  Expr *LHS = Visit(E->getLHS());
  Expr *RHS = Visit(E->getRHS());
  return Trans.createChainExpression(E->getExprLoc(), LHS, RHS);
}


Expr *Eliminator::VisitPhpArrayExpr(PhpArrayExpr *E) {
  if (!DiagnosticSuppressed)
    Semantics.Diag(E->getExprLoc(), diag::warn_php_unused_expression)
    << E->getSourceRange();
  DiagnosticSuppress X(*this);

  Expr *Result = nullptr;
  for (auto V : E->items()) {
    if (Expr *Element = Visit(V))
      Result = Trans.createChainExpression(V->getExprLoc(), Result, Element);
  }

  return Result;
}


Expr *Eliminator::VisitPhpMapExpr(PhpMapExpr *E) {
  DiagnosticSuppress X(*this);
  Expr *LHS = Visit(E->getLHS());
  Expr *RHS = Visit(E->getRHS());
  return Trans.createChainExpression(E->getExprLoc(), LHS, RHS);
}


Expr *Eliminator::VisitParenExpr(ParenExpr *E) {
  return Visit(E->getSubExpr());
}


Expr *Eliminator::VisitCallExpr(CallExpr* E) {
  if (!isa<StringLiteral>(E->getCallee()))
    return E;

  StringRef FName = cast<StringLiteral>(E->getCallee())->getBytes();
  if (const BuiltinInfo *BI = find_builtin_function(FName)) {
    if (!BI->Attributes.isPure())
      return E;
  } else {
    return E;
  }

  if (!DiagnosticSuppressed) {
    Semantics.Diag(E->getLocStart(), diag::warn_php_unused_expression)
        << E->getSourceRange();
  }
  DiagnosticSuppress X(*this);

  // Check arguments of pure builtin function.
  Expr *Result = nullptr;
  for (auto Arg : E->arguments()) {
    if (Expr *Val = Visit(Arg)) {
      Result = Trans.createChainExpression(Arg->getExprLoc(), Result, Val);
    }
  }
  return Result;
}


Expr *Eliminator::VisitPhpNullLiteral(PhpNullLiteral *E) {
  // Null is treated much like void, so no warning.
  return nullptr;
}


Expr *Eliminator::VisitCXXBoolLiteralExpr(CXXBoolLiteralExpr *E) {
  if (!DiagnosticSuppressed)
    Semantics.Diag(E->getLocStart(), diag::warn_php_unused_expression)
    << E->getSourceRange();
  return nullptr;
}


Expr *Eliminator::VisitIntegerLiteral(IntegerLiteral *E) {
  if (!DiagnosticSuppressed)
    Semantics.Diag(E->getLocStart(), diag::warn_php_unused_expression)
    << E->getSourceRange();
  return nullptr;
}


Expr *Eliminator::VisitFloatingLiteral(FloatingLiteral *E) {
  if (!DiagnosticSuppressed)
    Semantics.Diag(E->getLocStart(), diag::warn_php_unused_expression)
    << E->getSourceRange();
  return nullptr;
}


Expr *Eliminator::VisitStringLiteral(StringLiteral *E) {
  if (!DiagnosticSuppressed)
    Semantics.Diag(E->getLocStart(), diag::warn_php_unused_expression)
    << E->getSourceRange();
  return nullptr;
}


Expr *Eliminator::VisitDeclRefExpr(DeclRefExpr *E) {
  Decl *D = E->getDecl();
  // TODO: warnings on unused variable is suppressed because such variable can
  // appear as a result of optimizations ($x += null).
  if (!Semantics.isPhpConstant(cast<VarDecl>(D)))
    return nullptr;
  if (!DiagnosticSuppressed)
    Semantics.Diag(E->getLocStart(), diag::warn_php_unused_expression)
      << E->getSourceRange();
  return nullptr;
}

} // namespace phpfe
