//===--- LegalizeBuiltin.cpp ------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Legalization of builtin functions.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Builtin/BuiltinHandlers.h"
#include "phpfe/Transformer/Legalizer.h"
#include "shared/value_kinds.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Expr* legalizeVarDump(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() >= 1);
  Sema& Semantics = L.getSema();

  // process first argument
  SmallVector<Expr*, 1> Args;
  Args.push_back(cast<Expr>(L.Visit(E->getArg(0))));
  Expr *VDCall = Semantics.getRuntime().makeRuntimeFunctionCall("var_dump", Args);

  // process extra arguments (if present)
  for (unsigned i = 1; i < E->getNumArgs(); ++i) {
    SmallVector<Expr*, 1> TmpArgs;
    TmpArgs.push_back(cast<Expr>(L.Visit(E->getArg(i))));
    Expr *ExtraVDCall = Semantics.getRuntime().makeRuntimeFunctionCall("var_dump", TmpArgs);
    VDCall = Semantics.packSideEffect(E->getLocStart(), VDCall, ExtraVDCall);
  }

  return VDCall;
}

Expr* legalizeAssert(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1); // assert accepts only 1 argument
  Sema& Semantics = L.getSema();
  PresumedLoc AssertLoc = Semantics.getPresumedLocForSourceLoc(E->getLocStart());

  StringRef FileName = AssertLoc.getFilename();
  unsigned Line = AssertLoc.getLine();

  // Extract and legalize assert explicit argument.
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));

  // If argument is not a boolean value, display error message.
  // NOTE: variables holding boolean value are NOT considered boolean here.
  if (!Arg->getType()->isBooleanType()) {
    Semantics.Diag(Arg->getLocStart(),
                   diag::err_php_non_bool_assert);
    return E;
  }

  Expr *FileNameArg = Semantics.BuildStringLiteral(FileName, E->getLocStart());
  Expr *LineArg = Semantics.ActOnIntegerConstant(E->getLocStart(), Line).get();

  SmallVector<Expr *, 3> Args;
  Args.push_back(Arg);
  Args.push_back(FileNameArg);
  Args.push_back(LineArg);

  return Semantics.getRuntime().makeRuntimeFunctionCall("rt_assert", Args);
}

Expr* legalizeIsInt(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been proccessed in Reduce/Evaluator
  assert(Runtime::isUniversalType(Arg->getType()));
  return L.getSema().getRuntime().generateIsIntNode(Arg);
}

Expr* legalizeIsBool(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been proccessed in Reduce/Evaluator
  assert(Runtime::isUniversalType(Arg->getType()));
  return L.getSema().getRuntime().generateIsBoolNode(Arg);
}

Expr* legalizeIsFloat(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been proccessed in Reduce/Evaluator
  assert(Runtime::isUniversalType(Arg->getType()));
  return L.getSema().getRuntime().generateIsFloatNode(Arg);
}

Expr* legalizeIsNumeric(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been proccessed in Reduce/Evaluator
  assert(Runtime::isUniversalType(Arg->getType()));
  return L.getSema().getRuntime().generateIsNumericNode(Arg);
}

Expr* legalizeIsString(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been proccessed in Reduce/Evaluator
  assert(Runtime::isUniversalType(Arg->getType()));
  return L.getSema().getRuntime().generateIsStringNode(Arg);
}

Expr* legalizeIsArray(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been proccessed in Reduce/Evaluator
  assert(Runtime::isUniversalType(Arg->getType()));
  return L.getSema().getRuntime().generateIsArrayNode(Arg);
}

Expr* legalizeIsScalar(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been proccessed in Reduce/Evaluator
  assert(Runtime::isUniversalType(Arg->getType()));
  return L.getSema().getRuntime().generateIsScalarNode(Arg);
}

Expr* legalizeIsObject(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been proccessed in Reduce/Evaluator
  assert(Runtime::isUniversalType(Arg->getType()));
  return L.getSema().getRuntime().generateIsObjectNode(Arg);
}

Expr* legalizeIsNull(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been proccessed in Reduce/Evaluator
  assert(Runtime::isUniversalType(Arg->getType()));
  return L.getSema().getRuntime().generateIsNullNode(Arg);
}


Expr *legalizeGetType(Legalizer &L, const BuiltinInfo &FInfo, CallExpr *E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  // Non universal types should have been processed in Reducer/Evaluator.
  assert(Runtime::isUniversalType(Arg->getType()) ||
         Arg->getType() == Runtime::getNumberBoxType());

  SmallVector<Expr*, 1> Args;
  Args.push_back(Arg);
  return L.getSema().getRuntime().makeRuntimeFunctionCall("gettype", Args);
}


Expr *legalizeSetType(Legalizer &L, const BuiltinInfo &FInfo, CallExpr *E) {
  assert(E->getNumArgs() == 2);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));
  Expr *TypeNameExpr = cast<Expr>(L.Visit(E->getArg(1)));

  Runtime& RT = L.getSema().getRuntime();
  value_type TargetType = tyInternal;

  // If type name is a string literal, we can use numbers instead of strings
  // to denote target type.
  if (StringLiteral *TypeNameNode = dyn_cast<StringLiteral>(TypeNameExpr)) {
    std::string TypeName = TypeNameNode->getBytes().lower();
    TargetType = StringSwitch<value_type>(TypeName)
      .Cases("boolean", "bool", tyBool)
      .Cases("integer", "int", tyInteger)
      .Cases("float", "double", tyDouble)
      .Case ("string", tyString)
      .Case ("array", tyArray)
      .Case ("object", tyObject)
      .Case ("null", tyNull);
  }

  // If type name is known at compile time, we can set variable type directly.
  if (TargetType != tyInternal) {
    if (isa<DeclRefExpr>(Arg)) {
      // Variable on LHS.
      if (TargetType == tyNull)
        return RT.generateClearNode(Arg);
      if (TargetType == tyBool)
        return RT.generateConvertToBoolNode(Arg);
      if (TargetType == tyInteger)
        return RT.generateConvertToIntegerNode(Arg);
      if (TargetType == tyDouble)
        return RT.generateConvertToDoubleNode(Arg);
      if (TargetType == tyString)
        return RT.generateConvertToStringNode(Arg);
      if (TargetType == tyArray)
        return RT.generateConvertToArrayNode(Arg);
      if (TargetType == tyObject)
        return RT.generateConvertToObjectNode(Arg);
      assert(TargetType == tyInternal);
      llvm_unreachable("invalid type name");
    } else {
      // LHS is array element, object property, ...
      llvm_unreachable("Unimplemented");
    }
  }

  // If target type is not known at compile time, use runtime function call.
  TypeNameExpr = L.legalizeStringRefExpr(TypeNameExpr);
  assert(Runtime::isUniversalType(Arg->getType()));
  assert(TypeNameExpr->getType().getCanonicalType()
                                                == Runtime::getStringRefType());

  // Simple case when can use reference to variable directly
  if (isa<DeclRefExpr>(Arg)) {
    // Variable on LHS.
    SmallVector<Expr *, 2> Args;
    Args.push_back(Arg);
    Args.push_back(TypeNameExpr);
    return RT.makeRuntimeFunctionCall("settype", Args);
  } else {
    // LHS is array element, object property, ...
    llvm_unreachable("Unimplemented");
  }

  return E;
}

Expr *legalizeEmpty(Legalizer &L, const BuiltinInfo &FInfo, CallExpr *E) {
  assert(E->getNumArgs() == 1);
  Expr *Arg = cast<Expr>(L.Visit(E->getArg(0)));

  StringRef FKind;

  if (RuntimeTypes::isBoxedType(Arg->getType())) {
    FKind = "empty";
  } else if (Runtime::isArrayType(Arg->getType())) {
    FKind = "empty_arr";
    // TODO: legalize array expr
  } else if (Runtime::isStringType(Arg->getType())) {
    FKind = "empty_arr";
    Arg = L.legalizeStringRefExpr(Arg);
  } else if (Runtime::isObjectType(Arg->getType())) {
    FKind = "empry_obj";
    // TODO: legalize object expr
  } else {
    llvm_unreachable("Unimplemented!");
  }

  SmallVector<Expr*, 1> Args;
  Args.push_back(Arg);
  return L.getSema().getRuntime().makeRuntimeFunctionCall(FKind, Args);
}

}
