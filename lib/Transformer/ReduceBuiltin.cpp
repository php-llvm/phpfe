//===--- ReduceBuiltin.cpp --- AST Transformation Component -----*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Implementation of reduce handlers for builtin functions.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/AST/ASTContext.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/Lex/Preprocessor.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Transformer/Eliminator.h"
#include "phpfe/Transformer/Reducer.h"
#include "shared/constants.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Expr *Reducer::VisitCallExpr(CallExpr *E) {
  for (unsigned i = 0; i < E->getNumArgs(); ++i) {
    Expr *Arg = cast<Expr>(Visit(E->getArg(i)));
    if (Arg != E->getArg(i)) {
      E->setArg(i, Arg);
      Changed = true;
    }
  }

  if (!isa<StringLiteral>(E->getCallee()))
    return E;

  StringLiteral *S = cast<StringLiteral>(E->getCallee());
  auto *BI = find_builtin_function(S->getString());
  if (!BI)
    return E;

  if (BI->Handlers.ReducerH) {
    if (Expr *Result = BI->Handlers.ReducerH(*this, *BI, E))
      return Result;
  }

  if (BI->Attributes.isPure()) {
    Evaluator EV(getSema());
    if (ConstValue *V = EV.evaluate(E))
      return getSema().getConstantExpr(V, E->getExprLoc(), true);
    if (EV.seenError())
      return E;
  }
  return E;
}

Expr *reduceIsInt(Reducer &E, const BuiltinInfo& FInfo,
                  CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);


  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy) || Runtime::isNumberType(ArgTy))
    return CE;

  Expr *Result = nullptr;

  // If argument is non-boolean integer type - return true
  // to preserve potential side effects we need to use chain expression

  if (Runtime::isIntegerType(ArgTy)) {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_true).get();
  }
  else {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
  }

  return E.replaceBy(Arg, Result);
}

Expr *reduceIsFloat(Reducer &E, const BuiltinInfo& FInfo,
                  CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy) || Runtime::isNumberType(ArgTy))
    return CE;

  Expr *Result = nullptr;

  // If argument is any floating type or boxed float return true
  // to preserve potential side effects we need to use chain expression

  // NOTE: should we check boxed<float> as well? 
  // It probably should not appear in valid code
  if (ArgTy->isFloatingType() ||
      Semantics.getRuntime().isBoxedType(Semantics.getASTContext().DoubleTy)) {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_true).get();
  } else {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
  }

  assert(Result);
  return E.replaceBy(Arg, Result);
}

Expr *reduceIsBool(Reducer &E, const BuiltinInfo& FInfo,
                    CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy))
    return CE;

  Expr *Result = nullptr;

  // If argument is any boolean type or boxed boolean return true
  // to preserve potential side effects we need to use chain expression
  if (Runtime::isBooleanType(ArgTy)) {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_true).get();
  } else {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
  }

  assert(Result);
  return E.replaceBy(Arg, Result);
}

Expr *reduceIsArray(Reducer &E, const BuiltinInfo& FInfo,
                   CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy))
    return CE;

  Expr *Result = nullptr;

  // If argument is array type return true
  // to preserve potential side effects we need to use chain expression
  if (Runtime::isArrayType(ArgTy)) {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_true).get();
  } else {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
  }

  assert(Result);
  return E.replaceBy(Arg, Result);
}

Expr *reduceIsString(Reducer &E, const BuiltinInfo& FInfo,
                    CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy))
    return CE;

  Expr *Result = nullptr;

  // If argument is string or boxed string type return true
  // to preserve potential side effects we need to use chain expression
  if (Runtime::isStringType(ArgTy)) {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_true).get();
  } else {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
  }

  assert(Result);
  return E.replaceBy(Arg, Result);
}

Expr *reduceIsNumeric(Reducer &E, const BuiltinInfo& FInfo,
                     CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy))
    return CE;

  Expr *Result = nullptr;

  // If argument is string or boxed string type return true
  // to preserve potential side effects we need to use chain expression
  if (Runtime::isNumberType(ArgTy)) {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_true).get();
  } else {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
  }

  assert(Result);
  return E.replaceBy(Arg, Result);
}

Expr *reduceIsScalar(Reducer &E, const BuiltinInfo& FInfo,
                      CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy))
    return CE;

  Expr *Result = nullptr;

  // If argument is string or boxed string type return true
  // to preserve potential side effects we need to use chain expression
  if (Runtime::isNumberType(ArgTy) || 
      Runtime::isBooleanType(ArgTy) ||
      Runtime::isStringType(ArgTy)) {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_true).get();
  } else {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
  }

  assert(Result);
  return E.replaceBy(Arg, Result);
}

Expr *reduceIsObject(Reducer &E, const BuiltinInfo& FInfo,
                    CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy))
    return CE;

  Expr *Result = nullptr;

  // If argument is object type return true
  // to preserve potential side effects we need to use chain expression
  if (Runtime::isObjectType(ArgTy)) {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_true).get();
  } else {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
  }

  assert(Result);
  return E.replaceBy(Arg, Result);
}

Expr *reduceIsNull(Reducer &E, const BuiltinInfo& FInfo,
                     CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy))
    return CE;

  Expr *Result = nullptr;

  // If argument is void return true
  // to preserve potential side effects we need to use chain expression
  if (ArgTy->isVoidType()) {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_true).get();
  } else {
    Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
  }

  assert(Result);
  return E.replaceBy(Arg, Result);
}

Expr *reduceIsResource(Reducer &E, const BuiltinInfo& FInfo,
                   CallExpr* CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  Sema& Semantics = E.getSema();

  // TODO: implement runtime is_resource() and corresponding legalization
  // If argument is universal type this function can only be resolved at runtime
  // if (Runtime::isUniversalType(ArgTy))
  //  return CE;
  //

  Expr *Result = Semantics.ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();

  assert(Result);
  return E.replaceBy(Arg, Result);
}


Expr *reduceGetType(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  SourceLocation Loc = CE->getLocStart();
  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  Sema& Semantics = E.getSema();

  // If argument is universal type this function can only be resolved at runtime
  if (Runtime::isUniversalType(ArgTy) || ArgTy == Runtime::getNumberBoxType())
    return CE;

  Expr *Result = Semantics.BuildStringLiteral("unknown type", Loc);
  if (Runtime::isBooleanType(ArgTy))
    Result = Semantics.BuildStringLiteral("boolean", Loc);
  else if(Runtime::isIntegerType(ArgTy))
    Result = Semantics.BuildStringLiteral("integer", Loc);
  else if(Runtime::isFloatingType(ArgTy))
    // double is returned for historical reasons
    Result = Semantics.BuildStringLiteral("double", Loc);
  else if(Runtime::isStringType(ArgTy))
    Result = Semantics.BuildStringLiteral("string", Loc);
  else if(Runtime::isArrayType(ArgTy))
    Result = Semantics.BuildStringLiteral("array", Loc);
  else if(ArgTy->isVoidType())
    Result = Semantics.BuildStringLiteral("NULL", Loc);
  else if(Runtime::isObjectType(ArgTy))
    Result = Semantics.BuildStringLiteral("object", Loc);
  else if(Runtime::isResourceType(ArgTy))
    Result = Semantics.BuildStringLiteral("resource", Loc);

  assert(Result);
  return E.replaceBy(Arg, Result);
}


Expr *reduceSetType(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  assert(CE->getNumArgs() == 2);
  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);
  Expr *TypeName = cast<Expr>(E.Visit(CE->getArg(1)));
  CE->setArg(1, TypeName);

  // Check for invalid conversion type.
  bool TypeIsValid = true;
  QualType TargetType = TypeName->getType();

  if (StringLiteral *ConstantKey = dyn_cast<StringLiteral>(TypeName)) {
    TypeIsValid = StringSwitch<bool>(ConstantKey->getBytes().lower())
      .Cases("bool", "boolean", true)
      .Cases("integer", "int", true)
      .Cases("float", "double", true)
      .Case ("string", true)
      .Case ("array", true)
      .Case ("object", true)
      .Case ("null", true)
      .Default(false);
    if (!TypeIsValid) {
      E.getSema().Diag(TypeName->getLocStart(),
                       diag::warn_php_invalid_settype_type);
    }
  } else if (Runtime::isObjectType(TargetType)) {
    // Object may have conversion method.
    //TODO: does it really has?
    TypeIsValid = true;
  } else if (!Runtime::isUniversalType(TargetType)) {
    //TODO: warning
    E.getSema().Diag(TypeName->getLocStart(), diag::err_php_incompatible_types)
      << E.getSema().getPHPTypeName(TypeName->getType()) << "string";
    TypeIsValid = false;
  }

  if (!TypeIsValid) {
    // Invalid type in settype call. In this case we need to replace expression
    // with false (preserving any side effects).
    Expr *False = E.getSema().ActOnCXXBoolLiteral(
      CE->getLocStart(), tok::kw_false).get();
    return E.replaceBy(Arg, False);
  }
  return CE;
}


Expr *reduceEmpty(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  Sema& S = E.getSema();
  Evaluator EV(S);
  if (ConstValue *V = EV.evaluate(CE))
    return S.getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  if (ArgTy->isIntegerType() || ArgTy->isFloatingType()) {
    return S.ActOnBinOp(nullptr, CE->getLocStart(), tok::exclaimequal,
                  Arg, S.ActOnIntegerConstant(CE->getLocStart(),0).get()).get();
  }

  return CE;
}


Expr *reduceIntVal(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  Sema& S = E.getSema();
  Evaluator EV(S);
  if (ConstValue *V = EV.evaluate(CE))
    return S.getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);
  Expr *Base = nullptr;

  if (CE->getNumArgs() == 2) {
    Base = cast<Expr>(E.Visit(CE->getArg(1)));
    CE->setArg(1, Base);
  }

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  // Integer type does not need to be converted
  // We return it as is and preserve possible side effects of
  // base expr.
  if (Runtime::isIntegerType(ArgTy)) {
    if (Base) {
      E.getSema().Diag(Base->getExprLoc(), diag::warn_php_intval_base_unused);
      return E.replaceBy(Base, Arg);
    } else {
      return Arg;
    }
  }

  if (ArgTy->isFloatingType()) {
    ASTContext& CT = E.getSema().getASTContext();
    ImplicitCastExpr *Cast = ImplicitCastExpr::Create(CT,
                                                      Runtime::getIntValueType(), 
                                                      CK_FloatingToIntegral,
                                                      Arg, nullptr, VK_RValue);
    if (Base) {
      E.getSema().Diag(Base->getExprLoc(), diag::warn_php_intval_base_unused);
      return E.replaceBy(Base, Cast);
    }
    else {
      return Cast;
    }
  }

  return CE;
}

Expr *reduceFloatVal(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  Sema& S = E.getSema();
  ASTContext& CT = S.getASTContext();
  Evaluator EV(S);
  if (ConstValue *V = EV.evaluate(CE))
    return S.getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  // Float type does not need to be converted
  if (Runtime::isFloatingType(ArgTy)) 
    return Arg;

  if (ArgTy->isIntegerType()) {
    ImplicitCastExpr *Cast = ImplicitCastExpr::Create(CT,
                               CT.DoubleTy, CK_IntegralToFloating,
                               Arg, nullptr, VK_RValue);
    return Cast;
  }

  Preprocessor& PP = S.getPreprocessor();
  return S.ActOnCastExpr(CE->getLocStart(), PP.getIdentifierInfo("float"), CE->getLocEnd(),
                         CE->getArg(0)).get();
}

Expr *reduceBoolVal(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  Sema& S = E.getSema();
  ASTContext& CT = S.getASTContext();
  Evaluator EV(S);
  if (ConstValue *V = EV.evaluate(CE))
    return S.getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  // Integer type does not need to be converted
  // We return it as is and preserve possible side effects of
  // base expr.
  if (Runtime::isBooleanType(ArgTy))
    return Arg;

  if (ArgTy->isIntegerType()) {
    ImplicitCastExpr *Cast = ImplicitCastExpr::Create(CT,
                                                      CT.BoolTy, CK_IntegralToBoolean,
                                                      Arg, nullptr, VK_RValue);
    return Cast;
  }

  if (ArgTy->isFloatingType()) {
    ImplicitCastExpr *Cast = ImplicitCastExpr::Create(CT,
                                                      CT.BoolTy, CK_FloatingToBoolean,
                                                      Arg, nullptr, VK_RValue);
    return Cast;
  }

  const Preprocessor& PP = S.getPreprocessor();
  return S.ActOnCastExpr(CE->getLocStart(), PP.getIdentifierInfo("bool"), CE->getLocEnd(),
                         CE->getArg(0)).get();
}

Expr *reduceStrVal(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  Sema& S = E.getSema();
  Evaluator EV(S);
  if (ConstValue *V = EV.evaluate(CE))
    return S.getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgTy = Arg->getType().getNonReferenceType().getLocalUnqualifiedType();

  // String type does not need to be converted
  if (Runtime::isStringType(ArgTy))
    return Arg;

  if (Runtime::isArrayType(ArgTy)) {
    Expr* Result = E.getSema().BuildStringLiteral("Array", Arg->getLocStart());
    assert(Result);
    return E.replaceBy(Arg, Result);
  }

  Preprocessor& PP = S.getPreprocessor();
  return S.ActOnCastExpr(CE->getLocStart(), PP.getIdentifierInfo("string"), CE->getLocEnd(),
                         CE->getArg(0)).get();
}

Expr *reduceRound(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  assert(CE->getNumArgs() >= 1 && CE->getNumArgs() <= 3);

  if (ConstValue *V = Evaluator(E.getSema()).evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  Expr *Value = cast<Expr>(E.Visit(CE->getArg(0)));
  Expr *Precision = CE->getNumArgs() >= 2 ? CE->getArg(1) : nullptr;
  Expr *Mode = CE->getNumArgs() == 3 ? CE->getArg(2) : nullptr;

  CE->setArg(0, Value);
  if (Precision) {
    Precision = cast<Expr>(E.Visit(Precision));
    CE->setArg(1, Precision);
  }
  if (Mode) {
    Mode = cast<Expr>(E.Visit(Mode));
    CE->setArg(2, Mode);
  }

  Sema &S = E.getSema();
  if (Value->getType()->isVoidType()) {
    S.Diag(Value->getExprLoc(), diag::warn_php_implicit_null_cvt)
      << "zero" << Value->getSourceRange();

    ConstValue *CV = S.getConstants().get(APFloat(0.0));
    Expr *Result = S.getConstantExpr(CV, Value->getExprLoc(), true);

    Expr *SideEffects =
      Eliminator(E.getTransformer()).eliminateUnused(Value, true);
    assert(SideEffects && "evaluator failed to handle constant expression");
    return S.packSideEffect(Value->getExprLoc(), SideEffects, Result);
  }

  ConstValue *CPrecision = nullptr;
  if (Precision) {
    Evaluator EV(S);
    CPrecision = EV.evaluate(Precision);
    if (CPrecision) {
      APInt PVal = EV.convertToInt(CPrecision, Precision->getExprLoc());
      CPrecision = S.getConstants().get(PVal);
      Precision = S.getConstantExpr(CPrecision, Precision->getExprLoc(), true);
      CE->setArg(1, Precision);
    }
  }

  ConstValue *CMode = nullptr;
  if (Mode) {
    Evaluator EV(S);
    CMode = EV.evaluate(Mode);
    if (CMode) {
      APInt PVal = EV.convertToInt(CMode, Mode->getExprLoc());
      if (PVal.sgt((long)RoundingMode::HalfOdd) || PVal.slt((long)RoundingMode::HalfUp)) {
        S.Diag(Mode->getExprLoc(), diag::warn_php_invalid_rounding_mode)
          << Mode->getSourceRange();
        PVal = (long)RoundingMode::HalfUp;
      }
      CMode = S.getConstants().get(PVal);
      Mode = S.getConstantExpr(CMode, Mode->getExprLoc(), true);
      CE->setArg(2, Mode);
    }
  }

  if (Value->getType()->isIntegerType()) {
    if (!Precision) {
      S.Diag(Value->getExprLoc(), diag::warn_php_already_integer_argument)
        << Value->getSourceRange();
      return S.ImpCastExprToType(Value, Runtime::getDoubleType(), 
                                 CK_IntegralToFloating).get();
    }
    if (CPrecision) {
      APInt PVal = CPrecision->getInt();
      if (PVal.isNonNegative()) {
        S.Diag(Value->getExprLoc(), diag::warn_php_already_integer_argument)
          << Value->getSourceRange();
        Expr *Result = S.ImpCastExprToType(Value, Runtime::getDoubleType(),
                                           CK_IntegralToFloating).get();
        if (!Mode || CMode)
          return Result;
        Expr *SideEffects =
          Eliminator(E.getTransformer()).eliminateUnused(Mode, true);
        return S.packSideEffect(Value->getExprLoc(), SideEffects, Result);
      }
    }
  }
  return CE;
}


Expr *reduceRounding(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  assert(CE->getNumArgs() == 1);

  Sema &S = E.getSema();
  Expr *Arg = cast<Expr>(E.Visit(CE->getArg(0)));
  CE->setArg(0, Arg);

  QualType ArgType = Arg->getType();
  if (ArgType->isVoidType()) {
    S.Diag(Arg->getExprLoc(), diag::warn_php_implicit_null_cvt)
      << "zero" << Arg->getSourceRange();

    ConstValue *CV = S.getConstants().get(APFloat(0.0));
    Expr *Result = S.getConstantExpr(CV, Arg->getExprLoc(), true);

    Expr *SideEffects =
      Eliminator(E.getTransformer()).eliminateUnused(Arg, true);
    assert(SideEffects && "evaluator failed to handle constant expression");
    return S.packSideEffect(Arg->getExprLoc(), SideEffects, Result);
  }
  if (ArgType->isIntegerType()) {
    S.Diag(Arg->getExprLoc(), diag::warn_php_already_integer_argument) 
      << Arg->getSourceRange();
    return S.ImpCastExprToType(Arg, Runtime::getDoubleType(),
                               CK_IntegralToFloating).get();
  }
  if (Runtime::isStringType(ArgType)) {
    S.Diag(Arg->getExprLoc(), diag::warn_php_implicit_type_conversion)
      << S.getPHPTypeName(ArgType)
      << S.getPHPTypeName(Runtime::getDoubleType())
      << Arg->getSourceRange();
    Arg = E.getSema().ActOnCastExpr(SourceLocation(), 
      E.getSema().getPreprocessor().getIdentifierInfo("double"),
      SourceLocation(), Arg).get();
    CE->setArg(0, Arg);
    return CE;
  }
  if (Runtime::isArrayType(ArgType)) {
    S.Diag(Arg->getExprLoc(), diag::err_php_invalid_argument_type)
      << S.getPHPTypeName(Runtime::getDoubleType())
      << S.getPHPTypeName(ArgType)
      << Arg->getSourceRange();
    ConstValue *Result = S.getConstants().getFalse();
    return S.getConstantExpr(Result, Arg->getExprLoc(), true);
  }
  return CE;
}


Expr *reduceFmod(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  if (EV.seenError())
    return nullptr;

  Expr *ArgX = cast<Expr>(E.Visit(CE->getArg(0)));
  Expr *ArgY = cast<Expr>(E.Visit(CE->getArg(1)));

  CE->setArg(0, ArgX);
  CE->setArg(1, ArgY);

  if (ConstValue *X = EV.evaluate(ArgX)) {
    APFloat Val = EV.convertToFloat(X, ArgX->getExprLoc());
    if (Val.isZero()) {
      Sema &S = E.getSema();
      Expr *SideEffects =
        Eliminator(E.getTransformer()).eliminateUnused(ArgY, true);
      X = S.getConstants().get(Val);
      Expr *Result = S.getConstantExpr(X, CE->getExprLoc(), true);
      return E.getSema().packSideEffect(CE->getExprLoc(),
        SideEffects, Result);
    }
  }
  if (ConstValue *Y = EV.evaluate(ArgY)) {
    APFloat Val = EV.convertToFloat(Y, ArgY->getExprLoc());
    Sema &S = E.getSema();
    if (Val.isZero()) {
      S.Diag(ArgY->getExprLoc(), diag::err_php_division_by_zero)
        << ArgY->getSourceRange();
      return nullptr;
    }
    APFloat One(1.0);
    if (Val.compare(One) == APFloat::cmpEqual) {
      Expr *SideEffects =
        Eliminator(E.getTransformer()).eliminateUnused(ArgX, true);
      ConstValue *ResultVal = S.getConstants().get(APFloat(0.0));
      Expr *Result = S.getConstantExpr(ResultVal, CE->getExprLoc(), true);
      return E.getSema().packSideEffect(CE->getExprLoc(),
        SideEffects, Result);
    }
  }
  return CE;
}


Expr *reduceIntdiv(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE) {
  Evaluator EV(E.getSema());
  if (ConstValue *V = EV.evaluate(CE))
    return E.getSema().getConstantExpr(V, CE->getExprLoc(), true);

  if (EV.seenError())
    return nullptr;

  Expr *ArgX = cast<Expr>(E.Visit(CE->getArg(0)));
  Expr *ArgY = cast<Expr>(E.Visit(CE->getArg(1)));

  CE->setArg(0, ArgX);
  CE->setArg(1, ArgY);

  if (ConstValue *X = EV.evaluate(ArgX)) {
    APInt Val = EV.convertToInt(X, ArgX->getExprLoc());
    if (Val == 0) {
      Sema &S = E.getSema();
      Expr *SideEffects =
        Eliminator(E.getTransformer()).eliminateUnused(ArgY, true);
      X = S.getConstants().get(Val);
      Expr *Result = S.getConstantExpr(X, CE->getExprLoc(), true);
      return E.getSema().packSideEffect(CE->getExprLoc(),
        SideEffects, Result);
    }
  }
  if (ConstValue *Y = EV.evaluate(ArgY)) {
    APInt Val = EV.convertToInt(Y, ArgY->getExprLoc());
    Sema &S = E.getSema();
    if (Val == 0) {
      S.Diag(ArgY->getExprLoc(), diag::err_php_division_by_zero)
        << ArgY->getSourceRange();
      return nullptr;
    }
    if (Val == 1)
      return ArgX;
  }
  return CE;
}

}
