//===--- ReduceUnary.cpp ---- AST Transformation Component ------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Implementation of class Reducer, unary nodes.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Transformer/Eliminator.h"
#include "phpfe/Transformer/Reducer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Expr *Reducer::processUnaryNode(UnaryOperator *UO) {
  Expr *Exp = UO->getSubExpr();
  assert(Exp);

  // Try evaluate at compile time.
  Evaluator EV(Semantics);
  if (ConstValue *V = EV.evaluate(UO))
    return Semantics.getConstantExpr(V, UO->getLocStart(), true);

  if (EV.seenError())
    return UO;

  // Process subexpr.
  Exp = cast<Expr>(Visit(Exp));
  UO->setSubExpr(Exp);
  return UO;
}


Expr *Reducer::processIncDecNode(UnaryOperator *UO) {
  Expr *Exp = UO->getSubExpr();
  assert(Exp);

  // Process subexpr.
  Exp = cast<Expr>(Visit(Exp));
  UO->setSubExpr(Exp);
  return UO;
}


Expr *Reducer::VisitUnaryMinus(UnaryOperator *UO) {
  return processUnaryNode(UO);
}


Expr *Reducer::VisitUnaryPlus(UnaryOperator *UO) {
  return processUnaryNode(UO);
}


Expr *Reducer::VisitUnaryLNot(UnaryOperator *UO) {
  return processUnaryNode(UO);
}


Expr *Reducer::VisitUnaryNot(UnaryOperator *UO) {
  return processUnaryNode(UO);
}


Expr *Reducer::VisitUnaryPostInc(UnaryOperator *UO) {
  return processIncDecNode(UO);
}


Expr *Reducer::VisitUnaryPreInc(UnaryOperator *UO) {
  return processIncDecNode(UO);
}


Expr *Reducer::VisitUnaryPostDec(UnaryOperator *UO) {
  return processIncDecNode(UO);
}


Expr *Reducer::VisitUnaryPreDec(UnaryOperator *UO) {
  return processIncDecNode(UO);
}

}
