//===--- LegalizeCall.cpp - AST Transformation Component --------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Transformation that makes AST valid from viewpoint of Clang Code Generator.
// These methods legalize call expressions.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/Sema/Initialization.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Transformer/Legalizer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Expr *Legalizer::VisitCallExpr(CallExpr *E) {
  Expr *CalleeExpr = E->getCallee();
  if (isa<StringLiteral>(CalleeExpr))
    return LegalizeUnresolvedCall(E);

  Decl *CalleeD = E->getCalleeDecl();
  if (FunctionDecl *FD = dyn_cast<FunctionDecl>(CalleeD)) {
    // The callee is a function.
    FD = FD->getMostRecentDecl();

    // If the function has a body (this is a PHP function defined in this file)
    // or has a prototype (this is C++ function or PHP function imported from
    // external source), adjust arguments. The call is already prepared.
    if (FD->hasBody() || FD->hasWrittenPrototype()) {
      SmallVector<Expr*, 4> CallArgs(E->arguments());
      LegalizeCallArguments(FD, CallArgs);
      return Semantics.createFunctionCall(FD, E->getLocStart(),
                           E->getLocStart(), CallArgs, E->getRParenLoc()).get();
    }

    // No function definition was seen. Create runtime call.

    //TODO: Make up runtime function name in the form '\NS\NS2\func'
    Expr *Name = Semantics.BuildStringLiteral(FD->getNameAsString(),
                                              FD->getLocation());
    Name = legalizeStringRefExpr(Name);

    return getRuntime().makeFunctionCall(Name, ArrayRef<Expr *>(E->getArgs(),
                                                              E->getNumArgs()));
  } else {
    llvm_unreachable("unsupported callee");
  }
  return nullptr;
}


Expr *Legalizer::LegalizeUnresolvedCall(CallExpr *E) {
  StringLiteral *NameLiteral = cast<StringLiteral>(E->getCallee());
  StringRef Name = NameLiteral->getString();

  if (const BuiltinInfo *BI = find_builtin_function(Name)) {
    // Insert default arguments
    unsigned NumArgsWithDefault = BI->getNumRequiredArgs() + BI->getNumDefaultArgs();
    if (E->getNumArgs() < NumArgsWithDefault) {
      unsigned OldNumArgs = E->getNumArgs();
      E->setNumArgs(Semantics.getASTContext(), NumArgsWithDefault);
      for (unsigned I = OldNumArgs; I < NumArgsWithDefault; ++I) {
        auto Param = BI->Params[I];
        if (Param.A.isVariadic() || Param.A.isOptional())
          break;
        if (Param.Def.isSpecified())
          E->setArg(I,Param.Def.buildDefaultExpr(Semantics, E->getLocEnd()));
      }
    }

    if (BI->Handlers.LegalizerH) {
      if (Expr *Res = BI->Handlers.LegalizerH(*this, *BI, E))
        return Res;
    }

    // Resolve function name
    unsigned ResolvedNameIdx = 0;
    unsigned Idx = std::min(E->getNumArgs(), (unsigned)BI->Params.size());
    while (Idx--) {
      if (BI->Params[Idx].A.isOptional())
        ++ResolvedNameIdx;
    }
    StringRef ResolvedName = BI->RuntimeNames.get(ResolvedNameIdx);

    SmallVector<Expr*, 4> CallArgs(E->arguments());
    FunctionDecl *FD = Semantics.getRuntime().findRuntimeFunction(ResolvedName);
    assert(FD);
    LegalizeCallArguments(FD, CallArgs);

    SourceLocation Loc = CallArgs.empty()
      ? E->getExprLoc()
      : CallArgs[0]->getExprLoc();
    return Semantics.createFunctionCall(FD, E->getExprLoc(), Loc,
                                        CallArgs, E->getRParenLoc()).get();
  }

  llvm_unreachable("Unknown unresolved call");
}


Expr *Legalizer::LegalizeCast(QualType ParamTy, Expr *Val) {
  if (ParamTy == Val->getType())
    return Val;
  InitializedEntity Entity =
    InitializedEntity::InitializeResult(Val->getExprLoc(), ParamTy, false);
  return Semantics.PerformMoveOrCopyInitialization(Entity,
                                 /*NRVOCandidate=*/nullptr, ParamTy, Val).get();
}


Expr *Legalizer::LegalizeNonReferenceArgument(QualType ParamTy, Expr *Arg) {
  Runtime &RT = Semantics.getRuntime();
  assert(Runtime::isUniversalType(Arg->getType())
         || Runtime::isBoxedType(Arg->getType()));
  assert(!ParamTy->isLValueReferenceType());
  assert(!RT.isBoxedType(ParamTy));
  SmallVector<Expr*, 1> Args;
  Args.push_back(RT.castToClassBox(Arg));
  if (Semantics.isStrictTypes()) {
    if (ParamTy->isBooleanType())
      return RT.makeRuntimeFunctionCall("checked_get_bool_strict", Args);
    if (ParamTy->isIntegerType())
      return RT.makeRuntimeFunctionCall("checked_get_integer_strict", Args);
    if (ParamTy->isFloatingType())
      return RT.makeRuntimeFunctionCall("checked_get_double_strict", Args);
    if (Runtime::isStringType(ParamTy)) {
      Expr *Val = RT.makeRuntimeFunctionCall("checked_get_string_strict", Args);
      return LegalizeCast(ParamTy, Val);
    }
  } else {
    if (ParamTy->isBooleanType())
      return RT.makeRuntimeFunctionCall("checked_get_bool_weak", Args);
    if (ParamTy->isIntegerType())
      return RT.makeRuntimeFunctionCall("checked_get_integer_weak", Args);
    if (ParamTy->isFloatingType())
      return RT.makeRuntimeFunctionCall("checked_get_double_weak", Args);
    if (Runtime::isStringType(ParamTy)) {
      Expr *Val = RT.makeRuntimeFunctionCall("checked_get_string_weak", Args);
      return LegalizeCast(ParamTy, Val);
    }
  }
  if (Runtime::isArrayType(ParamTy))
    return RT.makeRuntimeFunctionCall("checked_get_array", Args);
  llvm_unreachable("unknown argument type");
}


Expr *Legalizer::LegalizeAccessArgument(QualType ParamTy, Expr *Arg) {
  Runtime &RT = Semantics.getRuntime();
  if (Runtime::isUniversalType(ParamTy)) {
    Expr *Val = Arg->isLValue() ? Arg : Semantics.castToLValueRef(Arg);
    return RT.castToClassBox(Val);
  }

  if (RT.isBoxedType(Arg->getType()))
    Arg = Semantics.castToLValueRef(Arg);

  SmallVector<Expr*, 1> Args;
  Args.push_back(RT.castToClassBox(Arg));

  if (Semantics.isStrictTypes()) {
    if (ParamTy->isBooleanType())
      return RT.makeRuntimeFunctionCall("checked_access_bool_strict", Args);
    if (ParamTy->isIntegerType())
      return RT.makeRuntimeFunctionCall("checked_access_integer_strict", Args);
    if (ParamTy->isFloatingType())
      return RT.makeRuntimeFunctionCall("checked_access_double_strict", Args);
    if (Runtime::isStringType(ParamTy))
      return RT.makeRuntimeFunctionCall("checked_access_string_strict", Args);
  } else {
    if (ParamTy->isBooleanType())
      return RT.makeRuntimeFunctionCall("checked_access_bool_weak", Args);
    if (ParamTy->isIntegerType())
      return RT.makeRuntimeFunctionCall("checked_access_integer_weak", Args);
    if (ParamTy->isFloatingType())
      return RT.makeRuntimeFunctionCall("checked_access_double_weak", Args);
    if (Runtime::isStringType(ParamTy))
      return RT.makeRuntimeFunctionCall("checked_access_string_weak", Args);
  }

  if (Runtime::isArrayType(ParamTy)) 
    return RT.makeRuntimeFunctionCall("checked_access_array", Args);

  llvm_unreachable("unsupported boxed type legalization");
}


// Legalize reference argument that can be a C++ reference when 
// function accepts reference e.g.:
//    function f(int &$a) {}     =>    Box f(int &a) {}
//
// or it can be a pointer when dealing with references in variadic part of a 
// function e.g:
//    function f(int &...$a) {}  =>    Box f(ArgPack<int*> a) {}
//
Expr *Legalizer::LegalizeReferenceArgument(QualType ParamTy, Expr *Arg) {
  assert(ParamTy->isLValueReferenceType() || ParamTy->isPointerType() ||
         RuntimeTypes::isRefBoxType(ParamTy));
  assert(Runtime::isUniversalType(Arg->getType())
         || Runtime::isBoxedType(Arg->getType()));
  assert(Arg->isLValue() ||
         Semantics.getRuntime().isBoxedType(Arg->getType()));

  QualType BaseType;
  if (RuntimeTypes::isRefBoxType(ParamTy))
    BaseType = RuntimeTypes::getParameterOfSpecialization(ParamTy);
  else if (ParamTy->isLValueReferenceType())
    BaseType = ParamTy->getAs<ReferenceType>()->getPointeeType();
  else
    BaseType = ParamTy->getAs<PointerType>()->getPointeeType();

  Expr *ValRef = LegalizeAccessArgument(BaseType, Arg);
  if (ParamTy->isLValueReferenceType() || ValRef->getType()->isPointerType())
    return ValRef;

  assert(ValRef->isLValue());
  return Semantics.ActOnUnaryOp(
    nullptr, Arg->getExprLoc(), tok::amp, ValRef).get();
}


Expr *Legalizer::LegalizeReturnExpr(FunctionDecl *FDecl, Expr *Arg) {
  assert(FDecl);
  QualType RetTy = FDecl->getReturnType();
  QualType ExprTy = Arg->getType();
  assert(!ExprTy->isVoidType());
  assert(!RetTy->isVoidType());

  if (RetTy == ExprTy)
    return Arg;

  if (RuntimeTypes::isRefBoxType(RetTy))
    return LegalizeReturnReference(RetTy, Arg);
  else
    return LegalizeReturnValue(RetTy, Arg);
}


/// \brief Prepares expression as a result of a function.
///
Expr *Legalizer::LegalizeReturnReference(QualType ReturnType, Expr *Arg) {
  assert(isRefBoxType(ReturnType));
  QualType ExprType = Arg->getType();
  assert(areTypesCompatible(ReturnType, ExprType));
  QualType BaseReturnType = getParameterOfSpecialization(ReturnType);

  // If return expression is also a boxed type, we can cast the expression to
  // the target type, all boxed types have the same layout.

  if (isRefBoxType(ExprType)) {
    // Possible cases:
    //
    //   box& <- box&
    //   box& <- int&
    //   int& <- box&
    //   int& <- int&
    //
    // They are possible if return statement returns:
    // - result of another function, which also returns a reference,
    // - value of a parameter, which is a reference,
    // - value of a local, which is deduced to be a reference.
    // Types must have been checked before.
    return Arg;
  }

  if (isBoxedType(ExprType)) {
    if (isUniversalType(BaseReturnType))
      // Function returns untyped reference. Possible cases:
      //
      //   box& <- box,
      //   box& <- NumberBox,
      //   box& <- Boxed<int>,
      //   box& <- Boxed<box>
      //
      // In all these cases we can just cast the expression to return type.
      return castBetweenBoxed(ReturnType, Arg);

    // Function returns typed reference. If return expression is also typed,
    // the types must be identical, this condition must be provided by parser.
    // In this case return expression can be just casted to reference box.
    QualType BaseExprType = getParameterOfSpecialization(ReturnType);
    if (!BaseExprType.isNull())
      return castBetweenBoxed(ReturnType, Arg);

    // Expression is of universal type. Must check in runtime if the value is
    // of right type.
    Runtime &RT = Semantics.getRuntime();
    SmallVector<Expr *, 1> Args;
    Args.push_back(Arg);
    if (Semantics.isStrictTypes()) {
      if (ReturnType->isBooleanType())
        return RT.makeRuntimeFunctionCall("checked_get_bool_ref_strict", Args);
      if (ReturnType->isIntegerType())
        return RT.makeRuntimeFunctionCall("checked_get_integer_ref_strict", Args);
      if (ReturnType->isFloatingType())
        return RT.makeRuntimeFunctionCall("checked_get_double_ref_strict", Args);
      if (Runtime::isStringType(ReturnType))
        return RT.makeRuntimeFunctionCall("checked_get_string_ref_strict", Args);
    } else {
      if (ReturnType->isBooleanType())
        return RT.makeRuntimeFunctionCall("checked_get_bool_ref_weak", Args);
      if (ReturnType->isIntegerType())
        return RT.makeRuntimeFunctionCall("checked_get_integer_ref_weak", Args);
      if (ReturnType->isFloatingType())
        return RT.makeRuntimeFunctionCall("checked_get_double_ref_weak", Args);
      if (Runtime::isStringType(ReturnType))
        return RT.makeRuntimeFunctionCall("checked_get_string_ref_weak", Args);
    }
    llvm_unreachable("unsupported boxed type legalization");
  }

  // Get here when return expression is not a box. This is possible only if a
  // value is returned as a reference.
  Arg = LegalizeTypedExpr(getClassBoxType(), Arg);
  return castBetweenBoxed(ReturnType, Arg);
}


Expr *Legalizer::LegalizeReturnValue(QualType ReturnType, Expr *Arg) {
  return LegalizeTypedExpr(ReturnType, Arg);
}


/// \brief Checks if the specified parameter type represents a parameter that is
/// passed as a reference.
///
bool Legalizer::isRefArgument(QualType ParamType) {
  // Parameter that is passed by reference is always represented as C++
  // reference type.
  if (auto RefType = ParamType->getAs<LValueReferenceType>()) {
    QualType PointeeType = RefType->getPointeeType();

    // If reference is constant, it cannot be modified, such parameters are
    // passed by value.
    if (PointeeType.isConstQualified())
      return false;

    // Primitive types.
    if (PointeeType->isBooleanType())
      return true;
    if (PointeeType->isIntegerType())
      return true;
    if (PointeeType->isFloatingType())
      return true;

    // Complex types.
    if (PointeeType == getStringBoxType())
      return true;
    if (PointeeType == getArrayBoxType())
      return true;

    // Untyped value.
    if (isUniversalType(PointeeType))
      return true;

    // Boxed types are used for primitive values and objects.
    if (isBoxedType(PointeeType)) {
      //TODO: check if primitive or object.
      return true;
    }

    // This type is invalid for using by reference. Issue error as it may be
    // caused by user error if he declared invalid C++ function.
    //TODO:
    llvm_unreachable("Invalid reference type");
    return false;
  }

  return false;
}


Expr *Legalizer::LegalizeArgument(ParmVarDecl *Param, Expr *Val) {
  QualType ParamTy = Param->getType();
  Expr *Arg = (Val == nullptr) ? Param->getDefaultArg() : Val;
  assert(Arg && "No default value for argument");

  if (isRefArgument(ParamTy))
    return legalizeRefArgument(ParamTy, Arg);
  else
    return LegalizeTypedExpr(ParamTy, Arg);
}


Expr *Legalizer::legalizeRefArgument(QualType ParamType, Expr *Arg) {
  assert(!ParamType.isNull());
  assert(Arg);

  Expr *Result = cast<Expr>(Visit(Arg));
  if (const ReferenceType *RefTy = ParamType->getAs<ReferenceType>()) {
    ParamType = RefTy->getPointeeType();

    // f(bool &x)
    if (ParamType->isBooleanType())
      return LegalizeAccessArgument(Context.BoolTy, Result);

    // f(int &x)
    if (ParamType->isIntegerType())
      return LegalizeAccessArgument(getIntValueType(), Result);
    //TODO: check other int types.

    // f(float &x)
    if (ParamType->isFloatingType())
      return LegalizeAccessArgument(Context.DoubleTy, Result);
    //TODO: check other float types.

    // f(StringBox &x)
    if (ParamType == getStringBoxType())
      return LegalizeAccessArgument(getStringBoxType(), Result);

    // f(ArrayBox &x)
    if (ParamType == getArrayBoxType())
      return LegalizeAccessArgument(getArrayBoxType(), Result);

    //TODO: IArray
    //TODO: IObject
  }
  llvm_unreachable("Invalid reference type");
  return nullptr;
}


/// \brief Given an expression return another expression that has the specified
/// type.
///
Expr *Legalizer::LegalizeTypedExpr(QualType ParamType, Expr *Arg) {
  assert(!ParamType.isNull());
  assert(Arg);

  // f(bool x)
  if (ParamType->isBooleanType())
    return legalizeBooleanExpr(Arg);

  // f(int x)
  if (ParamType->isIntegerType())
    return legalizeIntegerExpr(getIntValueType(), Arg);

  // f(double x)
  if (ParamType->isFloatingType())
    return legalizeFloatingExpr(Context.DoubleTy, Arg);

  // f(string_ref x)
  if (ParamType == getStringRefType())
    return legalizeStringRefExpr(Arg);

  // f(StringBox x)
  if (ParamType == getStringBoxType())
    return legalizeStringBoxExpr(Arg);

  // f(ArrayBox x)
  if (ParamType == getArrayBoxType())
    return legalizeArrayBoxExpr(Arg);

  // f(Box x), f(const Box &x)
  if (isUniversalType(ParamType))
    return legalizeBoxExpr(Arg, isa<ReferenceType>(ParamType));

  //TODO: add argument legalization
  llvm_unreachable("unsupported expression type");
  return Arg;
}


Expr *Legalizer::legalizeBooleanExpr(Expr *Arg) {
  Expr *Result = cast<Expr>(Visit(Arg));
  QualType ArgType = Result->getType()->getCanonicalTypeUnqualified();
  if (const ReferenceType *RefTy = ArgType->getAs<ReferenceType>())
    ArgType = RefTy->getPointeeType();

  // No conversion required.
  if (QualType(ArgType) == Context.BoolTy)
    return Result;

  // bool <- int
  if (ArgType->isIntegerType())
    return Semantics.ImpCastExprToType(Result, Context.BoolTy,
                                       CK_IntegralToBoolean).get();
  // bool <- float
  if (ArgType->isFloatingType())
    return Semantics.ImpCastExprToType(Result, Context.BoolTy,
                                       CK_FloatingToBoolean).get();
  // bool <- const char[N]
  if (auto *CArrayT = dyn_cast<ConstantArrayType>(ArgType.getTypePtr()))
    if (CArrayT->getArrayElementTypeNoTypeQual()->isCharType())
      return getRuntime().makeRuntimeFunctionCall("convert_cstring_to_bool",
                                                  MultiExprArg(Result));
  // bool <- string_ref
  if (ArgType == getStringRefType())
    return getRuntime().MakeRuntimeMethodCall("to_bool", getStringRefDecl(),
                                              Result, MultiExprArg());
  // bool <- IArray*
  // TODO: const? descendants?
  if (ArgType == getArrayPtrType())
    return getRuntime().MakeRuntimeMethodCall("not_empty", getArrayDecl(),
                                              Result, MultiExprArg());
  // bool <- IObject*, Descendant(IObject)*
  if (isObjectType(ArgType))
    return getRuntime().MakeRuntimeMethodCall("as_bool", getObjectDecl(),
                                              Result, MultiExprArg());

  // bool <- StringBox
  // bool <- ArrayBox
  // bool <- Box, box, Box&&
  // bool <- Boxed<T>
  // bool <- RefBox<T>
  //
  // All box representation types are handled in the same way.
  if (isBoxedType(ArgType))
    return getRuntime().MakeRuntimeMethodCall("to_bool", getPlainBoxDecl(),
                                              Result, MultiExprArg());

  // Void type may appear as a result of void function. Convert it into false
  // value. Also keep function call for side effects.
  if (ArgType->isVoidType()) {
    Expr *False = Semantics.ActOnCXXBoolLiteral(Result->getLocStart(),
                                                tok::kw_false).get();
    Result = Semantics.packSideEffect(Result->getLocStart(), Result, False);
    return Result;
  }

  llvm_unreachable("unexpected type");
  return Result;
}


Expr *Legalizer::legalizeIntegerExpr(QualType TargetType, Expr *Arg) {
  Expr *Result = cast<Expr>(Visit(Arg));
  QualType ArgType = Result->getType()->getCanonicalTypeUnqualified();
  if (const ReferenceType *RefTy = ArgType->getAs<ReferenceType>())
    ArgType = RefTy->getPointeeType();

  // Make complex conversion first, probably its result needs cast to proper
  // integer type.

  // int <- const char[N]
  if (auto *CArrayT = dyn_cast<ConstantArrayType>(ArgType.getTypePtr()))
    if (CArrayT->getArrayElementTypeNoTypeQual()->isCharType()) {
      Result = getRuntime().makeRuntimeFunctionCall("convert_cstring_to_integer",
                                                  MultiExprArg(Result));
      ArgType = Result->getType();
    }

  // int <- string_ref
  if (ArgType == getStringRefType()) {
    Result = getRuntime().makeRuntimeFunctionCall("convert_string_to_integer",
                                                MultiExprArg(Result));
    ArgType = Result->getType();
  }

  // int <- IObject*, Descendant(IObject)*
  if (isObjectType(ArgType)) {
    Result = getRuntime().MakeRuntimeMethodCall("as_int", getObjectDecl(),
                                              Result, MultiExprArg());
    ArgType = Result->getType();
  }

  if (ArgType == TargetType)
    return Result;

  // int <- bool, short, ...
  if (ArgType->isIntegerType())
    return Semantics.ImpCastExprToType(Result, TargetType,
                                       CK_IntegralCast).get();
  // int <- float
  if (ArgType->isFloatingType())
    return Semantics.ImpCastExprToType(Result, TargetType,
                                       CK_FloatingToIntegral).get();
  // int <- StringBox
  // int <- ArrayBox
  // int <- Box, box, Box&&
  // int <- Boxed<T>
  // int <- RefBox<T>
  //
  // All box representation types are handled in the same way.
  if (isBoxedType(ArgType))
    return getRuntime().MakeRuntimeMethodCall("to_integer", getPlainBoxDecl(),
                                              Result, MultiExprArg());

  // Void type may appear as a result of void function. Convert it into zero
  // value. Also keep function call for side effects.
  if (ArgType->isVoidType()) {
    Expr *Zero = Semantics.ActOnIntegerConstant(Result->getLocStart(),
                                                 0).get();
    Result = Semantics.packSideEffect(Result->getLocStart(), Result, Zero);
    return Result;
  }

  llvm_unreachable("unexpected type");
  return Result;
}


Expr *Legalizer::legalizeFloatingExpr(QualType TargetType, Expr *Arg) {
  Expr *Result = cast<Expr>(Visit(Arg));
  QualType ArgType = Result->getType()->getCanonicalTypeUnqualified();
  if (const ReferenceType *RefTy = ArgType->getAs<ReferenceType>())
    ArgType = RefTy->getPointeeType();

  // double <- const char[N]
  if (auto *CArrayT = dyn_cast<ConstantArrayType>(ArgType.getTypePtr()))
    if (CArrayT->getArrayElementTypeNoTypeQual()->isCharType()) {
      Result = getRuntime().makeRuntimeFunctionCall("convert_cstring_to_double",
                                                    MultiExprArg(Result));
      ArgType = Result->getType();
    }

  // int <- string_ref
  if (ArgType == getStringRefType()) {
    Result = getRuntime().makeRuntimeFunctionCall("convert_string_to_double",
                                               MultiExprArg(Result));
    ArgType = Result->getType();
  }

  // int <- IObject*, Descendant(IObject)*
  if (isObjectType(ArgType)) {
    Result = getRuntime().MakeRuntimeMethodCall("as_float", getObjectDecl(),
                                             Result, MultiExprArg());
    ArgType = Result->getType();
  }

  if (QualType(ArgType) == TargetType)
    return Result;

  // double <- bool, short, ...
  if (ArgType->isIntegerType())
    return Semantics.ImpCastExprToType(Result, getIntValueType(),
                                       CK_IntegralToFloating).get();
  // double <- float, long double
  if (ArgType->isFloatingType())
    return Semantics.ImpCastExprToType(Result, getIntValueType(),
                                       CK_FloatingCast).get();
  // int <- StringBox
  // int <- ArrayBox
  // int <- Box, box, Box&&
  // int <- Boxed<T>
  // int <- RefBox<T>
  //
  // All box representation types are handled in the same way.
  if (isBoxedType(ArgType))
    return getRuntime().MakeRuntimeMethodCall("to_double", getPlainBoxDecl(),
                                              Result, MultiExprArg());

  // Void type may appear as a result of void function. Convert it into zero
  // value. Also keep function call for side effects.
  if (ArgType->isVoidType()) {
    APFloat Val(0.0);
    Expr *Zero = FloatingLiteral::Create(Context, Val, true, Context.DoubleTy,
                                          Result->getLocStart());
    Result = Semantics.packSideEffect(Result->getLocStart(), Result, Zero);
    return Result;
  }

  llvm_unreachable("unexpected type");
  return Result;
}


/// \brief Return argument converted to string_ref.
///
Expr *Legalizer::legalizeStringRefExpr(Expr *Arg) {
  Expr *Result = cast<Expr>(Visit(Arg));
  QualType ArgType = Result->getType()->getCanonicalTypeUnqualified();
  if (const ReferenceType *RefTy = ArgType->getAs<ReferenceType>())
    ArgType = RefTy->getPointeeType().getLocalUnqualifiedType();

  if (ArgType == getStringRefType())
    return Result;

  // string_ref <- const char[N]
  if (auto *CArrayT = dyn_cast<ConstantArrayType>(ArgType.getTypePtr()))
    if (CArrayT->getArrayElementTypeNoTypeQual()->isCharType())
      return getRuntime().createStringRefFromCharArray(Arg);

  // string_ref <- bool
  if (ArgType->isBooleanType())
    return getRuntime().makeRuntimeFunctionCall("convert_bool_to_string",
                                                MultiExprArg(Result));
  // string_ref <- StringBox
  if (ArgType == getStringBoxType()) {
    return getRuntime().MakeRuntimeMethodCall("get_string",
                                     getPlainBoxDecl(), Result, MultiExprArg());
  }

  // string_ref <- String
  if (ArgType == getStringType()) {
    return getRuntime().MakeRuntimeMethodCall("get_string",
                                       getStringDecl(), Result, MultiExprArg());
  }

  // string_ref <- int
  // string_ref <- float
  // string_ref <- IObject
  //
  // All these transformations produce strings that cannot be represented by
  // small set of constants. In these cases first create Box, then convert it
  // into string representation. In this case it is possible to use inplace
  // strings, they can save memory. All results are temporary.
  if (ArgType->isIntegerType() || ArgType->isFloatingType() ||
      isObjectType(ArgType)) {
    Result = getRuntime().createExprBox(Result);
    ArgType = getClassBoxType();
  }

  // If this is boxed type, we can use to_string method to make the conversion.
  if (isBoxedType(ArgType)) {
    Expr *StringBox = getRuntime().MakeRuntimeMethodCall("to_string",
                                     getPlainBoxDecl(), Result, MultiExprArg());
    return getRuntime().MakeRuntimeMethodCall("get_string",
                                  getPlainBoxDecl(), StringBox, MultiExprArg());
  }

  llvm_unreachable("unexpected type");
  return Result;
}


Expr *Legalizer::legalizeStringBoxExpr(Expr *Arg) {
  Expr *Result = cast<Expr>(Visit(Arg));
  QualType ArgType = Result->getType()->getCanonicalTypeUnqualified();
  if (const ReferenceType *RefTy = ArgType->getAs<ReferenceType>())
    ArgType = RefTy->getPointeeType().getLocalUnqualifiedType();

  if (ArgType == getStringBoxType())
    return Result;

  return getRuntime().createStringBox(Result);
}


Expr *Legalizer::legalizeArrayBoxExpr(Expr *Arg) {
  Expr *Result = cast<Expr>(Visit(Arg));
  QualType ArgType = Result->getType()->getCanonicalTypeUnqualified();
  if (const ReferenceType *RefTy = ArgType->getAs<ReferenceType>())
    ArgType = RefTy->getPointeeType().getLocalUnqualifiedType();

  if (ArgType == getArrayBoxType())
    return Result;

  return getRuntime().createArrayBox(Result);

}


Expr *Legalizer::legalizeBoxExpr(Expr *Arg, bool isRef) {
  Expr *Result = cast<Expr>(Visit(Arg));
  QualType ArgType = Result->getType()->getCanonicalTypeUnqualified();
  if (const ReferenceType *RefTy = ArgType->getAs<ReferenceType>())
    ArgType = RefTy->getPointeeType().getLocalUnqualifiedType();

  // If the type is already box, a cast could be required.
  if (isBoxedType(ArgType) && isRef)
    return castBetweenBoxed(getClassBoxType(), Result);

  // For all other types use Box constructors.
  return getRuntime().createExprBox(Result);
}


void Legalizer::LegalizeCallArguments(FunctionDecl *FD,
                                      SmallVectorImpl<Expr*> &Args) {
  auto CurArg = Args.begin();

  bool SeenVariadic = false;
  for (auto CurParam : FD->parameters()) {
    assert(!SeenVariadic);
    if (Semantics.getRuntime().isArgPackType(CurParam->getType())) {
      SeenVariadic = true;
      continue;
    }
    bool ArgsEnded = (CurArg == Args.end());
    Expr *Arg = ArgsEnded ? nullptr : *CurArg;
    Expr *NewArg = LegalizeArgument(CurParam, Arg);
    assert(NewArg);
    if (ArgsEnded)
      Args.push_back(NewArg);
    else
      *CurArg = NewArg;
    ++CurArg;
  }

  if (SeenVariadic) {
    ParmVarDecl *VariadicParm = FD->parameters().back();
    QualType BaseType = 
      Runtime::unboxType(VariadicParm->getType());
    SmallVector<Expr *, 16> VariadicArgs;
    for (unsigned I = FD->getNumParams() - 1; I < Args.size(); ++I) {
      Expr *NewArg = LegalizeTypedExpr(BaseType, Args[I]);
      assert(NewArg);
      VariadicArgs.push_back(NewArg);
    }
    Expr *NewArg = Semantics.getRuntime().createVariadicArgument(
      VariadicParm->getLocation(), BaseType, VariadicArgs);
    assert(NewArg);
    if (CurArg != Args.end())
      *CurArg = NewArg;
    else
      Args.push_back(NewArg);
    assert(Args.size() >= FD->getNumParams());
    Args.resize(FD->getNumParams());
  } else {
    //TODO: handle extra args
  }
}


}