//===--- Transformer.cpp - AST Transformation Component ---------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// This file implements the actions class which performs semantic analysis and
// builds an AST out of a parse stream.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Transformer/Transformer.h"
#include "phpfe/Transformer/Reducer.h"
#include "phpfe/Transformer/Legalizer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Transformer::Transformer(TransMode M)
  : SemaPtr(nullptr), Mode(M) {
}


Transformer::~Transformer() {
}


bool Transformer::HandleTopLevelDecl(DeclGroupRef DG) {
  bool Failed = false;
  for (Decl *D : DG) {
    // If declaration originated from AST file there is no point in transforming it
    if (D->isFromASTFile())
      continue;
    Failed |= !Transform(D);
  }
  return !Failed;
}


void Transformer::HandleTranslationUnit(ASTContext &Ctx) {
  getSema().handlePostponedDeclarations();
}


void Transformer::InitializeSema(clang::Sema &S) {
  SemaPtr = &static_cast<Sema&>(S);
}


bool Transformer::Transform(Decl *D) {
  switch (Mode) {
  case TransMode::None: return true;
  case TransMode::AST: return TransformForAST(D);
  case TransMode::SyntaxOnly: return TransformForSyntax(D);
  case TransMode::CodeGen: return TransformForCodeGen(D);
  default:
    llvm_unreachable("wrong transformation mode");
  }
  return false;
}


bool Transformer::TransformForAST(Decl *D) {
  return ApplyReducer(D);
}


bool Transformer::TransformForSyntax(Decl *D) {
  return ApplyReducer(D);
}


bool Transformer::TransformForCodeGen(Decl *D) {
  return TransformForSyntax(D) && ApplyLegalizer(D);
}


// returns false on error
bool Transformer::ApplyReducer(Decl *D) {
  if (!ReducerT)
    ReducerT.reset(new Reducer(*this));

  if (FunctionDecl *FD = dyn_cast<FunctionDecl>(D)) {
    if (Stmt *Body = FD->getBody()) {
      Stmt *Result = ReducerT->Visit(Body);
      if (Result != Body)
        FD->setBody(Result);
    }
    return !getSema().getDiagnostics().hasErrorOccurred();
  }

  if (VarDecl *VD = dyn_cast<VarDecl>(D)) {
    if (Expr *Init = VD->getInit()) {
      Expr *NewInit = cast<Expr>(ReducerT->Visit(Init));
      if (NewInit != Init) {
        VD->setInit(NewInit);
      }
    }
  }

  //TODO: in class visit constants and property initializers, methods default
  // arguments, method bodies.
  return true;
}


bool Transformer::ApplyLegalizer(Decl *D) {
  if (!LegalizeT)
    LegalizeT.reset(new Legalizer(*this));
  if (FunctionDecl *FD = dyn_cast<FunctionDecl>(D))
    return LegalizeT->LegalizeFunction(FD);
  if (VarDecl *VD = dyn_cast<VarDecl>(D))
    return LegalizeT->legalizeVariable(VD);
  if (CXXRecordDecl *CD = dyn_cast<CXXRecordDecl>(D))
    return LegalizeT->LegalizeClass(CD);
  return true;
}


Expr *Transformer::createChainExpression(SourceLocation Loc,
                                         Expr *Chain, Expr *Item) {
  if (!Chain)
    return Item;
  if (!Item)
    return Chain;
  Expr *CommaExpr = getSema().ActOnBinaryOp(Loc, tok::comma, Chain, Item).get();
  CommaExpr->setValueKind(Item->getValueKind());
  return CommaExpr;
}


Expr *Transformer::createFunctionCall(Expr *Func,
                                      ArrayRef<Expr *> Args) {
  assert(0);
  return nullptr;
}


Expr *Transformer::createCFunctionCall(StringRef FuncName,
                                       ArrayRef<Expr *> Args) {
  assert(0);
  return nullptr;
}


Expr *Transformer::createRuntimeFunctionCall(StringRef FuncName,
                                             MutableArrayRef<Expr *> Args) {
  return getSema().getRuntime().makeRuntimeFunctionCall(FuncName, Args);
}

}
