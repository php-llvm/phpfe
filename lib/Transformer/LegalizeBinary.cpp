//===--- LegalizeBinary.cpp --- AST Transformation Component ----*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Legalization of binary operators.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Transformer/Legalizer.h"
#include "phpfe/Sema/Runtime.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


/// \brief Tries to adjust binary operation node so that it will be correct C++
/// binary operation.
///
/// \returns Adjusted binary operation node, or nullptr if C++ semantics cannot
/// be used.
///
Expr *Legalizer::TryCxxBinOpSemantics(BinaryOperator *BO) {
  Expr *LHS = BO->getLHS();
  Expr *RHS = BO->getRHS();
  // C++ semantics is applicable to integral types only.
  if (LHS->getType()->isArithmeticType() &&
      RHS->getType()->isArithmeticType()) {
    switch (BO->getOpcode()) {
    case BO_Add:
    case BO_Sub:
    case BO_Mul:
      // Some operation may be represented by C++ node only if special option
      // enables that.
      if (!Semantics.getPhpOptions().CxxArith)
        return nullptr;
      // If any of operand is float, result is float as well.
      if (LHS->getType()->isFloatingType() || RHS->getType()->isFloatingType()) {
        assert(BO->getType()->isFloatingType());
        //TODO: convert the other operand to float.
      }
      return BO;

    case BO_Div:
      // Division is unaffected by the option CxxArith.
      return nullptr;

    case BO_And:
    case BO_Or:
    case BO_Xor:
    case BO_LAnd:
    case BO_LOr:
    case BO_Rem: //TODO: is it really C++ semantics?
    case BO_Shl:
    case BO_Shr:
    case BO_LT:
    case BO_GT:
    case BO_LE:
    case BO_GE:
    case BO_EQ:
    case BO_NE:
      // These operations can be represented by C++ semantics.
      break;

    default:
      return nullptr;
    }
  }
  return nullptr;
}


Expr *Legalizer::VisitBinAssign(BinaryOperator *E) {
  // We do not legalize LHS yet, because it may require special legalization.
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();

  if (isa<DeclRefExpr>(LHS)) {
    // Assignment to a variable.
    if (Runtime::isUniversalType(LHS->getType())) {
      // We rely on Runtime methods, because actual assignment depends on
      // universal type implementation.
      RHS = cast<Expr>(Visit(E->getRHS()));
      return getRuntime().generateAssign(LHS, RHS);
    }

    QualType LHSType = LHS->getType();
    if (LHSType->isRealType()) {
      RHS = LegalizeTypedExpr(LHSType, RHS);
      return Semantics.BuildBinOp(nullptr, E->getExprLoc(),
                                  BO_Assign, LHS, RHS).get();
    }

    if (LHSType == RuntimeTypes::getStringBoxType() ||
        LHSType == RuntimeTypes::getArrayBoxType()) {
      RHS = LegalizeTypedExpr(LHSType, RHS);
      return getRuntime().generateAssign(LHS, RHS);
    }
    assert(!isStringType(LHSType) && "unexpected string type");

    if (isObjectType(LHSType)) {
      llvm_unreachable("obect type assig is not implemente yet");
    }

    // Assignment to a variable of type other than universal.
    llvm_unreachable("not implemented");
  } else {
    // TODO: we need to handle special cases, including assignment to array
    // element or object property
    llvm_unreachable("not implemented");
  }
  return E;
}


static Expr *generateOperationNode(BinaryOperatorKind Kind,
                                   Expr *LHS, Expr *RHS, Runtime &RT) {
  switch (Kind) {
  case BO_AddAssign: return RT.generateAddAssign(LHS, RHS);
  case BO_SubAssign: return RT.generateSubAssign(LHS, RHS);
  case BO_MulAssign: return RT.generateMulAssign(LHS, RHS);
  case BO_DivAssign: return RT.generateDivAssign(LHS, RHS);
  case BO_RemAssign: return RT.generateRemAssign(LHS, RHS);
  case BO_AndAssign: return RT.generateAndAssign(LHS, RHS);
  case BO_OrAssign:  return RT.generateOrAssign(LHS, RHS);
  case BO_XorAssign: return RT.generateXorAssign(LHS, RHS);
  case BO_ShlAssign: return RT.generateShlAssign(LHS, RHS);
  case BO_ShrAssign: return RT.generateShrAssign(LHS, RHS);
  case BO_PhpPowerAssign: return RT.generatePowAssign(LHS, RHS);
  default:
    break;
  }
  llvm_unreachable("unexpected operation node");
  return nullptr;
}


static QualType getRHSLegalizationType(BinaryOperator *E) {
  auto Kind = E->getOpcode();
  if (Kind == BO_RemAssign)
    return Runtime::getIntValueType();

  if (Kind == BO_PhpPowerAssign)
    return E->getRHS()->getType();
  
  if (Kind == BO_AndAssign || Kind == BO_OrAssign || Kind == BO_XorAssign) {
    if (Runtime::isStringType(E->getLHS()->getType()))
      return Runtime::getStringBoxType();
    return Runtime::getIntValueType();
  }
  
  return E->getLHS()->getType();
}


static bool canUseCXXOperatorSemantic(BinaryOperator *E) {
  return E->getOpcode() != BO_ShlAssign && E->getOpcode() != BO_PhpPowerAssign;
}


Expr *Legalizer::LegalizeCombinedAssign(CompoundAssignOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();

  if (isa<DeclRefExpr>(LHS)) {
    // Assignment to a variable.

    if (Runtime::isUniversalType(LHS->getType())) {
      // We rely on Runtime methods, because actual assignment depends on
      // universal type implementation.
      RHS = cast<Expr>(Visit(RHS));
      // Void type on RHS must has been processed by previous transformer
      // passes.
      assert(!RHS->getType()->isVoidType());
      RHS = generateOperationNode(E->getOpcode(), LHS, RHS, getRuntime());
      return getRuntime().generateAssign(LHS, RHS);
    }

    // Assignment to typed variable.
    QualType LHSType = LHS->getType();
    if (LHSType->isRealType()) {  // boolean, integer, float.
      RHS = LegalizeTypedExpr(getRHSLegalizationType(E), RHS);
      if (canUseCXXOperatorSemantic(E))
        return Semantics.BuildBinOp(nullptr, E->getExprLoc(),
          E->getOpcode(), LHS, RHS).get();
      RHS = generateOperationNode(E->getOpcode(), LHS, RHS, getRuntime());
      return Semantics.BuildBinOp(nullptr, E->getExprLoc(), BO_Assign, 
                                  LHS, RHS).get();
    }

    if (LHSType == Runtime::getArrayBoxType()) {
      RHS = Runtime::isArrayType(RHS->getType())
        ? cast<Expr>(Visit(RHS))
        : LegalizeTypedExpr(LHSType, RHS);
      RHS = generateOperationNode(E->getOpcode(), LHS, RHS, getRuntime());
      return getRuntime().generateAssign(LHS, RHS);
    }
    if (LHSType == Runtime::getStringBoxType()) {
      RHS = Runtime::isStringType(RHS->getType())
        ? cast<Expr>(Visit(RHS))
        : LegalizeTypedExpr(LHSType, RHS);
      RHS = generateOperationNode(E->getOpcode(), LHS, RHS, getRuntime());
      return getRuntime().generateAssign(LHS, RHS);
    }


    if (Runtime::isObjectType(LHSType)) {
      llvm_unreachable("obect type assig is not implemente yet");
    }

    // Assignment to a variable of type other than universal.
    llvm_unreachable("not implemented");
  } else {
    // TODO: we need to handle special cases, including assignment to array
    // element or object property
    llvm_unreachable("not implemented");
  }
  return E;
}


Expr *Legalizer::VisitBinAddAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}


Expr* Legalizer::VisitBinAdd(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);

  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;

  Runtime& RT = Semantics.getRuntime();
  return RT.generateAdd(LHS, RHS);
}

  
Expr *Legalizer::VisitBinSubAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}
  

Expr* Legalizer::VisitBinSub(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);

  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;

  Runtime& RT = Semantics.getRuntime();
  return RT.generateSub(LHS, RHS);
}


Expr *Legalizer::VisitBinMulAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}
  
  
Expr* Legalizer::VisitBinMul(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);
  
  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;
  
  Runtime& RT = Semantics.getRuntime();
  return RT.generateMul(LHS, RHS);
}
  
  
Expr *Legalizer::VisitBinDivAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}
  
  
Expr* Legalizer::VisitBinDiv(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);
  
  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;
  
  Runtime& RT = Semantics.getRuntime();
  return RT.generateDiv(LHS, RHS);
}
  
  
Expr *Legalizer::VisitBinRemAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}
  
  
Expr* Legalizer::VisitBinRem(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);
  
  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;
  
  Runtime& RT = Semantics.getRuntime();
  return RT.generateRem(LHS, RHS);
}
  
  
Expr *Legalizer::VisitBinAndAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}
  
  
Expr* Legalizer::VisitBinAnd(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);
  
  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;
  
  Runtime& RT = Semantics.getRuntime();
  return RT.generateAnd(LHS, RHS);
}
  
  
Expr *Legalizer::VisitBinOrAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}
  
  
Expr* Legalizer::VisitBinOr(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);
  
  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;
  
  Runtime& RT = Semantics.getRuntime();
  return RT.generateOr(LHS, RHS);
}
  
  
Expr *Legalizer::VisitBinXorAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}
  
  
Expr* Legalizer::VisitBinXor(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);
  
  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;
  
  Runtime& RT = Semantics.getRuntime();
  return RT.generateXor(LHS, RHS);
}
  
  
Expr *Legalizer::VisitBinShlAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}
  
  
Expr* Legalizer::VisitBinShl(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);
  
  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;
  
  Runtime& RT = Semantics.getRuntime();
  return RT.generateShl(LHS, RHS);
}
  
  
Expr *Legalizer::VisitBinShrAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}
  
  
Expr* Legalizer::VisitBinShr(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);
  
  if (Expr *CxxNode = TryCxxBinOpSemantics(E))
    return CxxNode;
  
  Runtime& RT = Semantics.getRuntime();
  return RT.generateShr(LHS, RHS);
}
  
  
Expr *Legalizer::VisitBinPhpPowerAssign(CompoundAssignOperator *E) {
  return LegalizeCombinedAssign(E);
}

  
Expr* Legalizer::VisitBinPhpPower(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  E->setLHS(LHS);
  E->setRHS(RHS);
  
  Runtime& RT = Semantics.getRuntime();
  return RT.generatePow(LHS, RHS);
}
  
  
  
Expr *Legalizer::VisitBinEQ(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  if (LHS->getType()->isArithmeticType() &&
      RHS->getType()->isArithmeticType()) {
    return Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_EQ,
                                LHS, RHS).get();
  }

  Expr *CompareResult = Semantics.getRuntime().generateCmp(LHS, RHS);
  ExprResult EQ = Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_EQ,
      CompareResult, Semantics.ActOnIntegerConstant(E->getLocStart(), 0).get());
  assert(EQ.isUsable());
  return EQ.get();
}


Expr *Legalizer::VisitBinNE(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  if (LHS->getType()->isArithmeticType() &&
      RHS->getType()->isArithmeticType()) {
    return Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_NE,
                                LHS, RHS).get();
  }

  Expr *CompareResult = Semantics.getRuntime().generateCmp(LHS, RHS);
  ExprResult NE = Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_NE,
      CompareResult, Semantics.ActOnIntegerConstant(E->getLocStart(), 0).get());
  assert(NE.isUsable());
  return NE.get();
}


Expr *Legalizer::VisitBinLT(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  if (LHS->getType()->isArithmeticType() &&
      RHS->getType()->isArithmeticType()) {
    return Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_LT,
                                LHS, RHS).get();
  }

  Expr *CompareResult = Semantics.getRuntime().generateCmp(LHS, RHS);
  ExprResult LT = Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_LT,
      CompareResult, Semantics.ActOnIntegerConstant(E->getLocStart(), 0).get());
  assert(LT.isUsable());
  return LT.get();
}


Expr *Legalizer::VisitBinGT(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  if (LHS->getType()->isArithmeticType() &&
      RHS->getType()->isArithmeticType()) {
    return Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_GT,
                                LHS, RHS).get();
  }

  Expr *CompareResult = Semantics.getRuntime().generateCmp(LHS, RHS);
  ExprResult GT = Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_GT,
      CompareResult, Semantics.ActOnIntegerConstant(E->getLocStart(), 0).get());
  assert(GT.isUsable());
  return GT.get();
}

Expr *Legalizer::VisitBinLE(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  if (LHS->getType()->isArithmeticType() &&
      RHS->getType()->isArithmeticType()) {
    return Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_LE,
                                LHS, RHS).get();
  }

  Expr *CompareResult = Semantics.getRuntime().generateCmp(LHS, RHS);
  ExprResult LE = Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_LE,
      CompareResult, Semantics.ActOnIntegerConstant(E->getLocStart(), 0).get());
  assert(LE.isUsable());
  return LE.get();
}


Expr *Legalizer::VisitBinGE(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  if (LHS->getType()->isArithmeticType() &&
      RHS->getType()->isArithmeticType()) {
    return Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_GE,
                                LHS, RHS).get();
  }

  Expr *CompareResult = Semantics.getRuntime().generateCmp(LHS, RHS);
  ExprResult GE = Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_GE,
      CompareResult, Semantics.ActOnIntegerConstant(E->getLocStart(), 0).get());
  assert(GE.isUsable());
  return GE.get();
}


Expr *Legalizer::VisitBinComma(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  
  return Semantics.BuildBinOp(nullptr, E->getOperatorLoc(), BO_Comma,
                              LHS, RHS).get();
}


Expr *Legalizer::VisitBinPhpSpaceship(BinaryOperator *S) {
  Expr *LHS = cast<Expr>(Visit(S->getLHS()));
  Expr *RHS = cast<Expr>(Visit(S->getRHS()));

  Expr *CompareResult = Semantics.getRuntime().generateCmp(LHS, RHS);
  assert(CompareResult);

  return CompareResult;
}

Expr *Legalizer::VisitBinPhpIdent(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  QualType LHSType = LHS->getType().getCanonicalType();
  if (LHSType->isRealType() && RHS->getType()->isRealType()) {
    assert(LHSType == RHS->getType().getCanonicalType());
    return 
      Semantics.BuildBinOp(nullptr, E->getExprLoc(), BO_EQ, LHS, RHS).get();
  }

  Expr *CompareResult = Semantics.getRuntime().generateIdent(LHS, RHS);
  assert(CompareResult);
  return CompareResult;
}


Expr *Legalizer::VisitBinPhpNotIdent(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  Expr *CompareResult = Semantics.getRuntime().generateIdent(LHS, RHS);
  assert(CompareResult);
  ExprResult Not = Semantics.BuildUnaryOp(nullptr, E->getOperatorLoc(),
                                          UO_LNot, CompareResult);
  assert(Not.isUsable());
  return Not.get();
}


Expr *Legalizer::VisitBinPhpConcat(BinaryOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  return Semantics.getRuntime().generateConcat(LHS, RHS);
}


Expr *Legalizer::VisitBinPhpConcatAssign(CompoundAssignOperator *E) {
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));

  return Semantics.getRuntime().generateConcatAssign(LHS, RHS);
}


Expr *Legalizer::VisitBinLAnd(BinaryOperator *E) {
  Expr *LHS = legalizeBooleanExpr(E->getLHS());
  Expr *RHS = legalizeBooleanExpr(E->getRHS());

  E->setLHS(LHS);
  E->setRHS(RHS);
  E->setType(Context.BoolTy);//TODO: remove when Decorator is activated.
  return E;
}


Expr *Legalizer::VisitBinLOr(BinaryOperator *E) {
  Expr *LHS = legalizeBooleanExpr(E->getLHS());
  Expr *RHS = legalizeBooleanExpr(E->getRHS());

  E->setLHS(LHS);
  E->setRHS(RHS);
  E->setType(Context.BoolTy);//TODO: remove when Decorator is activated.
  return E;
}


Expr *Legalizer::VisitConditionalOperator(ConditionalOperator *E) {
  Expr *Cond = legalizeBooleanExpr(E->getCond());
  E->setCond(Cond);
  Expr *TrueExpr = cast<Expr>(Visit(E->getTrueExpr()));
  E->setTrueExpr(TrueExpr);
  Expr *FalseExpr = cast<Expr>(Visit(E->getFalseExpr()));
  E->setFalseExpr(FalseExpr);

  QualType TType = TrueExpr->getType().getCanonicalType();
  QualType FType = FalseExpr->getType().getCanonicalType();
  if (TType == FType && TType->isBuiltinType()) {
    // Use C++ semantics.
    //TODO:assert(E->getType().getCanonicalType() == TType);
    E->setType(TType);
    return E;
  }

  // If types of true and false branches are different, cast branch values to
  // box type.
  TrueExpr = getRuntime().createExprBox(TrueExpr);
  E->setTrueExpr(TrueExpr);
  FalseExpr = getRuntime().createExprBox(FalseExpr);
  E->setFalseExpr(FalseExpr);
  E->setType(getClassBoxType());
  return E;
}


Expr *Legalizer::VisitBinaryConditionalOperator(BinaryConditionalOperator *E) {
  Expr *Common = cast<Expr>(Visit(E->getCommon()));
  if (OpaqueValueExpr *VE = dyn_cast<OpaqueValueExpr>(E->getTrueExpr())) {
    assert(VE->getSourceExpr() == E->getCommon());
    VE->setSourceExpr(Common);
  }
  E->setCommon(Common);
  Expr *Cond = legalizeBooleanExpr(E->getCond());
  E->setCond(Cond);
  Expr *FalseExpr = cast<Expr>(Visit(E->getFalseExpr()));
  E->setFalseExpr(FalseExpr);

  QualType TType = Common->getType().getCanonicalType();
  QualType FType = FalseExpr->getType().getCanonicalType();
  if (TType == FType && TType->isBuiltinType()) {
    // Use C++ semantics.
    //TODO:assert(E->getType().getCanonicalType() == TType);
    E->setType(TType);
    return E;
  }

  // If types of true and false branches are different, cast branch values to
  // box type.
  Expr *TrueExpr = E->getTrueExpr();
  if (TrueExpr->getType() != getClassBoxType()) {
    TrueExpr = getRuntime().createExprBox(TrueExpr);
    E->setTrueExpr(TrueExpr);
  }
  if (FalseExpr->getType() != getClassBoxType()) {
    FalseExpr = getRuntime().createExprBox(FalseExpr);
    E->setFalseExpr(FalseExpr);
  }
  E->setType(getClassBoxType());
  return E;
}

}
