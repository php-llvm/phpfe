//===--- Legalizer.cpp - AST Transformation Component -----------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Reducer transformation.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Transformer/Reducer.h"
#include "phpfe/Transformer/Eliminator.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Reducer::Reducer(Transformer &T)
  : Trans(T), Semantics(T.getSema()), Context(T.getASTContext()) {
}


Expr *Reducer::VisitIntegerLiteral(IntegerLiteral *E) {
  APInt Val = E->getValue();
  Semantics.getConstantValue(Semantics.alignToIntValueWidth(E));
  return E;
}


Expr *Reducer::VisitStringLiteral(StringLiteral *E) {
  Semantics.getConstantValue(E);
  return E;
}


Expr *Reducer::VisitFloatingLiteral(FloatingLiteral *E) {
  Semantics.getConstantValue(E);
  return E;
}


Expr *Reducer::VisitPhpNullLiteral(PhpNullLiteral *E) {
  Semantics.getConstantValue(E);
  return E;
}


Expr *Reducer::VisitCXXBoolLiteralExpr(CXXBoolLiteralExpr *E) {
  Semantics.getConstantValue(E);
  return E;
}


Expr *Reducer::VisitArraySubscriptExpr(ArraySubscriptExpr *E) {
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return Semantics.getConstantExpr(V, E->getLocStart(), true);

  Expr *Base = cast<Expr>(Visit(E->getLHS()));
  if (Base != E->getLHS()) {
    E->setLHS(Base);
    Changed = true;
  }
  Expr *Index = cast<Expr>(Visit(E->getRHS()));
  if (Index != E->getRHS()) {
    E->setRHS(Index);
    Changed = true;
  }

  return E;
}


Expr *Reducer::replaceBy(Expr *E, Expr *S) {
  Expr *SideEffects = Eliminator(Trans).eliminateUnused(E, true);
  if (SideEffects)
    return Semantics.packSideEffect(E->getLocStart(), SideEffects, S);
  return S;
}


Expr *Reducer::replaceBy(Expr *E, ConstValue *S) {
  Expr *Res = Semantics.getConstantExpr(S, E->getLocStart(), true);
  Expr *SideEffects = Eliminator(Trans).eliminateUnused(E, true);
  if (SideEffects)
    return Semantics.packSideEffect(E->getLocStart(), SideEffects, Res);
  return Res;
}


/// \brief Helper method for processing of casts.
///
/// \param TargetType Type to which expression is casted.
/// \param SubExpr The expression being casted.
/// \param Loc Start of cast expression.
///
/// Is used to make processing common for reduction of both implicit and
/// explicit casts.
///
Expr *Reducer::reduceCastExpression(QualType TargetType, Expr *SubExpr,
                                    SourceLocation Loc) {
  QualType ExprType = SubExpr->getType();

  if (ExprType == TargetType)
    return SubExpr;

  if (TargetType->isVoidType())
    return replaceBy(SubExpr, Semantics.getConstants().getNull());

  if (TargetType->isBooleanType()) {
    if (ExprType->isVoidType()) {
      return replaceBy(SubExpr, Semantics.getConstants().getFalse());
    }
  }
  else if (TargetType->isIntegerType()) {
    if (ExprType->isVoidType()) {
      return replaceBy(SubExpr, Semantics.getConstants().getZero());
    }
  }
  else if (TargetType->isFloatingType()) {
    if (ExprType == Runtime::getVoidType()) {
      return replaceBy(SubExpr, Semantics.getConstants().getFloatZero());
    }
  }
  else if (Runtime::isStringType(TargetType)) {
    if (ExprType->isVoidType())
      return replaceBy(SubExpr, getSema().BuildStringLiteral("", Loc));
    if (Runtime::isArrayType(ExprType)) {
      Semantics.Diag(SubExpr->getLocStart(),
                     diag::warn_php_implicit_type_conversion)
        << "array" << "string" << SubExpr->getSourceRange();
      return replaceBy(SubExpr, getSema().BuildStringLiteral("Array", Loc));
    }
  }
  else if (Runtime::isArrayType(TargetType) && !Runtime::isArrayType(ExprType)) {
    assert(!Runtime::isBoxedType(ExprType));
    llvm_unreachable("array cast reduction is not implemented for dynamic arrays yet");
  }

  return nullptr;
}


Expr *Reducer::VisitCStyleCastExpr(CStyleCastExpr *E) {
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return Semantics.getConstantExpr(V, E->getLocStart(), true);

  Expr *Sub = cast<Expr>(Visit(E->getSubExpr()));
  E->setSubExpr(Sub);

  if (Expr *CR = reduceCastExpression(E->getType(), Sub, E->getLParenLoc()))
    return CR;

  return E;
}


Expr *Reducer::VisitImplicitCastExpr(ImplicitCastExpr *E) {
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return Semantics.getConstantExpr(V, E->getLocStart(), true);

  Expr *Sub = cast<Expr>(Visit(E->getSubExpr()));
  E->setSubExpr(Sub);

  if (Expr *CR = reduceCastExpression(E->getType(), Sub, E->getExprLoc()))
    return CR;

  return E;
}


Expr *Reducer::VisitParenExpr(ParenExpr *E) {
  Expr *Sub = cast<Expr>(Visit(E->getSubExpr()));
  if (Sub != E->getSubExpr())
    E->setSubExpr(Sub);
  return E;
}


Expr *Reducer::VisitExpr(Expr *E) {
  return E;
}


Stmt *Reducer::VisitStmt(Stmt *S) {
  return S;
}


Stmt *Reducer::VisitCompoundStmt(CompoundStmt *S) {
  std::vector<Stmt *> NewBody;
  for (Stmt *Item : S->body()) {
    if (Item) {
      if (Expr *E = dyn_cast<Expr>(Item)) {
        Expr *NE = cast<Expr>(Visit(E));
        // If this is expression like '$a+1', it may be removed, but expressions
        // with side effects must be retained.
        NE = Eliminator(Trans).eliminateUnused(NE);
        if (NE != E)
          Changed = true;
        if (NE)
          NewBody.push_back(NE);
      } else {
        Stmt *NS = Visit(Item);
        if (NS != Item)
          Changed = true;
        if (NS)
          NewBody.push_back(NS);
      }
    }
  }
  if (NewBody.size() < S->size())
    S->setStmts(Context, NewBody.data(), NewBody.size());
  else
    std::copy(NewBody.begin(), NewBody.end(), S->body_begin());
  return S;
}


Stmt *Reducer::VisitDeclStmt(DeclStmt *S) {
  DeclGroupRef DG = S->getDeclGroup();
  for (Decl* D : DG) {
    if (StaticAssertDecl *SA = dyn_cast<StaticAssertDecl>(D)) {
      ProcessStaticAssertDecl(SA);
    }
  }
  return S;
}


void Reducer::ProcessStaticAssertDecl(StaticAssertDecl *D) {
  if (!D->isFailed())
    // Already evaluated.
    return;

  Evaluator EV(Semantics);
  Expr *E = D->getAssertExpr();

  if (ConstValue *V = EV.evaluate(E)) {
    bool Success = false;
    if (V->isBool()) {
      Success = V->getBool();
    } else {
      Semantics.Diag(E->getExprLoc(), diag::err_php_static_assert_bool)
        << E->getSourceRange();
      return;
    }

    if (Success) {
      D->setFailed(false);
      return;
    }

    if (auto Message = D->getMessage())
      Semantics.Diag(E->getExprLoc(), diag::err_php_static_assert)
      << 1 << Message->getString();
    else
      Semantics.Diag(E->getExprLoc(), diag::err_php_static_assert)
      << 0;
    return;
  }

  if (EV.seenError())
    return;

  const Expr *Offending = EV.getOffendingExpr();
  Semantics.Diag(Offending->getExprLoc(), diag::err_php_static_assert_not_const)
    << Offending->getSourceRange();
}


Stmt *Reducer::VisitReturnStmt(ReturnStmt *S) {
  if (Expr *Ret = S->getRetValue()) {
    Expr *Sub = cast<Expr>(Visit(Ret));
    if (Sub != Ret)
      S->setRetValue(Sub);
  }
  return S;
}

}
