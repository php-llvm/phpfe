//===--- ReduceAssign.cpp --- AST Transformation Component ------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Implementation of class Reducer, assignment nodes.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Transformer/Eliminator.h"
#include "phpfe/Transformer/Reducer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


/// \brief Transforms assignment of null.
///
/// If RHS is of void type, make transformation:
// \code
///   lhs = rhs;   ->   side_effect(rhs), x = (lhs_type)null;
/// \endcode
///
Expr *Reducer::processNullAssignment(BinaryOperator *BO) {
  assert(BO->getOpcode() == BO_Assign);
  Expr *LHS = BO->getLHS();
  Expr *RHS = BO->getRHS();
  SourceLocation RHSLoc = RHS->getLocStart();
  assert(RHS->getType()->isVoidType());
  QualType LHSType = LHS->getType();

  Expr *NewRHS = nullptr;
  if (LHSType->isBooleanType()) {
    NewRHS = Semantics.ActOnCXXBoolLiteral(RHSLoc, tok::kw_false).get();
  } else if (LHSType->isIntegerType()) {
    NewRHS = Semantics.ActOnIntegerConstant(RHSLoc, 0).get();
  } else if (LHSType->isFloatingType()) {
    NewRHS = FloatingLiteral::Create(Context, APFloat(0.0), true, LHSType, RHSLoc);
  } else if (Runtime::isStringType(LHSType)) {
    NewRHS = Semantics.BuildStringLiteral(StringRef(), RHSLoc);
  } else if (Runtime::isArrayType(LHSType)) {
  } else if (Runtime::isUniversalType(LHSType)) {
    if (!isa<PhpNullLiteral>(RHS))
      NewRHS = new(Context) PhpNullLiteral(Context.VoidTy, RHSLoc);
  } else {
    llvm_unreachable("not implemented");
  }

  if (NewRHS && NewRHS != BO->getRHS()) {
    BO->setRHS(NewRHS);
    Changed = true;
    if (Expr *SideEffects = Eliminator(Trans).eliminateUnused(RHS, true))
      return Semantics.packSideEffect(BO->getLocStart(), SideEffects, BO);
  }
  return BO;
}


Expr *Reducer::VisitBinAssign(BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  // Process operands.
  LHS = cast<Expr>(Visit(LHS));
  RHS = cast<Expr>(Visit(RHS));
  assert(LHS);
  assert(RHS);

  // Apply reduction if happened.
  if (E->getLHS() != LHS) {
    E->setLHS(LHS);
    Changed = true;
  }
  if (E->getRHS() != RHS) {
    E->setRHS(RHS);
    Changed = true;
  }

  return E;
}


Expr *Reducer::VisitBinAddAssign(CompoundAssignOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  // Process operands.
  LHS = cast<Expr>(Visit(LHS));
  RHS = cast<Expr>(Visit(RHS));
  assert(LHS);
  assert(RHS);

  // Apply reduction if happened.
  if (E->getLHS() != LHS) {
    E->setLHS(LHS);
    Changed = true;
  }
  if (E->getRHS() != RHS) {
    E->setRHS(RHS);
    Changed = true;
  }
  //TODO: probably we need to update ComputedLHS and Res.

  if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
    ConstValue *NewVal = Semantics.getConstantValue(Val,
                                E->getComputationLHSType(), RHS->getLocStart());
    if (!NewVal)
      // Conversion error, must have been reported.
      return E;
    Changed = true;
    QualType LHSType = LHS->getType();
    if (Val->isZero() && !Runtime::isObjectType(LHSType) &&
        !Runtime::isUniversalType(LHSType))
      return LHS;
    RHS = Semantics.getConstantExpr(NewVal, RHS->getLocStart(), true);
    E->setRHS(RHS);
  }

  return E;
}


Expr *Reducer::VisitBinSubAssign(CompoundAssignOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  // Process operands.
  LHS = cast<Expr>(Visit(LHS));
  RHS = cast<Expr>(Visit(RHS));
  assert(LHS);
  assert(RHS);

  // Apply reduction if happened.
  if (E->getLHS() != LHS) {
    E->setLHS(LHS);
    Changed = true;
  }
  if (E->getRHS() != RHS) {
    E->setRHS(RHS);
    Changed = true;
  }

  // Do not make reduction for objects and values of unknown type.
  if (Runtime::isObjectType(LHS->getType()) ||
      Runtime::isUniversalType(LHS->getType()) ||
      Runtime::isObjectType(RHS->getType()) ||
      Runtime::isUniversalType(RHS->getType()))
    return E;

  if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
    ConstValue *NewVal = Semantics.getConstantValue(Val,
                                E->getComputationLHSType(), RHS->getLocStart());
    if (!NewVal)
      // Conversion error, must have been reported.
      return E;
    Changed = true;
    if (Val->isZero())
      return LHS;
    RHS = Semantics.getConstantExpr(NewVal, RHS->getLocStart(), true);
    E->setRHS(RHS);
  }

  return E;
}


}
