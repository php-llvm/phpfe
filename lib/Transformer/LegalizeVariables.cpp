//===--- Transformer.cpp - AST Transformation Component ---------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2016, Serge Pavlov.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// This file implements the actions class which performs semantic analysis and
// builds an AST out of a parse stream.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
// #include "clang/AST/ASTContext.h"
// #include "clang/AST/StmtPhp.h"
// #include "clang/AST/ExprPhp.h"
// #include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
// #include "phpfe/Transformer/Transformer.h"
// #include "phpfe/Transformer/Reducer.h"
#include "phpfe/Transformer/Legalizer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


bool Legalizer::legalizeVariable(VarDecl *Var) {

  // If this is a global variable.
  if (Semantics.isPhpVariable(Var)) {
    // Adjust properties required for global variable.
    getRuntime().adjustGlobalVariableAttrs(Var);

    // We need to create corresponding entry in $GLOBALS and put it into special
    // linker section.
    VarDecl *GVR = getRuntime().createGlobalVariableRecord(Var);
    Semantics.addUnitDeclaration(GVR);
    return true;
  }

  // If this variable represents PHP constant.
  if (Semantics.isPhpConstant(Var)) {
    assert(Var->getType().getLocalUnqualifiedType() == getValueInfoType());
    //TODO: Check if Init is compile time expression.
    // If this is a constant defined in emodule, create constant in readonly
    // memory, otherwise create runtime constant.
    if (Semantics.getPhpOptions().Kind == PhpModuleKind::EModule &&
        Var->hasInit()) {
      getRuntime().initConstant(Var, Var->getInit());
      Var->setConstexpr(true);
    } else {
      getRuntime().initConstant(Var, nullptr);
      Var->setIsUsed();
    }
    return true;
  }

  // Assume this is some C++ variable, hence does not require legalization.
  return true;
}

}
