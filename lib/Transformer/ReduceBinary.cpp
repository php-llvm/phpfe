//===--- ReduceBinary.cpp --- AST Transformation Component ------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Implementation of class Reducer, binary nodes.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Analyzer/Decorator.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Transformer/Eliminator.h"
#include "phpfe/Transformer/Reducer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


/// \brief Makes generic processing of a binary operator node.
///
/// \returns nullptr if no further treatment required, pointer to the processed
/// node otherwise. The node may be the same as the argument or to be a new node
/// that should replace the argument.
///
Expr *Reducer::reduceBinaryOperator(BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  // Process operands.
  LHS = cast<Expr>(Visit(LHS));
  RHS = cast<Expr>(Visit(RHS));

  // Apply reduction if happened.
  if (E->getLHS() != LHS) {
    E->setLHS(LHS);
    Changed = true;
  }
  if (E->getRHS() != RHS) {
    E->setRHS(RHS);
    Changed = true;
  }

  // Update expression type.
  Decorator D(Semantics);
  QualType ResType = D.decorateByType(E);
  if (ResType.isNull())
    return nullptr; // Error occurred
  E->setType(ResType);

  // In most cases universal or object type prevents reducer from further
  // optimizations.
  //TODO: more specific test if type of object is known.
  if (isUniversalType(ResType) || isObjectType(ResType))
    return nullptr;

  // Try to evaluate the node.
  Evaluator EV(Semantics);
  if (ConstValue *Val = EV.evaluate(E))
    return Semantics.getConstantExpr(Val, E->getExprLoc(), true);
  if (EV.seenError())
    return nullptr;

  return E;
}


Expr *Reducer::VisitBinMulAssign(CompoundAssignOperator *E) {
  Expr *Res = reduceBinaryOperator(E);
  if (!Res)
    return E;

  // If node is not changed, try specific reduction.
  Expr *RHS = E->getRHS();
  if (E == Res && E->getOpcode() == BO_MulAssign) {
    // Process the case when RHS is a constant.
    if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
      if (Val->isZero()) {
        // Transform:
        //   $a *= 0;
        // to:
        //   $a = 0;
        RHS = replaceBy(RHS, Semantics.getConstants().getZero());
        Expr *Result = new (Context) BinaryOperator(E->getLHS(), RHS, BO_Assign,
            E->getType(), VK_RValue, OK_Ordinary, E->getExprLoc(),
            Semantics.FPFeatures.fp_contract);
        return Result;
      }
    }
  }

  return Res;
}


Expr *Reducer::VisitBinDivAssign(CompoundAssignOperator *E) {
  Expr *Res = reduceBinaryOperator(E);
  if (!Res)
    return E;

  // If node is not changed, try specific reduction.
  Expr *RHS = E->getRHS();
  if (E == Res && E->getOpcode() == BO_DivAssign) {
    // Process the case when RHS is a constant.
    if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
      Expr *Value, *SideEffect;
      Value = Semantics.unpackSideEffect(RHS, SideEffect);
      if (ConstValue *Val = Semantics.getConstantValue(Value)) {
        if (Val->isZero()) {
          Semantics.Diag(E->getExprLoc(), diag::err_php_division_by_zero)
              << E->getRHS()->getSourceRange();
          return E;
        }
        // $a /= 1  =>  $a
        else if (Val->isOne())
          return Semantics.packSideEffect(E->getExprLoc(), SideEffect, E->getLHS());
      }
    }
  }

  return Res;
}


Expr *Reducer::VisitBinRemAssign(BinaryOperator *E) {
  Expr *Res = reduceBinaryOperator(E);
  if (!Res)
    return E;

  // If node is not changed, try specific reduction.
  Expr *RHS = E->getRHS();
  if (E == Res && E->getOpcode() == BO_DivAssign) {
    // Process the case when RHS is a constant.
    if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
      Expr *Value, *SideEffect;
      Value = Semantics.unpackSideEffect(RHS, SideEffect);
      if (ConstValue *Val = Semantics.getConstantValue(Value)) {
        if (Val->isZero()) {
          Semantics.Diag(E->getExprLoc(), diag::err_php_division_by_zero)
            << E->getRHS()->getSourceRange();
          return E;
        }
      }
    }
  }

  return Res;
}


// returns true on error
static bool diagnoseBitwiseAssign(Sema &Semantics, BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  if (Runtime::isFloatingType(LHS->getType())) {
    Semantics.Diag(LHS->getExprLoc(), diag::err_php_cannot_change_type)
      << Semantics.getPHPTypeName(Runtime::getIntValueType())
      << E->getLHS()->getSourceRange();
    return true;
  }

  if (Runtime::isStringType(LHS->getType()) &&
    !Runtime::isStringType(RHS->getType()) &&
    !Runtime::isUniversalType(RHS->getType())) {
    Semantics.Diag(E->getExprLoc(), diag::err_php_unsupported_binary_operand_type)
      << Semantics.getPHPTypeName(RHS->getType()) << 1
      << RHS->getSourceRange();
    return true;
  }
//   if (!(Runtime::isStringType(LHS->getType()) &&
//     Runtime::isStringType(RHS->getType())) &&
//       applyImplicitAssignConversion(E))
//     return true;
  
  return false;
}


Expr *Reducer::VisitBinAndAssign(BinaryOperator *E) {
  Expr *Res = reduceBinaryOperator(E);
  if (!Res)
    return E;

  if (diagnoseBitwiseAssign(Semantics, E))
    return E;

  // If node is not changed, try specific reduction.
  Expr *RHS = E->getRHS();
  if (E == Res && E->getOpcode() == BO_AndAssign) {
    // Process the case when RHS is a constant.
    if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
      // $a &= 0  =>  $a = 0
      if (Val->isZero()) {
        RHS = Semantics.getConstantExpr(Semantics.getConstants().getZero(),
                                        RHS->getExprLoc(), true);
        return Semantics.BuildBinOp(nullptr, E->getExprLoc(),
                                    BO_Assign, E->getLHS(), RHS).get();
      }
    }
  }

  return Res;
}


Expr *Reducer::VisitBinOrAssign(BinaryOperator *E) {
  Expr *Res = reduceBinaryOperator(E);
  if (!Res)
    return E;

  if (diagnoseBitwiseAssign(Semantics, E))
    return E;

  // If node is not changed, try specific reduction.
  Expr *RHS = E->getRHS();
  if (E == Res && E->getOpcode() == BO_OrAssign) {
    // Process the case when RHS is a constant.
    if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
      // $a |= 0  =>  (int)$a
      if (Val->isZero())
        return Semantics.createImplicitCast(getIntValueType(), E->getLHS());
    }
  }
  return Res;
}


Expr *Reducer::VisitBinXorAssign(BinaryOperator *E) {
  Expr *Res = reduceBinaryOperator(E);
  if (!Res)
    return E;

  if (diagnoseBitwiseAssign(Semantics, E))
    return E;

  // If node is not changed, try specific reduction.
  Expr *RHS = E->getRHS();
  if (E == Res && E->getOpcode() == BO_XorAssign) {
    // Process the case when RHS is a constant.
    if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
      // $a ^= 0  =>  (int)$a
      if (Val->isZero())
        return Semantics.createImplicitCast(getIntValueType(), E->getLHS());
    }
  }
  return Res;
}


Expr *Reducer::VisitBinShlAssign(BinaryOperator *E) {
  Expr *Res = reduceBinaryOperator(E);
  if (!Res)
    return E;

  // If node is not changed, try specific reduction.
  Expr *RHS = E->getRHS();
  if (E == Res && E->getOpcode() == BO_ShlAssign) {
    Expr *LHS = E->getLHS();
    // Process the case when RHS is a constant.
    if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
      // $a <<= 0  =>  $a = 0
      if (Val->isZero())
        return Semantics.createImplicitCast(getIntValueType(), LHS);
      ConstValue *CCV = Semantics.getConstantValue(Val, getIntValueType(),
                                                   RHS->getLocStart());
      APInt IVal = CCV->getInt();
      assert(!IVal.isNegative()); // Must have been diagnosed by Decorator
      if (IVal.uge(Runtime::getIntValueWidth())) {
        Semantics.Diag(E->getExprLoc(), diag::warn_php_too_large_shift)
            << RHS->getSourceRange();
        ConstValue *Zero = Semantics.getConstants().getZero();
        RHS = Semantics.getConstantExpr(Zero, RHS->getExprLoc(), true);
        return Semantics.BuildBinOp(nullptr, E->getExprLoc(), BO_Assign,
                                    LHS, RHS).get();
      }
    }
  }
  return E;
}


Expr *Reducer::VisitBinShrAssign(BinaryOperator *E) {
  Expr *Res = reduceBinaryOperator(E);
  if (!Res)
    return E;

  // If node is not changed, try specific reduction.
  Expr *RHS = E->getRHS();
  if (E == Res && E->getOpcode() == BO_ShrAssign) {
    Expr *LHS = E->getLHS();
    // Process the case when RHS is a constant.
    if (ConstValue *Val = Semantics.getConstantValue(RHS)) {
      // $a <<= 0  =>  $a = 0
      if (Val->isZero())
        return Semantics.createImplicitCast(getIntValueType(), LHS);
      ConstValue *CCV = Semantics.getConstantValue(Val, getIntValueType(),
                                                   RHS->getLocStart());
      APInt IVal = CCV->getInt();
      assert(!IVal.isNegative()); // Must have been diagnosed by Decorator
      if (IVal.uge(Runtime::getIntValueWidth())) {
        Semantics.Diag(E->getExprLoc(), diag::warn_php_too_large_shift)
          << RHS->getSourceRange();
        ConstValue *Zero = Semantics.getConstants().getZero();
        RHS = Semantics.getConstantExpr(Zero, RHS->getExprLoc(), true);
        return Semantics.BuildBinOp(nullptr, E->getExprLoc(), BO_Assign,
                                    LHS, RHS).get();
      }
    }
  }
  return E;
}


Expr *Reducer::VisitBinPhpPowerAssign(BinaryOperator *E) {
  reduceBinaryOperator(E);
  return E;
}


ConstValue *Reducer::checkIdentity(BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return V;

  if (EV.seenError())
    return nullptr;

  // Process operands.
  LHS = cast<Expr>(Visit(LHS));
  RHS = cast<Expr>(Visit(RHS));

  if (LHS != E->getLHS()) {
    E->setLHS(LHS);
    Changed = true;
  }
  if (RHS != E->getRHS()) {
    E->setRHS(RHS);
    Changed = true;
  }

  QualType LType = LHS->getType();
  QualType RType = RHS->getType();

  // If any of the operands is of universal type only runtime can resolve the
  // comparison.
  if (Runtime::isUniversalType(LType) || Runtime::isUniversalType(RType))
    return nullptr;

  bool TypesNotMatch = false;

  // NumberBox can be resolved in runtime only but we know that it cannot match
  // some other types.
  if (LType == RuntimeTypes::getNumberBoxType() &&
      !RuntimeTypes::isNumberType(RType))
    TypesNotMatch = true;
  if (RType == RuntimeTypes::getNumberBoxType() &&
      !RuntimeTypes::isNumberType(LType))
    TypesNotMatch = true;
  if (!RuntimeTypes::areTypesCompatible(LType, RType))
    TypesNotMatch = true;

  // If the types definitely don't match we can replace the expression with
  // constant 'true' preserving possible side effects.
  if (TypesNotMatch)
    return Semantics.getConstants().getFalse();

  return nullptr;
}


Expr *Reducer::VisitBinPhpIdent(BinaryOperator *E) {
  if (ConstValue *V = checkIdentity(E))
    return replaceBy(E, V);
  return E;
}


Expr *Reducer::VisitBinPhpNotIdent(BinaryOperator *E) {
  if (ConstValue *V = checkIdentity(E)) {
    if (V->getBool())
      return replaceBy(E, Semantics.getConstants().getFalse());
    else
      return replaceBy(E, Semantics.getConstants().getTrue());
  }
  return E;
}


Expr *Reducer::VisitBinEQ(BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return Semantics.getConstantExpr(V, E->getLocStart(), true);

  return E;
}

Expr *Reducer::VisitBinNE(BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return Semantics.getConstantExpr(V, E->getLocStart(), true);

  return E;
}

Expr *Reducer::VisitBinAdd(BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return Semantics.getConstantExpr(V, E->getLocStart(), true);

  if (EV.seenError())
    return E;

  // Process operands.
  LHS = cast<Expr>(Visit(LHS));
  RHS = cast<Expr>(Visit(RHS));
  ConstValue *LVal = Semantics.getConstantValue(LHS);
  ConstValue *RVal = Semantics.getConstantValue(RHS);

  // If an argument is an array, the other must be of array, object or
  // universal type.
  //TODO:

  if (LVal || RVal) {
    // If one argument is NULL or 0 and the other is not of object or universal
    // type, the result is the second argument converted to number.
    //TODO:
  }

  // Apply reduction if happened.
  if (E->getLHS() != LHS) {
    E->setLHS(LHS);
    Changed = true;
  }
  if (E->getRHS() != RHS) {
    E->setRHS(RHS);
    Changed = true;
  }
  return E;
}


Expr *Reducer::VisitBinPhpPower(BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  assert(LHS);
  assert(RHS);

  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E))
    return Semantics.getConstantExpr(V, E->getLocStart(), true);

  return E;
}


Expr *Reducer::VisitBinLAnd(BinaryOperator *E) {
  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E)) {
    Changed = true;
    return Semantics.getConstantExpr(V, E->getLocStart(), true);
  }
  if (EV.seenError())
    return E;

  // Reduce arguments.
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  if (E->getLHS() != LHS) {
    E->setLHS(LHS);
    Changed = true;
  }
  if (E->getRHS() != RHS) {
    E->setRHS(RHS);
    Changed = true;
  }

  // Process cases when one of argument is a constant.
  if (ConstValue *LVal = Semantics.getConstantValue(LHS)) {
    ConstValue::ConversionStatus Status;
    if (!LVal->getAsBool(Status)) {
      return replaceBy(E, Semantics.getConstants().getFalse());
    } else {
      return RHS;
    }
  }

  if (ConstValue *RVal = Semantics.getConstantValue(RHS)) {
    ConstValue::ConversionStatus Status;
    if (!RVal->getAsBool(Status)) {
      // Result of the expression is 'false' but side effect of the LHS must be
      // saved.
      Eliminator EL(Trans);
      Expr *SideEffect = EL.eliminateUnused(LHS, true);
      return Semantics.packSideEffect(LHS->getLocStart(), SideEffect,
                             replaceBy(E, Semantics.getConstants().getFalse()));
    } else {
      return LHS;
    }
  }

  return E;
}


Expr *Reducer::VisitBinLOr(BinaryOperator *E) {
  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E)) {
    Changed = true;
    return Semantics.getConstantExpr(V, E->getLocStart(), true);
  }
  if (EV.seenError())
    return E;

  // Reduce arguments.
  Expr *LHS = cast<Expr>(Visit(E->getLHS()));
  Expr *RHS = cast<Expr>(Visit(E->getRHS()));
  if (E->getLHS() != LHS) {
    E->setLHS(LHS);
    Changed = true;
  }
  if (E->getRHS() != RHS) {
    E->setRHS(RHS);
    Changed = true;
  }

  // Process cases when one of argument is a constant.
  if (ConstValue *LVal = Semantics.getConstantValue(LHS)) {
    ConstValue::ConversionStatus Status;
    if (LVal->getAsBool(Status)) {
      return replaceBy(E, Semantics.getConstants().getTrue());
    } else {
      return RHS;
    }
  }

  if (ConstValue *RVal = Semantics.getConstantValue(RHS)) {
    ConstValue::ConversionStatus Status;
    if (RVal->getAsBool(Status)) {
      // Result of the expression is 'true' but side effect of the LHS must be
      // saved.
      Eliminator EL(Trans);
      Expr *SideEffect = EL.eliminateUnused(LHS, true);
      return Semantics.packSideEffect(LHS->getLocStart(), SideEffect,
                              replaceBy(E, Semantics.getConstants().getTrue()));
    } else {
      return LHS;
    }
  }

  return E;
}


Expr *Reducer::VisitConditionalOperator(ConditionalOperator *E) {
  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E)) {
    Changed = true;
    return Semantics.getConstantExpr(V, E->getLocStart(), true);
  }
  if (EV.seenError())
    return E;

  // Reduce arguments.
  Expr *CondExpr  = cast<Expr>(Visit(E->getCond()));
  Expr *TrueExpr  = cast<Expr>(Visit(E->getTrueExpr()));
  Expr *FalseExpr = cast<Expr>(Visit(E->getFalseExpr()));
  if (E->getCond() != CondExpr) {
    E->setCond(CondExpr);
    Changed = true;
  }
  if (E->getTrueExpr() != TrueExpr) {
    E->setTrueExpr(TrueExpr);
    Changed = true;
  }
  if (E->getFalseExpr() != FalseExpr) {
    E->setFalseExpr(FalseExpr);
    Changed = true;
  }

  // Process case when condition is a constant.
  if (ConstValue *CondVal = Semantics.getConstantValue(CondExpr)) {
    ConstValue::ConversionStatus Status;
    if (CondVal->getAsBool(Status)) {
      return TrueExpr;
    } else {
      return FalseExpr;
    }
  }

  return E;
}


Expr *Reducer::VisitBinaryConditionalOperator(BinaryConditionalOperator *E) {
  // Try evaluate at compile time.
  Evaluator EV(getSema());
  if (ConstValue *V = EV.evaluate(E)) {
    Changed = true;
    return Semantics.getConstantExpr(V, E->getLocStart(), true);
  }

  return E;
}

}
