//===--- RewritePhp.cpp - PHP code rewriter implementation ----------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// This file implements PHP code rewriter.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Rewrite/RewritePhp.h"
#include "phpfe/Sema/Runtime.h"
#include <sstream>
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


//------------------------------------------------------------------------------
// RewritePhp internals
//------------------------------------------------------------------------------

RewritePhp::RewritePhp(Sema &S) : Semantics(S), SM(S.getSourceManager()) {
  Rewrite.setSourceMgr(S.getSourceManager(), S.Context.getLangOpts());
}

std::string RewritePhp::getRewriteResult() {
  if (auto Buf = Rewrite.getRewriteBufferFor(SM.getMainFileID()))
    return std::string(Buf->begin(), Buf->end());

  // There are no changes
  return SM.getBufferData(SM.getMainFileID());
}

void RewritePhp::replaceText(SourceLocation Start, unsigned OrigLength,
                             StringRef Str) {
  if (!Rewrite.ReplaceText(Start, OrigLength, Str))
    return;

  Semantics.Diag(Start, diag::err_php_rewrite_failed);
}


void RewritePhp::insertText(SourceLocation Loc, StringRef Str, 
                            bool InsertAfter) {
  if (!Rewrite.InsertText(Loc, Str, InsertAfter))
    return;

  Semantics.Diag(Loc, diag::err_php_rewrite_failed);
}

//------------------------------------------------------------------------------

static SourceLocation getFunctionKeywordLocation(SourceManager &SM,
                                                 SourceLocation NameLoc) {
  const char *FnLocPtr = SM.getCharacterData(NameLoc);
  const char *FnLocEnd = FnLocPtr;
  do {
    --FnLocPtr;
    assert(FnLocPtr >=
      SM.getCharacterData(SM.getLocForStartOfFile(SM.getMainFileID())));
  } while (*FnLocPtr != 'f');
  assert(StringRef(FnLocPtr, FnLocEnd - FnLocPtr).startswith_lower("function"));

  return NameLoc.getLocWithOffset(FnLocPtr - FnLocEnd);
}


static unsigned getFunctionIndentation(SourceManager &SM,
                                       SourceLocation FnLoc) {
  auto Start = SM.getCharacterData(SM.getLocForStartOfFile(SM.getMainFileID()));
  auto FnPtr = SM.getCharacterData(FnLoc);
  auto Ptr = FnPtr;
  while (Ptr > Start && *Ptr != '\n' && *Ptr != '\r')
    --Ptr;
  return FnPtr - Ptr - 1;
}


static void emitTypeName(std::stringstream &Buf, QualType Ty, Sema &S) {
  if (Ty->isLValueReferenceType()) {
    emitTypeName(Buf, Ty->getPointeeType(), S);
    Buf << "&";
    return;
  }

  if (S.getRuntime().isRefBoxType(Ty)) {
    emitTypeName(Buf, Runtime::getParameterOfSpecialization(Ty), S);
    Buf << "&";
    return;
  }

  if (Runtime::isUniversalType(Ty))
    return;

  Buf << S.getPHPTypeName(Ty) << " ";
}


void RewritePhp::addFunctionComment(FunctionDecl *FD) {
  SourceLocation Loc = getFunctionKeywordLocation(SM, FD->getLocation());

  std::string Indent(getFunctionIndentation(SM, Loc), ' ');
  if (!Indent.empty())
    Loc = Loc.getLocWithOffset(-long(Indent.size()));
  std::stringstream Buf;

  Buf << Indent << "/**" << std::endl;
  Buf << Indent << " * " << FD->getNameAsString() << " description" << std::endl;
  Buf << Indent << " *" << std::endl;

  for (auto P : FD->parameters()) {
    Buf << Indent << " * @param ";
    emitTypeName(Buf, P->getType(), Semantics);
    Buf << "$" << FD->getNameAsString() << " " << std::endl;
  }
  
  Buf << Indent << " * @return ";
  emitTypeName(Buf, FD->getReturnType(), Semantics);
  Buf << std::endl;

  Buf << Indent << " */" << std::endl;

  insertText(Loc, Buf.str(), false);
}


void RewritePhp::updateFunctionComment(FunctionDecl *FD, 
                                       comments::FullComment *Comment) {
  //TODO: check comment
}

//------------------------------------------------------------------------------
// RewritePhp declarations
//------------------------------------------------------------------------------

bool RewritePhp::VisitFunctionDecl(FunctionDecl *D) {
  if (Semantics.getUnitFunction() == D)
    return true;

  auto Comment = 
    Semantics.Context.getCommentForDecl(D, &Semantics.getPreprocessor());

  if (!Comment)
    addFunctionComment(D);

  return true;
}

//------------------------------------------------------------------------------
// RewritePhp statements
//------------------------------------------------------------------------------

bool RewritePhp::VisitPhpEchoStmt(PhpEchoStmt *S) {
  assert(S->getNumArgs() > 0);

  if (S->isHtmlData())
    return true;

  SourceLocation StartLoc = S->getLocStart();
  SourceLocation EndLoc   = S->getArg(0)->getLocStart();
  const char *StartPtr = SM.getCharacterData(StartLoc);
  const char *EndPtr   = SM.getCharacterData(EndLoc);
  
  replaceText(StartLoc, EndPtr - StartPtr, "echo ");

  for (auto Arg : S->arguments())
    Visit(Arg);
  
  return true;
}

bool RewritePhp::Visit(Stmt *S) {
  //llvm_unreachable("unsupported statement in php code rewriter");
  return true;
}



} // namespace phpfe

