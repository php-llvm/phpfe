//===--- RewritePhp.cpp - PHP code rewriter implementation ----------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// This file implements PHP code rewriter.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/DeclGroup.h"
#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaConsumer.h"
#include "clang/Frontend/PhpOptions.h"
#include "clang/Rewrite/Frontend/ASTConsumers.h"
#include "phpfe/Rewrite/RewritePhp.h"
#include "phpfe/Sema/PhpSema.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;

class RewritePhp;

class RewritePhpConsumer : public SemaConsumer {
private:
  std::string InFile;
  raw_ostream *OS;
  phpfe::Sema *SemaPtr;

  std::unique_ptr<RewritePhp> RewriteT;

public:
  RewritePhpConsumer(std::string InFile, raw_ostream* OS)
  : InFile(InFile), OS(OS) {
  }

  ~RewritePhpConsumer() {}

  void InitializeSema(clang::Sema &S) {
    SemaPtr = (phpfe::Sema*)&S;
    RewriteT =
      make_unique<RewritePhp>(*SemaPtr);
  }

  bool HandleTopLevelDecl(DeclGroupRef D) override;
  void HandleTranslationUnit(ASTContext &Ctx) override;
  bool RewriteDeclaration(Decl *D);
};


//------------------------------------------------------------------------------
// RewritePhpConsumer implementation
//------------------------------------------------------------------------------

bool RewritePhpConsumer::HandleTopLevelDecl(DeclGroupRef DRG) {
  for (Decl *D : DRG) {
    if (D->isFromASTFile())
      continue;
    RewriteDeclaration(D);
  }

  return true;
}


// on error returns true
bool RewritePhpConsumer::RewriteDeclaration(Decl *D) {
  RewriteT->TraverseDecl(D);
  return false;
}


void RewritePhpConsumer::HandleTranslationUnit(ASTContext &Ctx) {
  *OS << RewriteT->getRewriteResult();
}


} // namespace phpfe

namespace clang {
std::unique_ptr<ASTConsumer>
CreatePhpRewriter(const std::string &InFile, raw_ostream *OS) {
  return 
    llvm::make_unique<phpfe::RewritePhpConsumer>(InFile, OS);
}
}
