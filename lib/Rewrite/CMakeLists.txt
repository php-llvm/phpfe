add_phpfe_library(phpfeRewrite
  RewritePhp.cpp
  RewritePhpConsumer.cpp

  LINK_LIBS
  phpfeSupport
)
