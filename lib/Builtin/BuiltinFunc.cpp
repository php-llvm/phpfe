//===--- BuiltinFunc.cpp ---- AST Builder and Semantic Analysis -----------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Builtin facility implementation.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Sema/PhpSema.h"
//------------------------------------------------------------------------------

using namespace llvm;
using namespace phpfe;

typedef llvm::DenseMap<StringRef, const BuiltinInfo *> BuiltinRegistryTy;

//------ Global variables ------------------------------------------------------
static std::unique_ptr<BuiltinRegistryTy> BuiltinRegistry;
//------------------------------------------------------------------------------


namespace phpfe {
namespace builtin {


//------------------------------------------------------------------------------
// FuncAttrs implementation
//------------------------------------------------------------------------------

void FuncAttrs::setFlags(AFunc A) {
  assert((A & ~(Generic | Pure | Disabled)) == 0 && "Unknown attribute");
  assert(!(Flags & A) && "Flag is already set");
  Flags |= A;
}

FuncAttrs::FuncAttrs(AFunc A) {
  Flags = Generic;
  setFlags(A);
}

FuncAttrs::FuncAttrs(std::initializer_list<AFunc> Attrs) {
  Flags = Generic;
  for (auto A : Attrs)
    setFlags(A);
}


//------------------------------------------------------------------------------
// ParamAttrs implementation
//------------------------------------------------------------------------------

void ParamAttrs::setFlags(AParam A) {
  assert((A & ~(Reference | Boxed | Variadic | Optional)) == 0
         && "Unknown argument type");
  assert(!(Flags & A) && "Flag is already set");
  Flags |= A;
}

ParamAttrs::ParamAttrs(AParam Flag) : Flags(0) {
  setFlags(Flag);
}

ParamAttrs::ParamAttrs(std::initializer_list<AParam> Attrs) {
  Flags = 0;
  for (auto A : Attrs)
    setFlags(A);
}


//------------------------------------------------------------------------------
// Types implementation
//------------------------------------------------------------------------------

Types::Types() {
  TypeSet.push_back(Runtime::getVoidType());
}

Types::Types(QualType Ty) {
  assert(!Ty.isNull());
  TypeSet.push_back(Ty);
}

Types::Types(std::initializer_list<QualType> Ts) {
  for (QualType T : Ts)
    add(T);
}

Types Types::add(QualType Ty) {
  assert(!Ty.isNull());
  TypeSet.push_back(Ty);
  return *this;
}

QualType Types::getType(unsigned I) const {
  assert(I < TypeSet.size());
  return TypeSet[I];
}


//------------------------------------------------------------------------------
// Convenience types function implementation
//------------------------------------------------------------------------------

QualType Object(StringRef Name) {
  if (Name.empty())
    return Runtime::getObjectType();

  llvm_unreachable("Unknown builtin object type");
}

QualType Resource() {
  return Runtime::getResourceType();
}

QualType Number() {
  return Runtime::getNumberBoxType();
}


//------------------------------------------------------------------------------
// Funcs implementation
//------------------------------------------------------------------------------
Default::Default() : Type(None) {
}

Default::Default(std::nullptr_t) : Type(Null) {
}

Default::Default(bool Val) : Type(Bool), BoolVal(Val) {
}

Default::Default(long Val) : Type(Long), LongVal(Val) {
}

Default::Default(double Val) : Type(Double), DoubleVal(Val) {
}

Default::Default(const char *Val) : Type(String), StrVal(Val) {
}

Expr *Default::buildDefaultExpr(Sema &S, SourceLocation Loc) const {
  switch (Type) {
  case Null:
    return S.ActOnPHPNullLiteral(Loc).get();

  case Bool:
    return S.ActOnCXXBoolLiteral(Loc, BoolVal
      ? tok::kw_true
      : tok::kw_false).get();

  case Long:
    return S.ActOnIntegerConstant(Loc, LongVal).get();

  case Double:
    return S.BuildDoubleLiteral(DoubleVal, Loc);

  case String:
    return S.BuildStringLiteral(StrVal, Loc);

  default:
    break;
  }
  llvm_unreachable("unsupported builtin default value kind");
}


//------------------------------------------------------------------------------
// Funcs implementation
//------------------------------------------------------------------------------

Funcs::Funcs(const char*Name) {
  Names.push_back(Name);
}

Funcs::Funcs(std::initializer_list<const char*> NameList) {
  for (auto Name : NameList)
    Names.push_back(Name);
}

StringRef Funcs::get(unsigned n) const {
  assert(n < Names.size());
  return Names[n];
}


//------------------------------------------------------------------------------
// RuntimeFuncs implementation
//------------------------------------------------------------------------------

RuntimeFuncs::RuntimeFuncs(const char*Name) {
  Names.push_back(Name);
}

RuntimeFuncs::RuntimeFuncs(std::initializer_list<const char*> NameList) {
  for (auto Name : NameList)
    Names.push_back(Name);
}

StringRef RuntimeFuncs::get(unsigned n) const {
  assert(n < Names.size());
  return Names[n];
}

} // namespace builtin
} // namespace phpfe


namespace phpfe {
using namespace builtin;

//------------------------------------------------------------------------------
// BuiltinInfo implementation
//------------------------------------------------------------------------------

BuiltinInfo::BuiltinInfo(const Funcs &F,
                         const FuncAttrs &FA,
                         const Types &RT,
                         const std::vector<Param> &P,
                         const builtin::RuntimeFuncs &RTF,
                         const builtin::Handlers &H)
  : Names(F), Attributes(FA), Returns(RT), Params(P), RuntimeNames(RTF),
  Handlers(H) {
  assert(Names.size() > 0);
  bool SeenDefault = false;
  bool SeenVariadic = false;
  unsigned OptionalCount = 0;
  for (auto P : Params) {
    // Variadic argument is single and is always the last one.
    assert(!SeenVariadic);
    // Optional arguments must not have default value.
    assert(!(P.isDefault() && P.isOptional()));
    // If argument with default value appears, next arguments cannot be
    // mandatory ones.
    assert(!(SeenDefault && P.isMandatory()));
    // if optional argument appears, next arguments cannot have default values.
    assert(!(P.isDefault() && OptionalCount));
    if (P.isDefault())
      SeenDefault = true;
    if (P.isOptional())
      ++OptionalCount;
    if (P.isVariadic()) {
      SeenVariadic = true;
      assert(!P.isDefault());
    }
  }
  if (RuntimeNames.size()) {
    assert(OptionalCount + 1 == RuntimeNames.size());
  }
}


unsigned BuiltinInfo::getNumRequiredArgs() const {
  unsigned Result = 0;
  for (auto P : Params) {
    if (P.A.isOptional() || P.Def.isSpecified())
      return Result;
    ++Result;
  }
  return Result;
}

unsigned BuiltinInfo::getNumDefaultArgs() const {
  unsigned Result = 0;
  for (auto P : Params) {
    if (P.Def.isSpecified())
      ++Result;
  }
  return Result;
}

bool BuiltinInfo::isVariadic() const {
  for (auto P : Params) {
    if (P.A.isVariadic())
      return true;
  }
  return false;
}


const BuiltinInfo *find_builtin_function(StringRef Name) {
  assert(BuiltinRegistry && "No builtin registry");
  auto Res = BuiltinRegistry->find(Name);
  if (Res == BuiltinRegistry->end())
    return nullptr;
  return Res->second;
}


void BuiltinInfo::record() {
  assert(BuiltinRegistry && "No builtin registry");
  for (unsigned I = 0; I < Names.size(); ++I) {
    StringRef FName = Names.get(I);
    assert(find_builtin_function(FName) == nullptr);
    (*BuiltinRegistry)[FName] = this;
  }
}


void init_builtin_functions() {
  assert(!BuiltinRegistry && "Builtin registry is already created");
  BuiltinRegistry = std::unique_ptr<BuiltinRegistryTy>(new BuiltinRegistryTy());
}

}
