//===--- BuiltinRegistry.cpp ----------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Implementation of builtin function registry.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "shared/constants.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Builtin/BuiltinHandlers.h"
//------------------------------------------------------------------------------


namespace phpfe {
using namespace builtin;


void register_builtin_functions() {
  init_builtin_functions();

  //---------------------------------------------
  // Start of function descriptor array.
  //---------------------------------------------

  static BuiltinInfo BuiltinFunctions [] = {

    { "abs",
      Pure,
      Returns(Number()),
      { { None,
          Universal(),
          "number" } },
      "abs",
      evaluateAbs
    },

    { "max",
      Pure,
      Returns(Universal()),
      { { None,                   { Array(), Universal()}, "value1" },
        { Optional,               Universal(),             "value2" },
        { { Optional, Variadic }, Universal(),             "rest"   } },
      { "max_array", "max", "max_argpack" },
      evaluateMax
    },

    { "min",
      Pure,
      Returns(Universal()),
      { { None,                   { Array(), Universal() },  "value1" },
        { Optional,               Universal(),               "value2" },
        { { Optional, Variadic }, Universal(),               "rest"   } },
      { "min_array", "min", "min_argpack" },
      evaluateMin
    },

    { "ceil",
      Pure,
      Returns(Double()),
      { { None, Double(), "value" } },
      "ceil",
      { semaRounding, evaluateRounding, reduceRounding }
    },

    { "floor",
      Pure,
      Returns(Double()),
      { { None, Double(), "value" } },
      "floor",
      { semaRounding, evaluateRounding, reduceRounding }
    },

    { "round",
      Pure,
      Returns(Double()),
      { { None, Double(), "value" },
        { None, Int(), "precision", Default(0L) },
        { None, Int(), "mode", Default((long)RoundingMode::HalfUp) } },
      "round",
      { evaluateRounding, reduceRound }
    },

    { "fmod",
      Pure,
      Returns(Double()),
      { { None, Double(),  "x" },
        { None, Double(),  "y" } },
      "fmod",
      { evaluateFmod, reduceFmod }
    },

    { "intdiv",
      Pure,
      Returns(Int()),
      { { None, Int(),  "x" },
        { None, Int(),  "y" } },
      "intdiv",
      { evaluateIntdiv, reduceIntdiv }
    },

    { "var_dump",
      Generic,
      Returns(),
      { { None,                   Universal(), "value" },
        { { Optional, Variadic }, Universal(), "rest"  } },
      {},
      legalizeVarDump
    },

    { "assert",
      Generic,
      Returns(),
      { { None, Universal(), "condition" } },
      {},
      legalizeAssert
    },

    { { "is_int", "is_integer", "is_long" },
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsInt, reduceIsInt, legalizeIsInt }
    },

    { { "is_float", "is_double", "is_real" },
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsFloat, reduceIsFloat, legalizeIsFloat }
    },
    
    { "is_numeric",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsNumeric, reduceIsNumeric, legalizeIsNumeric }
    },

    { "is_scalar",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsScalar, reduceIsScalar, legalizeIsScalar }
    },

    { "is_string",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsString, reduceIsString, legalizeIsString }
    },

    { "is_array",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsArray, reduceIsArray, legalizeIsArray }
    },

    { "is_object",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsObject, reduceIsObject, legalizeIsObject }
    },

    { "is_bool",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsBool, reduceIsBool, legalizeIsBool }
    },

    { "is_null",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsNull, reduceIsNull, legalizeIsNull }
    }, 

    { "is_resource",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateIsResource, reduceIsResource }
    },

    { "gettype",
      Pure,
      Returns(String()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateGetType, reduceGetType, legalizeGetType }
    },

    { "settype",
      Generic,
      Returns(Bool()),
      { { Reference, Universal(), "var"  },
        { None,      String(),    "type" } },
      {},
      { reduceSetType, legalizeSetType }
    },

    { "intval",
      Pure,
      Returns(Int()),
      { { None, Universal(), "expression" },
        { None, Int(), "base", Default(10L)} },
      { "intval" },
      { evaluateIntVal, reduceIntVal }
    },

    { {"floatval", "doubleval"},
      Pure,
      Returns(Double()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateFloatVal, reduceFloatVal }
    },

    { "boolval",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateBoolVal, reduceBoolVal }
    },

    { "strval",
      // Strictly speaking this function is not pure, as it may call __toString
      // method of objects, which can have side effects. However we use 'pure'
      // in looser sense, as a function that always evaluates the same result 
      // value given the same argument value(s). We can evaluate such functions
      // at compile - time, if arguments are constant.As objects are not
      // constants, this peculiarity does not break the function pureness.
      Pure, 
      Returns(String()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateStrVal, reduceStrVal }
    },

    { "empty",
      Pure,
      Returns(Bool()),
      { { None, Universal(), "expression" } },
      {},
      { evaluateEmpty, reduceEmpty, legalizeEmpty }
    },

    //-------------------------------------------
    // Server interface
    //-------------------------------------------

    { "header",
      Generic,
      Returns(),
      { { None,     String(), "hdr" },
        { None,     Bool(),   "replace", Default(true) },
        { Optional, Int(),    "http_response_code" } },
      { "add_header", "add_header_with_response_code" },
    },

    { "header_remove",
      Generic,
      Returns(),
      { { Optional, String(), "hdr" } },
      { "remove_all_headers", "remove_header" },
    },

    { "headers_sent",
      Generic,
      Returns(),
      { { { Reference, Optional }, String(), "file" },
        { { Reference, Optional }, Int(),    "line" } },
      { "headers_sent", "headers_sent_file", "headers_sent_file_line" },
    },

    { "headers_list",
      Generic,
      Returns(Array()),
      {},
      { "headers_list" },
    },

    { "header_register_callback",
      Generic,
      Returns(Bool()),
      { { None, Callable(), "callback" } },
      { "set_header_register_callback" },
    },

    { "http_response_code",
      Generic,
      Returns(Int(), Bool()),
      { { Optional, Int(), "response_code" } },
      { "get_http_response_code", "set_http_response_code" },
    },

  };

  //---------------------------------------------
  // End of function descriptor array.
  //---------------------------------------------

  for (BuiltinInfo &R : BuiltinFunctions)
    R.record();
}


} // namespace phpfe
