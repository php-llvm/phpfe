//===--- Decorator.cpp ------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Component that deduces type of expressions.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Analyzer/Decorator.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "conversion/number_recognizer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


QualType Decorator::decorateByType(Expr *E) {
  return Visit(E);
}


/// \brief Get constant value associated with the expression or tries to
/// evaluate it.
///
/// \returns Pointer to the evaluated constant value, if the expression can be
/// evaluated at compile time, zero if cannot or error flag if error occurred
/// at evaluation.
///
ValueResult Decorator::getConstantValue(Expr *E) {
  ConstValue *CV = Semantics.getConstantValue(E);
  if (!CV) {
    Evaluator EV(Semantics);
    CV = EV.evaluate(E);
    if (!CV)
      return ValueResult(EV.seenError());
  }
  return CV;
}


/// \brief Converts the given expression, which must have 'null' type, to the
/// specified type.
///
/// \returns New expression that should be used instead of the given. If such
/// replacement is not needed, returns null pointer.
///
Expr *Decorator::transformNull(QualType TargetType, Expr *E) {
  assert(E->getType()->isVoidType());
  assert(!TargetType.isNull());

  const char *target = nullptr;
  ConstValue *Replacement = nullptr;
  if (isStringType(TargetType)) {
    target = "empty string";
    Replacement = Semantics.getConstants().getEmptyString();
  } else if (TargetType->isBooleanType()) {
    target = "false";
    Replacement = Semantics.getConstants().getFalse();
  } else {
    assert(isNumberType(TargetType));
    target = "zero";
    if (isFloatingType(TargetType))
      Replacement = Semantics.getConstants().getFloatZero();
    else
      Replacement = Semantics.getConstants().getZero();
  }
  Semantics.Diag(E->getLocStart(), diag::warn_php_implicit_null_cvt)
      << target << E->getSourceRange();

  Expr *ReplacementExpr = Semantics.getConstantExpr(Replacement,
                                                    E->getLocStart(), true);
  if (isa<PhpNullLiteral>(E))
    return ReplacementExpr;

  // Do not throw away the argument of void type. It may be a function
  // returning void and this function may have side effects. Let other
  // components to make the removal if possible.
  return Semantics.packSideEffect(E->getLocStart(), E, ReplacementExpr);
}


/// \brief Casts the given expression to boolean type.
///
/// \returns New expression that should be used instead of the given. If such
/// replacement is not needed, returns null pointer.
///
Expr *Decorator::transformToBool(Expr *E) {
  QualType ExprType = E->getType();
  if (isBooleanType(ExprType))
    return nullptr;
  Semantics.Diag(E->getLocStart(), diag::warn_php_implicit_cvt_to_bool)
      << Semantics.getPHPTypeName(ExprType) << E->getSourceRange();
  return Semantics.createImplicitCast(getBoolType(), const_cast<Expr *>(E));
}


/// \brief Casts the given expression to floating type.
///
/// \returns New expression that should be used instead of the given. If such
/// replacement is not needed, returns null pointer.
///
Expr *Decorator::transformToFloat(Expr *E) {
  QualType ExprType = E->getType();
  if (isFloatingType(ExprType))
    return nullptr;
  if (isStringType(ExprType)) {
    // If the string is constant, we should catch conversion problems.
    Expr *Replacement = transformStringToNumber(E);
    assert(Replacement);  // Types are definitely different - must have cast.
    return Replacement;
  }
  if (!isNumberType(ExprType)) {
    Semantics.Diag(E->getLocStart(), diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(ExprType) << "double" << E->getSourceRange();
  }
  return Semantics.createImplicitCast(getDoubleType(), const_cast<Expr *>(E));
}


/// \brief Casts the given expression to integer type.
///
/// \returns New expression that should be used instead of the given. If such
/// replacement is not needed, returns null pointer.
///
Expr *Decorator::transformToInteger(Expr *E) {
  if (isIntegerType(E->getType()))
    return nullptr;
  if (isStringType(E->getType())) {
    // If the string is constant, we should catch conversion problems.
    Expr *Replacement = transformStringToNumber(E);
    assert(Replacement);  // Types are definitely different - must have cast.
    return Replacement;
  }
  if (E->getType()->isVoidType())
    return transformNull(getIntValueType(), E);
  if (E->getType() != getNumberBoxType()) {
    Semantics.Diag(E->getLocStart(), diag::warn_php_implicit_type_conversion)
        << Semantics.getPHPTypeName(E->getType()) << "integer"
        << E->getSourceRange();
  }
  return Semantics.createImplicitCast(getIntValueType(), const_cast<Expr *>(E));
}


/// \brief Casts the given string expression to number type.
///
/// \returns New expression that should be used instead of the given or nullptr
/// in the case of error.
///
Expr *Decorator::transformStringToNumber(Expr *E) {
  assert(isStringType(E->getType()));
  SourceLocation Loc = E->getLocStart();

  // When the argument is a string that represents a number, we can determine
  // resulting type if the string is constant.
  ValueResult CValue = getConstantValue(E);
  if (CValue.isInvalid())
    return nullptr;
  if (CValue.isUnset()) {
    // Cannot evaluate at compile time - cast to generic number type.
    Semantics.Diag(Loc, diag::warn_php_implicit_from_string)
        << "number" << E->getSourceRange();
    return Semantics.createImplicitCast(getNumberBoxType(),
                                        const_cast<Expr *>(E));
  }
  ConstValue *CV = CValue.get();

  bool HasExtra;
  if (CV->isStringFloat(HasExtra)) {
    Semantics.Diag(Loc, diag::warn_php_implicit_from_string)
      << "double" << E->getSourceRange();
    ConstValue::ConversionStatus Status;
    APFloat FVal = CV->getAsFloat(Status);
    Semantics.diagnoseConversion(Status, Loc);
    return replaceByFloat(FVal, Loc);
  }

  if (CV->isStringInteger(HasExtra)) {
    ConstValue::ConversionStatus Status;
    APInt IntVal = CV->getAsInt(Status);
    if (Status == ConstValue::IntTooLarge)
      Semantics.reportTooLargeInteger(Loc, false);
    else if (Status == ConstValue::IntTooLargeNegative)
      Semantics.reportTooLargeInteger(Loc, true);
    else
      Semantics.diagnoseConversion(Status, Loc);

    if (IntVal.getActiveBits() > getIntValueWidth() ||
        (Status == ConstValue::IntTooLarge) ||
        (Status == ConstValue::IntTooLargeNegative) ||
        IntVal.isMinSignedValue()) {
      APFloat FVal = CV->getAsFloat(Status);
      Semantics.Diag(Loc, diag::warn_php_implicit_from_string)
          << "double" << E->getSourceRange();
      return replaceByFloat(FVal, Loc);
    }

    if (Status == ConstValue::OK) {
      Semantics.Diag(Loc, diag::warn_php_implicit_from_string)
        << "integer"; //TODO: << E->getSourceRange();
      return replaceByInt(IntVal, Loc);
    }
  }

  // Not a number string. Implicitly convert it to zero.
  Semantics.Diag(Loc, diag::warn_php_not_a_number); //TODO: << E->getSourceRange();
  return replaceByInt(getZeroInteger(), Loc);
}


/// \brief Makes type decoration common for all binary operators.
///
/// \param Type Is assigned value of deduced type if type decoration is done.
/// \returns True if decoration is done and no more treatment is required.
///
bool Decorator::processBinaryOperator(QualType &Type, BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();

  if (isObjectType(LHSType) || isObjectType(RHSType) ||
      isUniversalType(LHSType) || isUniversalType(RHSType)) {
    // If any of operands is an object, operation is made by a call to
    // corresponding object method. Result may be of any type in this case.
    // The same is true for universal type as well, as it may contain object.
    Type = getClassBoxType();
    return true;
  }

  return false;
}


/// \brief Makes type decoration common for all binary operators that do not
/// accept array arguments.
///
/// \param Type Is assigned value of deduced type if type decoration is done or
///             null type if an error was detected.
/// \returns True if decoration is finished and no more treatment is required.
///
bool Decorator::processNonArrayOperator(QualType &Type, BinaryOperator *E) {
  if (processBinaryOperator(Type, E))
    return true;

  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();

  // Filter out array types.
  if (isArrayType(LHSType) || isArrayType(RHSType)) {
    bool LHSIsArray = isArrayType(LHSType);
    SourceRange Rng = LHSIsArray ? LHS->getSourceRange()
                                 : RHS->getSourceRange();
    Semantics.Diag(E->getExprLoc(), diag::err_php_wrong_array_operand) << Rng;
    Type = QualType();
    return true;
  }

  return false;
}


void Decorator::processBitwiseOperator(QualType &Type, BinaryOperator *E) {
  if (processNonArrayOperator(Type, E))
    return;

  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();

  // If both arguments are strings, the operation is made on bit arrays,
  // the result also must be a string.
  if (isStringType(LHSType) && isStringType(RHSType)) {
    Type = getStringBoxType();
    return;
  }

  // If one of arguments is not a string, convert both arguments to integers.
  if (!isIntegerType(LHSType)) {
    if (Expr *Replacement = transformToInteger(LHS))
      E->setLHS(Replacement);
  }
  if (!isIntegerType(RHSType)) {
    if (Expr *Replacement = transformToInteger(RHS))
      E->setRHS(Replacement);
  }

  Type = getIntValueType();
}


void Decorator::processIntegerOperator(QualType &Type, BinaryOperator *E) {
  if (processNonArrayOperator(Type, E))
    return;

  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();

  if (!isIntegerType(LHSType)) {
    if (Expr *Replacement = transformToInteger(LHS))
      E->setLHS(Replacement);
  }
  if (!isIntegerType(RHSType)) {
    if (Expr *Replacement = transformToInteger(RHS))
      E->setRHS(Replacement);
  }

  Type = getIntValueType();
}


void Decorator::processArithmeticOperator(QualType &Type, BinaryOperator *E) {
  if (processNonArrayOperator(Type, E))
    return;

  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();

  bool NegativeBoundary;
  if (Expr *Replacement = transformNumericExpr(LHSType, LHS, NegativeBoundary)) {
    LHS = Replacement;
    E->setLHS(Replacement);
  }
  if (Expr *Replacement = transformNumericExpr(RHSType, RHS, NegativeBoundary)) {
    RHS = Replacement;
    E->setRHS(Replacement);
  }

  if (LHSType->isFloatingType()) {
    if (!RHSType->isFloatingType()) {
      RHS = Semantics.createImplicitCast(getDoubleType(), RHS);
      E->setRHS(RHS);
    }
    Type = getDoubleType();
    return;
  }

  if (RHSType->isFloatingType()) {
    LHS = Semantics.createImplicitCast(getDoubleType(), LHS);
    E->setLHS(LHS);
    Type = getDoubleType();
    return;
  }

  // If C++ semantics is on and arguments are integers, use integer result.
  if (LHSType->isIntegerType() && RHSType->isIntegerType()) {
    if (Semantics.getPhpOptions().CxxArith)
      Type = getIntValueType();
    // Otherwise use generic numeric type.
  }

  Type = getNumberBoxType();
}


void Decorator::processCompareOperator(QualType &Type, BinaryOperator *E) {
  if (processBinaryOperator(Type, E))
    return;

  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();

  if (E->getOpcode() == BO_PhpSpaceship)
    Type = getIntValueType();
  else
    Type = getBoolType();

  if (areTypesEquivalent(LHSType, RHSType)) {
    // If arguments are string that represent numbers, the comparison will be
    // made on corresponding numbers. Issue appropriate warning in this case.
    if (isStringType(LHSType)) {
      // Try evaluating arguments as constants.
      ValueResult LValue = getConstantValue(LHS);
      if (LValue.isInvalid()) {
        Type = QualType();
        return;
      }
      ValueResult RValue = getConstantValue(RHS);
      if (RValue.isInvalid()) {
        Type = QualType();
        return;
      }

      // If any of arguments is not a constant string, we cannot evaluate
      // necessary conversions, f.e.
      //     $a == '123';
      // if $a is '456.7', RHS would be converted to float and comparison is
      // numerical, but if $a is 'qwe', comparison is alphabetical.
      if (LValue.isUnset() || RValue.isUnset())
        return;

      // If both arguments represent numeric strings, comparison is numerical.
      conversion::NumberRecognizer<> LRecog;
      StringRef LStr = LValue.get()->getString();
      LRecog.recognize(LStr.data(), LStr.size());
      if (LRecog.success()) {
        conversion::NumberRecognizer<> RRecog;
        StringRef RStr = RValue.get()->getString();
        RRecog.recognize(RStr.data(), RStr.size());
        if (RRecog.success()) {
          QualType ArgType;
          if (LRecog.is_float() || RRecog.is_float())
            ArgType = getDoubleType();
          else
            ArgType = getIntValueType();

          if (ArgType == getDoubleType()) {
            if (Expr *Replacement = transformToFloat(LHS))
              E->setLHS(Replacement);
            if (Expr *Replacement = transformToFloat(RHS))
              E->setRHS(Replacement);
          } else {
            if (Expr *Replacement = transformToInteger(LHS))
              E->setLHS(Replacement);
            if (Expr *Replacement = transformToInteger(RHS))
              E->setRHS(Replacement);
          }
        }
      }
    }
    return;
  }

  if (isArrayType(LHSType) || isArrayType(RHSType)) {
    Semantics.Diag(E->getExprLoc(), diag::warn_php_array_compare)
        << LHS->getSourceRange() << RHS->getSourceRange();
    // Do not change AST to avoid double message. Reducer must resolve
    // comparison of array and non-array at compile time.
    return;
  }

  // Null is converted to boolean, as well as the other argument.
  if (LHSType->isVoidType() || RHSType->isVoidType()) {
    // null + string => '' + string
    if (isStringType(LHSType) || isStringType(RHSType)) {
      if (LHSType->isVoidType()) {
        if (Expr *Replacement = transformNull(getStringBoxType(), LHS))
          E->setLHS(Replacement);
      }
      if (RHSType->isVoidType()) {
        if (Expr *Replacement = transformNull(getStringBoxType(), RHS))
          E->setRHS(Replacement);
      }
      return;
    }
    // null + type => false + type
    if (LHSType->isVoidType()) {
      if (Expr *Replacement = transformNull(getBoolType(), LHS))
        E->setLHS(Replacement);
    }
    if (RHSType->isVoidType()) {
      if (Expr *Replacement = transformNull(getBoolType(), RHS))
        E->setRHS(Replacement);
    }
    return;
  }

  // If one argument is bool, the other is converted to bool too.
  if (isBooleanType(LHSType)) {
    if (Expr *Replacement = transformToBool(RHS))
      E->setRHS(Replacement);
    return;
  }
  if (isBooleanType(RHSType)) {
    if (Expr *Replacement = transformToBool(LHS))
      E->setLHS(Replacement);
    return;
  }

  // At least one of arguments must be of numeric. The other one is casted to
  // that type.

  // A string may be converted to integer or float.
  if (isStringType(LHSType)) {
    Expr *Replacement = transformStringToNumber(LHS);
    if (!Replacement)
      return;  // Error
    LHS = Replacement;
    LHSType = LHS->getType();
    E->setLHS(Replacement);
  }
  if (isStringType(RHSType)) {
    Expr *Replacement = transformStringToNumber(RHS);
    if (!Replacement)
      return;  // Error
    RHS = Replacement;
    RHSType = RHS->getType();
    E->setRHS(Replacement);
  }

  if (isNumberBoxType(LHSType) || isNumberBoxType(RHSType))
    return;

  if (isFloatingType(LHSType)) {
    if (Expr *Replacement = transformToFloat(RHS))
      E->setRHS(Replacement);
    return;
  }
  if (isFloatingType(RHSType)) {
    if (Expr *Replacement = transformToFloat(LHS))
      E->setLHS(Replacement);
    return;
  }

  if (isIntegerType(LHSType)) {
    if (Expr *Replacement = transformToInteger(RHS))
      E->setRHS(Replacement);
    return;
  }
  if (isIntegerType(RHSType)) {
    if (Expr *Replacement = transformToInteger(LHS))
      E->setLHS(Replacement);
    return;
  }

  llvm_unreachable("unexpected type");
}


Expr *Decorator::transformNumericExpr(QualType &Type, Expr *E,
                                      bool &NegativeBoundary) {
  NegativeBoundary = false;
  QualType ExprType = E->getType();
  SourceLocation Loc = E->getLocStart();

  // Null is implicitly converted to zero.
  if (ExprType->isVoidType()) {
    Semantics.Diag(Loc, diag::warn_php_implicit_null_cvt)
        << "zero" << E->getSourceRange();
    // Do not throw away argument of void type. It may be a function that
    // returns void and this function may have side effects. Let other
    // components to make the removal if possible.
    Type = getIntValueType();
    return Semantics.createImplicitCast(getIntValueType(),
                                        const_cast<Expr *>(E));
  }

  if (isFloatingType(ExprType)) {
    Type = getDoubleType();
    return nullptr;
  }

  if (isBooleanType(ExprType)) {
    Semantics.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << "boolean" << "integer" << E->getSourceRange();
    Type = getIntValueType();
    return nullptr;
  }

  if (isIntegerType(ExprType)) {
    Type = getIntValueType();
    return nullptr;
  }

  if (isNumberType(ExprType)) { // NumberBox
    Type == ExprType;
    return nullptr;
  }

  // String may contain integer or float value. Only if argument can be
  // calculated at compile time we can determine the type more precisely.
  assert(isStringType(ExprType));
  Expr *Result = transformStringToNumber(const_cast<Expr*>(E));
  Type = Result->getType();
  return Result;
}


bool Decorator::processCompounsAssignment(QualType &Type,
                                          CompoundAssignOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = unboxType(LHS->getType());
  QualType RHSType = unboxType(RHS->getType());
  BinaryOperatorKind Kind = E->getOpcode();

  // If LHS is of object or universal type, we cannot say anything about the
  // intermediate type or restrictions on RHS. The same is true for RHS as well.
  if (isObjectType(LHSType) || isUniversalType(LHSType) ||
      isObjectType(RHSType) || isUniversalType(RHSType)) {
    E->setComputationLHSType(getClassBoxType());
    E->setComputationResultType(getClassBoxType());
    Type = getClassBoxType();
    return true;
  }

  // Filter out array types, they make sense only for addition.
  if (isArrayType(LHSType)) {
    if (isArrayType(RHSType)) {
      if (Kind == BO_AddAssign) {
        E->setComputationLHSType(getArrayType());
        E->setComputationResultType(getArrayType());
        Type = getArrayType();
        return true;
      }
      Semantics.Diag(E->getExprLoc(), diag::err_php_wrong_array_operand)
          << LHS->getSourceRange();
      Type = QualType();
      return true;
    }
    if (Kind == BO_AddAssign)
      Semantics.Diag(E->getExprLoc(), diag::err_php_array_addition)
          << RHS->getSourceRange();
    else
      Semantics.Diag(E->getExprLoc(), diag::err_php_wrong_array_operand)
          << LHS->getSourceRange();
    Type = QualType();
    return true;
  }
  if (isArrayType(RHSType)) {
    if (Kind == BO_AddAssign)
      Semantics.Diag(E->getExprLoc(), diag::err_php_array_addition)
          << LHS->getSourceRange();
    else
      Semantics.Diag(E->getExprLoc(), diag::err_php_wrong_array_operand)
          << RHS->getSourceRange();
    Type = QualType();
    return true;
  }

  // Try to evaluate RHS.
  ValueResult Res = getConstantValue(RHS);
  if (Res.isUsable())
    Semantics.addConstantExpr(RHS, Res.get());
  return false;
}


bool Decorator::processArithmeticAssignment(QualType &Type,
                                           CompoundAssignOperator *E) {
  if (processCompounsAssignment(Type, E))
    return true;

  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = unboxType(LHS->getType());
  QualType RHSType = unboxType(RHS->getType());

  // If RHS is of void type, replace it with integer zero.
  if (RHSType->isVoidType()) {
    QualType NewRHSType;
    if (isNumberType(LHSType))
      NewRHSType = LHSType;
    else if (isBooleanType(LHSType))
      NewRHSType = getIntValueType();
    else if (isStringType(LHSType))
      NewRHSType = getNumberBoxType();
    else
      llvm_unreachable("unexpected type");
    RHS = transformNull(NewRHSType, RHS);
    E->setRHS(RHS);
    RHSType = NewRHSType;
  }

  // If any of arguments is float, result and intermediate result are floats.
  if (isFloatingType(LHSType) || isFloatingType(RHSType)) {
    Type = getDoubleType();
    if (!isFloatingType(RHSType)) {
      RHS = transformToFloat(RHS);
      E->setRHS(RHS);
    } else if (!isFloatingType(LHSType) &&
               !isFloatingType(E->getComputationLHSType())) {
      Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_conversion_compass)
          << Semantics.getPHPTypeName(LHSType)
          << Semantics.getPHPTypeName(Type)
          << LHS->getSourceRange();
    }
    E->setComputationLHSType(Type);
    E->setComputationResultType(Type);
    return false;
  }

  // String may contain integer or float value.
  if (isStringType(RHSType)) {
    RHS = transformStringToNumber(RHS);
    E->setRHS(RHS);
    Type = getNumberBoxType();
    E->setComputationResultType(Type);
    // Do not return, need to check LHS as well.
  }
  if (isStringType(LHSType)) {
    Type = getNumberBoxType();
    if (!isNumberType(E->getComputationLHSType()))
      Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_conversion_compass)
          << "string"
          << Semantics.getPHPTypeName(Type)
          << LHS->getSourceRange();
    E->setComputationLHSType(Type);
    E->setComputationResultType(Type);
    return false;
  }

  if (isBooleanType(LHSType)) {
    if (isBooleanType(RHSType)) {
      // Overflow cannot occur.
      Type = getIntValueType();
      RHS = transformToInteger(RHS);
      E->setRHS(RHS);
    } else if (isIntegerType(RHSType) && Semantics.getPhpOptions().CxxArith) {
      Type = getIntValueType();
    } else {
      Type = getNumberBoxType();
    }
    if (!isNumberType(E->getComputationLHSType()))
      Semantics.Diag(E->getExprLoc(),
                     diag::warn_php_implicit_conversion_compass)
          << Semantics.getPHPTypeName(LHSType)
          << Semantics.getPHPTypeName(Type)
          << LHS->getSourceRange();
    E->setComputationLHSType(Type);
    E->setComputationResultType(Type);
    return false;
  }

  assert(isIntegerType(LHSType));
  if (Semantics.getPhpOptions().CxxArith)
    Type = getIntValueType();
  else
    Type = getNumberBoxType();
  E->setComputationLHSType(Type);
  E->setComputationResultType(Type);

  return false;
}


QualType Decorator::VisitExpr(Expr *E) {
  return E->getType();
}


QualType Decorator::VisitCallExpr(CallExpr *E) {
  if (FunctionDecl *FDecl = dyn_cast_or_null<FunctionDecl>(E->getCalleeDecl()))
    return FDecl->getReturnType();

  Expr *Callee = E->getCallee();
  if (const StringLiteral *S = dyn_cast<StringLiteral>(Callee)) {
    auto *BI = find_builtin_function(S->getString());
    if (BI && BI->Handlers.DecoratorH)
      return BI->Handlers.DecoratorH(*this, *BI, E);
    if (BI->Returns.size() == 1)
      return BI->Returns.getType(0);
  }

  return getClassBoxType();
}


QualType Decorator::VisitBinAddAssign(CompoundAssignOperator *E) {
  QualType Type;
  processArithmeticAssignment(Type, E);
  return Type;
}


QualType Decorator::VisitBinSubAssign(CompoundAssignOperator *E) {
  QualType Type;
  processArithmeticAssignment(Type, E);
  return Type;
}


QualType Decorator::VisitBinMulAssign(CompoundAssignOperator *E) {
  QualType Type;
  processArithmeticAssignment(Type, E);
  return Type;
}


QualType Decorator::VisitBinDivAssign(CompoundAssignOperator *E) {
  QualType Type;
  if (processArithmeticAssignment(Type, E))
    return Type;

  if (!Type.isNull()) {
    // Diagnose zero divide early, so that it will be emitted by semantic, not
    // reducer. In the latter case we have chance don't see the message at all,
    // as reducer may be skipped if parser founds errors.
    Expr *RHS = E->getRHS();
    if (ConstValue *CV = Semantics.getConstantValue(RHS)) {
      if (CV->isZero()) {
        Semantics.Diag(E->getExprLoc(), diag::err_php_division_by_zero)
          << E->getRHS()->getSourceRange();
        return QualType();
      }
    }
    // Void value at RHS will be converted into zero.
    if (RHS->getType()->isVoidType()) {
      Semantics.Diag(E->getExprLoc(), diag::err_php_division_by_zero)
        << E->getRHS()->getSourceRange();
      return QualType();
    }
    // Result of division may be a floating number if integer operands divide
    // with remainder.
    if (isIntegerType(E->getComputationLHSType()) &&
        isIntegerType(E->getRHS()->getType()))
      E->setComputationResultType(getNumberBoxType());
  }
  return Type;
}


QualType Decorator::VisitBinRemAssign(CompoundAssignOperator *E) {
  QualType Type;
  if (processArithmeticAssignment(Type, E))
    return Type;

  // Result of remainder operator is always an integer.
  E->setComputationResultType(getIntValueType());

  if (!Type.isNull()) {
    // Diagnose zero divide early.
    Expr *RHS = E->getRHS();
    if (ConstValue *CV = Semantics.getConstantValue(RHS)) {
      if (CV->isZero()) {
        Semantics.Diag(E->getExprLoc(), diag::err_php_division_by_zero)
          << E->getRHS()->getSourceRange();
        return QualType();
      }
    }
  }
  return Type;
}


QualType Decorator::VisitBinShlAssign(CompoundAssignOperator *E) {
  QualType Type;
  if (processArithmeticAssignment(Type, E))
    return Type;

  // Result of shift operator is always an integer.
  E->setComputationResultType(getIntValueType());

  if (!Type.isNull()) {
    Expr *RHS = E->getRHS();
    if (ConstValue *CV = Semantics.getConstantValue(RHS)) {
      if (!CV->isInt())
        CV = Semantics.getConstantValue(CV, getIntValueType(), RHS->getLocStart());
      APInt IVal = CV->getInt();
      if (IVal.isNegative()) {
        Semantics.Diag(E->getExprLoc(), diag::err_php_negative_bit_shift)
          << RHS->getSourceRange();
        return QualType();
      }
    }
  }
  return Type;
}


QualType Decorator::VisitBinShrAssign(CompoundAssignOperator *E) {
  QualType Type;
  if (processArithmeticAssignment(Type, E))
    return Type;

  // Result of shift operator is always an integer.
  E->setComputationResultType(getIntValueType());

  if (!Type.isNull()) {
    Expr *RHS = E->getRHS();
    if (ConstValue *CV = Semantics.getConstantValue(RHS)) {
      if (!CV->isInt())
        CV = Semantics.getConstantValue(CV, getIntValueType(), RHS->getLocStart());
      APInt IVal = CV->getInt();
      if (IVal.isNegative()) {
        Semantics.Diag(E->getExprLoc(), diag::err_php_negative_bit_shift)
          << RHS->getSourceRange();
        return QualType();
      }
    }
  }
  return Type;
}


QualType Decorator::VisitBinPhpConcatAssign(CompoundAssignOperator *E) {
  QualType Type;
  if (processCompounsAssignment(Type, E))
    return Type;

  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = unboxType(LHS->getType());
  QualType RHSType = unboxType(RHS->getType());

  // If RHS is of void type, replace it with empty string.
  if (RHSType->isVoidType()) {
    RHSType = getIntValueType();
    RHS = transformNull(getStringBoxType(), RHS);
    E->setRHS(RHS);
  }

  Type = getStringBoxType();
  E->setComputationLHSType(Type);
  E->setComputationResultType(Type);
  return Type;
}


QualType Decorator::VisitBinAssign(BinaryOperator *E) {
  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();
  assert(!LHSType->isVoidPointerType());

  // If either of sides has universal or object type, result type cannot be
  // determined at compile time: assignment and getting value of an object may
  // be determined by handlers 'set' and 'get', they may return values of
  // any type.
  if (isObjectType(LHSType) || isObjectType(RHSType) ||
      isUniversalType(LHSType) || isUniversalType(RHSType)) {
    return getClassBoxType();
  }

  if (areTypesEquivalent(LHSType, RHSType))
    return LHSType;

  // Disallow implicit cast to array.
  if (isArrayType(LHSType)) {
    Semantics.Diag(E->getExprLoc(), diag::err_php_cvt_to_array)
      << Semantics.getPHPTypeName(RHSType) << RHS->getSourceRange();
    return QualType();
  }

  // Disallow implicit cast from array, only conversion to bool is accepted.
  if (isArrayType(RHSType)) {
    if (!isBooleanType(LHSType)) {
      Semantics.Diag(E->getExprLoc(), diag::err_php_cvt_from_array)
          << Semantics.getPHPTypeName(LHSType) << RHS->getSourceRange();
      return QualType();
    }
    Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_cvt_to_bool)
        << Semantics.getPHPTypeName(RHSType) << RHS->getSourceRange();
    E->setRHS(Semantics.createImplicitCast(getBoolType(),
                                                      const_cast<Expr *>(RHS)));
    return getBoolType();
  }

  // NULL can be assigned to any remaining type.
  if (RHSType->isVoidType()) {
    const char *target = nullptr;
    if (isStringType(LHSType))
      target = "empty string";
    else if (isBooleanType(LHSType))
      target = "false";
    else
      target = "zero";
    Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_null_cvt)
        << target << RHS->getSourceRange();
    E->setRHS(Semantics.createImplicitCast(LHSType,
                                                      const_cast<Expr *>(RHS)));
    return LHSType;
  }

  // Any remaining type can be casted to string.
  if (isStringType(LHSType)) {
    Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_to_string)
        << Semantics.getPHPTypeName(RHSType) << RHS->getSourceRange();
    E->setRHS(Semantics.createImplicitCast(getStringBoxType(),
                                                      const_cast<Expr *>(RHS)));
    return getStringType();
  }
  assert(LHSType->isRealType());

  // String can be converted to any numeric type, but it requires warning.
  if (isStringType(RHSType)) {
    Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_from_string)
        << Semantics.getPHPTypeName(LHSType) << RHS->getSourceRange();
    E->setRHS(Semantics.createImplicitCast(LHSType,
                                                      const_cast<Expr *>(RHS)));
    return LHSType;
  }

  // Warn when cast to narrower type (number->bool, float->int).
  if (isBooleanType(LHSType)) {
    Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_cvt_to_bool)
        << Semantics.getPHPTypeName(RHSType) << RHS->getSourceRange();
    E->setRHS(Semantics.createImplicitCast(LHSType,
                                                      const_cast<Expr *>(RHS)));
  } else if (isFloatingType(RHSType)) {
    assert(isIntegerType(LHSType));
    Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_float_to_int)
        << RHS->getSourceRange();
    E->setRHS(Semantics.createImplicitCast(LHSType,
                                                      const_cast<Expr *>(RHS)));
  }
  return LHSType;
}


QualType Decorator::VisitCompoundAssignOperator(CompoundAssignOperator *E) {
  QualType Type;
  processCompounsAssignment(Type, E);
  return Type;
}


QualType Decorator::VisitBinAdd(BinaryOperator *E) {
  QualType Type;
  if (processBinaryOperator(Type, E))
    return Type;

  Expr *LHS = E->getLHS();
  Expr *RHS = E->getRHS();
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();

  if ((isArrayType(LHSType) && isArrayType(RHSType)))
    return getArrayPtrType();

  if ((isArrayType(LHSType) || isArrayType(RHSType))) {
    int LHSIsArray = isArrayType(LHSType);
    SourceRange Rng = LHSIsArray ? LHS->getSourceRange()
                                 : RHS->getSourceRange();
    Semantics.Diag(E->getExprLoc(), diag::err_php_array_addition) << Rng;
    return QualType();
  }

  processArithmeticOperator(Type, E);
  return Type;
}


QualType Decorator::VisitBinSub(BinaryOperator *E) {
  QualType Type;
  processArithmeticOperator(Type, E);
  return Type;
}


QualType Decorator::VisitBinMul(BinaryOperator *E) {
  QualType Type;
  processArithmeticOperator(Type, E);
  return Type;
}


QualType Decorator::VisitBinDiv(BinaryOperator *E) {
  QualType Type;
  if (processNonArrayOperator(Type, E))
    return Type;
  return getNumberBoxType();
}


QualType Decorator::VisitBinRem(BinaryOperator *E) {
  // mod is integer operation.
  QualType Type;
  processIntegerOperator(Type, E);
  return Type;
}


QualType Decorator::VisitBinShl(BinaryOperator *E) {
  QualType Type;
  processIntegerOperator(Type, E);
  return Type;
}


QualType Decorator::VisitBinShr(BinaryOperator *E) {
  QualType Type;
  processIntegerOperator(Type, E);
  return Type;
}


QualType Decorator::VisitBinAnd(BinaryOperator *E) {
  QualType Result;
  processBitwiseOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinOr(BinaryOperator *E) {
  QualType Result;
  processBitwiseOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinXor(BinaryOperator *E) {
  QualType Result;
  processBitwiseOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinLAnd(BinaryOperator *E) {
  QualType Result;
  if (processBinaryOperator(Result, E))
    return Result;
  return getBoolType();
}


QualType Decorator::VisitBinPhpLAnd(BinaryOperator *E) {
  QualType Result;
  if (processBinaryOperator(Result, E))
    return Result;
  return getBoolType();
}


QualType Decorator::VisitBinLOr(BinaryOperator *E) {
  QualType Result;
  if (processBinaryOperator(Result, E))
    return Result;
  return getBoolType();
}


QualType Decorator::VisitBinPhpLOr(BinaryOperator *E) {
  QualType Result;
  if (processBinaryOperator(Result, E))
    return Result;
  return getBoolType();
}


QualType Decorator::VisitBinPhpLXor(BinaryOperator *E) {
  QualType Result;
  if (processBinaryOperator(Result, E))
    return Result;
  return getBoolType();
}


QualType Decorator::VisitBinPhpCoalesce(BinaryOperator *E) {
  return getClassBoxType();
}


QualType Decorator::VisitBinPhpConcat(BinaryOperator *E) {
  QualType Result;
  if (processBinaryOperator(Result, E))
    return Result;
  return getStringType();
}


QualType Decorator::VisitBinLT(BinaryOperator *E) {
  QualType Result;
  processCompareOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinLE(BinaryOperator *E) {
  QualType Result;
  processCompareOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinGT(BinaryOperator *E) {
  QualType Result;
  processCompareOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinGE(BinaryOperator *E) {
  QualType Result;
  processCompareOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinEQ(BinaryOperator *E) {
  QualType Result;
  processCompareOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinNE(BinaryOperator *E) {
  QualType Result;
  processCompareOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinPhpIdent(BinaryOperator *E) {
  // Check for identity is not made without call to object handler.
  return getBoolType();
}


QualType Decorator::VisitBinPhpNotIdent(BinaryOperator *E) {
  // Check for identity is not made without call to object handler.
  return getBoolType();
}


QualType Decorator::VisitBinPhpSpaceship(BinaryOperator *E) {
  QualType Result;
  processCompareOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinPhpPower(BinaryOperator *E) {
  QualType Result;
  processArithmeticOperator(Result, E);
  return Result;
}


QualType Decorator::VisitBinComma(BinaryOperator *E) {
  return E->getRHS()->getType();
}


QualType Decorator::VisitUnaryMinus(UnaryOperator *E) {
  Expr *Arg = E->getSubExpr();
  QualType ArgType = Visit(Arg);
  if (Canceled)
    return QualType();

  if (isUniversalType(ArgType))
    return getClassBoxType();

  if (isObjectType(ArgType)) {
    // TODO: if object type is known, we can reveal if the type defines proper
    // handler and issue error if not.
    return getClassBoxType();
  }

  if (isArrayType(ArgType)) {
    Semantics.Diag(E->getExprLoc(), diag::err_php_wrong_array_operand)
      << E->getSubExpr()->getSourceRange();
    return cancel();
  }

  QualType Type;
  bool NegativeBoundary;
  if (Expr *Replacement = transformNumericExpr(Type, Arg, NegativeBoundary)) {
    if (!NegativeBoundary) {
      Arg = Replacement;
      E->setSubExpr(Replacement);
    }
  }

  if (Type->isIntegerType() || NegativeBoundary)
    return getIntValueType();
  if (Type->isFloatingType())
    return getDoubleType();

  // If argument can not be calculated at compile time, use generic number type.
  return getNumberBoxType();
}


QualType Decorator::VisitUnaryPlus(UnaryOperator *E) {
  Expr *Arg = E->getSubExpr();
  QualType ArgType = Visit(Arg);
  if (Canceled)
    return QualType();

  if (isUniversalType(ArgType))
    return getClassBoxType();

  if (isObjectType(ArgType)) {
    // TODO: if object type is known, we can reveal if the type defines proper
    // handler and issue error if not.
    return getClassBoxType();
  }

  if (ArgType->isVoidType()) {
    Semantics.Diag(Arg->getLocStart(), diag::warn_php_implicit_null_cvt)
        << "zero" << E->getSubExpr()->getSourceRange();
    E->setSubExpr(Semantics.createImplicitCast(
                                                       getIntValueType(), Arg));
    return getIntValueType();
  }

  if (isArrayType(ArgType)) {
    Semantics.Diag(E->getExprLoc(), diag::err_php_wrong_array_operand)
        << E->getSubExpr()->getSourceRange();
    return cancel();
  }

  QualType Type;
  bool NegativeBoundary;
  if (Expr *Replacement = transformNumericExpr(Type, Arg, NegativeBoundary)) {
    Arg = Replacement;
    E->setSubExpr(Replacement);
  }

  if (Type->isIntegerType())
    return getIntValueType();
  if (Type->isFloatingType())
    return getDoubleType();

  return getNumberBoxType();
}


QualType Decorator::VisitUnaryLNot(UnaryOperator *E) {
  Expr *Arg = E->getSubExpr();
  QualType ArgType = Visit(Arg);
  if (Canceled)
    return QualType();

  if (isUniversalType(ArgType))
    return getClassBoxType();

  if (isObjectType(ArgType)) {
    // TODO: if object type is known, we can reveal if the type defines proper
    // handler and issue error if not.
    return getClassBoxType();
  }

  if (ArgType->isBooleanType())
    return ArgType;

  // Handle null type separately, it is more unusual than conversion to bool.
  if (ArgType->isVoidType()) {
    Semantics.Diag(Arg->getLocStart(), diag::warn_php_implicit_null_cvt)
          << "false" << E->getSubExpr()->getSourceRange();

  // All other types (arrays, strings, integers, floats) may be used in this
  // operation, it is OK, however issue a warning about implicit conversion.
  } else {
    Semantics.Diag(Arg->getLocStart(), diag::warn_php_implicit_cvt_to_bool)
        << Semantics.getPHPTypeName(ArgType)
        << E->getSubExpr()->getSourceRange();
  }

  E->setSubExpr(Semantics.createImplicitCast(getBoolType(),
                                                               Arg));
  return getBoolType();
}


QualType Decorator::VisitUnaryNot(UnaryOperator *E) {
  Expr *Arg = E->getSubExpr();
  QualType ArgType = Visit(Arg);
  if (Canceled)
    return QualType();

  if (isUniversalType(ArgType))
    return getClassBoxType();

  if (isObjectType(ArgType)) {
    // TODO: if object type is known, we can reveal if the type defines proper
    // handler and issue error if not.
    return getClassBoxType();
  }

  if (isArrayType(ArgType)) {
    Semantics.Diag(E->getExprLoc(), diag::err_php_wrong_array_operand)
        << E->getSubExpr()->getSourceRange();
    return cancel();
  }

  if (isIntegerType(ArgType) || isStringType(ArgType))
    return ArgType;

  // All other types (floats, booleans, null) are converted to integer.
  if (ArgType->isVoidType()) {
    Semantics.Diag(Arg->getLocStart(), diag::warn_php_implicit_null_cvt)
        << "zero" << E->getSubExpr()->getSourceRange();
  } else {
    Semantics.Diag(Arg->getLocStart(), diag::warn_php_implicit_type_conversion)
        << Semantics.getPHPTypeName(ArgType) << "integer"
        << E->getSubExpr()->getSourceRange();
  }

  E->setSubExpr(Semantics.createImplicitCast(
                                                       getIntValueType(), Arg));
  return getIntValueType();
}


QualType Decorator::VisitUnaryOperator(UnaryOperator *E) {
  Expr *Arg = E->getSubExpr();
  QualType ArgType = Visit(Arg);
  if (Canceled)
    return QualType();

  if (isObjectType(ArgType) || isUniversalType(ArgType))
    return getClassBoxType();

  if (isArrayType(ArgType)) {
    Semantics.Diag(E->getExprLoc(), diag::err_php_wrong_array_operand)
      << Arg->getSourceRange();
    return QualType();
  }

  if (isBooleanType(ArgType)) {
    Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_conversion_compass)
      << "boolean" << "integer" << Arg->getSourceRange();
  }

  if (isStringType(ArgType)) {
    // Increment or decrement of a string may produce numerical value.
    UnaryOperatorKind Kind = E->getOpcode();
    if (Kind == UO_PreInc || Kind == UO_PreDec)
      return getClassBoxType();
  }

  return ArgType;
}


QualType Decorator::VisitPhpArrayExpr(PhpArrayExpr *E) {
  assert(RuntimeTypes::isArrayType(E->getType()));
  return E->getType();
}


QualType Decorator::VisitIntegerLiteral(IntegerLiteral *E) {
  return RuntimeTypes::getIntValueType();
}


QualType Decorator::VisitStringLiteral(StringLiteral *E) {
  return E->getType();
}


QualType Decorator::VisitFloatingLiteral(FloatingLiteral *E) {
  assert(E->getType()->isFloatingType());
  return RuntimeTypes::getDoubleType();
}


QualType Decorator::VisitPhpNullLiteral(PhpNullLiteral *E) {
  assert(E->getType()->isVoidType());
  return E->getType();
}


QualType Decorator::VisitCXXBoolLiteralExpr(CXXBoolLiteralExpr *E) {
  assert(E->getType()->isBooleanType());
  return RuntimeTypes::getBoolType();
}


Expr *Decorator::replaceByFloat(APFloat x, SourceLocation Loc) {
  ConstValue *NumVal = Semantics.getConstants().get(x);
  Expr *Replacement = Semantics.getConstantExpr(NumVal, Loc, true);
  return Replacement;
}


Expr *Decorator::replaceByInt(APInt x, SourceLocation Loc) {
  ConstValue *NumVal = Semantics.getConstants().get(x);
  Expr *Replacement = Semantics.getConstantExpr(NumVal, Loc, true);
  return Replacement;
}


}
