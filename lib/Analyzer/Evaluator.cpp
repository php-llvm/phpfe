//===--- Evaluator.cpp ----- AST Transformation component -------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Declaration of Evaluator, which is responsible constant evaluation at
//  compile time.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Support/Utilities.h"
#include "phpfe/Sema/Runtime.h"
#include "conversion/number_recognizer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


ConstValue *Evaluator::cancelEvaluation(const Expr *OffendingExpr,
                                        bool CausedByError) {
  assert(!SeenError && !OffendingExpression);
  SeenError = CausedByError;
  OffendingExpression = OffendingExpr;
  return nullptr;
}


/// \brief Tries to evaluate the specified expression as constant.
///
/// \param E Expression to evaluate.
/// \param allowRTConstants If true, runtime constants are allowed.
/// \returns Constant value for the evaluated expression, nullptr if the
///          expression is not constant.
///
ConstValue *Evaluator::evaluate(const Expr *E) {
  // Reset evaluator state.
  SeenError = false;
  OffendingExpression = nullptr;

  // Make evaluation.
  ConstValue *Result = Visit(E);

  return Result;
}

// TODO: remove, use Sema directly.
bool Evaluator::diagnoseConversion(ConstValue::ConversionStatus Status,
                                   SourceLocation Loc) {
  return Semantics.diagnoseConversion(Status, Loc);
}


bool Evaluator::diagnoseConversion(conversion::Status Status,
                                   SourceLocation Loc) {
  switch (Status) {
  case conversion::OK:
  case conversion::NegativeLimit:
    break;
  case conversion::DoubleToInt:
    Semantics.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Runtime::getStringBoxType())
      << Semantics.getPHPTypeName(Runtime::getIntValueType());
    break;
  case conversion::IntToDouble:
    Semantics.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Runtime::getStringBoxType())
      << Semantics.getPHPTypeName(Semantics.Context.DoubleTy);
    break;
  case conversion::IntOverflow:
  case conversion::IntOverflowNegative:
    Semantics.Diag(Loc, diag::warn_php_int_overflow);
    break;
  case conversion::DoubleOverflow:
    Semantics.reportTooLargeFloat(Loc);
    break;
  case conversion::DoubleUnderflow:
    break;
  case conversion::NaN:
    Semantics.Diag(Loc, diag::warn_php_not_a_number);
    return true;
  }
  return false;
}


ConstValue *Evaluator::convertStringToNumber(StringRef Str, SourceLocation Loc) {
  conversion::NumberRecognizer<> Recog;
  Recog.recognize(Str.data(), Str.size());
  if (Recog.failure()) {
    // String is not a number.
    diagnoseConversion(conversion::NaN, Loc);
    return Constants.getZero();
  }
  if (!Recog.is_float()) {
    Recog.to<int64_t>();
    diagnoseConversion(Recog.get_status(), Loc);
    if (Recog.get_status() == conversion::OK) {
      Semantics.Diag(Loc, diag::warn_php_implicit_type_conversion)
        << Semantics.getPHPTypeName(Runtime::getStringBoxType())
        << Semantics.getPHPTypeName(Runtime::getIntValueType());
    }
    APInt Val;
    if (getIntFromRecognizer(Val, Recog))
      return Constants.get(Val);
  }
  double dvalue = Recog.to<double>();
  diagnoseConversion(Recog.get_status(), Loc);
  if (Recog.get_status() == conversion::OK) {
    Semantics.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Runtime::getStringBoxType())
      << Semantics.getPHPTypeName(Semantics.Context.DoubleTy);
  }
  return Constants.get(APFloat(dvalue));
}


static QualType getPhpType(const ConstValue *E) {
  switch (E->getKind()) {
  case ConstValue::Null:
    return Runtime::getVoidType();

  case ConstValue::Bool:
    return Runtime::getBoolType();

  case ConstValue::Int:
    return Runtime::getIntValueType();

  case ConstValue::Float:
    return Runtime::getDoubleType();

  case ConstValue::String:
    return Runtime::getStringType();

  case ConstValue::Array:
    return Runtime::getArrayType();

  default:
    llvm_unreachable("unknown const value type");
    break;
  }
  return QualType();
}


bool Evaluator::diagnoseCompare(const BinaryOperator *E,
                                const ConstValue *LHS, const ConstValue *RHS) {
  return false;
  SourceLocation LHSLoc = E->getLHS()->getExprLoc();
  SourceLocation RHSLoc = E->getRHS()->getExprLoc();

  if (LHS->getKind() == RHS->getKind()) {
    if (LHS->isString()) {
      conversion::NumberRecognizer<> LRecog, RRecog;
      LRecog.recognize(LHS->getString().data(), LHS->getString().size());
      RRecog.recognize(RHS->getString().data(), RHS->getString().size());
      if (LRecog.success() && RRecog.success()) {
        QualType CommonType;
        if (LRecog.is_float() || RRecog.is_float())
          CommonType = getDoubleType();
        else
          CommonType = getIntValueType();
        Semantics.Diag(E->getLHS()->getLocStart(),
                       diag::warn_php_implicit_from_string)
          << Semantics.getPHPTypeName(CommonType)
          << E->getLHS()->getSourceRange();
        Semantics.Diag(E->getRHS()->getLocStart(),
                       diag::warn_php_implicit_from_string)
          << Semantics.getPHPTypeName(CommonType)
          << E->getRHS()->getSourceRange();
      }
    }
    return false;
  }

  if (LHS->isBool()) {
    QualType ResultTy = RHS->isFloat() 
      ? Runtime::getDoubleType() 
      : Runtime::getIntValueType();
    Semantics.Diag(LHSLoc, diag::warn_php_implicit_bool_conversion)
      << Semantics.getPHPTypeName(ResultTy)
      << E->getLHS()->getSourceRange();
    
    if (RHS->isString())
      Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Runtime::getStringType())
      << Semantics.getPHPTypeName(Runtime::getIntValueType())
      << E->getRHS()->getSourceRange();
    return true;
  }
  if (RHS->isBool()) {
    QualType ResultTy = LHS->isFloat()
      ? Runtime::getDoubleType()
      : Runtime::getIntValueType();
    Semantics.Diag(RHSLoc, diag::warn_php_implicit_bool_conversion)
      << Semantics.getPHPTypeName(ResultTy)
      << E->getRHS()->getSourceRange();

    if (LHS->isString())
      Semantics.Diag(E->getExprLoc(), diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Runtime::getStringType())
      << Semantics.getPHPTypeName(Runtime::getIntValueType())
      << E->getLHS()->getSourceRange();
    return true;
  }

  if (LHS->isNull()) {
    if (RHS->isString()) {
      Semantics.Diag(E->getLHS()->getLocStart(), diag::warn_php_implicit_null_cvt)
        << "empty string" << E->getLHS()->getSourceRange();
      return true;
    }
    Semantics.Diag(LHSLoc, diag::warn_php_implicit_null_cvt)
      << "zero" << E->getLHS()->getSourceRange();
    return true;
  }

  if (RHS->isNull()) {
    if (LHS->isString()) {
      Semantics.Diag(E->getRHS()->getLocStart(), diag::warn_php_implicit_null_cvt)
        << "empty string" << E->getRHS()->getSourceRange();
      return true;
    }
    Semantics.Diag(RHSLoc, diag::warn_php_implicit_null_cvt) <<
      "zero" << E->getRHS()->getSourceRange();
    return true;
  }

  if (LHS->isArray()) {
    Semantics.Diag(RHSLoc, diag::warn_php_compare_to_array)
      << E->getRHS()->getSourceRange();
    return true;
  }
  if (RHS->isArray()) {
    Semantics.Diag(LHSLoc, diag::warn_php_compare_to_array)
      << E->getLHS()->getSourceRange();
    return true;
  }

  if ((LHS->isInt() && RHS->isFloat()) || (LHS->isFloat() && RHS->isInt()))
    return false;
  
  if (!LHS->isInt() && !LHS->isFloat()) {
    Semantics.Diag(LHSLoc, diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(getPhpType(LHS))
      << Semantics.getPHPTypeName(Runtime::getNumberBoxType())
      << E->getLHS()->getSourceRange();
  }
  if (!RHS->isInt() && !RHS->isFloat()) {
    Semantics.Diag(RHSLoc, diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(getPhpType(RHS))
      << Semantics.getPHPTypeName(Runtime::getNumberBoxType())
      << E->getRHS()->getSourceRange();
  }

  return true;
}


APInt Evaluator::convertToInt(const ConstValue *V, SourceLocation Loc) {
  if (V->isInt())
    return V->getInt();

  ConstValue::ConversionStatus Status;
  if (V->isArray()) {
    Semantics.Diag(Loc, diag::err_php_invalid_argument_type)
      << Semantics.getPHPTypeName(Runtime::getIntValueType())
      << Semantics.getPHPTypeName(Runtime::getArrayBoxType());
    return V->getAsInt(Status);
  }

  APInt Result = V->getAsInt(Status);
  diagnoseConversion(Status, Loc);
  if (Status == ConstValue::OK && V->isString()) {
    Semantics.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Runtime::getStringType())
      << Semantics.getPHPTypeName(Runtime::getIntValueType());
  }
  return Result;
}


APFloat Evaluator::convertToFloat(const ConstValue *V, SourceLocation Loc) {
  if (V->isFloat())
    return V->getFloat();

  ConstValue::ConversionStatus Status;
  if (V->isArray()) {
    Semantics.Diag(Loc, diag::err_php_invalid_argument_type)
      << Semantics.getPHPTypeName(Runtime::getDoubleType())
      << Semantics.getPHPTypeName(Runtime::getArrayBoxType());
    return V->getAsFloat(Status);
  }

  APFloat Result = V->getAsFloat(Status);
  diagnoseConversion(Status, Loc);
  if (Status == ConstValue::OK && V->isString()) {
    Semantics.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Runtime::getStringType())
      << Semantics.getPHPTypeName(Runtime::getDoubleType());
  }
  return Result;
}


ConstValue *Evaluator::convertToNumber(ConstValue *V, SourceLocation Loc) {
  if (V->isFloat() || V->isInt())
    return V;
  if (V->isString())
    return convertStringToNumber(V->getString(), Loc);
  return Constants.get(convertToInt(V, Loc));
}


ConstValue *Evaluator::VisitExpr(const Expr *E) {
  return cancelEvaluation(E, false);
}


ConstValue *Evaluator::VisitParenExpr(const ParenExpr *E) {
  return Visit(E->getSubExpr());
}


ConstValue *Evaluator::VisitImplicitCastExpr(const ImplicitCastExpr *E) {
  ConstValue *V = Visit(E->getSubExpr());
  if (!V)
    return nullptr;

  QualType T = E->getType();
  if (ConstValue *Result = Semantics.getConstantValue(V, T, E->getLocStart())) {
    Semantics.addConstantExpr(E, Result);
    return Result;
  }

  return cancelEvaluation(E, false);
}


ConstValue *Evaluator::VisitCStyleCastExpr(const CStyleCastExpr *E) {
  ConstValue *V = Visit(E->getSubExpr());
  if (!V)
    return nullptr;

  QualType T = E->getType();
  if (ConstValue *Result = Semantics.getConstantValue(V, T, E->getLocStart())) {
    Semantics.addConstantExpr(E, Result);
    return Result;
  }

  return cancelEvaluation(E, false);
}


ConstValue *Evaluator::VisitCallExpr(const CallExpr *E) {
  if (const StringLiteral *S = dyn_cast<StringLiteral>(E->getCallee())) {
    auto *BI = find_builtin_function(S->getString());
    if (BI && BI->Handlers.EvaluatorH) {
      return BI->Handlers.EvaluatorH(*this, *BI, E);
    }
  }
  return cancelEvaluation(E, false);
}


ConstValue *Evaluator::VisitOpaqueValueExpr(const OpaqueValueExpr *E) {
  return Visit(E->getSourceExpr());
}


ConstValue *Evaluator::VisitUnaryMinus(const UnaryOperator *E) {
  ConstValue *V = Visit(E->getSubExpr());
  if (!V)
    return nullptr;

  ConstValue *Result;
  switch (V->getKind()) {
  case ConstValue::NotAConstant:
    llvm_unreachable("wrong constant");

  case ConstValue::Null:
    Result = Constants.getZero();
    break;

  case ConstValue::Bool:
    Result = V->getBool() ? Constants.getMinusOne() : Constants.getZero();
    break;

  case ConstValue::Int: {
    APInt IntValue = V->getInt();
    if (IntValue == APInt::getSignedMinValue(IntValue.getBitWidth())) {
      APFloat FVal(APFloat::IEEEdouble, IntValue.getLimitedValue());
      FVal.changeSign();
      Result = Constants.get(FVal);
    } else {
      Result = Constants.get(-V->getInt());
    }
    break;
  }
  case ConstValue::Float: {
    APFloat Val = V->getFloat();
    Val.changeSign();
    Result = Constants.get(Val);
    break;
  }

  case ConstValue::String: {
    conversion::NumberRecognizer<> Recog;
    Recog.recognize(V->getString().data(), V->getString().size());
    if (Recog.get_status() == conversion::NegativeLimit)
      return Constants.get(APInt::getSignedMinValue(Runtime::getIntValueWidth()));
    diagnoseConversion(Recog.get_status(), E->getSubExpr()->getExprLoc());
    if (Recog.is_float()) {
      double result = -Recog.to<double>();
      return Constants.get(APFloat(result));
    }

    int64_t result;
    Recog.read(result);
    return Constants.get(APInt(Runtime::getIntValueWidth(), result, true));
  }

  case ConstValue::Array:
    Semantics.Diag(E->getExprLoc(), 
      diag::err_php_unsupported_unary_operand_type)
        << Semantics.getPHPTypeName(Runtime::getArrayType())
        << E->getSubExpr()->getSourceRange();
    return cancelEvaluation(E->getSubExpr(), true);
  }

  Semantics.addConstantExpr(E, Result);
  return Result;
}


ConstValue *Evaluator::VisitUnaryPlus(const UnaryOperator *E) {
  ConstValue *V = Visit(E->getSubExpr());
  if (!V)
    return nullptr;

  if (V->isArray()) {
    Semantics.Diag(E->getExprLoc(),
      diag::err_php_unsupported_unary_operand_type)
      << Semantics.getPHPTypeName(Runtime::getArrayType())
      << E->getSubExpr()->getSourceRange();
    return cancelEvaluation(E->getSubExpr(), true);
  }

  V = convertToNumber(V, E->getSubExpr()->getExprLoc());
  Semantics.addConstantExpr(E, V);
  return V;
}


ConstValue *Evaluator::VisitUnaryNot(const UnaryOperator *E) {
  ConstValue *V = Visit(E->getSubExpr());
  if (!V)
    return nullptr;

  ConstValue *Result = nullptr;
  QualType ValType;
  switch (V->getKind()) {
  case ConstValue::Int:
    Result = Constants.get(~V->getInt());
    break;

  case ConstValue::Float: {
    SourceLocation Loc = E->getSubExpr()->getExprLoc();
    Semantics.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Context.DoubleTy)
      << Semantics.getPHPTypeName(Runtime::getIntValueType());
    ConstValue::ConversionStatus Status;
    APInt Val = V->getAsInt(Status);
    Result = Constants.get(~Val);
    break;
  }
    
  case ConstValue::String: {
    StringRef Str = V->getString();
    SmallString<64> ResultStr;
    ResultStr.resize(Str.size());
    std::transform(Str.begin(), Str.end(), ResultStr.begin(), 
      [](char V) { return (char)~V; });
    Result = Constants.get(StringRef(ResultStr));
    break;
  }

  case ConstValue::Null:
    ValType = Runtime::getVoidType();
    break;

  case ConstValue::Bool:
    ValType = Semantics.Context.BoolTy;
    break;

  case ConstValue::Array:
    ValType = Runtime::getArrayType();
    break;

  default:
    break;
  }

  if (!Result) {
    if (!ValType.isNull()) {
      SourceLocation Loc = E->getExprLoc();
      Semantics.Diag(Loc, diag::err_php_unsupported_unary_operand_type)
        << Semantics.getPHPTypeName(ValType) 
        << E->getSubExpr()->getSourceRange();
    }
    return cancelEvaluation(E->getSubExpr(), true);
  }

  Semantics.addConstantExpr(E, Result);
  return Result;
}


ConstValue *Evaluator::VisitUnaryLNot(const UnaryOperator *E) {
  ConstValue *V = Visit(E->getSubExpr());
  if (!V)
    return nullptr;

  ConstValue *Result;
  switch (V->getKind()) {
  case ConstValue::NotAConstant:
    llvm_unreachable("wrong constant");

  case ConstValue::Null:
    Result = Constants.getTrue();
    break;

  case ConstValue::Bool:
    Result = Constants.get(!V->getBool());
    break;

  case ConstValue::Int:
    Result = Constants.get(!V->getInt());
    break;

  case ConstValue::Float:
    Result = Constants.get(V->getFloat().isZero());
    break;

  case ConstValue::String: {
    StringRef Str = V->getString();
    Result = Constants.get(Str.empty() || Str.equals("0"));
    break;
  }

  case ConstValue::Array:
    Result = Constants.get(V->getArray().empty());
    break;
  }

  Semantics.addConstantExpr(E, Result);
  return Result;
}


ConstValue *Evaluator::VisitBinAdd(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;

  // Make array addition.
  if (LHS->isArray()) {
    if (RHS->isArray()) {
      SmallVector<ArrayItem, 16> Items;
      for (auto I : LHS->getArray())
        Items.push_back(I);
      auto &LHSArray = LHS->getArray();
      for (auto I : RHS->getArray()) {
        if (LHSArray.hasKey(I.getKey()))
          continue;
        Items.push_back(I);
      }
      ResValue = Constants.get(Items);
      Semantics.addConstantExpr(E, ResValue);
      return ResValue;
    }
    Semantics.Diag(E->getExprLoc(), diag::err_php_array_addition)
        << E->getRHS()->getSourceRange();
    return cancelEvaluation(E, true);
  }
  if (RHS->isArray()) {
    Semantics.Diag(E->getExprLoc(), diag::err_php_array_addition)
        << E->getLHS()->getSourceRange();
    return cancelEvaluation(E, true);
  }

  // Process string operands. They must be replaced by corresponding integer
  // or floating constants, with proper warnings.
  if (LHS->isString())
    LHS = convertToNumber(LHS, E->getLHS()->getExprLoc());

  if (RHS->isString()) 
    RHS = convertToNumber(RHS, E->getRHS()->getExprLoc());

  // Process boolean operands. They must be replaced by corresponding integer
  // constants, without warnings.
  if (LHS->isBool())
    LHS = Constants.getIntBool(LHS->getBool());
  if (RHS->isBool())
    RHS = Constants.getIntBool(RHS->getBool());

  // Process null operands. They must be replaces with 0 with warning.
  if (LHS->isNull()) {
    Semantics.Diag(E->getLHS()->getLocStart(),
                   diag::warn_php_implicit_null_cvt) << "zero";
    if (RHS->isNull()) {
      Semantics.Diag(E->getRHS()->getLocStart(),
                     diag::warn_php_implicit_null_cvt) << "zero";
      ResValue = Constants.getZero();
      Semantics.addConstantExpr(E, ResValue);
      return ResValue;
    }
    Semantics.addConstantExpr(E, RHS);
    return RHS;
  }
  
  if (RHS->isNull()) {
    Semantics.Diag(E->getRHS()->getLocStart(),
                   diag::warn_php_implicit_null_cvt) << "zero";
    Semantics.addConstantExpr(E, LHS);
    return LHS;
  }

  // Make real calculations.
  if (LHS->isFloat() || RHS->isFloat()) {
    APFloat Result = convertToFloat(LHS, E->getLHS()->getLocStart());
    APFloat RV = convertToFloat(RHS, E->getRHS()->getLocStart());
    Result.add(RV, APFloat::rmNearestTiesToEven);
    ResValue = Constants.get(Result);
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  // Make integer calculation.
  assert(LHS->isInt());
  assert(RHS->isInt());
  APInt LV = convertToInt(LHS, E->getLHS()->getLocStart());
  APInt RV = convertToInt(RHS, E->getRHS()->getLocStart());
  bool Overflow = false;
  APInt Result = LV.sadd_ov(RV, Overflow);
  if (Overflow) {
    Semantics.Diag(E->getRHS()->getLocStart(), diag::warn_php_overflow_in_op);
    APFloat Result = convertToFloat(LHS, E->getLHS()->getLocStart());
    APFloat RV = convertToFloat(RHS, E->getRHS()->getLocStart());
    Result.add(RV, APFloat::rmNearestTiesToEven);
    ResValue = Constants.get(Result);
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }
  ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinSub(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;

  // Diagnose array subtraction.
  if (LHS->isArray()) {
    Semantics.Diag(E->getLHS()->getLocStart(),
      diag::err_php_wrong_array_operand);
    return cancelEvaluation(E, true);
  }
  if (RHS->isArray()) {
    Semantics.Diag(E->getRHS()->getLocStart(),
      diag::err_php_wrong_array_operand);
    return cancelEvaluation(E, true);
  }

  // Process string operands. They must be replaced by corresponding integer
  // or floating constants, with proper warnings.
  if (LHS->isString())
    LHS = convertToNumber(LHS, E->getLHS()->getExprLoc());

  if (RHS->isString())
    RHS = convertToNumber(RHS, E->getRHS()->getExprLoc());

  // Process boolean operands. They must be replaced by corresponding integer
  // constants, without warnings.
  if (LHS->isBool())
    LHS = Constants.getIntBool(LHS->getBool());
  if (RHS->isBool())
    RHS = Constants.getIntBool(RHS->getBool());

  // Process null operands. They must be replaces with 0 with warning.
  if (LHS->isNull()) {
    Semantics.Diag(E->getLHS()->getLocStart(),
      diag::warn_php_implicit_null_cvt) << "zero";
    if (RHS->isNull()) {
      Semantics.Diag(E->getRHS()->getLocStart(),
        diag::warn_php_implicit_null_cvt) << "zero";
      ResValue = Constants.getZero();
      Semantics.addConstantExpr(E, ResValue);
      return ResValue;
    }
    // change RHS sign
    if (RHS->isInt())
      ResValue = Constants.get(-RHS->getInt());
    else {
      assert(RHS->isFloat());
      APFloat Val = RHS->getFloat();
      Val.changeSign();
      ResValue = Constants.get(Val);
    }
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  if (RHS->isNull()) {
    Semantics.Diag(E->getRHS()->getLocStart(),
      diag::warn_php_implicit_null_cvt) << "zero";
    Semantics.addConstantExpr(E, LHS);
    return LHS;
  }

  // Make real calculations.
  if (LHS->isFloat() || RHS->isFloat()) {
    APFloat Result = convertToFloat(LHS, E->getLHS()->getLocStart());
    APFloat RV = convertToFloat(RHS, E->getRHS()->getLocStart());
    Result.subtract(RV, APFloat::rmNearestTiesToEven);
    ResValue = Constants.get(Result);
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  // Make integer calculation.
  assert(LHS->isInt());
  assert(RHS->isInt());
  APInt LV = convertToInt(LHS, E->getLHS()->getLocStart());
  APInt RV = convertToInt(RHS, E->getRHS()->getLocStart());
  bool Overflow = false;
  APInt Result = LV.ssub_ov(RV, Overflow);
  if (Overflow) {
    Semantics.Diag(E->getRHS()->getLocStart(), diag::warn_php_overflow_in_op);
    APFloat Result = convertToFloat(LHS, E->getLHS()->getLocStart());
    APFloat RV = convertToFloat(RHS, E->getRHS()->getLocStart());
    Result.subtract(RV, APFloat::rmNearestTiesToEven);
    ResValue = Constants.get(Result);
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }
  ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinMul(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;

  // Diagnose array multiplication.
  if (LHS->isArray()) {
    Semantics.Diag(E->getLHS()->getLocStart(),
      diag::err_php_wrong_array_operand);
    return cancelEvaluation(E, true);
  }
  if (RHS->isArray()) {
    Semantics.Diag(E->getRHS()->getLocStart(),
      diag::err_php_wrong_array_operand);
    return cancelEvaluation(E, true);
  }

  // Process string operands. They must be replaced by corresponding integer
  // or floating constants, with proper warnings.
  if (LHS->isString())
    LHS = convertToNumber(LHS, E->getLHS()->getExprLoc());

  if (RHS->isString())
    RHS = convertToNumber(RHS, E->getRHS()->getExprLoc());

  // Process boolean operands. They must be replaced by corresponding integer
  // constants, without warnings.
  if (LHS->isBool())
    LHS = Constants.getIntBool(LHS->getBool());
  if (RHS->isBool())
    RHS = Constants.getIntBool(RHS->getBool());

  // Process null operands. They must be replaces with 0 with warning.
  if (LHS->isNull()) {
    Semantics.Diag(E->getLHS()->getLocStart(),
      diag::warn_php_implicit_null_cvt) << "zero";
    if (RHS->isNull()) {
      Semantics.Diag(E->getRHS()->getLocStart(),
        diag::warn_php_implicit_null_cvt) << "zero";
    }
    ResValue = RHS->isFloat()
      ? Constants.get(APFloat(0.0))
      : Constants.getZero();
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  if (RHS->isNull()) {
    Semantics.Diag(E->getRHS()->getLocStart(),
      diag::warn_php_implicit_null_cvt) << "zero";
    ResValue = LHS->isFloat()
      ? Constants.get(APFloat(0.0))
      : Constants.getZero();
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  // Make real calculations.
  if (LHS->isFloat() || RHS->isFloat()) {
    APFloat Result = convertToFloat(LHS, E->getLHS()->getLocStart());
    APFloat RV = convertToFloat(RHS, E->getRHS()->getLocStart());
    Result.multiply(RV, APFloat::rmNearestTiesToEven);
    ResValue = Constants.get(Result);
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  // Make integer calculation.
  assert(LHS->isInt());
  assert(RHS->isInt());
  APInt LV = convertToInt(LHS, E->getLHS()->getLocStart());
  APInt RV = convertToInt(RHS, E->getRHS()->getLocStart());
  bool Overflow = false;
  APInt Result = LV.smul_ov(RV, Overflow);
  if (Overflow) {
    Semantics.Diag(E->getRHS()->getLocStart(), diag::warn_php_overflow_in_op);
    APFloat Result = convertToFloat(LHS, E->getLHS()->getLocStart());
    APFloat RV = convertToFloat(RHS, E->getRHS()->getLocStart());
    Result.multiply(RV, APFloat::rmNearestTiesToEven);
    ResValue = Constants.get(Result);
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }
  ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinDiv(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;

  // Diagnose array division.
  if (LHS->isArray()) {
    Semantics.Diag(E->getLHS()->getLocStart(),
      diag::err_php_wrong_array_operand);
    return cancelEvaluation(E, true);
  }
  if (RHS->isArray()) {
    Semantics.Diag(E->getRHS()->getLocStart(),
      diag::err_php_wrong_array_operand);
    return cancelEvaluation(E, true);
  }

  // Process string operands. They must be replaced by corresponding integer
  // or floating constants, with proper warnings.
  if (LHS->isString())
    LHS = convertToNumber(LHS, E->getLHS()->getExprLoc());

  if (RHS->isString())
    RHS = convertToNumber(RHS, E->getRHS()->getExprLoc());

  // Process boolean operands. They must be replaced by corresponding integer
  // constants, without warnings.
  if (LHS->isBool())
    LHS = Constants.getIntBool(LHS->getBool());
  if (RHS->isBool())
    RHS = Constants.getIntBool(RHS->getBool());

  // Process null operands. They must be replaces with 0 with warning.
  if (LHS->isNull()) {
    Semantics.Diag(E->getLHS()->getLocStart(),
      diag::warn_php_implicit_null_cvt) << "zero";
    if (RHS->isNull()) {
      Semantics.Diag(E->getExprLoc(),
        diag::err_php_division_by_zero) << E->getRHS()->getSourceRange();
      return cancelEvaluation(E->getRHS(), true);
    }
    ResValue = RHS->isFloat()
      ? Constants.get(APFloat(0.0))
      : Constants.getZero();
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  if (RHS->isNull()) {
    Semantics.Diag(E->getExprLoc(),
      diag::err_php_division_by_zero) << E->getRHS()->getSourceRange();
    return cancelEvaluation(E->getRHS(), true);
  }

  // Make real calculations.
  APFloat Result = convertToFloat(LHS, E->getLHS()->getLocStart());
  APFloat RV = convertToFloat(RHS, E->getRHS()->getLocStart());
  if (RV.isZero()) {
    Semantics.Diag(E->getExprLoc(),
      diag::err_php_division_by_zero) << E->getRHS()->getSourceRange();
    return cancelEvaluation(E->getRHS(), true);
  }
  Result.divide(RV, APFloat::rmNearestTiesToEven);

  if (LHS->isInt() && RHS->isInt()) {
    APSInt IntVal(Runtime::getIntValueWidth(), false);
    bool IsExact;
    Result.convertToInteger(IntVal, APFloat::rmTowardZero, &IsExact);
    ResValue = IsExact 
      ? Constants.get(IntVal)
      : Constants.get(Result);
  } else {
    ResValue = Constants.get(Result);
  }
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinRem(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;

  // Diagnose array rem.
  if (LHS->isArray()) {
    Semantics.Diag(E->getLHS()->getLocStart(),
      diag::err_php_wrong_array_operand);
    return cancelEvaluation(E, true);
  }
  if (RHS->isArray()) {
    Semantics.Diag(E->getRHS()->getLocStart(),
      diag::err_php_wrong_array_operand);
    return cancelEvaluation(E, true);
  }

  // Process boolean operands. They must be replaced by corresponding integer
  // constants, without warnings.
  if (LHS->isBool())
    LHS = Constants.getIntBool(LHS->getBool());
  if (RHS->isBool())
    RHS = Constants.getIntBool(RHS->getBool());

  // Process null operands. They must be replaces with 0 with warning.
  if (LHS->isNull()) {
    Semantics.Diag(E->getLHS()->getLocStart(), diag::warn_php_implicit_null_cvt)
      << "zero";
    if (RHS->isNull()) {
      Semantics.Diag(E->getExprLoc(),
        diag::err_php_division_by_zero) << E->getRHS()->getSourceRange();
      return cancelEvaluation(E->getRHS(), true);
    }
    ResValue = Constants.getZero();
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  if (RHS->isNull()) {
    Semantics.Diag(E->getExprLoc(),
      diag::err_php_division_by_zero) << E->getRHS()->getSourceRange();
    return cancelEvaluation(E->getRHS(), true);
  }

  APInt Result = convertToInt(LHS, E->getLHS()->getLocStart());
  APInt RV = convertToInt(RHS, E->getRHS()->getLocStart());
  if (RV == 0) {
    Semantics.Diag(E->getExprLoc(),
      diag::err_php_division_by_zero) << E->getRHS()->getSourceRange();
    return cancelEvaluation(E->getRHS(), true);
  }
  ResValue = Constants.get(Result.srem(RV));
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


static bool diagnoseBitOperator(Sema &S, const BinaryOperator *E,
                                ConstValue *LHS, ConstValue *RHS) {
  // Diagnose array in bitwise operation
  if (LHS->isArray()) {
    if (RHS->isArray())
      S.Diag(E->getExprLoc(),
        diag::err_php_wrong_array_operand)
      << E->getLHS()->getSourceRange()
      << E->getRHS()->getSourceRange();
    else
      S.Diag(E->getExprLoc(),
        diag::err_php_wrong_array_operand)
      << E->getLHS()->getSourceRange();
    return true;
  }
  if (RHS->isArray()) {
    S.Diag(E->getExprLoc(),
      diag::err_php_wrong_array_operand)
      << E->getRHS()->getSourceRange();
    return true;
  }

  // Emit float operands implicit conversion diagnostics.
  if (LHS->isFloat()) {
    S.Diag(E->getLHS()->getExprLoc(),
      diag::warn_php_implicit_type_conversion)
      << S.getPHPTypeName(Runtime::getDoubleType())
      << S.getPHPTypeName(Runtime::getIntValueType())
      << E->getLHS()->getSourceRange();
  }
  if (RHS->isFloat()) {
    S.Diag(E->getRHS()->getExprLoc(),
      diag::warn_php_implicit_type_conversion)
      << S.getPHPTypeName(Runtime::getDoubleType())
      << S.getPHPTypeName(Runtime::getIntValueType())
      << E->getRHS()->getSourceRange();
  }

  // Process null operands.
  if (LHS->isNull()) {
    S.Diag(E->getLHS()->getLocStart(), diag::warn_php_implicit_null_cvt)
      << "zero";
  }
  if (RHS->isNull()) {
    S.Diag(E->getRHS()->getLocStart(), diag::warn_php_implicit_null_cvt)
      << "zero";
  }
  return false;
}


static bool diagnoseBitShift(Sema &S, const BinaryOperator *E,
                             ConstValue *LHS, ConstValue *RHS) {
  
  if (diagnoseBitOperator(S, E, LHS, RHS))
    return true;

  ConstValue::ConversionStatus Status;
  if ((RHS->isFloat() && RHS->getFloat().isNegative()) ||
      (RHS->getAsInt(Status).isNegative())) {
    S.Diag(E->getRHS()->getExprLoc(), diag::err_php_negative_bit_shift)
      << E->getRHS()->getSourceRange();
    return true;
  }
  return false;
}


ConstValue *Evaluator::VisitBinShl(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;


  if (diagnoseBitShift(Semantics, E, LHS, RHS))
    return cancelEvaluation(E, true);

  // Process null operands.
  if (LHS->isNull()) {
    ResValue = Constants.getZero();
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }
  if (RHS->isNull()) {
    ResValue = Constants.get(convertToInt(LHS, E->getLHS()->getExprLoc()));
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }
  
  APInt LV = convertToInt(LHS, E->getLHS()->getLocStart());
  APInt RV = convertToInt(RHS, E->getRHS()->getLocStart());
  APInt Result = LV;
  assert(!RV.isNegative());
  if (LV != 0 && RV != 0) {
    bool Overflow = false;
    Result = LV.sshl_ov(RV, Overflow);
    if (Overflow) {
      Semantics.Diag(E->getRHS()->getLocStart(), diag::warn_php_overflow_in_op);
      ResValue = Constants.getZero();
      Semantics.addConstantExpr(E, ResValue);
      return ResValue;
    }
  }
  ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinShr(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;

  if (diagnoseBitShift(Semantics, E, LHS, RHS))
    return cancelEvaluation(E, true);

  // Process null operands.
  if (LHS->isNull()) {
    ResValue = Constants.getZero();
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }
  if (RHS->isNull()) {
    ResValue = Constants.get(convertToInt(LHS, E->getLHS()->getExprLoc()));
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  APInt LV = convertToInt(LHS, E->getLHS()->getLocStart());
  APInt RV = convertToInt(RHS, E->getRHS()->getLocStart());
  APInt Result = LV;
  if (RV.isNegative()) {
    Semantics.Diag(E->getRHS()->getExprLoc(), diag::err_php_negative_bit_shift)
      << E->getRHS()->getSourceRange();
    return cancelEvaluation(E, true);
  }
  if (LV != 0 && RV != 0)
    Result = LV.ashr(RV);
  ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinAnd(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;

  if (diagnoseBitOperator(Semantics, E, LHS, RHS))
    return cancelEvaluation(E, true);

  // Process string operands.
  if (LHS->isString() && RHS->isString()) {
    SmallString<32> Str;
    StringRef LHSStr = LHS->getString();
    StringRef RHSStr = RHS->getString();
    unsigned Sz = std::min(LHS->getString().size(), RHS->getString().size());
    for (unsigned I = 0; I < Sz; ++I) {
      Str.push_back(LHSStr[I] & RHSStr[I]);
    }
    ResValue = Constants.get(Str.str());
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  // Process null operands.
  if (LHS->isNull() || RHS->isNull()) {
    ResValue = Constants.getZero();
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }
  
  APInt LV = convertToInt(LHS, E->getLHS()->getLocStart());
  APInt RV = convertToInt(RHS, E->getRHS()->getLocStart());
  APInt Result = LV & RV;
  ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinOr(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;

  if (diagnoseBitOperator(Semantics, E, LHS, RHS))
    return cancelEvaluation(E, true);

  // Process string operands.
  if (LHS->isString() && RHS->isString()) {
    SmallString<32> Str;
    StringRef LHSStr = LHS->getString();
    StringRef RHSStr = RHS->getString();
    Str.append(LHSStr.size() > RHSStr.size() ? LHSStr : RHSStr);
    unsigned Sz = std::min(LHS->getString().size(), RHS->getString().size());
    for (unsigned I = 0; I < Sz; ++I) {
      Str[I] = (LHSStr[I] | RHSStr[I]);
    }
    ResValue = Constants.get(Str.str());
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  APInt LV = convertToInt(LHS, E->getLHS()->getLocStart());
  APInt RV = convertToInt(RHS, E->getRHS()->getLocStart());
  APInt Result = LV | RV;
  ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinXor(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  ConstValue *ResValue = nullptr;

  if (diagnoseBitOperator(Semantics, E, LHS, RHS))
    return cancelEvaluation(E, true);

  // Process string operands.
  if (LHS->isString() && RHS->isString()) {
    SmallString<32> Str;
    StringRef LHSStr = LHS->getString();
    StringRef RHSStr = RHS->getString();
    unsigned Sz = std::min(LHS->getString().size(), RHS->getString().size());
    for (unsigned I = 0; I < Sz; ++I) {
      Str.push_back(LHSStr[I] ^ RHSStr[I]);
    }
    ResValue = Constants.get(Str.str());
    Semantics.addConstantExpr(E, ResValue);
    return ResValue;
  }

  APInt LV = convertToInt(LHS, E->getLHS()->getLocStart());
  APInt RV = convertToInt(RHS, E->getRHS()->getLocStart());
  APInt Result = LV ^ RV;
  ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinLAnd(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;

  ConstValue::ConversionStatus Status;
  ConstValue *Result = nullptr;
  if (!LHS->getAsBool(Status))
    Result = Constants.getFalse();
  else {
    ConstValue *RHS = Visit(E->getRHS());
    if (!RHS)
      return nullptr;

    Result = Constants.get(RHS->getAsBool(Status));
  }
  Semantics.addConstantExpr(E, Result);
  return Result;
}


ConstValue *Evaluator::VisitBinPhpLAnd(const BinaryOperator *E) {
  return VisitBinLAnd(E);
}


ConstValue *Evaluator::VisitBinLOr(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;

  ConstValue::ConversionStatus Status;
  ConstValue *Result = nullptr;
  if (LHS->getAsBool(Status))
    Result = Constants.getTrue();
  else {
    ConstValue *RHS = Visit(E->getRHS());
    if (!RHS)
      return nullptr;

    Result = Constants.get(RHS->getAsBool(Status));
  }
  Semantics.addConstantExpr(E, Result);
  return Result;
}


ConstValue *Evaluator::VisitBinPhpLOr(const BinaryOperator *E) {
  return VisitBinLOr(E);
}


ConstValue *Evaluator::VisitBinPhpLXor(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;

  ConstValue::ConversionStatus Status;
  bool LHSVal = LHS->getAsBool(Status);
  bool RHSVal = RHS->getAsBool(Status);

  auto Result = Constants.get(LHSVal != RHSVal);
  Semantics.addConstantExpr(E, Result);
  return Result;
}


ConstValue *Evaluator::VisitBinLT(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  diagnoseCompare(E, LHS, RHS);
  bool Result = (LHS->compare(RHS) == CompareResult::Less);
  ConstValue *ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinLE(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  diagnoseCompare(E, LHS, RHS);
  bool Result = (LHS->compare(RHS) != CompareResult::Greater);
  ConstValue *ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinGT(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  diagnoseCompare(E, LHS, RHS);
  // php idiosyncrasy: greater then function implemented using 
  // less than function with reversed arguments. But when we 
  // compare arrays like ['a'=>1], ['b'=>1] the first one is greater
  // since the second one does not have key 'a'.
  bool Result = (RHS->compare(LHS) == CompareResult::Less);
  ConstValue *ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinGE(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  diagnoseCompare(E, LHS, RHS);
  // php idiosyncrasy: greater then function implemented using 
  // less than function with reversed arguments. But when we 
  // compare arrays like ['a'=>1], ['b'=>1] the first one is greater
  // since the second one does not have key 'a'.
  bool Result = (RHS->compare(LHS) != CompareResult::Greater);
  ConstValue *ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinEQ(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  diagnoseCompare(E, LHS, RHS);
  bool Result = (LHS->compare(RHS) == CompareResult::Equal);
  ConstValue *ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinNE(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  diagnoseCompare(E, LHS, RHS);
  bool Result = (LHS->compare(RHS) != CompareResult::Equal);
  ConstValue *ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinPhpNotIdent(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  bool Result = !LHS->equals_strict(RHS);
  ConstValue *ResValue = Constants.get(Result);
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinPhpSpaceship(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;
  diagnoseCompare(E, LHS, RHS);
  ConstValue *ResValue;
  switch (LHS->compare(RHS)) {
  case CompareResult::Less:
    ResValue = Constants.getMinusOne();
    break;

  case CompareResult::Equal:
    ResValue = Constants.getZero();
    break;

  case CompareResult::Greater:
    ResValue = Constants.getOne();
    break;
  }
  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


ConstValue *Evaluator::VisitBinPhpCoalesce(const BinaryOperator *E) {//TODO:?
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;

  ConstValue *Result = LHS;
  if (LHS->isNull()) {
    ConstValue *RHS = Visit(E->getRHS());
    if (!RHS)
      return nullptr;
    Result = RHS;
  }
  Semantics.addConstantExpr(E, Result);
  return Result;
}


ConstValue *Evaluator::VisitBinPhpConcat(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;
  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;

  if (LHS->isArray()) {
    Semantics.Diag(E->getLHS()->getExprLoc(), 
      diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Runtime::getArrayType())
      << Semantics.getPHPTypeName(Runtime::getStringType())
      << E->getLHS()->getSourceRange();
  }
  if (RHS->isArray()) {
    Semantics.Diag(E->getRHS()->getExprLoc(),
      diag::warn_php_implicit_type_conversion)
      << Semantics.getPHPTypeName(Runtime::getArrayType())
      << Semantics.getPHPTypeName(Runtime::getStringType())
      << E->getRHS()->getSourceRange();
  }

  ConstValue::ConversionStatus Status;
  auto LHSStr = LHS->getAsString(Status);
  diagnoseConversion(Status, E->getLHS()->getExprLoc());

  auto RHSStr = RHS->getAsString(Status);
  diagnoseConversion(Status, E->getRHS()->getExprLoc());


  ConstValue *Result = Constants.get(LHSStr + RHSStr);
  Semantics.addConstantExpr(E, Result);
  return Result;
}


ConstValue *Evaluator::VisitBinPhpPower(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;

  if (LHS->isArray() || LHS->isNull()) {
    QualType Ty = LHS->isArray()
      ? Runtime::getArrayType()
      : Runtime::getVoidType();
    Semantics.Diag(E->getExprLoc(),
                   diag::err_php_unsupported_binary_operand_type)
      << Semantics.getPHPTypeName(Ty) << 0 << E->getLHS()->getSourceRange();
    return cancelEvaluation(E->getLHS(), true);
  }

  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;

  if (RHS->isArray() || RHS->isNull()) {
    QualType Ty = RHS->isArray()
      ? Runtime::getArrayType()
      : Runtime::getVoidType();
    Semantics.Diag(E->getExprLoc(),
                   diag::err_php_unsupported_binary_operand_type)
      << Semantics.getPHPTypeName(Ty) << 1 << E->getRHS()->getSourceRange();
    return cancelEvaluation(E->getRHS(), true);
  }

  LHS = convertToNumber(LHS, E->getLHS()->getExprLoc());
  RHS = convertToNumber(RHS, E->getRHS()->getExprLoc());

  bool LHSIsZero = LHS->isFloat()
    ? LHS->getFloat().isZero()
    : LHS->getInt().getLimitedValue() == 0;
  
  bool RHSIsZero = RHS->isFloat()
    ? RHS->getFloat().isZero()
    : RHS->getInt().getLimitedValue() == 0;
  
  if (LHSIsZero && RHSIsZero) {
    Semantics.Diag(E->getExprLoc(), diag::err_php_undefined_operation_result);
    return cancelEvaluation(E, true);
  }

  if (LHSIsZero) {
    bool IsNegativeRHS = RHS->isFloat()
      ? RHS->getFloat().isNegative()
      : RHS->getInt().isNegative();

    if (IsNegativeRHS) {
      Semantics.Diag(E->getExprLoc(), diag::err_php_division_by_zero);
      return cancelEvaluation(E, true);
    }
    ConstValue *Result = (LHS->isFloat() || RHS->isFloat())
      ? Constants.get(APFloat(0.0))
      : Constants.getZero();
    Semantics.addConstantExpr(E, Result);
    return Result;
  }

  if (RHSIsZero) {
    ConstValue *Result = (LHS->isFloat() || RHS->isFloat())
      ? Constants.get(APFloat(1.0))
      : Constants.getOne();
    Semantics.addConstantExpr(E, Result);
    return Result;
  }

  //TODO: do not convert int arguments to double if the result fits int.
  ConstValue::ConversionStatus Status;
  double LHSVal = LHS->getAsFloat(Status).convertToDouble();
  double RHSVal = RHS->getAsFloat(Status).convertToDouble();

  double PowResult = pow(LHSVal, RHSVal);
  ConstValue *Result = nullptr;
  if (LHS->isInt() && RHS->isInt()) {
    APInt Val(Runtime::getIntValueWidth(), (int64_t)PowResult);
    if (Val.sgt(Runtime::getMaxInteger()) ||
        Val.slt(Runtime::getMinInteger()))
      Result = Constants.get(APFloat(APFloat::IEEEdouble, PowResult));
    else
      Result = Constants.get(Val);
  } else {
    Result = Constants.get(APFloat(PowResult));
  }

  Semantics.addConstantExpr(E, Result);
  return Result;
}


ConstValue *Evaluator::VisitBinPhpIdent(const BinaryOperator *E) {
  ConstValue *LHS = Visit(E->getLHS());
  if (!LHS)
    return nullptr;

  ConstValue *RHS = Visit(E->getRHS());
  if (!RHS)
    return nullptr;

  if (!LHS || !RHS)
    return nullptr;

  auto Result = Constants.get(LHS->equals_strict(RHS));
  Semantics.addConstantExpr(E, Result);
  return Result;
}


static bool diagnoseKeyDuplicates(Sema &S, ArrayConstValue Array,
                                  const PhpArrayExpr *E) {
  bool IsInvalid = false;
  for (size_t I = 0; I < Array.size(); ++I) {
    for (size_t J = I + 1; J < Array.size(); ++J) {
      const ConstValue *IVal = Array.getKey(I);
      const ConstValue *JVal = Array.getKey(J);
      if (IVal->equals_strict(JVal)) {
        const Expr *IBase = E->getElement(I);
        const Expr *JBase = E->getElement(J);
        auto Msg = diag::note_php_previous_implicit_value;
        if (isa<PhpMapExpr>(IBase)) {
          IBase = cast<PhpMapExpr>(IBase)->getLHS();
          Msg = diag::note_php_previous_value;
        }
        assert(IBase && JBase);
        S.Diag(JBase->getExprLoc(), diag::err_php_array_key_duplicate);
        S.Diag(IBase->getExprLoc(), Msg);
        IsInvalid = true;
        Array.erase(J);
        --J;
      }
    }
  }
  return IsInvalid;
}


ConstValue *Evaluator::VisitPhpArrayExpr(const PhpArrayExpr *E) {
  SmallVector<ArrayItem, 16> ArrayContent;
  for (const Expr *Item : E->items()) {
    ConstValue *IV = Visit(Item);
    if (!IV)
      return nullptr;
    if (IV->hasKey()) {
      ConstValue *K = IV->takeKey();
      ArrayContent.push_back(ArrayItem(K, IV));
    } else {
      ArrayContent.push_back(ArrayItem(IV));
    }
  }
  // If we reach here, the array is constant.
  ConstValue *ResValue = Constants.get(ArrayContent);
  if (diagnoseKeyDuplicates(Semantics, ResValue->getArray(), E))
    return cancelEvaluation(E, true);

  Semantics.addConstantExpr(E, ResValue);
  return ResValue;
}


static void checkArrayKeyImplicitCast(Sema &S, SourceLocation Loc, ConstValue *Key) {
  ConstValue::ConversionStatus Status;
  if (Key->isNull()) {
    Key->set(std::string(""));
    S.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << S.getPHPTypeName(S.Context.VoidTy)
      << S.getPHPTypeName(Runtime::getStringType());

  } else if (Key->isBool()) {
    Key->set(Key->getAsInt(Status));
    S.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << S.getPHPTypeName(S.Context.BoolTy)
      << S.getPHPTypeName(Runtime::getIntValueType());

  } else if (Key->isFloat()) {
    Key->set(Key->getAsInt(Status));
    S.Diag(Loc, diag::warn_php_implicit_type_conversion)
      << S.getPHPTypeName(S.Context.FloatTy)
      << S.getPHPTypeName(Runtime::getIntValueType());

  } else if (Key->isString()) {
    conversion::NumberRecognizer<> Recog(0U);
    Recog.recognize(Key->getString().data(), Key->getString().size());
    // String can be converted to integer only if it contains only integer
    // value with optional '-' prefix.
    if (Recog.success() &&
        Recog.get_leading_ws() == 0 && Recog.get_trailing_ws() == 0 &&
        !Recog.has_plus()) {
      // TODO: read with range control.
      int64_t IVal;
      Recog.read(IVal);
      Key->set(APInt(Runtime::getIntValueWidth(), IVal));
      S.Diag(Loc, diag::warn_php_implicit_type_conversion)
        << S.getPHPTypeName(Runtime::getStringType())
        << S.getPHPTypeName(Runtime::getIntValueType());
    }
  }
}


ConstValue *Evaluator::VisitPhpMapExpr(const PhpMapExpr *E) {
  ConstValue *Key = Visit(E->getLHS());
  if (!Key)
    return nullptr;
  assert(Key);

  checkArrayKeyImplicitCast(Semantics, E->getLHS()->getExprLoc(), Key);
  assert(Key->isInt() || Key->isString());

  ConstValue *Value = Visit(E->getRHS());
  if (!Value)
    return nullptr;
  assert(Value);
  Value->setKey(Key);
  return Value;
}


ConstValue *Evaluator::VisitIntegerLiteral(const IntegerLiteral *E) {
  ConstValue *V = Constants.get(Semantics.alignToIntValueWidth(E)->getValue());
  Semantics.addConstantExpr(E, V);
  return V;
}


ConstValue *Evaluator::VisitStringLiteral(const StringLiteral *E) {
  ConstValue *V = Constants.get(E->getBytes());
  Semantics.addConstantExpr(E, V);
  return V;
}


ConstValue *Evaluator::VisitFloatingLiteral(const FloatingLiteral *E) {
  ConstValue *V = Constants.get(E->getValue());
  Semantics.addConstantExpr(E, V);
  return V;
}


ConstValue *Evaluator::VisitPhpNullLiteral(const PhpNullLiteral *E) {
  ConstValue *V = Constants.getNull();
  Semantics.addConstantExpr(E, V);
  return V;
}


ConstValue *Evaluator::VisitCXXBoolLiteralExpr(const CXXBoolLiteralExpr *E) {
  ConstValue *V = Constants.get(E->getValue());
  Semantics.addConstantExpr(E, V);
  return V;
}


ConstValue *Evaluator::VisitConditionalOperator(const ConditionalOperator *E) {
  ConstValue *CondValue = Visit(E->getCond());
  if (!CondValue)
    return nullptr;

  ConstValue::ConversionStatus Status;
  if (CondValue->getAsBool(Status)) {
    ConstValue *TrueValue = Visit(E->getTrueExpr());
    if (!TrueValue)
      return nullptr;
    Semantics.addConstantExpr(E, TrueValue);
    return TrueValue;
  }
  ConstValue *FalseValue = Visit(E->getFalseExpr());
  if (!FalseValue)
    return nullptr;
  Semantics.addConstantExpr(E, FalseValue);
  return FalseValue;
}


ConstValue *Evaluator::VisitBinaryConditionalOperator(
                                           const BinaryConditionalOperator *E) {
  ConstValue *CondValue = Visit(E->getCond());
  if (!CondValue)
    return nullptr;

  ConstValue::ConversionStatus Status;
  if (CondValue->getAsBool(Status)) {
    Semantics.addConstantExpr(E, CondValue);
    return CondValue;
  }
  ConstValue *FalseValue = Visit(E->getFalseExpr());
  if (!FalseValue)
    return nullptr;
  Semantics.addConstantExpr(E, FalseValue);
  return FalseValue;
}

}
