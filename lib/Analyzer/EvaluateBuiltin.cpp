//===--- EvaluateBuiltin.cpp --- AST Transformation component ---*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of Visit handlers, which is responsible constant 
//  evaluation of builtin functions at compile time.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "shared/constants.h"
#include "shared/functions.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


ConstValue *evaluateAbs(Evaluator &E, const BuiltinInfo &FInfo,
                        const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  ConstValue *Result = nullptr;
  const Expr *ArgExpr = CE->getArg(0);
  auto Arg = E.Visit(ArgExpr);
  if (!Arg)
    return nullptr;

  auto &Constants = E.getSema().getConstants();
  SourceLocation Loc = ArgExpr->getExprLoc();

  if (Arg->isString())
    Arg = E.convertToNumber(Arg, Loc);

  if (Arg->isArray()) {
    E.getSema().Diag(Loc, diag::err_php_invalid_argument_type)
      << E.getSema().getPHPTypeName(Runtime::getNumberBoxType())
      << E.getSema().getPHPTypeName(Runtime::getArrayType())
      << ArgExpr->getSourceRange();
    E.cancelEvaluation(ArgExpr, true);
    return nullptr;
  }

  switch (Arg->getKind()) {
  case ConstValue::Null:
    E.getSema().Diag(Loc, diag::warn_php_implicit_type_conversion)
      << E.getSema().getPHPTypeName(Runtime::getVoidType())
      << E.getSema().getPHPTypeName(Runtime::getIntValueType())
      << ArgExpr->getSourceRange();
    Result = Constants.getZero();
    break;

  case ConstValue::Bool:
    E.getSema().Diag(Loc, diag::warn_php_implicit_type_conversion)
      << E.getSema().getPHPTypeName(Runtime::getBoolType())
      << E.getSema().getPHPTypeName(Runtime::getIntValueType())
      << ArgExpr->getSourceRange();
    Result = Constants.getIntBool(Arg->getBool());
    break;

  case ConstValue::Int:
    Result = Arg->getInt().isNegative()
      ? Constants.get(-Arg->getInt())
      : Arg;
    break;

  case ConstValue::Float:
    Result = Arg;
    if (Arg->getFloat().isNegative()) {
      APFloat Val = Arg->getFloat();
      Val.changeSign();
      Result = Constants.get(Val);
    }
    break;

  default:
    llvm_unreachable("unexepcted const value type");
  }

  if (Result)
    E.getSema().addConstantExpr(CE, Result);
  return Result;
}


ConstValue *evaluateMin(Evaluator &E, const BuiltinInfo &FInfo,
                        const CallExpr *CE) {
  ConstValue *Min = nullptr;
  for (auto Arg : CE->arguments()) {
    auto Res = E.Visit(Arg);
    if (!Res)
      return nullptr;
    if (!Min || Min->compare(Res) == CompareResult::Greater)
      Min = Res;
  }
  return Min;
}


ConstValue *evaluateMax(Evaluator &E, const BuiltinInfo& FInfo,
                        const CallExpr* CE) {
  ConstValue *Max = nullptr;
  for (auto Arg : CE->arguments()) {
    auto Res = E.Visit(Arg);
    if (!Res)
      return nullptr;
    if (!Max || Max->compare(Res) == CompareResult::Less)
      Max = Res;
  }
  return Max;
}


APFloat::roundingMode getRoundingForBuiltin(const BuiltinInfo &BI) {
  StringRef Name = BI.Names.get(0);
  if (Name.equals("ceil"))
    return APFloat::rmTowardPositive;
  assert(Name.equals("floor"));
  return APFloat::rmTowardNegative;
}

// return true on error
static bool diagnoseIntConversion(Sema &S, ConstValue *CV, const Expr *Arg) {
  if (CV->isArray() || CV->isNull()) {
    S.Diag(Arg->getExprLoc(), diag::err_php_invalid_argument_type)
      << S.getPHPTypeName(Runtime::getIntValueType())
      << S.getPHPTypeName(CV->isArray()
        ? Runtime::getArrayBoxType()
        : Runtime::getVoidType())
      << Arg->getSourceRange();
    return true;
  }
  if (!CV->isInt()) {
    S.Diag(Arg->getExprLoc(), diag::warn_php_implicit_type_conversion)
      << S.getPHPTypeName(Arg->getType())
      << S.getPHPTypeName(Runtime::getIntValueType())
      << Arg->getSourceRange();
  }
  return false;
}


static ConstValue *evaluateRound(Evaluator &E, const BuiltinInfo &FInfo, 
                                 const CallExpr *CE, APFloat Val) {
  ConstValue *Precision;
  ConstValue *Mode;
  const Expr *ModeExpr = nullptr;
  if (CE->getNumArgs() >= 2) {
    const Expr *Arg = CE->getArg(1);
    Precision = E.Visit(Arg);
    if (Precision == nullptr)
      return nullptr;
    if (diagnoseIntConversion(E.getSema(), Precision, Arg))
      return E.cancelEvaluation(Arg, true);
  } else {
    Precision = E.getSema().getConstants().getZero();
  }
  if (CE->getNumArgs() == 3) {
    ModeExpr = CE->getArg(2);
    Mode = E.Visit(ModeExpr);
    if (Mode == nullptr)
      return nullptr;
    if (diagnoseIntConversion(E.getSema(), Mode, ModeExpr))
      return E.cancelEvaluation(ModeExpr, true);
  } else {
    Mode = E.getSema().getConstants().get(
      APInt(Runtime::getIntValueWidth(), FInfo.Params[2].Def.getLongValue()));
  }

  ConstValue::ConversionStatus Status;
  auto PrecisionVal = Precision->getAsInt(Status).getLimitedValue();
  auto ModeVal = (RoundingMode)Mode->getAsInt(Status).getLimitedValue();
  
  if (!(RoundingMode::HalfUp <= ModeVal && ModeVal <= RoundingMode::HalfOdd)) {
    Sema &S = E.getSema();
    S.Diag(ModeExpr->getExprLoc(), diag::warn_php_invalid_rounding_mode)
      << ModeExpr->getSourceRange();
  }

  double Result = phpfe::round(Val.convertToDouble(), PrecisionVal, ModeVal);
  return E.getSema().getConstants().get(APFloat(Result));
}


ConstValue *evaluateRounding(Evaluator &E, const BuiltinInfo &FInfo, 
                             const CallExpr *CE) {
  assert(CE->getNumArgs() >= 1);
  const Expr *ArgExpr = CE->getArg(0);
  if (ConstValue *Arg = E.Visit(ArgExpr)) {
    auto &Constants = E.getSema().getConstants();
    if (Arg->isNull() || Arg->isBool() || Arg->isInt()) {
      Sema &S = E.getSema();
      S.Diag(ArgExpr->getExprLoc(), diag::warn_php_already_integer_argument)
        << ArgExpr->getSourceRange();
    }
    APFloat Val = E.convertToFloat(Arg, ArgExpr->getExprLoc());
    if (FInfo.Names.get(0).equals("round"))
      return evaluateRound(E, FInfo, CE, Val);
    APSInt IntVal(Runtime::getIntValueWidth(), false);
    bool Exact;
    auto Rounding = getRoundingForBuiltin(FInfo);
    Val.convertToInteger(IntVal, Rounding, &Exact);
    Val.convertFromAPInt(IntVal, true, Rounding);
    return Constants.get(Val);
  }
  return nullptr;
}


ConstValue *evaluateFmod(Evaluator &E, const BuiltinInfo &FInfo,
                         const CallExpr *CE) {
  assert(CE->getNumArgs() == 2);

  const Expr *ArgX = CE->getArg(0);
  const Expr *ArgY = CE->getArg(1);

  ConstValue *CVX = E.evaluate(ArgX);
  if (!CVX)
    return nullptr;
  ConstValue *CVY = E.evaluate(ArgY);
  if (!CVY)
    return nullptr;
  APFloat XVal = E.convertToFloat(CVX, ArgX->getExprLoc());
  APFloat YVal = E.convertToFloat(CVY, ArgY->getExprLoc());
  auto Status = XVal.mod(YVal);

  Sema &S = E.getSema();
  if (Status == APFloat::opDivByZero) {
    S.Diag(ArgY->getExprLoc(), diag::err_php_division_by_zero)
      << ArgY->getSourceRange();
    return E.cancelEvaluation(ArgY, true);
  }

  auto Result = S.getConstants().get(XVal);
  S.addConstantExpr(CE, Result);
  return Result;
}


ConstValue *evaluateIntdiv(Evaluator &E, const BuiltinInfo &FInfo,
                           const CallExpr *CE) {
  assert(CE->getNumArgs() == 2);

  const Expr *ArgX = CE->getArg(0);
  const Expr *ArgY = CE->getArg(1);

  ConstValue *CVX = E.evaluate(ArgX);
  if (!CVX)
    return nullptr;
  ConstValue *CVY = E.evaluate(ArgY);
  if (!CVY)
    return nullptr;
  APInt XVal = E.convertToInt(CVX, ArgX->getExprLoc());
  APInt YVal = E.convertToInt(CVY, ArgY->getExprLoc());
  
  Sema &S = E.getSema();
  if (YVal == 0) {
    S.Diag(ArgY->getExprLoc(), diag::err_php_division_by_zero)
      << ArgY->getSourceRange();
    return E.cancelEvaluation(ArgY, true);
  }

  if (XVal.isMinSignedValue() && YVal == -1) {
    S.Diag(CE->getExprLoc(), diag::warn_php_int_overflow)
      << ArgX->getSourceRange()
      << ArgY->getSourceRange();
  }
  auto Result = S.getConstants().get(XVal.sdiv(YVal));
  S.addConstantExpr(CE, Result);
  return Result;
}


ConstValue *evaluateIsInt(Evaluator &E, const BuiltinInfo& FInfo,
                          const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  auto Res = E.Visit(Arg);
  if (!Res)
    return nullptr;
  if (Res->isInt())
    return E.getSema().getConstants().getTrue();
  else
    return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateIsFloat(Evaluator &E, const BuiltinInfo& FInfo,
                            const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  auto Res = E.Visit(Arg);
  if (!Res)
    return nullptr;
  if (Res->isFloat())
    return E.getSema().getConstants().getTrue();
  else
    return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateIsBool(Evaluator &E, const BuiltinInfo& FInfo,
                           const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  auto Res = E.Visit(Arg);
  if (!Res)
    return nullptr;
  if (Res->isBool())
    return E.getSema().getConstants().getTrue();
  else
    return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateIsString(Evaluator &E, const BuiltinInfo& FInfo,
                             const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  auto Res = E.Visit(Arg);
  if (!Res)
    return nullptr;
  if (Res->isString())
    return E.getSema().getConstants().getTrue();
  else
    return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateIsArray(Evaluator &E, const BuiltinInfo& FInfo,
                            const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  auto Res = E.Visit(Arg);
  if (!Res)
    return nullptr;
  if (Res->isArray())
    return E.getSema().getConstants().getTrue();
  else
    return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateIsNumeric(Evaluator &E, const BuiltinInfo &FInfo,
                              const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  ConstValue *Res = E.Visit(Arg);
  if (!Res)
    return nullptr;
  if (Res->isInt() || Res->isFloat())
    return E.getSema().getConstants().getTrue();

  if (Res->isString()) {
    bool HasExtra = false;
    if (Res->isStringNumeric(HasExtra)) {
      if (!HasExtra)
        return E.getSema().getConstants().getTrue();
    }
  }
  return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateIsScalar(Evaluator &E, const BuiltinInfo &FInfo,
                             const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  auto Res = E.Visit(Arg);
  if (!Res)
    return nullptr;

  if (Res->isInt() || Res->isFloat() ||
      Res->isBool() || Res->isString())
    return E.getSema().getConstants().getTrue();

  return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateIsObject(Evaluator &E, const BuiltinInfo &FInfo,
                             const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  auto Res = E.Visit(Arg);
  if (!Res)
    return nullptr;
  // A constant can't be object
  return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateIsNull(Evaluator &E, const BuiltinInfo &FInfo,
                           const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  auto Res = E.Visit(Arg);
  if (!Res)
    return nullptr;
  if (Res->isNull())
    return E.getSema().getConstants().getTrue();
  else 
    return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateIsResource(Evaluator &E, const BuiltinInfo &FInfo,
                               const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  auto Res = E.Visit(Arg);
  if (!Res)
    return nullptr;
  // Constant value can't be an resource
  return E.getSema().getConstants().getFalse();
}


ConstValue *evaluateGetType(Evaluator &E, const BuiltinInfo &FInfo,
                            const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  ConstValue *Res = E.Visit(Arg);
  if (!Res)
    return nullptr;

  auto& CT = E.getSema().getConstants();

  if (Res->isInt())
    return CT.get(std::string("integer"));
  if (Res->isFloat())
    return CT.get(std::string("double")); // double is used for historic reasons
  if (Res->isNull())
    return CT.get(std::string("NULL"));
  if (Res->isString())
    return CT.get(std::string("string"));
  if (Res->isArray())
    return CT.get(std::string("array"));
  if (Res->isBool())
    return CT.get(std::string("boolean"));
  return nullptr;
}


ConstValue *evaluateEmpty(Evaluator &E, const BuiltinInfo &FInfo,
                          const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  ConstValue *Res = E.Visit(Arg);
  if (!Res)
    return nullptr;

  auto& CT = E.getSema().getConstants();

  ConstValue::ConversionStatus Status;
  bool boolVal = Res->getAsBool(Status);

  return boolVal ? CT.getFalse() : CT.getTrue();
}


ConstValue *evaluateIntVal(Evaluator &E, const BuiltinInfo &FInfo,
                           const CallExpr *CE) {
  assert(CE->getNumArgs() >= 1 && CE->getNumArgs() <= 2);
  Sema& S = E.getSema();
  const Expr *Arg = CE->getArg(0);
  ConstValue *Res = E.Visit(Arg);
  if (!Res)
    return nullptr;

  if (CE->getNumArgs() == 2) {
    const Expr *Base = CE->getArg(1);
    ConstValue *BaseRes = E.Visit(Base);
    if (!BaseRes)
      return nullptr;

    // If value passed to intval function is not string constant
    // base parameter has no effect
    if (!Res->isString())
      E.getSema().Diag(Base->getExprLoc(), diag::warn_php_intval_base_unused);

    ConstValue::ConversionStatus Status;
    APInt BaseVal = BaseRes->getAsInt(Status);
    S.diagnoseConversion(Status, Base->getLocStart());
    APInt IntVal = Res->getAsInt(Status, (long)BaseVal.getLimitedValue());
    S.diagnoseConversion(Status, Arg->getLocStart());
    return E.getSema().getConstants().get(IntVal);
  }

  // We need to perform this conversion even when value if integer - to ensure
  // that it fits into intvalue_t.
  ConstValue::ConversionStatus Status;
  APInt IntVal = Res->getAsInt(Status);
  S.diagnoseConversion(Status, Arg->getLocStart());
  return S.getConstants().get(IntVal);
}


ConstValue *evaluateFloatVal(Evaluator &E, const BuiltinInfo &FInfo,
                             const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  Sema& S = E.getSema();
  const Expr *Arg = CE->getArg(0);
  ConstValue *Res = E.Visit(Arg);
  if (!Res)
    return nullptr;

  if (Res->isFloat())
    return Res;

  ConstValue::ConversionStatus Status;
  APFloat FloatVal = Res->getAsFloat(Status);
  S.diagnoseConversion(Status, Arg->getLocStart());
  return S.getConstants().get(FloatVal);
}


ConstValue *evaluateBoolVal(Evaluator &E, const BuiltinInfo &FInfo,
                            const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  ConstValue *Res = E.Visit(Arg);
  if (!Res)
    return nullptr;

  if (Res->isBool()) 
    return Res;

  auto& CT = E.getSema().getConstants();

  // TODO: possible display warning?
  ConstValue::ConversionStatus Status;
  bool boolVal = Res->getAsBool(Status);
  return boolVal ? CT.getTrue() : CT.getFalse();
}


ConstValue *evaluateStrVal(Evaluator &E, const BuiltinInfo &FInfo,
                           const CallExpr *CE) {
  assert(CE->getNumArgs() == 1);
  const Expr *Arg = CE->getArg(0);
  ConstValue *Res = E.Visit(Arg);
  if (!Res)
    return nullptr;

  if (Res->isString())
    return Res;

  ConstValue::ConversionStatus Status;
  std::string StrVal = Res->getAsString(Status);
  return E.getSema().getConstants().get(StrVal);
}

}
