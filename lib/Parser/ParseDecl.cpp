//===--- PparseDecl.cpp - PHP Declaration Parser --------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of PHP declaration parsing.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ExprCXX.h"
#include "clang/Basic/PrettyStackTrace.h"
#include "clang/Parse/ParseDiagnostic.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/Sema/PrettyDeclStackTrace.h"
#include "phpfe/Parser/PhpParser.h"

#include "Parse/RAIIObjectsForParser.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


// \brief Parses declaration of PHP function or lambda function expression.
//
// \verbatim
//
// function_declaration_statement:
//   function returns_ref T_STRING '(' parameter_list ')' return_type
//   backup_doc_comment '{' inner_statement_list '}'
//
// expr
// | function returns_ref '(' parameter_list ')' lexical_vars return_type
//   backup_doc_comment '{' inner_statement_list '}'
// | T_STATIC function returns_ref '(' parameter_list ')' lexical_vars
//   return_type backup_doc_comment '{' inner_statement_list '}'
//
// function:
//   T_FUNCTION
//
// returns_ref:
//     /* empty */
//   | '&'
//
// return_type:
//    /* empty */
//  | ':' type
//
// type:
//    T_ARRAY
//  | T_CALLABLE
//  | name
//
// parameter_list:
//     non_empty_parameter_list
//   | /* empty */
//
// non_empty_parameter_list:
//     parameter
//   | non_empty_parameter_list ',' parameter
//
// parameter:
//     optional_type is_reference is_variadic T_VARIABLE
//   | optional_type is_reference is_variadic T_VARIABLE '=' expr
//
// optional_type:
//     /* empty */
//   | T_ARRAY
//   | T_CALLABLE
//   | name
//
// is_reference:
//     /* empty */
//   | '&'
//
// is_variadic:
//     /* empty */
//   | T_ELLIPSIS
//
// backup_doc_comment:
//   /* empty */
//
// \endverbatim
//
StmtResult Parser::parseFunctionDeclaration(bool ParsingLambda) {
  assert((!ParsingLambda && Tok.is(tok::kw_function)) ||
         (ParsingLambda && isStartOfLambdaFunction()));

  Sema::FunctionParseInfo FInfo;
  bool IsStaticAssert = false;

  if (ParsingLambda && Tok.is(tok::kw_static))
    FInfo.StaticLoc = ConsumeToken();

  FInfo.FnLoc = ConsumeToken();

  if (Tok.is(tok::amp))
    FInfo.RefLoc = ConsumeToken();

  if (!ParsingLambda) {
    if (Tok.is(tok::kw_static_assert)) {
      IsStaticAssert = true;
      Diag(ConsumeToken(), diag::warn_php_static_assert_definition);
    } else {
      if (Tok.isNot(tok::identifier)) {
        Diag(Tok, diag::err_expected) << "function name";
        skipBlock();
        return StmtError();
      }
      FInfo.Name.setName(DeclarationName(Tok.getIdentifierInfo()));
      FInfo.Name.setLoc(ConsumeToken());
    }
  }

  if (parseParameterList(FInfo.LParen, FInfo.Params, FInfo.RParen)) {
    skipBlock();
    return StmtError();
  }

  if (ParsingLambda && Tok.is(tok::kw_use)) {
    FInfo.UseLoc = ConsumeToken();
    if (parseLexicalVariableList()) {
      skipBlock();
      return StmtError();
    }
  }

  if (Tok.is(tok::colon)) {
    ConsumeToken();
    FInfo.ReturnTypeHint = parseHintType(false);
    if (Tok.is(tok::amp)) {
      Diag(Tok, diag::err_php_return_type_reference)
        << FixItHint::CreateRemoval(ConsumeToken())
        << FixItHint::CreateInsertion(PP.getLocForEndOfToken(FInfo.FnLoc), "&");
    }
    else if (FInfo.ReturnTypeHint.isNull()) {
      Diag(Tok, diag::err_expected) << "type name";
      SkipUntil(tok::l_brace,
        Parser::StopBeforeMatch | Parser::StopAtSemi);
    }
  }
  FInfo.ReturnTypeHint =
    getActions().ActOnReturnTypeHint(FInfo.RefLoc, FInfo.ReturnTypeHint);

  FunctionDecl *FDecl = ParsingLambda
    ? getActions().ActOnClosureDeclaration(FInfo)
    : IsStaticAssert
      ? nullptr
      : getActions().ActOnFunctionDeclaration(getCurScope(), FInfo);

  if (Tok.isNot(tok::l_brace)) {
    Diag(Tok, diag::err_expected) << tok::l_brace;
    skipBlock();
    return StmtError();
  }

  ParseScope FnScope(this, Scope::DeclScope | Scope::FnScope);
  if (FDecl)
    getActions().ActOnFunctionDefinitionStart(FDecl);
  StmtResult Body = parseCompoundStmt();
  if (FDecl)
    getActions().ActOnFunctionDefinitionEnd();
  FnScope.Exit();

  if (FDecl && Body.isUsable())
    return getActions().ActOnFunctionDefinition(FDecl, Body.get());

  if (IsStaticAssert)
    return StmtEmpty();

  return StmtError();
}

// parse type hint for parameter or function return value
//
QualType Parser::parseHintType(bool IsParameter) {
  QualType Ty;
  switch (Tok.getKind()) {
  case tok::kw_array:
  case tok::kw_callable:
  case tok::identifier: {
    DeclarationName DN(Tok.getIdentifierInfo());
    DeclarationNameInfo DNI(DN, ConsumeToken());
    Ty = getActions().ActOnPrimitiveTypeHint(DNI);
    if (!Ty.isNull())
      break;
    // Otherwise try 'name'.
  }
  // Fall through

  case tok::backslash:
  case tok::kw_namespace: {
    CXXScopeSpec SS;
    if (!parseScopeSpecifier(SS, /*IsScopeName=*/true))
      return getActions().getTypeForSpec(SS);
    // Error must be already reported, recover by applying default type.
  }
  // Fall through

  case tok::amp:
  case tok::dollar:
  case tok::ellipsis:
    // No type hint was specified.
    if (IsParameter)
      Ty = getActions().ActOnDefaultTypeHint();
    break;

  default:
    break;
  }

  return Ty;
}


// Parser function parameter list, from opening paren to closing.
//
// parameter_list
// : non_empty_parameter_list
// | /* empty */
//
// non_empty_parameter_list
// : parameter
// | non_empty_parameter_list ',' parameter
//
bool Parser::parseParameterList(SourceLocation &LParen,
                                SmallVectorImpl<ParmVarDecl *> &Params,
                                SourceLocation &RParen) {
  BalancedDelimiterTracker T(*this, tok::l_paren);
  if (T.consumeOpen()) {
    Diag(Tok, diag::err_expected) << tok::l_paren;
    return true;
  }
  LParen = T.getOpenLocation();

  // Parse function parameters in special scope. If we define parameter '$a',
  // we must not see global variable '$a'.
  ParseScope FnScope(this, Scope::FunctionPrototypeScope |
                     Scope::FunctionDeclarationScope | Scope::DeclScope);
  SourceLocation EllipsisLoc;

  bool IsInvalid = false;
  if (Tok.isNot(tok::r_paren)) {
    do {
      ParmVarDecl *Param = parseParameter(EllipsisLoc);
      if (Param)
        Params.push_back(Param);
      else
        IsInvalid = true;
    } while (TryConsumeToken(tok::comma));
  }

  if (T.consumeClose())
    IsInvalid = true;
  RParen = T.getCloseLocation();

  return IsInvalid;
}


// Parses function parameter.
// Returns parameter declaration or nullptr, if error occurred.
//
// parameter
// : optional_type is_reference is_variadic T_VARIABLE
// | optional_type is_reference is_variadic T_VARIABLE '=' expr
//
// optional_type
// : /* empty */
// | T_ARRAY
// | T_CALLABLE
// | name

// is_reference
// : /* empty */
// | '&'
//
// is_variadic
// : /* empty */
// | T_ELLIPSIS
//
ParmVarDecl *Parser::parseParameter(SourceLocation &EllipsisLoc) {
  bool HasError = false;

  // TODO: parse qualified name, cannot determine type now?
  QualType ParamType = parseHintType();
  if (ParamType.isNull()) {
    Diag(Tok, diag::err_php_expected_parameter);
    SkipUntil(tok::comma, tok::r_paren,
              Parser::StopBeforeMatch | Parser::StopAtSemi);
    return nullptr;
  }
  if (Tok.is(tok::amp)) {
    SourceLocation RefLoc = ConsumeToken();  // &
    ParamType = getActions().ActOnReferenceTypeHint(RefLoc, ParamType);
  }

  if (EllipsisLoc.isValid()) {
    Diag(Tok, diag::err_php_ellipsis_not_the_last);
    Diag(EllipsisLoc, diag::note_previous_declaration);
    HasError = true;
  }
  if (Tok.is(tok::ellipsis))
    EllipsisLoc = ConsumeToken();

  if (checkParameterName(HasError))
    return nullptr;

  IdentifierInfo *NameId = Tok.getIdentifierInfo();
  SourceLocation NameLoc = ConsumeToken();

  SourceLocation AssignLoc;
  ExprResult DefVal;
  if (Tok.is(tok::equal)) {
    if (EllipsisLoc.isValid()) {
      Diag(Tok, diag::err_php_variadic_default_value);
      HasError = true;
    }
    AssignLoc = ConsumeToken();
    DefVal = parseExpression();
  }

  if (HasError)
    return nullptr;

  ParmVarDecl *Param = getActions().ActOnFunctionParam(getCurScope(), NameLoc,
                                               EllipsisLoc, ParamType, NameId);
  if (!Param)
    return nullptr;

  if (AssignLoc.isInvalid())
    return Param;

  if (!DefVal.isUsable() ||
      getActions().ActOnParamDefaultValue(AssignLoc, Param, DefVal.get()))
      return nullptr;

  return Param;
}


//
bool Parser::checkParameterName(bool &HasError) {
  if (Tok.is(tok::dollar)) {
    ConsumeToken();
  } else {
    if (Tok.isNot(tok::identifier)) {
      Diag(Tok, diag::err_expected) << "parameter name";
      SkipUntil(tok::r_paren, SkipUntilFlags::StopBeforeMatch);
      return true;
    }
    Diag(Tok, diag::err_expected) << tok::dollar
      << FixItHint::CreateInsertion(Tok.getLocation(), "$");
    HasError = true;
    // Recover by assuming '$'.
  }

  if (Tok.isNot(tok::identifier)) {
    Diag(Tok, diag::err_expected) << tok::identifier;
    SkipUntil(tok::r_paren, SkipUntilFlags::StopBeforeMatch);
    return true;
  }
  return false;
}


// lexical_var_list:
//    lexical_var_list ',' lexical_var
//  | lexical_var
//
// lexical_var:
//    T_VARIABLE
//  | '&' T_VARIABLE
//
// returns true on error
bool Parser::parseLexicalVariableList() {
  BalancedDelimiterTracker T(*this, tok::l_paren);
  if (T.consumeOpen()) {
    Diag(Tok, diag::err_expected) << tok::l_paren;
    return true;
  }

  bool HasError = false;
  do {
    HasError |= parseLexicalVariable();
  } while (TryConsumeToken(tok::comma));

  if (T.consumeClose())
    HasError = true;

  return HasError;
}


// returns true on error
bool Parser::parseLexicalVariable() {
  SourceLocation RefLoc;
  if (Tok.is(tok::amp))
    RefLoc = ConsumeToken();

  bool HasError = false;
  if (checkParameterName(HasError))
    return true;

  assert(Tok.is(tok::identifier));

  IdentifierInfo *II = Tok.getIdentifierInfo();
  SourceLocation NameLoc = ConsumeToken();

  if (HasError)
    return true;

  return getActions().ActOnClosureVariableUse(RefLoc, NameLoc, II);
}


StmtResult Parser::parseStaticAssert() {
  assert(Tok.is(tok::kw_static_assert));
  SourceLocation KWLoc = ConsumeToken();

  BalancedDelimiterTracker T(*this, tok::l_paren);
  if (T.consumeOpen()) {
    Diag(Tok, diag::err_expected) << tok::l_paren;
    return StmtError();
  }

  ExprResult E = parseExpression();
  if (!E.isUsable()) {
    SkipUntil(tok::comma, tok::r_paren,
              Parser::StopAtSemi | Parser::StopBeforeMatch);
  }

  StringLiteral *Message = nullptr;
  if (Tok.is(tok::comma)) {
    ConsumeToken();
    ExprResult MessageExpr = parseExpression();
    if (MessageExpr.isUsable() && !isa<StringLiteral>(MessageExpr.get()))
      Diag(MessageExpr.get()->getExprLoc(), diag::err_expected)
        << "string literal" << MessageExpr.get()->getSourceRange();
    else
      Message = cast<StringLiteral>(MessageExpr.get());
  }

  if (T.consumeClose() || E.isInvalid())
    return StmtError();

  return getActions().ActOnStaticAssert(KWLoc, E.get(), Message,
                                        T.getCloseLocation());
}


Decl *Parser::parseNamespace() {
  assert(Tok.is(tok::kw_namespace));
  assert(getCurScope()->getEntity());
  assert(getCurScope()->getEntity() == getActions().CurContext);
#ifdef _DEBUG
  // Current scope may be a scope of unit function or a scope of global
  // namespace.
  if (getCurScope()->isFunctionScope()) {
    assert(getCurScope()->getEntity() == getActions().getUnitFunction());
  } else {
    assert(getCurScope()->getFlags() == Scope::DeclScope);
    assert(getCurScope()->getEntity() == getActions().getPhpNamespace());
  }
#endif

  SourceLocation KWLoc = ConsumeToken();
  CXXScopeSpec SS;
  if (!Tok.isOneOf(tok::identifier, tok::backslash, tok::l_brace)) {
    Diag(Tok, diag::err_php_expected_namespace_name);
    skipBlock(true);
    return nullptr;
  }

  if (Tok.isNot(tok::l_brace)) {
    if (parseScopeSpecifier(SS, true)) {
      skipBlock(true);
      return nullptr;
    }
  }

  if (Tok.is(tok::semi))
    ;
  else if (Tok.is(tok::l_brace)) {
    // Diagnose mixing namespace formats.
    if (getActions().areNamespacesSimple()) {
      Diag(KWLoc, diag::err_php_mixed_namespaces);
      Diag(getActions().getNamespaceStartLoc(),
           diag::note_php_previous_namespace);
      skipBlock(true);
      return nullptr;
    }

    // Diagnose namespace nesting.
    Scope *S = getCurScope();
    if (S->getEntity() == getActions().getUnitFunction())
      S = S->getParent();
    if (S->getEntity() != getActions().getPhpNamespace()) {
      Diag(KWLoc, diag::err_php_nested_namespace);
      Diag(getActions().getNamespaceStartLoc(), diag::note_php_outer_namespace);
      skipBlock(true);
      return nullptr;
    }
  } else {
    Diag(Tok, diag::err_expected_either) << tok::l_brace << tok::semi;
    skipBlock(true);
    return nullptr;
  }

  // Exit unit function scope. It may be found even in the case of structured
  // syntax, initial namespace statement occurs in the scope of unit function
  // created during sema initialization.
  if (getCurScope()->getEntity() == getActions().getUnitFunction()) {
    ExitScope();
    getActions().leaveUnitFunction();
  }

  Decl *Res = nullptr;
  if (Tok.is(tok::semi)) {
    // Process simple syntax.
    ConsumeToken();

    // Exit user namespace scopes.
    assert(getCurScope()->getEntity());
    assert(getCurScope()->getEntity()->isNamespace());
    assert(getCurScope()->getEntity() == getActions().CurContext);

    if (getCurScope() != getActions().getGlobalNamespaceScope())
      ExitScope();
    assert(getCurScope()->getEntity());
    assert(getCurScope()->getEntity()->isNamespace());
    assert(getCurScope()->getEntity() == getActions().getPhpNamespace());

    // Enter scopes of new namespace and unit function.
    EnterScope(Scope::DeclScope);
    Res = getActions().ActOnNamespaceSwitch(KWLoc, SS);
    EnterScope(Scope::DeclScope | Scope::FnScope);
    getActions().enterUnitFunction();
  } else {
    // Process structured syntax.
    assert(Tok.is(tok::l_brace));
    assert(getCurScope()->getEntity());
    assert(getCurScope()->getEntity()->isNamespace());
    assert(getCurScope()->getEntity() == getActions().getPhpNamespace());

    SourceLocation LBrace = ConsumeBrace();
    ParseScope NSScope(this, Scope::DeclScope, !SS.isEmpty());
    if (Decl *NS = getActions().ActOnNamespaceStart(KWLoc, SS, LBrace)) {
      // Parse namespace content.
      {
        ParseScope UFNScope(this, Scope::DeclScope | Scope::FnScope);
        getActions().enterUnitFunction();
        PrettyDeclStackTraceEntry CrashInfo(Actions, NS, LBrace,
                                            "parsing namespace");
        parseTopStatements();
      }
      if (Tok.is(tok::r_brace)) {
        SourceLocation RBrace = ConsumeBrace();
        getActions().ActOnNamespaceEnd(RBrace);
        Res = NS;
      } else {
        Diag(getActions().getNamespaceStartLoc(),
             diag::err_php_unfinished_namespace);
      }
    } else {
      SkipUntil(tok::r_brace);
    }
  }

  return Res;
}


StmtResult Parser::parseConstantDeclaration() {
  assert(Tok.is(tok::kw_const));

  SourceLocation ConstLoc = ConsumeToken();  // 'const'
  SmallVector<Decl *, 8> ConstDecls;
  SourceLocation EndLoc;

  while (true) {
    // Get constant name
    if (Tok.isNot(tok::identifier)) {
      Diag(Tok, diag::err_expected) << tok::identifier;
      // Recover by skipping till next definition.
      SkipUntil(tok::comma, StopAtSemi | StopBeforeMatch);
      if (Tok.is(tok::comma)) {
        ConsumeToken(); // ','
        continue;
      }
      break;
    }

    DeclarationNameInfo ConstName(Tok.getIdentifierInfo(), Tok.getLocation());
    ConsumeToken(); // identifier

    // Get '='
    if (Tok.isNot(tok::equal)) {
      Diag(Tok, diag::err_expected) << tok::equal;
      // Recover by skipping invalid definition.
      SkipUntil(tok::comma, StopAtSemi | StopBeforeMatch);
      if (Tok.is(tok::comma)) {
        ConsumeToken(); // ','
        continue;
      }
      break;
    }
    SourceLocation EqualLoc = ConsumeToken();  // '='

    // Get constant definition.
    ExprResult InitExpr;
    if (getCurScope()->isClassScope()) {
      llvm_unreachable("not implemented");
    } else {
      // File level constant.
      InitExpr = parseConstantExpression();
    }

    if (InitExpr.isUsable()) {
      VarDecl *ConstDef = getActions().ActOnConstantDefinition(
        ConstName, EqualLoc, InitExpr.get());
      if (ConstDef)
        ConstDecls.push_back(ConstDef);
      EndLoc = InitExpr.get()->getLocEnd();
    }

    // Check declaration separator or statement end.
    if (Tok.is(tok::semi) || Tok.is(tok::eof))
      break;
    if (Tok.isNot(tok::comma)) {
      Diag(Tok, diag::err_expected) << tok::comma;
      SkipUntil(tok::comma, StopAtSemi | StopBeforeMatch);
    }
    if (Tok.isNot(tok::comma))
      break;
    ConsumeToken();  // ','
  }
  parseEndOfStatement("const");

  // Prepare declaration statement.
  if (!ConstDecls.empty())
    return getActions().ActOnConstantDefStatement(ConstLoc, ConstDecls, EndLoc);
  return StmtError();
}

}
