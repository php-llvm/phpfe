//===--- PhpControlStmt.cpp - PHP Statement Parser ------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of PHP control statements parsing
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Parse/ParseDiagnostic.h"
#include "Parse/RAIIObjectsForParser.h"
#include "phpfe/Parser/PhpParser.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


// if_stmt:
//     if_stmt_without_else
//   | if_stmt_without_else T_ELSE statement
// ;
//
// if_stmt_without_else:
//     T_IF '(' expr ')' statement
//   | if_stmt_without_else T_ELSEIF '(' expr ')' statement
// ;
//
//  alt_if_stmt:
//     alt_if_stmt_without_else T_ENDIF ';'
//   | alt_if_stmt_without_else T_ELSE ':' inner_statement_list T_ENDIF ';'
// ;
//
// alt_if_stmt_without_else:
//     T_IF '(' expr ')' ':' inner_statement_list
//   | alt_if_stmt_without_else T_ELSEIF '(' expr ')' ':' inner_statement_list
// ;
//
// inner_statement_list:
//     inner_statement_list inner_statement
//   | /* empty */
// ;
//
StmtResult Parser::parseIfStatement() {
  assert(Tok.is(tok::kw_if));
  SourceLocation IfLoc;

  StmtResult ResultIf;
  StmtResult IfStmt = StmtError();
  ExprResult Cond;
  
  bool ParseAlternativeSyntax = false;
  enum IfState {If, Elseif, Else} State = If;
  while (true) {
    if (State == If || State == Elseif) {
      assert(Tok.isOneOf(tok::kw_if, tok::kw_elseif));
      IfLoc = ConsumeToken();
      BalancedDelimiterTracker T(*this, tok::l_paren);
      if (T.consumeOpen()) {
        Diag(Tok.getLocation(), diag::err_expected) << tok::l_paren;
        Cond = ExprError();
      } else {
        Cond = parseExpression();
        if (T.consumeClose())
          Cond = ExprError();
        if (State == If)
          ParseAlternativeSyntax = Tok.is(tok::colon);
      }
    }

    ParseScope IfScope(this, Scope::DeclScope | Scope::ControlScope);
    StmtResult Then = StmtError();
    if (ParseAlternativeSyntax) {
      if (Tok.isNot(tok::colon)) {
        Diag(Tok, diag::err_expected) << tok::colon;
        SkipUntil(tok::kw_endif, StopAtSemi);
        return StmtError();
      }
      SourceLocation ColonLoc = ConsumeToken();

      SmallVector<Stmt*, 16> ThenStmts;
      bool IsInvalid = false;
      while (Tok.isNot(tok::kw_endif) &&
        Tok.isNot(tok::kw_else) &&
        Tok.isNot(tok::kw_elseif) &&
        Tok.isNot(tok::eof)) {
        StmtResult S = parseStatement();
        if (S.isUsable())
          ThenStmts.push_back(S.get());
        else
          IsInvalid = true;
      }
      if (!IsInvalid) {
        Sema::CompoundScopeRAII CompoundScope(Actions);
        Then = getActions().ActOnCompoundStmt(ColonLoc, Tok.getLocation(), ThenStmts, false);
      }
    } else
      Then = parseStatement();

    StmtResult CIf;
    if (State != Else && Cond.isUsable() && Then.isUsable())
      CIf = getActions().ActOnPHPIfStmt(IfLoc, Cond.get(), Then.get());

    switch (State) {
    case If:
      ResultIf = IfStmt = CIf;
      break;
    case Elseif:
      if (IfStmt.isUsable() && CIf.isUsable())
        IfStmt = getActions().ActOnPHPElseStmt(IfStmt.get(), IfLoc, CIf.get());
      IfStmt = IfStmt.isUsable() ? CIf : StmtError();
      break;
    case Else:
      if (IfStmt.isUsable() && Then.isUsable())
        IfStmt = getActions().ActOnPHPElseStmt(IfStmt.get(), IfLoc, Then.get());
    }

    IfScope.Exit();
    if (State == Else)
      break;
    else if (Tok.is(tok::kw_elseif)) {
      State = Elseif;
    }
    else if (Tok.is(tok::kw_else)) {
      IfLoc = ConsumeToken();
      State = Else;
    } else
      break;
  }

  if (ParseAlternativeSyntax) {
    if (Tok.isNot(tok::kw_endif)) {
      Diag(Tok, diag::err_expected) << tok::kw_endif;
      Diag(IfLoc, diag::note_matching) << 
        (State == If
          ? tok::kw_if
          : State == Elseif
             ? tok::kw_elseif
             : tok::kw_else);
      return StmtError();
    }
    ConsumeToken();
    parseEndOfStatement("alternative if");
  }

  return ResultIf;
}


StmtResult Parser::parseWhileStatement() {
  assert(Tok.is(tok::kw_while));
  SourceLocation WhileLoc = ConsumeToken();
  ExprResult Cond;

  BalancedDelimiterTracker T(*this, tok::l_paren);
  if (T.consumeOpen()) {
    Diag(Tok.getLocation(), diag::err_expected) << tok::l_paren;
    SkipUntil(tok::semi);
    return StmtError();
  }

  // Condition is parsed in its special scope.
  unsigned ScopeFlags = Scope::BreakScope | Scope::ContinueScope |
                        Scope::DeclScope | Scope::ControlScope;
  ParseScope WhileScope(this, ScopeFlags);

  Cond = parseExpression();
  if (T.consumeClose()) {
    Cond = ExprError();
    SkipUntil(tok::semi);
    return StmtError();
  }

  // Body is parsed in another scope.
  ParseScope InnerScope(this, Scope::DeclScope | Scope::ControlScope);

  if (Tok.is(tok::colon)) {
    SourceLocation ColonLoc = ConsumeToken();

    SmallVector<Stmt*, 16> WhileStmts;
    while (Tok.isNot(tok::kw_endwhile) &&
           Tok.isNot(tok::eof)) {
      WhileStmts.push_back(parseStatement().get());
    }

    SourceLocation EndWhileLoc;
    if (Tok.isNot(tok::kw_endwhile)) {
      Diag(Tok, diag::err_expected) << tok::kw_endwhile;
    } else {
      EndWhileLoc = ConsumeToken();
      parseEndOfStatement("endwhile");
    }

    return getActions().ActOnAlternativePhpWhileStmt(WhileLoc,
        T.getOpenLocation(), Cond.get(), T.getCloseLocation(),
        ColonLoc, WhileStmts, EndWhileLoc);
  } else {
    StmtResult WhileStmt = parseStatement();
    if (WhileStmt.isUsable())
      return getActions().ActOnPhpWhileStmt(WhileLoc, T.getOpenLocation(),
                             Cond.get(), T.getCloseLocation(), WhileStmt.get());
  }

  return StmtResult();
}


StmtResult Parser::parseReturnStatement() {
  assert(Tok.is(tok::kw_return));
  SourceLocation Loc = ConsumeToken();
  ExprResult RetVal;
  if (!Tok.is(tok::semi))
    RetVal = parseExpression();

  StmtResult Result = StmtError();
  if (!RetVal.isInvalid())
    Result = getActions().ActOnReturnStmt(Loc, RetVal.get());

  parseEndOfStatement("return");
  return Result;
}

}
