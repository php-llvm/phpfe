//===--- ParsePrimary.cpp -- PHP Parser -----------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Provides Parser methods for parsing primary expressions.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/Parse/ParseDiagnostic.h"
#include "Parse/RAIIObjectsForParser.h"
#include "phpfe/Parser/PhpParser.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


/// \brief Tries to parse PHP nonterminal "variable".
///
//
// expr
// : variable
// | expr_without_variable
//
// variable
// : callable_variable
// | static_member
// | dereferencable "->" property_name
//
// callable_variable
// : simple_variable
// | constant '[' optional_expr ']'
// | function_call
// | dereferencable '[' optional_expr ']'
// | dereferencable '{' expr '}'
// | dereferencable "->" property_name argument_list
//
// static_member
// : class_name "::" simple_variable
// | variable_class_name "::" simple_variable
//
// dereferencable
// : variable
// | '(' expr ')'                                                : (
// | dereferencable_scalar
//
// dereferencable_scalar
// : "array" '(' array_pair_list ')'                             : "array"
// | '[' array_pair_list ']'                                     : [
// | T_CONSTANT_ENCAPSED_STRING                                  : string
//
// variable_class_name
// : dereferencable
//
// simple_variable
// : '$' T_STRING                                                : $
// | '$' '{' expr '}'                                            : $
// | '$' simple_variable                                         : $
//
// constant
// : name
// | class_name "::" identifier
// | variable_class_name "::" identifier
//
// function_call
// : name argument_list
// | class_name "::" member_name argument_list
// | variable_class_name "::" member_name argument_list
// | callable_expr argument_list
//
// member_name
// : identifier
// | '{' expr '}'
// | simple_variable
//
// callable_expr
// : callable_variable
// | '(' expr ')'                                                : (
// | dereferencable_scalar
//
// class_name
// "static"                                                      : "static"
// | name
//
// name
// : namespace_name
// | "namespace" '\' namespace_name                              : "namespace"
// | '\' namespace_name                                          : \
//
// namespace_name
// : T_STRING                                                    : identifier
// | namespace_name '\' T_STRING
//
//------ expr_without_variable ----------------------------------:
//
// expr_without_variable
// | '(' expr ')'
// | scalar
// | '`' backticks_expr '`'
// | "function" returns_ref '(' parameter_list ')'               : "static"
//     lexical_vars return_type
//     backup_doc_comment '{' inner_statement_list '}'
// | "static" "function" returns_ref '(' parameter_list ')'      : "function"
//     lexical_vars return_type
//     backup_doc_comment '{' inner_statement_list '}'
//
// scalar
// : integer_literal                                             : integer
// | double_literal                                              : double
// | magic_constant                                              : identifier
// | string literal (including interpolation, HEREDOC, NOWDOC)   : string
// | dereferencable_scalar
// | constant
//
ExprResult Parser::parsePrimaryExpression(bool &IsVariable, bool AllowCall) {
  ExprResult Result;

  IsVariable = false;
  switch (Tok.getKind()) {
  case tok::numeric_constant:
    Result = parseNumberLiteral(false);
    break;

  case tok::quote:                //-----------------------------: string
    Result = parseInterpolatedStringLiteral();
    if (!Result.isUsable())
      return ExprError();
    if (Tok.is(tok::coloncolon))
      return parseClassMemberReference(Result.get(), IsVariable);
    Result = parsePostfixExpressionSuffix(Result.get(), IsVariable, AllowCall);
    break;

  case tok::string_literal:       //-----------------------------: string
    // Bottom-up diagram ('is a' chain).
    // string
    //   dereferencable_scalar
    //     scalar
    //     callable_expr         ('(')
    //     dereferencable        ('[', '{', "->")
    //       variable_class_name ('::')

    Result = getActions().ActOnStringLiteral(Tok,nullptr,false);
    ConsumeStringToken();
    if (!Result.isUsable())
      return ExprError();

    if (Tok.is(tok::coloncolon)) {
      // variable_class_name "::" simple_variable            "Test"::$prop
      // variable_class_name "::" identifier                 "Test"::CONST1
      // variable_class_name "::" member_name argument_list  "Test"::func()
      return parseClassMemberReference(Result.get(), IsVariable);
    }

    Result = parsePostfixExpressionSuffix(Result.get(), IsVariable, AllowCall);
    break;

  case tok::l_paren:              //-----------------------------: (
    // '(' expr ')' - expr_without_variable or dereferencable
    Result = parseParenExpression();
    if (!Result.isUsable())
      return ExprError();

    if (Tok.is(tok::coloncolon)) {
      // variable_class_name "::" simple_variable            ($a."1")::$prop
      // variable_class_name "::" identifier                 ($a."1")::CONST1
      // variable_class_name "::" member_name argument_list  ($a."1")::func()
      return parseClassMemberReference(Result.get(), IsVariable);
    }

    Result = parsePostfixExpressionSuffix(Result.get(), IsVariable, AllowCall);
    break;

  case tok::kw_function:
    return parseLambdaExpression();

  case tok::kw_array:
  case tok::l_square:
    return parseArrayExpression();

  case tok::dollar:
    IsVariable = true;
    return parseSimpleVariable();

  case tok::kw_static:
    if (NextToken().is(tok::kw_function)) {
      return parseLambdaExpression();
    }

  case tok::kw_static_assert: {
    Diag(ConsumeToken(), diag::err_php_static_assert_in_expression);
    SkipUntil(tok::semi, SkipUntilFlags::StopBeforeMatch);
    return ExprError();
  }

  case tok::identifier:
  case tok::backslash:
  case tok::kw_namespace:
    return parseNamedEntity();

  default:
    Diag(Tok, diag::err_expected_expression);
    return ExprError();
  }

  return Result;
}


ExprResult Parser::parseNumberLiteral(bool Negative) {
  assert(Tok.is(tok::numeric_constant));
  bool Invalid = false;
  SmallString<128> SpellingBuffer;
  SpellingBuffer.resize(Tok.getLength() + 1);
  StringRef TokSpelling = PP.getSpelling(Tok, SpellingBuffer, &Invalid);
  SourceLocation Loc = ConsumeToken();
  if (Invalid)
    return ExprError();

  return getActions().ActOnNumericLiteral(Loc, TokSpelling, Negative);
}


/// \Brief parse string in double quotes which may contain interpolating expressions
///
/// \verbatim
///
/// $var
/// $var->prop
/// ${ expr }
/// {$expr }
///
/// \endverbatim
///
ExprResult Parser::parseInterpolatedStringLiteral() {
  assert(Tok.is(tok::quote));

  // Enter interpolation mode
  PP.setInterpolateMode();
  SourceLocation QuoteLoc = ConsumeToken(); // "
  //storage for parts of interpolated string
  ExprResult StringParts;
  if (!Tok.is(tok::closing_quote)) {
    if (Tok.is(tok::string_literal)) {
      StringParts = getActions().ActOnStringLiteral(Tok);
      ConsumeStringToken();
    }
    else {
      if (Tok.is(tok::dollar)) {
        ExprResult Base = parseSimpleVariable();
        if (Tok.is(tok::r_square)) {
          StringParts = parseSubscriptedExpression(Base.get(), false);
        }
        else if (Tok.is(tok::arrow)) {
          StringParts = parseObjectMemberAccess(Base.get());
        }
        PP.restoreInterpolationMode();
      }
    }
  }

  while (!Tok.is(tok::closing_quote)) {
    if (Tok.is(tok::string_literal)) {
      StringParts = getActions().ActOnBinaryOp(Tok.getLocation(),
                                               tok::period, StringParts.get(),
                             getActions().ActOnStringLiteral(Tok).get());
      ConsumeStringToken();
    }
    else {
      if (Tok.is(tok::dollar)) {
        ExprResult Base = parseSimpleVariable();
        if (Tok.is(tok::l_square)) {
          StringParts = getActions().ActOnBinaryOp(Tok.getLocation(),
              tok::period, StringParts.get(),
              parseSubscriptedExpression(Base.get(), false).get());//TODO: handle parse errors
        }
        else if (Tok.is(tok::arrow)) {
          StringParts = getActions().ActOnBinaryOp(Tok.getLocation(),
              tok::period, StringParts.get(),
              parseObjectMemberAccess(Base.get()).get());//TODO: handle parse errors
        } else { //otherwise just concat parsed expression with previous part of string
          StringParts = getActions().ActOnBinaryOp(Tok.getLocation(),
              tok::period, StringParts.get(),
              Base.get()); //TODO: handle parse errors
        }
        // If we found closing quote interpolation has been completed
        // and there is no need to restore interpolation mode.
        if(!Tok.is(tok::closing_quote)) PP.restoreInterpolationMode();
      }
    }
  }
  // Consume closing_quote
  ConsumeToken();
  // Ensure that Lexer is in correct mode
  PP.setPhpMode();
  if (StringParts.isUnset())
    return getActions().BuildStringLiteral("", QuoteLoc);
  return StringParts; //for now assume we have only 1 element in string
}


/// \brief Parses constructs that may follow an expression.
///
/// \param Base expression that may be followed by postfix operation.
/// \param IsVariable Will be set to false if the resulting expression cannot
///                   be used in LHS of assignment.
/// \param AllowCall  If false, argument list will not be parsed. Used to
///                   distinguish 'new_variable'.
///
/// Postfix operations are:
///   - [ndx],
///   - [],
///   - {ndx},
///   - ->property
///   - ->method(args)
///   - (args)
///
/// Productions that involves postfix operations are:
///
/// \verbatim
///
/// variable
/// | callable_variable
/// | static_member
/// | dereferencable "->" property_name
///
/// dereferencable
/// : variable
/// | '(' expr ')'
/// | dereferencable_scalar
///
/// callable_variable
/// : simple_variable
/// | constant '[' optional_expr ']'
/// | dereferencable '[' optional_expr ']'
/// | dereferencable '{' expr '}'
/// | dereferencable "->" property_name argument_list
/// | function_call
///
/// function_call
/// : name argument_list
/// | class_name "::" member_name argument_list
/// | variable_class_name "::" member_name argument_list
/// | callable_expr argument_list
///
/// new_variable
/// : simple_variable
/// | new_variable '[' optional_expr ']'
/// | new_variable '{' expr '}'
/// | new_variable "->" property_name
/// | class_name "::" simple_variable
/// | new_variable "::" simple_variable
///
/// \endverbatim
///
ExprResult Parser::parsePostfixExpressionSuffix(Expr *Base, bool &IsVariable,
                                                bool AllowCall) {
  ExprResult Result;
  switch (Tok.getKind()) {
  case tok::l_square:
    // callable_variable
    // | constant '[' optional_expr ']'         ABC::con[12]
    // | dereferencable '[' optional_expr ']'   $a[1], (func())[1]
    //
    // new_variable
    // | new_variable '[' optional_expr ']'
    //
    // Subscript operations may also be applied to a string.
    Result = parseSubscriptedExpression(Base, IsVariable);
    // Subscript access produces a variable unless result is a constant entity.
    if (Expr *Res = Result.get()) {
      IsVariable = Res->getType().isConstQualified();
    }
    break;

  case tok::l_brace:
    // callable_variable
    // | dereferencable '{' expr '}'
    //
    // new_variable
    // | new_variable '{' expr '}'
    //
    // Subscript operations may also be applied to a string.
    Result = parseBraceSubscriptedExpression(Base);
    // Subscript access produces a variable unless result is a constant entity.
    if (Expr *Res = Result.get()) {
      IsVariable = Res->getType().isConstQualified();
    }
    break;

  case tok::arrow:
    // variable
    // | dereferencable "->" property_name
    //
    // callable_variable
    // | dereferencable "->" property_name argument_list
    //
    // new_variable
    // | new_variable "->" property_name
    Result = parseObjectMemberAccess(Base);
    // Member access produces a variable unless it is a class constant access.
    if (Expr *Res = Result.get()) {
      IsVariable = Res->getType().isConstQualified();
    }
    break;

  case tok::l_paren:
    // function_call
    // : name argument_list
    // | class_name "::" member_name argument_list
    // | variable_class_name "::" member_name argument_list
    // | callable_expr argument_list
    if (!AllowCall)
      return Base;
    Result = parseCallExpression(Base);
    // Call always gives a variable.
    IsVariable = true;
    break;

  default:
    return Base;
  }

  if (!Result.isUsable())
    return ExprError();

  // Recursively parse postfix constructs.
  return parsePostfixExpressionSuffix(Result.get(), IsVariable, AllowCall);
}


/// \brief Parses argument list surrounded by parentheses.
///
/// This method parses arguments in these productions:
///
/// \verbatim
///
/// function_call
/// : name argument_list
/// | class_name "::" member_name argument_list
/// | variable_class_name "::" member_name argument_list
/// | callable_expr argument_list
///
/// ctor_arguments
/// | argument_list
///
/// callable_variable
/// | dereferencable "->" property_name argument_list
///
/// argument_list
/// : '(' ')'
/// | '(' non_empty_argument_list ')'
///
/// non_empty_argument_list
/// : argument
/// | non_empty_argument_list ',' argument
///
/// argument
/// : expr
/// | "..." expr
///
/// \endverbatim
///
ExprResult Parser::parseCallExpression(Expr *Callee) {
  assert(Tok.is(tok::l_paren));
  SmallVector<Expr *, 8> Arguments;
  bool ErrorOccured = false;
  BalancedDelimiterTracker T(*this, tok::l_paren);

  T.consumeOpen();
  if (Tok.isNot(tok::r_paren)) {
    while (true) {
      SourceLocation EllipsisLoc;
      if (Tok.is(tok::ellipsis))
        EllipsisLoc = ConsumeToken();

      ExprResult Arg = parseExpression();
      if (Arg.isUsable()) {
        Arg = getActions().ActOnCallArgument(EllipsisLoc, Arg.get());
        Arguments.push_back(Arg.get());
      } else {
        if (Arg.isUnset()) {
          // Otherwise error must be already reported.
          Diag(PP.getLocForEndOfToken(EllipsisLoc),
               diag::err_expected_expression);
        }
        SkipUntil(tok::comma, tok::r_paren,
                  Parser::StopAtSemi | Parser::StopBeforeMatch);
        ErrorOccured = true;
        if (Tok.isNot(tok::comma))
          break;
      }

      if (Tok.isNot(tok::comma))
        break;
      ConsumeToken();
    }
  }

  if (T.consumeClose()) {
    Diag(Tok.getLocation(), diag::err_expected) << tok::r_paren;
    return ExprError();
  }

  if (ErrorOccured)
    return ExprError();

  return getActions().ActOnCallExpression(Callee,
                          T.getOpenLocation(), Arguments, T.getCloseLocation());
}


ExprResult Parser::parseObjectMemberAccess(Expr *Base) {
  if (Base->getType()->isFundamentalType()) {
    Diag(Tok, diag::err_php_bad_arrow_base) << Base->getType();
    return ExprError();
  }
  return ExprError();
}


ExprResult Parser::parseSubscriptedExpression(Expr *Base, bool LHS) {
  assert(Tok.is(tok::l_square));
  ExprResult Index;
  ExprResult Result;

  BalancedDelimiterTracker T(*this, tok::l_square);
  T.consumeOpen();

  if (Tok.isNot(tok::r_square)) {
    Index = parseExpression();
  } else if (!LHS) {
    Diag(Tok, diag::err_php_empty_subscript);
    T.skipToEnd();
    return ExprError();
  }

  if (T.consumeClose()) {
    Diag(Tok.getLocation(), diag::err_expected) << tok::r_square;
    T.skipToEnd();
    return ExprError();
  }

  if (!Index.isUsable())
    return ExprError();

  return getActions().ActOnSubscriptExpression(Base, T.getOpenLocation(),
                                        Index.get(), T.getCloseLocation(), LHS);
}


ExprResult Parser::parseBraceSubscriptedExpression(Expr *Base) {
  assert(Tok.is(tok::l_brace));
  ExprResult Index;
  ExprResult Result;

  BalancedDelimiterTracker T(*this, tok::l_brace);
  T.consumeOpen();

  if (Tok.isNot(tok::r_square)) {
    Index = parseExpression();
  } else {
    Diag(Tok, diag::err_php_empty_subscript);
    T.skipToEnd();
    return ExprError();
  }

  if (T.consumeClose()) {
    Diag(Tok.getLocation(), diag::err_expected) << tok::r_square;
    T.skipToEnd();
    return ExprError();
  }

  if (!Index.isUsable())
    return ExprError();

  return getActions().ActOnSubscriptExpression(Base, T.getOpenLocation(),
                                      Index.get(), T.getCloseLocation(), false);
}


ExprResult Parser::parseClassMemberReference(Expr *ClassName, bool &IsVariable) {
  return ExprError();
}


ExprResult Parser::parseLambdaExpression() {
  assert(isStartOfLambdaFunction());
  getActions().ActOnClosureDefinitionStart(Tok.getLocation());
  parseFunctionDeclaration(true);
  return getActions().ActOnClosureDefinitionEnd();
}


// dereferencable_scalar
// | "array" '(' array_pair_list ')'
// | '[' array_pair_list ']'
//
// array_pair_list
// : /* empty */
// | non_empty_array_pair_list possible_comma
//
// non_empty_array_pair_list
// : non_empty_array_pair_list ',' array_pair
// | array_pair
//
// array_pair
// : expr "=>" expr
// | expr
// | expr "=>" '&' variable
// | '&' variable
//
ExprResult Parser::parseArrayExpression() {
  assert(Tok.isOneOf(tok::l_square, tok::kw_array));
  SourceLocation KwLoc;

  if (Tok.is(tok::kw_array)) {
    KwLoc = ConsumeToken();
    if (Tok.isNot(tok::l_paren)) {
      Diag(Tok, diag::err_expected) << tok::l_paren;
      return ExprError();
    }
  }

  tok::TokenKind OpenToken =
    Tok.is(tok::l_square) ? tok::l_square : tok::l_paren;
  tok::TokenKind EndToken =
    Tok.is(tok::l_square) ? tok::r_square : tok::r_paren;

  BalancedDelimiterTracker T(*this, OpenToken);
  T.consumeOpen();

  SmallVector<Expr*, 16> Elements;
  bool IsValid = true;
  while (Tok.isNot(EndToken)) {
    ExprResult Element = Tok.is(tok::amp)
                            ? parseReferenceExpression()
                            : parseExpression();
    if (Tok.is(tok::equal_greater)) {
      SourceLocation ArrowLoc = ConsumeToken();
      ExprResult Val = Tok.is(tok::amp)
                          ? parseReferenceExpression()
                          : parseExpression();
      
      Element = Val.isUsable() && Element.isUsable()
        ? getActions().ActOnPHPMapExpr(ArrowLoc, Element, Val)
        : ExprError();
    }
    if (Element.isInvalid())
      IsValid = false;
    else
      Elements.push_back(Element.get());
    if (Tok.is(tok::comma))
      ConsumeToken();
    else 
      break;
  }
  if (T.consumeClose() || !IsValid)
    return ExprError();

  return getActions().ActOnPHPArrayExpr(KwLoc, T.getOpenLocation(), Elements,
                                        T.getCloseLocation());
}


// parse '&' variable
//
ExprResult Parser::parseReferenceExpression() {
  assert(Tok.is(tok::amp));
  SourceLocation RefLoc = ConsumeToken();
  bool IsVariable;
  ExprResult Element = parsePrimaryExpression(IsVariable, true);
  if (Element.isUsable())
    Element = getActions().ActOnPHPReferenceAccess(RefLoc, Element, IsVariable);
  return Element;
}

// simple_variable
// : '$' identifier
// | '$' '{' expr '}'
// | '$' simple_variable
// This method migth be invoked for variables, class properties & object properties
//
ExprResult Parser::parseSimpleVariable(CXXScopeSpec SS, ExprResult Base, bool isStatic) {
  assert(Tok.is(tok::dollar));
  
  SourceLocation Loc = ConsumeToken();
  SmallVector<SourceLocation, 8> Locs;
  while (Tok.is(tok::dollar) && NextToken().is(tok::dollar))
    Locs.push_back(ConsumeToken());

  ExprResult Deref;
  if (!Locs.empty() || Tok.is(tok::dollar)) {
    assert(Tok.is(tok::dollar));
    Deref = parseSimpleVariable();
    for (auto It = Locs.rbegin(); It != Locs.rend(); ++It) {
      Deref = getActions().ActOnVariableUse(*It, Deref);
      if (Deref.isInvalid())
        return ExprError();
    }
    if (Deref.isInvalid())
      return ExprError();
  }
  assert(Tok.isNot(tok::dollar));
  
  ExprResult Res;
  if (Deref.isUsable()) {
    if (SS.isValid())
      Res = getActions().ActOnPHPClassVariableUse(SS, Loc, Deref);
    else if (Base.isUsable())
      Res = isStatic
        ? getActions().ActOnPHPClassVariableUse(Base, Loc, Deref)
        : getActions().ActOnPHPObjectVariableUse(Base, Loc, Deref);
  } else if (Tok.is(tok::l_brace)) {
    BraceExpressionInfo Sub = parseBraceExpression(tok::l_brace, tok::r_brace);
    Res = Sub.E;
    if (SS.isValid())
      Res = getActions().ActOnPHPClassVariableUse(SS, Loc, Sub.E);
    else if (Base.isUsable())
      Res = isStatic
        ? getActions().ActOnPHPClassVariableUse(Base, Loc, Sub.E)
        : getActions().ActOnPHPObjectVariableUse(Base, Loc, Sub.E);
  } else if (Tok.isNot(tok::identifier)) {
    Diag(Tok, diag::err_expected) << tok::identifier;
    return ExprError();
  } else {
    assert(Tok.is(tok::identifier));
    DeclarationName DN(Tok.getIdentifierInfo());
    DeclarationNameInfo DNI(DN, ConsumeToken());
    if (SS.isValid()) {
      Res = getActions().ActOnPHPClassVariableUse(SS, Loc, DN);
    } else if (Base.isUsable()) {
      Res = isStatic
        ? getActions().ActOnPHPClassVariableUse(Base, Loc, DN)
        : getActions().ActOnPHPObjectVariableUse(Base, Loc, DN);
    } else {
      Res = getActions().ActOnVariableUse(getCurScope(), DNI);
    }
  }

  if (!Res.isUsable() || Base.isInvalid())
    return ExprError();

  return Res;
}


// parse: $a
ExprResult Parser::parseSimpleVariable() {
  return parseSimpleVariable(CXXScopeSpec(), ExprEmpty(), false);
}


// parse: SS::$a
ExprResult Parser::parseSimpleVariable(CXXScopeSpec SS) {
  return parseSimpleVariable(SS, ExprEmpty(), false);
}


// parse: $base->$a
// parse: $base::$a
ExprResult Parser::parseSimpleVariable(ExprResult Base, bool IsStaticAccess) {
  return parseSimpleVariable(CXXScopeSpec(), Base, IsStaticAccess);
}


/// \brief Parses an entity started with a name, possibly qualified.
///
/// \verbatim
///
/// variable
/// : callable_variable
/// | static_member
/// | dereferencable "->" property_name
///
/// static_member
/// : class_name "::" simple_variable
/// | variable_class_name "::" simple_variable
///
/// callable_variable
/// : simple_variable
/// | constant '[' optional_expr ']'
/// | function_call
/// | dereferencable '[' optional_expr ']'
/// | dereferencable '{' expr '}'
/// | dereferencable "->" property_name argument_list
///
/// constant
/// : name
/// | class_name "::" identifier
/// | variable_class_name "::" identifier
///
/// function_call
/// : name argument_list
/// | class_name "::" member_name argument_list
/// | variable_class_name "::" member_name argument_list
/// | callable_expr argument_list
///
/// class_name
/// "static"                                                      : "static"
/// | name
///
/// name
/// : namespace_name
/// | "namespace" '\' namespace_name                              : "namespace"
/// | '\' namespace_name                                          : \
///
/// namespace_name
/// : T_STRING                                                    : identifier
/// | namespace_name '\' T_STRING
///
/// \endverbatim
///
ExprResult Parser::parseNamedEntity() {
  assert(Tok.isOneOf(tok::identifier, tok::kw_namespace, tok::backslash,
                     tok::kw_static));

  CXXScopeSpec SS;
  if (parseScopeSpecifier(SS,/* IsScopeName */ false))
    return ExprError();

  if (Tok.is(tok::dollar))
    return parsePHPStaticPropertyAccess(SS);

  if (Tok.isNot(tok::identifier)) {
    Diag(Tok, diag::err_expected) << tok::identifier;
    return ExprError();
  }

  if (NextToken().is(tok::l_paren))
    return parsePhpFunctionCall(SS);

  IdentifierInfo *II = Tok.getIdentifierInfo();
  SourceLocation Loc = ConsumeToken();
  return getActions().ActOnConstantUse(SS, II, Loc);
}

}
