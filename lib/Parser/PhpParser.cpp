//===--- PhpParser.cpp - PHP Parser ---------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Defines interface to PHP parser.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "llvm/ADT/StringSwitch.h"
#include "clang/Parse/ParseDiagnostic.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Parser/PhpParser.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;

Parser::Parser(Preprocessor &PP, Sema &Actions, bool SkipFunctionBodies)
  : clang::Parser(PP, Actions, SkipFunctionBodies) {
  assert(Actions.isPhpSema());
}


Parser::~Parser() {
}


void Parser::Initialize() {
  clang::Parser::Initialize();
}


bool Parser::ParseTopLevelDecl(Parser::DeclGroupPtrTy &Result) {
  Result = DeclGroupPtrTy();
  if (parsePHPUnit())
    Result = getActions().getUnitDeclarations();
  return Result.get().isNull();
}


/// \brief Parses PHP source file.
///
/// \returns Declaration that represents unit descriptor, which contains all
///          information about the unit in runtime.
///
/// A compiled PHP source file is a unit. A unit may form an application, itself
/// or with other units, or be included by other units. In runtime the unit is
/// represented by a unit descriptor, which determines actions made when the
/// unit is used.
///
/// Depending on compilation options, the declarations created when parsing a
/// PHP source may be externally visible or accessed only through unit
/// descriptor.
///
Decl *Parser::parsePHPUnit() {
  SourceLocation Loc = Tok.getLocation();

  // All PHP entities are inside special namespace, not visible to user. It
  // acts as global PHP namespace. User namespace will be placed after this
  // namespace.
  ParseScope PhpNamespaceScope(this, Scope::DeclScope);

  // Create scope for the php unit function.
  ParseScope UnitFunctionScope(this, Scope::FnScope | Scope::DeclScope);

  if (!getActions().ActOnUnitStart(Loc))
    return nullptr;

  parseTopStatements();

  switch (Tok.getKind()) {
  case tok::r_brace:
    Diag(Tok, diag::err_extraneous_closing_brace);
    ConsumeBrace();
    break;
  default:
    break;
  }

  return getActions().ActOnUnitFinish(
    PrevTokLocation.isInvalid() 
      ? Loc 
      : PrevTokLocation);
}


/// \brief Parses sequence of top statements.
///
/// The function stops parsing on any top statement boundary.
///
void Parser::parseTopStatements() {
  while (Tok.isNot(tok::eof) &&
         Tok.isNot(tok::r_brace) &&
         Tok.isNot(tok::include_end))
    parseTopStatement();
}


/// \brief Parse a top statement.
///
/// top_statement
/// : statement
/// | function_declaration_statement
/// | class_declaration_statement
/// | T_HALT_COMPILER '(' ')' ';'
/// | T_NAMESPACE namespace_name ';'
/// | T_NAMESPACE namespace_name
/// | T_NAMESPACE
/// | T_USE use_declarations ';'
/// | T_USE T_FUNCTION use_declarations ';'
/// | T_USE T_CONST use_declarations ';'
/// | T_CONST const_list ';'
///
void Parser::parseTopStatement() {
  switch (Tok.getKind()) {
  case tok::kw___halt_compiler:
    parseHaltCompilerStmt();
    return;

  case tok::kw_function:
    if (isStartOfLambdaFunction()) {
      getActions().ActOnTopStatement(parseStatement());
      break;
    }
    parseFunctionDeclaration();
    return;

  case tok::kw_const:
    getActions().ActOnTopStatement(parseConstantDeclaration());
    return;

  case tok::kw_namespace:
    if (isStartOfNamespace()) {
      parseNamespace();
      return;
    }
    // TODO: implement
  default:
    getActions().ActOnTopStatement(parseStatement());
    break;
  }
}


StmtResult Parser::parseStatement() {
  StmtResult Result = StmtEmpty();
  bool ParseExpression = false;
  switch (Tok.getKind()) {
  case tok::kw_echo:
    return parseEchoStmt();
  case tok::kw_if:
    return parseIfStatement();
  case tok::kw_while:
    return parseWhileStatement();
  case tok::html_data:
    Result = getActions().ActOnHtmlData(Tok.getLastLoc(),
                              StringRef(Tok.getLiteralData(), Tok.getLength()));
    ConsumeToken();
    break;

  case tok::kw_static: 
    if (isStartOfLambdaFunction()) {
      ParseExpression = true;
      break;
    }
    //TODO: handle the rest of the cases for static
    llvm_unreachable("static keyword is not implemented yet");
    break;

  case tok::kw_static_assert:
    Result = parseStaticAssert();
    break;

  case tok::kw_function:
    if (isStartOfLambdaFunction()) {
      ParseExpression = true;
      break;
    }
    Result = parseFunctionDeclaration();
    break;

  case tok::kw_return:
    Result = parseReturnStatement();
    break;

  case tok::semi:
    ConsumeToken();
    break;

  case tok::l_brace:
    Result = parseCompoundStmt();
    break;

  case tok::kw_endwhile:
    Result = StmtError();
    Diag(ConsumeToken(), diag::err_php_unexpected) << "endwhile";
    break;
  case tok::kw_endif:
    Result = StmtError();
    Diag(ConsumeToken(), diag::err_php_unexpected) << "endif";
    break;
  case tok::kw_const:
    Diag(Tok.getLocation(), diag::err_php_misplaced_const);
    parseConstantDeclaration();
    break;

  default:
    ParseExpression = true;
    break;
  }

  if (ParseExpression) {
    ExprResult ER = parseExpression();
    if (!ER.isUsable()) {
      SkipUntil(tok::html_data, tok::semi);
      return StmtError();
    }
    if (parseEndOfStatement("<expression>"))
      return StmtError();
    Result = ER.get();
  }
  return Result;
}


/// \brief Verifies that the just parsed statement is correctly terminated.
//
bool Parser::parseEndOfStatement(const char *Stmt) {

  // Statement at end of file does not need semicolon.
  if (Tok.is(tok::eof) || Tok.is(tok::include_end))
    return false;

  // Closing html tags work as implicit terminators.
  if (Tok.is(tok::html_data)) {
    if (Tok.getLength() == 0) {
      // .. ?><?php ..
      ConsumeToken();
      return false;
    }
  }

  return ExpectAndConsume(tok::semi, diag::err_expected_semi_after_stmt, Stmt);
}


void Parser::parseHaltCompilerStmt() {
  assert(Tok.is(tok::kw___halt_compiler));
  ConsumeToken();

  if (Tok.isNot(tok::l_paren)) {
    Diag(Tok.getLocation(), diag::err_expected) << tok::l_paren;
  } else {
    ConsumeParen();
    if (Tok.isNot(tok::r_paren)) {
      Diag(Tok.getLocation(), diag::err_expected) << tok::r_paren;
    } else {
      ConsumeParen();
      parseEndOfStatement("__halt_compiler");
    }
  }
  Tok.setKind(tok::eof);
}


// Possible starts of lambda function:
// [static] function [&] {
bool Parser::isStartOfLambdaFunction() {
  if (!Tok.isOneOf(tok::kw_static, tok::kw_function))
    return false;

  unsigned LookAhead = 0;
  if (Tok.is(tok::kw_static)) {
    if (PP.LookAhead(LookAhead).isNot(tok::kw_function))
      return false;
    ++LookAhead;
  }
  if (PP.LookAhead(LookAhead).is(tok::l_paren))
    return true;
  if (PP.LookAhead(LookAhead).isNot(tok::amp))
    return false;

  return PP.LookAhead(LookAhead + 1).is(tok::l_paren);
}


bool Parser::isStartOfNamespace() {
  // | T_NAMESPACE namespace_name ';'
  // | T_NAMESPACE namespace_name '{' top_statement_list '}'
  // | T_NAMESPACE '{' top_statement_list '}'
  return Tok.is(tok::kw_namespace) && NextToken().isNot(tok::backslash);
}


bool Parser::isKeyword(const Token& Tok) {
  switch (Tok.getKind()) {
#define KEYWORD(NAME, FLAGS) case clang::tok::kw_##NAME:
#include "clang/Basic/TokenKinds.def"
    return true;
default:
  return false;
  };
}


bool Parser::isCastOperator() {
  if (Tok.isNot(tok::l_paren))
    return false;

  Token Next = NextToken();
  if (Next.is(tok::kw_unset) || Next.is(tok::kw_array)) 
    return PP.LookAhead(1).is(tok::r_paren);

  if (Next.isNot(tok::identifier))
    return false;

  StringRef Name = Next.getIdentifierInfo()->getName();
  std::string LCName = Name.lower();
  bool IsValidTargetType = llvm::StringSwitch<bool>(LCName)
    .Case("unset", true)
    .Cases("bool", "boolean", true)
    .Cases("int", "integer", true)
    .Cases("real", "float", "double", true)
    .Cases("string", "binary", true)
    .Case("array", true)
    .Case("object", true)
    .Default(false);
  if (!IsValidTargetType)
    return false;
  return PP.LookAhead(1).is(tok::r_paren);
}


void Parser::skipBlock(bool SkipSemi) {
  SkipUntil(tok::l_brace, StopAtSemi | StopBeforeMatch);
  if (Tok.is(tok::l_brace)) {
    ConsumeBrace();
    SkipUntil(tok::r_brace, StopBeforeMatch);
    if (Tok.is(tok::r_brace))
      ConsumeBrace();
    return;
  }
  if (Tok.is(tok::semi))
    ConsumeToken();
}


/// \brief Parses PHP scope specifier that possibly precedes a name.
///
/// \param SS Scope specifier that must be set by this procedure.
/// \param DN Will be set according to the name that follows scope specifier.
/// \param IsScopeName If true, parses class or namespace name. In this case
///                    the last namespace component will go to scope specifier,
///                    rather than to \c DN. This mode is used to parse class
///                    name if 'new' statement or namespace in 'namespace'
///                    statement.
/// \returns true of error, as other clang procedures do.
///
/// Respective part of grammar:
/// \verbatim
///
/// class_name
/// "static"                                                      : "static"
/// | name
///
/// name
/// : namespace_name
/// | "namespace" '\' namespace_name                              : "namespace"
/// | '\' namespace_name                                          : \
///
/// namespace_name
/// : T_STRING                                                    : identifier
/// | namespace_name '\' T_STRING
///
/// \endverbatim
///
bool Parser::parseScopeSpecifier(CXXScopeSpec &SS, bool IsScopeName) {
  assert(SS.isEmpty() || isGlobalScopeSpecifier(SS));
  bool HasScopeSpecifier = false;     // A\,
  bool HasClassSpecifier = false;     // A::

  // Parse optional 'namespace \'
  if (Tok.is(tok::kw_namespace)) {
    ConsumeToken();
    if (Tok.isNot(tok::backslash)) {
      Diag(Tok, diag::err_php_expected_namespace_delim);
      if (Tok.isNot(tok::identifier))
        return true;
      // Recover by dropping 'namespace'.
    }
    HasScopeSpecifier = true;
  }

  // Process global specifier.
  if (Tok.is(tok::backslash)) {
    if (!isGlobalScopeSpecifier(SS))
      getActions().ActOnPhpGlobalScopeSpecifier(Tok.getLocation(), SS);
    ConsumeToken();  // '\'
    HasScopeSpecifier = true;
  }

  // Some scope specifiers may have special meaning only when they are single
  // class components, these are 'self::', 'parent::' or 'static::'. The
  // specifiers '\self::' or 'ABC\parent' have no such meaning, these are usual
  // class names.
  if (!HasScopeSpecifier && NextToken().is(tok::coloncolon)) {
    if (Tok.is(tok::kw_static))
      Tok.setKind(tok::identifier);
    if (Tok.is(tok::identifier)) {
      StringRef IdText = Tok.getIdentifierInfo()->getName();
      enum SpecSpecifier {
        None, Self, Parent, Static
      } Spec = StringSwitch<SpecSpecifier>(IdText)
               .Case("self", Self)
               .Case("parent", Parent)
               .Case("static", Static)
               .Default(None);
      if (Spec != None && NextToken().is(tok::coloncolon)) {
        SourceLocation IdLoc = ConsumeToken();  // 'self', 'parent', 'static'
        SourceLocation CCLoc = ConsumeToken();  // '::'
        // All special specifiers may be used only at class scope.
        Scope *S = getCurScope();
        if (DeclContext *ClassDC = getCurrentClassContext(S)) {
          bool Result = true;
          switch (Spec) {
          case Self:
            Result = getActions().ActOnSelfScopeSpecifier(IdLoc, CCLoc,
                                                          ClassDC, SS);
            HasScopeSpecifier = HasClassSpecifier = true;
            break;
          case Parent:
            Result = getActions().ActOnParentScopeSpecifier(IdLoc, CCLoc,
                                                            ClassDC, SS);
            HasScopeSpecifier = HasClassSpecifier = true;
            break;
          case Static:
            Result = getActions().ActOnStaticScopeSpecifier(IdLoc, CCLoc,
                                                            ClassDC, SS);
            HasScopeSpecifier = HasClassSpecifier = true;
            break;
          default:
            ;
          }
          if (Result)
            return true;
        } else {
          Diag(Tok, diag::err_php_misplaced_scope_spec) << IdText;
          // Recover by treating it as usual scope spec.
        }
      }
    }
  }

  if (!HasClassSpecifier) {
    // if '\' is seen, it must be followed by a name. '::' may be followed by
    // larger set of constructs.
    if (HasScopeSpecifier && !Tok.is(tok::identifier)) {
      Diag(Tok, diag::err_expected) << tok::identifier;
      return true;
    }
  }

  // Early return if entity name is dynamic. In all remaining cases we must see
  // identifier.
  if (Tok.is(tok::dollar))
    return false;

  if (Tok.is(tok::coloncolon)) {
    Diag(Tok, diag::err_php_expected_classname);
    ConsumeToken();
    return true;
  }

  if (Tok.isNot(tok::identifier)) {
    Diag(Tok, diag::err_expected) << tok::identifier;
    return true;
  }

  // Process sequence of namespace components until we see ':: identifier' or
  // identifier is not followed by '\'.
  do {
    assert(Tok.is(tok::identifier));

    // Namespace fragment
    // identifier <backslash>
    if (NextToken().is(tok::backslash) || IsScopeName) {
      IdentifierInfo *II = Tok.getIdentifierInfo();
      SourceLocation IdLoc = ConsumeToken();
      SourceLocation BSLoc;
      if (Tok.is(tok::backslash)) {
        BSLoc = ConsumeToken();
      } else {
        assert(IsScopeName);
        BSLoc = IdLoc;
      }
      if (getActions().ActOnPhpNestedNameSpecifier(getCurScope(), *II, IdLoc,
        BSLoc, SS, /*EnteringContext*/ false, /*IsClassName*/ false)) {
        // Cannot add namespace component.
        return true;
      }
      if (IsScopeName && (BSLoc == IdLoc))
        return false;
      HasScopeSpecifier = true;
      //TODO: if such namespace isn't found, create tentative namespace object
    }

    // Class name fragment
    else if (NextToken().is(tok::coloncolon)) {
//       if (!IsScopeName)
//         return false;
      // [ns\] ident ::
      // Identifier preceding '::' must be a class name.
      IdentifierInfo *II = Tok.getIdentifierInfo();
      SourceLocation IdLoc = ConsumeToken();
      assert(Tok.is(tok::coloncolon));
      SourceLocation CCLoc = ConsumeToken();
      DeclarationNameInfo ClassName(DeclarationName(II), IdLoc);

      if (getActions().ActOnPhpNestedNameSpecifier(getCurScope(), *II, IdLoc,
                 CCLoc, SS, /*EnteringContext*/ false, /*IsClassName*/ false)) {
        // Cannot add namespace component.
        return true;
      }
      HasScopeSpecifier = true;

      // Class name must be followed by class member
      if (Tok.isNot(tok::identifier) &&
          Tok.isNot(tok::dollar) &&
          Tok.isNot(tok::l_brace) &&
          !isKeyword(Tok)) {
        Diag(Tok, diag::err_php_expected_static_member);
        //TODO: recovery
        return true;
      }
      return false;
    } else {
      break;
    }
  } while (Tok.is(tok::identifier) || isKeyword(Tok));

  return false;
}


bool isGlobalScopeSpecifier(const CXXScopeSpec &SS) {
  return SS.isSet() &&
         SS.getScopeRep()->getKind() == NestedNameSpecifier::Global;
}


ExprResult Parser::parseConstantExpression() {
  ExprResult Result = parseExpression();
  if (!Result.isUsable())
    return ExprError();

  // Is this a compile-time constant?
  Evaluator EV(getActions());
  if (ConstValue *CV = EV.evaluate(Result.get())) {
    return getActions().getConstantExpr(CV, Result.get()->getExprLoc(), true);
  }

  // This is not a compile-time constant, but it must be an expression composed
  // of compile and run-time constants.
  //TODO:

  Diag(Result.get()->getLocStart(), diag::err_php_not_const_expr)
    << EV.getOffendingExpr()->getSourceRange();
  return ExprEmpty();
}

}
