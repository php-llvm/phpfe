//===--- PhpExpression.cpp - PHP Parser -----------------------------------===//
//
//  LLVM based PHP compiler
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Provides the Expression parsing implementation.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/Expr.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/Parse/ParseDiagnostic.h"
#include "phpfe/Parser/OperatorPrecedence.h"
#include "phpfe/Parser/PhpParser.h"

#include "Parse/RAIIObjectsForParser.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


/// \brief Parses expression.
///
ExprResult Parser::parseExpression() {
  return parseSubExpression(Unknown);
}


/// \brief Parses subexpression composed by operators of increasing priority not
/// lower than the specified.
///
/// The method is called when parser expects operand, at the start of expression
/// or after an operator.
///
ExprResult Parser::parseSubExpression(PrecLevel Prec) {

  // Parse prefix.
  //
  // We must read expression until we find a postfix expression. All unary
  // operators preceding it will apply just to this postfix expression - there
  // is no choice, unary operators must associate right. Also all postfix
  // operators have higher priority: '2**$V++' must be parsed as '2**($V++)'
  // although priority of ** is higher than '++' according to grammar
  // description.
  tok::TokenKind Kind = Tok.getKind();
  PrecLevel UnaryPrec = isCastOperator() ? Cast : getUnaryOpPrecedence(Kind);
  ExprResult Result;
  if (UnaryPrec == Unknown) {
    // The current token is not an unary operator, parse as primary maybe
    // followed by postfix operators.
    Result = parsePostfixExpression();
    if (!Result.isUsable())
      return Result;
  } else {
    // Parse expression that starts with unary operator.
    //
    // If the requested expression priority is higher than the priority of the
    // unary operator, parse subexpression with the latter priority. It allows
    // correctly parse expressions like: '$a**+$b++'.
    if (UnaryPrec < Prec)
      Prec = UnaryPrec;
    switch (Kind) {
    case tok::minus:
      // Process unary minus in a specific way if is followed by a number
      // literal. This complexity is solely to parse integer literal that
      // represents minimal integer value: -M. When we have parsed 'M' we do not
      // know what result we must return: if this 'M' is a part of '-M', this is
      // integer expression, otherwise float.
      if (NextToken().is(tok::numeric_constant)) {
        ConsumeToken();  //  '-'
        Result = parseNumberLiteral(true);
        if (!Result.isUsable())
          return ExprError();
        break;
      }
    case tok::at:
    case tok::plus:
    case tok::exclaim:
    case tok::tilde:
    case tok::minusminus:
    case tok::plusplus:
    case tok::kw_clone:
    case tok::kw_print: {
      SourceLocation Loc = ConsumeToken();
      ExprResult LHS = parseSubExpression(UnaryPrec);
      if (!LHS.isUsable())
        return ExprError();
      Result = getActions().ActOnPrefixUnaryOp(Loc, Kind, LHS.get());
      if (!Result.isUsable())
        return ExprError();
      break;
    }

    case tok::kw_new:
      Result = parsePHPNewExpression();
      break;

    case tok::kw_include:
    case tok::kw_include_once:
    case tok::kw_require:
    case tok::kw_require_once:
      Result = parsePHPIncludeExpression();
      break;

    case tok::l_paren:
      Result = parseCastExpression();
      break;

    default:
      llvm_unreachable("expected unary operator");
    }
  }

  // Parse suffix.

  // If the parsed subexpression is followed by operator of higher priority than
  // the specified, parse it now, - we need to parse '+$a**$b' as '+($a**$b)'.
  while(true) {
    PrecLevel CurrPrec = getBinaryOpPrecedence(Tok.getKind());
    if (CurrPrec == Unknown || CurrPrec < Prec)
      break;
    if (CurrPrec == Prec) {
      Assoc OpAssoc = getAssociativity(Tok.getKind());
      if (OpAssoc == Assoc::Right) {
        tok::TokenKind OpKind = Tok.getKind();
        SourceLocation Loc = ConsumeToken();
        ExprResult RHS = parseSubExpression(CurrPrec);
        if (RHS.isUsable())
          return getActions().ActOnBinaryOp(Loc, OpKind, Result.get(),
                                            RHS.get());
      }
      if (OpAssoc == Assoc::None) {
        Diag(Tok, diag::err_php_nonassociative);
        return ExprError();
      }
      if (OpAssoc == Assoc::Left)
        break;
    }
    Result = parseBinaryOperand(Result.get());
    if (!Result.isUsable())
      return Result;
  }
  return Result;
}


ExprResult Parser::parseBinaryOperand(Expr *LHS) {
  switch (Tok.getKind()) {
  case tok::l_square: {
    BraceExpressionInfo BI = parseBraceExpression(tok::l_square, tok::r_square, true);
    return getActions().ActOnPHPSubscript(LHS, BI.LBLoc, BI.E.get(), BI.RBLoc);
  }

  case tok::kw_instanceof: {
    SourceLocation Loc = ConsumeToken();
    ClassNameReference CR;
    if (parsePHPClassNameReference(CR))
      return ExprError();
    return getActions().ActOnPHPInstanceOf(Loc, LHS, CR.SS, CR.ClassName);
  }

  case tok::star:
  case tok::slash:
  case tok::star_star:
  case tok::percent:
  case tok::plus:
  case tok::minus:
  case tok::period:
  case tok::lessless:
  case tok::greatergreater:
  case tok::less:
  case tok::lessequal:
  case tok::less_equal_greater:
  case tok::greater:
  case tok::greaterequal:
  case tok::equalequal:
  case tok::exclaimequal:
  case tok::equal_equal_equal:
  case tok::exclaim_equal_equal:
  case tok::less_greater:
  case tok::amp:
  case tok::caret:
  case tok::pipe:
  case tok::ampamp:
  case tok::pipepipe:
  case tok::kw_and:
  case tok::kw_xor:
  case tok::kw_or:
  case tok::question_question: {
    tok::TokenKind Kind = Tok.getKind();
    SourceLocation Loc = ConsumeToken();
    ExprResult RHS = parseSubExpression(getBinaryOpPrecedence(Kind));
    if (!LHS || RHS.isInvalid())
      return ExprError();
    ExprResult Result = getActions().ActOnBinaryOp(Loc, Kind, LHS, RHS.get());
    if (!Result.isUsable())
      return ExprError();
    return Result;
  }

  case tok::question: {
    SourceLocation QLoc = ConsumeToken();
    ExprResult Middle;
    if (Tok.isNot(tok::colon)) {
      Middle = parseExpression();
      if (Middle.isInvalid())
        return ExprError();
      // TODO: recovery
    }
    if (Tok.isNot(tok::colon)) {
      Diag(Tok, diag::err_expected) << tok::colon;
      Diag(QLoc, diag::note_matching) << tok::question;
      return ExprError();
    }
    SourceLocation CLoc = ConsumeToken();
    ExprResult RHS = parseSubExpression(Conditional);
    if (RHS.isInvalid())
      return ExprError();
    return getActions().ActOnConditionalOp(LHS, QLoc, Middle.get(), CLoc, RHS.get());
  }

  default:
    return ExprEmpty();
  }
}

/// \brief Parses primary expression, possibly followed by postfix operators.
///
ExprResult Parser::parsePostfixExpression() {
  // Parse primary expression, it may be a 'variable' or 'expr_without_variable'.
  bool IsVariable;
  ExprResult Result = parsePrimaryExpression(IsVariable, true);
  if (!Result.isUsable())
    return Result;

  // Primary may be followed by postfix suffices.
  Result = parsePostfixExpressionSuffix(Result.get(), IsVariable, true);
  if (!Result.isUsable())
    return Result;

  // Some postfix operators are not parsed by parsePostfixExpressionSuffix,
  // they are applicable only to variables.
  Result = parsePostfixOperator(Result.get(), IsVariable);
  return Result;
}


ExprResult Parser::parsePostfixOperator(Expr *Base, bool IsVariable) {
  if (!Tok.isOneOf(tok::plusplus, tok::minusminus) &&
      !Sema::isAssignmentOperator(Tok.getKind()))
    return Base;
  Token OpToken = Tok;
  if (!IsVariable && !isa<ArraySubscriptExpr>(Base)) {
    Diag(Tok.getLocation(), diag::err_php_not_lvalue)
        << Base->getSourceRange();
    return ExprError();
  }
  ConsumeToken();

  if (OpToken.isOneOf(tok::plusplus, tok::minusminus))
    return getActions().ActOnPostfixUnaryOp(OpToken.getLocation(),
                                            OpToken.getKind(), Base);
  assert(Sema::isAssignmentOperator(OpToken.getKind()));

  ExprResult RHS = parseSubExpression(Assignment);
  if (!RHS.isUsable())
    return ExprError();
  return getActions().ActOnBinaryOp(OpToken.getLocation(), OpToken.getKind(),
                                    Base, RHS.get());
}


ExprResult Parser::parseParenExpression() {
  BalancedDelimiterTracker T(*this, tok::l_paren);
  if (T.consumeOpen()) {
    Diag(Tok.getLocation(), diag::err_expected) << tok::l_paren;
    return ExprError();
  }

  ExprResult Result = parseExpression();

  if (Result.isInvalid())
    SkipUntil(tok::r_paren, Parser::StopAtSemi | Parser::StopBeforeMatch);

  if (T.consumeClose())
    return ExprError();

  if (Result.isInvalid())
    return ExprError();

  if (Tok.is(tok::coloncolon)) {
    // It is not parent expression, it is dereferencable:
    //   ($a . $b)::abcd = 11;
    // TODO:
    llvm_unreachable("not implemented");
  }

  return getActions().ActOnParenExpr(T.getOpenLocation(), T.getCloseLocation(),
                                     Result.get());
}

// Parse comma separated expression list. Returns true if error occurred.
//
bool Parser::parseExpressionList(SmallVectorImpl<Expr *> &Exprs) {
  bool Result = false;
  while (true) {
    ExprResult Res = parseExpression();
    if (Res.isInvalid())
      Result = true;
    Exprs.push_back(Res.get());
    if (Tok.isNot(tok::comma))
      break;
    ConsumeToken();
  }
  return Result;
}


ExprResult Parser::parseCastExpression() {
  assert(isCastOperator());
  BalancedDelimiterTracker T(*this, tok::l_paren);
  T.consumeOpen();
  IdentifierInfo *CastIdentifier = Tok.getIdentifierInfo();
  ConsumeToken();
  T.consumeClose();
  ExprResult E = parseSubExpression(Cast);
  if (!E.isUsable())
    return E;
  return getActions().ActOnCastExpr(T.getOpenLocation(), CastIdentifier,
                                    T.getCloseLocation(), E.get());
}


Parser::BraceExpressionInfo Parser::parseBraceExpression(tok::TokenKind LKind, tok::TokenKind RKind, bool ExprIsOptional) {
  assert(Tok.is(LKind));
  SourceLocation LBLoc = ConsumeAnyToken();
  ExprResult Res;
  if (!ExprIsOptional || Tok.isNot(RKind))
    Res = parseExpression();
  if (Tok.isNot(RKind)) {
    Diag(Tok, diag::err_expected) << RKind;
    Diag(LBLoc, diag::note_matching) << LKind;
    return {ExprError(), LBLoc, SourceLocation()};
  }
  SourceLocation RBLoc = ConsumeAnyToken();
  return {Res, LBLoc, RBLoc};
}


// class_name_reference:
//    class_name
//  | new_variable:

// class_name:
//     'static'
//   | name

// name:
//     namespace_name
//   | 'namespace' '\' namespace_name
//   | '\' namespace_name

// namespace_name:
//     <stirng>
//   | namespace_name '\' <stirng>;

// new_variable:
//     simple_variable
//   | new_variable '[' optional_expr ']'
//   | new_variable '{' expr '}'
//   | new_variable '->' member_name
//   | class_name '::' simple_variable
//   | new_varaible '::' simple_variable
bool Parser::parsePHPClassNameReference(ClassNameReference &CR) {
  if (parseScopeSpecifier(CR.SS, /*IsScopeName*/false))
    return true;
  
  if (getActions().IsClassNestedScopeSpecifier(CR.SS)) {
    if (Tok.isNot(tok::coloncolon))
      // matched rule class_name_reference -> class_name
      return false;
    // class_name_reference -> new_variable -> class_name '::'
    ConsumeToken();
  } else if (Tok.is(tok::identifier)) {
    // handle class name
    if (getActions().ActOnPhpNestedNameSpecifier(getCurScope(),
        *Tok.getIdentifierInfo(), Tok.getLocation(), SourceLocation(),
        CR.SS, false, true))
      return true;
    //assert(getActions().IsClassNestedScopeSpecifier(CR.SS) && "Nested name specifier builder is broken?");
    ConsumeToken();
    return false;
  }
  if (Tok.isNot(tok::dollar)) {
    //TODO: emit proper diagnostic
    Diag(Tok, diag::err_expected) << tok::dollar;
    return true;
  }
  // SS::$var
  ExprResult Res = parseSimpleVariable(CR.SS);
  while (Tok.is(tok::l_square) || 
         Tok.is(tok::l_brace) ||
         Tok.is(tok::arrow) ||
         Tok.is(tok::coloncolon)) {
    switch (Tok.getKind()) {
    case tok::l_square: { // [ optional_expr ]
      BraceExpressionInfo Sub = parseBraceExpression(tok::l_square, tok::r_square, true);
      Res = Sub.E.isUsable() && Res.isUsable()
        ? getActions().ActOnPHPSubscript(Res.get(), Sub.LBLoc, Sub.E.get(), Sub.RBLoc)
        : Sub.E;
      break;
    }
    case tok::l_brace: { // { expr }
      BraceExpressionInfo Sub = parseBraceExpression(tok::l_brace, tok::r_brace);
      Res = Sub.E.isUsable() && Res.isUsable()
        ? getActions().ActOnPHPSubscript(Res.get(), Sub.LBLoc, Sub.E.get(), Sub.RBLoc) 
        : Sub.E;
      break;
    }
    case tok::arrow: // -> member_name
      Res = parsePHPMemberName(Res);
      break;
    case tok::coloncolon: // :: simple_variable
      ConsumeToken();
      if (Tok.isNot(tok::dollar)) {
        //TODO: emit diagnostic
        Diag(Tok, diag::err_expected) << tok::dollar;
        return true;
      }
      Res = parseSimpleVariable(Res, true);
      break;
    default:
      llvm_unreachable("Invalid state");
      break;
    }
  }
  CR.ClassName = Res;
  return Res.isInvalid();
}


ExprResult Parser::parsePHPIncludeExpression() {
  assert(Tok.isOneOf(tok::kw_include, tok::kw_include_once) ||
         Tok.isOneOf(tok::kw_require, tok::kw_require_once));
  
  tok::TokenKind Kind = Tok.getKind();
  SourceLocation Loc = ConsumeToken();
  ExprResult FileName = parseSubExpression(Include);
  if (FileName.isInvalid())
    return ExprError();

  //TODO: implement include
  return getActions().ActOnPHPIncludeExpr(Loc, Kind, FileName);
}

ExprResult Parser::parsePHPLambdaExpression() {
  assert(Tok.is(tok::kw_function) || 
    (Tok.is(tok::kw_static) && NextToken().is(tok::kw_function)));
  llvm_unreachable("expression parsing is not implemented yet");
  return ExprError();
}

ExprResult Parser::parsePHPListExpression() {
  assert(Tok.is(tok::kw_list));
  llvm_unreachable("expression parsing is not implemented yet");
  return ExprError();
}


ExprResult Parser::parsePhpFunctionCall(CXXScopeSpec SS) {
  assert(Tok.is(tok::identifier));
  assert(NextToken().is(tok::l_paren));

  DeclarationName DN(Tok.getIdentifierInfo());
  SourceLocation Loc = ConsumeToken();

  BalancedDelimiterTracker T(*this, tok::l_paren);
  T.consumeOpen();
  SmallVector<Expr*, 8> Args;
  bool ParseFailed = false;
  if (Tok.isNot(tok::r_paren))
    ParseFailed = parseExpressionList(Args);
  if (T.consumeClose())
    return ExprError();
  if (ParseFailed)
    return ExprError();
  return getActions().ActOnCallExpression(SS, Loc, DN, T.getOpenLocation(),
                                          Args, T.getCloseLocation());
}


ExprResult Parser::parsePHPMemberName(ExprResult Base) {
  assert(Tok.is(tok::arrow));

  SourceLocation Loc = ConsumeToken();
  if (Tok.is(tok::identifier)) {
    DeclarationName DN(Tok.getIdentifierInfo());
    SourceLocation ArrowLoc = ConsumeToken();
    return Base.isUsable()
      ? getActions().ActOnPHPMemberAccessExpr(Base.get(), ArrowLoc, Loc, DN)
      : Base;
  }
  if (Tok.is(tok::l_brace)) {
    BraceExpressionInfo Sub = parseBraceExpression(tok::l_brace, tok::r_brace);
    return Base.isUsable() && Sub.E.isUsable()
      ? getActions().ActOnPHPMemberAccessExpr(Base.get(), Loc, Sub.LBLoc, Sub.E.get(), Sub.RBLoc)
      : Base;
  }
  if (Tok.is(tok::dollar))
    return parseSimpleVariable(Base, false);

  //TODO: emit diagnostic
  Diag(Tok, diag::err_expected) << tok::dollar;
  return ExprError();
}

ExprResult Parser::parsePHPNamedEntity() {
  assert(Tok.is(tok::identifier) || 
         Tok.is(tok::backslash)  ||
         Tok.is(tok::kw_static));

  CXXScopeSpec SS;
  if (parseScopeSpecifier(SS, /*IsScopeName*/false))
    return ExprError();
  
  if (Tok.is(tok::coloncolon))
    return parsePHPStaticPropertyAccess(SS);

  if (Tok.isNot(tok::identifier)) {
    Diag(Tok, diag::err_expected) << tok::identifier;
    return ExprError();
  }

  if (NextToken().is(tok::l_paren))
    return parsePhpFunctionCall(SS);

  IdentifierInfo *II = Tok.getIdentifierInfo();
  SourceLocation Loc = ConsumeToken();
  return getActions().ActOnConstantUse(SS, II, Loc);
}

ExprResult Parser::parsePHPNewExpression() {
  assert(Tok.is(tok::kw_new));

  SourceLocation Loc = ConsumeToken();
  ClassNameReference CR;
  if (parsePHPClassNameReference(CR))
    return ExprError();

  SmallVector<Expr*, 8> Args;
  if (Tok.is(tok::l_paren)) {
    SourceLocation LLoc = ConsumeParen();
    if (parseExpressionList(Args)) {
      SkipUntil(tok::r_paren);
      return ExprError();
    }
    if (Tok.isNot(tok::r_paren)) {
      Diag(Tok, diag::err_expected) << tok::r_paren;
      Diag(LLoc, diag::note_matching) << tok::l_paren;
      return ExprError();
    }
    ConsumeParen();
  }
  return getActions().ActOnPHPNewExpression(Loc, CR.SS, CR.ClassName, Args);
}


ExprResult Parser::parsePHPStaticPropertyAccess(CXXScopeSpec SS) {
  if (Tok.is(tok::dollar))
    return parseSimpleVariable(SS);

  if (Tok.is(tok::l_brace)) {
    BraceExpressionInfo Name = parseBraceExpression(tok::l_brace, tok::r_brace);
    if (Name.E.isInvalid())
      return ExprError();
    return getActions().ActOnPHPClassConstant(SS, Name.LBLoc, Name.E.get(), Name.RBLoc);
  }

  if (Tok.isNot(tok::identifier)) {
    Diag(Tok, diag::err_expected) << tok::identifier;
    SkipUntil(tok::semi, SkipUntilFlags::StopBeforeMatch);
    return ExprError();
  }

  if (NextToken().is(tok::l_paren))
    return parsePhpFunctionCall(SS);

  IdentifierInfo *II = Tok.getIdentifierInfo();
  SourceLocation Loc = ConsumeToken();
  return getActions().ActOnConstantUse(SS, II, Loc);
}


ExprResult Parser::parsePHPYieldExpression() {
  assert(Tok.is(tok::kw_yield));
  llvm_unreachable("expression parsing is not implemented yet");
  return ExprError();
}

}
