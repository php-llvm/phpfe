//===--- PhpStmt.cpp - PHP Statement Parser -------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Implementation of PHP specific statements parsing
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Basic/PrettyStackTrace.h"
#include "clang/Parse/ParseDiagnostic.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "Parse/RAIIObjectsForParser.h"
#include "phpfe/Parser/PhpParser.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;

StmtResult Parser::parseEchoStmt() {
  assert(Tok.is(tok::kw_echo));
  SourceLocation Loc = ConsumeToken();

  SmallVector<Expr*, 4> Exprs;
  if (parseExpressionList(Exprs)) {
    SkipUntil(tok::html_data, tok::semi);
    return StmtError();
  }
  
  parseEndOfStatement("echo");
  return getActions().ActOnEchoStmt(Loc, Exprs);
}


StmtResult Parser::parseCompoundStmt() {
  assert(Tok.is(tok::l_brace));

  PrettyStackTraceLoc CrashInfo(PP.getSourceManager(),
                                Tok.getLocation(),
                                "in compound statement ('{}')");
  ParseScope PS(this, Scope::DeclScope);
  BalancedDelimiterTracker T(*this, tok::l_brace);
  Sema::CompoundScopeRAII CompoundScope(Actions);

  T.consumeOpen();  // '{'
  SmallVector<Stmt*, 64> BodyStmts;
  bool ErrorSeen = parseStatementList(BodyStmts);
  if (T.consumeClose() || ErrorSeen)
    return StmtError();

  return getActions().ActOnCompoundStmt(
      T.getOpenLocation(), T.getCloseLocation(), BodyStmts, false);
}


bool Parser::parseStatementList(SmallVectorImpl<Stmt *> &Stmts) {
  bool ErrorSeen = false;
  while (Tok.isNot(tok::r_brace) &&
         Tok.isNot(tok::eof) &&
         Tok.isNot(tok::include_end)) {
    StmtResult S = parseStatement();
    if (S.isUsable())
      Stmts.push_back(S.get());
    else if (S.isInvalid())
      ErrorSeen = true;
  }
  return ErrorSeen;
}

}
