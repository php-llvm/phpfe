//===--- SemaOperator.cpp -------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Part of Semantics responsible for creation of AST nodes of binary and
// unary operators.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Lex/LiteralSupport.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/Sema/SemaDiagnostic.h"
#include "phpfe/Analyzer/Decorator.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Support/Path.h"
#include "phpfe/Support/Utilities.h"
#include "shared/constants.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


ExprResult Sema::ActOnBinaryOp(SourceLocation Loc, tok::TokenKind Kind,
                               Expr *LHS, Expr *RHS) {
  BinaryOperatorKind Opc = ConvertTokenKindToBinaryOpcode(Kind);

  // Create resulting node. At the beginning it has universal type, the actual
  // type will be calculated by Decorator component.
  Expr *Result = nullptr;
  QualType ResType = getClassBoxType();
  if (BinaryOperator::isCompoundAssignmentOp(Opc)) {
    Result = new (Context) CompoundAssignOperator(LHS, RHS, Opc, ResType,
         VK_RValue, OK_Ordinary, ResType, ResType, Loc, FPFeatures.fp_contract);
  } else {
    Result = new (Context) BinaryOperator(LHS, RHS, Opc, ResType, VK_RValue,
                                      OK_Ordinary, Loc, FPFeatures.fp_contract);
  }

  Decorator D(*this);
  ResType = D.decorateByType(Result);
  if (ResType.isNull())
    return ExprError();
  Result->setType(ResType);
  return Result;
}

}
