//===--- SemaCall.cpp ---- AST Builder and Semantic Analysis --------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Tools for creation of function calls.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/AST/ExprPhp.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
//------------------------------------------------------------------------------


namespace phpfe {

using namespace clang;
using namespace llvm;


/// \brief checks if the specified declaration refers to PHP variadic function.
///
static bool isVariadic(FunctionDecl *FD) {
  // If the last parameter has type 'ArgPack<T>', we think this function is
  // variadic.
  if (unsigned NParams = FD->getNumParams()) {
    if (RuntimeTypes::isArgPackType(FD->getParamDecl(NParams - 1)->getType()))
      return true;
  }
  return false;
}


/// \brief Checks if the specified expression a valid variable in PHP sense.
///
static bool isPhpVariableExpr(Expr *E) {
  QualType T = E->getType();
  // Consider values boxed in RefBoxed template as variables. Values returned
  // by reference marked as RValues, but they may be used in PHP as variables.
  if (RuntimeTypes::isBoxedType(T))
    return true;
  if (E->isLValue()) {
    // Filter out C++ lvalues that are not variable expressions in PHP:
    // - constant objects,
    // - C-string,
    if (T.isConstQualified() || RuntimeTypes::isCStringType(T))
      return false;
    return true;
  }
  return false;
}


ExprResult Sema::createFunctionCall(FunctionDecl *FDecl, SourceLocation CallLoc,
          SourceLocation LParen, ArrayRef<Expr *> Args, SourceLocation RParen) {
  assert(FDecl);
  Expr *Fn = BuildDeclRefExpr(FDecl, FDecl->getType(), VK_RValue, CallLoc).get();
  return BuildResolvedCallExpr(Fn, FDecl, LParen, Args, RParen);
}


/// \brief Checks if expression E has type compatible with the specified type
/// taken from function declaration.
///
/// On error returns true and prints diagnostics.
///
bool Sema::checkTypeVsDeclaration(Expr *E, QualType Ty, bool IsReturn) {
  assert(Ty != Runtime::getVoidType());
  if (Ty == E->getType())
    return false;
  bool IsInvalid = false;
  QualType ExpectedType = Ty;

  bool MustBeVariable = false; // RT->isBoxedType(Ty) && !RT->isBoxedType(E->getType());
  if (const ReferenceType *Ref = Ty->getAs<ReferenceType>()) {
    // Primitive types are passed by reference as C++ reference.
    MustBeVariable = true;
    ExpectedType = Ref->getPointeeType();
  }
  else if (const PointerType *Ref = Ty->getAs<PointerType>()) {
    // Reference counting types are passed by value as pointers.
    ExpectedType = Ref->getPointeeType();
  }
  else if (RT->isBoxedType(Ty)) {
    // Boxed types denotes references when function returns reference. However
    // any value may be returned by reference, so check for variable is not
    // needed.
    ExpectedType = Runtime::getParameterOfSpecialization(Ty);
    //TODO: we must distinguish between reference and values.
    MustBeVariable = !RT->isBoxedType(E->getType());
  }
  if (Ty->isLValueReferenceType() || Ty->isPointerType()) { // TODO:
    if (Runtime::getArrayPtrType() != Ty)
      MustBeVariable = true;
  }
  if (MustBeVariable && !isPhpVariableExpr(E)) {
    Diag(E->getExprLoc(), diag::err_expected) << "l-value";
    IsInvalid = true;
  }

  QualType ExprType = E->getType();
  switch (comparePHPTypes(ExpectedType, ExprType)) {
  case TC_weak:
    if (!isStrictTypes()) {
      diagnoseImplicitTypeCast(E->getExprLoc(), ExpectedType, ExprType);
      break;
    } // Fall through
    
  case TC_incompatible:
    if (IsReturn)
      Diag(E->getExprLoc(), diag::err_php_invalid_return_type)
      << getPHPTypeName(ExpectedType) << getPHPTypeName(E->getType());
    else
      Diag(E->getExprLoc(), diag::err_php_invalid_argument_type)
      << getPHPTypeName(ExpectedType) << getPHPTypeName(E->getType());
    IsInvalid = true;
    break;

  case TC_value_dependent:
  case TC_strict:
    break;
  }
  return IsInvalid;
}

/// \brief Checks argument vs parameters, prints diagnostics on mismatch.
/// \returns True on error.
///
bool Sema::checkFunctionCallArguments(SourceLocation Loc, FunctionDecl *FD,
                                      ArrayRef<Expr*> Args) {
  // Check argument number.
  bool Variadic = isVariadic(FD);
  unsigned MinNumArgs = FD->getMinRequiredArguments() - Variadic;
  // NOTE: cast to unsigned in intentional - otherwise code will not 
  // compile on 64 bit platform (i.e. where size_t != unsigned)
  // The problem is caused by the fact that DiagnosticBuilder foes not
  // have << operator for long types, but SemaDiagnosticBuilder has template
  // operator which will attempt to invoke << operator for any type.

  // This cast should not cause any problem as we are unlikely
  // to allow more than UINT_MAX arguments in function call
  if (MinNumArgs > Args.size()) {
    Diag(Loc, diag::err_php_too_few_arguments_in_call)
        << 0 << MinNumArgs << (unsigned)Args.size();
    return true;
  }
  if (!Variadic && FD->getNumParams() < Args.size()) {
    Diag(Loc, diag::err_php_too_many_arguments_in_call)
        << 0 << FD->getNumParams() << (unsigned)Args.size();
    return true;
  }

  // Check argument types.
  bool IsInvalid = false;
  bool ReachedVariadic = false;
  QualType ParamType;
  for (unsigned I = 0; I < Args.size(); ++I) {
    if (!ReachedVariadic)
      ParamType = FD->getParamDecl(I)->getType();
    if (RT->isArgPackType(ParamType)) {
      ReachedVariadic = true;
      ParamType = Runtime::getParameterOfSpecialization(ParamType);
    }
    IsInvalid |= ParamType->isVoidType() || checkTypeVsDeclaration(Args[I], ParamType);
  }
  return IsInvalid;
}

}
