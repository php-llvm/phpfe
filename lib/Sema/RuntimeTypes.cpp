//===--- RuntimeTypes.cpp -- Interface with PHP RuntimeTypes ---------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Implements access to types used by runtime.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/DeclTemplate.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Sema/Lookup.h"
#include "phpfe/Sema/PhpSema.h"
//------------------------------------------------------------------------------

//------ Helper macros ---------------------------------------------------------

#define LOAD_GLOBAL_TYPE_DECL(TypeName, BaseName)                              \
    BaseName##Type = Semantics.loadCType(TypeName,                             \
                                         (NamedDecl**)&BaseName##Decl);        \
    if (BaseName##Type.isNull())                                               \
      return false;

#define LOAD_RT_TYPE_DECL(TypeName, BaseName)                                  \
    BaseName##Type = Semantics.loadRTType(TypeName,                            \
                                          (NamedDecl**)&BaseName##Decl);       \
    if (BaseName##Type.isNull())                                               \
      return false;

#define LOAD_NESTED_TYPE_DECL(TypeName, BaseName, Ctx)                         \
    BaseName##Type = Semantics.loadType(TypeName,                              \
                                        (NamedDecl**)&BaseName##Decl, Ctx);    \
    if (BaseName##Type.isNull())                                               \
      return false;

#define LOAD_GLOBAL_TYPE(TypeName, BaseName)                                   \
    BaseName##Type = Semantics.loadCType(TypeName, nullptr);                   \
    if (BaseName##Type.isNull())                                               \
      return false;

#define LOAD_RT_TYPE(TypeName, BaseName)                                       \
    BaseName##Type = Semantics.loadRTType(TypeName, nullptr);                  \
    if (BaseName##Type.isNull())                                               \
      return false;

#define LOAD_GLOBAL_TEMPLATE(TName, BaseName)                                  \
    BaseName##TemplateDecl = Semantics.loadTemplateType(TName, nullptr);       \
    if (BaseName##TemplateDecl == nullptr)                                     \
      return false;

#define LOAD_RT_TEMPLATE(TName, BaseName)                                      \
    BaseName##TemplateDecl = Semantics.loadTemplateType(TName,                 \
                                                Semantics.getRTNamespace());   \
    if (BaseName##TemplateDecl == nullptr)                                     \
      return false;

#define LOAD_CONST_TYPE(TypeName)                                              \
    load_const_type(Semantics, TypeName##Type, Const##TypeName##Type);

#define LOAD_PTR_TYPE(TypeName)                                                \
    load_ptr_type(Semantics, TypeName##Type, TypeName##PtrType);

#define LOAD_REF_TYPE(TypeName)                                                \
    load_ref_type(Semantics, TypeName##Type, TypeName##LRefType);

#define LOAD_TYPE_INFO(TypeName)                                               \
    load_type_info(Semantics, TypeName##Type, TypeName##Info);

#define LOAD_FIELD_DECL(BaseName, FieldName)                                   \
    load_field_decl(Semantics, FieldName, BaseName##Decl, BaseName##FieldName##Decl);

//------------------------------------------------------------------------------


namespace phpfe {

//------ Static data members ---------------------------------------------------

// box
QualType         RuntimeTypes::PlainBoxType;
TypeSourceInfo  *RuntimeTypes::PlainBoxInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::PlainBoxDecl = nullptr;

// const box
QualType         RuntimeTypes::ConstPlainBoxType;
TypeSourceInfo  *RuntimeTypes::ConstPlainBoxInfo = nullptr;

// Box
QualType         RuntimeTypes::ClassBoxType;
TypeSourceInfo  *RuntimeTypes::ClassBoxInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::ClassBoxDecl = nullptr;

// const Box
QualType         RuntimeTypes::ConstClassBoxType;
TypeSourceInfo  *RuntimeTypes::ConstClassBoxInfo = nullptr;

// const Box &
QualType         RuntimeTypes::ConstClassBoxLRefType;
TypeSourceInfo  *RuntimeTypes::ConstClassBoxLRefInfo;

// NumberBox
QualType         RuntimeTypes::NumberBoxType;
TypeSourceInfo  *RuntimeTypes::NumberBoxInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::NumberBoxDecl = nullptr;

// ArrayBox
QualType         RuntimeTypes::ArrayBoxType;
TypeSourceInfo  *RuntimeTypes::ArrayBoxInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::ArrayBoxDecl = nullptr;

// Value
QualType         RuntimeTypes::ValueInfoType;
TypeSourceInfo  *RuntimeTypes::ValueInfoInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::ValueInfoDecl = nullptr;
FieldDecl       *RuntimeTypes::ValueInfoValueField = nullptr;

// const Value
QualType         RuntimeTypes::ConstValueInfoType;
TypeSourceInfo  *RuntimeTypes::ConstValueInfoInfo = nullptr;

// unit record
QualType         RuntimeTypes::UnitRecordType;
TypeSourceInfo  *RuntimeTypes::UnitRecordInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::UnitRecordDecl = nullptr;

QualType         RuntimeTypes::ConstUnitRecordType;
TypeSourceInfo  *RuntimeTypes::ConstUnitRecordInfo = nullptr;

// module unit handler
QualType         RuntimeTypes::UnitExecuteHandlerType;
TypeSourceInfo  *RuntimeTypes::UnitExecuteHandlerInfo = nullptr;

// UnitState
QualType         RuntimeTypes::UnitStateType;
TypeSourceInfo  *RuntimeTypes::UnitStateInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::UnitStateDecl = nullptr;

// VarRecord
QualType         RuntimeTypes::VarRecordType;
TypeSourceInfo  *RuntimeTypes::VarRecordInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::VarRecordDecl = nullptr;


// UnitState *
QualType         RuntimeTypes::UnitStatePtrType;
TypeSourceInfo  *RuntimeTypes::UnitStatePtrInfo = nullptr;

// Array
QualType         RuntimeTypes::ArrayType;
TypeSourceInfo  *RuntimeTypes::ArrayInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::ArrayDecl = nullptr;

// ConstArray
QualType         RuntimeTypes::ConstArrayType;
TypeSourceInfo  *RuntimeTypes::ConstArrayInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::ConstArrayDecl = nullptr;

// Array
QualType         RuntimeTypes::ArrayPtrType;
TypeSourceInfo  *RuntimeTypes::ArrayPtrInfo = nullptr;

// BaseArray::association
QualType         RuntimeTypes::AssociationType;
TypeSourceInfo  *RuntimeTypes::AssociationInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::AssociationDecl = nullptr;

// string_ref
QualType         RuntimeTypes::StringRefType;
TypeSourceInfo  *RuntimeTypes::StringRefInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::StringRefDecl = nullptr;

// String
QualType         RuntimeTypes::StringType;
TypeSourceInfo  *RuntimeTypes::StringInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::StringDecl = nullptr;

// StringBox
QualType         RuntimeTypes::StringBoxType;
TypeSourceInfo  *RuntimeTypes::StringBoxInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::StringBoxDecl = nullptr;

// Resource
QualType         RuntimeTypes::ResourceType;
TypeSourceInfo  *RuntimeTypes::ResourceInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::ResourceDecl = nullptr;

// Object
QualType         RuntimeTypes::ObjectType;
TypeSourceInfo  *RuntimeTypes::ObjectInfo = nullptr;
CXXRecordDecl   *RuntimeTypes::ObjectDecl = nullptr;

// Object *
QualType         RuntimeTypes::ObjectPtrType;
TypeSourceInfo  *RuntimeTypes::ObjectPtrInfo = nullptr;

// templates
ClassTemplateDecl *RuntimeTypes::BoxedTemplateDecl = nullptr;
ClassTemplateDecl *RuntimeTypes::RefBoxTemplateDecl = nullptr;
ClassTemplateDecl *RuntimeTypes::ArgPackTemplateDecl = nullptr;

// Builtin types
QualType         RuntimeTypes::VoidType;
QualType         RuntimeTypes::BoolType;
QualType         RuntimeTypes::DoubleType;

QualType         RuntimeTypes::IntValueType;
unsigned         RuntimeTypes::IntValueWidth;


CXXBaseSpecifier *RuntimeTypes::BaseOfRefBox = nullptr;
CXXBaseSpecifier *RuntimeTypes::BaseOfBoxed = nullptr;
CXXBaseSpecifier *RuntimeTypes::BaseOfBox = nullptr;

// Constants
APInt            RuntimeTypes::MaxInteger;
APInt            RuntimeTypes::MinInteger;
APInt            RuntimeTypes::ZeroInteger;
APInt            RuntimeTypes::OneInteger;
APInt            RuntimeTypes::MinusOneInteger;


static void load_type_info(Sema &S, QualType QType, TypeSourceInfo *&TInfo) {
  TInfo = S.getTypeSourceInfo(QType);
}


static void load_const_type(Sema &S, QualType QType, QualType &ConstType) {
  ConstType = QType;
  ConstType.addConst();
}


static void load_ptr_type(Sema &S, QualType QType, QualType &PtrType) {
  PtrType = S.Context.getPointerType(QType);
}


static void load_ref_type(Sema &S, QualType QType, QualType &RefType) {
  RefType = S.Context.getLValueReferenceType(QType);
}


static void load_field_decl(Sema &S, const char *FName,
                            CXXRecordDecl *Base, FieldDecl *&FD) {
  IdentifierInfo *FNameId = S.getPreprocessor().getIdentifierInfo(FName);
  DeclarationName DN(FNameId);
  DeclarationNameInfo DName(DN, SourceLocation());

  LookupResult Member(S, DName, Sema::LookupMemberName);
  S.LookupQualifiedName(Member, Base);
  assert(Member.getResultKind() == LookupResult::Found);
  FD = Member.getAsSingle<FieldDecl>();
}


bool RuntimeTypes::load(Sema &Semantics) {
  LOAD_GLOBAL_TYPE_DECL ("box", PlainBox);
  LOAD_TYPE_INFO        (PlainBox);
  LOAD_CONST_TYPE       (PlainBox);
  LOAD_TYPE_INFO        (ConstPlainBox);

  LOAD_RT_TYPE_DECL     ("Box", ClassBox);
  LOAD_TYPE_INFO        (ClassBox);
  LOAD_CONST_TYPE       (ClassBox);
  LOAD_TYPE_INFO        (ConstClassBox);
  LOAD_REF_TYPE         (ConstClassBox);
  LOAD_TYPE_INFO        (ConstClassBoxLRef);

  LOAD_RT_TYPE_DECL     ("NumberBox", NumberBox);
  LOAD_TYPE_INFO        (NumberBox);

  LOAD_RT_TYPE_DECL     ("ArrayBox", ArrayBox);
  LOAD_TYPE_INFO        (ArrayBox);

  LOAD_RT_TYPE_DECL     ("ValueInfo", ValueInfo);
  LOAD_TYPE_INFO        (ValueInfo);
  LOAD_CONST_TYPE       (ValueInfo);
  LOAD_TYPE_INFO        (ConstValueInfo);

  LOAD_RT_TYPE_DECL     ("Unit", UnitRecord);
  LOAD_TYPE_INFO        (UnitRecord);
  LOAD_CONST_TYPE       (UnitRecord);
  LOAD_TYPE_INFO        (ConstUnitRecord);

  LOAD_RT_TEMPLATE      ("Boxed", Boxed);
  LOAD_RT_TEMPLATE      ("RefBox", RefBox);
  LOAD_RT_TEMPLATE      ("ArgPack", ArgPack);

  LOAD_RT_TYPE          ("unit_execute_handler", UnitExecuteHandler);
  LOAD_TYPE_INFO        (UnitExecuteHandler);

  LOAD_RT_TYPE_DECL     ("UnitState", UnitState);
  LOAD_TYPE_INFO        (UnitState);
  LOAD_PTR_TYPE         (UnitState);
  LOAD_TYPE_INFO        (UnitStatePtr);

  LOAD_RT_TYPE_DECL     ("VarRecord", VarRecord);
  LOAD_TYPE_INFO        (VarRecord);

  LOAD_RT_TYPE_DECL     ("IArray", Array);
  LOAD_TYPE_INFO        (Array);

  LOAD_RT_TYPE_DECL     ("ConstArray", ConstArray);
  LOAD_TYPE_INFO        (ConstArray);

  LOAD_PTR_TYPE         (Array);
  LOAD_TYPE_INFO        (ArrayPtr);

  LOAD_NESTED_TYPE_DECL ("association", Association, ConstArrayDecl);
  LOAD_TYPE_INFO        (Association);

  LOAD_GLOBAL_TYPE_DECL ("string_ref", StringRef);
  LOAD_TYPE_INFO        (StringRef);

  LOAD_RT_TYPE_DECL     ("String", String);
  LOAD_TYPE_INFO        (String);

  LOAD_RT_TYPE_DECL     ("StringBox", StringBox);
  LOAD_TYPE_INFO        (StringBox);

  LOAD_RT_TYPE_DECL     ("IObject", Object);
  LOAD_TYPE_INFO        (Object);

  LOAD_PTR_TYPE         (Object);
  LOAD_TYPE_INFO        (ObjectPtr);

  LOAD_GLOBAL_TYPE      ("intvalue_t", IntValue);

  // builtin types
  VoidType = Semantics.Context.VoidTy;
  BoolType = Semantics.Context.BoolTy;
  DoubleType = Semantics.Context.DoubleTy;
  assert(IntValueType->isIntegerType());

  CXXRecordDecl *RefBoxDecl = RefBoxTemplateDecl->getTemplatedDecl();
  assert(RefBoxDecl->getNumBases() == 1);
  BaseOfRefBox = RefBoxDecl->bases_begin();

  CXXRecordDecl *BoxedDecl = BoxedTemplateDecl->getTemplatedDecl();
  assert(BoxedDecl->getNumBases() == 1);
  BaseOfBoxed = BoxedDecl->bases_begin();

  assert(ClassBoxDecl->getNumBases() == 1);
  BaseOfBox = ClassBoxDecl->bases_begin();

  IntValueWidth = Semantics.Context.getIntWidth(IntValueType);
  assert(IntValueWidth >=
         Semantics.Context.getIntWidth(Semantics.Context.LongTy));

  MaxInteger = APInt::getSignedMaxValue(IntValueWidth);
  MinInteger = APInt::getSignedMinValue(IntValueWidth);
  ZeroInteger = APInt::getNullValue(IntValueWidth);
  OneInteger = APInt::getOneBitSet(IntValueWidth, 0);
  MinusOneInteger = -OneInteger;

  return true;
}


bool RuntimeTypes::isBooleanType(QualType x) {
  x = x.getNonReferenceType().getLocalUnqualifiedType().getCanonicalType();
  x = unboxType(x);
  return x->isBooleanType();
}


bool RuntimeTypes::isIntegerType(QualType x) {
  x = x.getNonReferenceType().getLocalUnqualifiedType().getCanonicalType();
  x = unboxType(x);
  return (!x->isBooleanType() && x->isIntegerType());
}


bool RuntimeTypes::isFloatingType(QualType x) {
  x = x.getNonReferenceType().getLocalUnqualifiedType().getCanonicalType();
  x = unboxType(x);
  return x->isFloatingType();
}

bool RuntimeTypes::isCStringType(QualType x) {
  QualType U = x.getLocalUnqualifiedType();
  return U->isArrayType() &&
         U->getArrayElementTypeNoTypeQual()->isAnyCharacterType();
}


bool RuntimeTypes::isStringType(QualType x) {
  x = x.getNonReferenceType().getLocalUnqualifiedType().getCanonicalType();
  x = unboxType(x);
  return x == StringRefType ||
         x == StringType ||
         x == StringBoxType ||
         isCStringType(x);
}


bool RuntimeTypes::isArrayType(QualType x) {
  if (x->isPointerType())
    x = x->getPointeeType();
  x = x.getNonReferenceType().getLocalUnqualifiedType().getCanonicalType();
  x = unboxType(x);
  return x == ArrayType ||
         x == ConstArrayType ||
         x == ArrayBoxType;
}


bool RuntimeTypes::isObjectType(QualType x) {
  // Filter out other PHP types that may be represented by classes.
  if (isArrayType(x) || isStringType(x))
    return false;
  if (x->isPointerType())
    x = x->getPointeeType();
  x = x.getNonReferenceType().getLocalUnqualifiedType().getCanonicalType();
  x = unboxType(x);
  if (x == ObjectType)
    return true;
  if (const RecordType *RT = x->getAs<RecordType>()) {
    CXXRecordDecl *RD = cast<CXXRecordDecl>(RT->getDecl());
    return RD->isDerivedFrom(getObjectDecl());
  }
  return false;
}


bool RuntimeTypes::isResourceType(QualType x) {
  return false;//TODO:
}


bool RuntimeTypes::isUniversalType(QualType x) {
  x = x.getNonReferenceType().getLocalUnqualifiedType().getCanonicalType();
  x = unboxType(x);
  return x == ClassBoxType || x == PlainBoxType;
}


bool RuntimeTypes::isUniversalPlainType(QualType x) {
  x = x.getNonReferenceType().getLocalUnqualifiedType().getCanonicalType();
  x = unboxType(x);
  return x == PlainBoxType;
}


/// \brief Returns true if expression of type ExprTy may be used where type
/// TargetTy is required.
///
/// This function checks type compatibility. Type 'A' is compatible with type
/// 'B' if 'A' is identical to 'B' or can be converted to it in some 'standard'
/// way.
///
/// The set of standard conversion is determined by algorithms of Legalizer,
/// these are conversions applied by it by default. For instance, type
/// 'string_ref' is compatible with 'StringBox', as there is a constructor that
/// makes the conversion string_ref->StringBox. Type 'String' is compatible with
/// as Legalizer makes the conversion from Box to string_ref by calling method
/// 'get_string()'. But 'string_ref' is not compatible with 'double', although
/// there is conversion 'string_ref->double' but it is not applied by default.
///
bool RuntimeTypes::areTypesCompatible(QualType TargetTy, QualType ExprTy) {
  QualType TargetType = unboxType(TargetTy).getLocalUnqualifiedType()
                                           .getCanonicalType();
  QualType ExprType = unboxType(ExprTy).getLocalUnqualifiedType()
                                       .getCanonicalType();

  if (TargetType == ExprType)
    return true;

  // If target type is universal, any PHP type is compatible with it. Also,
  // expression of universal type may keep value of any type, so do not report
  // about incompatibility.
  if (isUniversalType(TargetType) || isUniversalType(ExprType))
    return true;

  // TODO: objects

  // All string types are compatible with each other.
  if (isStringType(TargetType) && isStringType(ExprType))
    return true;

  // All array types are compatible with each other.
  if (isArrayType(TargetType) && isArrayType(ExprType))
    return true;

  // NumberBox is compatible with double, integer, another number NumberBox.
  if ((TargetType == NumberBoxType && isNumberType(ExprType)) ||
      (ExprType == NumberBoxType && isNumberType(TargetType)))
    return true;

  // Otherwise type don't match. We do not allow matching some types, for
  // instance 'integer' with 'double', as these conversions are not applied in
  // some cases.
  return false;
}


bool RuntimeTypes::isNarrowerType(QualType Ty, QualType RefTy) {
  // Remove various type decorations.
  if (Ty->isPointerType())
    Ty = Ty->getPointeeType();
  Ty.removeLocalFastQualifiers();
  Ty = unboxType(Ty.getCanonicalType());

  // Types 'Box' and 'box' are the most wide types.
  if (isUniversalType(RefTy))
    return !isUniversalType(Ty);

  if (isObjectType(RefTy)) {
    //TODO: check class hierarchy.
    return false;
  }

  if (isArrayType(RefTy))
    return false;

  // String may be implicitly converted to float, integer and boolean types.
  if (isStringType(RefTy))
    return Ty->isRealType();

  if (RefTy->isFloatingType())
    return Ty->isIntegerType();

  if (RefTy->isIntegerType())
    return Ty->isBooleanType();

  if (RefTy->isBooleanType())
    return false;

  llvm_unreachable("unexpected type");
}


bool RuntimeTypes::areTypesEquivalent(QualType Ty, QualType RefTy) {
  // Remove type decorations.
  if (Ty->isPointerType())
    Ty = Ty->getPointeeType();
  Ty.removeLocalFastQualifiers();
  Ty = unboxType(Ty.getCanonicalType());
  if (RefTy->isPointerType())
    RefTy = RefTy->getPointeeType();
  RefTy.removeLocalFastQualifiers();
  RefTy = unboxType(RefTy.getCanonicalType());

  if (isUniversalType(RefTy))
    return isUniversalType(Ty);

  if (isObjectType(RefTy)) {
    //TODO: check class hierarchy
    return isObjectType(Ty);
  }

  if (isArrayType(RefTy))
    return isArrayType(Ty);

  if (isStringType(RefTy))
    return isStringType(Ty);

  if (RefTy->isFloatingType())
    return Ty->isFloatingType();

  if (RefTy->isBooleanType())
    return Ty->isBooleanType();

  if (RefTy->isIntegerType())
    return Ty->isIntegerType() && !Ty->isBooleanType();

  if (RefTy->isVoidType())
    return Ty->isVoidType();

  llvm_unreachable("unexpected type");
}


bool RuntimeTypes::isNotWiderType(QualType Ty, QualType RefTy) {
  return isNarrowerType(Ty, RefTy) || areTypesEquivalent(Ty, RefTy);
}


bool RuntimeTypes::isUniversalClassType(QualType x) {
  x = x.getNonReferenceType().getLocalUnqualifiedType().getCanonicalType();
  return x == ClassBoxType;
}


bool RuntimeTypes::isBoxedType(QualType x) {
  if (!x->isRecordType())
    return false;
  if (isUniversalType(x))
    return true;
  const CXXRecordDecl *CDecl = x->getAsCXXRecordDecl();
  if (auto Spec = dyn_cast<ClassTemplateSpecializationDecl>(CDecl)) {
    for (auto S : BoxedTemplateDecl->specializations())
      if (S == Spec)
        return true;
  }
  return CDecl->isDerivedFrom(PlainBoxDecl);
}


bool RuntimeTypes::isRefBoxType(QualType x) {
  if (const CXXRecordDecl *CDecl = x->getAsCXXRecordDecl()) {
    if (auto Spec = dyn_cast<ClassTemplateSpecializationDecl>(CDecl)) {
      for (auto S : RefBoxTemplateDecl->specializations())
        if (S == Spec)
          return true;
    }
  }
  return false;
}


QualType RuntimeTypes::unboxType(QualType x) {
  if (!x->isRecordType())
    return x;

  // Remove RefBox and Boxed decorations.
  QualType TParamType = getParameterOfSpecialization(x);
  if (!TParamType.isNull())
    x = TParamType;

  // We consider types ArrayBox, StringBox etc as unboxed, even though they are
  // boxes. We cannot proceed to more elementary type in these cases.

  return x;
}


bool RuntimeTypes::isBoxedSpecialization(QualType T, QualType TargetType) {
  if (auto *Spec = T->getAs<TemplateSpecializationType>()) {
    return Spec->getTemplateName().getAsTemplateDecl() == BoxedTemplateDecl &&
           getParameterOfSpecialization(T) == TargetType;
  }

  if (T->isRecordType()) {
    if (auto *TSpec =
           dyn_cast<ClassTemplateSpecializationDecl>(T->getAsCXXRecordDecl())) {
      return TSpec->getSpecializedTemplate() == BoxedTemplateDecl &&
             getParameterOfSpecialization(T) == TargetType;
    }
  }
  return false;
}


QualType RuntimeTypes::getParameterOfSpecialization(QualType Ty) {
  if (!Ty->isRecordType())
    return QualType();
  CXXRecordDecl *RDecl = Ty->getAsCXXRecordDecl();
  if (auto Spec = dyn_cast<ClassTemplateSpecializationDecl>(RDecl)) {
    auto TArgs = Spec->getTemplateArgs().asArray();
    assert(TArgs.size() == 1);
    return TArgs.front().getAsType();
  }
  return QualType();
}


bool RuntimeTypes::isSpecializationOf(CXXRecordDecl *RD,
                                      ClassTemplateDecl *TDecl) {
  if (!RD)
    return false;
  auto *TSpec = dyn_cast<ClassTemplateSpecializationDecl>(RD);
  if (!TSpec)
    return false;
  for (auto Spec : TDecl->specializations()) {
    if (TSpec == Spec)
      return true;
  }
  return false;
}


bool RuntimeTypes::isArgPackType(QualType x) {
  return isSpecializationOf(x->getAsCXXRecordDecl(),
                            getArgPackTemplateDecl());
}


bool RuntimeTypes::isNumberBoxType(QualType x) {
  return x == NumberBoxType ||
         isBoxedSpecialization(x, NumberBoxType);
}


bool RuntimeTypes::isNumberType(QualType x) {
  return
    x == NumberBoxType ||
    x == getIntValueType() ||
    x == getDoubleType() ||
    isBoxedSpecialization(x, NumberBoxType) ||
    isBoxedSpecialization(x, getIntValueType()) ||
    isBoxedSpecialization(x, getDoubleType());
}

}
