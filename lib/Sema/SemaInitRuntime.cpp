//===--- SemaInitRuntime.cpp -- Interface with PHP Runtime ------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Code that load definitions describing runtime execution environment.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "phpfe/Sema/RuntimeTypes.h"
#include "phpfe/Sema/PhpSema.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


QualType Sema::loadType(StringRef TypeName, NamedDecl **D, DeclContext *DC) {
  QualType Res = findType(TypeName, D, DC);
#ifdef _DEBUG
  if (Res.isNull()) {
    llvm::errs() << "Type " << TypeName << " cannot be found" << "\n";
  }
#endif
  return Res;
}

QualType Sema::loadCType(StringRef TypeName, NamedDecl **D) {
  QualType Res = findType(TypeName, D, nullptr);
#ifdef _DEBUG
  if (Res.isNull()) {
    llvm::errs() << "Type " << TypeName << " cannot be found" << "\n";
  }
#endif
  return Res;
}


QualType Sema::loadRTType(StringRef TypeName, NamedDecl **D) {
  QualType Res = findType(TypeName, D, RTNamespace);
#ifdef _DEBUG
  if (Res.isNull()) {
    llvm::errs() << "Type phprt::" << TypeName << " cannot be found" << "\n";
  }
#endif
  return Res;
}


ClassTemplateDecl *Sema::loadTemplateType(StringRef TName, DeclContext *DC) {
  ClassTemplateDecl *Res = findTemplate(TName, DC);
#ifdef _DEBUG
  if (!Res) {
    llvm::errs() << "Template " << TName << " cannot be found" << "\n";
  }
#endif
  return Res;
}


void Sema::loadRuntimeTypes() {
  RTNamespace = findNamespace("phprt");
  assert(RTNamespace);

  if (!RuntimeTypes::load(*this)) {
    llvm_unreachable("Cannot load rumtime types");
  }
}

}
