//===--- SemaLookup.cpp - AST Builder and Semantic Analysis ---------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// This file implements the actions class which performs semantic analysis and
// builds an AST out of a parse stream.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/DeclTemplate.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Sema/Lookup.h"
// #include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Sema/PhpSema.h"
#include <iostream>
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


static QualType peelType(Sema &S, NamedDecl *ND, NamedDecl **D) {
  // Convert declaration into type. We support only two kinds of declaration,
  // typedef and tag.
  const Type *ResType = nullptr;

  if (TypedefDecl *TD = dyn_cast<TypedefDecl>(ND)) {
    QualType T = TD->getUnderlyingType();
    assert(!T.isNull());
    ResType = T.getTypePtr();
  }
  else if (RecordDecl *TD = dyn_cast<RecordDecl>(ND)) {
    QualType T = S.Context.getRecordType(TD);
    assert(!T.isNull());
    ResType = T.getTypePtr();
  }
  else if (EnumDecl *ED = dyn_cast<EnumDecl>(ND)) {
    QualType T = S.Context.getEnumType(ED);
    assert(!T.isNull());
    ResType = T.getTypePtr();
  }
  else {
    llvm_unreachable("unsupported declaration");
  }

  // Process type.
  if (const TypedefType *TT = dyn_cast<TypedefType>(ResType)) {
    QualType T = TT->desugar();
    assert(!T.isNull());
    ResType = T.getTypePtr();
  }
  if (const ElaboratedType *ET = dyn_cast<ElaboratedType>(ResType)) {
    QualType T = ET->getNamedType();
    assert(ET->getQualifier() == 0); // Not supported yet.
    assert(!T.isNull());
    ResType = T.getTypePtr();
  }

  // For the result type build corresponding declaration.
  if (const TagType *TT = dyn_cast<TagType>(ResType))
    ND = TT->getDecl();

  // Keep declaration if necessary.
  if (D)
    *D = ND;

  return QualType(ResType, 0);
}


QualType Sema::findType(StringRef TypeName, NamedDecl **D, DeclContext *DC) {
  IdentifierInfo *TypeIdent = getPreprocessor().getIdentifierInfo(TypeName);
  NamedDecl *ND = nullptr;

  if (!DC)
    DC = Context.getTranslationUnitDecl();

  // see Sema::getTypeName
  LookupResult R(*this, TypeIdent, SourceLocation(), Sema::LookupOrdinaryName);
  if (LookupQualifiedName(R, DC)) {
    typedef std::pair<QualType, NamedDecl*> Item;
    SmallVector<Item, 2> FoundTypes;
    for (LookupResult::iterator I = R.begin(), E = R.end(); I != E; ++I) {
      NamedDecl *FoundDecl = (*I)->getUnderlyingDecl();
      NamedDecl *ND = nullptr;
      QualType T = peelType(*this, FoundDecl, &ND);
      if (std::find_if(FoundTypes.begin(), FoundTypes.end(),
                       [T](const Item &v) {
                         return v.first == T;
                       }) == FoundTypes.end())
        FoundTypes.emplace_back(Item(T, ND));
    }
    if (FoundTypes.size() != 1) {
      llvm_unreachable("Cannot handle overloaded functions yet");
    } else {
      ND = FoundTypes.front().second;
    }
    assert(ND != 0);
    if (D)
      *D = ND;
    return FoundTypes.front().first;
  }

  return QualType();
}


ClassTemplateDecl *Sema::findTemplate(StringRef TypeName, DeclContext *DC) {
  IdentifierInfo *TypeIdent = getPreprocessor().getIdentifierInfo(TypeName);

  if (!DC)
    DC = Context.getTranslationUnitDecl();

  // see Sema::getTypeName
  LookupResult R(*this, TypeIdent, SourceLocation(), Sema::LookupOrdinaryName);
  if (LookupQualifiedName(R, DC)) {
    SmallVector<ClassTemplateDecl*, 2> FoundTemplates;
    for (LookupResult::iterator I = R.begin(), E = R.end(); I != E; ++I) {
      if (isa<ClassTemplateDecl>(*I))
        FoundTemplates.push_back(cast<ClassTemplateDecl>(*I));
    }
    assert(FoundTemplates.size() == 1 && "multiple templates found");
    return FoundTemplates.front();
  }
  return nullptr;
}


// Find global variable in dlecaration in php::var namespace
VarDecl *Sema::findGlobalVariable(DeclarationName Name) {
  LookupResult R(*this, Name.getAsIdentifierInfo(), SourceLocation(),
                 Sema::LookupOrdinaryName);
  CXXScopeSpec DummySS;
  LookupQualifiedName(R, getVarNamespace(), DummySS);
  if (R.isSingleResult())
    return dyn_cast<VarDecl>(R.getFoundDecl());
  return nullptr;
}


// This method is used to handle generic reference to variable
// It should be able to handle usage of both local and global 
// variables as well as function/method parameters
VarDecl *Sema::findVariable(Scope *S, DeclarationName Name) {
  // Lookup for local variables first
  for (auto I = IdResolver.begin(Name); I != IdResolver.end(); ++I) {
    if (isa<VarDecl>(*I))
      return cast<VarDecl>(*I);
  }

  //TODO: do we need to walk up the scope and lookup for variable?

  if (isUnitFunctionContext())
    return findGlobalVariable(Name);

  //TODO: handle function global variables
  return nullptr;
}


FunctionDecl *Sema::findFunction(Scope *S, DeclarationName Name) {

  for (auto I = IdResolver.begin(Name); I != IdResolver.end(); ++I) {
    if (isa<FunctionDecl>(*I))
      return cast<FunctionDecl>(*I);
  }

  while (true) {
    if (DeclContext *DC = S->getEntity()) {
      if (DC == UnitFunction)
        DC = getFuncNamespace();
      auto Res = DC->lookup(Name);
      for (auto I : Res) {
        if (FunctionDecl *FD = dyn_cast<FunctionDecl>(I)) {
          if (isa<RecordDecl>(FD->getDeclContext()))
            return FD;
          // skip conditional functions
          if (FD->getStorageClass() != SC_Static)
            return FD;
        }
      }
      if (S->isFunctionScope())
        return nullptr;
    }
    S = S->getParent();
  }
}


FunctionDecl *Sema::findFunction(CXXScopeSpec &SS, DeclarationName Name) {
  DeclContext *DC = getContextForScopeSpec(SS);
  assert(DC);
  if (CXXRecordDecl *CD = dyn_cast<CXXRecordDecl>(DC)) {
    // Search for method.
    assert(0 && "not implemented");
  }

  // Only namespaces can be found here.
  NamespaceDecl *NS = cast<NamespaceDecl>(DC);
  NamespaceDecl *FNS = getFuncNamespace(NS);

  // Search the specified namespace
  for (auto I : FNS->lookup(Name)) {
    if (FunctionDecl *FD = dyn_cast<FunctionDecl>(I)) {
      return FD;
    }
  }

  return nullptr;
}


static VarDecl *findGlobalConstant(Sema &S, const DeclarationNameInfo &Name) {
  NamespaceDecl *GNS = S.getConstNamespace(S.getPhpNamespace());
  auto R = GNS->lookup(Name.getName());
  if (R.empty())
    return nullptr;
  assert(R.size() == 1);
  return cast<VarDecl>(R.front());
}


/// \brief Search for declaration of unqualified constant.
///
VarDecl *Sema::findConstant(Scope *S, const DeclarationNameInfo &Name) {
  DeclarationName DN = Name.getName();

  // First search the current scope chain.
  if (S) {
    //TODO: search decls in he scope, not in IdRes.
    for (auto I = IdResolver.begin(DN); I != IdResolver.end(); ++I) {
      if (VarDecl *VD = dyn_cast<VarDecl>(*I))
        if (VD->getDeclContext()->getPrimaryContext() == getConstNamespace())
          return VD;
    }
  }

  // Search current namespace first. This operation may load declarations from
  // external storage.
  if (isInNamedNamespace()) {
    auto Res = getConstNamespace(CurNamespace)->lookup(DN);
    for (auto I = Res.begin(), E = Res.end(); I != E; ++I) {
      if (VarDecl *VD = dyn_cast<VarDecl>(*I))
        if (VD->getDeclContext()->getPrimaryContext()
            == getConstNamespace()->getPrimaryContext())
          return VD;
    }
  }

  // Finally search global namespace. This operation may load declarations from
  // external storage.
  return findGlobalConstant(*this, Name);
}


VarDecl *Sema::findConstant(CXXScopeSpec &SS, const DeclarationNameInfo &Name) {
  if (SS.isEmpty() ||
      SS.getScopeRep()->getKind() == NestedNameSpecifier::Global) {
    // Search global namespace.
    return findGlobalConstant(*this, Name);
  }

  if (CXXRecordDecl *R = SS.getScopeRep()->getAsRecordDecl()) {
    (void)R;
    llvm_unreachable("not implemented");
    return nullptr;
  }

  // TODO: if there are USE directives, apply them.

  if (NamespaceDecl *NS = SS.getScopeRep()->getAsNamespace()) {
    NamespaceDecl *CNS = getConstNamespace(NS);
    auto R = CNS->lookup(Name.getName());
    if (R.empty())
      return nullptr;
    assert(R.size() == 1);
    return cast<VarDecl>(R.front());
  }

  return nullptr;
}

}
