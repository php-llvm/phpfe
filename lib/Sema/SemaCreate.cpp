//===--- SemaCreate.cpp -- PHP Semantic Implementation ----------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Implements part of Sema interface that is responsible for creation of
// AST objects.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/ExprCXX.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Sema/Designator.h"
#include "clang/Sema/Lookup.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "shared/value_kinds.h"
#include <iostream>
#include <numeric>
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


NamespaceDecl* Sema::findNamespace(StringRef Name, NamespaceDecl *Owner) {
  IdentifierInfo *II = PP.getIdentifierInfo(Name);
  DeclarationName DN(II);

  DeclContext *DC;
  if (Owner)
    DC = Owner;
  else
    DC = Context.getTranslationUnitDecl();

  auto Res = DC->lookup(DN);
  NamedDecl *Found = nullptr;
  for (auto I : Res) {
    if (isa<NamespaceDecl>(I)) {
      Found = I;
      break;
    }
  }
  if (Found)
    return cast<NamespaceDecl>(Found)->getMostRecentDecl();
  return nullptr;
}


NamespaceDecl* Sema::createNamespace(StringRef Name, NamespaceDecl *Owner) {
  IdentifierInfo *II = PP.getIdentifierInfo(Name);

  DeclContext *DC;
  if (Owner)
    DC = Owner;
  else
    DC = Context.getTranslationUnitDecl();

  NamespaceDecl *Previous = findNamespace(Name, Owner);
  NamespaceDecl *NS = NamespaceDecl::Create(Context, DC, /*IsInline*/ false,
      /*StartLoc*/ SourceLocation(), /*IdLoc*/ SourceLocation(), II,
      /*PrevDecl*/ Previous);
  DeclContext *SavedDC = CurContext;
  CurContext = DC;
  CurContext->addDecl(NS);
  CurContext = SavedDC;

  return NS;
}


Expr *Sema::packSideEffect(SourceLocation Loc, Expr *Side, Expr *Val) {
  if (!Side)
    return Val;
  Expr *CommaExpr = ActOnBinaryOp(Loc, tok::comma, Side, Val).get();
  CommaExpr->setValueKind(Val->getValueKind());
  return CommaExpr;
}


Expr *Sema::unpackSideEffect(Expr *E, Expr *&Side) {
  if (BinaryOperator * RBO = dyn_cast<BinaryOperator>(E)) {
    if (RBO->getOpcode() == BO_Comma) {
      Side = RBO->getLHS();
      return RBO->getRHS();
    }
  }
  Side = nullptr;
  return E;
}


// This method creates StringLiteral based on given string
// Implementation is mostly based on ActOnStringLiteral
// but it works with string instead of token.

// TODO: maybe move to separate file?
StringLiteral * Sema::BuildStringLiteral(StringRef Str, SourceLocation StrLoc) {
  QualType CharTy = Context.CharTy;
  CharTy.addConst();
  // String are always assumed to be ASCII.
  StringLiteral::StringKind Kind = StringLiteral::Ascii;

  // String type is const char[N] where N is string length + 1 (to accoung for /0)
  QualType StrTy = Context.getConstantArrayType(CharTy,
                                                llvm::APInt(32, Str.size() + 1),
                                                ArrayType::Normal, 0);

  return StringLiteral::Create(Context, Str,
                               Kind, false, StrTy,
                               &StrLoc,
                               1);
}


FloatingLiteral *Sema::BuildDoubleLiteral(double Val, SourceLocation Loc) {
  llvm::APFloat FloatVal(Val);
  return FloatingLiteral::Create(Context, FloatVal,
                                 /*isExact*/true, Context.DoubleTy, Loc);
}


Expr *Sema::BuildUnresolvedCall(StringRef Name, SourceLocation NameLoc,
                      QualType RetTy, MultiExprArg Args, SourceLocation RPLoc) {
  Expr *NameExpr = BuildStringLiteral(Name, NameLoc);
  //TODO: is this really VK_RValue?
  return 
    new(Context) CallExpr(Context, NameExpr, Args, RetTy, VK_RValue, RPLoc);
}


Expr *Sema::buildFieldAccess(StringRef FieldName, CXXRecordDecl *CD, Expr *Base) {
  SourceLocation Loc = Base->getLocStart();
  IdentifierInfo *II = PP.getIdentifierInfo(FieldName);
  DeclarationNameInfo DNI(DeclarationName(II), Loc);
  LookupResult FieldRes(*this, DNI, LookupMemberName);
  if (LookupQualifiedName(FieldRes, CD)) {
    FieldDecl *MD = cast<FieldDecl>(FieldRes.getFoundDecl());
    // TODO: cache search results.
    MemberExpr *ME = MemberExpr::Create(Context, Base, false, Loc,
      NestedNameSpecifierLoc(), SourceLocation(), MD, DeclAccessPair(), DNI,
      nullptr, MD->getType(), VK_LValue, OK_Ordinary);
    return ME;
  }

  llvm_unreachable("unknowd field");
}


QualType Sema::BuildFunctionType(QualType ReturnType,
                                 ArrayRef<QualType> ArgTypes) {
  FunctionProtoType::ExtProtoInfo EPI;
  EPI.ExceptionSpec.Type = EST_None;
  EPI.ExtInfo = FunctionType::ExtInfo();//EI;
  return Context.getFunctionType(ReturnType, ArgTypes, EPI);
}


FunctionDecl *Sema::BuildFunctionForwardDeclaration(CXXScopeSpec SS,
                                                    SourceLocation Loc,
                                                    DeclarationName Name) {
  FunctionDecl *Prev = nullptr;
  if (SS.isEmpty())
    Prev = findFunction(getCurScope(), Name);
  else
    Prev = findFunction(SS, Name);
  if (Prev)
    return Prev;

  DeclContext *DC = nullptr;
  if (SS.isEmpty())
    DC = getFuncNamespace();
  else
    DC = getContextForScopeSpec(SS);
  assert(DC);

  QualType FnType = BuildFunctionType(Runtime::getClassBoxType(),
                                      ArrayRef<QualType>());
  TypeSourceInfo *TInfo = getTypeSourceInfo(FnType);

  // Create declaration.
  DeclarationNameInfo DNI(Name, Loc);
  return FunctionDecl::Create(Context, DC, Loc, DNI, FnType, TInfo, SC_None,
                              false, false);
}


Stmt *Sema::BuildDefaultReturnStmt(QualType RetTy, SourceLocation Loc) {
  if (RetTy == Context.VoidTy)
    return new (Context) ReturnStmt(Loc);

  if (!Runtime::isUniversalType(RetTy))
    return nullptr;

  Expr *BoxExpr = RT->createNullBox(Loc);
  return new (Context) ReturnStmt(Loc, BoxExpr, nullptr);
}


static Expr *BuildArrayLocatorsVariable(Sema &S, SourceLocation Loc,
                                        const ArrayConstValue &Array) {
  SmallVector<unsigned, 64> Locators(Array.size());
  SmallVector<Expr*, 64> LocatorsInitializers;

  std::iota(Locators.begin(), Locators.end(), 0);
  std::sort(Locators.begin(), Locators.end(), [&](unsigned I, unsigned J) {
    return Array.getKey(I) < Array.getKey(J);
  });

  for (auto V : Locators) {
    LocatorsInitializers.push_back(S.ActOnIntegerConstant(Loc, V).get());
  }

  Runtime &RT = S.getRuntime();
  auto &Context = S.Context;

  InitListExpr *LocatorsInit = cast<InitListExpr>(
    S.ActOnInitList(Loc, LocatorsInitializers, Loc.getLocWithOffset(1)).get());
  VarDecl *LocatorsVD = RT.createConstArrayLocatorsVariable(LocatorsInit);
  Expr *Ref =
    S.BuildDeclRefExpr(LocatorsVD, LocatorsVD->getType(), VK_LValue, Loc).get();
  QualType BaseType = Context.getPointerType(Context.UnsignedIntTy);
  return
    S.ImpCastExprToType(Ref, BaseType, CK_ArrayToPointerDecay, VK_RValue).get();
}


Expr *Sema::BuildArrayInitVariable(SourceLocation Loc,
                                   const ArrayConstValue &Val) {
  APInt Size(Runtime::getIntValueWidth(), Val.size(), true);
  Expr *LocatorsExpr = Val.isVector() ? nullptr
      : BuildArrayLocatorsVariable(*this, Loc, Val);

  // Create initializers for array elements
  SmallVector<Expr*, 4> Initializers;
  for (const ArrayItem &Item : Val) {
    if (Val.isVector())
      Initializers.push_back(RT->createBoxInitializer(Loc, Item.getValue()));
    else
      Initializers.push_back(RT->createAssosiationInitializer(Loc, Item));
  }
  Expr *Init = ActOnInitList(Loc, Initializers, Loc.getLocWithOffset(1)).get();

  // Create variable for array of elements
  QualType BaseType = Val.isVector() ? Runtime::getConstPlainBoxType()
                                     : Runtime::getAssociationType();

  VarDecl *ContentVD =
      RT->createConstArrayContentVariable(BaseType, cast<InitListExpr>(Init));
  Expr *Content =
      BuildDeclRefExpr(ContentVD, ContentVD->getType(), VK_LValue, Loc).get();
  Content = ImpCastExprToType(Content, Context.getPointerType(BaseType),
      CK_ArrayToPointerDecay, VK_RValue).get();

  // Create ConstArray variable
  VarDecl *CArray = RT->createConstArray(Loc);
  addUnitDeclaration(CArray);
  if (Val.isVector()) {
    APInt Start(Runtime::getIntValueWidth(), 0, true);
    if (!Val.empty())
      Start = Val.front().getKey()->getInt();
    RT->addArrayContent(CArray, Content, Size, Start);
  } else {
    assert(LocatorsExpr);
    RT->addArrayContent(CArray, Content, Size, LocatorsExpr);
  }
  CArray = RT->doneConstArray(CArray);

  ExprResult Ref = BuildDeclRefExpr(CArray, CArray->getType(), VK_LValue, Loc);

  return new (Context)UnaryOperator(Ref.get(), clang::UO_AddrOf,
    Context.getPointerType(CArray->getType()), VK_RValue, OK_Ordinary, Loc);
}


Expr *Sema::BuildConstCast(Expr *E) {
  assert(E->getType()->isPointerType());
  SourceLocation Loc = E->getExprLoc();
  QualType BaseType = E->getType()->getPointeeType();
  BaseType.removeLocalConst();
  BaseType = Context.getPointerType(BaseType);
  TypeSourceInfo *TSI = getTypeSourceInfo(BaseType);
  return CXXConstCastExpr::Create(Context, BaseType, VK_RValue, E, TSI,
    Loc, Loc, SourceRange(Loc, Loc));
}


Stmt *Sema::createNullStmt(SourceLocation Loc) {
  return new(Context) NullStmt(Loc);
}


VarDecl *Sema::createGlobalVariable(StringRef VarName, SourceLocation Loc) {
  IdentifierInfo *II = PP.getIdentifierInfo(VarName);
  VarDecl *VD = VarDecl::Create(Context, getVarNamespace(), Loc, Loc, II,
                                getPlainBoxType(), PlainBoxInfo, SC_None);
  getVarNamespace()->addDecl(VD);
  return VD;
}


VarDecl *Sema::createLocalVariable(Scope *S, StringRef VarName,
                                   SourceLocation Loc) {
  // Find closest function scope.
  while (S) {
    if (S->isFunctionScope())
      break;
    S = S->getParent();
  }
  assert(S);
  assert(S->getEntity());
  assert(cast<FunctionDecl>(S->getEntity()) == getCurFunctionDecl());
  IdentifierInfo *II = PP.getIdentifierInfo(VarName);
  VarDecl *VD = VarDecl::Create(Context, getCurFunctionDecl(), Loc, Loc, II,
                                getClassBoxType(), getClassBoxInfo(), SC_None);
  getCurFunctionDecl()->addDecl(VD);
  PushOnScopeChains(VD, S, false);
  return VD;
}


/// \brief Creates implicit cast node so that the given expression has required
/// type.
///
/// This method creates implicit casts, that have no representation in source
/// code. Compiler uses them to adjust types so that when it sees this
/// expression next time, it won't emit message about wrong type.
///
Expr *Sema::createImplicitCast(QualType TargetType, Expr *Arg) {
  QualType ArgType = Arg->getType().getLocalUnqualifiedType();
  if (TargetType == ArgType)
    return Arg;

  // Avoid creation of extra cast nodes if argument is constant.
  if (ConstValue *CV = getConstantValue(Arg)) {
    ConstValue *CVC = getConstantValue(CV, TargetType, Arg->getLocStart());
    assert(CVC);
    return getConstantExpr(CVC, Arg->getLocStart(), true);
  }

  CastKind CKind = CK_Dependent;

  if (TargetType->isFloatingType()) {
    if (ArgType->isIntegerType())
      CKind = CK_IntegralToFloating;
    return ImplicitCastExpr::Create(Context, TargetType, CKind, Arg, nullptr,
                                    VK_RValue);
  }

  if (TargetType->isBooleanType()) {
    if (ArgType->isFloatingType())
      CKind = CK_FloatingToBoolean;
    else if (ArgType->isIntegerType())
      CKind = CK_IntegralToBoolean;
    return ImplicitCastExpr::Create(Context, TargetType, CKind, Arg, nullptr,
                                    VK_RValue);
  }

  if (TargetType->isIntegerType()) {
    if (ArgType->isFloatingType())
      CKind = CK_FloatingToIntegral;
    else if (ArgType->isBooleanType())
      CKind = CK_IntegralCast;
    return ImplicitCastExpr::Create(Context, TargetType, CKind, Arg, nullptr,
                                    VK_RValue);
  }

  if (isNumberType(TargetType)) {
    return ImplicitCastExpr::Create(Context, TargetType,
                                    CK_Dependent, Arg, nullptr, VK_RValue);
  }

  if (isStringType(TargetType)) {
    return ImplicitCastExpr::Create(Context, TargetType,
                                    CK_Dependent, Arg, nullptr, VK_RValue);
  }

  llvm_unreachable("unimplemented");
}

}
