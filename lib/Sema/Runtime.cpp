//===--- Runtime.cpp -- Interface with PHP Runtime --------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Implements interface of PHP compiler to runtime.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "llvm/ADT/APInt.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/DeclTemplate.h"
#include "clang/AST/ExprCXX.h"
#include "clang/AST/CXXInheritance.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Sema/Designator.h"
#include "clang/Sema/Lookup.h"
#include "clang/Sema/Template.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "shared/section_names.h"
#include "shared/value_kinds.h"

#include <iostream>
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;

bool Runtime::loadBuiltinFunctions() {
  register_builtin_functions();
  return true;
}


void Runtime::initSection(StringRef SectionName, int Flags) {
  auto Section = Semantics.Context.SectionInfos.find(SectionName);
  if (Section == Semantics.Context.SectionInfos.end()) {
    Semantics.Context.SectionInfos[SectionName] =
      ASTContext::SectionInfo(nullptr, SourceLocation(), Flags);
    return;
  }
  assert(Section->second.SectionFlags == Flags);
}


const char *Runtime::getUnitSectionName() {
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Win32)
    return WIN_UNIT_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Linux)
    return LINUX_UNIT_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Apple)
    return OSX_UNIT_SECTION_NAME;
  llvm_unreachable("Unsupported target");
}


const char *Runtime::getUnitStateSectionName() {
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Win32)
    return WIN_UNITSTATE_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Linux)
    return LINUX_UNITSTATE_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Apple)
    return OSX_UNITSTATE_SECTION_NAME;
  llvm_unreachable("Unsupported target");
}


const char *Runtime::getGVRSectionName() {
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Win32)
    return WIN_GVR_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Linux)
    return LINUX_GVR_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Apple)
    return OSX_GVR_SECTION_NAME;
  llvm_unreachable("Unsupported target");
}


const char *Runtime::getCTConstantSectionName() {
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Win32)
    return WIN_CTC_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Linux)
    return LINUX_CTC_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Apple)
    return OSX_CTC_SECTION_NAME;
  llvm_unreachable("Unsupported target");
}


const char *Runtime::getRTConstantSectionName() {
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Win32)
    return WIN_RTC_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Linux)
    return LINUX_RTC_SECTION_NAME;
  if (Semantics.Context.getTargetInfo().getTriple().getOS() == Triple::Apple)
    return OSX_RTC_SECTION_NAME;
  llvm_unreachable("Unsupported target");
}


void Runtime::initialize() {
  bool Res = loadBuiltinFunctions();
  assert(Res); (void)Res;

  initSection(getUnitSectionName(), UNIT_SECTION_FLAGS);
  initSection(getUnitStateSectionName(), UNITSTATE_SECTION_FLAGS);
  initSection(getGVRSectionName(), GVR_SECTION_FLAGS);
  initSection(getCTConstantSectionName(), CTC_SECTION_FLAGS);
  initSection(getRTConstantSectionName(), RTC_SECTION_FLAGS);
}


Runtime::Runtime(Sema &S) : Semantics(S) {
}


// NOTE: using llvm::DenseMap fails (despite having the same interface
// as std::map). DenseMap requires key type to have method isEqual which
// is absent in both std::string and llvm::SmallString.

// Designation cache (".a.b.c" ...)
static std::map<std::string, Designation> Designations;
// Designator cache (".a", ".b" ...)
static std::map<std::string, Designator> Designators;


Designation &Runtime::makeDesignation(StringRef DesigStr) {
  auto Ptr = Designations.find(DesigStr);
  if (Ptr != Designations.end())
    return Ptr->second;

  // Split str into designators
  SmallVector<StringRef, 2> DesignatorsStrings;
  StringRef Res = DesigStr;
  while (!Res.empty()) {
    auto Split = Res.split(".");
    DesignatorsStrings.push_back(Split.first);
    Res = Split.second;
  }

  // Create new designation
  Designation D;
  for (auto DesignatorStr : DesignatorsStrings) {
    auto DPtr = Designators.find(DesignatorStr);
    if (DPtr != Designators.end()) {
      D.AddDesignator(DPtr->second);
    } else {
      Preprocessor &PP = Semantics.getPreprocessor();
      IdentifierInfo *FldName = PP.getIdentifierInfo(DesignatorStr);
      Designator Desig = Designator::getField(FldName, SourceLocation(),
                                              SourceLocation());
      Designators.insert(std::make_pair(DesignatorStr, Desig));
      D.AddDesignator(Desig);
    }
  }

  return Designations[DesigStr] = D;
}


FunctionDecl *Runtime::findRuntimeFunction(StringRef Name) {
  // First find the function declaration. Runtime functions are searched for in
  // the namespace 'phprt'.
  Preprocessor &PP = Semantics.getPreprocessor();
  IdentifierInfo *FunctionName = PP.getIdentifierInfo(Name);
  LookupResult R(Semantics, FunctionName, SourceLocation(),
                 Sema::LookupOrdinaryName);
  FunctionDecl *Func = nullptr;
  if (Semantics.LookupQualifiedName(R, Semantics.getRTNamespace())) {
    for (LookupResult::iterator I = R.begin(), E = R.end(); I != E; ++I) {
      NamedDecl *D = (*I)->getUnderlyingDecl();
      if (FunctionDecl *FD = dyn_cast<FunctionDecl>(D)) {
        assert(!Func && "ambiguous function");
        Func = FD;
      }
    }
  }
  return Func;
}


// This methods performs additional argument conversions required to make
// function call legal in C++.
// It is assumed than arguments have already been processed by Legalizer
// or do not require such processing.
void Runtime::fitCallArguments(FunctionDecl *FD, MutableArrayRef<Expr *> Args) {
  // TODO: take into account default arguments?
  assert(FD->getNumParams() >= Args.size());
  for (unsigned i = 0; i < FD->getNumParams(); ++i) {
    QualType ParamT = FD->getParamDecl(i)->getType();
    Expr *Arg = Args[i];
    // If function expects const Box& or Box and arguments is plain box
    // we can safely perform cast without losing any data and creating
    // temporary objects
    if (isUniversalClassType(ParamT)) {
      if (isUniversalPlainType(Arg->getType()))
        Args[i] = Semantics.castPlainTypeToConstRef(Arg);

      // If argument is C string we should call Box constructor
      else if (Runtime::isCStringType(Arg->getType())) {
        QualType T = Semantics.Context.getPointerType(Semantics.Context.CharTy);
        Expr *ArgPtr = Semantics.ImpCastExprToType(Arg, T,
                                                   CK_ArrayToPointerDecay).get();
        Args[i] = createExprBox(ArgPtr);
      }
    } else if (ParamT == getStringRefType()) {
      if (StringLiteral *Str = dyn_cast<StringLiteral>(Arg))
        Args[i] = createStringRefFromLiteral(Str);
    }
  }
}


Expr *Runtime::makeRuntimeFunctionCall(StringRef FName, MutableArrayRef<Expr *> Args) {
  // If the function declaration is found, we can build the call.
  if (FunctionDecl *Func = findRuntimeFunction(FName)) {
    // TODO: fix source locations
    fitCallArguments(Func, Args);
    return Semantics.createFunctionCall(Func, SourceLocation(),
                                        SourceLocation(), Args,
                                        SourceLocation()).get();
  }

#ifdef _DEBUG
  llvm::errs() << "Cannot find function phprt::" << FName << "\n";
#endif
  llvm_unreachable("cannot find runtime function");
  return nullptr;
}


Expr *Runtime::makeFunctionCall(Expr *FnName, ArrayRef<Expr *> Args) {
  return makeRuntimeFunctionCall("call_function", MutableArrayRef<Expr*>(FnName));
}


Expr *Runtime::makeRuntimeStaticMethodCall(StringRef MName, CXXRecordDecl *Class,
                                           MutableArrayRef<Expr *> Args) {
  IdentifierInfo *FunctionName =
    Semantics.getPreprocessor().getIdentifierInfo(MName);

  // Perform qualified lookup in class.
  LookupResult R(Semantics, FunctionName, SourceLocation(),
                 Sema::LookupOrdinaryName);
  Semantics.LookupQualifiedName(R, Class);

  // At least one method must be present.
  assert(!R.empty());

  // Create expression referencing methods.
  CXXScopeSpec EmptySS;
  Expr *FN = Semantics.BuildDeclarationNameExpr(EmptySS, R, false, false).get();
  assert(FN);
  Expr *MethodCall = Semantics.ActOnCallExpr(nullptr, FN, SourceLocation(),
                                             Args, SourceLocation()).get();
  assert(MethodCall);
  return MethodCall;
}


Expr *Runtime::MakeRuntimeMethodCall(StringRef MName, CXXRecordDecl *Class,
                                     Expr *Base, MultiExprArg Args) {
  IdentifierInfo *FunctionName =
      Semantics.getPreprocessor().getIdentifierInfo(MName);

  // Perform qualified lookup in class.
  LookupResult R(Semantics, FunctionName, SourceLocation(),
                 Sema::LookupOrdinaryName);
  Semantics.LookupQualifiedName(R, Class);

  // At least one method must be present.
  assert(!R.empty());

  // Create expression referencing methods. This may be normal MemberExpr, but
  // may also be an UnresolvedMemberExpr if overloading is needed.
  CXXScopeSpec EmptySS;
  Expr *MethodRef = Semantics.BuildMemberReferenceExpr(Base, Base->getType(),
      Base->getLocStart(), Base->getType()->isPointerType(),
      EmptySS, SourceLocation(), nullptr, R, nullptr, nullptr).get();

  // This method builds CallExpr and resolves any overloading.
  Expr* MethodCall = Semantics.BuildCallToMemberFunction(nullptr, MethodRef,
      SourceLocation(), Args, SourceLocation()).get();

  return MethodCall;
}


Expr *Runtime::MakeStaticMethodCall(StringRef MName, CXXRecordDecl *Class,
                                    MultiExprArg Args, SourceLocation Loc) {
  IdentifierInfo *MethodName =
    Semantics.getPreprocessor().getIdentifierInfo(MName);
  // Perform the required lookup.
  LookupResult R(Semantics, MethodName, SourceLocation(), 
                 Sema::LookupOrdinaryName);
  Semantics.LookupQualifiedName(R, Class);
  
  // At least one method must be present.
  assert(!R.empty());
  CXXScopeSpec SS;
  auto Ref = Semantics.BuildDeclarationNameExpr(SS, R, false);

  return Semantics.ActOnCallExpr(nullptr, Ref.get(), Loc, Args, Loc).get();
}


bool Runtime::addIntegerInitializer(handler H, StringRef Desig, int Val) {
  ExprResult Value = Semantics.ActOnIntegerConstant(H->getLocStart(), Val);
  ExprResult Init = Semantics.ActOnDesignatedInitializer(
    makeDesignation(Desig), H->getLocStart(), true, Value.get());
  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());
  Ptr->second.Initializers.push_back(Init.get());
  return true;
}


bool Runtime::addStringInitializer(handler H, StringRef Desig, StringRef Val) {
  StringLiteral *ValLiteral = Semantics.BuildStringLiteral(Val, SourceLocation());
  ExprResult Init = Semantics.ActOnDesignatedInitializer(
      makeDesignation(Desig), H->getLocStart(), true, ValLiteral);
  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());
  Ptr->second.Initializers.push_back(Init.get());
  return true;
}

bool Runtime::addExprInitializer(handler H, StringRef Desig, Expr* Val) {
  ExprResult Init = Semantics.ActOnDesignatedInitializer(
    makeDesignation(Desig), H->getLocStart(), true, Val);
  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());
  Ptr->second.Initializers.push_back(Init.get());
  return true;
}

bool Runtime::addStringRefInitializer(handler H, StringRef Desig, StringRef Val) {
  ASTContext &Context = Semantics.Context;
  SourceLocation Loc = H->getLocation();
  SmallVector<Expr *, 2> InitExprs;

  // string_ref.data
  StringLiteral *ValLiteral = Semantics.BuildStringLiteral(Val, Loc);
  ExprResult DataInit = Semantics.ActOnDesignatedInitializer(
      makeDesignation("data"), Loc, true, ValLiteral);
  InitExprs.push_back(DataInit.get());
  
  // string_ref.len
  llvm::APInt NameLen(Context.getIntWidth(Context.UnsignedIntTy), Val.size());
  IntegerLiteral *SizeLiteral = IntegerLiteral::Create(
      Context, NameLen, Context.UnsignedIntTy, Loc);
  ExprResult SizeInit = Semantics.ActOnDesignatedInitializer(
      makeDesignation("len"), Loc, true, SizeLiteral);
  InitExprs.push_back(SizeInit.get());

  // create initializer
  ExprResult SRInit = Semantics.ActOnInitList(
      H->getLocation(), InitExprs, H->getLocation());
  ExprResult Init = Semantics.ActOnDesignatedInitializer(
      makeDesignation(Desig), H->getLocStart(), true, SRInit);

  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());
  Ptr->second.Initializers.push_back(Init.get());
  return true;
}


bool Runtime::addFunctionInitializer(handler H, StringRef Desig, FunctionDecl *EH) {
  // Make DeclRefExpr for current handler.
  ExprResult HandlerRef = Semantics.BuildDeclRefExpr(
                               EH, EH->getType(), VK_RValue, EH->getLocation());

  // Cast DeclRefExp to pointer.
  ASTContext& Context = Semantics.getASTContext();
  ExprResult CastToPtr = ImplicitCastExpr::Create(Context,
      Context.getPointerType(EH->getType()), CK_FunctionToPointerDecay,
      HandlerRef.get(), nullptr, VK_RValue);
  ExprResult HandlerInit = Semantics.ActOnDesignatedInitializer(
      makeDesignation(Desig), H->getLocStart(), true, CastToPtr.get());

  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());
  Ptr->second.Initializers.push_back(HandlerInit.get());

  return true;
}

Expr* Runtime::getVarAddr(VarDecl *VD) {
  ExprResult Ref = Semantics.BuildDeclRefExpr(VD, VD->getType(), VK_LValue, 
                                              VD->getLocation());

  return new (Semantics.Context)UnaryOperator(Ref.get(), UO_AddrOf,
                                Semantics.Context.getPointerType(VD->getType()),
                                VK_RValue, OK_Ordinary, VD->getLocation());
}

bool Runtime::addVarPtrInitializer(handler H, StringRef Desig, VarDecl *VD) {
  ExprResult VarPtr = getVarAddr(VD);

  ExprResult HandlerInit = Semantics.ActOnDesignatedInitializer(
    makeDesignation(Desig), H->getLocStart(), true, VarPtr.get());

  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());
  Ptr->second.Initializers.push_back(HandlerInit.get());

  return true;
}


VarDecl *Runtime::doneVariable(handler H) {
  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());
  SourceLocation Loc = H->getLocStart();
  Expr *Init = Semantics.ActOnInitList(Loc, Ptr->second.Initializers,
                                       Loc.getLocWithOffset(1)).get();
  Semantics.AddInitializerToDecl(H, Init, true, false);
  PendingVars.erase(Ptr);
  return H;
}


VarDecl *Runtime::createVariable(StringRef VarName, DeclContext *DC,
                                 TypeSourceInfo *TInfo, SourceLocation Loc,
                                 bool IsStatic) {
  ASTContext &Context = Semantics.getASTContext();

  IdentifierInfo *II = Semantics.PP.getIdentifierInfo(VarName);
  VarDecl *VD = VarDecl::Create(Context, DC,
                                Loc, Loc, II, TInfo->getType(), TInfo,
                                IsStatic ? SC_Static : SC_None);
  return VD;
}


void Runtime::placeInSection(ValueDecl *D, StringRef SectionName) {
  Attr *Section = Semantics.mergeSectionAttr(D, D->getLocation(), SectionName, 0);
  assert(Section);
  D->addAttr(Section);
  D->addAttr(UsedAttr::CreateImplicit(Semantics.Context));
}


Runtime::handler Runtime::createUnitRecord(FunctionDecl *UnitFunc) {
  VarDecl *UnitRecord = createVariable("__Unit",
      Semantics.Context.getTranslationUnitDecl(), ConstUnitRecordInfo,
      UnitFunc->getLocStart(), true);
  placeInSection(UnitRecord, getUnitSectionName());
  VarDescriptor VD;
  VD.Var = UnitRecord;
  PendingVars[UnitRecord] = VD;
  return UnitRecord;
}


bool Runtime::addUnitFlags(handler H, unsigned Flags) {
  return addIntegerInitializer(H, "flags", Flags);
}


bool Runtime::addUnitName(handler H, StringRef Name) {
  return addStringRefInitializer(H, "name", Name);
}


bool Runtime::addUnitVersion(handler H, StringRef Version) {
  return addStringRefInitializer(H, "version", Version);
}


bool Runtime::addUnitInitHandler(handler H, FunctionDecl *EH) {
  return addFunctionInitializer(H,"on_unit_init", EH);
}


bool Runtime::addUnitShutdownHandler(handler H, FunctionDecl *EH) {
  return addFunctionInitializer(H,"on_unit_shutdown", EH);
}


bool Runtime::addUnitLoadHandler(handler H, FunctionDecl *EH) {
  return addFunctionInitializer(H,"on_unit_load", EH);
}


bool Runtime::addUnitUnloadHandler(handler H, FunctionDecl *EH) {
  return addFunctionInitializer(H, "on_unit_unload", EH);
}


bool Runtime::addUnitExecuteHandler(handler H, FunctionDecl *EH) {
  return addFunctionInitializer(H, "on_unit_execute", EH);
}


VarDecl *Runtime::doneUnitRecord(handler H) {
  return doneVariable(H);
}


Runtime::handler Runtime::createUnitState(SourceLocation Loc,
                                          StringRef UnitName) {
  VarDecl *UnitState = createVariable(
      Twine("__UnitState_").concat(UnitName).str(),
      Semantics.Context.getTranslationUnitDecl(), UnitStateInfo, Loc, false);
  placeInSection(UnitState, getUnitStateSectionName());
  VarDescriptor VD;
  VD.Var = UnitState;
  PendingVars[UnitState] = VD;
  return UnitState;
}

bool Runtime::addUnitStateFlags(handler H, unsigned Flags) {
  return addIntegerInitializer(H, "flags", Flags);
}

bool Runtime::addUnitStateUnit(handler H, VarDecl *UnitRecord) {
  return addVarPtrInitializer(H, "unit", UnitRecord);
}

VarDecl *Runtime::doneUnitStateRecord(handler H) {
  return doneVariable(H);
}


VarDecl *Runtime::createConstArray(SourceLocation Loc) {
  static unsigned ArrayId = 0;
  QualType ArrayType = Runtime::getConstArrayType();
  ArrayType.addConst();
  TypeSourceInfo *TSI = Semantics.getTypeSourceInfo(ArrayType);
  VarDecl *CObject = createVariable(
    Twine("__ca_object_").concat(std::to_string(++ArrayId)).str(),
    Semantics.Context.getTranslationUnitDecl(), TSI, Loc, true);
  Semantics.Context.getTranslationUnitDecl()->addDecl(CObject);

  VarDescriptor VD;
  VD.Var = CObject;
  PendingVars[CObject] = VD;
  return CObject;
}


bool Runtime::addArrayContent(handler H, Expr *Content,
                              APInt Size, APInt Start) {
  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());

  auto &Ctx = Semantics.Context;

  Ptr->second.Initializers.push_back(Content);
  Ptr->second.Initializers.push_back(
    IntegerLiteral::Create(Ctx, Size, Ctx.LongTy, Content->getExprLoc()));
  Ptr->second.Initializers.push_back(
    IntegerLiteral::Create(Ctx, Start, Ctx.LongTy, Content->getExprLoc()));
  return true;
}


bool Runtime::addArrayContent(handler H, Expr *Content,
                              APInt Size, Expr *Locators) {
  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());

  auto &Ctx = Semantics.Context;

  Ptr->second.Initializers.push_back(Content);
  Ptr->second.Initializers.push_back(
    IntegerLiteral::Create(Ctx, Size, Ctx.LongTy, Content->getExprLoc()));
  Ptr->second.Initializers.push_back(Locators);
  return true;
}


VarDecl *Runtime::doneConstArray(handler H) {
  auto Ptr = PendingVars.find(H);
  assert(Ptr != PendingVars.end());

  SourceLocation Loc = H->getLocation();

  Expr *Init = Semantics.ActOnInitList(Loc, Ptr->second.Initializers,
    Loc.getLocWithOffset(1)).get();
  Semantics.AddInitializerToDecl(H, Init, true, false);

  return H;
}


/// \brief Creates declaration that will represent the specified PHP constant.
///
/// \param DC Context in which the new constant is created.
/// \param Name Constant name.
/// \param Init Expression that represents constant value. May be nullptr, in
///             this case runtime constant is created.
///
VarDecl  *Runtime::createConstant(DeclContext *DC,
                                  const DeclarationNameInfo &Name, Expr *Init) {
  QualType ConstType;
  TypeSourceInfo *ConstInfo = nullptr;
  const char *VarSection;
  // Any constant defined in emodule is externally visible and has strong
  // linkage even if its initializing expression is not a compile time constant.
  bool IsGlobal = (Semantics.getPhpOptions().Kind == PhpModuleKind::EModule);
  // Initializing expression is compile-time constant?
  bool IsCTConstant = Init && Semantics.isLiteralExpr(Init);

  // If:
  // - the current module is compiled as external module and
  // - the constant is initialized by compile time constant expression,
  // the constant is created in read-only memory.
  if (IsGlobal && IsCTConstant) {
    ConstType = ConstValueInfoType;
    ConstInfo = ConstValueInfoInfo;
    VarSection = getCTConstantSectionName();
  } else {
    ConstType = ValueInfoType;
    ConstInfo = ValueInfoInfo;
    VarSection = getRTConstantSectionName();
  }

  VarDecl *ConstD = VarDecl::Create(Semantics.Context, DC, Name.getBeginLoc(),
                            Name.getLoc(), Name.getName().getAsIdentifierInfo(),
                                    ConstType, ConstInfo, SC_None);
  placeInSection(ConstD, VarSection);
  if (!IsGlobal)
    ConstD->addAttr(WeakAttr::CreateImplicit(Semantics.Context));

  // If constant value is specified, store it as init expression. Actual
  // initializer will be set latter by legalizer.
  if (IsCTConstant)
    ConstD->setInit(Init);

  return ConstD;
}


void Runtime::initConstant(VarDecl *Var, Expr *IniVal) {
  Expr *Initializer = getInitCTConstant(Var, IniVal);
  Semantics.AddInitializerToDecl(Var, Initializer, true, false);
}


Expr *Runtime::generateRtConstantUse(StringRef Name, SourceLocation Loc,
                                     StringRef NsName) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(Semantics.BuildStringLiteral(Name, Loc));
  Args.push_back(Semantics.BuildStringLiteral(NsName, Loc));
  return makeRuntimeFunctionCall("find_constant", Args);
}


Expr *Runtime::generateGlobalRtConstantUse(VarDecl *VD) {
  ExprResult Base = Semantics.BuildDeclarationNameExpr(CXXScopeSpec(),
                 DeclarationNameInfo(VD->getDeclName(), VD->getLocation()), VD);
  assert(Base.isUsable());
  return MakeRuntimeMethodCall("get", ValueInfoDecl, Base.get(), MultiExprArg());
}


bool Runtime::isBoxedType(QualType x) {
  return isSpecializationOf(x->getAsCXXRecordDecl(),
                            Runtime::getBoxedTemplateDecl());
}


Expr *Runtime::createNullBox(SourceLocation Loc) {
  SmallVector<Expr*, 0> Args;
  return Semantics.BuildCXXTypeConstructExpr(
                    getClassBoxInfo(), Loc, Args, Loc).get();
}

Expr *Runtime::createStringBox(Expr *E) {
  SourceLocation Loc = E->getExprLoc();
  return Semantics.BuildCXXTypeConstructExpr(getStringBoxInfo(), Loc,
                                             MultiExprArg(E), Loc).get();
}


Expr *Runtime::createExprBox(Expr *E) {
  SourceLocation Loc = E->getExprLoc();
  return Semantics.BuildCXXTypeConstructExpr(getClassBoxInfo(), Loc,
                                             MultiExprArg(E), Loc).get();
}


Expr *Runtime::createArrayBox(Expr *E) {
  SourceLocation Loc = E->getExprLoc();
  return Semantics.BuildCXXTypeConstructExpr(getArrayBoxInfo(), Loc,
                                             MultiExprArg(E), Loc).get();
}


static Expr *castToDerived(Sema &S, QualType ResultType, Expr *E) {
  if (E->getType() == ResultType)
    return E;
  SourceLocation Loc = E->getExprLoc();
  DeclarationName Name;
  QualType BaseType = E->getType();
  QualType RefRefType = S.BuildReferenceType(BaseType, false, Loc, Name);

  // cast to (T&&)
  TypeSourceInfo *TSI = S.getTypeSourceInfo(RefRefType);
  Expr *Intermediate = S.BuildCStyleCastExpr(Loc, TSI, Loc, E).get();

  QualType RefType = S.BuildReferenceType(ResultType, E->isLValue(), Loc, Name);
  TSI = S.getTypeSourceInfo(RefType);
  return S.BuildCStyleCastExpr(Loc, TSI, Loc, Intermediate).get();
}


Expr *Runtime::castToClassBox(Expr *E) {
  if (Runtime::isUniversalType(E->getType())) {
    if (Runtime::getClassBoxType() == E->getType())
      return E;
    return castToDerived(Semantics, Runtime::getClassBoxType(), E);
  }
  assert(Runtime::isBoxedType(E->getType()));

  CXXCastPath BasePath;
  bool CastFailed = Semantics.CheckDerivedToBaseConversion(E->getType(), 
                              Runtime::getClassBoxType(), 
                              E->getLocStart(), E->getSourceRange(), &BasePath);
  assert(!CastFailed);
  (void)CastFailed;

  Expr *Result = Semantics.ImpCastExprToType(E, Runtime::getClassBoxType(), 
      CK_DerivedToBase, E->isLValue() ? VK_LValue : VK_RValue, &BasePath).get();
  return Result;
}


Expr *Runtime::castToArrayBox(Expr *E) {
  if (E->getType() == getArrayBoxType())
    return E;

  if (isUniversalType(E->getType()))
    return castToDerived(Semantics, Runtime::getArrayBoxType(), E);
  
  if (isBoxedType(E->getType())) {
    assert(unboxType(E->getType()) == getArrayBoxType());
    return castToArrayBox(castToClassBox(E));
  }

  llvm_unreachable("cannot cast unknown type to array box type");
}


Expr *Runtime::createConstArrayContent(QualType BaseType, InitListExpr *Init) {
  auto &Context = Semantics.Context;
  unsigned TypeSize = Context.getIntWidth(Context.UnsignedIntTy);
  llvm::APInt ArraySize(TypeSize, Init->getNumInits());
  QualType ArrayType = Context.getConstantArrayType(BaseType, ArraySize, 
                                                         ArrayType::Normal, 0);
  TypeSourceInfo *TSI = Semantics.getTypeSourceInfo(ArrayType);
  Expr *Array = Semantics.BuildCompoundLiteralExpr(
                  Init->getLBraceLoc(), TSI, Init->getRBraceLoc(), Init).get();
  return Semantics.ImpCastExprToType(Array, 
               Context.getPointerType(BaseType), CK_ArrayToPointerDecay).get();
}


static VarDecl *createConstArrayVariable(Sema &S, QualType BaseType,
                                        StringRef VarName, InitListExpr *Init) {
  auto &Context = S.Context;
  unsigned TypeSize = Context.getIntWidth(Context.UnsignedIntTy);
  llvm::APInt ArraySize(TypeSize, Init->getNumInits());
  QualType ArrayType = Context.getConstantArrayType(BaseType, ArraySize,
                                                    ArrayType::Normal, 0);
  TypeSourceInfo *TSI = S.getTypeSourceInfo(ArrayType);
  VarDecl *VD = VarDecl::Create(Context, Context.getTranslationUnitDecl(),
    Init->getLBraceLoc(), Init->getLocStart(), S.PP.getIdentifierInfo(VarName),
    ArrayType, TSI, SC_Static);
  S.AddInitializerToDecl(VD, Init, true, false);
  Context.getTranslationUnitDecl()->addDecl(VD);
  S.addUnitDeclaration(VD);
  return VD;
}


VarDecl *Runtime::createConstArrayContentVariable(QualType BaseType,
                                                  InitListExpr *Init) {
  static unsigned ArrayId = 0;
  std::string VarName = "__ca_content_" + std::to_string(++ArrayId);
  return createConstArrayVariable(Semantics, BaseType, VarName, Init);
}


VarDecl *Runtime::createConstArrayLocatorsVariable(InitListExpr *Init) {
  static unsigned ArrayId = 0;
  std::string VarName = "__ca_locators_" + std::to_string(++ArrayId);
  return createConstArrayVariable(Semantics, Semantics.Context.UnsignedIntTy,
                                  VarName, Init);
}


Expr *Runtime::createAssosiationInitializer(SourceLocation Loc,
                                            ArrayItem Item) {
  SmallVector<Expr *, 2> Inits;
  Inits.push_back(createBoxInitializer(Loc, Item.getValue()));
  Inits.push_back(createBoxInitializer(Loc, Item.getKey()));

  return Semantics.ActOnInitList(Loc, Inits, Loc.getLocWithOffset(1)).get();
}


Expr *Runtime::createVariadicArgument(SourceLocation Loc, QualType BaseType,
                                      MutableArrayRef<Expr *> Args) {
  SmallVector<Expr *, 2> ConstuctorArgs;
  SourceLocation RBLoc = Args.empty() ? Loc : Args.back()->getLocEnd();
  if (!Args.empty()) {
    Expr *Init = Semantics.ActOnInitList(Loc, Args, RBLoc).get();
    Init->setType(Semantics.Context.getPointerType(BaseType));

    Expr *Array = createConstArrayContent(BaseType, cast<InitListExpr>(Init));
    Expr *ArraySize = Semantics.ActOnIntegerConstant(Loc, Args.size()).get();

    ConstuctorArgs.push_back(ArraySize);
    ConstuctorArgs.push_back(Array);
  }

  TypeSourceInfo *TSI = Semantics.getTypeSourceInfo(
                                  Semantics.getArgPackType(Loc, BaseType));
  return Semantics.BuildCXXTypeConstructExpr(TSI, Loc, ConstuctorArgs, RBLoc)
      .get();
}


Expr *Runtime::createStringRef(Expr *E) {
  assert(isCStringType(E->getType()));

  SmallVector<Expr*, 1> CArgs;
  CArgs.push_back(E);
  return MakeStaticMethodCall("make", getStringRefDecl(),
                              CArgs, E->getExprLoc());
}


Expr *Runtime::createStringRefFromLiteral(StringLiteral *Str) {
  SmallVector<Expr *, 2> Args;
  QualType T = Semantics.Context.getPointerType(Semantics.Context.CharTy);
  Expr *ArgPtr = Semantics.ImpCastExprToType(Str, T,
                                             CK_ArrayToPointerDecay).get();
  Args.push_back(ArgPtr);
  Expr *ArgLen = Semantics.ActOnIntegerConstant(Str->getLocStart(),
                                                Str->getLength()).get();
  Args.push_back(ArgLen);
  InitListExpr *List = new (Semantics.Context) InitListExpr(Semantics.Context,
                                      SourceLocation(), Args, SourceLocation());
  List->setType(getStringRefType());
  Args.clear();
  Args.push_back(List);
  return Semantics.BuildCXXTypeConstructExpr(StringRefInfo, SourceLocation(),
                                             Args, SourceLocation()).get();
}


Expr *Runtime::createStringRefFromCharArray(Expr *E) {
  auto *CArrayT = cast<ConstantArrayType>(E->getType().getTypePtr());
  assert(CArrayT->getArrayElementTypeNoTypeQual()->isCharType());
  SmallVector<Expr *, 2> Args;
  QualType T = Semantics.Context.getPointerType(Semantics.Context.CharTy);
  Expr *ArgPtr = Semantics.ImpCastExprToType(E, T,
                                             CK_ArrayToPointerDecay).get();
  Args.push_back(ArgPtr);
  Expr *ArgLen = IntegerLiteral::Create(Semantics.Context, CArrayT->getSize(),
                             Semantics.Context.UnsignedIntTy, E->getLocStart());
  Args.push_back(ArgLen);
  return Semantics.BuildCXXTypeConstructExpr(StringRefInfo, SourceLocation(),
                                             Args, SourceLocation()).get();
}


/// \brief Change attributes of the given variable, so that it they fits
/// properties of global variables.
///
void Runtime::adjustGlobalVariableAttrs(VarDecl *GlobalVar) {
  if (Semantics.getPhpOptions().Threading != PhpThreading::Single)
    GlobalVar->setTSCSpec(TSCS_thread_local);
}


/// \brief Creates variable that stores mapping from global variable names to
/// their locations.
///
VarDecl *Runtime::createGlobalVariableRecord(VarDecl *Var) {
  VarDecl *VR = createVariable(Twine("__GVR_").concat(Var->getName()).str(),
                               Semantics.Context.getTranslationUnitDecl(),
                               VarRecordInfo, Var->getLocation(), false);
  Semantics.getASTContext().getTranslationUnitDecl()->addDecl(VR);
  placeInSection(VR, getGVRSectionName());

  // Need this because addXXXInitializer expects it.
  VarDescriptor VD;
  VD.Var = VR;
  PendingVars[VR] = VD;

  // The first element of the record is global variable name.
  addStringRefInitializer(VR, "name", Var->getName());

  // The second element is offset of the variable relative to TLS. We use
  // variable $GLOBALS as a reference to the TLS. All threads have the same
  // layout of TLS, so this offset must be an invariant. So if the address of
  // $GLOBALS is known, we can calculate address of any global variable having
  // the offset value.
  //
  // The offset value is stored as signed integer, intptr_t, because it may be
  // negative. This type is platform specific.

  ASTContext& Context = Semantics.getASTContext();

  // Find declaration of $GLOBALS variable
  VarDecl *Globals = Semantics.findGlobalVariable(
     DeclarationName(Semantics.getPreprocessor().getIdentifierInfo("GLOBALS")));

  // Compute variable address. It is not a compile time address, so it will be
  // evaluated at runtime, when the variable is initialized. The initialization
  // takes place in the context of main thread, so in fact we get address of
  // $GLOBALS for the main thread.
  Expr *GlobalsAddr = getVarAddr(Globals);

  // Compute the given global variable address. Again, this will be the address
  // of the variable in main thread, when this record is initialized.
  Expr *VarAddr = getVarAddr(Var);

  // We need to cast both values to intptr_t to compute their difference.
  Expr *GlobalsAddrAsInt = ImplicitCastExpr::Create(Context,
      Context.getIntPtrType(), CK_PointerToIntegral, GlobalsAddr, nullptr,
      VK_RValue);
  Expr *VarAddrAsInt = ImplicitCastExpr::Create(Context,
      Context.getIntPtrType(), CK_PointerToIntegral, VarAddr, nullptr,
      VK_RValue);

  // Compute the offset.
  Expr *Offset = Semantics.ActOnBinOp(nullptr, Var->getLocation(), tok::minus,
                                      GlobalsAddrAsInt, VarAddrAsInt).get();

  // Set computed value as initializer for offset field.
  addExprInitializer(VR, "offset", Offset);

  return doneVariable(VR);
}


// Generate expression for subscript access for reading.
Expr *Runtime::generateSubscriptRead(Expr *Base, Expr *Key) {
  assert(Base);
  assert(Key);
  QualType BaseType = Base->getType().getUnqualifiedType();

  // Convert complex types to simple ones:
  //     StringBox        -> string_ref
  //     String           -> string_ref
  //     Boxed<StringBox> -> string_ref
  //     Boxed<IArray>    -> IArray
  if (isBoxedType(BaseType)) {
    BaseType = unboxType(BaseType);
    if (isArrayType(BaseType))
      Base = MakeRuntimeMethodCall("get_array", getPlainBoxDecl(), Base,
                                   MutableArrayRef<Expr*>());
    else if (isStringType(BaseType))
      Base = MakeRuntimeMethodCall("get_string", getPlainBoxDecl(), Base,
                                   MutableArrayRef<Expr*>());
  }
  if (BaseType == StringType) {
    Base = MakeRuntimeMethodCall("get_string", getPlainBoxDecl(), Base,
                                 MutableArrayRef<Expr*>());
  }

  SmallVector<Expr *, 2> Args;

  // If the base is of universal type, we do not know what kind of subscript is
  // used (string, array, object), so generate generic variant.
  if (isUniversalType(BaseType)) {
    Args.push_back(Base);
    Args.push_back(Key);
    return makeRuntimeFunctionCall("read_subscript", Args);
  }

  // Process subscripting of arrays.
  if (BaseType == ArrayType) {
    Args.push_back(Key);
    return MakeRuntimeMethodCall("get", getArrayDecl(), Base, Args);
  }

  // Process subscripting of strings.
  if (BaseType == StringRefType) {
    Args.push_back(Key);
    return MakeRuntimeMethodCall("get", getStringRefDecl(), Base, Args);
  }

  // Process subscripting of objects.
  if (BaseType == ObjectType) {
    Args.push_back(Key);
    return MakeRuntimeMethodCall("get", getObjectDecl(), Base, Args);
  }

  // Other types cannot be seen here
  llvm_unreachable("invalis type");
}


Expr *Runtime::generateToBoolNode(Expr *Exp) {
  return MakeRuntimeMethodCall("to_bool", PlainBoxDecl, Exp, MultiExprArg());
}


Expr *Runtime::generateToIntegerNode(Expr *Exp) {
  return MakeRuntimeMethodCall("to_integer", PlainBoxDecl, Exp, MultiExprArg());
}


Expr *Runtime::generateToDoubleNode(Expr *Exp) {
  return MakeRuntimeMethodCall("to_double", PlainBoxDecl, Exp, MultiExprArg());
}


Expr *Runtime::generateToNumberNode(Expr *Exp) {
  return MakeRuntimeMethodCall("to_number", PlainBoxDecl, Exp, MultiExprArg());
}


Expr *Runtime::generateToStringNode(Expr *Exp) {
  return MakeRuntimeMethodCall("to_string", PlainBoxDecl, Exp, MultiExprArg());
}


Expr *Runtime::generateToArrayNode(Expr *Exp) {
  return MakeRuntimeMethodCall("to_array", PlainBoxDecl, Exp, MultiExprArg());
}

Expr *Runtime::generateToObjectNode(Expr *Exp) {
  return MakeRuntimeMethodCall("to_object", PlainBoxDecl, Exp, MultiExprArg());
}

Expr *Runtime::generateClearNode(Expr *Exp) {
  return MakeRuntimeMethodCall("clear", PlainBoxDecl, Exp, MultiExprArg());
}

Expr *Runtime::generateConvertToBoolNode(Expr *Exp) {
  return makeRuntimeFunctionCall("convert_to_bool", MultiExprArg(Exp));
}

Expr *Runtime::generateConvertToIntegerNode(Expr *Exp) {
  return makeRuntimeFunctionCall("convert_to_integer", MultiExprArg(Exp));
}

Expr *Runtime::generateConvertToDoubleNode(Expr *Exp) {
  return makeRuntimeFunctionCall("convert_to_double", MultiExprArg(Exp));
}

Expr *Runtime::generateConvertToStringNode(Expr *Exp) {
  return makeRuntimeFunctionCall("convert_to_string", MultiExprArg(Exp));
}

Expr *Runtime::generateConvertToArrayNode(Expr *Exp) {
  return makeRuntimeFunctionCall("convert_to_array", MultiExprArg(Exp));
}

Expr *Runtime::generateConvertToObjectNode(Expr *Exp) {
  return makeRuntimeFunctionCall("convert_to_object", MultiExprArg(Exp));
}

Expr *Runtime::generateGetArrayNode(Expr *Exp) {
  return MakeRuntimeMethodCall("get_array", PlainBoxDecl, Exp, MultiExprArg());
}

Expr *Runtime::generateGetIntegerNode(Expr *Exp) {
  return MakeRuntimeMethodCall("get_integer", PlainBoxDecl, Exp, MultiExprArg());
}
  
Expr *Runtime::generateGetDoubleNode(Expr *Exp) {
  return MakeRuntimeMethodCall("get_double", PlainBoxDecl, Exp, MultiExprArg());
}

Expr *Runtime::generateGetStringNode(Expr *Exp) {
  return MakeRuntimeMethodCall("get_string", PlainBoxDecl, Exp, MultiExprArg());
}

}
