//===--- SemaConstant.cpp -------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Part of Semantics responsible for constant support.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/Sema/Lookup.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Support/Utilities.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


/// \brief Returns true if the given name specifies builtin constant.
///
/// Builtin constant (such as 'true', 'null') cannot be redefined in any
/// namespace.
///
bool Sema::isBuiltinConstant(StringRef Name) {
  if (StringRef("null").equals_lower(Name))
    return true;
  if (StringRef("true").equals_lower(Name))
    return true;
  if (StringRef("false").equals_lower(Name))
    return true;
  return false;
}


/// \brief Returns expression representing builtin constant of the specified
/// name.
///
ExprResult Sema::getBuiltinConstant(StringRef Name, SourceLocation Loc) {
  if (StringRef("null").equals_lower(Name))
    return ActOnPHPNullLiteral(Loc);
  if (StringRef("true").equals_lower(Name))
    return ActOnCXXBoolLiteral(Loc, tok::kw_true);
  if (StringRef("false").equals_lower(Name))
    return ActOnCXXBoolLiteral(Loc, tok::kw_false);
  return ExprEmpty();
}


/// \brief Creates a constant declaration in the specified context.
///
/// \param DC Declaration context that the constant must go to.
/// \param Name The constant name.
/// \param Init The constant value. May be 'nullptr' in this case the created
/// constant declaration is a forward declaration.
/// \returns New constant declaration.
///
VarDecl *Sema::createConstant(DeclContext *DC,
                              const DeclarationNameInfo &Name, Expr *Init) {
  // TODO: check if Init is compile time constant. If not, create runtime constant.
  VarDecl *ConstD = getRuntime().createConstant(DC, Name, Init);

  // Set up constant value. Depending on the initializing expression, it may be
  // a value itself or a function that will set the value at first read.
//   if (Init)
//     ConstD->setInit(Init);

  addUnitDeclaration(ConstD);
  return ConstD;
}


/// \brief Semantic action called by parser when it recognizes a constant
/// definition.
///
VarDecl *Sema::ActOnConstantDefinition(DeclarationNameInfo Name,
                                       SourceLocation EqLoc, Expr *Init) {
  assert(Init != nullptr);

  // Catch invalid redefinition.
  if (isBuiltinConstant(Name.getAsString())) {
    Diag(Name.getLoc(), diag::err_php_builtin_const_redefinition);
    return nullptr;
  }

  // Determine declaration scope where the constant must go to. Until the end of
  // this scope constant is considered defined and may be replaced by
  // corresponding value. For instance:
  //
  // @code
  //     echo ABCD;               // ABCD is undefined, access is implemented
  //                              // by runtime
  //     if ($x < 0) {
  //       define("ABCD", 222);
  //       $y = ABCD;             // ABCD is defined, may be replaced by value
  //     }
  //     $z = ABCD;               // ABCD is undefined, access is implemented
  //                              // by runtime
  // @endcode
  //
  // In this example scope of ABCD is 'then' branch of 'if' statement.
  Scope *DeclS = getCurScope();
  while (DeclS && (DeclS->getFlags() & Scope::DeclScope) == 0)
    DeclS = DeclS->getParent();

  // Find scope corresponding to the declaration context that should contain the
  // constant declaration. It is either a file or class scope.
  Scope *S = DeclS;
  while (S) {
    if (S && S->getEntity() &&
        (S->getEntity()->isRecord() || S->getEntity()->isFileContext()))
      break;
    S = S->getParent();
  }
  assert(S->getEntity());

  // Determine declaration context where the constant must be placed. We take it
  // from current namespace or class.
  if (S->getEntity()->isFileContext()) {
    DeclContext *DC = S->getEntity();
    assert(DC == CurNamespace);
  } else {
    //TODO: handle case of class constant.
  }

  // Check for constant redefinition and find previous declaration if it exists.
  VarDecl *Previous = findConstant(S, Name);
  if (Previous && Previous->hasDefinition()) {
    if (isInNamespace(Previous, CurNamespace)) {
      Diag(Name.getLoc(), diag::err_php_constant_redefinition);
      Diag(Previous->getLocation(), clang::diag::note_previous_definition);
      return nullptr;
    }
  }

  NamespaceDecl *NS = getConstNamespace();
  VarDecl *CD = createConstant(NS, Name, Init);

  // Add the constant declaration to all necessary containers.
  DeclContext *SavedDC = CurContext;
  CurContext = NS;
  NS->addDecl(CD);
  CurContext = SavedDC;
  S->AddDecl(CD);
  IdResolver.AddDecl(CD);

  // If we have compile time constant in emodule, do not return just created
  // declaration, as it is used for initialization code.
  if ((getPhpOptions().Kind == PhpModuleKind::EModule) && Init &&
      isLiteralExpr(Init))
    return nullptr;

  return CD;
}


/// \brief Semantic action called by parser when it has recognizes operator 
/// 'const'.
///
StmtResult Sema::ActOnConstantDefStatement(SourceLocation ConstLoc,
                         MutableArrayRef<Decl *> Decls, SourceLocation EndLoc) {
  DeclGroupRef DRef;
  if (Decls.size() > 1)
    DRef = DeclGroupRef(DeclGroup::Create(Context, Decls.data(), Decls.size()));
  else
    DRef = DeclGroupRef(Decls.front());
  DeclStmt *DS = new (Context) DeclStmt(DRef, ConstLoc, EndLoc);
  return DS;
}


/// \brief Semantic action called by parser when it recognizes constant use.
///
/// \param SS Namespace or class specifier.
/// \param Name Identifier that represents the constant name.
/// \param Loc Location of the constant name.
/// \returns expression representing value of the specified constant.
///
ExprResult Sema::ActOnConstantUse(CXXScopeSpec SS, IdentifierInfo *Name,
                                  SourceLocation Loc) {
  // Check for builtin constants, like 'true', 'null'. They are available in all
  // namespaces.
  if (isBuiltinConstant(Name->getName())) {
    if (!SS.isEmpty()) {
      Diag(Loc, diag::err_php_builtin_const_redefinition);
      return ExprError();
    }
    return getBuiltinConstant(Name->getName(), Loc);
  }

  // If scope is specified, find corresponding namespace and class contexts.
  NamespaceDecl *NSScope = nullptr;
  CXXRecordDecl *ClassScope = nullptr;

  if (!SS.isEmpty()) {
    NestedNameSpecifier *NNS = SS.getScopeRep();
    if (NNS->getKind() == NestedNameSpecifier::TypeSpec) {
      ClassScope = NNS->getAsType()->getAsCXXRecordDecl();
      NNS = NNS->getPrefix();
    }
    if (!NNS)
      ;
    else if (NamespaceDecl *NS = NNS->getAsNamespace())
      NSScope = NS;
    else if (NNS->getKind() == NestedNameSpecifier::Global)
      NSScope = PhpNamespace;
  }

  // Process class constant use.
  if (ClassScope) {
    //TODO:
    llvm_unreachable("not implemented");
    return ExprError();
  }

  // Process namespace scope constants.
  DeclarationNameInfo DNInfo(DeclarationName(Name), Loc);
  VarDecl *VD = nullptr;

  // If the constant name is specified without scope specifier, it should be
  // searched in the current namespace, if not found, then in the global.
  if (!NSScope)
    VD = findConstant(getCurScope(), DNInfo);
  else
    VD = findConstant(SS, DNInfo);
  if (!VD) {
    // Constant is not found, either because it is defined latter or because
    // it is undefined. Treat the constant as runtime one.
    //
    // If the name is qualified, the constant must be found in exactly the
    // specified namespace. So create forward declaration for the constant.
    if (NSScope) {
      VarDecl *FwdDecl = createForwardConstantDecl(DNInfo, NSScope);
      return BuildDeclarationNameExpr(SS, DNInfo, FwdDecl);
    }

    // If constant name is unqualified, constant may be found in the current
    // namespace as well as in global one. Even when the global namespace
    // contains constant with the same name, we cannot use it, because there may
    // be runtime constant defined somewhere and it can hide the global
    // constant. So put runtime query here.
    return createRuntimeConstantUse(DNInfo, NSScope);
  }

  // If the constant resolves to compile-time definition, use that definition as
  // the expression value.
  if (Expr *Def = VD->getInit()) {
    if (isLiteralExpr(Def))
      return Def;
  }

  // Declaration of the constant has been seen but its value is not known. This
  // is possible when the constant is defined in C++ sources and only its
  // declaration has been seen. So just build reference to the constant value.
  return BuildDeclarationNameExpr(SS, DNInfo, VD);
}


/// \brief Creates constant declaration when constant definition is not known to
/// the compiler.
///
VarDecl *Sema::createForwardConstantDecl(DeclarationNameInfo Name,
                                         NamespaceDecl *NS) {
  NS = getConstNamespace(NS);
  VarDecl *CD = createConstant(NS, Name, nullptr);
  DeclContext *SavedDC = CurContext;
  CurContext = NS;
  NS->addDecl(CD);
  CurContext = SavedDC;
  return CD;
}


/// \brief Creates reference to runtime constant. This function works only with
/// constants specified by unqualified names.
///
/// \param Name The constant name.
/// \param NS Namespace in which the constant is referenced, nullptr if it is
///           global namespace.
/// \returns Expression that implements constant value access.
///
ExprResult Sema::createRuntimeConstantUse(DeclarationNameInfo Name,
                                          NamespaceDecl *CurNS) {
  std::string ConstName = Name.getAsString();
  if (CurNS) {
    std::string NsName;
    NsName = getNamespaceAsString(CurNS);
    return getRuntime().generateRtConstantUse(ConstName, Name.getLoc(), NsName);
  } else {
    // If a constant is referenced from global namespace, it can be associated
    // with particular value object.
    VarDecl *VD = createConstant(getConstNamespace(), Name, nullptr);
    ExprResult Base = BuildDeclarationNameExpr(CXXScopeSpec(), Name, VD);
    if (!Base.isUsable())
      return ExprError();
    return getRuntime().generateGlobalRtConstantUse(VD);
  }
}

}
