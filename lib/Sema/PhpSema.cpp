//===--- PhpSema.cpp - AST Builder and Semantic Analysis Implementation ---===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// This file implements the actions class which performs semantic analysis and
// builds an AST out of a parse stream.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Lex/LiteralSupport.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/Sema/SemaDiagnostic.h"
#include "phpfe/Analyzer/Evaluator.h"
#include "phpfe/Analyzer/Decorator.h"
#include "phpfe/Builtin/BuiltinFunc.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "phpfe/Support/Path.h"
#include "phpfe/Support/Utilities.h"
#include "shared/constants.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


Sema::Sema(Preprocessor &pp, ASTContext &ctxt, ASTConsumer &consumer,
           PhpOptions &POpts, TranslationUnitKind TUKind,
           CodeCompleteConsumer *CodeCompleter)
  : clang::Sema(pp, ctxt, consumer, TUKind, CodeCompleter),
    UnitFunction(nullptr), UnitRecord(nullptr) {
  IsPhpSema = true;
  StrictTypes = false;
  PhpOpts.reset(new PhpOptions(POpts));
  llvm::SmallString<256> AbsolutePath(PhpOpts->AppDir);
  llvm::sys::fs::make_absolute(AbsolutePath);
  PhpOpts->AppDir = normalizePath(AbsolutePath);
}


Sema::~Sema() {
}


void Sema::Initialize() {
  clang::Sema::Initialize();
  loadRuntimeTypes();
  RT.reset(new Runtime(*this));
  RT->initialize();
  Constants.init();

  // initialize php namespaces
  PhpNamespace = createNamespace("php");
  VarNamespace = createNamespace(VarNamespaceName, PhpNamespace);
  CurNamespace = PhpNamespace;
  FuncNamespace = nullptr;
  ConstNamespace = nullptr;
  ClassNamespace = nullptr;
}


Sema::DeclGroupPtrTy Sema::getUnitDeclarations() {
  auto Result = DeclGroupPtrTy::make(DeclGroupRef::Create(Context,
      UnitDeclarations.data(), UnitDeclarations.size()));
  UnitDeclarations.clear();
  return Result;
}


void Sema::handlePostponedDeclarations() {
  if (UnitDeclarations.empty())
    return;
  getASTConsumer().HandleTopLevelDecl(getUnitDeclarations().get());
}


FunctionDecl *Sema::createUnitFunction(StringRef FileName, SourceLocation Loc) {
  // Make unit function name. It must be unique within application, so build it
  // from normalized path of the current file.
  const SourceManager& SM = getSourceManager();
  FileID Main = SM.getMainFileID();
  const FileEntry *MainEntry = SM.getFileEntryForID(Main);
  const char *MainFileName = MainEntry->getName();
  std::string unit_path = makeRelativePath(MainFileName, PhpOpts->AppDir);
  std::string unit_name = makeNameFromPath(unit_path);

  DeclarationName DN(PP.getIdentifierInfo(unit_name));
  DeclContext *DC = Context.getTranslationUnitDecl();
  FunctionDecl *Unit = FunctionDecl::Create(Context, DC, Loc, Loc, DN,
      getUnitExecuteHandlerType(),
      getUnitExecuteHandlerInfo(), SC_None);
  DC->addDecl(Unit);

  // Add function parameters.
  SmallVector<ParmVarDecl*, 1> Params;
  ParmVarDecl *ModulePtr = ParmVarDecl::Create(Context, Unit, Loc, Loc,
      PP.getIdentifierInfo("__State"),
      getUnitStatePtrType(),
      getUnitStatePtrInfo(),
      SC_None,
      nullptr);
  Params.push_back(ModulePtr);
  Unit->setParams(Params);

  UnitDeclarations.push_back(Unit);

  return Unit;
}

PresumedLoc Sema::getPresumedLocForSourceLoc(SourceLocation Loc) {
  return getSourceManager().getPresumedLoc(Loc, false);
}

void Sema::setupUnitFunction(SourceLocation Loc) {
  assert(!UnitFunction && "Unit function is already created");
  assert(!UnitRecord && "Unit record without unit function");
  assert(Loc.isValid());

  PresumedLoc FileLoc = getPresumedLocForSourceLoc(Loc);
  SmallString<128> FileName(FileLoc.getFilename());
  
  //TODO: check file extension?

  std::transform(FileName.begin(), FileName.end(), FileName.begin(), 
    [](char c) { return isAlphanumeric(c) ? c : '_'; });

  // Create unit function and prepare function scope which will be created 
  // irrespective to current context (either translation unit or namespace).
  UnitFunction = createUnitFunction(FileName, Loc);
  PushFunctionScope();

  // setup unit record for unit function
  UnitRecord = RT->createUnitRecord(UnitFunction);

  RT->addUnitName(UnitRecord, FileLoc.getFilename());
  RT->addUnitExecuteHandler(UnitRecord, UnitFunction);

  int Flags;
  switch (PhpOpts->Kind) {
  case PhpModuleKind::Module:  Flags = UnitFlag::Static; break;
  case PhpModuleKind::EModule: Flags = UnitFlag::Module; break;
  case PhpModuleKind::Page:    Flags = UnitFlag::Page;   break;
  case PhpModuleKind::Script:  Flags = UnitFlag::Script; break;
  case PhpModuleKind::Config:  Flags = UnitFlag::Config; break;
  }
  RT->addUnitFlags(UnitRecord, Flags);
  UnitDeclarations.push_back(UnitRecord);

  // setup unit state for unit record
  UnitStateVar = RT->createUnitState(Loc, FileName);
  RT->addUnitStateUnit(UnitStateVar, UnitRecord);
  UnitDeclarations.push_back(UnitStateVar);
}




bool Sema::ActOnUnitStart(SourceLocation StartLoc) {
  if (UnitFunction)
    // Sema is already initialized. Most likely this is the second call of
    // parsePhpUnit.
    return false;

  UnitStatements.clear();
  UnitDeclarations.clear();
  UnitStartLoc = StartLoc;

  // Current scope must be a scope of unit function.
  Scope *UnitScope = getCurScope();
  assert(UnitScope->getFlags() & Scope::FnScope);
  assert(UnitScope->getEntity() == nullptr);

  // Previous scope must be a scope of internal PHP namespace.
  GlobalNamespaceScope = UnitScope->getParent();
  assert(GlobalNamespaceScope->getFlags() == Scope::DeclScope);
  assert(UnitScope->getEntity() == nullptr);
  PushDeclContext(GlobalNamespaceScope, getPhpNamespace());

  setupUnitFunction(StartLoc);
  assert(getUnitFunction());
  UnitScope->setEntity(getUnitFunction());
  CurContext = getUnitFunction();

  return true;
}


Decl *Sema::ActOnUnitFinish(SourceLocation EndLoc) {
  assert(UnitFunction && "No unit function");

  Stmt *Ret = BuildDefaultReturnStmt(getClassBoxType(), EndLoc);
  UnitStatements.push_back(Ret);

  CompoundStmt *Body = new(Context) CompoundStmt(Context,
                           UnitStatements, UnitFunction->getLocation(), EndLoc);
  UnitFunction->setBody(Body);

  RT->doneUnitRecord(UnitRecord);
  RT->doneUnitStateRecord(UnitStateVar);

  return UnitFunction;
}


ParmVarDecl *Sema::ActOnFunctionParam(Scope *S, SourceLocation Loc,
        SourceLocation EllipsisLoc, QualType TypeHint, IdentifierInfo *Name) {
  assert(S->getFlags() & Scope::FunctionPrototypeScope);

  if (TypeHint->isVoidType())
    Diag(Loc, diag::err_php_void_parameter);

  VarDecl *PrevDecl = findVariable(S, DeclarationName(Name));
  if (PrevDecl && isa<ParmVarDecl>(PrevDecl)) {
    Diag(Loc, diag::err_php_parameter_redefinition);
    Diag(PrevDecl->getLocation(), diag::note_previous_declaration);
    return nullptr;
  }

  if (TypeHint.isNull())
    TypeHint = getClassBoxType();

  if (EllipsisLoc.isValid()) {
    if (TypeHint->isReferenceType()) {
      auto BaseType = TypeHint.getTypePtr()->getAs<ReferenceType>();
      TypeHint = Context.getPointerType(BaseType->getPointeeType());
    }
    TypeHint = getArgPackType(Loc, TypeHint);
  }
  
  Scope *DeclScope = S;
  while (!DeclScope->getEntity())
    DeclScope = DeclScope->getParent();

  TypeSourceInfo *TInfo = getTypeSourceInfo(TypeHint);
  DeclContext *DC = DeclScope->getEntity();
  ParmVarDecl *Parm = ParmVarDecl::Create(Context, DC, Loc, Loc, Name, TypeHint,
                                          TInfo, SC_None, nullptr);
  PushOnScopeChains(Parm, S, false);
  return Parm;
}


bool Sema::ActOnParamDefaultValue(SourceLocation, ParmVarDecl *Param, Expr *E) {
  assert(Param && "Parameter is not specified");
  assert(E && "Default value is not specified");

  //TODO: check correctness

  Param->setDefaultArg(E);
  return false;
}


/// \brief Create new function declaration.
///
/// The action is called when function prototype has already been processed but
/// before body parsing started.
///
FunctionDecl *Sema::ActOnFunctionDeclaration(Scope *S,
                                             FunctionParseInfo &FInfo) {
  // Anonymous functions are processed elsewhere. They are not declarations from
  // viewpoint of syntax.
  assert(!FInfo.Name.getName().isEmpty());

  // Check function redefinition.
  FunctionDecl *Prev = findFunction(S, FInfo.Name.getName());
  if (Prev && Prev->hasBody()) {
    Diag(FInfo.FnLoc, diag::err_php_function_redeclaration);
    Diag(Prev->getLocation(), diag::note_previous_definition);
    return nullptr;
  }

  // Check the scope if which function declaration appears. Functions declared
  // in file scopes can be made global, all others are always local.

  // If the current scope is not associated with any declaration context, this
  // is a scope of a statement (if, switch, etc). Function defined in such scope
  // is always local.
  bool AlwaysLocal = S->getEntity() == nullptr;
  if (!AlwaysLocal) {
    if (S->isFunctionScope()) {
      // A function declared in function scope is local, unless this is unit
      // function scope.
      if (S->getEntity() != UnitFunction) {
        AlwaysLocal = true;
      } else {
        // Move to the parent scope, which must be file-level scope.
        S = S->getParent();
        assert(S->getEntity() && S->getEntity()->isFileContext());
      }
    } else if (S->isClassScope()) {
      // This is a class method.
      //TODO: process
    } else if (S->getEntity()->isFileContext()) {
      // Functions defined in namespace or in translation unit can be global or
      // local depending on compilation options.
      if (getPhpOptions().Kind != PhpModuleKind::EModule)
        AlwaysLocal = true;
    } else {
      llvm_unreachable("wrong scope");
    }
  }

  // Get the declaration context in which the function will reside.
  DeclContext *DC = getFuncNamespace();
  assert(DC);

  // Determine the function return value.
  if (FInfo.ReturnTypeHint.isNull())
    FInfo.ReturnTypeHint = getClassBoxType();

  // Get types of all parameters.
  SmallVector<QualType, 8> ParamTypes;
  for (ParmVarDecl *Param : FInfo.Params) {
    QualType Ty = Param->getType();
    assert(!Ty.isNull());
    ParamTypes.push_back(Ty);
  }

  // Build function type.
  // TODO: handle methods.
  QualType FnType = BuildFunctionType(FInfo.ReturnTypeHint, ParamTypes);
  TypeSourceInfo *TInfo = getTypeSourceInfo(FnType);

  // Storage class is static unless the function may be global and compiler
  // options allow it.
  StorageClass SC = SC_Static;
  if (!AlwaysLocal && PhpOpts->Kind == PhpModuleKind::EModule)
    SC = SC_None;

  // Create declaration.
  FunctionDecl *FD = FunctionDecl::Create(Context, DC, FInfo.FnLoc, FInfo.Name,
                                          FnType, TInfo, SC, false, true);

  // Parameters were created in translation unit. Now we can move them into
  // function.
  for (auto Param : FInfo.Params) {
    Param->setDeclContext(FD);
    if (Param->isInvalidDecl())
      Param->setOwningFunction(FD);
  }
  FD->setParams(FInfo.Params);
  FD->setRangeEnd(FInfo.RParen);

  // Add this function to owning context.
  if (!FD->isInvalidDecl()) {
    DeclContext *SaveDC = CurContext;
    CurContext = DC;
    DC->addDecl(FD);
    CurContext = SaveDC;
  }
  if (Prev)
    FD->setPreviousDeclaration(Prev);
  PushOnScopeChains(FD, S, false); // For unit function S points to parent.

  return FD;
}


void Sema::ActOnFunctionDefinitionStart(FunctionDecl *FD) {
  assert(FD && "Invalid function");

  // Current scope must be already prepared for the new function, but
  // declaration context still refers to the scope in which the function
  // declaration occurs.
  assert(getCurScope()->isFunctionScope());
  assert(getCurScope()->getFlags() & Scope::DeclScope);
  assert(getCurScope()->getEntity() == nullptr);

  PushFunctionScope();

  CurContext = FD->getDeclContext();
  PushDeclContext(getCurScope(), FD);
  for (auto Param : FD->parameters()) {
    PushOnScopeChains(Param, getCurScope());
  }
}


void Sema::ActOnFunctionDefinitionEnd() {
  // Current scope must be the scope of the finished function.
  assert(getCurScope()->isFunctionScope());
  assert(getCurScope()->getFlags() & Scope::DeclScope);
  FunctionDecl *FD = cast<FunctionDecl>(getCurScope()->getEntity());
  assert(FD);
  assert(getCurScope()->getEntity() == FD);

  assert(FD == CurContext && "Corrupted context");

  // Restore context, - find closest scope that contains declaration context.
  Scope *S = getCurScope()->getParent();
  while (S && !S->getEntity())
    S = S->getParent();
  CurContext = S->getEntity();
  assert(CurContext);
  assert(CurContext->isFileContext() || CurContext->isFunctionOrMethod() ||
         CurContext->isRecord());
  PopFunctionScopeInfo(nullptr, FD);
}


// Attaches function body to the function declaration.
//
StmtResult Sema::ActOnFunctionDefinition(FunctionDecl *FDecl, Stmt *Body) {
  assert(FDecl && "Function declaration is not specified");
  assert(!FDecl->hasBody() && "Function already has body");

  SmallVector<Stmt*, 32> BodyStmts;

  // insert local variable declarations
  for (auto *D : FDecl->decls()) {
    if (VarDecl *VD = dyn_cast<VarDecl>(D))
      if (!isa<ParmVarDecl>(D)) {
        SmallVector<Decl*, 1> Decls;
        Decls.push_back(D);
        DeclGroupPtrTy DG = BuildDeclaratorGroup(Decls, false);
        StmtResult VarDeclStmt =
          ActOnDeclStmt(DG, VD->getLocStart(), VD->getLocEnd());
        BodyStmts.push_back(VarDeclStmt.get());
      }
  }

  CompoundStmt *BodyCS = cast<CompoundStmt>(Body);
  BodyStmts.insert(BodyStmts.end(), BodyCS->body_begin(), BodyCS->body_end());

  // insert return stmt if necessary
  if (Stmt *Ret = BuildDefaultReturnStmt(FDecl->getReturnType(),
                                         BodyCS->getRBracLoc())) {
    BodyStmts.push_back(Ret);
  } else {
    // if the last stmt in the body is return then we are happy
    if (!BodyCS->size() || !isa<ReturnStmt>(BodyCS->body_back())) {
      //TODO: not really true, can we do better?
      Diag(FDecl->getLocation(), diag::err_php_no_return_in_function);
      return StmtError();
    }
  }
  BodyCS->setStmts(Context, BodyStmts.data(), BodyStmts.size());
  FDecl->setBody(Body);
  addUnitDeclaration(FDecl);

  //TODO: return PhpFunctionDefStmt
  return StmtEmpty();
}


CXXRecordDecl *Sema::ActOnClosureDefinitionStart(SourceLocation Loc) {
  //TODO: implement closure handling
  return nullptr;
}


FunctionDecl *Sema::ActOnClosureDeclaration(FunctionParseInfo &FInfo) {
  //TODO: implement closure handling
  static unsigned ClosureId = 0;
  DeclarationName DN(PP.getIdentifierInfo("lambda_" + std::to_string(++ClosureId)));
  FInfo.Name = DeclarationNameInfo(DN, FInfo.FnLoc);
  return ActOnFunctionDeclaration(getCurScope(), FInfo);
}


ExprResult Sema::ActOnClosureDefinitionEnd() {
  //TODO: implement closure handling
  return ExprError();
}


// returns true on error
bool Sema::ActOnClosureVariableUse(SourceLocation RefLoc,
                                   SourceLocation NameLoc, IdentifierInfo *II) {
  //TODO: implement closure variable use
  return false;
}



void Sema::addUnitDeclaration(Decl *D) {
  assert(std::find(UnitDeclarations.begin(), UnitDeclarations.end(), D)
         == UnitDeclarations.end());
  UnitDeclarations.push_back(D);
}


StmtResult Sema::ActOnHtmlData(SourceLocation StartLoc, StringRef Text) {
  StringLiteral *Str = BuildStringLiteral(Text, StartLoc);
  SmallVector<Expr*, 1> Data;
  Data.push_back(Str);
  return PhpEchoStmt::Create(Context, StartLoc, Data, true);
}

bool Sema::isAssignmentOperator(tok::TokenKind Kind) {
  switch (Kind) {
  case tok::equal:
  case tok::starequal:
  case tok::slashequal:
  case tok::percentequal:
  case tok::plusequal:
  case tok::minusequal:
  case tok::lesslessequal:
  case tok::greatergreaterequal:
  case tok::ampequal:
  case tok::caretequal:
  case tok::pipeequal:
  case tok::periodequal:
  case tok::star_star_equal:
    return true;
  default:
    return false;
  }
}


ExprResult Sema::ActOnNumericLiteral(SourceLocation Loc, StringRef Spelling,
                                     bool Negative) {
  NumericLiteralParser Literal(Spelling, Loc, PP);
  if (Literal.hadError)
    return ExprError();

  if (Literal.isIntegerLiteral()) {
    APInt IntVal(getIntValueWidth(), 0);
    if (!Literal.GetIntegerValue(IntVal)) {
      if (Negative) {
        if (IntVal.ule(APInt::getSignedMinValue(getIntValueWidth())))
          return IntegerLiteral::Create(Context, -IntVal,
                                        getIntValueType(), Loc);
      } else {
        if (IntVal.ule(APInt::getSignedMaxValue(getIntValueWidth())))
          return IntegerLiteral::Create(Context, IntVal,
                                        getIntValueType(), Loc);
      }
    }

    // Integer overflow occurred.
    reportTooLargeInteger(Loc, Negative);
    unsigned bitwidth = getIntValueWidth();
    while (bitwidth <= 2048) {
      APInt LongVal(bitwidth, 0);
      if (!Literal.GetIntegerValue(LongVal)) {
        // Literal is successfully converted to integer value. Need to convert
        // it to 64 bit value.
        unsigned extra_bits = 0;
        if (LongVal.getActiveBits() >= 64) {
          extra_bits = LongVal.getActiveBits() - 64 + 1;
          LongVal = LongVal.lshr(extra_bits);
        }
        LongVal = LongVal.zextOrTrunc(64);
        APFloat FVal(APFloat::IEEEdouble, LongVal.getLimitedValue());
        APFloat Two(APFloat::IEEEdouble, 2);
        for (; extra_bits; --extra_bits)
          FVal = FVal * Two;
        if (Negative)
          FVal.changeSign();
        return FloatingLiteral::Create(Context, FVal, false,
                                       getDoubleType(), Loc);
      }
      bitwidth *= 2;
    }

    // Huge integer that cannot fit double.
    reportTooLargeFloat(Loc);
    APFloat Inf(APFloat::IEEEdouble);
    Inf.getInf(APFloat::IEEEdouble);
    if (Negative)
      Inf.changeSign();
    return FloatingLiteral::Create(Context, Inf, false, getDoubleType(), Loc);
  }

  APFloat Val(APFloat::IEEEdouble);
  auto Status = Literal.GetFloatValue(Val);

  if ((Status & APFloat::opOverflow) || Val.isInfinity()) {
    reportTooLargeFloat(Loc);
  } else if (Status & APFloat::opUnderflow) {
    SmallString<40> buffer;
    APFloat::getSmallest(APFloat::IEEEdouble).toString(buffer);
    Diag(Loc, diag::warn_float_underflow) << Context.DoubleTy << buffer;
  }

  bool Exact = (Status == APFloat::opOK);
  if (Negative)
    Val.changeSign();
  return FloatingLiteral::Create(Context, Val, Exact, getDoubleType(), Loc);
}


ExprResult Sema::ActOnIntegerConstant(SourceLocation Loc, uint64_t Val) {
  return IntegerLiteral::Create(Context,
    llvm::APInt(getIntValueWidth(), Val),
    getIntValueType(), Loc);
}


ExprResult Sema::ActOnPHPArrayExpr(SourceLocation KwLoc, SourceLocation LBLoc,
                               ArrayRef<Expr*> Elements, SourceLocation RBLoc) {
  Expr *Array = PhpArrayExpr::Create(Context, getArrayPtrType(), KwLoc,
                                     LBLoc, Elements, RBLoc);
  return Array;
}


StmtResult Sema::ActOnEchoStmt(SourceLocation Loc, ArrayRef<Expr*> Exprs) {
  return PhpEchoStmt::Create(getASTContext(), Loc, Exprs);
}


StmtResult Sema::ActOnCompoundStmt(SourceLocation L, SourceLocation R,
                                   ArrayRef<Stmt*> Elts, bool isStmtExpr) {
  return new (Context) CompoundStmt(Context, Elts, L, R);
}


StmtResult Sema::ActOnReturnStmt(SourceLocation Loc, Expr *RetVal) {
  QualType RetTy = getCurFunctionDecl()->getReturnType();
  if (RetVal) {
    if (RetTy->isVoidType()) {
      Diag(Loc, diag::err_php_return_in_void_function);
      return StmtError();
    }
    if (checkTypeVsDeclaration(RetVal, RetTy, true))
      return StmtError();
  } else if (isUniversalType(RetTy)) {
    RetVal = ActOnPHPNullLiteral(Loc).get();
  } else if (!RetTy->isVoidType()) {
    Diag(Loc, diag::err_php_expected_return_value);
    return StmtError();
  }
  return new(Context) ReturnStmt(Loc, RetVal, nullptr);
}


StmtResult Sema::ActOnStaticAssert(SourceLocation Loc,
                                   Expr *Cond, StringLiteral *Message,
                                   SourceLocation RPLoc) {
//TODO: enable Evaluator in sema
//   Evaluator Ev(*this);
//   if (auto *V = Ev.evaluate(E)) {
//     ConstValue::ConversionStatus Status;
//     if (!V->getAsBool(Status)) {
//       if (!Message.empty())
//          Diag(E->getExprLoc(), diag::err_php_static_assert) << 1 << Message;
//       else
//         Diag(E->getExprLoc(), diag::err_php_static_assert) << 0;
//       return StmtError();
//     }
//   } else if (!Ev.seenError()) {
//     Expr *Offending = Ev.getOffendingExpr();
//     assert(Offending);
//     Diag(Offending->getExprLoc(), diag::err_php_static_assert_not_const)
//         << Offending->getSourceRange();
//     return StmtError();
//
//TODO:-------------------------
  if (!isUniversalType(Cond->getType())) {
    if (!Cond->getType()->isBooleanType()) {
      Diag(Cond->getExprLoc(), diag::err_php_static_assert_bool)
        << Cond->getSourceRange();
      return StmtError();
    }
  }

  Decl *SADecl = StaticAssertDecl::Create(Context, CurContext, Loc,
                                          Cond, Message, RPLoc, true);
  addUnitDeclaration(SADecl);
  DeclStmt *Stmt = new (Context) DeclStmt(DeclGroupRef(SADecl), Loc, RPLoc);
  return Stmt;
}



ExprResult Sema::ActOnCallArgument(SourceLocation EllipsisLoc, Expr *Arg) {
  if (EllipsisLoc.isValid())
    return new(Context)PhpEllipsisExpandExpr(QualType(), EllipsisLoc, Arg);
  // TODO:
  return ExprError();
}


ExprResult Sema::ActOnPHPClassConstant(CXXScopeSpec SS, SourceLocation LBLoc, 
                                       Expr *Name, SourceLocation RBLoc) {
  assert(Name && "Expected a valid class constant name expression");
  assert(SS.isValid() && "Expected a valid scope specifier");
  // TODO: implement
  return ExprError();
}


ExprResult Sema::ActOnCallExpression(Expr *Callee, SourceLocation LB,
                                     ArrayRef<Expr *> Arguments,
                                     SourceLocation RB) {
  // TODO:
  return ExprError();
}


ExprResult Sema::ActOnCallExpression(CXXScopeSpec SS,
                                     SourceLocation Loc,
                                     DeclarationName Name,
                                     SourceLocation LPLoc,
                                     MutableArrayRef<Expr*> Args,
                                     SourceLocation RPLoc) {
  if (SS.isEmpty()) {
    ExprResult BuiltinCall = processBuiltinCall(
        Name.getAsIdentifierInfo()->getName(), Loc, LPLoc, Args, RPLoc);
    if (!BuiltinCall.isUnset())
      return BuiltinCall;
  }

  FunctionDecl *FD = nullptr;
  if (SS.isEmpty())
    FD = findFunction(getCurScope(), Name);
  else
    FD = findFunction(SS, Name);

  if (!FD) {
    FD = BuildFunctionForwardDeclaration(SS, Loc, Name);
    assert(FD && !FD->isInvalidDecl());
    DeclContext *SaveDC = CurContext;
    CurContext = FD->getDeclContext();
    CurContext->addDecl(FD);
    CurContext = SaveDC;
    //TODO: save CallExpr for further check, when declaration becomes available.
  } else {
    // It is known function.
    if (FD->hasWrittenPrototype() && checkFunctionCallArguments(Loc, FD, Args))
      return ExprError();
  }

  Expr *FnRef = BuildDeclRefExpr(FD, FD->getType(), VK_RValue, Loc).get();
  ExprResult FnCast = CallExprUnaryConversions(FnRef);
  if (FnCast.isInvalid())
    return ExprError();

  return new (Context) CallExpr(Context, FnCast.get(), Args,
                                FD->getReturnType(), VK_RValue, RPLoc);
}


ExprResult Sema::ActOnSubscriptExpression(Expr *Base, SourceLocation Left,
                                          Expr *Index, SourceLocation Right,
                                          bool LHS) {
  // Type of 'Base' must allow subscript access. These are arrays, strings
  // and objects.
  QualType BaseType = Base->getType();
  if (BaseType->isFundamentalType()) {
    Diag(Left, diag::err_php_bad_subscript_base) << Base->getType();
    return ExprError();
  }

  // Determine type of result.
  QualType ResultType;
  if (const ConstantArrayType *BType = dyn_cast<ConstantArrayType>(BaseType))
    ResultType = BType->getElementType();
  else if (isStringType(BaseType))
    ResultType = getStringRefType();
  else
    ResultType = getClassBoxType();

  ExprValueKind VK = LHS ? VK_LValue : VK_RValue;
  return new (Context) ArraySubscriptExpr(Base, Index, ResultType, VK,
                                          OK_Ordinary, Right);
}


void Sema::ActOnTopStatement(StmtResult S) {
  if (S.isUsable())
    UnitStatements.push_back(S.get());
}


ExprResult Sema::ActOnConditionalOp(Expr *CondExpr, SourceLocation QueryLoc,
                                    Expr *TrueExpr, SourceLocation ColonLoc,
                                    Expr *FalseExpr) {
  OpaqueValueExpr *opaqueValue = nullptr;
  Expr *commonExpr = nullptr;
  if (TrueExpr == nullptr) {
    commonExpr = CondExpr;
    opaqueValue = new (Context) OpaqueValueExpr(commonExpr->getExprLoc(),
                                                commonExpr->getType(),
                                                commonExpr->getValueKind(),
                                                commonExpr->getObjectKind(),
                                                commonExpr);
   TrueExpr = CondExpr = opaqueValue;
  }

  if (!commonExpr)
    return new (Context) ConditionalOperator(CondExpr, QueryLoc, TrueExpr,
                ColonLoc, FalseExpr, getClassBoxType(), VK_RValue, OK_Ordinary);

  return new (Context) BinaryConditionalOperator(commonExpr, opaqueValue,
    CondExpr, TrueExpr, FalseExpr, QueryLoc, ColonLoc, getClassBoxType(),
    VK_RValue, OK_Ordinary);
}


StmtResult Sema::ActOnPHPIfStmt(SourceLocation IfLoc, Expr *Cond, Stmt *Then) {
  assert(Cond && Then);

  if (!isa<CompoundStmt>(Then[0])) {
    SmallVector<Stmt*, 1> Stmts;
    Stmts.push_back(Then);
    Then = new (Context)CompoundStmt(Context, Stmts, 
      Then->getLocStart(), Then->getLocEnd());
  }

  return new (Context)IfStmt(Context, IfLoc, nullptr, Cond, Then);
}


StmtResult Sema::ActOnPhpWhileStmt(SourceLocation WhileLoc,
         SourceLocation LParen, Expr* Cond, SourceLocation RParen, Stmt* Body) {
  assert(Cond);
  assert(Body);
  return new (Context) WhileStmt(Context, nullptr, Cond, Body, WhileLoc);
}


StmtResult Sema::ActOnAlternativePhpWhileStmt(SourceLocation WhileLoc,
                      SourceLocation LParen, Expr * Cond, SourceLocation RParen,
                      SourceLocation ColonLoc, ArrayRef<Stmt*> WhileStmts,
                      SourceLocation EndWhile) {
  assert(Cond);
  assert(!WhileStmts.empty());
  Stmt *Body =  new (Context) CompoundStmt(Context, WhileStmts, ColonLoc, EndWhile);
  return new (Context) WhileStmt(Context, nullptr, Cond, Body, WhileLoc);
}


StmtResult Sema::ActOnPHPElseStmt(Stmt *Node, 
                                  SourceLocation ElseLoc, Stmt *Else) {
  if (Else && !isa<CompoundStmt>(Else)) {
    SmallVector<Stmt*, 1> Stmts;
    Stmts.push_back(Else);
    Else = new(Context)CompoundStmt(Context, Stmts, Else->getLocStart(),
      Else->getLocEnd());
  }
  IfStmt *If = cast<IfStmt>(Node);
  If->setElseLoc(ElseLoc);
  If->setElse(Else);
  return If;
}

ExprResult Sema::ActOnPHPIncludeExpr(SourceLocation Loc, 
                               tok::TokenKind Kind, 
                               ExprResult FileName) {
  PhpIncludeExpr::IncludeKind IncludeKind;
  switch (Kind) {
  case tok::kw_include: IncludeKind = PhpIncludeExpr::Include; break;
  case tok::kw_include_once: IncludeKind = PhpIncludeExpr::IncludeOnce; break;
  case tok::kw_require: IncludeKind = PhpIncludeExpr::Require; break;
  case tok::kw_require_once: IncludeKind = PhpIncludeExpr::RequireOnce; break;
  default:
    llvm_unreachable("Invalid include kind");
  }
  return new (Context)PhpIncludeExpr(QualType(), Loc, 
                                     IncludeKind, FileName.get());
}

ExprResult Sema::ActOnPHPInstanceOf(SourceLocation Loc, 
                                    ExprResult Base, 
                                    CXXScopeSpec SS, 
                                    ExprResult Name) {
  // TODO: implement
  return ExprError();
}

ExprResult Sema::ActOnPHPMapExpr(SourceLocation Loc, 
                                 ExprResult Id, 
                                 ExprResult Val) {
  assert(Id.isUsable() && Val.isUsable());
  //TODO: check Id types & emit warnings
  return new(Context)PhpMapExpr(Loc, Id.get(), Val.get());
}

ExprResult Sema::ActOnPHPMemberAccessExpr(Expr *Base, 
                                          SourceLocation ArrowLoc, 
                                          SourceLocation Loc, 
                                          DeclarationName DN) {
  // TODO: implement
  return ExprError();
}

ExprResult Sema::ActOnPHPMemberAccessExpr(Expr *Base, 
                                          SourceLocation ArrowLoc, 
                                          SourceLocation LBLoc, 
                                          Expr *Name, 
                                          SourceLocation RBLoc) {
  // TODO: implement
  return ExprError();
}

ExprResult Sema::ActOnPHPNewExpression(SourceLocation Loc, 
                                       CXXScopeSpec SS, 
                                       ExprResult ClassName, 
                                       ArrayRef<Expr*> Args) {
  // TODO: implement
  return ExprError();
}

ExprResult Sema::ActOnPHPNullLiteral(SourceLocation Loc) {
  return new(Context)PhpNullLiteral(Context.VoidTy, Loc);
}


ExprResult Sema::ActOnPostfixUnaryOp(SourceLocation OpLoc,
                                        tok::TokenKind Kind,
                                        ExprResult Base) {
  UnaryOperatorKind Opc;
  switch (Kind) {
  case tok::minusminus: Opc = UO_PostDec; break;
  case tok::plusplus:   Opc = UO_PostInc; break;
  default:
    llvm_unreachable("Unsupported unary operator");
  }

  Expr *Result = new (Context) UnaryOperator(Base.get(), Opc, getClassBoxType(),
                                             VK_RValue, OK_Ordinary, OpLoc);
  Decorator D(*this);
  QualType ResType = D.decorateByType(Result);
  if (ResType.isNull())
    return ExprError();
  Result->setType(ResType);
  return Result;
}


ExprResult Sema::ActOnPrefixUnaryOp(SourceLocation Loc, tok::TokenKind Kind,
                                    Expr *SubExpr) {
  assert(SubExpr);
  QualType ResultTy = SubExpr->getType();

  // First process operators that are not mapped to unary operation codes.
  switch (Kind) {
  case tok::at:
    return new (Context) PhpSuppressExpr(Loc, SubExpr);
  case tok::kw_clone:
    return new (Context) PhpCloneExpr(Loc, SubExpr);
  case tok::kw_print:
    llvm_unreachable("unimplemented"); //TODO:
  default:
    break;
  }

  // Convert token to operation.
  UnaryOperatorKind Opc;
  switch (Kind) {
  case tok::minus:      Opc = UO_Minus;  break;
  case tok::plus:       Opc = UO_Plus;   break;
  case tok::minusminus: Opc = UO_PreDec; break;
  case tok::plusplus:   Opc = UO_PreInc; break;
  case tok::exclaim:    Opc = UO_LNot;   break;
  case tok::tilde:      Opc = UO_Not;    break;
  default:
    llvm_unreachable("Unsupported unary operator");
  }

  // Create resulting node. At the beginning it has universal type, the actual
  // type will be calculated by Decorator component.
  Expr *Result = new (Context) UnaryOperator(SubExpr, Opc, getClassBoxType(),
                                             VK_RValue, OK_Ordinary, Loc);
  Decorator D(*this);
  QualType ResType = D.decorateByType(Result);
  if (ResType.isNull())
    return ExprError();
  Result->setType(ResType);

  // Operation specific processing.
  switch (Opc) {
  case UO_PreInc:
  case UO_PreDec:
//TODO:    if (!isPhpVariableExpr(SubExpr))
    if (!SubExpr->isLValue() && !isa<ArraySubscriptExpr>(SubExpr)) {
      Diag(SubExpr->getLocStart(), diag::err_php_not_lvalue);
      return ExprError();
    } else if (ResultTy.isConstQualified()) {
      Diag(SubExpr->getLocStart(), diag::err_php_const_modification);
      return ExprError();
    }
    break;
  default:
    ;
  }

  return Result;
}


static QualType getCastType(StringRef Name) {
  QualType Res = StringSwitch<QualType>(Name)
    .Case("unset", RuntimeTypes::getVoidType())
    .Cases("bool", "boolean", RuntimeTypes::getBoolType())
    .Cases("int", "integer", RuntimeTypes::getIntValueType())
    .Cases("real", "float", "double", RuntimeTypes::getDoubleType())
    .Cases("string", "binary", RuntimeTypes::getStringBoxType())
    .Case("array", RuntimeTypes::getArrayPtrType())
    .Case("object", RuntimeTypes::getObjectPtrType())
    .Default(QualType());

  assert(!Res.isNull());
  return Res;
}


CastKind Sema::getCastKind(QualType ExprType, QualType CastType) {
  if (ExprType == CastType)
    return CK_NoOp;

  // Cast to and from void must be processed so that side effects are preserved.
  if (CastType->isVoidType() || ExprType->isVoidType())
    return CK_Dependent;

  if (CastType == getBoolType()) {
    if (ExprType->isIntegerType())
      return CK_IntegralToBoolean;
    if (ExprType->isFloatingType())
      return CK_FloatingToBoolean;
    return CK_Dependent;
  }

  if (CastType == getIntValueType()) {
    if (ExprType->isIntegerType())
      return CK_IntegralCast;
    if (ExprType->isFloatingType())
      return CK_FloatingToIntegral;
    return CK_Dependent;
  }

  if (CastType == getDoubleType()) {
    if (ExprType->isFloatingType())
      return CK_FloatingCast;
    if (ExprType->isIntegerType())
      return CK_IntegralToFloating;
    return CK_Dependent;
  }

  if (isStringType(CastType) || isArrayType(CastType) || isObjectType(CastType))
    return CK_Dependent;

  llvm_unreachable("unknown cast type");
}


ExprResult Sema::ActOnCastExpr(SourceLocation LPLoc, IdentifierInfo *CastName,
                               SourceLocation RPLoc, Expr *SubExpr) {
  QualType ExprType = SubExpr->getType();
  QualType CastType = getCastType(CastName->getName().lower());
  CastKind Kind = getCastKind(ExprType, CastType);

  TypeSourceInfo *TSI = getTypeSourceInfo(CastType);
  return CStyleCastExpr::Create(Context, CastType, VK_RValue, Kind, SubExpr, 
                                nullptr, TSI, LPLoc, RPLoc);
}


ExprResult Sema::ActOnPHPReferenceAccess(SourceLocation Loc, ExprResult Arg, bool IsVariable) {
  assert(Arg.isUsable());
  if (!IsVariable) {
    Diag(Loc, diag::err_expected) << "l-value variable";
    return ExprError();
  }
  return new(Context)PhpReferenceExpr(Loc, Arg.get());
}

ExprResult Sema::ActOnPHPSubscript(Expr *Base, 
                                   SourceLocation LPLoc, 
                                   Expr *Subscript, 
                                   SourceLocation RPLoc) {
  assert(Base && "Invalid subscript base");
  // TODO: implement
  return ExprError();
}


ExprResult Sema::ActOnVariableUse(SourceLocation Loc, 
                                     ExprResult Name) {
  // TODO: implement
  return ExprError();
}


ExprResult Sema::ActOnVariableUse(Scope *S, DeclarationNameInfo Name) {
  VarDecl *Var = findVariable(S, Name.getName());
  if (!Var) {
    if (isUnitFunctionContext()) {
      Var = createGlobalVariable(Name.getAsString(), Name.getLoc());
      UnitDeclarations.push_back(Var);
    } else {
      Var = createLocalVariable(S, Name.getAsString(), Name.getLoc());
    }
  }
  QualType ExprType = Var->getType();
  if (ExprType->isLValueReferenceType())
    ExprType = ExprType->getPointeeType();
  return BuildDeclRefExpr(Var, ExprType, VK_LValue, Name.getLoc());
}


// \Scope\SS::${expr-name}
ExprResult Sema::ActOnPHPClassVariableUse(CXXScopeSpec SS, 
                                          SourceLocation Loc, 
                                          ExprResult Name) {
  // TODO: implement
  return ExprError();
}


// \Scope\SS::$name
ExprResult Sema::ActOnPHPClassVariableUse(CXXScopeSpec SS,
                                          SourceLocation Loc,
                                          DeclarationName Name) {
  // TODO: implement
  return ExprError();
}


// $base::${expr}
ExprResult Sema::ActOnPHPClassVariableUse(ExprResult Base, 
                                          SourceLocation Loc, 
                                          ExprResult Name) {
  // TODO: implement
  return ExprError();
}


// $base::$name
ExprResult Sema::ActOnPHPClassVariableUse(ExprResult Base, 
                                          SourceLocation Loc, 
                                          DeclarationName Name) {
  // TODO: implement
  return ExprError();
}


// $base->${expr-name}
ExprResult Sema::ActOnPHPObjectVariableUse(ExprResult Base, 
                                           SourceLocation Loc, 
                                           ExprResult Name) {
  // TODO: implement
  return ExprError();
}


// $base->name
ExprResult Sema::ActOnPHPObjectVariableUse(ExprResult Base, 
                                           SourceLocation Loc, 
                                           DeclarationName Name) { 
  // TODO: implement
  return ExprError();
}


QualType Sema::getTypeForSpec(CXXScopeSpec SS) {
  llvm_unreachable("not implemented");
  return QualType();
}


DeclContext *Sema::getContextForScopeSpec(CXXScopeSpec SS) {
  // If the scope specifier is not specified, assume it selects th current
  // namespace. We do not know, if we are searching for function, class or
  // constant, so do not specify particular namespace.
  if (SS.isEmpty())
    return CurNamespace;

  NestedNameSpecifier *Spec = SS.getScopeRep();
  assert(Spec != nullptr);
  switch (Spec->getKind()) {
  case NestedNameSpecifier::Global:
    return PhpNamespace;
  case NestedNameSpecifier::Namespace:
    return Spec->getAsNamespace();
  case NestedNameSpecifier::TypeSpec: {
    const Type *Ty = Spec->getAsType();
    if (CXXRecordDecl *CD = Ty->getAsCXXRecordDecl()) {
      return CD;
    }
    llvm_unreachable("Invalid type in scope spec");
  }
  default:
    llvm_unreachable("Invalid scope spec");
  }
}


bool Sema::IsClassNestedScopeSpecifier(CXXScopeSpec SS) {
  //TODO: implement
  return false;
}


std::string Sema::getNamespaceAsString(NamespaceDecl *NS) {
  std::string Result;
  // Strip out PHP-specific namespaces.
  if (NS->getNameAsString() == ConstNamespaceName ||
      NS->getNameAsString() == FuncNamespaceName  ||
      NS->getNameAsString() == ClassNamespaceName ||
      NS->getNameAsString() == VarNamespaceName    ) {
    NS = cast<NamespaceDecl>(NS->getDeclContext());
  }
  for (DeclContext *DC = NS; DC; DC = DC->getParent()) {
    if (isa<TranslationUnitDecl>(DC))
      break;
    if (NamespaceDecl *CNS = dyn_cast<NamespaceDecl>(DC)) {
      if (!CNS->isAnonymousNamespace()) {
        if (CNS->getNameAsString() == "php")
          if (DeclContext *PCNS = CNS->getParent())
            if (PCNS->isTranslationUnit())
              break;
        if (Result.empty())
          Result = CNS->getNameAsString();
        else
          Result = CNS->getNameAsString() + "\\" + Result;
      }
    } else {
      assert(isa<LinkageSpecDecl>(DC));
    }
  }
  return Result;
}


ExprResult Sema::processBuiltinCall(StringRef FuncName, SourceLocation NameLoc,
  SourceLocation LParenLoc, MultiExprArg ArgExprs,
  SourceLocation RParenLoc) {
  const BuiltinInfo *BI = find_builtin_function(FuncName);
  if (!BI)
    return ExprEmpty();

  if (BI->Handlers.SemaH) {
    ExprResult Res = BI->Handlers.SemaH(*this, *BI, FuncName,
                                        LParenLoc, ArgExprs, RParenLoc);
    if (Res.isUsable())
      return Res;
    if (Res.isInvalid())
      return ExprError();
  }

  return handleDefaulBuiltinCall(*BI, FuncName, NameLoc,
    LParenLoc, ArgExprs, RParenLoc);
}


static bool isCorrectArgumentType(Sema &S, const builtin::Param &ParamInfo,
                                  QualType ArgType, Expr *Arg) {
  QualType PossibleMatchTy;
  for (unsigned I = 0; I < ParamInfo.T.size(); ++I) {
    QualType ParamTy = ParamInfo.T.getType(I);
    if (ParamInfo.A.isReference())
      ParamTy = S.Context.getLValueReferenceType(ParamTy);
    auto TC = S.comparePHPTypes(ParamTy, ArgType);
    if (TC == Sema::TC_strict || TC == Sema::TC_value_dependent)
      return true;
    if (PossibleMatchTy.isNull() && !S.isStrictTypes() && TC == Sema::TC_weak)
      PossibleMatchTy = ParamTy;
  }
  if (!PossibleMatchTy.isNull()) {
    S.Diag(Arg->getExprLoc(), diag::warn_php_implicit_type_conversion)
      << S.getPHPTypeName(ArgType)
      << S.getPHPTypeName(PossibleMatchTy)
      << Arg->getSourceRange();
    return true;
  }
  return false;
}


ExprResult Sema::handleDefaulBuiltinCall(const BuiltinInfo &BI,
                                StringRef FuncName, SourceLocation NameLoc,
                                SourceLocation LParenLoc, MultiExprArg ArgExprs,
                                SourceLocation RParenLoc) {

  // Check argument count
  if (ArgExprs.size() < BI.getNumRequiredArgs()) {
    Diag(RParenLoc, diag::err_php_too_few_arguments_in_call)
      << BI.isVariadic() << BI.getNumRequiredArgs() << int(ArgExprs.size());
    return ExprError();
  }
  if (!BI.isVariadic() && ArgExprs.size() > BI.Params.size()) {
    SourceLocation Loc = ArgExprs[BI.Params.size()]->getExprLoc();
    Diag(Loc, diag::err_php_too_many_arguments_in_call)
      << BI.isVariadic() << BI.getNumRequiredArgs() << int(ArgExprs.size());
    return ExprError();
  }

  // Check argument types
  SmallVector<Expr*, 8> CallArgs;
  unsigned ArgIdx = 0;
  for (; ArgIdx < ArgExprs.size(); ++ArgIdx) {
    QualType ArgTy = ArgExprs[ArgIdx]->getType();
    auto &P = BI.Params[ArgIdx];
    if (P.A.isVariadic())
      break;
    if (!isCorrectArgumentType(*this, P, ArgTy, ArgExprs[ArgIdx])) {
      Diag(ArgExprs[ArgIdx]->getExprLoc(), diag::err_php_invalid_argument_type)
          << getPHPTypeName(P.T.getType(0)) << getPHPTypeName(ArgTy);
      return ExprError();
    }
    CallArgs.push_back(ArgExprs[ArgIdx]);
  }

  // Check variadic arguments type
  if (ArgExprs.size() > ArgIdx && 
      BI.Params.size() == ArgIdx + 1 && 
      BI.Params.back().A.isVariadic()) {
    auto &VariadicInfo = BI.Params.back();
    for (; ArgIdx < ArgExprs.size(); ++ArgIdx) {
      QualType ArgTy = ArgExprs[ArgIdx]->getType();
      if (!isCorrectArgumentType(*this, VariadicInfo, ArgTy, ArgExprs[ArgIdx])) {
        Diag(ArgExprs[ArgIdx]->getExprLoc(), diag::err_php_invalid_argument_type)
          << getPHPTypeName(VariadicInfo.T.getType(0)) << getPHPTypeName(ArgTy);
        return ExprError();
      }
      CallArgs.push_back(ArgExprs[ArgIdx]);
    }
  }
  assert(ArgIdx == ArgExprs.size());

  // Build return type
  QualType RetTy;
  if (BI.Returns.size() == 0)
    RetTy = getVoidType();
  if (BI.Returns.size() == 1)
    RetTy = BI.Returns.getType(0);
  else
    RetTy = getClassBoxType();

  return BuildUnresolvedCall(FuncName, NameLoc, RetTy, CallArgs, RParenLoc);
}


/// \brief Converts expression type from 'box &' to 'const Box &'.
///
/// This method assumes that there is no difference in data layout between plain
/// and class types and no actual conversion is necessary.
///
Expr *Sema::castPlainTypeToConstRef(Expr *E) {
  assert(isUniversalPlainType(E->getType()));
  ASTContext& Context = getASTContext();
  CXXCastPath BasePath;
  QualType BaseType = getPlainBoxType();
  QualType DerivedType = getClassBoxType();

  // Build right conversion path.
  CheckDerivedToBaseConversion(DerivedType, BaseType, E->getLocStart(),
                      SourceRange(E->getLocStart(), E->getLocEnd()), &BasePath);

  return CXXStaticCastExpr::Create(Context, DerivedType, VK_LValue,
      CK_BaseToDerived, E, &BasePath, getConstClassBoxLRefInfo(),
      E->getLocStart(), E->getLocEnd(),
      SourceRange(E->getLocStart(), E->getLocEnd()));
}


// Build generic type for two php types
//
QualType Sema::BuildGenericType(QualType t1, QualType t2) {
  if (t1 == t2)
    return t1;

  if (isUniversalType(t1) || isUniversalType(t2))
    return getClassBoxType();

  if (t1->isRealType() && t2->isRealType())
    return getNumberBoxType();

  if (isStringType(t1) && isStringType(t2))
    return getStringBoxType();

  if (isArrayType(t1) && isArrayType(t2))
    return getArrayBoxType();

  return getClassBoxType();
}


// Type check in PHP:
//   By default PHP uses weak type comparison that allows type coercion
//   when possible (e.g. "123" -> int). Strict typing can be enabled on
//   per file basis using declare(strict_types) statement.
//
//   The only implicit conversion allowed in strict type checking is cast
//   from long to float. Otherwise types should be exactly the same.
//
//   If weak type comparison is enabled the following implicit casts are
//   applied for mismatched types of (parameter, argument) pair:
//     1. [long, double, string] can be casted to bool.
//     2. - string can be casted to long if it starts with double that fits into 
//          long;
//        - double can be casted to long unless it is nan or its value does not
//          fit into long;
//        - bool can be casted to long.
//     3. - [bool, long] can be casted to double;
//        - string can be casted to double if it starts with double.
//     4. - [bool, long, double] can be casted to string;
//        - object can be casted to string if it has __toString method.
//
Sema::TypeCompare Sema::comparePHPTypes(QualType ParamType,
                                        QualType ArgType) {
  ArgType = unboxType(ArgType);
  ParamType = unboxType(ParamType);

  if (ParamType == ArgType)
    return TC_strict;

  if (isUniversalType(ArgType) || isUniversalType(ParamType))
    return TC_value_dependent;

  // Parameter is box&
  if (const ReferenceType *RT = dyn_cast<ReferenceType>(ParamType.getTypePtr())) {
    if (isUniversalType(RT->getPointeeType())) {
      if (isUniversalType(ArgType))
        return TC_value_dependent;
      else
        return TC_incompatible;
    }
  }

  // Parameter is boolean
  if (isBooleanType(ParamType)) {
    if (Runtime::isRealType(ArgType))
      return TC_weak;
    if (isNumberType(ArgType) || isStringType(ArgType))
      return TC_value_dependent;
    return TC_incompatible;
  }

  // Parameter is long
  if (isIntegerType(ParamType)) {
    assert(!isBooleanType(ParamType) && "should be handled by now");
    if (isBooleanType(ArgType) || isFloatingType(ArgType))
      return TC_weak;
    if (isIntegerType(ArgType))
      return TC_strict;
    if (isNumberType(ArgType) || isStringType(ArgType))
      return TC_value_dependent;
    return TC_incompatible;
  }

  // Parameter is double
  if (isFloatingType(ParamType)) {
    if (isBooleanType(ArgType))
      return TC_weak;
    if (Runtime::isRealType(ArgType))
      return TC_strict;
    if (isNumberType(ArgType) || isStringType(ArgType))
      return TC_value_dependent;
    return TC_incompatible;
  }

  // Parameter is number
  if (isNumberType(ParamType)) {
    assert(!isNumberType(ArgType) && "should be handled by now");
    if (Runtime::isRealType(ArgType))
      return TC_strict;
    if (isStringType(ArgType))
      return TC_value_dependent;
    return TC_incompatible;
  }

  // Parameter is string
  if (isStringType(ParamType)) {
    if (isStringType(ArgType))
      return TC_strict;
    if (Runtime::isRealType(ArgType))
      return TC_weak;
    //TODO: handle objects
    return TC_incompatible;
  }

  // Parameter is an array.
  if (isArrayType(ParamType))
    return isArrayType(ArgType) ? TC_strict : TC_incompatible;

  // TODO: compare objects
  llvm_unreachable("unknown parameter type");
}


std::string Sema::getPHPTypeName(QualType Ty) {
  Ty = unboxType(Ty);
  if (Ty->isVoidType())
    return "NULL";
  if (isBooleanType(Ty))
    return "boolean";
  if (isIntegerType(Ty))
    return "integer";
  if (isFloatingType(Ty))
    return "double";
  if (isStringType(Ty))
    return "string";
  if (isArrayType(Ty))
    return "array";
  if (isObjectType(Ty))
    return "object";
  if (isResourceType(Ty))
    return "resource";
  if (isNumberType(Ty))
    return "number";

//   if (isBoxedType(Ty))
//     return "&" + getPHPTypeName(unboxType(Ty));
// 
//   if (isArgPackType(Ty)) {
//     QualType PointeeTy = unboxType(Ty);
//     if (PointeeTy->isPointerType())
//       return "&..." + getPHPTypeName(PointeeTy->getPointeeType());
//     return "..." + getPHPTypeName(PointeeTy);
//   }
// 
  //TODO: handle the rest of types
  return Ty->getTypeClassName();
}


void Sema::diagnoseImplicitTypeCast(SourceLocation Loc, 
                                    QualType ParamType, QualType ArgType) {
  assert(comparePHPTypes(ParamType, ArgType) == TC_weak);

  ArgType = unboxType(ArgType);
  if (isRefBoxType(ParamType)) {
    ParamType = unboxType(ParamType);
    Diag(Loc, diag::warn_php_implicit_ref_conversion)
      << getPHPTypeName(ArgType) << getPHPTypeName(ParamType);
    return;
  }

  // diagnose possible dangerous implicit casts

  // float -> int
  if (ArgType->isFloatingType() && 
    ParamType->isIntegerType() && !ParamType->isBooleanType()) {
    Diag(Loc, diag::warn_php_implicit_type_conversion)
      << getPHPTypeName(ArgType) << getPHPTypeName(ParamType);
    return;
  }
}


Expr *Sema::castToLValueRef(Expr *E) {
  if (E->getType()->isLValueReferenceType())
    return E;
  SourceLocation Loc = E->getExprLoc();
  DeclarationName Name;
  QualType BaseType = E->getType();
  QualType RefRefType =
    BuildReferenceType(BaseType, false, Loc, Name);

  // cast to (T&&)
  TypeSourceInfo *TSI = getTypeSourceInfo(RefRefType);
  Expr *Intermediate = BuildCStyleCastExpr(Loc, TSI, Loc, E).get();

  QualType RefType = Context.getLValueReferenceType(BaseType);
  TSI = getTypeSourceInfo(RefType);
  return BuildCStyleCastExpr(Loc, TSI, Loc, Intermediate).get();
}


//------------------------------------------------------------------------------
// Constant expression support.
//------------------------------------------------------------------------------

/// \brief Records the fact that the given expression calculates to the
/// specified constant values.
///
void Sema::addConstantExpr(const Expr *E, ConstValue *V) {
  assert(E);
  assert(V);
  ConstantExprs[E] = V;
  ConstantsInTree.insert(std::make_pair(V, E));
}


/// \brief Creates new expression that represents the specified constant value
/// started at the given location in source text.
///
Expr *Sema::buildConstantExpr(ConstValue *V, SourceLocation Loc) {
  Expr *E = nullptr;
  if (V->isNull()) {
    E = new(Context) PhpNullLiteral(Context.VoidTy, Loc);
  } else if (V->isBool()) {
    E = new(Context) CXXBoolLiteralExpr(V->getBool(), Context.BoolTy, Loc);
  } else if (V->isInt()) {
    E = IntegerLiteral::Create(Context, V->getInt(), getIntValueType(), Loc);
  } else if (V->isFloat()) {
    E = FloatingLiteral::Create(Context, V->getFloat(), true, Context.DoubleTy,
                                Loc);
  } else if (V->isString()) {
    E = BuildStringLiteral(V->getString(), Loc);
  } else if (V->isArray()) {
    llvm_unreachable("not implemented");
  } else {
    llvm_unreachable("Bad constant type");
  }
  addConstantExpr(E, V);
  return E;
}


/// \brief Finds or creates expression that corresponds to the given value at
/// given location.
///
/// \param V Constant value.
/// \param Loc Location in source that correspond to the value start. If it is
///            invalid, location does not matter.
/// \param Literal If true, the function returns only literal expression.
///                Otherwise it returns the expression that was previously
///                identified as evaluated to constant.
///
Expr *Sema::getConstantExpr(ConstValue *V, SourceLocation Loc, bool Literal) {
  // If such value has not been seen, create new literal expression for it.
  if (!Constants.has(V)) {
    Constants.add(V);
    return buildConstantExpr(V, Loc);
  }

  // Among all expressions that correspond to the given value try finding
  // the expression that fits the required source location.
  auto Range = ConstantsInTree.equal_range(V);
  for (auto I = Range.first; I != Range.second; ++I) {
    const Expr *E = I->second;
    if ((Loc.isInvalid() || E->getLocStart() == Loc)) {
      if (!Literal || isLiteralExpr(E))
        return const_cast<Expr*>(E);
    }
  }

  return buildConstantExpr(V, Loc);
}


ConstValue *Sema::getConstantValue(const Expr *E) {
  Expr *Side = nullptr;
  E = unpackSideEffect(const_cast<Expr*>(E), Side);

  auto Ptr = ConstantExprs.find(E);
  if (Ptr != ConstantExprs.end())
    return Ptr->second;

  if (isa<PhpNullLiteral>(E)) {
    ConstValue *V = Constants.getNull();
    addConstantExpr(E, V);
    return V;
  }
  if (const CXXBoolLiteralExpr *BE = dyn_cast<CXXBoolLiteralExpr>(E)) {
    ConstValue *V = Constants.get(BE->getValue());
    addConstantExpr(BE, V);
    return V;
  }
  if (const IntegerLiteral *IE = dyn_cast<IntegerLiteral>(E)) {
    ConstValue *V = Constants.get(alignToIntValueWidth(IE)->getValue());
    addConstantExpr(IE, V);
    return V;
  }
  if (const FloatingLiteral *FE = dyn_cast<FloatingLiteral>(E)) {
    ConstValue *V = Constants.get(FE->getValue());
    addConstantExpr(FE, V);
    return V;
  }
  if (const StringLiteral *SE = dyn_cast<StringLiteral>(E)) {
    ConstValue *V = Constants.get(SE->getBytes());
    addConstantExpr(SE, V);
    return V;
  }

  return nullptr;
}


ConstValue *Sema::getConstantValue(ConstValue *CV, QualType TargetType,
                                   SourceLocation Loc) {
  assert(CV);
  ConstValue *NCV = nullptr;
  ConstValue::ConversionStatus Status = ConstValue::OK;
  if (isStringType(TargetType)) {
    NCV = Constants.get(CV->getAsString(Status));
  } else if (TargetType->isBooleanType()) {
    NCV = Constants.get(CV->getAsBool(Status));
  } else if (TargetType->isFloatingType()) {
    NCV = Constants.get(CV->getAsFloat(Status));
  } else if (TargetType->isIntegerType()) {
    NCV = Constants.get(CV->getAsInt(Status));
  } else if (isNumberType(TargetType)) {
    if (CV->isFloat() || CV->isInt() || CV->isBool())
      return CV;
    if (CV->isNull())
      return Constants.getZero();
    bool HasExtra;
    if (CV->isStringFloat(HasExtra))
      NCV = Constants.get(CV->getAsFloat(Status));
    else if (CV->isStringInteger(HasExtra))
      NCV = Constants.get(CV->getAsInt(Status));
    if (CV->isString()) {
      if (diagnoseConversion(ConstValue::NaN, Loc))
        return nullptr;
      return Constants.getZero();
    }
  } else if (isArrayType(TargetType)) {
    if (CV->isArray())
      return CV;
    SmallVector<ArrayItem, 1> Items;
    if (!CV->isNull())
      Items.push_back(CV);
    return Constants.get(Items);
  } else if (TargetType->isVoidType()) {
    return Constants.getNull();
  } else {
    llvm_unreachable("invalid target type");
  }

  if (diagnoseConversion(Status, Loc))
    return nullptr;
  return NCV;
}


QualType Sema::getConstantType(ConstValue *CV) {
  assert(CV);
  if (CV->isNull())
    return getVoidType();
  if (CV->isBool())
    return getBoolType();
  if (CV->isInt())
    return getIntValueType();
  if (CV->isFloat())
    return getDoubleType();
  if (CV->isString())
    return getStringRefType();
  if (CV->isArray())
    return getArrayPtrType();
  llvm_unreachable("unexpected constant type");
  return QualType();
}


IntegerLiteral *Sema::alignToIntValueWidth(const IntegerLiteral *E) {
  APInt Val = E->getValue();

  if (Val.getBitWidth() == Runtime::getIntValueWidth())
    return const_cast<IntegerLiteral*>(E);

  assert(Val.getBitWidth() < Runtime::getIntValueWidth());
  APInt Ext = Val.sext(Runtime::getIntValueWidth());
  auto CV = getConstants().get(Ext);
  return cast<IntegerLiteral>(getConstantExpr(CV, E->getExprLoc(), true));
}


bool Sema::diagnoseConversion(ConstValue::ConversionStatus Status,
                              SourceLocation Loc) {
  switch (Status) {
  case ConstValue::OK:
    break;
  case ConstValue::ExtraSymbols:
    Diag(Loc, diag::warn_php_extra_symbols_in_number) << "integer";
    break;
  case ConstValue::NaN:
    Diag(Loc, diag::warn_php_not_a_number);
    break;
  case ConstValue::IntTooLarge:
    reportTooLargeInteger(Loc, false);
    break;
  case ConstValue::IntTooLargeNegative:
    reportTooLargeInteger(Loc, true);
    break;
  case ConstValue::FloatTooLarge:
    reportTooLargeFloat(Loc);
    break;
  case ConstValue::ArrayConv:
    Diag(Loc, diag::warn_php_array_to_number);
    break;
  case ConstValue::Invalid:
    Diag(Loc, diag::warn_php_invalid_conversion);
    return true;
  }
  return false;
}


/// \brief Checks if the given expression is "literal", that is it is a constant
/// and cannot be simplified anymore.
///
bool Sema::isLiteralExpr(const Expr *E) {
  if (isa<PhpNullLiteral>(E) ||
      isa<CXXBoolLiteralExpr>(E) ||
      isa<IntegerLiteral>(E) ||
      isa<FloatingLiteral>(E) ||
      isa<StringLiteral>(E))
    return true;
  if (const PhpArrayExpr *AE = dyn_cast<PhpArrayExpr>(E)) {
    for (const Expr *Item : AE->items())
      if (const PhpMapExpr *Map = dyn_cast<PhpMapExpr>(Item)) {
        if (!isLiteralExpr(Map->getLHS()) || !isLiteralExpr(Map->getRHS()))
          return false;
      } else if (!isLiteralExpr(Item)) {
        return false;
      }
    return true;
  }
  return false;
}


void Sema::setCurNamespace(NamespaceDecl *NS, SourceLocation NSLoc,
                           SourceLocation LBrLoc) {
  if (NS == CurNamespace)
    return;
  CurNamespace = NS;
  CurContext = NS;
  NamespaceStartLoc = NSLoc;
  NamespaceLBrace = LBrLoc;
  FuncNamespace = nullptr;
  ConstNamespace = nullptr;
  ClassNamespace = nullptr;
}


NamespaceDecl *Sema::getFuncNamespace(NamespaceDecl *Host) {
  if (!Host) {
    Host = CurNamespace;
  } else {
    assert(!StringRef(FuncNamespaceName).equals_lower(Host->getNameAsString()));
  }
  if (Host == CurNamespace) {
    if (FuncNamespace)
      return FuncNamespace;
  }
  NamespaceDecl *NS = createNamespace(FuncNamespaceName, Host);
  if (Host == CurNamespace)
    FuncNamespace = NS;
  return NS;
}


NamespaceDecl *Sema::getConstNamespace(NamespaceDecl *Host) {
  if (!Host) {
    Host = CurNamespace;
  } else {
    assert(!StringRef(ConstNamespaceName).equals_lower(Host->getNameAsString()));
  }
  if (Host == CurNamespace) {
    if (ConstNamespace)
      return ConstNamespace;
  }
  NamespaceDecl *NS = createNamespace(ConstNamespaceName, Host);
  if (Host == CurNamespace)
    ConstNamespace = NS;
  return NS;
}


NamespaceDecl *Sema::getClassNamespace() {
  if (!ClassNamespace)
    ClassNamespace = createNamespace(ClassNamespaceName, CurNamespace);
  return ClassNamespace;
}


bool Sema::isInGlobalNamespace(Decl *D) {
  DeclContext *DC = D->getDeclContext();

  if (FunctionDecl *FD = dyn_cast<FunctionDecl>(DC)) {
    if (NamespaceDecl *NS = dyn_cast_or_null<NamespaceDecl>(FD->getDeclContext())) {
      if (NS->getNameAsString() == FuncNamespaceName) {
        if (NamespaceDecl *PNS = dyn_cast_or_null<NamespaceDecl>(NS->getDeclContext())) {
          if (PNS->getName() == "php") {
            return true;
          }
        }
      }
    }
  } else if (VarDecl *VD = dyn_cast<VarDecl>(DC)) {
    if (NamespaceDecl *NS = dyn_cast_or_null<NamespaceDecl>(VD->getDeclContext())) {
      if (NS->getNameAsString() == ConstNamespaceName) {
        if (NamespaceDecl *PNS = dyn_cast_or_null<NamespaceDecl>(NS->getDeclContext())) {
          if (PNS->getName() == "php") {
            return true;
          }
        }
      }
    }
  }

  return false;
}


bool Sema::isPhpNamespace(NamespaceDecl *NS) {
  while (NS) {
    if (NS->getFirstDecl() == PhpNamespace->getFirstDecl())
      return true;
    NS = dyn_cast<NamespaceDecl>(NS->getDeclContext());
  }
  return false;
}


bool Sema::isInNamespace(Decl *D1, NamespaceDecl *NS) {
  DeclContext *DC = D1->getDeclContext();
  if (NamespaceDecl *NS2 = dyn_cast<NamespaceDecl>(DC)) {
    if (NS2->getName() == ConstNamespaceName) {
      assert(isa<VarDecl>(D1));
      NS2 = cast<NamespaceDecl>(NS2->getDeclContext());
    } else if (NS2->getName() == FuncNamespaceName) {
      assert(isa<FunctionDecl>(D1));
      NS2 = cast<NamespaceDecl>(NS2->getDeclContext());
    } else if (NS2->getName() == ClassNamespaceName) {
      assert(isa<CXXRecordDecl>(D1));
      NS2 = cast<NamespaceDecl>(NS2->getDeclContext());
    }
    NS2 = NS2->getFirstDecl();
    return NS2 == NS->getFirstDecl();
  }
  return false;
}


bool Sema::isPhpFunction(FunctionDecl *FD) {
  if (NamespaceDecl *NS = dyn_cast<NamespaceDecl>(FD->getDeclContext()))
    if (NS->getDeclName().getAsString() == FuncNamespaceName)
      if (NamespaceDecl *NS2 = dyn_cast<NamespaceDecl>(NS->getDeclContext()))
        return isPhpNamespace(NS2);
  return false;
}


bool Sema::isPhpConstant(VarDecl *FD) {
  if (NamespaceDecl *NS = dyn_cast<NamespaceDecl>(FD->getDeclContext()))
    if (NS->getDeclName().getAsString() == ConstNamespaceName)
      if (NamespaceDecl *NS2 = dyn_cast<NamespaceDecl>(NS->getDeclContext()))
        return isPhpNamespace(NS2);
  return false;
}


bool Sema::isPhpVariable(VarDecl *FD) {
  if (NamespaceDecl *NS = dyn_cast<NamespaceDecl>(FD->getDeclContext()))
    if (NS->getDeclName().getAsString() == VarNamespaceName)
      return isInNamespace(NS, getPhpNamespace());
  return false;
}


void Sema::reportTooLargeInteger(SourceLocation Loc, bool Negative) {
    SmallString<40> buffer;
  if (Negative) {
    APInt::getSignedMinValue(getIntValueWidth()).toStringSigned(buffer);
    Diag(Loc, diag::warn_php_too_large_int_negative) << buffer;
  } else {
    APInt::getSignedMaxValue(getIntValueWidth()).toStringSigned(buffer);
    Diag(Loc, diag::warn_php_too_large_int) << buffer;
  }
}


void Sema::reportTooLargeFloat(SourceLocation Loc) {
  SmallString<40> buffer;
  APFloat::getLargest(APFloat::IEEEdouble).toString(buffer);
  Diag(Loc, diag::warn_float_overflow) << Context.DoubleTy << buffer;
}

}
