//===--- RuntimeInit.cpp -- Interface with PHP Runtime ----------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2016, Serge Pavlov.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Part of Runtime interface responsible for creation of variable initializers.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
#include "shared/constants.h"
#include "shared/value_kinds.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


Expr *Runtime::getInitStringRef(StringLiteral *Val) {
  SourceLocation Loc = Val->getLocStart();
  SmallVector<Expr *, 2> Initializers;

  // var.data = Val.data;
  QualType CharPtrTy = Semantics.Context.CharTy;
  CharPtrTy.addConst();
  CharPtrTy = Semantics.Context.getPointerType(CharPtrTy);
  ImplicitCastExpr *StrPtr = ImplicitCastExpr::Create(Semantics.Context,
                    CharPtrTy, CK_ArrayToPointerDecay, Val, nullptr, VK_RValue);
  Expr *ValueInit = Semantics.ActOnDesignatedInitializer(
                             makeDesignation("data"), Loc, false, StrPtr).get();
  Initializers.push_back(ValueInit);

  // var.len = Val.size;
  Expr *StrLen = Semantics.ActOnIntegerConstant(Loc, Val->getByteLength()).get();
  ValueInit = Semantics.ActOnDesignatedInitializer(
                              makeDesignation("len"), Loc, false, StrLen).get();
  Initializers.push_back(ValueInit);

  // Create initializer
  ExprResult Initializer = Semantics.ActOnInitList(Loc, Initializers, Loc);
  return Initializer.get();
}


Expr *Runtime::getInitUndefined(SourceLocation Loc) {
  // var.e.type = vkUndef
  ExprResult TypeCode = Semantics.ActOnIntegerConstant(Loc, vkUndef);
  Expr *TypeInit = Semantics.ActOnDesignatedInitializer(
                          makeDesignation("e.type"), Loc, true, TypeCode).get();
  // Create initializer
  ExprResult Initializer = Semantics.ActOnInitList(Loc, MultiExprArg(TypeInit),
                                                   Loc);
  return Initializer.get();
}


Expr *Runtime::getInitNull(SourceLocation Loc) {
  // var.e.t.type = vkNull
  ExprResult TypeCode = Semantics.ActOnIntegerConstant(Loc, vkNull);
  Expr *TypeInit = Semantics.ActOnDesignatedInitializer(
                        makeDesignation("e.t.type"), Loc, true, TypeCode).get();

  // Create initializer
  ExprResult Initializer = Semantics.ActOnInitList(Loc, MultiExprArg(TypeInit),
                                                   Loc);
  return Initializer.get();
}


Expr *Runtime::getInitBoolean(CXXBoolLiteralExpr *Val) {
  SourceLocation Loc = Val->getLocStart();
  SmallVector<Expr *, 2> Initializers;

  // var.e.t.type = vkBool
  ExprResult TypeCode = Semantics.ActOnIntegerConstant(Loc, vkBool);
  Expr *TypeInit = Semantics.ActOnDesignatedInitializer(
                        makeDesignation("e.t.type"), Loc, true, TypeCode).get();
  Initializers.push_back(TypeInit);

  // var.v.v_long = Val;
  Expr *ValueInit = Semantics.ActOnDesignatedInitializer(
                            makeDesignation("v.v_long"), Loc, false, Val).get();
  Initializers.push_back(ValueInit);

  // Create initializer
  ExprResult Initializer = Semantics.ActOnInitList(Loc, Initializers, Loc);
  return Initializer.get();
}


Expr *Runtime::getInitInteger(IntegerLiteral *Val) {
  SourceLocation Loc = Val->getLocStart();
  SmallVector<Expr *, 2> Initializers;

  // var.e.t.type = vkInteger
  ExprResult TypeCode = Semantics.ActOnIntegerConstant(Loc, vkInteger);
  Expr *TypeInit = Semantics.ActOnDesignatedInitializer(
                        makeDesignation("e.t.type"), Loc, true, TypeCode).get();
  Initializers.push_back(TypeInit);

  // var.v.v_long = Val;
  Expr *ValueInit = Semantics.ActOnDesignatedInitializer(
                         makeDesignation("v.v_integer"), Loc, false, Val).get();
  Initializers.push_back(ValueInit);

  // Create initializer
  ExprResult Initializer = Semantics.ActOnInitList(Loc, Initializers, Loc);
  return Initializer.get();
}


Expr *Runtime::getInitFloat(FloatingLiteral *Val) {
  SourceLocation Loc = Val->getLocStart();
  SmallVector<Expr *, 2> Initializers;

  // var.e.t.type = vkDouble
  ExprResult TypeCode = Semantics.ActOnIntegerConstant(Loc, vkDouble);
  Expr *TypeInit = Semantics.ActOnDesignatedInitializer(
                        makeDesignation("e.t.type"), Loc, true, TypeCode).get();
  Initializers.push_back(TypeInit);

  // var.v.v_double = Val;
  Expr *ValueInit = Semantics.ActOnDesignatedInitializer(
                          makeDesignation("v.v_double"), Loc, false, Val).get();
  Initializers.push_back(ValueInit);

  // Create initializer
  ExprResult Initializer = Semantics.ActOnInitList(Loc, Initializers, Loc);
  return Initializer.get();
}


Expr *Runtime::getInitString(StringLiteral *Val) {
  SourceLocation Loc = Val->getLocStart();
  SmallVector<Expr *, 3> Initializers;

  // var.e.t.type = vkConstString
  ExprResult TypeCode = Semantics.ActOnIntegerConstant(Loc, vkConstString);
  Expr *TypeInit = Semantics.ActOnDesignatedInitializer(
                        makeDesignation("e.t.type"), Loc, true, TypeCode).get();
  Initializers.push_back(TypeInit);

  // var.v.v_cstring = Val.data;
  QualType CharPtrTy = Semantics.Context.CharTy;
  CharPtrTy.addConst();
  CharPtrTy = Semantics.Context.getPointerType(CharPtrTy);
  ImplicitCastExpr *StrPtr = ImplicitCastExpr::Create(Semantics.Context,
                          CharPtrTy, CK_ArrayToPointerDecay, Val, 0, VK_LValue);
  Expr *ValueInit = Semantics.ActOnDesignatedInitializer(
                      makeDesignation("v.v_cstring"), Loc, false, StrPtr).get();
  Initializers.push_back(ValueInit);

  // var.e.t.size = Val.data;
  Expr *StrLen = Semantics.ActOnIntegerConstant(Loc, Val->getByteLength()).get();
  ValueInit = Semantics.ActOnDesignatedInitializer(
                         makeDesignation("e.t.size"), Loc, false, StrLen).get();
  Initializers.push_back(ValueInit);

  // Create initializer
  ExprResult Initializer = Semantics.ActOnInitList(Loc, Initializers, Loc);
  return Initializer.get();
}


Expr *Runtime::getInitBoolean(SourceLocation Loc, bool Val) {
  CXXBoolLiteralExpr *BoolVal = new(Semantics.Context) CXXBoolLiteralExpr(Val,
                                                            getBoolType(), Loc);
  return getInitBoolean(BoolVal);
}


Expr *Runtime::getInitInteger(SourceLocation Loc, int64_t Val) {
  Expr *IntVal = Semantics.ActOnIntegerConstant(Loc, (int64_t)Val).get();
  return getInitInteger(cast<IntegerLiteral>(IntVal));
}


Expr *Runtime::getInitFloat(SourceLocation Loc, double Val) {
  auto *FloatVal = FloatingLiteral::Create(Semantics.Context, APFloat(Val),
                                           false, getDoubleType(), Loc);
  return getInitFloat(FloatVal);
}


Expr *Runtime::getInitString(SourceLocation Loc, StringRef Val) {
  StringLiteral *StrVal = Semantics.BuildStringLiteral(Val, Loc);
  return getInitString(StrVal);
}


Expr *Runtime::getBoxInitializer(Expr *Val) {
  assert(Val);
  assert(Semantics.isLiteralExpr(Val));

  if (Val->getType() == getVoidType())
    return getInitNull(Val->getLocStart());
  if (auto *ValLiteral = dyn_cast<CXXBoolLiteralExpr>(Val))
    return getInitBoolean(ValLiteral);
  if (auto *ValLiteral = dyn_cast<IntegerLiteral>(Val))
    return getInitInteger(ValLiteral);
  if (auto *ValLiteral = dyn_cast<FloatingLiteral>(Val))
    return getInitFloat(ValLiteral);
  if (auto *ValLiteral = dyn_cast<StringLiteral>(Val))
    return getInitString(ValLiteral);
  //TODO: arrays
  llvm_unreachable("invalid literal expression");
}


Expr *Runtime::createBoxInitializer(SourceLocation Loc, const ConstValue *Val) {
  value_kind VK;
  Expr *Value = nullptr;
  StringRef ValueFieldName;
  ASTContext &Ctx = Semantics.Context;
  Expr *Size = nullptr;
  switch (Val->getKind()) {
  case ConstValue::Null:
    VK = vkNull;
    break;
  case ConstValue::Bool:
    VK = vkBool;
    Value = new (Ctx) CXXBoolLiteralExpr(Val->getBool(), Ctx.BoolTy, Loc);
    ValueFieldName = "v.v_bool";
    break;
  case ConstValue::Int:
    VK = vkInteger;
    Value = IntegerLiteral::Create(Ctx, Val->getInt(),
                                   Runtime::getIntValueType(), Loc);
    ValueFieldName = "v.v_integer";
    break;
  case ConstValue::Float:
    VK = vkDouble;
    Value = FloatingLiteral::Create(Ctx, Val->getFloat(), true,
                                    Ctx.DoubleTy, Loc);
    ValueFieldName = "v.v_double";
    break;
  case ConstValue::String:
    VK = vkConstString;
    Value = Semantics.BuildStringLiteral(Val->getString(), Loc);
    ValueFieldName = "v.v_cstring";
    Size = Semantics.ActOnIntegerConstant(Loc, Val->getString().size()).get();
    break;
  case ConstValue::Array:
    VK = vkConstArray;
    Value = Semantics.BuildArrayInitVariable(Loc, Val->getArray());
    Value = Semantics.BuildConstCast(Value);
    ValueFieldName = "v.v_array";
    break;
  default:
    llvm_unreachable("unsupported constant type");
    return nullptr;
  }

  if (!Size)
    Size = Semantics.ActOnIntegerConstant(Loc, 0).get();

  Expr *SizeInit = Semantics.ActOnDesignatedInitializer(
      makeDesignation("e.t.size"), Loc, true, Size).get();
  assert(SizeInit && "failed to build size initializer");

  Expr *Type = Semantics.ActOnIntegerConstant(Loc, VK).get();
  Expr *TypeInit = Semantics.ActOnDesignatedInitializer(
      makeDesignation("e.t.type"), Loc, true, Type).get();
  assert(TypeInit && "failed to build type initializer");

  if (!ValueFieldName.empty()) {
    Value = Semantics.ActOnDesignatedInitializer(
      makeDesignation(ValueFieldName), Loc, true, Value).get();
    assert(Value && "failed to build value initializer");
  }

  SmallVector<Expr*, 4> Initializers;
  Initializers.push_back(SizeInit);
  Initializers.push_back(TypeInit);
  if (!ValueFieldName.empty())
    Initializers.push_back(Value);

  return Semantics.ActOnInitList(Loc, Initializers, Loc).get();
}


Expr *Runtime::getInitCTConstant(VarDecl *Var, Expr *IniVal) {
  assert(Var->getType().getLocalUnqualifiedType() == getValueInfoType());
  assert(!IniVal || Semantics.isLiteralExpr(IniVal));

  SourceLocation Loc = Var->getLocStart();
  SmallVector<Expr *, 8> Initializers;

  // name
  NamespaceDecl *NS = cast<NamespaceDecl>(Var->getDeclContext());
  std::string QName = Semantics.getNamespaceAsString(NS);
  if (!QName.empty())
    QName += "\\";
  QName += Var->getNameAsString();
  StringLiteral *Name = Semantics.BuildStringLiteral(QName, Loc);
  Expr *InitValue = getInitStringRef(Name);
  ExprResult Init = Semantics.ActOnDesignatedInitializer(
                                 makeDesignation("name"), Loc, true, InitValue);
  Initializers.push_back(Init.get());

  // flags
  unsigned Flags = vConstant;
  if (IniVal)
    Flags |= vInitialized;
  ExprResult Value = Semantics.ActOnIntegerConstant(Loc, Flags);
  ExprResult FlagsInit = Semantics.ActOnDesignatedInitializer(
                              makeDesignation("flags"), Loc, true, Value.get());
  Initializers.push_back(FlagsInit.get());

  if (IniVal) {
    // value
    InitValue = getBoxInitializer(IniVal);
    ExprResult ValueInitExpr = Semantics.ActOnDesignatedInitializer(
                                makeDesignation("value"), Loc, true, InitValue);
    Initializers.push_back(ValueInitExpr.get());
  }

  // Create initializer
  return Semantics.ActOnInitList(Loc, Initializers, Loc).get();
}

}
