//===--- SemaType.cpp ---- AST Builder and Semantic Analysis --------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Part of Semantics responsible for type manipulations.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/AST/ASTContext.h"
#include "clang/Sema/Template.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/RuntimeTypes.h"
//------------------------------------------------------------------------------


namespace phpfe {

using namespace clang;
using namespace llvm;


QualType Sema::getTempalteSpecializationType(SourceLocation Loc,
                                             ClassTemplateDecl *TDecl,
                                             QualType Ty) {
  TypeSourceInfo *TInfo = getTypeSourceInfo(Ty);
  TemplateArgumentListInfo TArgs(Loc, Loc);
  TArgs.addArgument(TemplateArgumentLoc(TemplateArgument(Ty), TInfo));

  return CheckTemplateIdType(TemplateName(TDecl), Loc, TArgs);
}


QualType Sema::getBoxedType(SourceLocation Loc, QualType Ty) {
  ClassTemplateDecl *TDecl = RuntimeTypes::getBoxedTemplateDecl();
  return getTempalteSpecializationType(Loc, TDecl, Ty);
}


QualType Sema::getRefBoxType(SourceLocation Loc, QualType Ty) {
  ClassTemplateDecl *TDecl = RuntimeTypes::getRefBoxTemplateDecl();
  return getTempalteSpecializationType(Loc, TDecl, Ty);
}


QualType Sema::getArgPackType(SourceLocation Loc, QualType Ty) {
  ClassTemplateDecl *TDecl = RuntimeTypes::getArgPackTemplateDecl();
  QualType SType = getTempalteSpecializationType(Loc, TDecl, Ty);
  return SType;
}


TypeSourceInfo *Sema::getTypeSourceInfo(QualType Ty) {
  auto Ptr = TypeSourceInfoPool.find(Ty);
  if (Ptr != TypeSourceInfoPool.end()) {
    assert(Ptr->second != nullptr);
    return Ptr->second;
  }
  TypeSourceInfo *NewInfo = Context.getTrivialTypeSourceInfo(Ty);
  TypeSourceInfoPool[Ty] = NewInfo;
  return NewInfo;
}


// class name: integer, ABC...
QualType Sema::ActOnPrimitiveTypeHint(const DeclarationNameInfo &Name) {
  std::string StrName = Name.getAsString();
  StringRef SName(StrName);
  if (SName.equals_lower("int") || SName.equals_lower("integer") ||
      SName.equals_lower("long"))
    return getIntValueType();
  if (SName.equals_lower("float") || SName.equals_lower("real") ||
      SName.equals_lower("double"))
    return Context.DoubleTy;
  if (SName.equals_lower("bool") || SName.equals_lower("boolean"))
    return Context.BoolTy;
  if (SName.equals_lower("string") || SName.equals_lower("binary"))
    return getStringBoxType();
  if (SName.equals_lower("resource"))
    return getResourceType();
  if (SName.equals_lower("object"))
    return getObjectType();
  if (SName.equals_lower("array"))
    return getArrayBoxType();
  if (SName.equals_lower("callable"))
    llvm_unreachable("callable type hint is not supported yet");
  if (SName.equals_lower("void"))
    return getVoidType();

  //TODO: handle class name
  return QualType();
}


// namespace class name
QualType Sema::ActOnClassTypeHint(CXXScopeSpec SS) {
  return getTypeForSpec(SS);
}


QualType Sema::ActOnDefaultTypeHint() {
  return RuntimeTypes::getClassBoxType();
}


QualType Sema::ActOnReferenceTypeHint(SourceLocation RefLoc, QualType Ty) {
  if (Ty->isVoidType())
    Diag(RefLoc, diag::err_php_void_reference);

  // Normalize base type: IArray->ArrayBox, string_ref->StringBox etc.
  if (isArrayType(Ty))
    Ty = getArrayBoxType();
  else if (isStringType(Ty))
    Ty = getStringBoxType();
  //TODO: object, callable

  return Context.getLValueReferenceType(Ty);
//TODO:  return getRefBoxType(RefLoc, Ty);
}


QualType Sema::ActOnReturnTypeHint(SourceLocation RefLoc, QualType Ty) {
  if (Ty.isNull())
    Ty = RuntimeTypes::getClassBoxType();

  if (RefLoc.isInvalid())
    return Ty;

  if (Ty->isVoidType())
    Diag(RefLoc, diag::err_php_void_reference);

  return getRefBoxType(RefLoc, Ty);
}


}
