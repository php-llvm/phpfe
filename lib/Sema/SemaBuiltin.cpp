//===--- SemaBuiltin.cpp ---- AST Builder and Semantic Analysis -----------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Semantic builtin handlers implementation.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Lex/Preprocessor.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Sema/Runtime.h"
//------------------------------------------------------------------------------


namespace phpfe {

using namespace clang;
using namespace llvm;

ExprResult semaRounding(Sema &S, const BuiltinInfo& FInfo, StringRef FuncName,
        SourceLocation LParenLoc, MultiExprArg Args, SourceLocation RParenLoc) {
  if (Args.empty())
    return ExprEmpty();

  //TODO: use proper name location
  SourceLocation NameLoc = Args[0]->getExprLoc();

  Expr *Arg = Args[0];
  QualType ArgType = Arg->getType();
  if (ArgType->isIntegerType()) {
    S.Diag(Arg->getExprLoc(), diag::warn_php_already_integer_argument)
      << Arg->getSourceRange();
    return S.ActOnCastExpr(NameLoc,
      S.getPreprocessor().getIdentifierInfo("double"), 
      NameLoc.getLocWithOffset(3), Arg);
  }
  return ExprEmpty();
}

}
