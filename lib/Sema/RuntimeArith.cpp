//===--- RuntimeArith.cpp ---------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
// 
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
// 
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Generates calls to runtime functions for doing arithmetic operations.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTContext.h"
#include "clang/AST/StmtPhp.h"
#include "clang/AST/ExprPhp.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "phpfe/Transformer/Transformer.h"
#include "phpfe/Sema/Runtime.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
using namespace llvm;


/// \brief Generates calls to runtime functions to perform assignment.
///
Expr *Runtime::generateAssign(Expr *Var, Expr *Value) {
  assert(Var);
  assert(Var->isLValue());

  QualType LHSType = Var->getType();
  QualType RHSType = Value->getType().getCanonicalType();

  if (LHSType == getStringBoxType()) {
    assert(isStringType(RHSType) || isUniversalType(RHSType));
  } else if (LHSType == getArrayBoxType()) {
    assert(isArrayType(RHSType) || isUniversalType(RHSType));
  } else {
    // Specific type assignments are handled by sema.
    isUniversalType(LHSType);
  }

  if (RHSType->isBooleanType()) {
    return MakeRuntimeMethodCall("set_bool", PlainBoxDecl, Var, Value);
  } if (RHSType->isIntegerType()) {
    return MakeRuntimeMethodCall("set_integer", PlainBoxDecl, Var, Value);
  } else if (RHSType->isFloatingType()) {
    return MakeRuntimeMethodCall("set_double", PlainBoxDecl, Var, Value);
  } else if (StringLiteral *StrLiteral = dyn_cast<StringLiteral>(Value)) {
    SmallVector<Expr *, 2> Args;
    Args.push_back(StrLiteral);
    Args.push_back(Semantics.ActOnIntegerConstant(Value->getLocStart(),
                                                StrLiteral->getLength()).get());
    return MakeRuntimeMethodCall("set_str", PlainBoxDecl, Var, Args);
  } else if (RHSType.getUnqualifiedType() == StringRefType) {
    return MakeRuntimeMethodCall("set_str", PlainBoxDecl, Var, Value);
  } else if (RHSType.getUnqualifiedType() == StringBoxType) {
    return MakeRuntimeMethodCall("set_str", PlainBoxDecl, Var, Value);
  } else if (RuntimeTypes::isBoxedType(RHSType)) {
    return MakeRuntimeMethodCall("copy_value", PlainBoxDecl, Var, Value);
  } else if (Runtime::isArrayType(RHSType)) {
    return MakeRuntimeMethodCall("set", PlainBoxDecl, Var, Value);
  } else if (Value->getType()->isPointerType()) {
    QualType Pointee = Value->getType()->getPointeeType();
    if (Pointee.isLocalConstQualified())
      Pointee.removeLocalConst();
    if (Runtime::isArrayType(Pointee))
      return MakeRuntimeMethodCall("set", PlainBoxDecl, Var, Value);
  }

  llvm_unreachable("unsupported RHS type");
  return nullptr;
}


/// \brief Generates calls to runtime functions to perform addition.
///
Expr *Runtime::generateAdd(Expr *LHS, Expr *RHS) {
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();
  SmallVector<Expr *, 2> Args;

  if (isArrayType(LHSType) && isArrayType(RHSType)) {
    if (isBoxedType(LHSType))
      LHS = generateGetArrayNode(LHS);
    if (isBoxedType(RHSType))
      RHS = generateGetArrayNode(RHS);
    Args.push_back(LHS);
    Args.push_back(RHS);
    return makeRuntimeFunctionCall("add_arrays", Args);
  }

  Args.push_back(LHS);
  Args.push_back(RHS);
  if (Semantics.getPhpOptions().CxxArith)
    return makeRuntimeFunctionCall("add_box_integer", Args);
  else
    return makeRuntimeFunctionCall("add_box", Args);
}


Expr *Runtime::generateAddAssign(Expr *LHS, Expr *RHS) {
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();
  SmallVector<Expr*, 2> Args;

  if (isArrayType(LHSType) && isArrayType(RHSType)) {
    if (isBoxedType(RHSType))
      RHS = generateGetArrayNode(RHS);
    Args.push_back(LHS);
    Args.push_back(RHS);
    return makeRuntimeFunctionCall("add_assign_arrays", Args);
  }

  Args.push_back(LHS);
  Args.push_back(RHS);
  if (Semantics.getPhpOptions().CxxArith)
    return makeRuntimeFunctionCall("add_assign_box_integer", Args);
  else
    return makeRuntimeFunctionCall("add_assign_box", Args);
}


/// \brief Generates calls to runtime functions to perform subtraction.
///
Expr *Runtime::generateSub(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  if (Semantics.getPhpOptions().CxxArith) {
    return makeRuntimeFunctionCall("sub_box_integer", Args);
  } else {
    return makeRuntimeFunctionCall("sub_box", Args);
  }
}


Expr *Runtime::generateSubAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr*, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  if (Semantics.getPhpOptions().CxxArith)
    return makeRuntimeFunctionCall("sub_assign_box_integer", Args);
  return makeRuntimeFunctionCall("sub_assign_box", Args);
}

  

/// \brief Generates calls to runtime functions to perform multiplication.
///
Expr *Runtime::generateMul(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  if (Semantics.getPhpOptions().CxxArith)
    return makeRuntimeFunctionCall("mul_box_integer", Args);

  return makeRuntimeFunctionCall("mul_box", Args);
}
  
  
Expr *Runtime::generateMulAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  if (Semantics.getPhpOptions().CxxArith)
    return makeRuntimeFunctionCall("mul_assign_box_integer", Args);
  
  return makeRuntimeFunctionCall("mul_assign_box", Args);
}


/// \brief Generates calls to runtime functions to perform division.
///
Expr *Runtime::generateDiv(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  if (Semantics.getPhpOptions().CxxArith)
    return makeRuntimeFunctionCall("div_box_integer", Args);

  return makeRuntimeFunctionCall("div_box", Args);
}
  
  
Expr *Runtime::generateDivAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  if (Semantics.getPhpOptions().CxxArith)
    return makeRuntimeFunctionCall("div_assign_box_integer", Args);
  
  return makeRuntimeFunctionCall("div_assign_box", Args);
}


/// \brief Generates calls to runtime functions to get remainder.
///
Expr *Runtime::generateRem(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("rem_box", Args);
}

  
Expr *Runtime::generateRemAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("rem_assign_box", Args);
}

  
  

/// \brief Generates calls to runtime functions to perform bitwise and.
///
Expr *Runtime::generateAnd(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();
  if (isStringType(LHSType) && isStringType(RHSType)) {
    Args.push_back(Runtime::isCStringType(LHSType)
      ? createStringRef(LHS)
      : generateGetStringNode(LHS));

    Args.push_back(Runtime::isCStringType(RHSType)
      ? createStringRef(RHS)
      : generateGetStringNode(RHS));
    
    return makeRuntimeFunctionCall("bitwise_and_string", Args);
  }
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("bitwise_and_box", Args);
}
  
  
Expr *Runtime::generateAndAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();
  if (isStringType(LHSType) && isStringType(RHSType)) {
    assert(LHSType == getStringBoxType());
    
    Args.push_back(LHS);
    Args.push_back(Runtime::isCStringType(RHSType)
                   ? createStringRef(RHS)
                   : generateGetStringNode(RHS));
    
    return makeRuntimeFunctionCall("bitwise_and_assign_string", Args);
  }
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("bitwise_and_assign_box", Args);
}


/// \brief Generates calls to runtime functions to perform bitwise or.
///
Expr *Runtime::generateOr(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();
  if (isStringType(LHSType) && isStringType(RHSType)) {
    Args.push_back(Runtime::isCStringType(LHSType)
      ? createStringRef(LHS)
      : generateGetStringNode(LHS));

    Args.push_back(Runtime::isCStringType(RHSType)
      ? createStringRef(RHS)
      : generateGetStringNode(RHS));

    return makeRuntimeFunctionCall("bitwise_or_string", Args);
  }
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("bitwise_or_box", Args);
}
  
  
Expr *Runtime::generateOrAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();
  if (isStringType(LHSType) && isStringType(RHSType)) {
    assert(LHSType == getStringBoxType());
    
    Args.push_back(LHS);
    Args.push_back(Runtime::isCStringType(RHSType)
                   ? createStringRef(RHS)
                   : generateGetStringNode(RHS));
    
    return makeRuntimeFunctionCall("bitwise_or_assign_string", Args);
  }
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("bitwise_or_assign_box", Args);
}
  


/// \brief Generates calls to runtime functions to perform bitwise xor.
///
Expr *Runtime::generateXor(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();
  if (isStringType(LHSType) && isStringType(RHSType)) {
    Args.push_back(Runtime::isCStringType(LHSType)
      ? createStringRef(LHS)
      : generateGetStringNode(LHS));

    Args.push_back(Runtime::isCStringType(RHSType)
      ? createStringRef(RHS)
      : generateGetStringNode(RHS));

    return makeRuntimeFunctionCall("bitwise_xor_string", Args);
  }
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("bitwise_xor_box", Args);
}
  
  
Expr *Runtime::generateXorAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  QualType LHSType = LHS->getType();
  QualType RHSType = RHS->getType();
  if (isStringType(LHSType) && isStringType(RHSType)) {
    assert(LHSType == getStringBoxType());
    
    Args.push_back(LHS);
    Args.push_back(Runtime::isCStringType(RHSType)
                   ? createStringRef(RHS)
                   : generateGetStringNode(RHS));
    
    return makeRuntimeFunctionCall("bitwise_xor_assign_string", Args);
  }
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("bitwise_xor_assign_box", Args);
}


/// \brief Generates calls to runtime functions to perform shift left.
///
Expr *Runtime::generateShl(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  
  if (isIntegerType(LHS->getType()) && isIntegerType(RHS->getType())) 
    return makeRuntimeFunctionCall("shl_integer", Args);
  
  return makeRuntimeFunctionCall("shl_box", Args);
}
  
  
Expr *Runtime::generateShlAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  
  if (isIntegerType(LHS->getType()) && isIntegerType(RHS->getType()))
    return makeRuntimeFunctionCall("shl_assign_integer", Args);
  
  return makeRuntimeFunctionCall("shl_assign_box", Args);
}


/// \brief Generates calls to runtime functions to perform shift right.
///
Expr *Runtime::generateShr(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("shr_box", Args);
}
  
  
Expr *Runtime::generateShrAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("shr_assign_box", Args);
}


/// \brief Generates calls to runtime functions to perform power operation.
///
Expr *Runtime::generatePow(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  if (isIntegerType(LHS->getType()) && isIntegerType(RHS->getType()))
    return makeRuntimeFunctionCall("pow_int", Args);
  if (isFloatingType(LHS->getType()) && isFloatingType(RHS->getType()))
    return makeRuntimeFunctionCall("pow_float", Args);
  return makeRuntimeFunctionCall("pow_generic", Args);
}
  
  
Expr *Runtime::generatePowAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  if (isIntegerType(LHS->getType())) {
    if (isBoxedType(RHS->getType()) && isIntegerType(RHS->getType()))
      RHS = generateGetIntegerNode(RHS);
    Args.push_back(RHS);
    return isIntegerType(RHS->getType()) && !isBoxedType(RHS->getType())
      ? makeRuntimeFunctionCall("pow_assign_int", Args)
      : makeRuntimeFunctionCall("pow_assign_int_box", Args);
  }
  
  if (isFloatingType(LHS->getType()))
    if (isBoxedType(RHS->getType()) && isFloatingType(RHS->getType()))
      RHS = generateGetDoubleNode(RHS);
    Args.push_back(RHS);
    return isFloatingType(RHS->getType()) && !isBoxedType(RHS->getType())
      ? makeRuntimeFunctionCall("pow_assign_float", Args)
      : makeRuntimeFunctionCall("pow_assign_float_box", Args);
  
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("pow_assign_generic", Args);
}


Expr *Runtime::generateCmp(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("compare", Args);
}


Expr *Runtime::generateIdent(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  return makeRuntimeFunctionCall("identical", Args);
}


Expr *Runtime::generateConcat(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  if (isStringType(LHS->getType()) && isStringType(RHS->getType()))
    return makeRuntimeFunctionCall("concat", Args);
  return makeRuntimeFunctionCall("concat_generic", Args);
}


Expr *Runtime::generateConcatAssign(Expr *LHS, Expr *RHS) {
  SmallVector<Expr *, 2> Args;
  Args.push_back(LHS);
  Args.push_back(RHS);
  if (isStringType(LHS->getType()))
    return makeRuntimeFunctionCall("concat_assign_str", Args);
  return makeRuntimeFunctionCall("concat_assign", Args);
}

/// \brief Generates calls to runtime function to check is 
/// expression is integer
///
Expr *Runtime::generateIsIntNode(Expr *Exp) {
  SmallVector<Expr *, 0> Args;
  return MakeRuntimeMethodCall("is_integer", PlainBoxDecl, Exp, Args);
}

Expr *Runtime::generateIsBoolNode(Expr *Exp) {
  SmallVector<Expr *, 0> Args;
  return MakeRuntimeMethodCall("is_bool", PlainBoxDecl, Exp, Args);
}

Expr *Runtime::generateIsFloatNode(Expr *Exp) {
  SmallVector<Expr *, 0> Args;
  return MakeRuntimeMethodCall("is_double", PlainBoxDecl, Exp, Args);
}

Expr *Runtime::generateIsNumericNode(Expr *Exp) {
  SmallVector<Expr *, 0> Args;
  return MakeRuntimeMethodCall("is_number", PlainBoxDecl, Exp, Args);
}

Expr *Runtime::generateIsScalarNode(Expr *Exp) {
  SmallVector<Expr *, 0> Args;
  return MakeRuntimeMethodCall("is_scalar", PlainBoxDecl, Exp, Args);
}

Expr *Runtime::generateIsArrayNode(Expr *Exp) {
  SmallVector<Expr *, 0> Args;
  return MakeRuntimeMethodCall("is_array", PlainBoxDecl, Exp, Args);
}

Expr *Runtime::generateIsStringNode(Expr *Exp) {
  SmallVector<Expr *, 0> Args;
  return MakeRuntimeMethodCall("is_string", PlainBoxDecl, Exp, Args);
}

Expr *Runtime::generateIsObjectNode(Expr *Exp) {
  SmallVector<Expr *, 0> Args;
  return MakeRuntimeMethodCall("is_object", PlainBoxDecl, Exp, Args);
}

Expr *Runtime::generateIsNullNode(Expr *Exp) {
  SmallVector<Expr *, 0> Args;
  return MakeRuntimeMethodCall("is_null", PlainBoxDecl, Exp, Args);
}
}
