//===--- SemaScope.cpp - AST Builder and Semantic Analysis Implementation -===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Functions for working with scopes.
//
//===----------------------------------------------------------------------===//

//------ Dependencies ----------------------------------------------------------
#include "phpfe/Sema/PhpSema.h"
#include "clang/Basic/PhpDiagnostic.h"
#include "clang/AST/ASTContext.h"
#include "clang/Sema/Lookup.h"

#include "Sema/TypeLocBuilder.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


/// \brief The parser has parsed a global nested-name-specifier '\'.
///
/// \param Loc The location of the '\'.
/// \param SS The nested-name-specifier, which will be updated in-place
///           to reflect the parsed nested-name-specifier.
/// \returns true if an error occurred, false otherwise.
///
bool Sema::ActOnPhpGlobalScopeSpecifier(SourceLocation Loc, CXXScopeSpec &SS) {
  assert(SS.isEmpty());
  SS.MakeGlobal(getASTContext(), Loc);
  return false;
}


/// \brief The parser has parsed a nested-name-specifier.
///
/// \param S The scope in which this nested-name-specifier occurs.
/// \param Id Class or namespace identifier.
/// \param IdLoc Identifier location.
/// \param BSLoc '\' or '::' location after namespace/class identifier.
/// \param SS The nested-name-specifier, which is both an input parameter (the
///            nested-name-specifier before this type) and an output parameter
///            (containing the full nested-name-specifier, including this
///            new type).
/// \param EnteringContext Whether we're entering the context of the
///                        nested-name-specifier.
/// \param IsClassName true if Id is a class name.
///
/// \returns true if an error occurred, false otherwise.
///
bool Sema::ActOnPhpNestedNameSpecifier(Scope *S, IdentifierInfo &Id,
                                       SourceLocation IdLoc,
                                       SourceLocation BSLoc,
                                       CXXScopeSpec &SS,
                                       bool EnteringContext,
                                       bool IsClassName) {
  if (SS.isInvalid())
    return true;
  LookupResult Previous(*this, &Id, IdLoc,
             IsClassName ? LookupNestedNameSpecifierName : LookupNamespaceName);

  // Determine where to perform the name lookup.
  DeclContext *LookupCtx = nullptr;

  if (SS.isSet()) {
    // The nested-name-specifier occurs after namespace specifier, so look into
    // the context associated with the previous namespace.
    LookupCtx = computeDeclContext(SS, false);
    Previous.setContextRange(SS.getRange());
  }

  if (!LookupCtx)
    // Perform name lookup in the current namespace.
    LookupCtx = CurNamespace;

  assert(LookupCtx);
  LookupQualifiedName(Previous, LookupCtx);

  if (Previous.getResultKind() == LookupResult::NotFound) {
    // If no such such namespace/class is found, probably this is a use before
    // declaration.  Create corresponding entity and use it.
    if (!IsClassName) {
      // Namespace
      NamespaceDecl *D = NamespaceDecl::Create(Context, LookupCtx,
                                               /* Inline */ false,
                                               /* StartLoc */ SourceLocation(),
                                               IdLoc, &Id,
                                               /* PrevDecl */ nullptr);
      LookupCtx->addDecl(D);
      SS.Extend(Context, D, IdLoc, BSLoc);
      return false;
    } else {
//      llvm_unreachable("not implemented");
    }
  } else if (Previous.getResultKind() == LookupResult::Found) {
    NamespaceDecl *D = Previous.getAsSingle<NamespaceDecl>();
    SS.Extend(Context, D, IdLoc, BSLoc);
    return false;
  } else {
    llvm_unreachable("wrong lookup");
  }

  return false;
}


bool Sema::ActOnSelfScopeSpecifier(SourceLocation IdLoc, SourceLocation CCLoc,
                                   DeclContext *CDecl, CXXScopeSpec &SS) {
  CXXRecordDecl *ClassDecl = cast<CXXRecordDecl>(CDecl);
  QualType T = Context.getTypeDeclType(cast<TypeDecl>(ClassDecl));
  TypeLocBuilder TLB;
  if (isa<RecordType>(T)) {
    clang::RecordTypeLoc RecordTL = TLB.push<RecordTypeLoc>(T);
    RecordTL.setNameLoc(IdLoc);
  } else {
    llvm_unreachable("Invalid node in nested-name-specifier");
  }
  SS.Extend(Context, SourceLocation(), TLB.getTypeLocInContext(Context, T),
            CCLoc);
  return false;
}


bool Sema::ActOnParentScopeSpecifier(SourceLocation IdLoc, SourceLocation CCLoc,
                                     DeclContext *CDecl, CXXScopeSpec &SS) {
  llvm_unreachable("unimplemented");
  return true;
}


bool Sema::ActOnStaticScopeSpecifier(SourceLocation IdLoc, SourceLocation CCLoc,
                                     DeclContext *CDecl, CXXScopeSpec &SS) {
  llvm_unreachable("unimplemented");
  return true;
}


void Sema::enterUnitFunction() {
  assert(getCurScope()->getEntity() == nullptr);
  assert(getCurScope()->getFlags() & Scope::FnScope);
  getCurScope()->setEntity(getUnitFunction());
  CurContext = getUnitFunction();
}


void Sema::leaveUnitFunction() {
  assert(getCurScope()->getFlags() == Scope::DeclScope);
  assert(getCurScope()->getEntity());
  assert(getCurScope()->getEntity()->isNamespace());
  CurContext = getCurScope()->getEntity();
}


Decl *Sema::ActOnNamespaceSwitch(SourceLocation KwLoc, CXXScopeSpec &SS) {
  assert(!SS.isEmpty());
  assert(getCurScope()->getEntity() == nullptr);
  assert(getCurScope()->getFlags() == Scope::DeclScope);

  Decl *Result = nullptr;
  if (areNamespacesBracketed()) {
    Diag(KwLoc, diag::err_php_mixed_namespaces);
    Diag(NamespaceStartLoc, diag::note_php_previous_namespace);
  } else {
    NamespaceStartLoc = KwLoc;
    setCurNamespace(SS.getScopeRep()->getAsNamespace(), KwLoc, SourceLocation());
    Result = CurNamespace;
  }
  getCurScope()->setEntity(CurNamespace);

  return Result;
}


Decl *Sema::ActOnNamespaceStart(SourceLocation KwLoc, CXXScopeSpec &SS,
                                SourceLocation LBrace) {
  assert(getCurScope()->getFlags() == Scope::DeclScope);
  assert(!isInNamedNamespace());
  assert(!areNamespacesSimple());

  NamespaceDecl *Result = SS.isEmpty() ? PhpNamespace
                                       : SS.getScopeRep()->getAsNamespace();
  setCurNamespace(Result, KwLoc, LBrace);

  if (SS.isEmpty() ||
      SS.getScopeRep()->getKind() == NestedNameSpecifier::Global) {
    // We are setting global namespace.
    assert(getCurScope()->getEntity());
    assert(getCurScope()->getEntity() == PhpNamespace);
  } else {
    assert(getCurScope()->getEntity() == nullptr);
    getCurScope()->setEntity(CurNamespace);
  }
  return Result;
}


void Sema::ActOnNamespaceEnd(SourceLocation RBrace) {
  setCurNamespace(PhpNamespace, NamespaceStartLoc, NamespaceLBrace);
  CurContext = PhpNamespace;
}


/// \brief Given a scope returns class the scope is embedded into.
///
/// \note: a function can be defined inside a method, but this function is
/// outside any class.
///
CXXRecordDecl *getCurrentClassContext(Scope *&S) {
  // Find closest declaration scope that is associated with some declaration
  // context. Statements like if/while etc define declaration scopes, but they
  // are not associated with a context.
  while (S) {
    if ((S->getFlags() & Scope::DeclScope) && S->getEntity())
      break;
    S = S->getParent();
  }
  assert(S);
  assert(S->getEntity());

  // Valid scopes are function scope (found in the method body) and class
  // scope (initializers of constant and properties are there, default values
  // of function parameters also).
  bool InsideClass = false;
  if (S->isClassScope())
    InsideClass = true;
  else if (S->getFlags() & Scope::FnScope) {
    Scope *ParentScope = S->getParent();
    if (ParentScope->isClassScope())
      InsideClass = true;
  }
  if (!InsideClass)
    return nullptr;

  // Get context of the relevant class.
  DeclContext *DC = S->getEntity();
  DeclContext *ClassDC = nullptr;
  if (DC->isFunctionOrMethod())
    ClassDC = DC->getParent();
  else
    // TODO: check lambda
    ClassDC = DC;
  assert(ClassDC);
  assert(ClassDC->isRecord());

  return cast<CXXRecordDecl>(ClassDC);
}

}
