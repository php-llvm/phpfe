//===--- Runtime.h ------- Interface with PHP runtime -----------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Defines interface of PHP compiler to runtime.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_SEMA_RUNTIME_H
#define PHPFE_SEMA_RUNTIME_H

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ExprCXX.h"
#include "phpfe/Sema/RuntimeTypes.h"
#include "phpfe/Sema/PhpSema.h"
#include "llvm/ADT/APInt.h"
#include <map>
//------------------------------------------------------------------------------

namespace phpfe {

using namespace llvm;
using namespace clang;

struct BuiltinInfo;


class Runtime : public RuntimeTypes {
protected:
  // Object state.
  Sema &Semantics;

  bool loadBuiltinFunctions();
  void initSection(StringRef SectionName, int Flags);

  Designation &makeDesignation(StringRef D);

  typedef VarDecl *handler;
  bool addIntegerInitializer(handler H, StringRef Desig, int Value);
  bool addStringInitializer(handler H, StringRef Desig, StringRef Value);
  bool addExprInitializer(handler H, StringRef Desig, Expr * Val);
  bool addStringRefInitializer(handler H, StringRef Desig, StringRef Value);
  bool addFunctionInitializer(handler H, StringRef Desig, FunctionDecl *EH);
  bool addVarPtrInitializer(handler H, StringRef Desig, VarDecl *VD);
  VarDecl *doneVariable(handler H);

  VarDecl *createVariable(StringRef VarName, DeclContext *DC,
                          TypeSourceInfo *TInfo, SourceLocation Loc,
                          bool IsStatic);
  void placeInSection(ValueDecl *D, StringRef SectionName);
  Expr *getVarAddr(VarDecl *VD);

public:

  Runtime(Sema &S);

  void initialize();

  //------ Calls to runtime

  FunctionDecl *findRuntimeFunction(StringRef FName);
  void fitCallArguments(FunctionDecl * FD, MutableArrayRef<Expr*> Args);
  Expr *makeRuntimeFunctionCall(StringRef FName, MutableArrayRef<Expr *> Args);
  Expr *makeFunctionCall(Expr *FnName, ArrayRef<Expr *> Args);
  Expr *makeRuntimeStaticMethodCall(StringRef MName, CXXRecordDecl *Class, MutableArrayRef<Expr*> Args);
  Expr *MakeRuntimeMethodCall(StringRef MName, CXXRecordDecl *Class,
                              Expr *Base, MultiExprArg Args);
  Expr *MakeStaticMethodCall(StringRef MName, CXXRecordDecl *Class, 
                             MultiExprArg Args, SourceLocation Loc);

  //------ Types
  //

  // templates

  static bool isBoxedType(QualType x);
  // Type checks.
  static bool isRealType(QualType x) {
    if (x->isRealType())
      return true;
    QualType SpecTy = getParameterOfSpecialization(x);
    return !SpecTy.isNull() && SpecTy->isRealType();
  }

  //------ Building Box constructor call
  Expr *createNullBox(SourceLocation Loc);
  Expr *createStringBox(Expr *E);
  Expr *createArrayBox(Expr *E);
  Expr *createExprBox(Expr *E);
  Expr *castToClassBox(Expr *E);
  Expr *castToArrayBox(Expr *E);

  //------ Building ArgPack
  Expr *createConstArrayContent(QualType BaseType, InitListExpr *Init);
  Expr *createVariadicArgument(SourceLocation Loc, QualType BaseType, MutableArrayRef<Expr*> Args);

  //------ Building string_ref
  Expr *createStringRef(Expr *E);
  Expr *createStringRefFromLiteral(StringLiteral *Str);
  Expr *createStringRefFromCharArray(Expr *E);

  //------ Building const array
  VarDecl *createConstArrayContentVariable(QualType BaseType, InitListExpr *Init);
  VarDecl *createConstArrayLocatorsVariable(InitListExpr *Init);
  Expr *createAssosiationInitializer(SourceLocation Loc, ArrayItem Item);
  

  //------ Building variables
  //
  // To build a variable of a type known to the runtime, a method 'createXXX' is
  // called first. It returns a handler that references a variable in the
  // runtime interface. The various methods 'addXXXYYY' are used to add an
  // initializer to the variable. Finally a method 'doneXXX' finishes
  // initialization and returns the built variable.

  struct VarDescriptor {
    VarDecl *Var;
    SmallVector<Expr *, 8> Initializers;
  };
  std::map<handler, VarDescriptor> PendingVars;

  // UnitRecord.
  handler  createUnitRecord(FunctionDecl *UnitFunc);
  bool     addUnitFlags(handler H, unsigned Flags);
  bool     addUnitName(handler H, StringRef Name);
  bool     addUnitVersion(handler H, StringRef Name);
  bool     addUnitInitHandler(handler H, FunctionDecl *EH);
  bool     addUnitShutdownHandler(handler H, FunctionDecl *EH);
  bool     addUnitLoadHandler(handler H, FunctionDecl *EH);
  bool     addUnitUnloadHandler(handler H, FunctionDecl *EH);
  bool     addUnitExecuteHandler(handler H, FunctionDecl *EH);
  VarDecl *doneUnitRecord(handler h);

  // UnitState.
  handler  createUnitState(SourceLocation Loc, StringRef UnitName);
  bool     addUnitStateFlags(handler H, unsigned Flags);
  bool     addUnitStateUnit(handler H, VarDecl *UnitRecord);
  VarDecl *doneUnitStateRecord(handler H);

  // Global variables.
  void      adjustGlobalVariableAttrs(VarDecl *Var);
  VarDecl  *createGlobalVariableRecord(VarDecl *Var);

  Expr *getInitStringRef(StringLiteral *Val);

  // box initializers.
  Expr *getInitUndefined(SourceLocation Loc);
  Expr *getInitNull(SourceLocation Loc);
  Expr *getInitBoolean(CXXBoolLiteralExpr *Val);
  Expr *getInitInteger(IntegerLiteral *Val);
  Expr *getInitFloat(FloatingLiteral *Val);
  Expr *getInitString(StringLiteral *Val);
  Expr *getInitBoolean(SourceLocation Loc, bool Val);
  Expr *getInitInteger(SourceLocation Loc, int64_t Val);
  Expr *getInitFloat(SourceLocation Loc, double Val);
  Expr *getInitString(SourceLocation Loc, StringRef Val);
  Expr *getBoxInitializer(Expr *Val);
  Expr *createBoxInitializer(SourceLocation Loc, const ConstValue *Val);

  // ValueInfo initializers.
  Expr *getInitCTConstant(VarDecl *V, Expr *IniVal);

  const char *getUnitSectionName();
  const char *getUnitStateSectionName();
  const char *getGVRSectionName();
  const char *getCTConstantSectionName();
  const char *getRTConstantSectionName();

  //------ Building constants

  // ConstArray.
  handler  createConstArray(SourceLocation Loc);
  bool     addArrayContent(handler H, Expr *Content, APInt Size, APInt Start);
  bool     addArrayContent(handler H, Expr *Content, APInt Size, Expr *Locators);
  VarDecl *doneConstArray(handler H);

  // Runtime constant representation.
  VarDecl  *createConstant(DeclContext *DC, const DeclarationNameInfo &Name,
                           Expr *Init);
  void initConstant(VarDecl *Var, Expr *IniVal);
  Expr *generateRtConstantUse(StringRef Name, SourceLocation Loc,
                              StringRef NsName);
  Expr *generateGlobalRtConstantUse(VarDecl *VD);

  //------ Intrinsic functions support
  BuiltinInfo *findBuiltinFunction(StringRef);

  //------ Expression operators.
  Expr *generateAssign(Expr *Var, Expr *Value);
  Expr *generateAdd(Expr *LHS, Expr *RHS);
  Expr *generateAddAssign(Expr *LHS, Expr *RHS);
  Expr *generateSub(Expr *LHS, Expr *RHS);
  Expr *generateSubAssign(Expr *LHS, Expr *RHS);
  Expr *generateMul(Expr *LHS, Expr *RHS);
  Expr *generateMulAssign(Expr *LHS, Expr *RHS);
  Expr *generateDiv(Expr *LHS, Expr *RHS);
  Expr *generateDivAssign(Expr *LHS, Expr *RHS);
  Expr *generateRem(Expr *LHS, Expr *RHS);
  Expr *generateRemAssign(Expr *LHS, Expr *RHS);
  Expr *generateAnd(Expr *LHS, Expr *RHS);
  Expr *generateAndAssign(Expr *LHS, Expr *RHS);
  Expr *generateOr(Expr *LHS, Expr *RHS);
  Expr *generateOrAssign(Expr *LHS, Expr *RHS);
  Expr *generateXor(Expr *LHS, Expr *RHS);
  Expr *generateXorAssign(Expr *LHS, Expr *RHS);
  Expr *generateShl(Expr *LHS, Expr *RHS);
  Expr *generateShlAssign(Expr *LHS, Expr *RHS);
  Expr *generateShr(Expr *LHS, Expr *RHS);
  Expr *generateShrAssign(Expr *LHS, Expr *RHS);
  Expr *generatePow(Expr *LHS, Expr *RHS);
  Expr *generatePowAssign(Expr *LHS, Expr *RHS);
  Expr *generateCmp(Expr *LHS, Expr *RHS);
  Expr *generateIdent(Expr *LHS, Expr *RHS);
  Expr *generateConcat(Expr *LHS, Expr *RHS);
  Expr *generateConcatAssign(Expr *LHS, Expr *RHS);

  // ------ Array operations
  Expr *generateSubscriptRead(Expr *Arr, Expr *Key);

  // ------ is_* functions
  Expr *generateIsIntNode(Expr *Exp);
  Expr *generateIsBoolNode(Expr *Exp);
  Expr *generateIsFloatNode(Expr *Exp);
  Expr *generateIsNumericNode(Expr *Exp);
  Expr *generateIsScalarNode(Expr *Exp);
  Expr *generateIsArrayNode(Expr *Exp);
  Expr *generateIsStringNode(Expr *Exp);
  Expr *generateIsObjectNode(Expr *Exp);
  Expr *generateIsNullNode(Expr *Exp);

  // ------ Query methods
  Expr *generateGetArrayNode(Expr *Exp);
  Expr *generateGetIntegerNode(Expr *Exp);
  Expr *generateGetDoubleNode(Expr *Exp);
  Expr *generateGetStringNode(Expr *Exp);

  // ------ Conversion methods
  Expr *generateToBoolNode(Expr *Exp);
  Expr *generateToIntegerNode(Expr *Exp);
  Expr *generateToDoubleNode(Expr *Exp);
  Expr *generateToNumberNode(Expr *Exp);
  Expr *generateToStringNode(Expr *Exp);
  Expr *generateToArrayNode(Expr *Exp);
  Expr *generateToObjectNode(Expr *Exp);

  Expr *generateClearNode(Expr *Exp);
  Expr *generateConvertToBoolNode(Expr *Exp);
  Expr *generateConvertToIntegerNode(Expr *Exp);
  Expr *generateConvertToDoubleNode(Expr *Exp);
  Expr *generateConvertToStringNode(Expr *Exp);
  Expr *generateConvertToArrayNode(Expr *Exp);
  Expr *generateConvertToObjectNode(Expr *Exp);
};

}
#endif
