//===--- PhpSema.h - Semantic Analysis & AST Building -----------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// This file defines the Sema class, which performs semantic analysis and
// builds ASTs for PHP compilations.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_SEMA_PHPSEMA_H
#define PHPFE_SEMA_PHPSEMA_H

//------ Dependencies ----------------------------------------------------------
#include "clang/Sema/Sema.h"
#include "clang/Frontend/PhpOptions.h"
#include "phpfe/Sema/RuntimeTypes.h"
#include "phpfe/Support/ConstValue.h"
#include <map>
#include <vector>
//------------------------------------------------------------------------------

namespace phpfe {

// Constants
const std::string ConstNamespaceName = "__Const";
const std::string FuncNamespaceName  = "__Func";
const std::string ClassNamespaceName = "__Class";
const std::string VarNamespaceName   = "var";


using namespace clang;

class Runtime;
struct BuiltinInfo;

/// \brief Provides semantic actions for parser.
///
/// PHP semantic may be used for compiling C++ files. Generally we have three
/// possible cases:
/// - C++ semantics and C++ source, this is normal case for clang,
/// - PHP semantics and PHP source, this is 'clang -php' called for PHP source,
/// - PHP semantics and C++ source. It happens at initial state of PHP source
///   compilation, when necessary C++ object are loaded from C++ headers. It is
///   also possible to use this compilation mode to generate PHP runtime
///   information for C++ entities.
///
class Sema : public clang::Sema, public RuntimeTypes {
  std::unique_ptr<PhpOptions> PhpOpts;
  std::unique_ptr<Runtime> RT;

public:
  PhpOptions &getPhpOptions() { return *PhpOpts.get(); }
  Runtime &getRuntime() { assert(RT);  return *RT; }

  //------ Runtime interface support
private:

  NamespaceDecl *RTNamespace;

  void loadRuntimeTypes();

public:

  QualType loadType(StringRef TypeName, NamedDecl **D, DeclContext *DC);
  QualType loadCType(StringRef TypeName, NamedDecl **D);
  QualType loadRTType(StringRef TypeName, NamedDecl **D);
  ClassTemplateDecl *loadTemplateType(StringRef TName, DeclContext *DC);

  NamespaceDecl *getRTNamespace() const { return RTNamespace; }

  //------ Unit support

  FunctionDecl *UnitFunction;
  VarDecl *UnitRecord;
  VarDecl *UnitStateVar;
  std::vector<Stmt*> UnitStatements;
  std::vector<Decl*> UnitDeclarations;
  SourceLocation UnitStartLoc;

  FunctionDecl *createUnitFunction(StringRef FileName, SourceLocation Loc);
  void setupUnitFunction(SourceLocation Loc);
  bool isUnitFunctionContext() { return CurContext == UnitFunction; }

public:
  bool  ActOnUnitStart(SourceLocation StartLoc);
  Decl *ActOnUnitFinish(SourceLocation EndLoc);
  void enterUnitFunction();
  void leaveUnitFunction();
  void addUnitDeclaration(Decl *D);

  FunctionDecl *getUnitFunction() const { return UnitFunction; }
  DeclGroupPtrTy getUnitDeclarations();

  //------ Namespace support

private:
  // Namespaces for PHP entities. They do not depend on the current namespace.
  NamespaceDecl *PhpNamespace;
  NamespaceDecl *VarNamespace;

  // Namespaces for PHP entities, they are embedded into current namespace.
  NamespaceDecl *FuncNamespace;
  NamespaceDecl *ConstNamespace;
  NamespaceDecl *ClassNamespace;

  Scope *GlobalNamespaceScope;

  // User namespace

  /// \brief Current PHP namespace or null it is anonymous.
  NamespaceDecl *CurNamespace;

  SourceLocation NamespaceStartLoc;
  SourceLocation NamespaceLBrace;

  void setCurNamespace(NamespaceDecl *NS, SourceLocation NSLoc,
                       SourceLocation LBrLoc);

public:
  NamespaceDecl *getPhpNamespace() const { return PhpNamespace; }
  NamespaceDecl *getVarNamespace() const { return VarNamespace; }
  NamespaceDecl *getFuncNamespace(NamespaceDecl *Host = nullptr);
  NamespaceDecl *getConstNamespace(NamespaceDecl *Host = nullptr);
  NamespaceDecl *getClassNamespace();

  NamespaceDecl *findNamespace(StringRef Name, NamespaceDecl *Owner = nullptr);
  NamespaceDecl *createNamespace(StringRef Name, NamespaceDecl *Owner = nullptr);

  Scope *getGlobalNamespaceScope() const { return GlobalNamespaceScope; }

  SourceLocation getNamespaceStartLoc() const { return NamespaceStartLoc; }
  bool areNamespacesBracketed() const { return NamespaceLBrace.isValid(); }
  bool areNamespacesSimple() const {
    return NamespaceStartLoc.isValid() && !NamespaceLBrace.isValid();
  }
  bool isInNamedNamespace() const { return CurNamespace != PhpNamespace; }
  bool isInGlobalNamespace(Decl *D);
  bool isPhpNamespace(NamespaceDecl *NS);
  bool isInNamespace(Decl *D1, NamespaceDecl *NS);
  bool isPhpFunction(FunctionDecl *FD);
  bool isPhpConstant(VarDecl *FD);
  bool isPhpVariable(VarDecl *FD);
  bool isPhpClass(CXXRecordDecl *CD);

public:
  Sema(Preprocessor &pp, ASTContext &ctxt, ASTConsumer &consumer,
       PhpOptions &POpts, TranslationUnitKind TUKind = TU_Complete,
       CodeCompleteConsumer *CompletionConsumer = nullptr);
  ~Sema() override;

  void Initialize() override;

  PresumedLoc getPresumedLocForSourceLoc(SourceLocation Loc);

  void handlePostponedDeclarations();

  //------ Declarations

  // Keeps information about function being parsed. By essence it is parser
  // structure but we define it in Sema because Sema action gets all information
  // about function in this pack.
  struct FunctionParseInfo {
    SourceLocation StaticLoc;     // static                             [lambda]
    SourceLocation FnLoc;         // function
    SourceLocation RefLoc;        // function &
    DeclarationNameInfo Name;     // function &abc
    SourceLocation LParen;        // function &abc(
    SmallVector<ParmVarDecl*, 16> Params;   //  function &abc(int $a
    SourceLocation RParen;        // function &abc(int $a)
    SourceLocation UseLoc;        // function &abc(int $a) use          [lambda]
    SourceLocation ColonLoc;      // function &abc(int $a) :
    QualType ReturnTypeHint;      // function &abc(int $a) : string
  };

  QualType ActOnPrimitiveTypeHint(const DeclarationNameInfo &Name);
  QualType ActOnClassTypeHint(CXXScopeSpec SS);
  QualType ActOnDefaultTypeHint();
  QualType ActOnReferenceTypeHint(SourceLocation RefLoc, QualType Ty);
  QualType ActOnReturnTypeHint(SourceLocation RefLoc, QualType Ty);
  ParmVarDecl *ActOnFunctionParam(Scope *S, SourceLocation Loc, 
        SourceLocation EllipsisLoc, QualType TypeHint, IdentifierInfo *Name);
  bool ActOnParamDefaultValue(SourceLocation AssignLoc, ParmVarDecl *Param,
                              Expr *E);
  FunctionDecl *ActOnFunctionDeclaration(Scope *S,
                                         FunctionParseInfo &FInfo);
  void ActOnFunctionDefinitionStart(FunctionDecl *FD);
  void ActOnFunctionDefinitionEnd();
  StmtResult ActOnFunctionDefinition(FunctionDecl *FDecl, Stmt *Body);

  CXXRecordDecl *ActOnClosureDefinitionStart(SourceLocation Loc);
  FunctionDecl *ActOnClosureDeclaration(FunctionParseInfo &FInfo);
  ExprResult ActOnClosureDefinitionEnd();
  bool ActOnClosureVariableUse(SourceLocation RefLoc, 
                               SourceLocation NameLoc, IdentifierInfo *II);

  bool isBuiltinConstant(StringRef Name);
  ExprResult getBuiltinConstant(StringRef Name, SourceLocation Loc);
  VarDecl *createConstant(DeclContext *DC, const DeclarationNameInfo &Name,
                          Expr *Init);
  VarDecl *ActOnConstantDefinition(DeclarationNameInfo Name,
                                   SourceLocation EqLoc, Expr *Init);
  StmtResult ActOnConstantDefStatement(SourceLocation ConstLoc,
                          MutableArrayRef<Decl *> Decls, SourceLocation EndLoc);
  VarDecl *createForwardConstantDecl(DeclarationNameInfo Name,
                                     NamespaceDecl *NS);
  ExprResult createRuntimeConstantUse(DeclarationNameInfo Name,
                                      NamespaceDecl *CurNS);
  ExprResult ActOnConstantUse(CXXScopeSpec SS, IdentifierInfo *Name,
                              SourceLocation Loc);

  //------ Statements

  void ActOnTopStatement(StmtResult S);
  StmtResult ActOnEchoStmt(SourceLocation Loc, ArrayRef<Expr*> Exprs);
  StmtResult ActOnPHPIfStmt(SourceLocation IfLoc, Expr *Cond, Stmt *Then);
  StmtResult ActOnPhpWhileStmt(SourceLocation WhileLoc, SourceLocation LParen,
                               Expr *Cond, SourceLocation RParen, Stmt *Body);
  StmtResult ActOnAlternativePhpWhileStmt(SourceLocation WhileLoc,
      SourceLocation LParen, Expr *Cond, SourceLocation RParen,
      SourceLocation ColonLoc, ArrayRef<Stmt*> WhileStmts,
      SourceLocation EndWhile);
  StmtResult ActOnPHPElseStmt(Stmt *If, SourceLocation ElseLoc, Stmt *Else);
  StmtResult ActOnHtmlData(SourceLocation StartLoc, StringRef Text);
  StmtResult ActOnCompoundStmt(SourceLocation L, SourceLocation R,
                               ArrayRef<Stmt *> Elts, bool isStmtExpr);
  StmtResult ActOnReturnStmt(SourceLocation Loc, Expr *RetVal);
  StmtResult ActOnStaticAssert(SourceLocation Loc,
                               Expr *E, StringLiteral *Message,
                               SourceLocation RPLoc);

  //------ Expressions

  // Binary operators
  ExprResult ActOnBinaryOp(SourceLocation Loc, tok::TokenKind Kind, Expr *LHS,
                           Expr *RHS);

  ExprResult ActOnNumericLiteral(SourceLocation Loc, StringRef Spelling,
                                 bool Negative);
  ExprResult ActOnIntegerConstant(SourceLocation Loc, uint64_t Val);
  ExprResult ActOnSubscriptExpression(Expr *Base, SourceLocation Left,
                                      Expr *Index, SourceLocation Right,
                                      bool LHS);
  ExprResult ActOnCallArgument(SourceLocation EllipsisLoc, Expr *Arg);
  ExprResult ActOnCallExpression(Expr *Calle,
              SourceLocation LB, ArrayRef<Expr *> Arguments, SourceLocation RB);
  ExprResult ActOnCallExpression(CXXScopeSpec SS, SourceLocation Loc,
                                 DeclarationName Name, SourceLocation LPLoc,
                                 MutableArrayRef<Expr*> Args, SourceLocation RPLoc);
  ExprResult ActOnPHPArrayExpr(SourceLocation KwLoc, SourceLocation LBLoc,
                               ArrayRef<Expr*> Elements, SourceLocation RBLoc);
  ExprResult ActOnPHPClassConstant(CXXScopeSpec SS, SourceLocation LBLoc, Expr *Name, SourceLocation RBLoc);
  ExprResult ActOnConditionalOp(Expr *CondExpr, SourceLocation QueryLoc,
                                Expr *TrueExpr, SourceLocation ColonLoc,
                                Expr *FalseExpr);
  ExprResult ActOnPHPIncludeExpr(SourceLocation Loc, tok::TokenKind Kind, ExprResult FileName);
  ExprResult ActOnPHPInstanceOf(SourceLocation Loc, ExprResult Base, CXXScopeSpec SS, ExprResult Name);
  ExprResult ActOnPHPMapExpr(SourceLocation Loc, ExprResult Id, ExprResult Val);
  ExprResult ActOnPHPMemberAccessExpr(Expr *Base, SourceLocation ArrowLoc, SourceLocation Loc, DeclarationName DN);
  ExprResult ActOnPHPMemberAccessExpr(Expr *Base, SourceLocation ArrowLoc, SourceLocation LBLoc, Expr *Name, SourceLocation RBLoc);
  ExprResult ActOnPHPNewExpression(SourceLocation Loc, CXXScopeSpec SS, ExprResult ClassName, ArrayRef<Expr*> Args);
  ExprResult ActOnPHPNullLiteral(SourceLocation Loc);
  ExprResult ActOnPostfixUnaryOp(SourceLocation OpLoc, tok::TokenKind Kind, ExprResult Base);
  ExprResult ActOnPrefixUnaryOp(SourceLocation Loc, tok::TokenKind Kind,
                                Expr *SubExpr);
  ExprResult ActOnCastExpr(SourceLocation LPLoc, IdentifierInfo *CastName, SourceLocation RPLoc, Expr *SubExpr);
  ExprResult ActOnPHPReferenceAccess(SourceLocation Loc, ExprResult Arg, bool IsVariable);
  ExprResult ActOnPHPSubscript(Expr *Base, SourceLocation LPLoc, Expr *Subscript, SourceLocation RPLoc);
  ExprResult ActOnVariableUse(SourceLocation Loc, ExprResult Name);
  ExprResult ActOnVariableUse(Scope *S, DeclarationNameInfo Name);
  ExprResult ActOnPHPClassVariableUse(CXXScopeSpec SS, SourceLocation Loc, ExprResult Name);
  ExprResult ActOnPHPClassVariableUse(CXXScopeSpec SS, SourceLocation Loc, DeclarationName Name);
  ExprResult ActOnPHPClassVariableUse(ExprResult Base, SourceLocation Loc, ExprResult Name);
  ExprResult ActOnPHPClassVariableUse(ExprResult Base, SourceLocation Loc, DeclarationName Name);
  ExprResult ActOnPHPObjectVariableUse(ExprResult Base, SourceLocation Loc, ExprResult Name);
  ExprResult ActOnPHPObjectVariableUse(ExprResult Base, SourceLocation Loc, DeclarationName Name);

  bool ActOnPhpGlobalScopeSpecifier(SourceLocation Loc, CXXScopeSpec &SS);
  bool ActOnSelfScopeSpecifier(SourceLocation IdLoc, SourceLocation CCLoc,
                               DeclContext *CDecl, CXXScopeSpec &SS);
  bool ActOnParentScopeSpecifier(SourceLocation IdLoc, SourceLocation CCLoc,
                                 DeclContext *CDecl, CXXScopeSpec &SS);
  bool ActOnStaticScopeSpecifier(SourceLocation IdLoc, SourceLocation CCLoc,
                                 DeclContext *CDecl, CXXScopeSpec &SS);
  bool ActOnPhpNestedNameSpecifier(Scope *S, IdentifierInfo &Id,
                                   SourceLocation IdLoc, SourceLocation BSLoc,
                                   CXXScopeSpec &SS, bool EnteringContext,
                                   bool IsClassName);

  Decl *ActOnNamespaceSwitch(SourceLocation KwLoc, CXXScopeSpec &SS);
  Decl *ActOnNamespaceStart (SourceLocation KwLoc, CXXScopeSpec &SS,
                             SourceLocation LBrace);
  void ActOnNamespaceEnd    (SourceLocation RBrace);


  QualType getTypeForSpec(CXXScopeSpec SS);
  DeclContext *getContextForScopeSpec(CXXScopeSpec SS);

  bool IsClassNestedScopeSpecifier(CXXScopeSpec SS);
  static std::string getNamespaceAsString(NamespaceDecl *NS);

  static bool isAssignmentOperator(tok::TokenKind Kind);

  // Helper methods
  StringLiteral *BuildStringLiteral(StringRef Str, SourceLocation StrLoc);
  FloatingLiteral *BuildDoubleLiteral(double Val, SourceLocation Loc);
  Expr *BuildUnresolvedCall(StringRef Name, SourceLocation NameLoc,
                       QualType RetTy, MultiExprArg Args, SourceLocation RPLoc);
  Expr *buildFieldAccess(StringRef FieldName, CXXRecordDecl *CD, Expr *Base);

  FunctionDecl *BuildFunctionForwardDeclaration(CXXScopeSpec SS, 
                                                SourceLocation Loc,
                                                DeclarationName Name);
  QualType BuildFunctionType(QualType ReturnType, ArrayRef<QualType> ArgTypes);
  Stmt *BuildDefaultReturnStmt(QualType RetTy, SourceLocation Loc);

  Expr *BuildArrayInitVariable(SourceLocation Loc, const ArrayConstValue &Val);

  bool checkTypeVsDeclaration(Expr *E, QualType T, bool IsReturn = false);
  bool checkFunctionCallArguments(SourceLocation Loc, FunctionDecl *FD,
                                  ArrayRef<Expr*> Args);

  // Tries to treat the function call as a call to builtin function. If the
  // call is indeed such, returns expression, that represents the call (in
  // general, it may be any expression, not only CallExpr). If it returns
  // ExprEmpty, the function is not recognized as builtin. If returns ExprError,
  // the function is builtin but the call is wrong.
  ExprResult  processBuiltinCall(StringRef FuncName, SourceLocation NameLoc,
                                 SourceLocation LParenLoc, MultiExprArg ArgExprs,
                                 SourceLocation RParenLoc);

  ExprResult handleDefaulBuiltinCall(const BuiltinInfo &BI, 
                                StringRef FuncName, SourceLocation NameLoc,
                                SourceLocation LParenLoc, MultiExprArg ArgExprs,
                                SourceLocation RParenLoc);

  // Work with types.
  //
private:

  struct CmpQualTypes {
    bool operator()(QualType x1, QualType x2) const {
      return x1.getAsOpaquePtr() < x2.getAsOpaquePtr();
    }
  };
  std::map<QualType, TypeSourceInfo*, CmpQualTypes> TypeSourceInfoPool;
  bool StrictTypes;   // if false allows implicit casts to match argument type

public:

  QualType getTempalteSpecializationType(SourceLocation Loc,
                                         ClassTemplateDecl *TDecl, QualType Ty);
  QualType getBoxedType(SourceLocation Loc, QualType Ty);
  QualType getRefBoxType(SourceLocation Loc, QualType Ty);
  QualType getArgPackType(SourceLocation Loc, QualType Ty);

  TypeSourceInfo *getTypeSourceInfo(QualType);

  QualType BuildGenericType(QualType t1, QualType t2);

  enum TypeCompare {
    TC_strict,            // argument have correct type in a strict sense
    TC_value_dependent,   // depending on value argument might be compatible (e.g. "123" -> int)
    TC_weak,              // argument have correct type but require casting
    TC_incompatible       // argument is incompatible with requested type
  };
  TypeCompare comparePHPTypes(QualType ParamType, QualType ArgType);
  std::string getPHPTypeName(QualType Ty);
  bool isStrictTypes() const { return StrictTypes; }

  // Search
  QualType findType(StringRef TypeName, NamedDecl **D, DeclContext *DC);
  ClassTemplateDecl *findTemplate(StringRef TypeName, DeclContext *DC);

  // PHP lookup
  VarDecl *findConstant(Scope *S, const DeclarationNameInfo &Name);
  VarDecl *findVariable(Scope *S, DeclarationName Name);
  VarDecl *findGlobalVariable(DeclarationName Name);
  FunctionDecl *findFunction(Scope *S, DeclarationName Name);
  FunctionDecl *findFunction(CXXScopeSpec &SS, DeclarationName Name);
  VarDecl *findConstant(CXXScopeSpec &SS, const DeclarationNameInfo &Name);

  //------ Type conversions
  //

  Expr *createImplicitCast(QualType TargetType, Expr *Arg);
  static CastKind getCastKind(QualType ExprType, QualType CastType);
  Expr *BuildConstCast(Expr *E);
  Expr * castPlainTypeToConstRef(Expr * E);
  void diagnoseImplicitTypeCast(SourceLocation Loc, QualType ParamType, QualType ArgType);
  Expr *castToLValueRef(Expr *E);


  //------ AST build support
  //
  VarDecl *createGlobalVariable(StringRef Name, SourceLocation Loc);
  VarDecl *createLocalVariable(Scope *S, StringRef Name, SourceLocation Loc);
  Stmt *createNullStmt(SourceLocation Loc);
  Expr *packSideEffect(SourceLocation Loc, Expr *LHS, Expr *RHS);
  Expr *unpackSideEffect(Expr *E, Expr *&Side);
  ExprResult createFunctionCall(FunctionDecl *Fn, SourceLocation CallLoc,
                                SourceLocation LParen, ArrayRef<Expr *> Args,
                                SourceLocation RParen);

  //------ Diagnostic helpers.
  //
  void reportTooLargeInteger(SourceLocation Loc, bool Negative);
  void reportTooLargeFloat(SourceLocation Loc);

  //------ Constant expression support.
  //
  // If an expression can be calculated in compile time, it can be associated
  // with some value. Mapping of expressions to values allow to avoid redundant
  // calculations. An expression which is associated with a compile-time value
  // may be replaced with appropriate literal expression. Reverse mapping, from
  // constant value to literal expression assists in this operation.
private:

  ConstantPool Constants;
  std::map<const Expr *, ConstValue *> ConstantExprs;
  std::multimap<ConstValue *, const Expr *> ConstantsInTree;

  Expr *buildConstantExpr(ConstValue *V, SourceLocation Loc);

public:

  ConstantPool &getConstants() { return Constants; }
  void addConstantExpr(const Expr *E, ConstValue *V);
  Expr *getConstantExpr(ConstValue *V, SourceLocation Loc, bool Literal);

  /// \brief Finds constant value for the given expression.
  /// \param E Expression for which constant value is searched for.
  /// \returns Constant value corresponding to the expression or nullptr if
  ///          the expression is not a constant.
  ///
  /// This function is proposed for using after evaluator is called, when
  /// subexpressions are already associated with proper values. So it only
  /// searches pool of all marked expressions. As optimization it also processes
  /// literals other than PhpArrayExpr.
  ///
  ConstValue *getConstantValue(const Expr *E);

  /// \brief Converts the given constant to another constant of the specified
  /// type.
  ConstValue *getConstantValue(ConstValue *CV, QualType TargetType,
                               SourceLocation Loc);

  /// \brief Returns type of the constant value.
  ///
  QualType getConstantType(ConstValue *CV);

  bool diagnoseConversion(ConstValue::ConversionStatus Status,
                          SourceLocation Loc);

  bool isLiteralExpr(const Expr *E);
  IntegerLiteral *alignToIntValueWidth(const IntegerLiteral *E);
};


ExprResult semaRounding(Sema &S, const BuiltinInfo& FInfo,
         StringRef FuncName,
         SourceLocation LParenLoc, MultiExprArg Args, SourceLocation RParenLoc);


// Helper global functions.
CXXRecordDecl *getCurrentClassContext(Scope *&S);

}

#endif
