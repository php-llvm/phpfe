//===--- Runtime.h ------- Interface with PHP runtime -----------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Defines interface to PHP runtime types.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_CODEGEN_RUNTIMETYPES_H
#define PHPFE_CODEGEN_RUNTIMETYPES_H

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/Type.h"
#include "clang/AST/Decl.h"
#include "clang/AST/DeclCXX.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace llvm;
using namespace clang;

class Sema;


class RuntimeTypes {
public:

  //------ Builtin types.
  //
  static QualType getVoidType()      { return VoidType; }
  static QualType getBoolType()      { return BoolType; }
  static QualType getDoubleType()    { return DoubleType; }

  static QualType getIntValueType()  { return IntValueType; }
  static unsigned getIntValueWidth() { return IntValueWidth; }

  //------ Box and flavors.
  //
  static QualType        getPlainBoxType() { return PlainBoxType; }
  static TypeSourceInfo *getPlainBoxInfo() { return PlainBoxInfo; }
  static CXXRecordDecl  *getPlainBoxDecl() { return PlainBoxDecl; }
  static QualType        getConstPlainBoxType() { return ConstPlainBoxType; }
  static QualType        getClassBoxType() { return ClassBoxType; }
  static TypeSourceInfo *getClassBoxInfo() { return ClassBoxInfo; }
  static CXXRecordDecl  *getClassBoxDecl() { return ClassBoxDecl; }
  static QualType        getConstClassBoxLRefType() { return ConstClassBoxLRefType; }
  static TypeSourceInfo *getConstClassBoxLRefInfo() { return ConstClassBoxLRefInfo; }

  static QualType        getNumberBoxType() { return NumberBoxType; }
  static TypeSourceInfo *getNumberBoxInfo() { return NumberBoxInfo; }
  static CXXRecordDecl  *getNumberBoxDecl() { return NumberBoxDecl; }
  static QualType        getArrayBoxType() { return ArrayBoxType; }
  static TypeSourceInfo *getArrayBoxInfo() { return ArrayBoxInfo; }
  static CXXRecordDecl  *getArrayBoxDecl() { return ArrayBoxDecl; }
  static QualType        getStringBoxType() { return StringBoxType; }
  static TypeSourceInfo *getStringBoxInfo() { return StringBoxInfo; }
  static CXXRecordDecl  *getStringBoxDecl() { return StringBoxDecl; }

  //------ Basic value types.
  //
  static QualType        getArrayType() { return ArrayType; }
  static TypeSourceInfo *getArrayInfo() { return ArrayInfo; }
  static CXXRecordDecl  *getArrayDecl() { return ArrayDecl; }
  static QualType        getArrayPtrType() { return ArrayPtrType; }
  static TypeSourceInfo *getArrayPtrInfo() { return ArrayPtrInfo; }
  static QualType        getConstArrayType() { return ConstArrayType; }
  static TypeSourceInfo *getConstArrayInfo() { return ConstArrayInfo; }
  static CXXRecordDecl  *getConstArrayDecl() { return ConstArrayDecl; }

  static QualType        getStringRefType() { return StringRefType; }
  static TypeSourceInfo *getStringRefInfo() { return StringRefInfo; }
  static CXXRecordDecl  *getStringRefDecl() { return StringRefDecl; }
  static QualType        getStringType() { return StringType; }
  static TypeSourceInfo *getStringInfo() { return StringInfo; }
  static CXXRecordDecl  *getStringDecl() { return StringDecl; }

  static QualType        getObjectType() { return ObjectType; }
  static TypeSourceInfo *getObjectInfo() { return ObjectInfo; }
  static CXXRecordDecl  *getObjectDecl() { return ObjectDecl; }
  static QualType        getObjectPtrType() { return ObjectPtrType; }
  static TypeSourceInfo *getObjectPtrInfo() { return ObjectPtrInfo; }

  static QualType        getResourceType() { return ResourceType; }
  static TypeSourceInfo *getResourceInfo() { return ResourceInfo; }
  static CXXRecordDecl  *getResourceDecl() { return ResourceDecl; }

  //------ Execution data types.
  //
  static QualType        getUnitRecordType() { return UnitRecordType; }
  static TypeSourceInfo *getUnitRecordInfo() { return UnitRecordInfo; }
  static CXXRecordDecl  *getUnitRecordDecl() { return UnitRecordDecl; }
  static QualType        getConstUnitRecordType() { return ConstUnitRecordType; }
  static TypeSourceInfo *getConstUnitRecordInfo() { return ConstUnitRecordInfo; }

  static QualType        getUnitStateType() { return UnitStateType; }
  static TypeSourceInfo *getUnitStateInfo() { return UnitStateInfo; }
  static CXXRecordDecl  *getUnitStateDecl() { return UnitStateDecl; }
  static QualType        getUnitStatePtrType() { return UnitStatePtrType; }
  static TypeSourceInfo *getUnitStatePtrInfo() { return UnitStatePtrInfo; }

  static QualType        getUnitExecuteHandlerType() { return UnitExecuteHandlerType; }
  static TypeSourceInfo *getUnitExecuteHandlerInfo() { return UnitExecuteHandlerInfo; }

  //------ Miscellaneous types.
  //
  static QualType        getValueInfoType() { return ValueInfoType; }
  static TypeSourceInfo *getValueInfoInfo() { return ValueInfoInfo; }
  static CXXRecordDecl  *getValueInfoDecl() { return ValueInfoDecl; }
  static QualType        getAssociationType() { return AssociationType; }

  static ClassTemplateDecl *getBoxedTemplateDecl() { return BoxedTemplateDecl; }
  static ClassTemplateDecl *getRefBoxTemplateDecl() { return RefBoxTemplateDecl; }
  static ClassTemplateDecl *getArgPackTemplateDecl() { return ArgPackTemplateDecl; }

  static CXXBaseSpecifier *getBaseOfRefBox() { return BaseOfRefBox; }
  static CXXBaseSpecifier *getBaseOfBoxed() { return BaseOfBoxed; }
  static CXXBaseSpecifier *getBaseOfBox() { return BaseOfBox; }

  //------ Constants
  //
  static const APInt &getMaxInteger() { return MaxInteger; }
  static const APInt &getMinInteger() { return MinInteger; }
  static const APInt &getZeroInteger() { return ZeroInteger; }
  static const APInt &getOneInteger() { return OneInteger; }
  static const APInt &getMinusOneInteger() { return MinusOneInteger; }

  //------ Basic type tests.
  //
  static bool isBooleanType(QualType x);
  static bool isIntegerType(QualType x);
  static bool isFloatingType(QualType x);
  static bool isCStringType(QualType x);
  static bool isStringType(QualType x);
  static bool isArrayType(QualType x);
  static bool isObjectType(QualType x);
  static bool isResourceType(QualType x);
  static bool isUniversalType(QualType x);
  static bool isUniversalPlainType(QualType x);
  static bool isUniversalClassType(QualType x);
  static bool isNumberBoxType(QualType x);
  static bool isNumberType(QualType x);

  static bool areTypesCompatible(QualType TargetTy, QualType ExprTy);
  static bool areTypesEquivalent(QualType Ty, QualType RefTy);
  static bool isNarrowerType(QualType Ty, QualType RefTy);
  static bool isNotWiderType(QualType Ty, QualType RefTy);

  // Operations with boxed types.
  static bool isBoxedType(QualType x);
  static bool isRefBoxType(QualType x);
  static QualType unboxType(QualType x);
  static bool isBoxedSpecialization(QualType T, QualType TargetType);

  // Operations with variadic packs.
  static bool isArgPackType(QualType x);

  // Helper functions.
  static QualType getParameterOfSpecialization(QualType Ty);
  static bool isSpecializationOf(CXXRecordDecl *RD, ClassTemplateDecl *TDecl);

protected:

  //------ PHP value types

  // box
  static QualType         PlainBoxType;
  static TypeSourceInfo  *PlainBoxInfo;
  static CXXRecordDecl   *PlainBoxDecl;

  // const box
  static QualType         ConstPlainBoxType;
  static TypeSourceInfo  *ConstPlainBoxInfo;

  // Box
  static QualType         ClassBoxType;
  static TypeSourceInfo  *ClassBoxInfo;
  static CXXRecordDecl   *ClassBoxDecl;

  // const Box
  static QualType         ConstClassBoxType;
  static TypeSourceInfo  *ConstClassBoxInfo;

  // const Box&
  static QualType         ConstClassBoxLRefType;
  static TypeSourceInfo  *ConstClassBoxLRefInfo;

  // NumberBox
  static QualType         NumberBoxType;
  static TypeSourceInfo  *NumberBoxInfo;
  static CXXRecordDecl   *NumberBoxDecl;

  // ArrayBox
  static QualType         ArrayBoxType;
  static TypeSourceInfo  *ArrayBoxInfo;
  static CXXRecordDecl   *ArrayBoxDecl;

  // ValueInfo
  static QualType         ValueInfoType;
  static TypeSourceInfo  *ValueInfoInfo;
  static CXXRecordDecl   *ValueInfoDecl;
  static FieldDecl       *ValueInfoValueField;

  // const ValueInfo
  static QualType         ConstValueInfoType;
  static TypeSourceInfo  *ConstValueInfoInfo;

  // Unit
  static QualType         UnitRecordType;
  static TypeSourceInfo  *UnitRecordInfo;
  static CXXRecordDecl   *UnitRecordDecl;

  // const Unit
  static QualType         ConstUnitRecordType;
  static TypeSourceInfo  *ConstUnitRecordInfo;

  // module execute handler
  static QualType         UnitExecuteHandlerType;
  static TypeSourceInfo  *UnitExecuteHandlerInfo;

  // UnitState
  static QualType         UnitStateType;
  static TypeSourceInfo  *UnitStateInfo;
  static CXXRecordDecl   *UnitStateDecl;

  // UnitState *
  static QualType         UnitStatePtrType;
  static TypeSourceInfo  *UnitStatePtrInfo;

  // VarRecord
  static QualType        VarRecordType;
  static TypeSourceInfo  *VarRecordInfo;
  static CXXRecordDecl   *VarRecordDecl;

  // Array
  static QualType         ArrayType;
  static TypeSourceInfo  *ArrayInfo;
  static CXXRecordDecl   *ArrayDecl;

  // ConstArray
  static QualType         ConstArrayType;
  static TypeSourceInfo  *ConstArrayInfo;
  static CXXRecordDecl   *ConstArrayDecl;

  // Array *
  static QualType         ArrayPtrType;
  static TypeSourceInfo  *ArrayPtrInfo;

  // ArrayBase::association
  static QualType         AssociationType;
  static TypeSourceInfo  *AssociationInfo;
  static CXXRecordDecl   *AssociationDecl;

  // string_ref
  static QualType         StringRefType;
  static TypeSourceInfo  *StringRefInfo;
  static CXXRecordDecl   *StringRefDecl;

  // String (dynamic string)
  static QualType         StringType;
  static TypeSourceInfo  *StringInfo;
  static CXXRecordDecl   *StringDecl;

  // StringBox
  static QualType         StringBoxType;
  static TypeSourceInfo  *StringBoxInfo;
  static CXXRecordDecl   *StringBoxDecl;

  // Resource
  static QualType         ResourceType;
  static TypeSourceInfo  *ResourceInfo;
  static CXXRecordDecl   *ResourceDecl;

  // Object
  static QualType         ObjectType;
  static TypeSourceInfo  *ObjectInfo;
  static CXXRecordDecl   *ObjectDecl;

  // Object *
  static QualType         ObjectPtrType;
  static TypeSourceInfo  *ObjectPtrInfo;

  // templates
  static ClassTemplateDecl *BoxedTemplateDecl;
  static ClassTemplateDecl *RefBoxTemplateDecl;
  static ClassTemplateDecl *ArgPackTemplateDecl;

  // Builtin types
  static QualType         VoidType;
  static QualType         BoolType;
  static QualType         DoubleType;

  // Integer type used to represent integer value in the universal type.
  static QualType IntValueType;
  static unsigned IntValueWidth;

  // Bases of some record types.
  static CXXBaseSpecifier *BaseOfRefBox;
  static CXXBaseSpecifier *BaseOfBoxed;
  static CXXBaseSpecifier *BaseOfBox;

  // Some useful constants.
  //
  static APInt MaxInteger;
  static APInt MinInteger;
  static APInt ZeroInteger;
  static APInt OneInteger;
  static APInt MinusOneInteger;

protected:
  static bool load(Sema &Semantics);
};

}
#endif
