//===--- ConstValue.h -----------------------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// Class that represents a value in constant folding calculations.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_SUPPORT_CONSTVALUE_H
#define PHPFE_SUPPORT_CONSTVALUE_H

//------ Dependencies ----------------------------------------------------------
#include "llvm/ADT/APInt.h"
#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/ADT/PointerUnion.h"
#include <map>
#include <string>
//------------------------------------------------------------------------------


namespace phpfe {

using namespace llvm;


class ConstValue;
class ConstantPool;


const unsigned int DoubleFormatPrecision = 14;

// Result of value comparison.
enum class CompareResult {
  Less,
  Equal,
  Greater
};


class ArrayElement {
  const ConstValue *Key;
  const ConstValue *Value;

public:
  ArrayElement(const ConstValue *K, const ConstValue *V)
      : Key(K), Value(V) {}

  const ConstValue *getKey() const { return Key; }
  const ConstValue *getValue() const { return Value; }

  bool less(const ArrayElement &X) const {
    if (Key < X.Key)
      return true;
    if (Key > X.Key)
      return false;
    return Value < X.Value;
  }

  bool equals(const ArrayElement &X) const {
    return Key == X.Key && Value == X.Value;
  }

  bool operator < (const ArrayElement &X) const {
    return less(X);
  }

  bool operator == (const ArrayElement &X) const {
    return equals(X);
  }

  bool operator != (const ArrayElement &X) const {
    return !equals(X);
  }

  void setKey(const ConstValue *K) {
    assert(!K && "Key is already set");
    Key = K;
  }
};


class ArrayItem {
  llvm::PointerUnion<const ConstValue *, ArrayElement *> Ptr;
public:
  ArrayItem(const ConstValue *V) : Ptr(V) {
    assert(V);
  }

  ArrayItem(const ConstValue *K, const ConstValue *V) {
    assert(K);
    assert(V);
    Ptr = new ArrayElement(K, V);
  }

// TODO:
//   ~ArrayItem() {
//     if (ArrayElement *AE = Ptr.dyn_cast<ArrayElement*>())
//       delete AE;
//   }

  const ConstValue *getKey() const {
    if (ArrayElement *AE = Ptr.dyn_cast<ArrayElement*>())
      return AE->getKey();
    return nullptr;
  }

  void setKey(const ConstValue *K) {
    assert(K);
    assert(Ptr.is<const ConstValue*>());
    ArrayElement *Elt = new ArrayElement(K, Ptr.get<const ConstValue*>());
    Ptr = Elt;
  }

  const ConstValue *getValue() const {
    if (ArrayElement *AE = Ptr.dyn_cast<ArrayElement*>())
      return AE->getValue();
    return Ptr.get<const ConstValue *>();
  }

  bool less(const ArrayItem &X) const {
    if (Ptr.is<const ConstValue*>() < X.Ptr.is<const ConstValue*>())
      return true;
    if (Ptr.is<const ConstValue*>() > X.Ptr.is<const ConstValue*>())
      return false;
    if (const ConstValue *V = Ptr.dyn_cast<const ConstValue*>())
      return V < X.Ptr.dyn_cast<const ConstValue*>();
    return Ptr.get<ArrayElement *>()->less(*X.Ptr.get<ArrayElement *>());
  }

  bool equals(const ArrayItem &X) const {
    if (Ptr.is<const ConstValue*>() != X.Ptr.is<const ConstValue*>())
      return false;
    if (ArrayElement *AE = Ptr.dyn_cast<ArrayElement *>())
      return AE->equals(*X.Ptr.dyn_cast<ArrayElement *>());
    return Ptr.dyn_cast<const ConstValue *>() ==
           X.Ptr.dyn_cast<const ConstValue *>();
  }

  bool operator < (const ArrayItem &X) const {
    return less(X);
  }

  bool operator == (const ArrayItem &X) const {
    return equals(X);
  }

  bool operator != (const ArrayItem &X) const {
    return !equals(X);
  }
};


class ArrayConstValue {
  SmallVector<ArrayItem, 16> Content;
  bool IsVector;
public:
  typedef SmallVectorImpl<ArrayItem>::const_iterator const_iterator;

  ArrayConstValue() : IsVector(false) {}
  void set(const SmallVectorImpl<ArrayItem> &X, ConstantPool &Pool);

  unsigned size() const { return Content.size(); }
  bool empty() const { return Content.empty(); }
  const_iterator begin() const { return Content.begin(); }
  const_iterator end() const { return Content.end(); }
  ArrayItem *erase(unsigned I) { return Content.erase(Content.data() + I); }

  ArrayItem front() const { return Content.front(); }
  ArrayItem back() const { return Content.back(); }
  ArrayItem get(unsigned I) const { return Content[I]; }
  const ConstValue *getKey(unsigned I) const { return Content[I].getKey(); }
  const ConstValue *getValue(unsigned I) const { return Content[I].getValue(); }

  bool isVector() const { return IsVector; }
  bool equals(const ArrayConstValue &X) const;
  bool less(const ArrayConstValue &X) const;
  bool hasKey(const ConstValue *Key) const;

  CompareResult compare(const ArrayConstValue &X) const;
  bool equals_strict(const ArrayConstValue &X) const;

  bool operator <  (const ArrayConstValue &X) const { return less(X); }
  bool operator == (const ArrayConstValue &X) const { return equals(X); }
  bool operator != (const ArrayConstValue &X) const { return !equals(X); }
};


/// \brief Constant value in constant folding calculation.
///
class ConstValue {
  friend class ConstantPool;

public:
  // Possible states.
  enum ConstKind {
    NotAConstant,
    Null,
    Bool,
    Int,
    Float,
    String,
    Array
  };

  // Status of value conversion.
  enum ConversionStatus {
    OK,                   // Conversion was successful
    NaN,                  // "abc" -> int
    ExtraSymbols,         // "123abc" -> 123
    IntTooLarge,          // "123...123" -> int
    IntTooLargeNegative,  // "-123...123" -> int
    FloatTooLarge,        // "1e+10000" -> double
    ArrayConv,            // array() -> 0, array(1,2,3) -> 1
    Invalid,              // Conversion is impossible (1 -> array)
  };

private:

  ConstKind Kind;

  bool BoolVal;
  APInt IntVal;
  APFloat FloatVal;
  std::string StringVal;
  ArrayConstValue ArrayVal;

  ConstValue *Key;

  ConstValue();
  ConstValue(bool X);

public:

  ConstKind getKind() const { return Kind; }

  bool isNotConstant() const { return Kind == NotAConstant; }
  bool isNull()        const { return Kind == Null; }
  bool isBool()        const { return Kind == Bool; }
  bool isInt()         const { return Kind == Int; }
  bool isFloat()       const { return Kind == Float; }
  bool isString()      const { return Kind == String; }
  bool isArray()       const { return Kind == Array; }

  bool isStringNumeric(bool &HasExtra) const;
  bool isStringFloat(bool &HasExtra) const;
  bool isStringInteger(bool &HasExtra) const;

  bool            getBool()   const { assert(isBool()); return BoolVal; }
  APInt           getInt()    const { assert(isInt());  return IntVal; }
  APFloat         getFloat()  const { assert(isFloat()); return FloatVal; }
  StringRef       getString() const { assert(isString()); return StringVal; }
  const ArrayConstValue &getArray() const { assert(isArray()); return ArrayVal; }

  bool getAsBool(ConversionStatus &Status) const;
  APInt getAsInt(ConversionStatus &Status, long base = 10) const;
  APFloat getAsFloat(ConversionStatus &Status) const;
  std::string getAsString(ConversionStatus &Status) const;

  bool isZero() const;
  bool isOne() const;

  void setNull() { Kind = Null; }
  void set(bool Val) { Kind = Bool; BoolVal = Val; }
  void set(APInt Val) { Kind = Int; IntVal = Val; }
  void set(APFloat Val) { Kind = Float; FloatVal = Val; }
  void set(std::string Val) { Kind = String; StringVal = Val; }
  void set(const ArrayConstValue &Val);

  bool hasKey() const { return Key != nullptr; }
  ConstValue *getKey() const { return Key; }
  void setKey(ConstValue *K) { Key = K; }
  ConstValue *takeKey();

  bool equals_strict(const ConstValue *V) const;
  CompareResult compare(const ConstValue *V) const;
};


class ConstantPool {

  struct IntComparator {
    bool operator() (const APInt &V1, const APInt &V2) const {
      return V1.slt(V2);
    }
  };

  struct FloatComparator {
    bool operator() (const APFloat &V1, const APFloat &V2) const {
      // Process different signs, because 'compare' is sometimes unordered.
      if (V1.isNegative() && !V2.isNegative())
        return true;
      if (!V1.isNegative() && V2.isNegative())
        return false;
      return V1.compare(V2) == APFloat::cmpLessThan;
    }
  };

  std::unique_ptr<ConstValue> NullConstant;
  std::unique_ptr<ConstValue> TrueConstant;
  std::unique_ptr<ConstValue> FalseConstant;
  ConstValue *ZeroConstant;
  ConstValue *OneConstant;
  ConstValue *MinusOneConstant;
  ConstValue *FloatZeroConstant;
  ConstValue *EmptyStringConstant;
  std::map<APInt, ConstValue *, IntComparator> IntConstants;
  std::map<APFloat, ConstValue *, FloatComparator> FloatConstants;
  std::map<std::string, ConstValue *> StringConstants;
  std::map<ArrayConstValue, ConstValue *> ArrayConstants;

public:
  ConstantPool();
  ~ConstantPool();

  void init();

  ConstValue *getNull() { return NullConstant.get(); }
  ConstValue *getFalse() { return FalseConstant.get(); }
  ConstValue *getTrue() { return TrueConstant.get(); }
  ConstValue *getZero() { return ZeroConstant; }
  ConstValue *getOne() { return OneConstant; }
  ConstValue *getMinusOne() { return MinusOneConstant; }
  ConstValue *getFloatZero() { return FloatZeroConstant; }
  ConstValue *getEmptyString() { return EmptyStringConstant; }
  ConstValue *get(bool X) { return X ? TrueConstant.get() : FalseConstant.get(); }
  ConstValue *getIntBool(bool X) { return X ? OneConstant : ZeroConstant; }
  ConstValue *get(const APInt &X);
  ConstValue *get(const APFloat &X);
  ConstValue *get(std::string X);
  ConstValue *get(const SmallVectorImpl<ArrayItem> &X);

  bool has(ConstValue *V);
  void add(ConstValue *V);
};

}
#endif
