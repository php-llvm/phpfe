//===--- Transformer.h --- AST Transformation component ---------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Transformer component that make AST valid from viewpoint of clang code
//  generator.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_TRANSFORMER_LEGALIZER_H
#define PHPFE_TRANSFORMER_LEGALIZER_H

//------ Dependencies ----------------------------------------------------------
#include "phpfe/Transformer/Transformer.h"
#include "phpfe/Sema/RuntimeTypes.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


// Transforms statements to the form suitable for processing by code generator.
//
class Legalizer : public clang::StmtVisitor<Legalizer, Stmt*>,
                  public RuntimeTypes {
  Transformer &Trans;
  Sema &Semantics;
  ASTContext &Context;
  FunctionDecl *CurrentFunction;

public:

  Runtime &getRuntime() { return Semantics.getRuntime(); }

  Expr *LegalizeTypedExpr(QualType TargetTy, Expr *Val);
  Expr *legalizeBooleanExpr(Expr *E);
  Expr *legalizeIntegerExpr(QualType TargetType, Expr *E);
  Expr *legalizeFloatingExpr(QualType TargetType, Expr *E);
  Expr *legalizeStringRefExpr(Expr *Val);
  Expr *legalizeStringBoxExpr(Expr *Val);
  Expr *legalizeArrayBoxExpr(Expr *Val);
  Expr *legalizeBoxExpr(Expr *Val, bool isRef);

  bool isRefArgument(QualType ParamType);
  Expr *LegalizeArgument(ParmVarDecl *Param, Expr *Val);
  Expr *legalizeRefArgument(QualType TargetTy, Expr *Val);

  Expr *LegalizeEchoArgument(Expr *Arg);
  Expr *LegalizeUnresolvedCall(CallExpr *E);
  Expr *LegalizeCastExpression(QualType CastType, Expr *SubExpr);
  void LegalizeCallArguments(FunctionDecl *FD, SmallVectorImpl<Expr*> &Args);
  Expr *TryCxxBinOpSemantics(BinaryOperator *BO);
  Expr *LegalizeUnaryIncDec(UnaryOperator *E);

  Expr *LegalizeAccess(Expr *E);
  Expr *LegalizeCombinedAssign(CompoundAssignOperator *E);

  Legalizer(Transformer &T);
  ~Legalizer();

  Sema& getSema() { return Semantics; }

  // Internal methods.
  Expr *castBetweenBoxed(QualType TargetType, Expr *E);

  // Entry points.
  bool LegalizeFunction(FunctionDecl *FDecl);
  bool legalizeVariable(VarDecl *VD);
  bool LegalizeClass(CXXRecordDecl *CD);

  // Helper methods for legalizing functions calls
  Expr *LegalizeCast(QualType ParamTy, Expr *Val);
  Expr *LegalizeNonReferenceArgument(QualType ParamTy, Expr *Arg);
  Expr *LegalizeAccessArgument(QualType ParamTy, Expr *Arg);
  Expr *LegalizeReferenceArgument(QualType ParamTy, Expr *Arg);
  Expr *LegalizeReturnExpr(FunctionDecl *FDecl, Expr *Arg);
  Expr *LegalizeReturnReference(QualType ReturnType, Expr *Arg);
  Expr *LegalizeReturnValue(QualType ReturnType, Expr *Arg);

  // Statements.
  Stmt *VisitStmt(Stmt *S);
  Stmt *VisitCompoundStmt(CompoundStmt *S);
  Stmt *VisitIfStmt(IfStmt *S);
  Stmt *VisitWhileStmt(WhileStmt *S);
  Stmt *VisitReturnStmt(ReturnStmt *S);
  Stmt *VisitDeclStmt(DeclStmt *S);

  // PHP statements.
  Stmt *VisitPhpEchoStmt(PhpEchoStmt *S);

  // C++ expressions.
  Expr *VisitCXXMemberCallExpr(CXXMemberCallExpr *E);

  // Expressions.
  Expr *VisitParenExpr(ParenExpr *E);
  Expr *VisitArraySubscriptExpr(ArraySubscriptExpr *E);
  Expr *VisitCStyleCastExpr(CStyleCastExpr *E);
  Expr *VisitImplicitCastExpr(ImplicitCastExpr *E);
  Expr *VisitCallExpr(CallExpr *E);
  Expr *VisitOpaqueValueExpr(OpaqueValueExpr *E);


  Expr *VisitConditionalOperator(ConditionalOperator *E);
  Expr *VisitBinaryConditionalOperator(BinaryConditionalOperator *E);

  // Binary operators

  Expr *VisitBinMul(BinaryOperator *E);
  Expr *VisitBinDiv(BinaryOperator *E);
  Expr *VisitBinRem(BinaryOperator *E);
  Expr *VisitBinAdd(BinaryOperator *E);
  Expr *VisitBinSub(BinaryOperator *E);
  Expr *VisitBinShl(BinaryOperator *E);
  Expr *VisitBinShr(BinaryOperator *E);

  Expr *VisitBinEQ(BinaryOperator *E);
  Expr *VisitBinNE(BinaryOperator *E);
  Expr *VisitBinLT(BinaryOperator *E);
  Expr *VisitBinGT(BinaryOperator *E);
  Expr *VisitBinLE(BinaryOperator *E);
  Expr *VisitBinGE(BinaryOperator *E);

  Expr *VisitBinAnd(BinaryOperator *E);
  Expr *VisitBinXor(BinaryOperator *E);
  Expr *VisitBinOr(BinaryOperator *E);

  Expr *VisitBinLAnd(BinaryOperator *E);
  Expr *VisitBinLOr(BinaryOperator *E);

  Expr *VisitBinAssign(BinaryOperator *E);
  Expr *VisitBinMulAssign(CompoundAssignOperator *E);
  Expr *VisitBinDivAssign(CompoundAssignOperator *E);
  Expr *VisitBinRemAssign(CompoundAssignOperator *E);
  Expr *VisitBinAddAssign(CompoundAssignOperator *E);
  Expr *VisitBinSubAssign(CompoundAssignOperator *E);
  Expr *VisitBinShlAssign(CompoundAssignOperator *E);
  Expr *VisitBinShrAssign(CompoundAssignOperator *E);
  Expr *VisitBinAndAssign(CompoundAssignOperator *E);
  Expr *VisitBinXorAssign(CompoundAssignOperator *E);
  Expr *VisitBinOrAssign(CompoundAssignOperator *E);

  Expr *VisitBinComma(BinaryOperator *E);

  Expr *VisitBinPhpConcat(BinaryOperator *E);
  Expr *VisitBinPhpIdent(BinaryOperator *E);
  Expr *VisitBinPhpNotIdent(BinaryOperator *E);
  Expr *VisitBinPhpSpaceship(BinaryOperator *E);
  Expr *VisitBinPhpPower(BinaryOperator *E);
  Expr *VisitBinPhpConcatAssign(CompoundAssignOperator *E);
  Expr *VisitBinPhpPowerAssign(CompoundAssignOperator *E);

  // Unary operators
  Expr *VisitUnaryLNot(UnaryOperator *E);
  Expr *VisitUnaryPostInc(UnaryOperator *E);
  Expr *VisitUnaryPreInc(UnaryOperator *E);
  Expr *VisitUnaryPostDec(UnaryOperator *E);
  Expr *VisitUnaryPreDec(UnaryOperator *E);

  Expr *VisitPhpNullLiteral(PhpNullLiteral *E);
  Expr *VisitPhpArrayExpr(PhpArrayExpr *E);
};

}
#endif
