//===--- Transformer.h --- AST Transformation component ---------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Transformer component that processes expressions result of which is unused.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_TRANSFORMER_ELIMINATOR_H
#define PHPFE_TRANSFORMER_ELIMINATOR_H

//------ Dependencies ----------------------------------------------------------
#include "phpfe/Transformer/Transformer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


/// \brief Processes an expression the result of which is unused.
///
/// The component throws away unused calculations and keeps expressions with
/// side effect.
///
class Eliminator : public StmtVisitor<Eliminator, Expr*> {
  Transformer &Trans;
  Sema &Semantics;

  bool DiagnosticSuppressed;

public:
  Eliminator(Transformer &T)
    : Trans(T), Semantics(T.getSema()) {
  }

  bool isDiagnosticSuppressed() const { return DiagnosticSuppressed; }
  void suppressDiagnostic(bool X) { DiagnosticSuppressed = X; }

  Expr *eliminateUnused(Expr *E, bool SuppressDiagnostic = false);

  Expr *VisitExpr(Expr *E);

  Expr *VisitBinaryOperator(BinaryOperator *E);
  Expr *VisitBinComma(BinaryOperator *E);

  Expr *VisitUnaryOperator(UnaryOperator *E);
  Expr *VisitUnaryPreInc(UnaryOperator *E) { return E; }
  Expr *VisitUnaryPreDec(UnaryOperator *E) { return E; }
  Expr *VisitUnaryPostInc(UnaryOperator *E) { return E; }
  Expr *VisitUnaryPostDec(UnaryOperator *E) { return E; }

  Expr *VisitPhpArrayExpr(PhpArrayExpr *E);
  Expr *VisitPhpMapExpr(PhpMapExpr *E);

  Expr *VisitParenExpr(ParenExpr *E);
  Expr *VisitCallExpr(CallExpr* E);

  Expr *VisitPhpNullLiteral(PhpNullLiteral *E);
  Expr *VisitCXXBoolLiteralExpr(CXXBoolLiteralExpr *E);
  Expr *VisitIntegerLiteral(IntegerLiteral *E);
  Expr *VisitFloatingLiteral(FloatingLiteral *E);
  Expr *VisitStringLiteral(StringLiteral *E);
  Expr *VisitDeclRefExpr(DeclRefExpr *E);
};

}
#endif
