//===--- Transformer.h --- AST Transformation component ---------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Compiler component that make AST transformations.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_TRANSFORMER_TRANSFORMER_H
#define PHPFE_TRANSFORMER_TRANSFORMER_H

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/StmtVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Sema/SemaConsumer.h"
#include "phpfe/Sema/PhpSema.h"
#include <memory>
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;

class Reducer;
class Legalizer;

/// \brief Runs different transformations over AST.
///
class Transformer : public SemaConsumer {

  Sema *SemaPtr;
  TransMode  Mode;
  std::unique_ptr<Legalizer> LegalizeT;
  std::unique_ptr<Reducer> ReducerT;

public:

  Transformer(TransMode M);
  ~Transformer();

  // ASTConsumer interface
  bool HandleTopLevelDecl(DeclGroupRef D) override;
  void HandleTranslationUnit(ASTContext &Ctx) override;
  void InitializeSema(clang::Sema &S) override;

  // Query information.
  ASTContext &getASTContext() {
    assert(SemaPtr); return SemaPtr->getASTContext();
  }
  Sema &getSema() {
    assert(SemaPtr);
    return *SemaPtr;
  }
  ConstantPool &getConstants() {
    assert(SemaPtr);
    return SemaPtr->getConstants();
  }

  // Running.
  bool Transform(Decl *D);

  // Groups of transformations.
  bool TransformForAST(Decl *D);
  bool TransformForSyntax(Decl *D);
  bool TransformForCodeGen(Decl *D);

  // Particular transformations.
  bool ApplyResolver(Decl *D);
  bool ApplyPreshaper(Decl *D);
  bool ApplyReducer(Decl *D);
  bool ApplyLegalizer(Decl *D);

  // Helper methods.
  Expr *createChainExpression(SourceLocation Loc, Expr *Chain, Expr *Item);
  Expr *createFunctionCall(Expr *Func, ArrayRef<Expr *> Args);
  Expr *createCFunctionCall(StringRef FuncName, ArrayRef<Expr *> Args);
  Expr *createRuntimeFunctionCall(StringRef FuncName, MutableArrayRef<Expr *> Args);
};

}
#endif
