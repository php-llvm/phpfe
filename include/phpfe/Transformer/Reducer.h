//===--- Reducer.h ------- AST Transformation component ---------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Transformer component, which is responsible for AST simplification including
//  constant folding.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_TRANSFORMER_REDUCER_H
#define PHPFE_TRANSFORMER_REDUCER_H

//------ Dependencies ----------------------------------------------------------
#include "phpfe/Transformer/Transformer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;
  

/// \brief Simplifies an expression or a statement.
///
/// Given a statement (in particular, an expression), produces a statement,
/// which is simpler in some sense but retains the same semantics.
///
class Reducer : public StmtVisitor<Reducer, Stmt*>,
                public RuntimeTypes {
  Transformer &Trans;
  Sema &Semantics;
  ASTContext &Context;

  bool Changed;

  Expr *reduceCastExpression(QualType TargetType, Expr *SubExpr,
                             SourceLocation Loc);
  Expr *reduceBinaryOperator(BinaryOperator *E);

  Expr *processNullAssignment(BinaryOperator *BO);
  ConstValue *checkIdentity(BinaryOperator *E);
  void ProcessStaticAssertDecl(StaticAssertDecl *D);
  Expr *processUnaryNode(UnaryOperator *UO);
  Expr *processIncDecNode(UnaryOperator *UO);

public:

  Transformer &getTransformer() { return Trans; }
  Sema &getSema() { return Semantics; }

  Expr *replaceBy(Expr *E, Expr *S);
  Expr *replaceBy(Expr *E, ConstValue *S);

  Reducer(Transformer &T);

  Expr *VisitExpr(Expr *E);

  Expr *VisitParenExpr(ParenExpr *E);
  Expr *VisitCallExpr(CallExpr *E);

  Expr *VisitIntegerLiteral(IntegerLiteral *E);
  Expr *VisitStringLiteral(StringLiteral *E);
  Expr *VisitFloatingLiteral(FloatingLiteral *E);
  Expr *VisitPhpNullLiteral(PhpNullLiteral *E);
  Expr *VisitCXXBoolLiteralExpr(CXXBoolLiteralExpr *E);

  Expr *VisitArraySubscriptExpr(ArraySubscriptExpr *E);
  Expr *VisitCStyleCastExpr(CStyleCastExpr *E);
  Expr *VisitImplicitCastExpr(ImplicitCastExpr *E);

  Expr *VisitConditionalOperator(ConditionalOperator *E);
  Expr *VisitBinaryConditionalOperator(BinaryConditionalOperator *E);

  Expr *VisitPhpArrayExpr(PhpArrayExpr *E);
  Expr *VisitPhpMapExpr(PhpMapExpr *E);

  Expr *VisitBinPhpPower(BinaryOperator *E);

  Expr *VisitBinEQ(BinaryOperator *E);
  Expr *VisitBinNE(BinaryOperator *E);
  Expr *VisitBinPhpIdent(BinaryOperator *E);
  Expr *VisitBinPhpNotIdent(BinaryOperator *E);

  Expr *VisitBinLAnd(BinaryOperator *E);
  Expr *VisitBinLOr(BinaryOperator *E);

  Expr *VisitBinAdd(BinaryOperator *E);
  Expr *VisitBinAssign(BinaryOperator *E);
  Expr *VisitBinAddAssign(CompoundAssignOperator *E);
  Expr *VisitBinSubAssign(CompoundAssignOperator *E);
  Expr *VisitBinMulAssign(CompoundAssignOperator *E);
  Expr *VisitBinDivAssign(CompoundAssignOperator *E);
  Expr *VisitBinRemAssign(BinaryOperator *E);
  Expr *VisitBinAndAssign(BinaryOperator *E);
  Expr *VisitBinOrAssign(BinaryOperator *E);
  Expr *VisitBinXorAssign(BinaryOperator *E);
  Expr *VisitBinShlAssign(BinaryOperator *E);
  Expr *VisitBinShrAssign(BinaryOperator *E);
  Expr *VisitBinPhpPowerAssign(BinaryOperator *E);

  Expr *VisitUnaryMinus(UnaryOperator *UO);
  Expr *VisitUnaryPlus(UnaryOperator *UO);
  Expr *VisitUnaryLNot(UnaryOperator *UO);
  Expr *VisitUnaryNot(UnaryOperator *UO);
  Expr *VisitUnaryPostInc(UnaryOperator *UO);
  Expr *VisitUnaryPreInc(UnaryOperator *UO);
  Expr *VisitUnaryPostDec(UnaryOperator *UO);
  Expr *VisitUnaryPreDec(UnaryOperator *UO);

  Stmt *VisitStmt(Stmt *S);

  Stmt *VisitCompoundStmt(CompoundStmt *S);
  Stmt *VisitDeclStmt(DeclStmt *S);
  Stmt *VisitReturnStmt(ReturnStmt *S);
  Stmt *VisitPhpEchoStmt(PhpEchoStmt *S);
};

}
#endif
