//===--- log.cpp -- PHP Runtime ---------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Declaration of builtin function handlers.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_BUILTIN_BUILTINHANDLERS_H
#define PHPFE_BUILTIN_BUILTINHANDLERS_H

//------ Dependencies ----------------------------------------------------------
//------------------------------------------------------------------------------


namespace clang {
class CallExpr;
class Expr;
}

namespace phpfe{

using clang::CallExpr;
using clang::Expr;

struct BuiltinInfo;
class ConstValue;
class Evaluator;
class Reducer;
class Legalizer;


ConstValue *evaluateAbs(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateMin(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateMax(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateRounding(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateFmod(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIntdiv(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsInt(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsBool(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsFloat(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsNumeric(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsScalar(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsString(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsArray(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsObject(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsNull(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIsResource(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateGetType(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateFloatVal(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateBoolVal(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateStrVal(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateIntVal(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);
ConstValue *evaluateEmpty(Evaluator &E, const BuiltinInfo &FInfo, const CallExpr *CE);


Expr *reduceIsInt(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIsBool(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIsFloat(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIsNumeric(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIsScalar(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIsArray(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIsString(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIsObject(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIsNull(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIsResource(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceGetType(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceSetType(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIntVal(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceFloatVal(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceBoolVal(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceStrVal(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceRound(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceRounding(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceFmod(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceIntdiv(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);
Expr *reduceEmpty(Reducer &E, const BuiltinInfo &FInfo, CallExpr *CE);

Expr* legalizeVarDump(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr *legalizeAssert(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeIsInt(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeIsBool(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeIsFloat(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeIsNumeric(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeIsScalar(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeIsArray(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeIsString(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeIsObject(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeIsNull(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeGetType(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeSetType(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);
Expr* legalizeEmpty(Legalizer &L, const BuiltinInfo& FInfo, CallExpr* E);

}
#endif
