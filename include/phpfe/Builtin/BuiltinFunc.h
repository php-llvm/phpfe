//===--- BuiltinFunc.h ---- Interface with PHP runtime ----------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Interface of working with builtin functions in PHP compiler.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_SEMA_BUILTINFUNC_H
#define PHPFE_SEMA_BUILTINFUNC_H

//------ Dependencies ----------------------------------------------------------
#include "llvm/ADT/SmallVector.h"
#include "phpfe/Sema/Runtime.h"
//------------------------------------------------------------------------------

namespace phpfe {

struct BuiltinInfo;
class Evaluator;
class Decorator;
class Reducer;
class Legalizer;


typedef ExprResult  (SemaHandler)(Sema &S,
                                  const BuiltinInfo& FInfo,
                                  StringRef FuncName,
                                  SourceLocation LParenLoc,
                                  MultiExprArg Args,
                                  SourceLocation RParenLoc);
typedef QualType    (DecoratorHandler)(Decorator &Dec,
                                       const BuiltinInfo& FInfo,
                                       const CallExpr* E);
typedef ConstValue *(EvaluatorHandler)(Evaluator &Eval,
                                       const BuiltinInfo& FInfo,
                                       const CallExpr* E);
typedef Expr       *(ReducerHandler)(Reducer &R,
                                     const BuiltinInfo& FInfo,
                                     CallExpr* E);
typedef Expr       *(LegalizerHandler)(Legalizer &L,
                                       const BuiltinInfo& FInfo,
                                       CallExpr* E);
}


namespace phpfe { namespace builtin {

using llvm::SmallVector;


// Function attribute.
enum AFunc {
  Generic   = 0,
  Pure      = 1,  //< May be calculated in compile-time if arguments are const
  Disabled  = 2   //< Function is not available
};


// Set of function attributes.
class FuncAttrs {
  unsigned Flags;
  void setFlags(AFunc);
public:
  FuncAttrs(AFunc attr);
  FuncAttrs(std::initializer_list<AFunc>);
  bool isPure() const { return Flags & AFunc::Pure; }
  bool isDisabled() const { return Flags & AFunc::Disabled; }
  bool isEnabled() const { return (Flags & AFunc::Disabled) == 0; }
};


// Parameter attribute.
enum AParam {
  None      = 0,  //< No special flags
  Reference = 1,  //< Is passed by reference
  Boxed     = 2,  //< Must be packed into universal type
  Variadic  = 4,  //< Variadic argument
  Optional  = 8   //< may be omitted (no defualt value)
};


// Set of parameter attributes.
class ParamAttrs {
  unsigned Flags;
  void setFlags(AParam A);
public:
  ParamAttrs(AParam Flag);
  ParamAttrs(std::initializer_list<AParam>);
  bool isReference() const { return Flags & Reference; }
  bool isVariadic() const { return Flags & Variadic; }
  bool isBoxed() const { return Flags & Boxed; }
  bool isOptional() const { return Flags & Optional; }
};


// Set of PHP types.
class Types {
  SmallVector<QualType, 4> TypeSet;

public:
  Types();
  Types(QualType);
  Types(std::initializer_list<QualType>);

  Types add(QualType);

  unsigned size() const { return TypeSet.size(); }
  QualType getType(unsigned I) const;
};

inline QualType Null() { return Runtime::getVoidType(); }
inline QualType Bool() { return Runtime::getBoolType(); }
inline QualType Int() { return Runtime::getIntValueType(); }
inline QualType Long() { return Runtime::getIntValueType(); }
inline QualType Double() { return Runtime::getDoubleType(); }
inline QualType String() { return Runtime::getStringType(); }
inline QualType Array() { return Runtime::getArrayType(); }
inline QualType Callable() { return Runtime::getClassBoxType(); }//TODO:
inline QualType Universal() { return Runtime::getClassBoxType(); }
QualType Object(StringRef Name = StringRef());
QualType Number();

inline Types Returns() { return Types(Null()); }
inline Types Returns(QualType T) { return Types(T); }
template<typename ...Ts>
Types Returns(Ts... Tys) {
  return Types({Tys...});
}


// Default values for a parameter.
class Default {
  enum DefType {
    None, Null, Bool, Long, Double, String
  } Type;

  union {
    bool BoolVal;
    long LongVal;
    double DoubleVal;
    const char *StrVal;
  };

public:
  Default();
  explicit Default(std::nullptr_t);
  explicit Default(bool Val);
  explicit Default(long Val);
  explicit Default(double Val);
  explicit Default(const char *Val);

  bool isSpecified() const { return Type != None; }

  bool isNull()   const { return Type == Null;   }
  bool isBool()   const { return Type == Bool;   }
  bool isLong()   const { return Type == Long;   }
  bool isDouble() const { return Type == Double; }
  bool isString() const { return Type == String; }

  bool      getBoolValue()   const { assert(isBool());   return BoolVal;   }
  long      getLongValue()   const { assert(isLong());   return LongVal;   }
  double    getDoubleValue() const { assert(isDouble()); return DoubleVal; }
  StringRef getStringValue() const { assert(isString()); return StrVal;    }

  Expr *buildDefaultExpr(Sema &S, SourceLocation Loc) const;
};


// Function parameter.
class Param {
public:
  ParamAttrs A;
  Types T;
  const char *Name;
  Default Def;

  bool isMandatory() const {
    return !A.isOptional() && !Def.isSpecified() && !A.isVariadic();
  }
  bool isDefault() const { return Def.isSpecified(); }
  bool isOptional() const { return A.isOptional(); }
  bool isVariadic() const { return A.isVariadic(); }
};


// Set of function names.
//
class Funcs {
  SmallVector<const char *, 4>  Names;
public:
  Funcs(const char*);
  Funcs(std::initializer_list<const char *>);
  StringRef get(unsigned n) const;
  unsigned size() const { return Names.size(); }
};


// Set of runtime function names.
//
class RuntimeFuncs {
  SmallVector<const char *, 4>  Names;
public:
  RuntimeFuncs() {}
  RuntimeFuncs(const char*);
  RuntimeFuncs(std::initializer_list<const char *>);
  StringRef get(unsigned n) const;
  unsigned size() const { return Names.size(); }
};


struct Handlers {
  Handlers() {
    clear();
  }

  template <typename ...HTys>
  Handlers(HTys... Hs) {
    clear();
    add(Hs...);
  }

  void clear() {
    SemaH = nullptr;
    DecoratorH = nullptr;
    EvaluatorH = nullptr;
    ReducerH = nullptr;
    LegalizerH = nullptr;
  }

  template <typename HTy, typename ...HTys>
  void add(HTy H, HTys... Hs) {
    add(H);
    add(Hs...);
  }

  void add(SemaHandler H) {
    assert(SemaH == nullptr);
    SemaH = H;
  }

  void add(DecoratorHandler H) {
    assert(DecoratorH == nullptr);
    DecoratorH = H;
  }

  void add(EvaluatorHandler H) {
    assert(EvaluatorH == nullptr);
    EvaluatorH = H;
  }

  void add(ReducerHandler H) {
    assert(ReducerH == nullptr);
    ReducerH = H;
  }

  void add(LegalizerHandler H) {
    assert(LegalizerH == nullptr);
    LegalizerH = H;
  }

  SemaHandler           *SemaH;
  DecoratorHandler        *DecoratorH;
  EvaluatorHandler      *EvaluatorH;
  ReducerHandler        *ReducerH;
  LegalizerHandler      *LegalizerH;
};

} //namespace builtin


struct BuiltinInfo {
  builtin::Funcs        Names;
  builtin::FuncAttrs    Attributes;
  builtin::Types        Returns;
  std::vector<builtin::Param> Params;
  builtin::RuntimeFuncs RuntimeNames;
  builtin::Handlers     Handlers;

  BuiltinInfo(const builtin::Funcs &F,
              const builtin::FuncAttrs &FA,
              const builtin::Types &T,
              const std::vector<builtin::Param> &P,
              const builtin::RuntimeFuncs &RTF = builtin::RuntimeFuncs(),
              const builtin::Handlers &H = builtin::Handlers());

  unsigned getNumRequiredArgs() const;
  unsigned getNumDefaultArgs() const;
  bool isVariadic() const;
  void record();
};


void init_builtin_functions();
void register_builtin_functions();
const BuiltinInfo *find_builtin_function(StringRef Name);

}
#endif
