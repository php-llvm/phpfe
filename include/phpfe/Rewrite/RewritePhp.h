//===--- RewritePhp.h -------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
// PHP code rewriter interface.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_REWRITE_REWRITEPHP_H
#define PHPFE_REWRITE_REWRITEPHP_H

//------ Dependencies ----------------------------------------------------------
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "phpfe/Sema/PhpSema.h"
#include <map>
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;

class RewritePhp : public RecursiveASTVisitor<RewritePhp> {
private:
  Rewriter Rewrite;
  Sema &Semantics;
  SourceManager &SM;

  DeclContext *CurContext;

  void replaceText(SourceLocation Start, unsigned OrigLength, StringRef Str);
  void insertText(SourceLocation Loc, StringRef Str, bool InsertAfter = true);

  void addFunctionComment(FunctionDecl *FD);
  void updateFunctionComment(FunctionDecl *FD, comments::FullComment *Comment);

public:
  RewritePhp(Sema &S);
  ~RewritePhp() {}

  std::string getRewriteResult();

  // Declarations
  bool VisitFunctionDecl(FunctionDecl *D);
  //bool Visit(Decl *D);

  // Statements
  bool VisitPhpEchoStmt(PhpEchoStmt *S);
  bool Visit(Stmt *S);

};


} // namespace phpfe

#endif
