//===--- Evaluator.h --------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Declaration of Evaluator, which is responsible constant evaluation at
//  compile time.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_ANALYZER_EVALUATOR_H
#define PHPFE_ANALYZER_EVALUATOR_H

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/StmtVisitor.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Support/ConstValue.h"
#include "conversion/number_recognizer.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


class Evaluator : public ConstStmtVisitor<Evaluator, ConstValue*>,
                  public RuntimeTypes {
  Sema &Semantics;
  ASTContext &Context;
  ConstantPool &Constants;

  bool SeenError;
  const Expr *OffendingExpression;

  // Constant conversion utilities.
  bool diagnoseConversion(ConstValue::ConversionStatus, SourceLocation Loc);
  bool diagnoseConversion(conversion::Status, SourceLocation Loc);
  bool diagnoseCompare(const BinaryOperator *E,
                       const ConstValue *LHS, const ConstValue *RHS);
  ConstValue *convertStringToNumber(StringRef Str, SourceLocation Loc);

public:

  Evaluator(Sema &S)
    : Semantics(S), Context(S.getASTContext()), Constants(S.getConstants()),
      SeenError(false), OffendingExpression(nullptr) {
  }

  ConstValue *evaluate(const Expr *E);
  ConstValue *cancelEvaluation(const Expr *OffendingExpr, bool CausedByError);

  ConstValue *convertToNumber(ConstValue *V, SourceLocation Loc);
  APInt convertToInt(const ConstValue *V, SourceLocation Loc);
  APFloat convertToFloat(const ConstValue *V, SourceLocation Loc);

  bool seenError() const { return SeenError; }
  const Expr *getOffendingExpr() const { return OffendingExpression; }
  Sema &getSema() const { return Semantics; }

  ConstValue *VisitExpr(const Expr *E);
  ConstValue *VisitParenExpr(const ParenExpr *E);
  ConstValue *VisitImplicitCastExpr(const ImplicitCastExpr *E);
  ConstValue *VisitCStyleCastExpr(const CStyleCastExpr *E);
  ConstValue *VisitCallExpr(const CallExpr *E);
  ConstValue *VisitOpaqueValueExpr(const OpaqueValueExpr *E);

  ConstValue *VisitConditionalOperator(const ConditionalOperator *E);
  ConstValue *VisitBinaryConditionalOperator(const BinaryConditionalOperator *E);

  ConstValue *VisitUnaryMinus(const UnaryOperator *E);
  ConstValue *VisitUnaryPlus(const UnaryOperator *E);
  ConstValue *VisitUnaryLNot(const UnaryOperator *E);
  ConstValue *VisitUnaryNot(const UnaryOperator *E);

  ConstValue *VisitBinAdd(const BinaryOperator *E);
  ConstValue *VisitBinSub(const BinaryOperator *E);
  ConstValue *VisitBinMul(const BinaryOperator *E);
  ConstValue *VisitBinDiv(const BinaryOperator *E);
  ConstValue *VisitBinRem(const BinaryOperator *E);
  ConstValue *VisitBinShl(const BinaryOperator *E);
  ConstValue *VisitBinShr(const BinaryOperator *E);
  ConstValue *VisitBinAnd(const BinaryOperator *E);
  ConstValue *VisitBinOr(const BinaryOperator *E);
  ConstValue *VisitBinXor(const BinaryOperator *E);

  ConstValue *VisitBinLAnd(const BinaryOperator *E);
  ConstValue *VisitBinPhpLAnd(const BinaryOperator *E);
  ConstValue *VisitBinLOr(const BinaryOperator *E);
  ConstValue *VisitBinPhpLOr(const BinaryOperator *E);
  ConstValue *VisitBinPhpLXor(const BinaryOperator *E);
  ConstValue *VisitBinPhpCoalesce(const BinaryOperator *E);
  ConstValue *VisitBinPhpConcat(const BinaryOperator *E);

  ConstValue *VisitBinLT(const BinaryOperator *E);
  ConstValue *VisitBinLE(const BinaryOperator *E);
  ConstValue *VisitBinGT(const BinaryOperator *E);
  ConstValue *VisitBinGE(const BinaryOperator *E);
  ConstValue *VisitBinEQ(const BinaryOperator *E);
  ConstValue *VisitBinNE(const BinaryOperator *E);
  ConstValue *VisitBinPhpIdent(const BinaryOperator *E);
  ConstValue *VisitBinPhpNotIdent(const BinaryOperator *E);
  ConstValue *VisitBinPhpSpaceship(const BinaryOperator *E);
  ConstValue *VisitBinPhpPower(const BinaryOperator *E);

  ConstValue *VisitPhpArrayExpr(const PhpArrayExpr *E);
  ConstValue *VisitPhpMapExpr(const PhpMapExpr *E);

  ConstValue *VisitIntegerLiteral(const IntegerLiteral *E);
  ConstValue *VisitStringLiteral(const StringLiteral *E);
  ConstValue *VisitFloatingLiteral(const FloatingLiteral *E);
  ConstValue *VisitCXXBoolLiteralExpr(const CXXBoolLiteralExpr *E);
  ConstValue *VisitPhpNullLiteral(const PhpNullLiteral *E);
};

}
#endif
