//===--- Decorator.h --------------------------------------------*- C++ -*-===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Component that deduces type of expressions.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_ANALYZER_DECORATOR_H
#define PHPFE_ANALYZER_DECORATOR_H

//------ Dependencies ----------------------------------------------------------
#include "clang/AST/StmtVisitor.h"
#include "phpfe/Sema/PhpSema.h"
#include "phpfe/Support/ConstValue.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;


/// \brief Keeps together a pointer to ConstValue and an error flag.
///
/// Using this type instead of pointer to ConstValue allows to distinguish cases
/// when constant evaluation failed because there is non-constant item in the
/// evaluated expression and the case when an error occurred during the
/// evaluation. In both cases evaluation procedure returns null pointer.
///
typedef ActionResult<ConstValue *, true> ValueResult;


/// \brief Component that "decorates" expression nodes with appropriate types.
///
/// The component is responsible for:
/// - calculation of expression type given the types of arguments and operation
///   kind. For instance, in absence of objects the result of concatenation
///   is always a string, the type of addition is float if at least one argument
///   is float etc.
/// - diagnostic for any type consistency problems, both errors and warnings.
/// - Fix of type issues by inserting type casts. They are necessary to avoid
///   repeating warning messages, as this component may be called several times
///   for he same piece of code.
///
/// The component is called from both semantics and reducer, this is why it is
/// made a separate component. It may be called more than once for the same
/// code. Strictly speaking it should not a part of analyzer as it modifies AST,
/// but analyzer is a good place for this component as it is accessible from
/// both Semantics and Transformer.
///
/// In general Decorator is not a recursive visitor, due to logic of using it by
/// Semantics and Reducer. They construct or modify AST down-top, arguments
// firs, then node that uses them. In this case arguments already have right
/// types and there is no need to call Decorator on children, just read the
/// their types.
class Decorator : public StmtVisitor<Decorator, QualType>,
                public RuntimeTypes {
  Sema &Semantics;
  bool Canceled;

  ValueResult getConstantValue(Expr *E);

  Expr *replaceByFloat(APFloat x, SourceLocation Loc);
  Expr *replaceByInt(APInt x, SourceLocation Loc);
  Expr *transformNull(QualType TargetType, Expr *E);
  Expr *transformToBool(Expr *E);
  Expr *transformToFloat(Expr *E);
  Expr *transformToInteger(Expr *E);
  Expr *transformStringToNumber(Expr *E);
  bool  processBinaryOperator(QualType &Type, BinaryOperator *E);
  bool  processNonArrayOperator(QualType &Type, BinaryOperator *E);
  void  processBitwiseOperator(QualType &Type, BinaryOperator *E);
  void  processIntegerOperator(QualType &Type, BinaryOperator *E);
  void  processArithmeticOperator(QualType &Type, BinaryOperator *E);
  void  processCompareOperator(QualType &Type, BinaryOperator *E);
  Expr *transformNumericExpr(QualType &Type, Expr *E, bool &NegativeBoundary);
  bool  processCompounsAssignment(QualType &Type, CompoundAssignOperator *E);
  bool  processArithmeticAssignment(QualType &Type, CompoundAssignOperator *E);

public:

  Decorator(Sema &S) : Semantics(S), Canceled(false) {}

  Sema &getSema() const { return Semantics; }

  QualType decorateByType(Expr *E);
  QualType cancel() { Canceled = true; return QualType(); }

  QualType VisitExpr(Expr *E);
//   QualType VisitParenExpr(const ParenExpr *E);
//   QualType VisitImplicitCastExpr(const ImplicitCastExpr *E);
//   QualType VisitCStyleCastExpr(const CStyleCastExpr *E);
  QualType VisitCallExpr(CallExpr *E);

  QualType VisitBinAssign(BinaryOperator *E);
  QualType VisitCompoundAssignOperator(CompoundAssignOperator *E);
  QualType VisitBinAddAssign(CompoundAssignOperator *E);
  QualType VisitBinSubAssign(CompoundAssignOperator *E);
  QualType VisitBinMulAssign(CompoundAssignOperator *E);
  QualType VisitBinDivAssign(CompoundAssignOperator *E);
  QualType VisitBinRemAssign(CompoundAssignOperator *E);
  QualType VisitBinShlAssign(CompoundAssignOperator *E);
  QualType VisitBinShrAssign(CompoundAssignOperator *E);
  QualType VisitBinPhpConcatAssign(CompoundAssignOperator *E);

  QualType VisitBinAdd(BinaryOperator *E);
  QualType VisitBinSub(BinaryOperator *E);
  QualType VisitBinMul(BinaryOperator *E);
  QualType VisitBinDiv(BinaryOperator *E);
  QualType VisitBinRem(BinaryOperator *E);
  QualType VisitBinShl(BinaryOperator *E);
  QualType VisitBinShr(BinaryOperator *E);
  QualType VisitBinAnd(BinaryOperator *E);
  QualType VisitBinOr(BinaryOperator *E);
  QualType VisitBinXor(BinaryOperator *E);
  QualType VisitBinPhpPower(BinaryOperator *E);

  QualType VisitBinLAnd(BinaryOperator *E);
  QualType VisitBinPhpLAnd(BinaryOperator *E);
  QualType VisitBinLOr(BinaryOperator *E);
  QualType VisitBinPhpLOr(BinaryOperator *E);
  QualType VisitBinPhpLXor(BinaryOperator *E);
  QualType VisitBinPhpCoalesce(BinaryOperator *E);
  QualType VisitBinPhpConcat(BinaryOperator *E);

  QualType VisitBinLT(BinaryOperator *E);
  QualType VisitBinLE(BinaryOperator *E);
  QualType VisitBinGT(BinaryOperator *E);
  QualType VisitBinGE(BinaryOperator *E);
  QualType VisitBinEQ(BinaryOperator *E);
  QualType VisitBinNE(BinaryOperator *E);
  QualType VisitBinPhpIdent(BinaryOperator *E);
  QualType VisitBinPhpNotIdent(BinaryOperator *E);
  QualType VisitBinPhpSpaceship(BinaryOperator *E);

  QualType VisitBinComma(BinaryOperator *E);

  QualType VisitUnaryMinus(UnaryOperator *E);
  QualType VisitUnaryPlus(UnaryOperator *E);
  QualType VisitUnaryLNot(UnaryOperator *E);
  QualType VisitUnaryNot(UnaryOperator *E);
  QualType VisitUnaryOperator(UnaryOperator *E);

  QualType VisitPhpArrayExpr(PhpArrayExpr *E);
  QualType VisitIntegerLiteral(IntegerLiteral *E);
  QualType VisitStringLiteral(StringLiteral *E);
  QualType VisitFloatingLiteral(FloatingLiteral *E);
  QualType VisitCXXBoolLiteralExpr(CXXBoolLiteralExpr *E);
  QualType VisitPhpNullLiteral(PhpNullLiteral *E);
};

}
#endif