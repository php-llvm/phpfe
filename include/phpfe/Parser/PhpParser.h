//===--- PhpParser.cpp - PHP Parser ---------------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  Defines interface to PHP parser.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_PARSER_PHPPARSER_H
#define PHPFE_PARSER_PHPPARSER_H

//------ Dependencies ----------------------------------------------------------
#include "clang/Parse/Parser.h"
#include "clang/Sema/Ownership.h"
#include "phpfe/Parser/OperatorPrecedence.h"
#include "phpfe/Sema/PhpSema.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;

/// \brief Parser class for parsing PHP sources.
///
/// Although parsing PHP sources is completely different than C++, making parser
/// class a descendant of C++ parser allows reusing parser infrastructure.
///
class Parser : public clang::Parser {
public:
  Parser(Preprocessor &PP, Sema &Actions, bool SkipFunctionBodies);
  ~Parser() override;

  void Initialize() override;
  bool ParseTopLevelDecl(DeclGroupPtrTy &Result) override;

  Sema &getActions() { return static_cast<Sema&>(clang::Parser::getActions()); }

  void skipBlock(bool SkipSemi = false);

  Decl *parsePHPUnit();
  void  parseTopStatements();
  void  parseTopStatement();
  StmtResult parseStatement();
  bool  parseEndOfStatement(const char *Stmt);

  StmtResult parseFunctionDeclaration(bool ParsingLambda = false);
  Decl *parseNamespace();

  StmtResult parseEchoStmt();
  StmtResult parseIfStatement();
  StmtResult parseWhileStatement();
  StmtResult parseCompoundStmt();
  void  parseHaltCompilerStmt();
  StmtResult parseReturnStatement();

  StmtResult parseFunctionDeclarationStmt(bool ParsingLambda = false);
  StmtResult parseStaticAssert();
  StmtResult parseConstantDeclaration();
  bool isStartOfLambdaFunction();
  bool isStartOfNamespace();
  bool parseLexicalVariableList();
  bool parseLexicalVariable();
  QualType parseHintType(bool IsParameter = true);
  bool parseParameterList(SourceLocation &LParen,
                          SmallVectorImpl<ParmVarDecl *> &Params,
                          SourceLocation &RParen);
  bool checkParameterName(bool &HasError);
  ParmVarDecl *parseParameter(SourceLocation &EllipsisLoc);
  ExprResult parseConstantExpression();

  ExprResult parseExpression();
  ExprResult parseSubExpression(PrecLevel LOpPrec);
  ExprResult parseBinaryOperand(Expr *LHS);
  ExprResult parsePostfixExpression();
  ExprResult parsePrimaryExpression(bool &IsVariable, bool AllowCall);
  ExprResult parsePostfixExpressionSuffix(Expr *Base, bool &IsVariable,
                                          bool AllowCall);
  ExprResult parsePostfixOperator(Expr *Base, bool IsVariable);
  ExprResult parseNumberLiteral(bool Negative);
  ExprResult parseInterpolatedStringLiteral();
  ExprResult parseObjectMemberAccess(Expr *Base);
  ExprResult parseNewExpression();
  ExprResult parseParenExpression();
  ExprResult parseSubscriptedExpression(Expr *Base, bool LHS);
  ExprResult parseBraceSubscriptedExpression(Expr *Base);
  ExprResult parseClassMemberReference(Expr *ClassName, bool &IsVariable);
  ExprResult parseCallExpression(Expr *Callee);
  ExprResult parseLambdaExpression();
  ExprResult parseArrayExpression();
  ExprResult parseReferenceExpression();
  ExprResult parseCastExpression();

  ExprResult parseSimpleVariable();
  ExprResult parseSimpleVariable(CXXScopeSpec SS);
  ExprResult parseSimpleVariable(ExprResult Base, bool IsStaticAccess);
  ExprResult parseSimpleVariable(CXXScopeSpec SS, ExprResult Base, bool isStatic);

  ExprResult parseNamedEntity();

  ExprResult parsePHPIncludeExpression();
  ExprResult parsePHPLambdaExpression();
  ExprResult parsePHPListExpression();
  ExprResult parsePHPMemberName(ExprResult Base);
  ExprResult parsePHPNamedEntity();
  ExprResult parsePHPNewExpression();
  ExprResult parsePHPYieldExpression();
  
  ExprResult parsePhpFunctionCall(CXXScopeSpec SS);
  ExprResult parsePHPStaticPropertyAccess(CXXScopeSpec SS);
  bool parseScopeSpecifier(CXXScopeSpec &SS, bool IsScopeName);

  bool parseExpressionList(SmallVectorImpl<Expr *> &Exprs);
  bool parseStatementList(SmallVectorImpl<Stmt *> &Stmts);

  bool isKeyword(const Token& Tok);
  bool isCastOperator();

  struct BraceExpressionInfo {
    ExprResult E;
    SourceLocation LBLoc;
    SourceLocation RBLoc;
  };
  BraceExpressionInfo parseBraceExpression(tok::TokenKind LKind, tok::TokenKind RKind, bool IsExprOptional = false);

  struct ClassNameReference {
    CXXScopeSpec SS;
    ExprResult ClassName;   // valid if class name represented as an expression
  };
  bool parsePHPClassNameReference(ClassNameReference &CR);
};


// Helper global functions.
bool isGlobalScopeSpecifier(const CXXScopeSpec &SS);

}
#endif
