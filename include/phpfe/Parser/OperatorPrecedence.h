//===--- OperatorPrecedence.cpp - PHP Parser ------------------------------===//
//
// phlang - the open source PHP compiler based on llvm/clang.
//
// Copyright(c) 2015, Serge Pavlov, Denis Polunin and Sergey Matvienko.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met :
//
// - Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// - Neither the name "phlang" nor the names of its contributors may be used to
// endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//===----------------------------------------------------------------------===//
//
//  PHP operator precedence and related functions.
//
//===----------------------------------------------------------------------===//
#ifndef PHPFE_PARSER_OPERATORPRECEDENCE_H
#define PHPFE_PARSER_OPERATORPRECEDENCE_H

//------ Dependencies ----------------------------------------------------------
#include "clang/Parse/Parser.h"
#include "clang/Sema/Ownership.h"
#include "phpfe/Sema/PhpSema.h"
//------------------------------------------------------------------------------

namespace phpfe {

using namespace clang;

// Operator precedence level.
enum PrecLevel {
  Unknown = 0,            // Not binary operator
  Include,                // include, include_once, require, require_once, eval
  LogicalOr,              // or
  LogicalXor,             // xor
  LogicalAnd,             // and
  Print,                  // unary: print
  Yield,                  // unary: yield
  DoubleArrow,            // =>
  YieldFrom,              // unary: yield from
  Assignment,             // =, += etc
  Conditional,            // ? :
  Coalesce,               // ??
  Or,                     // ||
  And,                    // &&
  BinaryOr,               // |
  BinaryXor,              // ^
  BinaryAnd,              // &
  Equality,               // ==, !=, ===, !==, <>, <=>
  Relational,             //  >=, <=, >, <
  Shift,                  // <<, >>
  Additive,               // -, +, .
  Multiplicative,         // *, /, %
  LogicalNot,             // Unary: !
  InstanceOf,             // instanceof
  Cast,                   // unary: ++, --, (type), @, ~, +, -
  Power,                  // **
  New,                    // unary: new, clone

  // According to PHP grammar, '[' has lower precedence than 'clone'. However
  // the expression 'clone $a[1]' is parsed as 'clone ($a[1])'. It is so because
  // in PHP '$a[1]' is parsed as variable. The expression 'clone $a' is not a
  // dereferencable and subscript cannot be applied to arbitrary expression.
  // As a workaround all postfix operators get maximal priority.
  //
  // The same problem exists for assignment. Expression 'if(!$a=$b)' is valid in
  // PHP although priority of '=' is lower than '!'. However '=' requires a
  // variable on the left side but '!$a' is not a variable. To cope with this
  // peculiarity of PHP grammar consider assignments as postfix operations too.
  Postfix                 // [, ++, --, =, += etc.
};


// Binary operator associativity.
enum class Assoc {
  None,
  Left,
  Right
};


inline PrecLevel getBinaryOpPrecedence(tok::TokenKind Kind) {
  switch (Kind) {
  case tok::star_star:           return Power;
  case tok::kw_instanceof:       return InstanceOf;
  case tok::star:
  case tok::slash:
  case tok::percent:             return Multiplicative;
  case tok::plus:
  case tok::minus:
  case tok::period:              return Additive;
  case tok::lessless:
  case tok::greatergreater:      return Shift;
  case tok::less:
  case tok::lessequal:
  case tok::greater:
  case tok::greaterequal:        return Relational;
  case tok::equalequal:
  case tok::exclaimequal:
  case tok::equal_equal_equal:
  case tok::exclaim_equal_equal:
  case tok::less_equal_greater:
  case tok::less_greater:        return Equality;
  case tok::amp:                 return BinaryAnd;
  case tok::caret:               return BinaryXor;
  case tok::pipe:                return BinaryOr;
  case tok::ampamp:              return And;
  case tok::pipepipe:            return Or;
  case tok::question_question:   return Coalesce;
  case tok::question:            return Conditional;
  case tok::kw_yield:            return Yield;
  case tok::kw_print:            return Print;
  case tok::kw_and:              return LogicalAnd;
  case tok::kw_xor:              return LogicalXor;
  case tok::kw_or:               return LogicalOr;
  case tok::kw_include:
  case tok::kw_include_once:
  case tok::kw_require:
  case tok::kw_require_once:     return Include;

  // Postfix operators
  case tok::equal:
  case tok::plusequal:
  case tok::minusequal:
  case tok::starequal:
  case tok::slashequal:
  case tok::periodequal:
  case tok::percentequal:
  case tok::ampequal:
  case tok::pipeequal:
  case tok::caretequal:
  case tok::lesslessequal:
  case tok::greatergreaterequal:
  case tok::star_star_equal:
  case tok::plusplus:
  case tok::minusminus:
  case tok::l_square:            return Postfix;

  default:
    return Unknown;
  }
}


inline PrecLevel getUnaryOpPrecedence(tok::TokenKind Kind) {
  switch (Kind) {
  case tok::kw_clone:
  case tok::kw_new:              return New;
  case tok::plusplus:
  case tok::minusminus:
  case tok::tilde:
  case tok::at:                  return Cast;
  case tok::exclaim:             return LogicalNot;
  case tok::plus:
  case tok::minus:               return Additive;
  case tok::kw_print:            return Print;
  case tok::kw_include:
  case tok::kw_include_once:
  case tok::kw_require:
  case tok::kw_require_once:     return Include;
  default:
    return Unknown;
  }
}


inline Assoc getAssociativity(tok::TokenKind Kind) {
  switch (Kind) {
  //------ Non associative operators.

  // Binary operators.
  case tok::equalequal:
  case tok::exclaimequal:
  case tok::equal_equal_equal:
  case tok::exclaim_equal_equal:
  case tok::less:
  case tok::lessequal:
  case tok::greater:
  case tok::greaterequal:
  case tok::less_greater:
  case tok::less_equal_greater:
  case tok::kw_instanceof:
    return Assoc::None;

  //------ Right associative operators.

  // Unary operators.

  // These operators are marked as non-associative in PHP yacc file, but it is
  // not true, as expression 'clone new ABC' compiles successfully in PHP.
  case tok::kw_clone:
  case tok::kw_new:

  case tok::kw_print:
  case tok::kw_yield:
  case tok::exclaim:
  case tok::tilde:
  case tok::plusplus:
  case tok::minusminus:
  case tok::at:

  // Binary operators.

  // In PHP grammar these operations are classified as left-associative. This is
  // not true, as expression '$a=$b=4' must be processed right to left. So
  // consider them as right-associative.

  case tok::equal:
  case tok::plusequal:
  case tok::minusequal:
  case tok::starequal:
  case tok::slashequal:
  case tok::periodequal:
  case tok::percentequal:
  case tok::ampequal:
  case tok::pipeequal:
  case tok::caretequal:
  case tok::lesslessequal:
  case tok::greatergreaterequal:
  case tok::star_star_equal:

  case tok::question_question:
  case tok::star_star:
    return Assoc::Right;

  //------ Left associative operators

  // Unary operators.
  //
  // These are marked as left-associative in PHP yacc file but it does not have
  // meaning. These operators are handled by separate grammar rules, so their
  // associativity does not matter.
  //
  //   case tok::kw_include:
  //   case tok::kw_include_once:
  //   case tok::kw_require:
  //   case tok::kw_require_once:

  // Binary operators.
  case tok::kw_and:
  case tok::kw_xor:
  case tok::kw_or:
  case tok::question:
  case tok::colon:
  case tok::ampamp:
  case tok::pipepipe:
  case tok::pipe:
  case tok::caret:
  case tok::amp:
  case tok::lessless:
  case tok::greatergreater:
  case tok::plus:
  case tok::minus:
  case tok::period:
  case tok::star:
  case tok::slash:
  case tok::percent:
    return Assoc::Left;

  default:
    llvm_unreachable("unexpected operator");
  }
}

}
#endif
