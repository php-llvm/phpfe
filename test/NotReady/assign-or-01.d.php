// RUN: %clang_php %s -verify
<?php

function static_assert($a) { assert($a); } // expected-warning{{static_assert has special treatment that cannot be redefined}}

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }

function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_bool_01(bool $a) {
  assert($a === true);

  $a |= false; assert($a === false);
  $a |= true;  assert($a === false);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a |= null;    assert($a === false); // expected-warning{{implicit convertion from NULL to integer}}
  $a = true; $a |= 123;     assert($a === true);
  $a = true; $a |= 12.34;   assert($a === false); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = true; $a |= "qwe";   assert($a === false); // expected-warning{{implicit conversion from 'string' to 'integer'}} 
  $a = true; $a |= [];      // expected-error{{array value is not compiatible with boolean type}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a |= get_void();   assert($a === true);  // expected-warning{{implicit convertion from NULL to integer}}
  $a = true; $a |= get_bool();   assert($a === false);
  $a = true; $a |= get_int();    assert($a === false);
  $a = true; $a |= get_double(); assert($a === false); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = true; $a |= get_string(); assert($a === false); // expected-warning{{implicit conversion from 'string' to 'integer'}}
  $a = true; $a |= get_array();                        // expected-error{{array value is not compiatible with boolean type}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a |= get_bool_ref();   assert($a === false);
  $a = true; $a |= get_int_ref();    assert($a === false);
  $a = true; $a |= get_double_ref(); assert($a === false); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = true; $a |= get_string_ref(); assert($a === false); // expected-warning{{implicit conversion from 'string' to 'integer'}}
  $a = true; $a |= get_array_ref();                        // expected-error{{array value is not compiatible with boolean type}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a = true; $a |= $b; assert($a === true);
  $b = true;    $a = true; $a |= $b; assert($a === false);
  $b = 123;     $a = true; $a |= $b; assert($a === false);
  $b = 12.45;   $a = true; $a |= $b; assert($a === false);
  $b = 'qwe';   $a = true; $a |= $b; assert($a === true);
  $b = [1,2,3]; $a = true; $a |= $b; assert($a === false);
}
check_assign_to_bool_01(true);


function check_assign_to_bool_02(bool &$a) {
  assert($a === true);

  $a |= false;                       // 
  $a |= true;  assert($a === true);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a |= null;    // expected-warning{{implicit convertion from NULL to integer}}
  $a = true; $a |= 123;     assert($a === false);
  $a = true; $a |= 12.34;   assert($a === false); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = true; $a |= "qwe";   // expected-warning{{implicit conversion from 'string' to 'integer'}} 
  $a = true; $a |= [];      // expected-error{{array value is not compiatible with boolean type}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a |= get_void();   assert($a === true);  // expected-warning{{implicit convertion from NULL to integer}}
  $a = true; $a |= get_bool();   assert($a === false);
  $a = true; $a |= get_int();    assert($a === false);
  $a = true; $a |= get_double(); assert($a === false); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = true; $a |= get_string(); assert($a === false); // expected-warning{{implicit conversion from 'string' to 'integer'}}
  $a = true; $a |= get_array();                        // expected-error{{array value is not compiatible with boolean type}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a |= get_bool_ref();   assert($a === false);
  $a = true; $a |= get_int_ref();    assert($a === false);
  $a = true; $a |= get_double_ref(); assert($a === false); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = true; $a |= get_string_ref(); assert($a === false); // expected-warning{{implicit conversion from 'string' to 'integer'}}
  $a = true; $a |= get_array_ref();                        // expected-error{{array value is not compiatible with boolean type}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a = true; $a |= $b; assert($a === true);
  $b = true;    $a = true; $a |= $b; assert($a === false);
  $b = 123;     $a = true; $a |= $b; assert($a === false);
  $b = 12.45;   $a = true; $a |= $b; assert($a === false);
  $b = 'qwe';   $a = true; $a |= $b; assert($a === true);
  $b = [1,2,3]; $a = true; $a |= $b; assert($a === false);
}
$a = true;
check_assign_to_bool_02($a);








function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a |= 456;  assert($a === -333);
  $a |= -12;  assert($a === -321);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a |= null;                         // expected-warning{{implicit convertion from NULL to integer}}
  $a |= false;                        // 
  $a |= 123;   assert($a === -444);
  $a |= 12.34; assert($a === -456);   // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a |= "qwe"; assert($a === -456);   // expected-warning{{implicit conversion from 'string' to 'integer'}} 
  $a |= [1,2,3];                      // expected-error{{array value is not compiatible with integer type}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a |= get_void();   assert($a === -456);  // expected-warning{{implicit convertion from NULL to integer}}
  $a |= get_bool();   assert($a === -457);
  $a |= get_int();    assert($a === -579);
  $a |= get_double(); assert($a === -591);  // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a |= get_string(); assert($a === -591);  // expected-warning{{implicit conversion from 'string' to 'integer'}}
  $a |= get_array();                        // expected-error{{array value is not compiatible with integer type}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a |= get_bool_ref();   assert($a === -592);
  $a |= get_int_ref();    assert($a === -715);
  $a |= get_double_ref(); assert($a === -727);  // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a |= get_string_ref(); assert($a === -727);  // expected-warning{{implicit conversion from 'string' to 'integer'}}
  $a |= get_array_ref(); assert($a === -727);   // expected-error{{array value is not compiatible with integer type}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a |= $b; assert($a === -727);
  $b = true;  $a |= $b; assert($a === -728);
  $b = 123;   $a |= $b; assert($a === -851);
  $b = 12.45; $a |= $b; assert($a === -863);
  $b = 'qwe';   $a |= $b; assert($a === -863);
  $b = [1,2,3]; $a |= $b;
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a |= 456;  assert($a === -333);
  $a |= -12;  assert($a === -321);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a |= null;                         // expected-warning{{implicit convertion from NULL to integer}}
  $a |= false;                        // 
  $a |= 123;   assert($a === -444);
  $a |= 12.34; assert($a === -456);   // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a |= "qwe"; assert($a === -456);   // expected-warning{{implicit conversion from 'string' to 'integer'}} 
  $a |= [1,2,3];                      // expected-error{{array value is not compiatible with integer type}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a |= get_void();   assert($a === -456);  // expected-warning{{implicit convertion from NULL to integer}}
  $a |= get_bool();   assert($a === -457);
  $a |= get_int();    assert($a === -579);
  $a |= get_double(); assert($a === -591);  // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a |= get_string(); assert($a === -591);  // expected-warning{{implicit conversion from 'string' to 'integer'}}
  $a |= get_array();                        // expected-error{{array value is not compiatible with integer type}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a |= get_bool_ref();   assert($a === -592);
  $a |= get_int_ref();    assert($a === -715);
  $a |= get_double_ref(); assert($a === -727);  // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a |= get_string_ref(); assert($a === -727);  // expected-warning{{implicit conversion from 'string' to 'integer'}}
  $a |= get_array_ref(); assert($a === -727);   // expected-error{{array value is not compiatible with integer type}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a |= $b; assert($a === -727);
  $b = true;  $a |= $b; assert($a === -728);
  $b = 123;   $a |= $b; assert($a === -851);
  $b = 12.45; $a |= $b; assert($a === -863);
  $b = 'qwe';   $a |= $b; assert($a === -863);
  $b = [1,2,3]; $a |= $b;
}
$a = 123;
check_assign_to_int_02($a);








function eq(double $a, double $b) : bool { return abs($a - $b) < 1e-10; }

function check_assign_to_double_01(float $a) {
  assert($a === 123.456);

  $a |= 456.5;   assert($a === -333.044); // expected-error{{expression cannot be converted to integer type}}
  $a |= -12.258; assert($a === -345.302); // expected-error{{expression cannot be converted to integer type}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a |= null;  assert($a === -345.302);   // expected-error{{expression cannot be converted to integer type}}
  $a |= false; assert($a === -345.302);   // expected-error{{expression cannot be converted to integer type}}
  $a |= 123;   assert($a === -468.302);   // expected-error{{expression cannot be converted to integer type}}
  $a |= 12.34; assert($a === -480.642);   // expected-error{{expression cannot be converted to integer type}}
  $a |= "qwe";                            // expected-error{{expression cannot be converted to integer type}}
  $a |= [];                               // expected-error{{expression cannot be converted to integer type}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a |= get_void();   assert($a === -480.642); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_bool();   assert($a === -481.642); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_int();    assert($a === -604.642); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_double(); assert($a === -616.982); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_string(); assert($a === -616.982); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_array();                           // expected-error{{expression cannot be converted to integer type}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a |= get_bool_ref();   assert($a === -617.982);  // expected-error{{expression cannot be converted to integer type}}
  $a |= get_int_ref();    assert($a === -740.982);  // expected-error{{expression cannot be converted to integer type}}
  $a |= get_double_ref(); assert(eq($a, -753.322)); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_string_ref(); assert($a === -753.322);  // expected-error{{expression cannot be converted to integer type}}
  $a |= get_array_ref();                            // expected-error{{expression cannot be converted to integer type}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a |= $b; assert($a === -753.322);   // expected-error{{expression cannot be converted to integer type}}
  $b = true;    $a |= $b; assert(eq($a, -754.322));  // expected-error{{expression cannot be converted to integer type}}
  $b = 123;     $a |= $b; assert(eq($a, -877.322));  // expected-error{{expression cannot be converted to integer type}}
  $b = 12.45;   $a |= $b; assert(eq($a, -889.772));  // expected-error{{expression cannot be converted to integer type}}
  $b = 'qwe';   $a |= $b; assert($a === -889.772);   // expected-error{{expression cannot be converted to integer type}}
  $b = [1,2,3]; $a |= $b;                            // expected-error{{expression cannot be converted to integer type}}
}

check_assign_to_double_01(123.456);

function check_assign_to_double_02(float &$a) {
  assert($a === 123.456);

  $a |= 456.5;   assert($a === -333.044); // expected-error{{expression cannot be converted to integer type}}
  $a |= -12.258; assert($a === -345.302); // expected-error{{expression cannot be converted to integer type}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a |= null;  assert($a === -345.302);   // expected-error{{expression cannot be converted to integer type}}
  $a |= false; assert($a === -345.302);   // expected-error{{expression cannot be converted to integer type}}
  $a |= 123;   assert($a === -468.302);   // expected-error{{expression cannot be converted to integer type}}
  $a |= 12.34; assert($a === -480.642);   // expected-error{{expression cannot be converted to integer type}}
  $a |= "qwe";                            // expected-error{{expression cannot be converted to integer type}}
  $a |= [];                               // expected-error{{expression cannot be converted to integer type}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a |= get_void();   assert($a === -480.642); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_bool();   assert($a === -481.642); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_int();    assert($a === -604.642); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_double(); assert($a === -616.982); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_string(); assert($a === -616.982); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_array();                           // expected-error{{expression cannot be converted to integer type}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a |= get_bool_ref();   assert($a === -617.982);  // expected-error{{expression cannot be converted to integer type}}
  $a |= get_int_ref();    assert($a === -740.982);  // expected-error{{expression cannot be converted to integer type}}
  $a |= get_double_ref(); assert(eq($a, -753.322)); // expected-error{{expression cannot be converted to integer type}}
  $a |= get_string_ref(); assert($a === -753.322);  // expected-error{{expression cannot be converted to integer type}}
  $a |= get_array_ref();                            // expected-error{{expression cannot be converted to integer type}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a |= $b; assert($a === -753.322);   // expected-error{{expression cannot be converted to integer type}}
  $b = true;    $a |= $b; assert(eq($a, -754.322));  // expected-error{{expression cannot be converted to integer type}}
  $b = 123;     $a |= $b; assert(eq($a, -877.322));  // expected-error{{expression cannot be converted to integer type}}
  $b = 12.45;   $a |= $b; assert(eq($a, -889.772));  // expected-error{{expression cannot be converted to integer type}}
  $b = 'qwe';   $a |= $b; assert($a === -889.772);   // expected-error{{expression cannot be converted to integer type}}
  $b = [1,2,3]; $a |= $b;                            // expected-error{{expression cannot be converted to integer type}}
}
$a = 123.456;
check_assign_to_double_02($a);








//TODO: add warninigs for <string> |= anything?
function check_assign_to_string_01(string $a) {
  assert($a === 'qwe');

  $a |= 'zxc';   
  $a |= '';      

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a |= null;    // expected-error{{value of NULL type cannot be used on right hand side of binary operator}}
  $a |= false;   // expected-error{{value of boolean type cannot be used on right hand side of binary operator}}
  $a |= 123;     // expected-error{{value of integer type cannot be used on right hand side of binary operator}}
  $a |= 12.34;   // expected-error{{value of double type cannot be used on right hand side of binary operator}}
  $a |= "qwe";   

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a |= get_void();   // expected-error{{value of NULL type cannot be used on right hand side of binary operator}}
  $a |= get_bool();   // expected-error{{value of boolean type cannot be used on right hand side of binary operator}}
  $a |= get_int();    // expected-error{{value of integer type cannot be used on right hand side of binary operator}}
  $a |= get_double(); // expected-error{{value of double type cannot be used on right hand side of binary operator}}
  $a |= get_string(); 

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a |= get_bool_ref();   // expected-error{{value of boolean type cannot be used on right hand side of binary operator}}
  $a |= get_int_ref();    // expected-error{{value of integer type cannot be used on right hand side of binary operator}}
  $a |= get_double_ref(); // expected-error{{value of double type cannot be used on right hand side of binary operator}}
  $a |= get_string_ref(); 

//-------------------------------------------------------------------
// assign box to string
//-------------------------------------------------------------------
  $b = null;    $a |= $b; 
  $b = true;    $a |= $b; 
  $b = 123;     $a |= $b; 
  $b = 12.45;   $a |= $b; 
  $b = 'qwe';   $a |= $b; 
  $b = [1,2,3]; $a |= $b; 
}
check_assign_to_string_01('qwe');

function check_assign_to_string_02(string &$a) {
  assert($a === 'qwe');

  $a |= 'zxc';   
  $a |= '';      

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a |= null;    // expected-error{{value of NULL type cannot be used on right hand side of binary operator}}
  $a |= false;   // expected-error{{value of boolean type cannot be used on right hand side of binary operator}}
  $a |= 123;     // expected-error{{value of integer type cannot be used on right hand side of binary operator}}
  $a |= 12.34;   // expected-error{{value of double type cannot be used on right hand side of binary operator}}
  $a |= "qwe";   

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a |= get_void();   // expected-error{{value of NULL type cannot be used on right hand side of binary operator}}
  $a |= get_bool();   // expected-error{{value of boolean type cannot be used on right hand side of binary operator}}
  $a |= get_int();    // expected-error{{value of integer type cannot be used on right hand side of binary operator}}
  $a |= get_double(); // expected-error{{value of double type cannot be used on right hand side of binary operator}}
  $a |= get_string(); 

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a |= get_bool_ref();   // expected-error{{value of boolean type cannot be used on right hand side of binary operator}}
  $a |= get_int_ref();    // expected-error{{value of integer type cannot be used on right hand side of binary operator}}
  $a |= get_double_ref(); // expected-error{{value of double type cannot be used on right hand side of binary operator}}
  $a |= get_string_ref(); 

//-------------------------------------------------------------------
// assign box to string
//-------------------------------------------------------------------
  $b = null;    $a |= $b; 
  $b = true;    $a |= $b; 
  $b = 123;     $a |= $b; 
  $b = 12.45;   $a |= $b; 
  $b = 'qwe';   $a |= $b; 
  $b = [1,2,3]; $a |= $b;  
}
$a = 'qwe';
check_assign_to_string_02($a);






function check_assign_to_array_01(array $a) {
  assert($a === [1,2,3]);

  $a |= [4,5,6]; // expected-error{{value of array type cannot be used on left hand side of binary operator}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a |= null;    // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= false;   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= 123;     // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= 12.34;   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= "qwe";   // expected-error{{value of array type cannot be used on left hand side of binary operator}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a |= get_void();   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_bool();   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_int();    // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_double(); // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_string(); // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_array();  // expected-error{{value of array type cannot be used on left hand side of binary operator}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a |= get_bool_ref();   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_int_ref();    // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_double_ref(); // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_string_ref(); // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_array_ref();  // expected-error{{value of array type cannot be used on left hand side of binary operator}}

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = true;    $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = 123;     $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = 12.45;   $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = 'qwe';   $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = [1,2,3]; $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
}
check_assign_to_array_01([1,2,3]);

function check_assign_to_array_02(array &$a) {
  assert($a === [1,2,3]);

  $a |= [4,5,6]; // expected-error{{value of array type cannot be used on left hand side of binary operator}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a |= null;    // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= false;   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= 123;     // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= 12.34;   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= "qwe";   // expected-error{{value of array type cannot be used on left hand side of binary operator}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a |= get_void();   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_bool();   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_int();    // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_double(); // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_string(); // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_array();  // expected-error{{value of array type cannot be used on left hand side of binary operator}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a |= get_bool_ref();   // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_int_ref();    // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_double_ref(); // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_string_ref(); // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $a |= get_array_ref();  // expected-error{{value of array type cannot be used on left hand side of binary operator}}

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = true;    $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = 123;     $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = 12.45;   $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = 'qwe';   $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
  $b = [1,2,3]; $a |= $b; // expected-error{{value of array type cannot be used on left hand side of binary operator}}
}
$a = [1,2,3];
check_assign_to_array_02($a);







function check_assign_to_universal_01($a) {
  $a |= [4,5,6];

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a |= null;                                     // 
  $a = 123; $a |= false;  assert($a === 0);
  $a = 123; $a |= 123;    assert($a === 15129);
  $a = 123; $a |= 12.34;  assert($a === 1517.82); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a |= "qwe";

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = 123; $a |= get_void();   // 
  $a = 123; $a |= get_bool();   assert($a === 123);
  $a = 123; $a |= get_int();    assert($a === 15129);
  $a = 123; $a |= get_double(); assert($a === 1517.82); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = 123; $a |= get_string();
  $a = 123; $a |= get_array();

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = 123; $a |= get_bool_ref();   assert($a === 123);
  $a = 123; $a |= get_int_ref();    assert($a === 15129);
  $a = 123; $a |= get_double_ref(); assert($a === 1517.82); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a |= get_string_ref();
  $a |= get_array_ref(); 

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a |= $b;
  $b = true;  $a = 123; $a |= $b; assert($a === 123);
  $b = 123;   $a = 123; $a |= $b; assert($a === 15129);
  $b = 12.45; $a = 123; $a |= $b; assert($a === 1531.35);
  $b = 'qwe';   $a |= $b;
  $b = [1,2,3]; $a |= $b;
}
check_assign_to_universal_01(123);


?>