// RUN: %clang_php %s -verify
<?php

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_bool_01(bool $a) {
  $a = true; $a -= [];                            // expected-error{{operation cannot be applied to arrays}}
  $a = true; $a -= get_array();                        // expected-error{{operation cannot be applied to arrays}}
  $a = true; $a -= get_array_ref();                        // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a = true; $a -= $b; assert($a === true);
  $b = true;    $a = true; $a -= $b; assert($a === false);
  $b = 123;     $a = true; $a -= $b; assert($a === false);
  $b = 12.45;   $a = true; $a -= $b; assert($a === false);
  $b = 'qwe';   $a = true; $a -= $b; assert($a === true);
  $b = [1,2,3]; $a = true; $a -= $b; assert($a === false);
}
check_assign_to_bool_01(true);


function check_assign_to_bool_02(bool &$a) {
  $a = true; $a -= [];                            // expected-error{{operation cannot be applied to arrays}}
  $a = true; $a -= get_array();                        // expected-error{{operation cannot be applied to arrays}}
  $a = true; $a -= get_array_ref();                         // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a = true; $a -= $b; assert($a === true);
  $b = true;    $a = true; $a -= $b; assert($a === false);
  $b = 123;     $a = true; $a -= $b; assert($a === false);
  $b = 12.45;   $a = true; $a -= $b; assert($a === false);
  $b = 'qwe';   $a = true; $a -= $b; assert($a === true);
  $b = [1,2,3]; $a = true; $a -= $b; assert($a === false);
}
check_assign_to_bool_02($a);


function check_assign_to_int_01(int $a) {
  $a -= [1,2,3];                      // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array();                        // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array_ref(); assert($a === -727);   // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a -= $b; assert($a === -727);
  $b = true;  $a -= $b; assert($a === -728);
  $b = 123;   $a -= $b; assert($a === -851);
  $b = 12.45; $a -= $b; assert($a === -863);
  $b = 'qwe';   $a -= $b; assert($a === -863);
  $b = [1,2,3]; $a -= $b;
}
check_assign_to_int_01(123);


function check_assign_to_int_02(int &$a) {
  $a -= [1,2,3];                      // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array();                        // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array_ref(); assert($a === -727);   // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a -= $b; assert($a === -727);
  $b = true;  $a -= $b; assert($a === -728);
  $b = 123;   $a -= $b; assert($a === -851);
  $b = 12.45; $a -= $b; assert($a === -863);
  $b = 'qwe';   $a -= $b; assert($a === -863);
  $b = [1,2,3]; $a -= $b;
}
check_assign_to_int_02($a);


function eq(double $a, double $b) : bool { return abs($a - $b) < 1e-10; }

function check_assign_to_double_01(float $a) {
  $a -= [];                               // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array();                          // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array_ref();                       // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a -= $b; assert($a === -753.322);
  $b = true;    $a -= $b; assert(eq($a, -754.322));
  $b = 123;     $a -= $b; assert(eq($a, -877.322));
  $b = 12.45;   $a -= $b; assert(eq($a, -889.772));
  $b = 'qwe';   $a -= $b; assert($a === -889.772);
  $b = [1,2,3]; $a -= $b;
}
check_assign_to_double_01(123.456);


function check_assign_to_double_02(float &$a) {
  $a -= [];                               // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array();                          // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array_ref();                       // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a -= $b; assert($a === -753.322);
  $b = true;    $a -= $b; assert(eq($a, -754.322));
  $b = 123;     $a -= $b; assert(eq($a, -877.322));
  $b = 12.45;   $a -= $b; assert(eq($a, -889.772));
  $b = 'qwe';   $a -= $b; assert($a === -889.772);
  $b = [1,2,3]; $a -= $b;
}
check_assign_to_double_02($a);


function check_assign_to_string_02(string &$a) {
  $a -= get_array();  // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array_ref();  // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign box to string
//-------------------------------------------------------------------
  $b = null;    $a -= $b;
  $b = true;    $a -= $b;
  $b = 123;     $a -= $b;
  $b = 12.45;   $a -= $b;
  $b = 'qwe';   $a -= $b;
  $b = [1,2,3]; $a -= $b;
}
check_assign_to_string_02($a);


function check_assign_to_array_01(array $a) {
  assert($a === [1,2,3]);

  $a -= [4,5,6]; // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;    // expected-error{{operation cannot be applied to arrays}}
  $a -= false;   // expected-error{{operation cannot be applied to arrays}}
  $a -= 123;     // expected-error{{operation cannot be applied to arrays}}
  $a -= 12.34;   // expected-error{{operation cannot be applied to arrays}}
  $a -= "qwe";   // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   // expected-error{{operation cannot be applied to arrays}}
  $a -= get_bool();   // expected-error{{operation cannot be applied to arrays}}
  $a -= get_int();    // expected-error{{operation cannot be applied to arrays}}
  $a -= get_double(); // expected-error{{operation cannot be applied to arrays}}
  $a -= get_string(); // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array();  // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   // expected-error{{operation cannot be applied to arrays}}
  $a -= get_int_ref();    // expected-error{{operation cannot be applied to arrays}}
  $a -= get_double_ref(); // expected-error{{operation cannot be applied to arrays}}
  $a -= get_string_ref(); // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array_ref();  // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a -= $b;
  $b = true;    $a -= $b;
  $b = 123;     $a -= $b;
  $b = 12.45;   $a -= $b;
  $b = 'qwe';   $a -= $b;
  $b = [1,2,3]; $a -= $b;
}
check_assign_to_array_01([1,2,3]);


function check_assign_to_array_02(array &$a) {
  assert($a === [1,2,3]);

  $a -= [4,5,6]; // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;    // expected-error{{operation cannot be applied to arrays}}
  $a -= false;   // expected-error{{operation cannot be applied to arrays}}
  $a -= 123;     // expected-error{{operation cannot be applied to arrays}}
  $a -= 12.34;   // expected-error{{operation cannot be applied to arrays}}
  $a -= "qwe";   // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   // expected-error{{operation cannot be applied to arrays}}
  $a -= get_bool();   // expected-error{{operation cannot be applied to arrays}}
  $a -= get_int();    // expected-error{{operation cannot be applied to arrays}}
  $a -= get_double(); // expected-error{{operation cannot be applied to arrays}}
  $a -= get_string(); // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array();  // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   // expected-error{{operation cannot be applied to arrays}}
  $a -= get_int_ref();    // expected-error{{operation cannot be applied to arrays}}
  $a -= get_double_ref(); // expected-error{{operation cannot be applied to arrays}}
  $a -= get_string_ref(); // expected-error{{operation cannot be applied to arrays}}
  $a -= get_array_ref();  // expected-error{{operation cannot be applied to arrays}}

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a -= $b;
  $b = true;    $a -= $b;
  $b = 123;     $a -= $b;
  $b = 12.45;   $a -= $b;
  $b = 'qwe';   $a -= $b;
  $b = [1,2,3]; $a -= $b;
}
$a = [1,2,3];
check_assign_to_array_02($a);

?>