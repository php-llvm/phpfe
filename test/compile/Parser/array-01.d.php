// RUN: %clang_php %s -verify
<?php

//----------------------------------------------------------------------------- 
// implicit convertion to long
//----------------------------------------------------------------------------- 
$a = [
         0, // expected-note{{previous implicit value generated here}}
    0 => 1, // expected-error{{duplicate key value}}
];

$a = [
             0, // expected-note{{previous implicit value generated here}}
    false => 1, // expected-warning{{implicit conversion from 'boolean' to 'integer'}} \
                   expected-error{{duplicate key value}}
];

$a = [
           0, // expected-note{{previous implicit value generated here}}
    '0' => 1, // expected-warning{{implicit conversion from 'string' to 'integer'}} \
                 expected-error{{duplicate key value}}
];

$a = [
          0, // expected-note{{previous implicit value generated here}}
    00 => 1, // expected-error{{duplicate key value}}
];

$a = [
           0, // expected-note{{previous implicit value generated here}}
    0x0 => 1, // expected-error{{duplicate key value}}
];

$a = [
           0, // expected-note{{previous implicit value generated here}}
    0.5 => 1, // expected-warning{{implicit conversion from 'double' to 'integer'}} \
                 expected-error{{duplicate key value}}
];

/*TODO: add reducer
$a = [
            0,
    -0.5 => 1
];*/

$a = [
           0, // expected-note{{previous implicit value generated here}}
    0.1 => 1, // expected-warning{{implicit conversion from 'double' to 'integer'}} \
                 expected-error{{duplicate key value}}
];

$a = [
           0, // expected-note{{previous implicit value generated here}}
    0.9 => 1, // expected-warning{{implicit conversion from 'double' to 'integer'}} \
                 expected-error{{duplicate key value}}
];


$a = [
    //-123    => 0,
    "-123"  => 1   // expected-warning{{implicit conversion from 'string' to 'integer'}}
];

//----------------------------------------------------------------------------- 
// implicit convertion to string
//----------------------------------------------------------------------------- 
$a = [
    ''   => 0, // expected-note{{previous value}}
    null => 1, // expected-warning{{implicit conversion from 'NULL' to 'string'}} \
                  expected-error{{duplicate key value}}
];

//----------------------------------------------------------------------------- 
// no convertion
//----------------------------------------------------------------------------- 

$a = [
    123       => 1,
    '123qwe'  => 2,
    ' 123'    => 3,
    '123 '    => 4,
    ' + 123'  => 5,
    '+123'    => 6,
    ' - 123'  => 7,
    '- 123'   => 8,
    '-123 '   => 9,
    '123.456' => 10,
    '123 456' => 11,
];

?>