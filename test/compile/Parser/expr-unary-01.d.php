// RUN: %clang_php %s -verify
<?php 

//-------------------------------------------------------------------
// PHP primary expression parser tests
//-------------------------------------------------------------------

//-------------------------------------------------------------------
// clone
//-------------------------------------------------------------------
//class Test {}
//$a = new Test;
clone $a;
clone f();
clone f($a);
clone f(1,2,3);
clone \f(1,2,3);
clone \A\B\C\f(1,2,3);
clone A::f();
clone \A::f();
clone \A\B\C::f();

clone \A\123; // expected-error {{expected identifier}}
clone A\B::123; // expected-error {{expected static member}}
//TODO: clone A\B::{$b = $a};
clone A\B::$a;
clone A\B::$$$$a;

$b = clone $a . $b; // <=> (clone $a) . $b
$b = clone $a[1];   // <=> (clone $a)[1]

$o = new Test;
$v = clone new Test;

$a = new Test;
$v = clone $a ? 1 : 2; // <=> $v = ((clone $a) ? 1 : 2)
assert($v === 1);

$a = new Test;
$b = "qwe";
$v = clone $a . $b;
assert($v === "10007qwe");


$b = new Test;
$vv = (clone $b)["qwe"]; // <=> $v = (clone ($a["qwe"]))
var_dump($b, $vv);

$a = new Test;
$v = clone $a["qwe"]; // <=> $v = (clone ($a["qwe"]))
var_dump($a, $v);
assert($v instanceof Test);
//assert($v->id === 10009);
//assert($v->sub === "qwe");

$a = new Test;
$b = new Test;
$v = clone $a[$b] . "qwe"; // <=> $v = ((clone ($a[new Test])) . "qwe");

assert($v === "10012qwe");

//-------------------------------------------------------------------
// clone runtime test
//-------------------------------------------------------------------
/*class Test implements ArrayAccess {
    public $id;

    public function __construct() {
        static $a = 0;
        $this->id = ++$a;
    }

    public function offsetExists($offset) {
        return true;
    }

    public function offsetGet($offset) {
        echo "[" . $this->id . "] -> offsetGet\n";
        $this->id += 100;
        return $this;
    }

    public function offsetSet($offset, $value) {
        echo "[$offset] -> $value\n";
    }

    public function offsetUnset($offset) {
        echo "unset [$offset]\n";
    }

    public function __clone() {
        echo "[" . $this->id . "] -> __clone\n";
    }
}

echo " ---------- Test 1\n";
$a = new Test;
$v = (clone $a)['qwe'];
var_dump($a, $v);


echo " ---------- Test 2\n";
$a = new Test;
$v = clone $a['qwe']; // <=> $v = (clone ($a['qwe']))
var_dump($a, $v);


echo " ---------- Test 3\n";
$a = new Test;
$v = clone $a ? 1 : 2; // <=> $v = ((clone $a) ? 1 : 2)
var_dump($a, $v);
*/

//-------------------------------------------------------------------
// new
//-------------------------------------------------------------------
//new static;         // IsClassNestedScopeSpecifier is not implemented yet 
//new static(1,2,3);  // IsClassNestedScopeSpecifier is not implemented yet
new A;
new A\B\C;
new \A\B\C;
new namespace \A\B\C;

new $a;
new $a(1,2,3);
new $$$a;

new $a{123};
new $a{123}{456}{789};

new $a[];
new $a[123];
new $a[1][2][3];

new $a->abc;
new $a::$b;

new $a[1]->b{"qwe"}::$$$c->{123}->$$$b;


$v = new Test . new Test;
assert($v === "12");

/*$a = [ "Test" ];
var_dump(new $a[]); //runtime error*/

$v = new $a[0] . "_qwe";
assert($v === "3_qwe");

$a = "Te"; $b = "st";
$v = new $a . $b;
assert($v === "<Te>st");


new ::A; // expected-error {{expected class name}}
new A::; // expected-error {{expected static member}}

new $a[; // expected-error {{expected expression}}
         // expected-error@-1 {{expected ']'}}
         // expected-note@-2 {{to match this '['}}

new $a[$b;  // expected-error {{expected ']'}}
            // expected-note@-1 {{to match this '['}}

new $a{}; // expected-error {{expected expression}}
new $a{;  // expected-error {{expected expression}}
          // expected-error@-1 {{expected '}'}}
          // expected-note@-2 {{to match this '{'}}
new $a{$b;// expected-error {{expected '}'}}
          // expected-note@-1 {{to match this '{'}}

new $a->;   // expected-error {{expected '$'}}
new $a->123;// expected-error {{expected '$'}}
new $a::;   // expected-error {{expected '$'}}
new $a::123;// expected-error {{expected '$'}}

new clone $a; // expected-error {{}}
new "qwe";    // expected-error {{}}

//-------------------------------------------------------------------
// print
//-------------------------------------------------------------------
//print print "123";

?>
