// RUN: %clang_php %s -verify
<?php

//--------------------------------------------------------------------
// closure stmt parsing
//--------------------------------------------------------------------
function() {};
function(bool $a, $b, string $c) {};
function(): bool { return true; };
function() use($a) {};
function() use($a, $b, &$c): float { return 0; };

static function() {};
static function(bool $a, $b, string $c) {};
static function(): bool { return true; };
static function() use($a) {};
static function() use($a, $b, &$c): float { return 0; };


//--------------------------------------------------------------------
// closure in expression
//--------------------------------------------------------------------
$a = function() {};
$a = function(bool $a, $b, string $c) {};
$a = function(): bool { return true; };
$a = function() use($a) {};
$a = function() use($a, $b, &$c): float { return 0; };

$a = static function() {};
$a = static function(bool $a, $b, string $c) {};
$a = static function(): bool { return true; };
$a = static function() use($a) {};
$a = static function() use($a, $b, &$c): float { return 0; };


//--------------------------------------------------------------------
// closure non top stmt
//--------------------------------------------------------------------
function fn() {
  function() {};
  function(bool $a, $b, string $c) {};
  function(): bool { return true; };
  function() use($a) {};
  function() use($a, $b, &$c): float { return 0; };

  static function() {};
  static function(bool $a, $b, string $c) {};
  static function(): bool { return true; };
  static function() use($a) {};
  static function() use($a, $b, &$c): float { return 0; };

  $a = function() {};
  $a = function(bool $a, $b, string $c) {};
  $a = function(): bool { return true; };
  $a = function() use($a) {};
  $a = function() use($a, $b, &$c): float { return 0; };

  $a = static function() {};
  $a = static function(bool $a, $b, string $c) {};
  $a = static function(): bool { return true; };
  $a = static function() use($a) {};
  $a = static function() use($a, $b, &$c): float { return 0; };
}

//--------------------------------------------------------------------
// closure errors
//--------------------------------------------------------------------

function() use () {}; // expected-error{{expected parameter name}}
function() use $a {}; // expected-error{{expected '('}}
function() use ($a,) {}; // expected-error{{expected parameter name}}

?>