// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '<<'
//-------------------------------------------------------------------

static_assert((true  << true   ) === 2 );      // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}

//----- boolean with types ------------------------------------------
static_assert((false << null   ) === 0 );      // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  << null   ) === 1 );      // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false << false  ) === 0 );      // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((false << true   ) === 0 );      // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  << false  ) === 1 );      // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  << true   ) === 2 );      // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  << 123    ) === 0 );      // expected-warning{{integer overflow occurs when expression is evaluated}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false << 123    ) === 0 );      // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  << 123.456) === 0 );      // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
                                               // expected-warning@-2{{integer overflow occurs when expression is evaluated}}
static_assert((false << 123.456) === 0 );      // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  << ''     ) === 1 );      // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false << ''     ) === 0 );      // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  << "0"    ) === 1 );      // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false << "0"    ) === 0 );      // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  << "qwe"  ) === 1 );      // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false << "qwe"  ) === 0 );      // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false << []     ) === 0 );      // expected-error{{operation cannot be applied to arrays}} 
static_assert(([]    << false  ) === 0 );      // expected-error{{operation cannot be applied to arrays}} 

static_assert((null    << false) === 0   );    // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((null    << true ) === 0   );    // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123     << true ) === 246 );    // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123     << false) === 123 );    // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123.456 << true ) === 246 );    // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123.456 << false) === 123 );    // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((''      << true ) === 0   );    // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((''      << false) === 0   );    // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("0"     << true ) === 0   );    // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("0"     << false) === 0   );    // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("qwe"   << true ) === 0   );    // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("qwe"   << false) === 0   );    // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}


//----- null with types ---------------------------------------------
static_assert((null << null   ) === 0 );       // expected-warning 2 {{'null' implicitly converted to 'zero'}}
static_assert((null << ''     ) === 0 );       // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{string does not contain a number}}
static_assert((null << "0"    ) === 0 );       // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((null << "qwe"  ) === 0 );       // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{string does not contain a number}}
static_assert((null << "123"  ) === 0 );       // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((null << 123    ) === 0 );       // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null << -123   ) === 0 );       // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-error@-1{{negative bit shift}} 
static_assert((null << 123.123) === 0 );       // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}

static_assert((''      << null) === 0   );     // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{'null' implicitly converted to 'zero'}}
static_assert(("0"     << null) === 0   );     // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{'null' implicitly converted to 'zero'}}
static_assert(("qwe"   << null) === 0   );     // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{'null' implicitly converted to 'zero'}} 
static_assert(("123"   << null) === 123 );     // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{'null' implicitly converted to 'zero'}}
static_assert((123     << null) === 123 );     // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((-123    << null) === -123);     // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((123.123 << null) === 123 );     // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{'null' implicitly converted to 'zero'}}


//----- integer with types ------------------------------------------
static_assert((123 << 2    ) === 492   );
static_assert((123 << -2   ) === 0 );          // expected-error{{negative bit shift}} 
static_assert((123 << 123.4) === 0 );          // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{integer overflow occurs when expression is evaluated}}
static_assert((123 << 456  ) === 0 );          // expected-warning{{integer overflow occurs when expression is evaluated}}
static_assert((123 << ''   ) === 123);         // expected-warning{{string does not contain a number}}
static_assert((123 << "qwe") === 123);         // expected-warning{{string does not contain a number}}
static_assert((123 << "456") === 0 );          // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{integer overflow occurs when expression is evaluated}}

static_assert((123.4 << 123) === 0 );          // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{integer overflow occurs when expression is evaluated}}
static_assert((456   << 123) === 0 );          // expected-warning{{integer overflow occurs when expression is evaluated}}
static_assert((''    << 123) === 0 );          // expected-warning{{string does not contain a number}}
static_assert(("qwe" << 123) === 0 );          // expected-warning{{string does not contain a number}}
static_assert(("456" << 123) === 0 );          // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{integer overflow occurs when expression is evaluated}}


//----- double with types -------------------------------------------
static_assert((1.2 << 1.2   ) === 2 );         // expected-warning 2 {{implicit conversion from 'double' to 'integer'}}
static_assert((-.2 << ''    ) === 0 );         // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{string does not contain a number}}
static_assert((1.2 << "qwe" ) === 1 );         // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}
static_assert((1/8.2 << "1e8") === 0 );        // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'string' to 'double'}}


static_assert((''    << -1.2) === 0 );         // expected-warning{{string does not contain a number}}
                                               // expected-error@-1{{negative bit shift}}
                                               // expected-warning@-2{{implicit conversion from 'double' to 'integer'}}
static_assert(("qwe"  << 1.2) === 0 );         // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}
static_assert(("1e8"  << 1.2) === 200000000 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
                                               // expected-warning@-2{{implicit conversion from 'double' to 'integer'}}


//----- string with types -------------------------------------------
static_assert(("123"     << "0456"   ) === 0  ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-2{{integer overflow occurs when expression is evaluated}}
static_assert(("qwe"     << "0456"   ) === 0  ); // expected-warning{{string does not contain a number}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert(("0456"    << "qwe"    ) === 456); // expected-warning{{string does not contain a number}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"     << "qwe"    ) === 0  ); // expected-warning{{string does not contain a number}}
                                                 // expected-warning@-1{{string does not contain a number}}
static_assert(("-123"    << ".123"   ) === -123   ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                     // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
                                                     // expected-warning@-2{{implicit conversion from 'double' to 'integer'}}
static_assert((".123"    << "-123"   ) === 0  ); // expected-error{{negative bit shift}} 
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-2{{implicit conversion from 'string' to 'double'}}
                                                 // expected-warning@-3{{implicit conversion from 'double' to 'integer'}}

static_assert(([]      << []     )  === 0);       // expected-error{{operation cannot be applied to arrays}} 
static_assert(([1,2,3] << []     )  === 1);       // expected-error{{operation cannot be applied to arrays}} 
static_assert(([3,4]   << [1,2,3])  === 2);       // expected-error{{operation cannot be applied to arrays}} 
static_assert((["a" => 1] << ["b" => 1])  === 2); // expected-error{{operation cannot be applied to arrays}} 
