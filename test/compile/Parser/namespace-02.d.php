<?php
// RUN: %clang_php %s -verify

namespace ABCD; // expected-note{{previous namespace defined here}}

namespace DEF {} // expected-error{{cannot mix bracketed namespaces with simple}}

namespace ;  // expected-error{{expected namespace name}}

namespace A/B;  // expected-error{{expected '{' or ';'}}

namespace A\B;
namespace A\B();  // expected-error{{expected '{' or ';'}}
