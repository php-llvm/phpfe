// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '%'
//-------------------------------------------------------------------

//----- division by zero --------------------------------------------
static_assert((1  % null   ) === 0       ); // expected-error{{division by zero}} 
                                            // expected-warning@-1{{'null' implicitly converted to 'zero'}}
static_assert((2  % false  ) === 0       ); // expected-error{{division by zero}} 
                                            // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((3  % 0      ) === 0       ); // expected-error{{division by zero}} 
static_assert((4  % 0.0    ) === 0       ); // expected-error{{division by zero}}
                                            // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}
static_assert((4  % 0.9    ) === 0       ); // expected-error{{division by zero}}
                                            // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}
static_assert((5  % ''     ) === 0       ); // expected-error{{division by zero}}
                                            // expected-warning@-1{{string does not contain a number}}
static_assert((6  % '0'    ) === 0       ); // expected-error{{division by zero}}
                                            // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}

//----- boolean with types ------------------------------------------
static_assert( false % []       === 0 ); // expected-error{{operation cannot be applied to arrays}}
static_assert( []    % false    === 0 ); // expected-error{{operation cannot be applied to arrays}}
static_assert( [1,2,3] % []     === 0 ); // expected-error{{operation cannot be applied to arrays}}
