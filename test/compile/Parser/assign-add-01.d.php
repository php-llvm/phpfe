// RUN: %clang_php %s -verify
<?php

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $x = true;    return $x; }
function &get_int_ref    () : int    { $x = 123;     return $x; }
function &get_double_ref () : float  { $x = 12.34;   return $x; }
function &get_string_ref () : string { $x = 'qwe';   return $x; }
function &get_array_ref  () : array  { $x = [1,2,3]; return $x; }


function check_assign_to_bool_01(bool $a) {
  assert($a === false);

  $a += false; assert($a === false);  // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                      // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a += true;  assert($a === true);   // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                      // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;    assert($a === true); // expected-warning{{'null' implicitly converted to 'zero'}}
                                      // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += 123;     assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += 12.34;   assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a += "qwe";   assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
                                      // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === true); // expected-warning{{'null' implicitly converted to 'zero'}}
                                           // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += get_bool();   assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                           // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a += get_int();    assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += get_double(); assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a += get_string(); assert($a === true); // expected-warning{{implicit conversion from 'string' to 'number'}}
                                           // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a += get_int_ref();    assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += get_double_ref(); assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a += get_string_ref(); assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
                                               // expected-warning@-1{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a += $b; assert($a === false);
  $b = true;    $a += $b; assert($a === true);
  $b = 123;     $a += $b; assert($a === true);
  $b = 12.45;   $a += $b; assert($a === true);
  $b = 'qwe';   $a += $b; assert($a === true);
  $b = [1,2,3]; $a += $b; assert($a === false);
}
check_assign_to_bool_01(false);


function check_assign_to_bool_02(bool &$a) {
  assert($a === false);

  $a += false; assert($a === false);  // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                      // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a += true;  assert($a === true);   // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                      // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;    assert($a === true); // expected-warning{{'null' implicitly converted to 'zero'}}
                                      // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += 123;     assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += 12.34;   assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a += "qwe";   assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
                                      // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === true); // expected-warning{{'null' implicitly converted to 'zero'}}
                                           // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += get_bool();   assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                           // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a += get_int();    assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += get_double(); assert($a === true); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a += get_string(); assert($a === true); // expected-warning{{implicit conversion from 'string' to 'number'}}
                                           // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === true);  // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a += get_int_ref();    assert($a === true);  // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a += get_double_ref(); assert($a === true);  // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a += get_string_ref(); assert($a === true);  // expected-warning{{implicit conversion from 'string' to 'number'}}
                                                // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a += $b; assert($a === false);
  $b = true;    $a += $b; assert($a === true);
  $b = 123;     $a += $b; assert($a === true);
  $b = 12.45;   $a += $b; assert($a === true);
  $b = 'qwe';   $a += $b; assert($a === true);
  $b = [1,2,3]; $a += $b; assert($a === false); 
}
$a = false;
check_assign_to_bool_02($a);








function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a += 456;  assert($a === 579);
  $a += -12;  assert($a === 567);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;  assert($a === 567);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a += false; assert($a === 567);
  $a += 123;   assert($a === 690);
  $a += 12.34; assert($a === 702);   // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a += "qwe"; assert($a === 702);   // expected-warning{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === 702);  // expected-warning{{'null' implicitly converted to 'zero'}}
  $a += get_bool();   assert($a === 703);
  $a += get_int();    assert($a === 826);
  $a += get_double(); assert($a === 838);  // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a += get_string(); assert($a === 0);    // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === 839);
  $a += get_int_ref();    assert($a === 962);
  $a += get_double_ref(); assert($a === 974); // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a += get_string_ref(); assert($a === 0);   // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a += $b; assert($a === 0);
  $b = true;  $a += $b; assert($a === 975);
  $b = 123;   $a += $b; assert($a === 1098);
  $b = 12.45; $a += $b; assert($a === 1110);
  $b = 'qwe';   $a += $b; assert($a === 0);
  $b = [1,2,3]; $a += $b; assert($a === false);
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a += 456;  assert($a === 579);
  $a += -12;  assert($a === 567);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;  assert($a === 567);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a += false; assert($a === 567);
  $a += 123;   assert($a === 690);
  $a += 12.34; assert($a === 702);   // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a += "qwe"; assert($a === 702);   // expected-warning{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === 702);  // expected-warning{{'null' implicitly converted to 'zero'}}
  $a += get_bool();   assert($a === 703);
  $a += get_int();    assert($a === 826);
  $a += get_double(); assert($a === 838);  // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a += get_string(); assert($a === 0);    // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === 839);
  $a += get_int_ref();    assert($a === 962);
  $a += get_double_ref(); assert($a === 974); // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a += get_string_ref(); assert($a === 0);   // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a += $b; assert($a === 0);
  $b = true;  $a += $b; assert($a === 975);
  $b = 123;   $a += $b; assert($a === 1098);
  $b = 12.45; $a += $b; assert($a === 1110);
  $b = 'qwe';   $a += $b; assert($a === 0);
  $b = [1,2,3]; $a += $b; assert($a === false);
}
$a = 123;
check_assign_to_int_02($a);








function eq(double $a, double $b) : bool { return abs($a - $b) < 1e-10; }

function check_assign_to_double_01(float $a) {
  assert($a === 123.456);

  $a += 456.5;   assert($a === 579.956);
  $a += -12.258; assert($a === 567.698);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;  assert($a === 567.698);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a += false; assert($a === 567.698);   // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a += 123;   assert($a === 690.698);
  $a += 12.34; assert($a === 703.038);
  $a += "qwe"; assert($a === 703.038);   // expected-warning{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === 703.038); // expected-warning{{'null' implicitly converted to 'zero'}}
  $a += get_bool();   assert($a === 704.038); // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a += get_int();    assert($a === 827.038);
  $a += get_double(); assert($a === 839.378);
  $a += get_string(); assert($a === 0.0);     // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === 840.378);   // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a += get_int_ref();    assert($a === 963.378);   // expected-warning{{implicit conversion from 'integer' to 'double'}}
  $a += get_double_ref(); assert(eq($a, 975.718));
  $a += get_string_ref(); assert($a === 0.0);       // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a += $b; assert($a === 0);
  $b = true;    $a += $b; assert(eq($a, 976.718));
  $b = 123;     $a += $b; assert(eq($a, 1099.718));
  $b = 12.45;   $a += $b; assert(eq($a, 1112.168));
  $b = 'qwe';   $a += $b; assert($a === 0);
  $b = [1,2,3]; $a += $b; assert($a === false);
}

check_assign_to_double_01(123.456);

function check_assign_to_double_02(float &$a) {
  assert($a === 123.456);

  $a += 456.5;   assert($a === 579.956);
  $a += -12.258; assert($a === 567.698);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;  assert($a === 567.698);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a += false; assert($a === 567.698);   // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a += 123;   assert($a === 690.698);
  $a += 12.34; assert($a === 703.038);
  $a += "qwe"; assert($a === 703.038);   // expected-warning{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   assert($a === 703.038); // expected-warning{{'null' implicitly converted to 'zero'}}
  $a += get_bool();   assert($a === 704.038); // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a += get_int();    assert($a === 827.038);
  $a += get_double(); assert($a === 839.378);
  $a += get_string(); assert($a === 0.0);     // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   assert($a === 840.378);   // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a += get_int_ref();    assert($a === 963.378);   // expected-warning{{implicit conversion from 'integer' to 'double'}}
  $a += get_double_ref(); assert(eq($a, 975.718));
  $a += get_string_ref(); assert($a === 0.0);       // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a += $b; assert($a === 0);
  $b = true;    $a += $b; assert(eq($a, 976.718));
  $b = 123;     $a += $b; assert(eq($a, 1099.718));
  $b = 12.45;   $a += $b; assert(eq($a, 1112.168));
  $b = 'qwe';   $a += $b; assert($a === 0);
  $b = [1,2,3]; $a += $b; assert($a === false);
}
$a = 123.456;
check_assign_to_double_02($a);








//TODO: add warninigs for <string> += anything?
function check_assign_to_string_01(string $a) {
  assert($a === 'qwe');

  $a += 'zxc';   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}
  $a += '';      // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}


//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;    // expected-warning{{'null' implicitly converted to 'zero'}}
                 // expected-warning@-1{{operation requires conversion from 'string' to 'number' and back}}
  $a += false;   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += 123;     // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += 12.34;   // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a += "qwe";   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   // expected-warning{{'null' implicitly converted to 'zero'}}
                      // expected-warning@-1{{operation requires conversion from 'string' to 'number' and back}}
  $a += get_bool();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += get_int();    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += get_double(); // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a += get_string(); // expected-warning{{implicit conversion from 'string' to 'number'}}
                      // expected-warning@-1{{operation requires conversion from 'string' to 'number' and back}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += get_int_ref();    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += get_double_ref(); // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a += get_string_ref(); // expected-warning{{implicit conversion from 'string' to 'number'}}
                          // expected-warning@-1{{operation requires conversion from 'string' to 'number' and back}}

//-------------------------------------------------------------------
// assign box to string
//-------------------------------------------------------------------
  $b = null;    $a += $b;
  $b = true;    $a += $b;
  $b = 123;     $a += $b;
  $b = 12.45;   $a += $b;
  $b = 'qwe';   $a += $b;
  $b = [1,2,3]; $a += $b;
}
check_assign_to_string_01('qwe');

function check_assign_to_string_02(string &$a) {
  assert($a === 'qwe');

  $a += 'zxc';   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}
  $a += '';      // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{'null' implicitly converted to 'zero'}}
  $a += false;   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += 123;     // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += 12.34;   // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a += "qwe";   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                      // expected-warning@-1{{'null' implicitly converted to 'zero'}}
  $a += get_bool();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += get_int();    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += get_double(); // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a += get_string(); // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                      // expected-warning@-1{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += get_int_ref();    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a += get_double_ref(); // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a += get_string_ref(); // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                          // expected-warning@-1{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to string
//-------------------------------------------------------------------
  $b = null;    $a += $b;
  $b = true;    $a += $b;
  $b = 123;     $a += $b;
  $b = 12.45;   $a += $b;
  $b = 'qwe';   $a += $b;
  $b = [1,2,3]; $a += $b;
}
$a = 'qwe';
check_assign_to_string_02($a);






function check_assign_to_array_01(array $a) {
  assert($a === [1,2,3]);

  $a += [4,5,6]; assert($a === [4,5,6]);

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref();

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a += $b;
  $b = true;    $a += $b;
  $b = 123;     $a += $b;
  $b = 12.45;   $a += $b;
  $b = 'qwe';   $a += $b;
  $b = [1,2,3]; $a += $b;
}
check_assign_to_array_01([1,2,3]);

function check_assign_to_array_02(array &$a) {
  assert($a === [1,2,3]);

  $a += [4,5,6]; assert($a === [4,5,6]);

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref();

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a += $b;
  $b = true;    $a += $b;
  $b = 123;     $a += $b;
  $b = 12.45;   $a += $b;
  $b = 'qwe';   $a += $b;
  $b = [1,2,3]; $a += $b;
}
$a = [1,2,3];
check_assign_to_array_02($a);

?>