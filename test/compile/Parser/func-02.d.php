// RUN: %clang_php %s -verify
<?php

function $a;    // expected-error{{expected function name}}
function fe00;  // expected-error{{expected '('}}
function fe01();// expected-error{{expected '{'}}
function fe02(123) {} // expected-error{{expected parameter declaration}}
function fe03(...) {} // expected-error{{expected parameter name}}
function fe04(&) {}   // expected-error{{expected parameter name}}
function fe05(int&) {}// expected-error{{expected parameter name}}
function fe06(...$a, ...$b, $c) {} // expected-error{{variadic parameter should be the last one}} \
                                   // expected-note{{previous declaration is here}} \
                                   // expected-error{{variadic parameter should be the last one}} \
                                   // expected-note{{previous declaration is here}}
function fe07(...$a, $b) {}        // expected-error{{variadic parameter should be the last one}} expected-note{{previous declaration is here}} 
function fe08(int ...$a = 123) {}  // expected-error{{variadic parameter cannot have a default value}}
function fe09(): {}      // expected-error{{expected type name}}
function fe10(): int& {} // expected-error{{return type reference should be specified after 'function' key word}}\
                         // expected-error{{function with return type hint should end with return statement}}
function fe11(): & {}    // expected-error{{return type reference should be specified after 'function' key word}}
function fe12(): $a {};  // expected-error{{expected type name}}
//function fe13(qwe qwe) {}// _expected-error{{expected '$'}}

if (1) function cond() {}
if (2) function cond() {}

if (3) {
  function cond() {}  // expected-note{{previous definition is here}}
  if (4) {
    function cond() {}// expected-error{{function is already defined}}
  }
}


function  fv_0() : void     { echo "!fv_0!"; } 
function  fv_1() : void     { return 123; }    //expected-error{{function declared with void type hint cannot return value}}
function &fv_2() : void     { return $a; }     //expected-error{{reference to void is not allowed}}
function  fv_3(void $a)     { return $a; }     //expected-error{{function parameter cannot have void type}}
function  fv_4(void &$a)    { return $a; }     //expected-error{{reference to void is not allowed}}
function  fv_5(void ...$a)  {}                 //expected-error{{function parameter cannot have void type}}

echo fv_0();
echo fv_1();
echo fv_2();
echo fv_3(123);
echo fv_4($a);
echo fv_5(1,2,3);

function f(int $a) {}
f(fv_0()); 				//expected-error{{invalid argument type, expected integer, have NULL}}


?>