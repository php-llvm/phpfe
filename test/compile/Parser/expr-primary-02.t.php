<?php 
// RUN: %clang_php -ast-dump -dump-stage=parse %s | FileCheck %s

//-------------------------------------------------------------------
// PHP primary expression parser tests
//-------------------------------------------------------------------


//-------------------------------------------------------------------
// echo
//-------------------------------------------------------------------

echo "qwe";
// CHECK:      PhpEchoStmt
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

echo 1,2,3;
// CHECK:      PhpEchoStmt
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 1
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 2
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 3

//-------------------------------------------------------------------
// array
//-------------------------------------------------------------------
[];
// CHECK-NEXT: PhpArrayExpr

[1, 2, 3];
// CHECK-NEXT: PhpArrayExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 2
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 3

[1 => 3];
// CHECK-NEXT: PhpArrayExpr
// CHECK-NEXT: PhpMapExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 3

[1 => 3, "qwe", ];
// CHECK-NEXT: PhpArrayExpr
// CHECK-NEXT: PhpMapExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 3
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

array();
// CHECK-NEXT: PhpArrayExpr

array(1, 2, 3);
// CHECK-NEXT: PhpArrayExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 2
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 3

array(1 => 3);
// CHECK-NEXT: PhpArrayExpr
// CHECK-NEXT: PhpMapExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 3

array(1 => 3, "qwe", );
// CHECK-NEXT: PhpArrayExpr
// CHECK-NEXT: PhpMapExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 3
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

//-------------------------------------------------------------------
// clone
//-------------------------------------------------------------------
clone 123;
// CHECK-NEXT: PhpCloneExpr
// CHECK-SAME: 'long'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 123

clone 123 + 456;
// CHECK-NEXT: BinaryOperator
// CHECK-NEXT: PhpCloneExpr
// CHECK-SAME: 'long'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 456

//-------------------------------------------------------------------
// null
//-------------------------------------------------------------------
null;
// CHECK-NEXT: PhpNullLiteral
// CHECK-SAME: 'void'

NULL;
// CHECK-NEXT: PhpNullLiteral
// CHECK-SAME: 'void'


//-------------------------------------------------------------------
// include
//-------------------------------------------------------------------
include 123;
// CHECK-NEXT: PhpIncludeExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123

include_once "qwe";
// CHECK-NEXT: PhpIncludeExpr
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

require "qwe";
// CHECK-NEXT: PhpIncludeExpr
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

require_once "qwe";
// CHECK-NEXT: PhpIncludeExpr
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

include 1 + 2;
// CHECK-NEXT: PhpIncludeExpr
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '+'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 1
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 2

//-------------------------------------------------------------------
// Unary operators
//-------------------------------------------------------------------
-123;
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: -123

+123;
// CHECK-NEXT: UnaryOperator
// CHECK-SAME: prefix '+'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123

!123;
// CHECK-NEXT: UnaryOperator
// CHECK-SAME: prefix '!'
// CHECK-NEXT: CXXBoolLiteralExpr {{.*}} '_Bool' true

~123;
// CHECK-NEXT: UnaryOperator
// CHECK-SAME: prefix '~'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123

//--$a;
//++$a;
//$a--;
//$a++;
//&$a;

@"qwe";
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

@123 + 456;
// CHECK-NEXT: BinaryOperator
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

1 + @@@@2;
// CHECK-NEXT: BinaryOperator
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 2

@"qwe"[1];
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: ArraySubscriptExpr
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1

?>