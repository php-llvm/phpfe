// RUN: %clang_php %s -verify
<?php
static_assert(intval(42) === 42);                      
static_assert(intval(4.2) === 4);                    
static_assert(intval('42') === 42);                  
static_assert(intval('+42') === 42);                
static_assert(intval('-42') === -42);              
static_assert(intval(042) === 34);                 
static_assert(intval('042') === 42);               
//TODO: handle 64-bit case
//static_assert(intval(1e10) === 2147483647);           
static_assert(intval('1e10') === 1);                 
static_assert(intval(0x1A) === 26);                  
static_assert(intval(42000000) === 42000000);        
//TODO: handle 64-bit case
//static_assert(intval(420000000000000000000) === 2147483647);         
//static_assert(intval('420000000000000000000') === 2147483647); 
static_assert(intval(42, 8) === 42);    // expected-warning {{base parameter of intval() function has no effect for non-string values}} 
static_assert(intval(34.4434,8) === 34); // expected-warning {{base parameter of intval() function has no effect for non-string values}} 
static_assert(intval(array(1,2,3),8) === 1); // expected-warning {{base parameter of intval() function has no effect for non-string values}}  // expected-warning {{implicit conversion of array to number}}
static_assert(intval(true,8) === 1); // expected-warning {{base parameter of intval() function has no effect for non-string values}} 
static_assert(intval('42', 8) === 34);                  
static_assert(intval(array()) === 0);       // expected-warning {{implicit conversion of array to number}}          
static_assert(intval(array('foo', 'bar')) === 1);  // expected-warning {{implicit conversion of array to number}}  

static_assert(boolval(0) === false);
static_assert(boolval(1) === true);
static_assert(boolval(1.0) === true);
static_assert(boolval("1") === true);
static_assert(boolval("1.556") === true);
static_assert(boolval("0") === false);
static_assert(boolval("abcde") === true);
static_assert(boolval(array()) === false); 
static_assert(boolval(array(1,2,3,4)) === true); 
static_assert(boolval("") === false);
static_assert(floatval(1e100000000) != 0); //expected-warning {{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}} 

static_assert(floatval(0) === 0.0);
static_assert(floatval(1) === 1.0);
static_assert(floatval(1.0) === 1.0);
static_assert(floatval("1") === 1.0);
static_assert(floatval("1.556") === 1.556);
static_assert(floatval("0") === 0.0);
static_assert(floatval("abcde") === 0.0); // expected-warning {{string does not contain a number}}
static_assert(floatval(array()) === 0.0); // expected-warning {{implicit conversion of array to number}}
static_assert(floatval(array(1,2,3,4)) === 1.0); // expected-warning {{implicit conversion of array to number}}

static_assert(strval(0) === "0");
static_assert(strval(1) === "1");
static_assert(strval(1.0) === "1");
static_assert(strval("1") === "1");
static_assert(strval("1.556") === "1.556");
static_assert(strval("0") === "0");
static_assert(strval("abcde") === "abcde");
static_assert(strval(array()) === "Array");
static_assert(strval(array(1,2,3,4)) === "Array");
static_assert(strval("") === "");
?>