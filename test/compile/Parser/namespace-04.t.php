<?php
// RUN: %clang_php -ast-dump -dump-stage=parse %s | FileCheck %s

// CHECK:      TranslationUnitDecl
// CHECK:      |-NamespaceDecl {{.+}} prev {{.+}} php

namespace ABCD {

function funca() {
}

}
// CHECK:      | |-NamespaceDecl {{.+}} ABCD
// CHECK-NEXT: |   `-NamespaceDecl {{.+}} __Func
// CHECK-NEXT: |     `-FunctionDecl {{.+}} funca 'class phprt::Box (void)' static

namespace {

function funcb() {
}

}
// CHECK:      | |-NamespaceDecl {{.+}} __Func
// CHECK:      |   `-FunctionDecl {{.+}} funcb 'class phprt::Box (void)' static

namespace AAA\BBB {

function funcc() {
}

}
// CHECK:      | |-NamespaceDecl {{.+}} AAA
// CHECK:      | | `-NamespaceDecl {{.*}} BBB
// CHECK:      | |   `-NamespaceDecl {{.*}} __Func
// CHECK:      | |     `-FunctionDecl {{.*}} funcc 'class phprt::Box (void)' static

namespace {
function funcz() {}
}
// CHECK:      | `-NamespaceDecl {{.*}} prev {{.*}} __Func
// CHECK:      |   `-FunctionDecl {{.*}} funcz 'class phprt::Box (void)' static

// CHECK:      |-FunctionDecl {{.+}} 'class phprt::Box (struct phprt::UnitState *)':'class phprt::Box (struct phprt::UnitState *)'
