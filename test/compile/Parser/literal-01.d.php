<?php
// RUN: %clang_php -triple x86_64-unknown-linux-gnu -verify %s

// The test uses constants values for 64-bit platform.
//TODO:static_assert(PHP_INT_SIZE == 8);


$a = 123;
$a = 123456789012345678901234567890;     // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
$a = 9223372036854775807;
$a = 9223372036854775808;                // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
$a = -123;
$a = -123456789012345678901234567890;    // expected-warning{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}
$a = -9223372036854775808;
$a = -9223372036854775809;               // expected-warning{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}


static_assert(is_integer(9223372036854775807));
static_assert(is_float(9223372036854775808));                // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
static_assert(is_integer(-9223372036854775807));
static_assert(is_integer(-9223372036854775808));
static_assert(is_float(-9223372036854775809));               // expected-warning{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}

// NOTE: we use non-strict comparison because of precision loss when
// integer is converted to float.

static_assert(9223372036854775806 < 9223372036854775807);
static_assert(9223372036854775807 <= 9223372036854775808);   // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
static_assert(9223372036854775808 <= 9223372036854775809);   // expected-warning 2 {{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
static_assert(0 < 9223372036854775807);
static_assert(0 < 9223372036854775808);                      // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
static_assert(0 < 9223372036854775809);                      // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}

static_assert(-9223372036854775807 >= -9223372036854775808);
static_assert(-9223372036854775808 >= -9223372036854775809); // expected-warning{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}
static_assert(-9223372036854775807 >= -9223372036854775809); // expected-warning{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}
static_assert(0 > -9223372036854775809);                     // expected-warning{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}

static_assert(-9223372036854775807 < 9223372036854775807);
static_assert(-9223372036854775807 < 9223372036854775808);   // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
static_assert(-9223372036854775808 < 9223372036854775807);
static_assert(-9223372036854775808 < 9223372036854775808);   // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
static_assert(-9223372036854775808 < 9223372036854775809);   // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
static_assert(-9223372036854775809 < 9223372036854775808);   // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
                                                             // expected-warning@-1{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}
static_assert(-9223372036854775809 < 9223372036854775809);   // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
                                                             // expected-warning@-1{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}


static_assert(is_float(123456789012345678901234567890));     // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
static_assert(is_float(-123456789012345678901234567890));    // expected-warning{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}


$a = 12.3;
$a = 12.3e1000;     // expected-warning{{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}}
$a = 12.3e-1000;    // expected-warning{{magnitude of floating-point constant too small for type 'double'; minimum is 4.9406564584124654E-324}}
$a = -12.3;
$a = -12.3e1000;    // expected-warning{{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}}
$a = -12.3e-1000;   // expected-warning{{magnitude of floating-point constant too small for type 'double'; minimum is 4.9406564584124654E-324}}
