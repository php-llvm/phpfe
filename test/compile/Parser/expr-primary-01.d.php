// RUN: %clang_php %s -verify
<?php

//-----------------------------------------------------------------------------
// Primary expressions.
//----------------------------------------------------------------------------- 

// String literals

//echo 'abcd';
echo "abcd";

echo "abcd" [2];
echo "abcd" {1};
echo "abcd" []; // expected-error{{empty subscript is not allowed here}}
echo "abcd" [1] [2];  // expected-error{{subscript cannot be applied to value of type 'const char'}}

//echo 'abcd' [2];
//echo 'abcd' {1};
//echo 'abcd' [];
//echo 'abcd' [1] [2];

//-------------------------------------------------------------------
// Array
//-------------------------------------------------------------------
[1, &2, 3 => &4]; // expected-error{{expected l-value variable}}, expected-error{{expected l-value variable}}

array(;	
// expected-error@-1{{expected expression}}
// expected-error@-2{{expected ')'}}
// expected-note@-3{{to match this '('}}
	
array ); // expected-error{{expected '('}}
array;	 // expected-error{{expected '('}}

//-------------------------------------------------------------------
// Variables.
//-------------------------------------------------------------------

$b = $a;
$a = "a";
$b = $$a;
$d = $$$$$$$$$$$$$$$$a;

$b = ${ $a };
${;
// expected-error@-1 {{expected expression}}
// expected-error@-2 {{expected '}'}}
// expected-note@-3 {{to match this '{'}}

$123; //expected-error{{expected identifier}}


