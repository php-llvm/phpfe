<?php
// RUN: %clang_php %s

namespace {

const AA_1 = 111;

static_assert(AA_1 === 111);
}

namespace ABCD {

const AA_1 = 'cdef';

static_assert(AA_1 === 'cdef');
}


namespace {

static_assert(AA_1 === 111);
static_assert(ABCD\AA_1 === 'cdef');
}
