// RUN: %clang_php %s -verify
<?php

function fr01(): bool    { return "qwe"; }
function fr02(): boolean { return 123;   } 

function fr03(): int     { return true;  } 
function fr04(): integer { return 1.2;   } //expected-warning{{implicit conversion from 'double' to 'integer'}}

function fr05(): real    { return 123;   } 
function fr06(): float   { return null;  } //expected-error{{invalid return type, expected double, have NULL}}
function fr07(): double  { return true;  }

function fr08(): string  { return true;  }
function fr09(): binary  { return 123;   }

function &fr10(): bool    { return true; }

?>