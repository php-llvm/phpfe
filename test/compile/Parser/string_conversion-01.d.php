// RUN: %clang_php %s -verify
<?php

//TODO: many of messages from 'string' to 'number' must to 'integer' or to 'double'
function static_assert($a) { assert($a); } // expected-warning{{static_assert has special treatment that cannot be redefined}}

static_assert((" -123" + 0) === -123);      // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((" -123" + 0) === -123);      // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((" -123" + 0) === -123);      // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((" -123" + 0) === -123);      // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((" -123" + 0) === -123);      // expected-warning{{implicit conversion from 'string' to 'integer'}}

//--------------------------------------------------------------------
// convert integers
//--------------------------------------------------------------------
static_assert(("123" + 0) === 123);                   // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((" +123" + 0) === 123);                 // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((" -123" + 0) === -123);                // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("0123" + 0) === 123);                  // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("\t\r\n\f\v -123" + 0) === -123);      // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("123_extra" + 0) === 123);             // expected-warning{{implicit conversion from 'string' to 'integer'}}

//--------------------------------------------------------------------
// convert doubles
//--------------------------------------------------------------------
static_assert((".123" + 0) === 0.123);                // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("+.123" + 0) === 0.123);               // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("-.123" + 0) === -0.123);              // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("0.123" + 0) === 0.123);               // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("123e" + 0) === 123);                  // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("123e2" + 0) === 12300.0);             // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("123e2.2" + 0) === 12300.0);           // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert((".123e2" + 0) === 12.3);               // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert((".123E2" + 0) === 12.3);               // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert((".123E+2" + 0) === 12.3);              // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("123E-2" + 0) === 1.23);               // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("-123E-2" + 0) === -1.23);             // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert((" \t\r\n\v\f-123E-2" + 0) === -1.23);  // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert((".123.123" + 0) === 0.123);            // expected-warning{{implicit conversion from 'string' to 'double'}}

//--------------------------------------------------------------------
// no conversion
//--------------------------------------------------------------------
static_assert((" + 123" + 0) === 0);          // expected-warning{{string does not contain a number}}
static_assert((" - 123" + 0) === 0);          // expected-warning{{string does not contain a number}}
static_assert(("- 123" + 0) === 0);           // expected-warning{{string does not contain a number}}
static_assert(("0x123" + 0) === 0);           // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("+ .123" + 0) === 0);          // expected-warning{{string does not contain a number}}
static_assert(("- .123" + 0) === 0);          // expected-warning{{string does not contain a number}}

?>