// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '-'
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
static_assert((false - null   ) ===  0       ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  - null   ) ===  1       ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false - false  ) ===  0       ); // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((false - true   ) === -1       ); // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  - false  ) ===  1       ); // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  - true   ) ===  0       ); // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  - 123    ) === -122     ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false - 123    ) === -123     ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  - 123.456) === -122.456 ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false - 123.456) === -123.456 ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  - ''     ) ===  1       ); // expected-warning{{string does not contain a number}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false - ''     ) ===  0       ); // expected-warning{{string does not contain a number}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  - "0"    ) ===  1       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false - "0"    ) ===  0       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  - "qwe"  ) ===  1       ); // expected-warning{{string does not contain a number}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false - "qwe"  ) ===  0       ); // expected-warning{{string does not contain a number}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert( false - []                    ); // expected-error{{operation cannot be applied to arrays}}
static_assert( []    - false                 ); // expected-error{{operation cannot be applied to arrays}}
static_assert( [1,2,3] - []                  ); // expected-error{{operation cannot be applied to arrays}}

static_assert((null    - false) ===  0       ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((null    - true ) === -1       ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123     - true ) ===  122     ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123     - false) ===  123     ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123.456 - true ) ===  122.456 ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123.456 - false) ===  123.456 ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((''      - true ) === -1       ); // expected-warning{{string does not contain a number}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((''      - false) ===  0       ); // expected-warning{{string does not contain a number}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("0"     - true ) === -1       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("0"     - false) ===  0       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("qwe"   - true ) === -1       ); // expected-warning{{string does not contain a number}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("qwe"   - false) ===  0       ); // expected-warning{{string does not contain a number}}
                                                // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}


//----- null with types ---------------------------------------------
static_assert((null - null   ) ===  0      ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null - ''     ) ===  0      ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{string does not contain a number}}
static_assert((null - "0"    ) ===  0      ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((null - "qwe"  ) ===  0      ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{string does not contain a number}}
static_assert((null - "123"  ) === -123    ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((null - 123    ) === -123    ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null - -123   ) ===  123    ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null - 123.123) === -123.123); // expected-warning{{'null' implicitly converted to 'zero'}}

static_assert((''      - null) ===  0      ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{string does not contain a number}}
static_assert(("0"     - null) ===  0      ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"   - null) ===  0      ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{string does not contain a number}}
static_assert(("123"   - null) ===  123    ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((123     - null) ===  123    ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((-123    - null) === -123    ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((123.123 - null) ===  123.123); // expected-warning{{'null' implicitly converted to 'zero'}}


//----- integer with types ------------------------------------------
static_assert(abs((123 - 123.4) + 0.4) < 1e-10  );
static_assert((123 - 456  ) === -333 );
static_assert((456 - 123  ) ===  333 );
static_assert((123 - 123  ) ===  0   );
static_assert((123 - ''   ) ===  123 ); // expected-warning{{string does not contain a number}}
static_assert((123 - "qwe") ===  123 ); // expected-warning{{string does not contain a number}}
static_assert((123 - "456") === -333 ); // expected-warning{{implicit conversion from 'string' to 'integer'}}

static_assert(abs((123.4 - 123) - 0.4) < 1e-10  );
static_assert((456   - 123) ===  333  );
static_assert((123   - 456) === -333  );
static_assert((123   - 123) ===  0    );
static_assert((''    - 123) === -123  ); // expected-warning{{string does not contain a number}}
static_assert(("qwe" - 123) === -123  ); // expected-warning{{string does not contain a number}}
static_assert(("456" - 123) ===  333  ); // expected-warning{{implicit conversion from 'string' to 'integer'}}


//----- double with types -------------------------------------------
static_assert((1.2 - 1.2   ) ===  0.0           );
static_assert(abs((1.3 - 1.2) - 0.1) < 1e-10    );
static_assert(abs((1.2 - 1.3) + 0.1) < 1e-10    );
static_assert((-.2 - ''    ) === -0.2           ); // expected-warning{{string does not contain a number}}
static_assert((1.2 - "qwe" ) ===  1.2           ); // expected-warning{{string does not contain a number}}
static_assert((1.2 - "1e10") === -9999999998.8  ); // expected-warning{{implicit conversion from 'string' to 'double'}}

static_assert((''     - -.2) ===  0.2          ); // expected-warning{{string does not contain a number}}
static_assert(("qwe"  - 1.2) === -1.2          ); // expected-warning{{string does not contain a number}}
static_assert(("1e10" - 1.2) ===  9999999998.8 ); // expected-warning{{implicit conversion from 'string' to 'double'}}


//----- string with types -------------------------------------------
static_assert(("123"     - "0456"   ) === -333      ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"     - "0456"   ) === -456      ); // expected-warning{{string does not contain a number}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("0456"    - "qwe"    ) ===  456      ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{string does not contain a number}}
static_assert(("qwe"     - "qwe"    ) ===  0        ); // expected-warning{{string does not contain a number}} expected-warning{{string does not contain a number}}
static_assert(("-123"    - ".123"   ) === -123.123  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    - ".123"   ) === 122.877  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    - ' .123'  ) === 122.877  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert((' +123'   - ".123"   ) === 122.877  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("+123asd" - ".123qwe") === 122.877  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}

static_assert(("0456"  - "123"  ) ===  333          ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("0456"  - "qwe"  ) ===  456          ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{string does not contain a number}}
static_assert(("qwe"   - "0456" ) === -456          ); // expected-warning{{string does not contain a number}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"   - "qwe"  ) ===  0            ); // expected-warning{{string does not contain a number}} expected-warning{{string does not contain a number}}
static_assert((".123"    - "-123"   ) === 123.123 ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((".123"    - "+123"   ) === -122.877  ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((' .123'   - "+123"   ) === -122.877  ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((".123"    - ' +123'  ) === -122.877  ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((".123qwe" - "+123asd") === -122.877  ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}

// TODO: add overflow tests
