// RUN: %clang_php %s -verify -triple i686-pc-windows-msvc18.0.0
<?php
const AA_1 = 1, AA_2 = 'abcd';
const SPQR = 1, SPQR = 2;  // expected-error{{constant redefinition}}
                           // expected-note@-1{{previous definition is here}}

const SPQR_2 = 12;  // expected-note{{previous definition is here}}
const SPQR_2 = 12;  // expected-error{{constant redefinition}}

const ABCD; // expected-error{{expected '='}}
const $AB;  // expected-error{{expected identifier}}
const 123;  // expected-error{{expected identifier}}

const ABCD1 = 123 + $abcde / 12; // expected-error{{not a constant expression}}
const ABCD2 == 2; // expected-error{{expected '='}}

const null = 1;  // expected-error{{cannot redefine builtin constant}}
const true = 1;  // expected-error{{cannot redefine builtin constant}}
const false = 1; // expected-error{{cannot redefine builtin constant}}

$aaa = NSAA\null;   // expected-error{{cannot redefine builtin constant}}
$aaa = NSAA\true;   // expected-error{{cannot redefine builtin constant}}
$aaa = NSAA\false;  // expected-error{{cannot redefine builtin constant}}

{
  const BBB_1 = 666; // expected-error{{const may appear only at file or namespace scope}}
}

function func_01() {
  const BBB_2 = 'abc'; // expected-error{{const may appear only at file or namespace scope}}
}

if ($a)
  const BBB_3 = 11; // expected-error{{const may appear only at file or namespace scope}}

AA_1 = 12; // expected-error{{operation may be applied only to lvalues}}

const CCC = 1.2e7777;  // expected-warning{{magnitude of floating-point constant too large for type 'double'; maximum is}}
const CCM = 1.2e-7777; // expected-warning{{magnitude of floating-point constant too small for type 'double'; minimum is}}
const CCI = 0x80000000000000000; // expected-warning{{literal cannot be represented as integer (max is 2147483647), converted to float}}
