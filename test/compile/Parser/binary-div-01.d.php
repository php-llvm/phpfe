// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '/'
//-------------------------------------------------------------------

//----- division by zero --------------------------------------------
static_assert((1  / null   ) === 0       ); // expected-error{{division by zero}} 
static_assert((2  / false  ) === 0       ); // expected-error{{division by zero}} 
static_assert((3  / 0      ) === 0       ); // expected-error{{division by zero}} 
static_assert((4  / 0.0    ) === 0       ); // expected-error{{division by zero}} 
static_assert((5  / ''     ) === 0       ); // expected-error{{division by zero}}  expected-warning{{string does not contain a number}}
static_assert((6  / '0'    ) === 0       ); // expected-error{{division by zero}}  expected-warning{{implicit conversion from 'string' to 'integer'}}

//----- boolean with types ------------------------------------------
static_assert((false / true   ) === 0       );
static_assert((true  / true   ) === 1       );
static_assert((true  / 2      ) === 0.5     );
static_assert((false / 123    ) === 0       );
static_assert((true  / 4      ) === 0.25    );

static_assert((false / 123.456) === 0.0     );
static_assert( false / []                   ); // expected-error{{operation cannot be applied to arrays}}
static_assert( []    / false                ); // expected-error{{operation cannot be applied to arrays}}
static_assert( [1,2,3] / []                 ); // expected-error{{operation cannot be applied to arrays}} 

static_assert((null    / true ) === 0       ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((123     / true ) === 123     );
static_assert((123.456 / true ) === 123.456 );
static_assert((''      / true ) === 0       ); // expected-warning{{string does not contain a number}}
static_assert(("0"     / true ) === 0       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"   / true ) === 0       ); // expected-warning{{string does not contain a number}}


//----- null with types ---------------------------------------------
static_assert((null / "123"  ) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((null / 123    ) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null / -123   ) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null / 123.123) === 0.0    ); // expected-warning{{'null' implicitly converted to 'zero'}}


//----- integer with types ------------------------------------------
static_assert(abs((123 / 123.4) - 0.9967585089) < 1e-10 );
static_assert((512 / 128  ) === 4       );
static_assert((128 / 512  ) === 0.25    );
static_assert((123 / 123  ) === 1       );
static_assert(abs((123 / "456") - 0.2697368421) < 1e-10 ); // expected-warning{{implicit conversion from 'string' to 'integer'}}

static_assert(abs((123.4 / 123) - 1.0032520325) < 1e-10 );
static_assert((''    / 123) === 0        ); // expected-warning{{string does not contain a number}}
static_assert(("qwe" / 123) === 0        ); // expected-warning{{string does not contain a number}}
static_assert(("1024" / 32) === 32       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}


//----- double with types -------------------------------------------
static_assert((1.2 / 1.2   ) === 1.0          );
static_assert((2.4 / 1.2   ) === 2.0          );
static_assert((1.2 / 2.4   ) === 0.5          );
static_assert((1.2 / "1e5") === 0.000012      ); // expected-warning{{implicit conversion from 'string' to 'double'}}

static_assert((''     / -.2) === 0.0           ); // expected-warning{{string does not contain a number}}
static_assert(("qwe"  / 1.2) === 0.0           ); // expected-warning{{string does not contain a number}}
static_assert(("1e5"  / 0.2) === 500000.0      ); // expected-warning{{implicit conversion from 'string' to 'double'}}


//----- string with types -------------------------------------------
static_assert(("123"     / "246"    ) ===  0.5     ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"     / "0456"   ) ===  0       ); // expected-warning{{string does not contain a number}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("-123"    / ".123"   ) === -1000.0  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    / ".123"   ) ===  1000.0  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    / ' .123'  ) ===  1000.0  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert((' +123'   / ".123"   ) ===  1000.0  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(("+123asd" / ".123qwe") ===  1000.0  ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'double'}}

static_assert(("246"    / "123"     ) ===  2       ); // expected-warning{{implicit conversion from 'string' to 'integer'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"     / "0456"   ) ===  0       ); // expected-warning{{string does not contain a number}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((".123"    / "-123"   ) === -0.001  ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((".123"    / "+123"   ) ===  0.001  ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((' .123'   / "+123"   ) ===  0.001  ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((".123"    / ' +123'  ) ===  0.001  ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((".123qwe" / "+123asd") ===  0.001  ); // expected-warning{{implicit conversion from 'string' to 'double'}} expected-warning{{implicit conversion from 'string' to 'integer'}}

