// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}

static_assert((null  ? 11  : 22 ) === 22);
static_assert((true  ? 11  : 22 ) === 11);
static_assert((false ? 11  : 22 ) === 22);
static_assert((0     ? 11  : 22 ) === 22);
static_assert((11    ? 11  : 22 ) === 11);
static_assert((0.1   ? 11  : 22 ) === 11);
static_assert((0.0   ? 11  : 22 ) === 22);
static_assert((''    ? 11  : 22 ) === 22);
static_assert(('0'   ? 11  : 22 ) === 22);
static_assert(('x'   ? 11  : 22 ) === 11);
static_assert(('0.0' ? 11  : 22 ) === 11);
static_assert(([]    ? 11  : 22 ) === 22);
static_assert(([1]   ? 11  : 22 ) === 11);


static_assert((null  ? 11  : '22' ) === '22');
static_assert((true  ? 11  : '22' ) === 11  );
static_assert((false ? 11  : '22' ) === '22');


static_assert((null  ? : 22 ) === 22   );
static_assert((true  ? : 22 ) === true );
static_assert((false ? : 22 ) === 22   );
static_assert((0     ? : 22 ) === 22   );
static_assert((11    ? : 22 ) === 11   );
static_assert((0.1   ? : 22 ) === 0.1  );
static_assert((0.0   ? : 22 ) === 22   );
static_assert((''    ? : 22 ) === 22   );
static_assert(('0'   ? : 22 ) === 22   );
static_assert(('x'   ? : 22 ) === 'x'  );
static_assert(('0.0' ? : 22 ) === '0.0');
static_assert(([]    ? : 22 ) === 22   );
static_assert(([1]   ? : 22 ) === [1]  );


// Associativity

static_assert( (0 ? 1 : 2 ? 3 : 4) === 3);
static_assert( (1 ? 0 : 2 ? 3 : 4) === 4);
static_assert( (0 ? 1 : 0 ? 3 : 4) === 4);

static_assert( (0 ?: 1 ?: 2 ?: 3)  === 1 );
static_assert( (1 ?: 0 ?: 3 ?: 2)  === 1 );
static_assert( (2 ?: 1 ?: 0 ?: 3)  === 2 );
static_assert( (3 ?: 2 ?: 1 ?: 0)  === 3 );
static_assert( (0 ?: 1 ?: 2 ?: 3)  === 1 );
static_assert( (0 ?: 0 ?: 2 ?: 3)  === 2 );
static_assert( (0 ?: 0 ?: 0 ?: 3)  === 3 );

/* TODO:
static_assert( 1 ?: 0 ?: 3 ?: 2  === 1 );
static_assert( 2 ?: 1 ?: 0 ?: 3  === 2 );
static_assert( 3 ?: 2 ?: 1 ?: 0  === 3 );
static_assert( 0 ?: 1 ?: 2 ?: 3  === 1 );
static_assert( 0 ?: 0 ?: 2 ?: 3  === 2 );
static_assert( 0 ?: 0 ?: 0 ?: 3  === 3 );
*/