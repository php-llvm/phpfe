// RUN: %clang_php %s -verify
<?php

func();
\func();
\A\func();
\A\B\func();
\\A\func;  // expected-error{{expected identifier}}
A\func();
A\\func(); // expected-error{{expected identifier}}

namespace \func();
namespace \A\func();
$a = namespace func();    // expected-error{{expected namespace delimiter}}

self\func();
parent\func();
//TODO: static\func();

self::dunc();    // expected-error{{scope specifier 'self' cannot be used outside class scope}}
parent::func();  // expected-error{{scope specifier 'parent' cannot be used outside class scope}}
//TODO: static::func();
