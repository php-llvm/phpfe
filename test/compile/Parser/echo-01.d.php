// RUN: %clang_php %s -verify
<?php

echo 1;
echo 1, "qwe", 3;
echo (1);

echo; // expected-error {{expected expression}}
echo 1,;// expected-error {{expected expression}}
echo echo 123; // expected-error {{expected expression}}
echo (1,2,3);  // expected-error {{expected ')'}} \
               // expected-note {{to match this '('}}

?>
