// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } // expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// short circuit of '&&', '||' and '?:'
//-------------------------------------------------------------------

static_assert((false && $a) === false);
static_assert((true  || $a) === true );

static_assert(($a && false) === false);   // expected-error{{static_assert argument cannot be evaluated in compile time}}
static_assert(($a || true ) === true );   // expected-error{{static_assert argument cannot be evaluated in compile time}}
