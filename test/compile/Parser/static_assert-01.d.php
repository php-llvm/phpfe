// RUN: %clang_php %s -verify
<?php

function static_assert($a) { // expected-warning{{static_assert has special treatment that cannot be redefined}}
  echo "static_assert!\n";
}

static_assert(true);
static_assert(123 === 123);

static_assert(false);  // expected-error{{static_assert failed}}
static_assert(123 === 456); // expected-error{{static_assert failed}}

$a = static_assert(true); // expected-error{{static_assert cannot be used in expression}}

static_assert(false, 'false passed to static assert!');// expected-error{{static_assert failed: false passed to static assert!}}

static_assert($a); // expected-error{{static_assert argument cannot be evaluated in compile time}}
static_assert($a == 1); // expected-error{{static_assert argument cannot be evaluated in compile time}}

static_assert(123); // expected-error{{first argument of static_assert must be a boolean expression}}
static_assert(0**0 == 0); // expected-error{{operation result is undefined}}
?>