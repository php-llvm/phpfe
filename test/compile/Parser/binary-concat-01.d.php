// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '.'
//-------------------------------------------------------------------
static_assert(((1/3) . "qwe") === "0.33333333333333qwe");

//----- boolean with types ------------------------------------------
static_assert((false . null   ) === ''        );
static_assert((true  . null   ) === "1"       );
static_assert((false . false  ) === ''        );
static_assert((false . true   ) === "1"       );
static_assert((true  . false  ) === "1"       );
static_assert((true  . true   ) === "11"      );
static_assert((true  . 123    ) === "1123"    );
static_assert((false . 123    ) === "123"     );
static_assert((true  . 123.456) === "1123.456");
static_assert((false . 123.456) === "123.456" );
static_assert((true  . ''     ) === "1"       );
static_assert((false . ''     ) === ''        );
static_assert((true  . "0"    ) === "10"      );
static_assert((false . "0"    ) === "0"       );
static_assert((true  . "qwe"  ) === "1qwe"    );
static_assert((false . "qwe"  ) === "qwe"     );
static_assert((false . []     ) === "Array"   ); // expected-warning{{implicit conversion from 'array' to 'string'}}
static_assert((true  . []     ) === "1Array"  ); // expected-warning{{implicit conversion from 'array' to 'string'}}

static_assert((null    . false) === ''        );
static_assert((null    . true ) === "1"       );
static_assert((123     . true ) === "1231"    );
static_assert((123     . false) === "123"     );
static_assert((123.456 . true ) === "123.4561");
static_assert((123.456 . false) === "123.456" );
static_assert((''      . true ) === "1"       );
static_assert((''      . false) === ''        );
static_assert(("0"     . true ) === "01"      );
static_assert(("0"     . false) === "0"       );
static_assert(("qwe"   . true ) === "qwe1"    );
static_assert(("qwe"   . false) === "qwe"     );
static_assert(([]      . false) === "Array"   ); // expected-warning{{implicit conversion from 'array' to 'string'}}
static_assert(([1,2,3] . true ) === "Array1"  ); // expected-warning{{implicit conversion from 'array' to 'string'}}


//----- null with types ---------------------------------------------
static_assert((null . null   ) === ''       );
static_assert((null . ''     ) === ''       );
static_assert((null . "0"    ) === "0"      );
static_assert((null . "qwe"  ) === "qwe"    );
static_assert((null . "123"  ) === "123"    );
static_assert((null . 123    ) === "123"    );
static_assert((null . -123   ) === "-123"   );
static_assert((null . 123.123) === "123.123");
static_assert((null . []     ) === "Array"  ); // expected-warning{{implicit conversion from 'array' to 'string'}}

static_assert((null    . null) === ''       );
static_assert((''      . null) === ''       );
static_assert(("0"     . null) === "0"      );
static_assert(("qwe"   . null) === "qwe"    );
static_assert(("123"   . null) === "123"    );
static_assert((123     . null) === "123"    );
static_assert((-123    . null) === "-123"   );
static_assert((123.123 . null) === "123.123");
static_assert(([]      . null) === "Array"  ); // expected-warning{{implicit conversion from 'array' to 'string'}}


//----- integer with types ------------------------------------------
static_assert((123 . 123.4) === "123123.4");
static_assert((123 . 456  ) === "123456");
static_assert((456 . 123  ) === "456123");
static_assert((123 . 123  ) === "123123");
static_assert((123 . ''   ) === "123");
static_assert((123 . "qwe") === "123qwe");
static_assert((123 . "456") === "123456");
static_assert((123 . []   ) === "123Array"); // expected-warning{{implicit conversion from 'array' to 'string'}}

static_assert((123.4 . 123) === "123.4123");
static_assert((456   . 123) === "456123");
static_assert((123   . 456) === "123456");
static_assert((123   . 123) === "123123");
static_assert((''    . 123) === "123");
static_assert(("qwe" . 123) === "qwe123");
static_assert(("456" . 123) === "456123");
static_assert(([]    . 123) === "Array123"); // expected-warning{{implicit conversion from 'array' to 'string'}}


//----- double with types -------------------------------------------
static_assert((1.2 . 1.2   ) === "1.21.2");
static_assert((1.3 . 1.2   ) === "1.31.2");
static_assert((-.2 . ''    ) === "-0.2");
static_assert((1.2 . "qwe" ) === "1.2qwe");
static_assert((1.2 . "1e10") === "1.21e10");
static_assert((1.2 . []    ) === "1.2Array"); // expected-warning{{implicit conversion from 'array' to 'string'}}

static_assert((''     . -.2) === "-0.2");
static_assert(("qwe"  . 1.2) === "qwe1.2");
static_assert(("1e10" . 1.2) === "1e101.2");
static_assert(([]     . 1.2) === "Array1.2"); // expected-warning{{implicit conversion from 'array' to 'string'}}


//----- string with types -------------------------------------------
static_assert(("123"   . "0456" ) === "1230456");
static_assert(("qwe"   . "0456" ) === "qwe0456");
static_assert(("0456"  . "qwe"  ) === "0456qwe");
static_assert(("qwe"   . "qwe"  ) === "qweqwe");
static_assert(("qwe"   . []     ) === "qweArray"); // expected-warning{{implicit conversion from 'array' to 'string'}}
static_assert(([]      . "qwe"  ) === "Arrayqwe"); // expected-warning{{implicit conversion from 'array' to 'string'}}


//----- array with types --------------------------------------------
static_assert(([]      . []     )        === "ArrayArray"); // expected-warning{{implicit conversion from 'array' to 'string'}} expected-warning{{implicit conversion from 'array' to 'string'}}
static_assert(([3,4]   . [1,2,3])        === "ArrayArray"); // expected-warning{{implicit conversion from 'array' to 'string'}} expected-warning{{implicit conversion from 'array' to 'string'}}
static_assert((["a" => 1] . ["a" => 2])  === "ArrayArray"); // expected-warning{{implicit conversion from 'array' to 'string'}} expected-warning{{implicit conversion from 'array' to 'string'}}
