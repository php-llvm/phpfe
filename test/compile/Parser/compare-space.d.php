// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '<=>'
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
static_assert((false <=> null   ) ===  0); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((true  <=> null   ) ===  1); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((false <=> false  ) ===  0);
static_assert((false <=> true   ) === -1);
static_assert((true  <=> false  ) ===  1);
static_assert((true  <=> true   ) ===  0);
static_assert((true  <=> 123    ) ===  0); // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
static_assert((false <=> 123    ) === -1); // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
static_assert((true  <=> 123.456) ===  0); // expected-warning{{implicit conversion from 'double' to 'boolean'}}
static_assert((false <=> 123.456) === -1); // expected-warning{{implicit conversion from 'double' to 'boolean'}}
static_assert((true  <=> ''     ) ===  1); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((false <=> ''     ) ===  0); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((true  <=> "0"    ) ===  1); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((false <=> "0"    ) ===  0); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((true  <=> "qwe"  ) ===  0); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((false <=> "qwe"  ) === -1); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((false <=> []     ) ===  0); // expected-warning{{comparing array with non-array}}
static_assert((true  <=> []     ) ===  1); // expected-warning{{comparing array with non-array}}
static_assert((false <=> [0]    ) === -1); // expected-warning{{comparing array with non-array}}
static_assert((true  <=> [0]    ) ===  0); // expected-warning{{comparing array with non-array}}
static_assert((false <=> [123]  ) === -1); // expected-warning{{comparing array with non-array}}
static_assert((true  <=> [123]  ) ===  0); // expected-warning{{comparing array with non-array}}


static_assert((null    <=> false) ===  0); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((null    <=> true ) === -1); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((123     <=> true ) ===  0); // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
static_assert((123     <=> false) ===  1); // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
static_assert((123.456 <=> true ) ===  0); // expected-warning{{implicit conversion from 'double' to 'boolean'}}
static_assert((123.456 <=> false) ===  1); // expected-warning{{implicit conversion from 'double' to 'boolean'}}
static_assert((''      <=> true ) === -1); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((''      <=> false) ===  0); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(("0"     <=> true ) === -1); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(("0"     <=> false) ===  0); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(("qwe"   <=> true ) ===  0); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(("qwe"   <=> false) ===  1); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(([]      <=> false) ===  0); // expected-warning{{comparing array with non-array}}
static_assert(([]      <=> true ) === -1); // expected-warning{{comparing array with non-array}}
static_assert(([0]     <=> false) ===  1); // expected-warning{{comparing array with non-array}}
static_assert(([0]     <=> true ) ===  0); // expected-warning{{comparing array with non-array}}
static_assert(([123]   <=> false) ===  1); // expected-warning{{comparing array with non-array}}
static_assert(([123]   <=> true ) ===  0); // expected-warning{{comparing array with non-array}}


//----- null with types ---------------------------------------------
static_assert((null <=> null   ) ===  0);
static_assert((null <=> ''     ) ===  0); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((null <=> "0"    ) === -1); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((null <=> "qwe"  ) === -1); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((null <=> "123"  ) === -1); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((null <=> 123    ) === -1); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((null <=> -123   ) ===  1); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((null <=> 123.123) === -1); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((null <=> []     ) ===  0); // expected-warning{{comparing array with non-array}}
static_assert((null <=> [0]    ) === -1); // expected-warning{{comparing array with non-array}}
static_assert((null <=> [123]  ) === -1); // expected-warning{{comparing array with non-array}}

static_assert((null    <=> null) ===  0);
static_assert((''      <=> null) ===  0); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert(("0"     <=> null) ===  1); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert(("qwe"   <=> null) ===  1); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert(("123"   <=> null) ===  1); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((123     <=> null) ===  1); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((-123    <=> null) === -1); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((123.123 <=> null) ===  1); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert(([]      <=> null) ===  0); // expected-warning{{comparing array with non-array}}
static_assert(([0]     <=> null) ===  1); // expected-warning{{comparing array with non-array}}
static_assert(([123]   <=> null) ===  1); // expected-warning{{comparing array with non-array}}


//----- integer with types ------------------------------------------
static_assert((123 <=> 123.4) === -1);
static_assert((123 <=> 456  ) === -1);
static_assert((456 <=> 123  ) ===  1);
static_assert((123 <=> 123  ) ===  0);
static_assert((123 <=> ''   ) ===  1); // expected-warning{{string does not contain a number}}
static_assert((123 <=> "qwe") ===  1); // expected-warning{{string does not contain a number}}
static_assert((123 <=> "456") === -1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((123 <=> []   ) === -1); // expected-warning{{comparing array with non-array}}
static_assert((123 <=> [0]  ) === -1); // expected-warning{{comparing array with non-array}}
static_assert((123 <=> [123]) === -1); // expected-warning{{comparing array with non-array}}

static_assert((123.4 <=> 123) ===  1);
static_assert((456   <=> 123) ===  1);
static_assert((123   <=> 456) === -1);
static_assert((123   <=> 123) ===  0);
static_assert((''    <=> 123) === -1); // expected-warning{{string does not contain a number}}
static_assert(("qwe" <=> 123) === -1); // expected-warning{{string does not contain a number}}
static_assert(("456" <=> 123) ===  1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(([]    <=> 123) ===  1); // expected-warning{{comparing array with non-array}}
static_assert(([0]   <=> 123) ===  1); // expected-warning{{comparing array with non-array}}
static_assert(([123] <=> 123) ===  1); // expected-warning{{comparing array with non-array}}


//----- double with types -------------------------------------------
static_assert((1.2 <=> 1.2   ) ===  0);
static_assert((1.3 <=> 1.2   ) ===  1);
static_assert((1.2 <=> 1.3   ) === -1);
static_assert((-.2 <=> ''    ) === -1); // expected-warning{{string does not contain a number}}
static_assert((1.2 <=> "qwe" ) ===  1); // expected-warning{{string does not contain a number}}
static_assert((1.2 <=> "1e10") === -1); // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert((1.2 <=> []    ) === -1); // expected-warning{{comparing array with non-array}}
static_assert((1.2 <=> [0]   ) === -1); // expected-warning{{comparing array with non-array}}
static_assert((1.2 <=> [123] ) === -1); // expected-warning{{comparing array with non-array}}

static_assert((1.2    <=> 1.2) ===  0);
static_assert((1.2    <=> 1.3) === -1);
static_assert((1.3    <=> 1.2) ===  1);
static_assert((''     <=> -.2) ===  1); // expected-warning{{string does not contain a number}}
static_assert(("qwe"  <=> 1.2) === -1); // expected-warning{{string does not contain a number}}
static_assert(("1e10" <=> 1.2) ===  1); // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(([]     <=> 1.2) ===  1); // expected-warning{{comparing array with non-array}}
static_assert(([0]    <=> 1.2) ===  1); // expected-warning{{comparing array with non-array}}
static_assert(([123]  <=> 1.2) ===  1); // expected-warning{{comparing array with non-array}}


//----- string with types -------------------------------------------
static_assert(("123"   <=> "0456" ) === -1); // expected-warning 2 {{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"   <=> "0456" ) ===  1);
static_assert(("0456"  <=> "qwe"  ) === -1);
static_assert(("qwe"   <=> "qwe"  ) ===  0);
static_assert(("abcde" <=> "aacd" ) ===  1);
static_assert(("aacd"  <=> "abcde") === -1);
static_assert(("aacd"  <=> "abcde") === -1);
static_assert(("qwe"   <=> []     ) === -1); // expected-warning{{comparing array with non-array}}
static_assert(("qwe"   <=> [0]    ) === -1); // expected-warning{{comparing array with non-array}}
static_assert(("qwe"   <=> [123]  ) === -1); // expected-warning{{comparing array with non-array}}
static_assert(("-123"    <=> ".123"   ) === -1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    <=> ".123"   ) ===  1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    <=> ' .123'  ) ===  1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((' +123'   <=> ".123"   ) ===  1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert(("+123asd" <=> ".123qwe") === -1);

static_assert(("0456"  <=> "123"  ) ===  1); // expected-warning 2 {{implicit conversion from 'string' to 'integer'}}
static_assert(("0456"  <=> "qwe"  ) === -1);
static_assert(("qwe"   <=> "0456" ) ===  1);
static_assert(("qwe"   <=> "qwe"  ) ===  0);
static_assert(("aacd"  <=> "abcde") === -1);
static_assert(("abcde" <=> "aacd" ) ===  1);
static_assert(("abcde" <=> "aacd" ) ===  1);
static_assert(([]      <=> "qwe"  ) ===  1); // expected-warning{{comparing array with non-array}}
static_assert(([0]     <=> "qwe"  ) ===  1); // expected-warning{{comparing array with non-array}}
static_assert(([123]   <=> "qwe"  ) ===  1); // expected-warning{{comparing array with non-array}}
static_assert((".123"    <=> "-123"   ) ===  1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((".123"    <=> "+123"   ) === -1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((' .123'   <=> "+123"   ) === -1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((".123"    <=> ' +123'  ) === -1); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((".123qwe" <=> "+123asd") ===  1);


//----- array with types --------------------------------------------
static_assert(([]      <=> []     )  ===  0);
static_assert(([1,2,3] <=> []     )  ===  1);
static_assert(([]      <=> [1,2,3])  === -1);
static_assert(([3,4]   <=> [1,2,3])  === -1);
static_assert(([1,2]   <=> [1,2]  )  ===  0);

static_assert((["a" => 1] <=> ["b" => 1])  ===  1);
static_assert((["b" => 1] <=> ["a" => 1])  ===  1);

static_assert(([3 => 1] <=> [7 => 1])  ===  1);
static_assert(([7 => 1] <=> [3 => 1])  ===  1);

static_assert((["a" => 1] <=> ["a" => 2])  === -1);
static_assert((["a" => 2] <=> ["a" => 1])  ===  1);


