// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// unary '-'
//-------------------------------------------------------------------
static_assert((-true)  === -1);             // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((-false) ===  0);             // expected-warning{{implicit conversion from 'boolean' to 'integer'}}

static_assert(-null === 0);                 // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert(-1 === -1);
static_assert(-1.2 === -1.2);
static_assert(-"123" === -123);             // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(-"123.456" === -123.456);     // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(-"qwe" === 0);                // expected-warning{{string does not contain a number}}
static_assert(-"2e10000" < 0);              // expected-warning{{implicit conversion from 'string' to 'double'}}
                                            // expected-warning@-1{{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}}


//-------------------------------------------------------------------
// unary '+'
//-------------------------------------------------------------------

static_assert((+true)  === 1);             // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((+false) === 0);             // expected-warning{{implicit conversion from 'boolean' to 'integer'}}

static_assert(+1 === 1);
static_assert(+null === 0);                // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert(+1 === 1);
static_assert(+1.2 === 1.2);
static_assert(+"123" === 123);             // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(+"123.456" === 123.456);     // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(+"qwe" === 0);               // expected-warning{{string does not contain a number}}
static_assert(+"2e10000" > 0);             // expected-warning{{implicit conversion from 'string' to 'double'}}
                                           // expected-warning@-1{{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}}


//-------------------------------------------------------------------
// unary '~'
//-------------------------------------------------------------------
static_assert(~1 === -2);
static_assert(~1.2 === -2);     //expected-warning{{implicit conversion from 'double' to 'integer'}}

//TODO: add string test


//-------------------------------------------------------------------
// unary '!'
//-------------------------------------------------------------------
static_assert((!null) === true);           // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((!true) === false);
static_assert((!false) === true);
static_assert((!123) === false);           // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
static_assert((!1.2) === false);           // expected-warning{{implicit conversion from 'double' to 'boolean'}}
static_assert((!"qwe") === false);         // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((!"0") === true);            // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((!'') === true);             // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((![]) === true);             // expected-warning{{implicit conversion from 'array' to 'boolean'}}
static_assert((![0]) === false);           // expected-warning{{implicit conversion from 'array' to 'boolean'}}
static_assert((![1,2,3]) === false);       // expected-warning{{implicit conversion from 'array' to 'boolean'}}

//-------------------------------------------------------------------
// unary '++', '--'
//-------------------------------------------------------------------

?>
