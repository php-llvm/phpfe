// RUN: %clang_php %s -triple x86_64-unknown-linux-gnu -verify
<?php

function static_assert($a) { assert($a); } // expected-warning{{static_assert has special treatment that cannot be redefined}}

//--------------------------------------------------------------------
// builtin abs
//--------------------------------------------------------------------

abs(); // expected-error {{too few arguments in function call, expected 1, have 0}}
abs(1,2,3);// expected-error {{too many arguments in function call, expected 1, have 3}}

$x = abs(123);
$x = abs(-123);
$x = abs("qwe"); // expected-warning{{string does not contain a number}}

static_assert(abs(null  ) === 0);     // expected-warning{{implicit conversion from 'NULL' to 'integer'}}
static_assert(abs(false ) === 0);     // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(abs(true  ) === 1);     // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(abs(-123  ) === 123);
static_assert(abs(123   ) === 123);
static_assert(abs(123.4 ) === 123.4);
static_assert(abs(-123.4) === 123.4);
static_assert(abs("qwe" ) === 0);     // expected-warning{{string does not contain a number}}
static_assert(abs("123" ) === 123);   // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(abs("-123") === 123);   // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(abs("2e1000") > 0);     // expected-warning{{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}}
static_assert(abs([]    ) === false); // expected-error{{invalid argument type, expected number, have array}}


//--------------------------------------------------------------------
// builtin min
//--------------------------------------------------------------------

static_assert(min(1,2,3)     === 1);
static_assert(min(1,2,3) + 1 === 2);

static_assert(min(true, false)    === false);
static_assert(min(true, 123)      === true);
static_assert(min(true, 123.456)  === true);

static_assert(min(123, 456)       === 123);
static_assert(min(123, 123.456)   === 123);

static_assert(min(3.14, 123.456)  === 3.14);
static_assert(min(3.14, 123)      === 3.14);

static_assert(min(null, null)     === null);

//--------------------------------------------------------------------
// builtin max
//--------------------------------------------------------------------

static_assert(max(1,2,3)     === 3);
static_assert(max(1,2,3)     === 3);
static_assert(max(1,2,3) + 1 === 4);

static_assert(max(true, false)    === true);
static_assert(max(true, 123)      === 123);
static_assert(max(true, 123.456)  === 123.456);

static_assert(max(123, 456)       === 456);
static_assert(max(123, 123.456)   === 123.456);

static_assert(max(3.14, 123.456)  === 123.456);
static_assert(max(3.14, 123)      === 123);

static_assert(max(null, null)     === null);


//--------------------------------------------------------------------
// builtin ceil
//--------------------------------------------------------------------
function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }

static_assert(ceil(123) === 123.0);       // expected-warning{{argument is already integer}}
static_assert(ceil(1.1) === 2.0);
static_assert(ceil(1.5) === 2.0);
static_assert(ceil(1.8) === 2.0);


static_assert(ceil(-1.1) === -1.0);
static_assert(ceil(-1.5) === -1.0);
static_assert(ceil(-1.8) === -1.0);

static_assert(ceil(null) === 0.0);          // expected-error{{invalid argument type, expected double, have NULL}}

static_assert(ceil("qwe") === 0.0);         // expected-warning{{string does not contain a number}}
static_assert(ceil("123") === 123.0);       // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(ceil("1.12345e2") === 113.0); // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(ceil(true) === 1.0);          // expected-warning{{argument is already integer}}
static_assert(ceil(false) === 0.0);         // expected-warning{{argument is already integer}}

static_assert(ceil([1,2,3]) === false);     // expected-error{{invalid argument type, expected double, have array}}


assert(ceil(get_void()) === 0.0);       // expected-error{{invalid argument type, expected double, have NULL}} expected-error{{expected ')'}} expected-note{{to match this '('}}
assert(ceil(get_bool()) === 1.0);       // expected-warning{{argument is already integer}}
assert(ceil(get_int()) === 123.0);      // expected-warning{{argument is already integer}}
assert(ceil(get_double()) === 13.0);  
assert(ceil(get_string()) === 0.0);     // expected-warning{{implicit conversion from 'string' to 'double'}}
assert(ceil(get_array()) === false);    // expected-error{{invalid argument type, expected double, have array}} expected-error{{expected ')'}} expected-note{{to match this '('}}

//--------------------------------------------------------------------
// builtin floor
//--------------------------------------------------------------------
static_assert(floor(123) === 123.0);       // expected-warning{{argument is already integer}}
static_assert(floor(1.1) === 1.0);
static_assert(floor(1.5) === 1.0);
static_assert(floor(1.8) === 1.0);


static_assert(floor(-1.1) === -2.0);
static_assert(floor(-1.5) === -2.0);
static_assert(floor(-1.8) === -2.0);

static_assert(floor(null) === 0.0);          // expected-error{{invalid argument type, expected double, have NULL}}

static_assert(floor("qwe") === 0.0);         // expected-warning{{string does not contain a number}}
static_assert(floor("123") === 123.0);       // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(floor("1.12345e2") === 112.0); // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(floor(true) === 1.0);          // expected-warning{{argument is already integer}}
static_assert(floor(false) === 0.0);         // expected-warning{{argument is already integer}}

static_assert(floor([1,2,3]) === false);     // expected-error{{invalid argument type, expected double, have array}}


assert(floor(get_void()) === 0.0);       // expected-error{{invalid argument type, expected double, have NULL}} expected-error{{expected ')'}} expected-note{{to match this '('}}
assert(floor(get_bool()) === 1.0);       // expected-warning{{argument is already integer}}
assert(floor(get_int()) === 123.0);      // expected-warning{{argument is already integer}}
assert(floor(get_double()) === 12.0);  
assert(floor(get_string()) === 0.0);     // expected-warning{{implicit conversion from 'string' to 'double'}}
assert(floor(get_array()) === false);    // expected-error{{invalid argument type, expected double, have array}} expected-error{{expected ')'}} expected-note{{to match this '('}}


//-------------------------------------------------------------------
// round with defaults
//-------------------------------------------------------------------
static_assert(round( 2.375) ===  2.0);
static_assert(round(-2.375) === -2.0);

static_assert(round( 2.5) ===  3.0);
static_assert(round(-2.5) === -3.0);

static_assert(round( 2.825) ===  3.0);
static_assert(round(-2.825) === -3.0);

assert(round([]));        // expected-error{{invalid argument type, expected double, have array}}
assert(round(get_int())); // expected-warning{{argument is already integer}}
//-------------------------------------------------------------------
// round with precision
//-------------------------------------------------------------------
assert(round(get_double(), []));   // expected-error{{invalid argument type, expected integer, have array}}
assert(round(get_int(),  123));     // expected-warning{{argument is already integer}}
assert(round(get_int(), -123));

static_assert(round( 2.375, 1) ===  2.4);
static_assert(round(-2.375, 1) === -2.4);

static_assert(round( 2.5, 1) ===  2.5);
static_assert(round(-2.5, 1) === -2.5);

static_assert(round( 2.825, 1) ===  2.8);
static_assert(round(-2.825, 1) === -2.8);


static_assert(round( 2.825, -2) ===  0.0);
static_assert(round(-2.825, -2) === -0.0);

static_assert(round( 282.5, -2) ===  300.0);
static_assert(round(-282.5, -2) === -300.0);


static_assert(round( 2.5555555555555555555555555555555555555555, 30) ===  2.555555555555555555555555555556);
static_assert(round(-2.5555555555555555555555555555555555555555, 30) === -2.555555555555555555555555555556);

static_assert(round(2.8250123654789632147851598753214567896541, 30) === 2.825012365478963214785159875321);
static_assert(round(-2.8250123654789632147851598753214567896541, 30) === -2.825012365478963214785159875321);

//-------------------------------------------------------------------
// round with modes
//-------------------------------------------------------------------
//TODO: use constants

assert(round(get_double(), 0, []));   // expected-error{{invalid argument type, expected integer, have array}}

static_assert(round( 2.372, 2, 123) ===  2.37); // expected-warning{{invalid rounding mode, using PHP_ROUND_HALF_UP insted}}

// PHP_ROUND_HALF_UP    = 1
static_assert(round( 2.375, 0, 1) ===  2.0);
static_assert(round(-2.375, 0, 1) === -2.0);

static_assert(round( 2.5, 0, 1) ===  3.0);
static_assert(round(-2.5, 0, 1) === -3.0);

static_assert(round( 2.825, 0, 1) ===  3.0);
static_assert(round(-2.825, 0, 1) === -3.0);

static_assert(round( 5.375, 0, 1) ===  5.0);
static_assert(round(-5.375, 0, 1) === -5.0);

static_assert(round( 5.5, 0, 1) ===  6.0);
static_assert(round(-5.5, 0, 1) === -6.0);

static_assert(round( 5.825, 0, 1) ===  6.0);
static_assert(round(-5.825, 0, 1) === -6.0);

// PHP_ROUND_HALF_DOWN  = 2
static_assert(round( 2.375, 0, 2) ===  2.0);
static_assert(round(-2.375, 0, 2) === -2.0);

static_assert(round( 2.5, 0, 2) ===  2.0);
static_assert(round(-2.5, 0, 2) === -2.0);

static_assert(round( 2.825, 0, 2) ===  3.0);
static_assert(round(-2.825, 0, 2) === -3.0);

static_assert(round( 5.375, 0, 2) ===  5.0);
static_assert(round(-5.375, 0, 2) === -5.0);

static_assert(round( 5.5, 0, 2) ===  5.0);
static_assert(round(-5.5, 0, 2) === -5.0);

static_assert(round( 5.825, 0, 2) ===  6.0);
static_assert(round(-5.825, 0, 2) === -6.0);

// PHP_ROUND_HALF_EVEN  = 3
static_assert(round( 2.375, 0, 3) ===  2.0);
static_assert(round(-2.375, 0, 3) === -2.0);

static_assert(round( 2.5, 0, 3) ===  2.0);
static_assert(round(-2.5, 0, 3) === -2.0);

static_assert(round( 2.825, 0, 3) ===  3.0);
static_assert(round(-2.825, 0, 3) === -3.0);

static_assert(round( 5.375, 0, 3) ===  5.0);
static_assert(round(-5.375, 0, 3) === -5.0);

static_assert(round( 5.5, 0, 3) ===  6.0);
static_assert(round(-5.5, 0, 3) === -6.0);

static_assert(round( 5.825, 0, 3) ===  6.0);
static_assert(round(-5.825, 0, 3) === -6.0);

// PHP_ROUND_HALF_ODD   = 4
static_assert(round( 2.375, 0, 4) ===  2.0);
static_assert(round(-2.375, 0, 4) === -2.0);

static_assert(round( 2.5, 0, 4) ===  3.0);
static_assert(round(-2.5, 0, 4) === -3.0);

static_assert(round( 2.825, 0, 4) ===  3.0);
static_assert(round(-2.825, 0, 4) === -3.0);

static_assert(round( 5.375, 0, 4) ===  5.0);
static_assert(round(-5.375, 0, 4) === -5.0);

static_assert(round( 5.5, 0, 4) ===  5.0);
static_assert(round(-5.5, 0, 4) === -5.0);

static_assert(round( 5.825, 0, 4) ===  6.0);
static_assert(round(-5.825, 0, 4) === -6.0);

//-------------------------------------------------------------------
// round with precision and modes
//-------------------------------------------------------------------
//TODO: use constants

// PHP_ROUND_HALF_UP    = 1
static_assert(round( 2.372, 2, 1) ===  2.37);
static_assert(round(-2.372, 2, 1) === -2.37);

static_assert(round( 2.575, 2, 1) ===  2.58);
static_assert(round(-2.575, 2, 1) === -2.58);

static_assert(round( 2.877, 2, 1) ===  2.88);
static_assert(round(-2.877, 2, 1) === -2.88);


static_assert(round( 2.322, 2, 1) ===  2.32);
static_assert(round(-2.322, 2, 1) === -2.32);

static_assert(round( 2.525, 2, 1) ===  2.53);
static_assert(round(-2.525, 2, 1) === -2.53);

static_assert(round( 2.827, 2, 1) ===  2.83);
static_assert(round(-2.827, 2, 1) === -2.83);


// PHP_ROUND_HALF_DOWN  = 2
static_assert(round( 2.372, 2, 2) ===  2.37);
static_assert(round(-2.372, 2, 2) === -2.37);

static_assert(round( 2.575, 2, 2) ===  2.57);
static_assert(round(-2.575, 2, 2) === -2.57);

static_assert(round( 2.877, 2, 2) ===  2.88);
static_assert(round(-2.877, 2, 2) === -2.88);


static_assert(round( 2.322, 2, 2) ===  2.32);
static_assert(round(-2.322, 2, 2) === -2.32);

static_assert(round( 2.525, 2, 2) ===  2.52);
static_assert(round(-2.525, 2, 2) === -2.52);

static_assert(round( 2.827, 2, 2) ===  2.83);
static_assert(round(-2.827, 2, 2) === -2.83);

// PHP_ROUND_HALF_EVEN  = 3
static_assert(round( 2.372, 2, 3) ===  2.37);
static_assert(round(-2.372, 2, 3) === -2.37);

static_assert(round( 2.575, 2, 3) ===  2.58);
static_assert(round(-2.575, 2, 3) === -2.58);

static_assert(round( 2.877, 2, 3) ===  2.88);
static_assert(round(-2.877, 2, 3) === -2.88);


static_assert(round( 2.322, 2, 3) ===  2.32);
static_assert(round(-2.322, 2, 3) === -2.32);

static_assert(round( 2.525, 2, 3) ===  2.52);
static_assert(round(-2.525, 2, 3) === -2.52);

static_assert(round( 2.827, 2, 3) ===  2.83);
static_assert(round(-2.827, 2, 3) === -2.83);

// PHP_ROUND_HALF_ODD   = 4
static_assert(round( 2.372, 2, 4) ===  2.37);
static_assert(round(-2.372, 2, 4) === -2.37);

static_assert(round( 2.575, 2, 4) ===  2.57);
static_assert(round(-2.575, 2, 4) === -2.57);

static_assert(round( 2.877, 2, 4) ===  2.88);
static_assert(round(-2.877, 2, 4) === -2.88);


static_assert(round( 2.322, 2, 4) ===  2.32);
static_assert(round(-2.322, 2, 4) === -2.32);

static_assert(round( 2.525, 2, 4) ===  2.53);
static_assert(round(-2.525, 2, 4) === -2.53);

static_assert(round( 2.827, 2, 4) ===  2.83);
static_assert(round(-2.827, 2, 4) === -2.83);



//-------------------------------------------------------------------
// fmod with defaults
//-------------------------------------------------------------------
static_assert(fmod( 1, 2) ===  1.0);
static_assert(fmod( 3, 5) ===  3.0);
static_assert(fmod( 6, 5) ===  1.0);
static_assert(fmod(-7, 5) === -2.0);
static_assert(fmod(15, 1) ===  0.0);

static_assert(abs(fmod(15.456, 2) - 1.456) < 1e-10);
static_assert(abs(fmod(15.456, 2.5) - 0.456) < 1e-10);
static_assert(abs(fmod(15.456, 2.12) - 0.616) < 1e-10);
static_assert(abs(fmod(15.456, 2.91) - 0.906) < 1e-10);

static_assert(fmod(true, 2) ===  1.0);    // expected-warning{{implicit conversion from 'boolean' to 'double'}}
static_assert(fmod(false, 5) ===  0.0);   // expected-warning{{implicit conversion from 'boolean' to 'double'}}
static_assert(fmod("123", 5) ===  3.0);   // expected-warning{{implicit conversion from 'string' to 'double'}}

static_assert(fmod("qwe", 5) === 0.0);    // expected-warning{{string does not contain a number}}
static_assert(fmod([], 1) ===  0.0);      // expected-error{{invalid argument type, expected double, have array}}


//-------------------------------------------------------------------
// intdiv with defaults
//-------------------------------------------------------------------
static_assert(intdiv( 1, 2) ===  0);
static_assert(intdiv(15, 5) ===  3);
static_assert(intdiv( 6, 5) ===  1);
static_assert(intdiv(-7, 5) === -1);
static_assert(intdiv(15, 1) === 15);

static_assert(intdiv(15.456, 2) === 7);   // expected-warning{{implicit conversion from 'double' to 'integer'}}
static_assert(intdiv(15.456, 2.5) === 7); // expected-warning{{implicit conversion from 'double' to 'integer'}} expected-warning{{implicit conversion from 'double' to 'integer'}}

static_assert(intdiv(true, 2) ===  0);    // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(intdiv(false, 5) ===  0);   // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(intdiv("123", 5) ===  24);   // expected-warning{{implicit conversion from 'string' to 'integer'}}

static_assert(intdiv("qwe", 5) === 0);    // expected-warning{{string does not contain a number}}
static_assert(intdiv([], 1) ===  0);      // expected-error{{invalid argument type, expected integer, have array}}
static_assert(intdiv(123, 0) ===  0);      // expected-error{{division by zero}}


?>