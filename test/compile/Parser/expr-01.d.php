// RUN: %clang_php -triple i686-pc-windows-msvc18.0.0 %s -verify
<?php


$a = 123456789012345678901; // expected-warning{{literal cannot be represented as integer (max is 2147483647), converted to float}}
$a = 2e1000;                // expected-warning{{magnitude of floating-point constant too large for type 'double'; maximum is}}

//==============================================================================
// Tests for diagnostics on malformed expressions
//==============================================================================

echo 1 == 2 == 3;       // expected-error{{operator is not associative}}
echo 1 != 2 == 3;       // expected-error{{operator is not associative}}
echo 1 === 2 === 3;     // expected-error{{operator is not associative}}
echo 1 === 2 !== 3;     // expected-error{{operator is not associative}}
echo 1 < 2 < 3;         // expected-error{{operator is not associative}}
echo 1 < 2 <= 3;        // expected-error{{operator is not associative}}
echo 1 > 2 > 3;         // expected-error{{operator is not associative}}
echo 1 > 2 >= 3;        // expected-error{{operator is not associative}}
echo 1 <> 2 <> 3;       // expected-error{{operator is not associative}}
echo 1 <=> 2 <=> 3;     // expected-error{{operator is not associative}}

1 = 1;      // expected-error{{operation may be applied only to lvalues}}
1 += 1;     // expected-error{{operation may be applied only to lvalues}}
1 -= 1;     // expected-error{{operation may be applied only to lvalues}}
1 *= 1;     // expected-error{{operation may be applied only to lvalues}}
1 /= 1;     // expected-error{{operation may be applied only to lvalues}}
1 %= 1;     // expected-error{{operation may be applied only to lvalues}}
1 <<= 1;    // expected-error{{operation may be applied only to lvalues}}
1 >>= 1;    // expected-error{{operation may be applied only to lvalues}}
1 **= 1;    // expected-error{{operation may be applied only to lvalues}}
1 .= 1;     // expected-error{{operation may be applied only to lvalues}}
1 &= 1;     // expected-error{{operation may be applied only to lvalues}}
1 |= 1;     // expected-error{{operation may be applied only to lvalues}}
1 ^= 1;     // expected-error{{operation may be applied only to lvalues}}

$a = $b = 4;