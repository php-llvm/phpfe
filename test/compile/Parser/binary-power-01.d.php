// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '**'
//-------------------------------------------------------------------
static_assert(0 ** 0 == 0);       // expected-error{{operation result is undefined}}
static_assert(0 ** false == 0);   // expected-error{{operation result is undefined}}
                                  // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(0 ** 0 == 0);       // expected-error{{operation result is undefined}}
static_assert(0 ** 0.0 == 0);     // expected-error{{operation result is undefined}}
static_assert(0 ** "+0" == 0);    // expected-error{{operation result is undefined}}
                                  // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert(0 ** "-0.0" == 0);  // expected-error{{operation result is undefined}}
                                  // expected-warning@-1{{implicit conversion from 'string' to 'double'}}

static_assert(0 ** -123 == 0);    // expected-error{{division by zero}}
static_assert(0 ** [] == 0);      // expected-error{{operation cannot be applied to arrays}}
static_assert([] ** 0 == 0);      // expected-error{{operation cannot be applied to arrays}}

static_assert(0 ** 0 == 0);       // expected-error{{operation result is undefined}}
static_assert(0 ** 0.0 == 0);     // expected-error{{operation result is undefined}}
static_assert(0.0 ** 0 == 0);     // expected-error{{operation result is undefined}}

static_assert(123 ** null == 1);  // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert(null ** 123 == 0);  // expected-warning{{'null' implicitly converted to 'zero'}}

static_assert((true ** false)  === 1);    // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((2 ** 5) === 32);
static_assert((4 ** 0.5)  === 2.0);
static_assert(abs((1.21 ** 0.5) - 1.1)  < 1e-10);
static_assert(("-2" ** "3")  === -8);     // expected-warning 2 {{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe" ** "3")  === 0);     // expected-warning{{string does not contain a number}}
                                          // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert(abs(("1e4" ** -0.5) - 0.01)  < 1e-10);// expected-warning{{implicit conversion from 'string' to 'double'}}
