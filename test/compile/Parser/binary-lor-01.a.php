// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '||'
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
static_assert((false || false  ) === false);
static_assert((false || true   ) === true );
static_assert((true  || false  ) === true );
static_assert((true  || true   ) === true );

static_assert((false || null   ) === false);
static_assert((true  || null   ) === true );
static_assert((true  || 123    ) === true );
static_assert((false || 123    ) === true );
static_assert((true  || 0      ) === true );
static_assert((false || 0      ) === false);
static_assert((true  || 123.456) === true );
static_assert((false || 123.456) === true );
static_assert((true  || 0.0    ) === true );
static_assert((false || 0.0    ) === false);
static_assert((true  || ''     ) === true );
static_assert((false || ''     ) === false);
static_assert((true  || "0"    ) === true );
static_assert((false || "0"    ) === false);
static_assert((true  || "qwe"  ) === true );
static_assert((false || "qwe"  ) === true );
static_assert((false || []     ) === false);
static_assert((true  || []     ) === true );
static_assert((false || [1]    ) === true );
static_assert((true  || [1]    ) === true );

static_assert((null    || false) === false);
static_assert((null    || true ) === true );
static_assert((123     || true ) === true );
static_assert((123     || false) === true );
static_assert((0       || true ) === true );
static_assert((0       || false) === false);
static_assert((123.456 || true ) === true );
static_assert((123.456 || false) === true );
static_assert((0.0     || true ) === true );
static_assert((0.0     || false) === false);
static_assert((''      || true ) === true );
static_assert((''      || false) === false);
static_assert(("0"     || true ) === true );
static_assert(("0"     || false) === false);
static_assert(("qwe"   || true ) === true );
static_assert(("qwe"   || false) === true );
static_assert(([]      || false) === false);
static_assert(([]      || true ) === true );
static_assert(([1]     || false) === true );
static_assert(([1]     || true ) === true );


//----- null with types ---------------------------------------------
static_assert((null || null   ) === false);
static_assert((null || ''     ) === false);
static_assert((null || "0"    ) === false);
static_assert((null || "qwe"  ) === true );
static_assert((null || "123"  ) === true );
static_assert((null || 123    ) === true );
static_assert((null || -123   ) === true );
static_assert((null || 0      ) === false);
static_assert((null || 123.123) === true );
static_assert((null || 0.0    ) === false);
static_assert((null || []     ) === false);
static_assert((null || [1]    ) === true );

static_assert((''      || null) === false);
static_assert(("0"     || null) === false);
static_assert(("qwe"   || null) === true );
static_assert(("123"   || null) === true );
static_assert((123     || null) === true );
static_assert((-123    || null) === true );
static_assert((0       || null) === false);
static_assert((123.123 || null) === true );
static_assert((0.0     || null) === false);
static_assert(([]      || null) === false);
static_assert(([1]     || null) === true );


//----- integer with types ------------------------------------------
static_assert((123 || 0    ) === true );
static_assert((123 || 123  ) === true );
static_assert((123 || 123.4) === true );
static_assert((123 || 0.0  ) === true );
static_assert((123 || ''   ) === true );
static_assert((123 || '0'  ) === true );
static_assert((123 || "qwe") === true );
static_assert((123 || '0.0') === true );
static_assert((123 || []   ) === true );
static_assert((123 || [1]  ) === true );

static_assert((0     || 123) === true );
static_assert((123.4 || 123) === true );
static_assert((0.0   || 123) === true );
static_assert((''    || 123) === true );
static_assert(('0'   || 123) === true );
static_assert(('0.0' || 123) === true );
static_assert(("qwe" || 123) === true );
static_assert(([]    || 123) === true );
static_assert(([1]   || 123) === true );


//----- double with types -------------------------------------------
static_assert((1.2 || 1.2   ) === true );
static_assert((1.3 || 0.0   ) === true );
static_assert((0.0 || 1.3   ) === true );
static_assert((-.2 || ''    ) === true );
static_assert((-.2 || '0'   ) === true );
static_assert((1.2 || "qwe" ) === true );
static_assert((1.2 || "0.0" ) === true );

static_assert((''     || -.2) === true );
static_assert(('0'    || -.2) === true );
static_assert(("qwe"  || 1.2) === true );
static_assert(("0.0"  || 1.2) === true );


//----- array with types -------------------------------------------
static_assert(([]      || []     )  === false);
static_assert(([1,2,3] || []     )  === true );
static_assert(([3,4]   || [1,2,3])  === true );
static_assert(([1,2]   || [1,2]  )  === true );
