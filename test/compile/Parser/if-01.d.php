// RUN: %clang_php %s -verify
<?php

if (1 . 2)::abc echo 1; // expected-error{{expected expression}}
if;   // expected-error{{expected '('}}
if(;
// expected-error@-1 {{expected expression}}
// expected-error@-2 {{expected ')'}}
// expected-note@-3 {{to match this '('}}

if(); // expected-error{{expected expression}}
if(1) 2 echo 3; // expected-error{{expected ';' after <expression> statement}}
if(1) echo 2; else 3 $a = 4;// expected-error{{expected ';' after <expression> statement}}
if(1) echo 1; elseif; // expected-error{{expected '('}}
if(1) echo 1; elseif 1;// expected-error{{expected '('}}
if(1) echo 1; elseif (1;
// expected-error@-1 {{expected ')'}}
// expected-note@-2 {{to match this '('}}

if(1): 1 endif; //expected-error{{expected ';' after <expression> statement}}
if(1): else endif; //expected-error{{expected ':'}}
if(1): echo 1; else;//expected-error{{expected ':'}}
if(1): echo 1; elseif echo 2; // expected-error{{expected '('}}, expected-error{{expected ':'}}
if(1): echo 1; elseif (2); //expected-error{{expected ':'}}
if(1): echo 1; elseif (2) echo 1;//expected-error{{expected ':'}}
if(1): echo 1; elseif (2) endif;//expected-error{{expected ':'}}

?>