// RUN: %clang_php %s -verify
<?php

//--------------------------------------------------------------------
// call simple function
//--------------------------------------------------------------------
function fn() {}
fn();
fn(1); // expected-error{{too many arguments in function call, expected 0, have 1}}

//--------------------------------------------------------------------
// call function with arguments
//--------------------------------------------------------------------
function arg1($a) {}
arg1();     // expected-error{{too few arguments in function call, expected 1, have 0}}
arg1(1);
arg1(1,2,3);// expected-error{{too many arguments in function call, expected 1, have 3}}

//--------------------------------------------------------------------
// call function with arguments with type hints
//--------------------------------------------------------------------
function arg2($a, int $b, bool $c, string $d, float $e) {}
arg2(1, 3.14, null, true, 123); // \
    // expected-warning{{implicit conversion from 'double' to 'integer'}} \
    // expected-error{{invalid argument type, expected boolean, have NULL}} 

function fn_01(bool     $a) { echo $a; echo "\n"; }
function fn_02(boolean  $a) { echo $a; echo "\n"; }
function fn_03(int      $a) { echo $a; echo "\n"; }
function fn_04(long     $a) { echo $a; echo "\n"; }
function fn_05(integer  $a) { echo $a; echo "\n"; }
function fn_06(float    $a) { echo $a; echo "\n"; }
function fn_07(real     $a) { echo $a; echo "\n"; }
function fn_08(double   $a) { echo $a; echo "\n"; }
function fn_09(string   $a) { echo $a; echo "\n"; }
function fn_10(binary   $a) { echo $a; echo "\n"; }

// implicit cast to bool
fn_01(null);          //expected-error{{invalid argument type, expected boolean, have NULL}}
fn_01(true);
fn_01(false);
fn_01(123);
fn_01(123.456);
fn_01('qwe');
fn_01('123qwe');
fn_01('123');
fn_01('-123');
fn_01('123.456');


fn_02(null);          //expected-error{{invalid argument type, expected boolean, have NULL}}
fn_02(true);
fn_02(false);
fn_02(123);
fn_02(123.456);
fn_02('qwe');
fn_02('123qwe');
fn_02('123');
fn_02('-123');
fn_02('123.456');


// implicit cast to int
fn_03(null);          //expected-error{{invalid argument type, expected integer, have NULL}}
fn_03(true);
fn_03(false);
fn_03(123);
fn_03(123.456);       //expected-warning{{implicit conversion from 'double' to 'integer'}}
fn_03('qwe');         
fn_03('123qwe');
fn_03('123');
fn_03('-123');
fn_03('123.456');

fn_04(null);          //expected-error{{invalid argument type, expected integer, have NULL}}
fn_04(true);
fn_04(false);
fn_04(123);
fn_04(123.456);       //expected-warning{{implicit conversion from 'double' to 'integer'}}
fn_04('qwe');         
fn_04('123qwe');
fn_04('123');
fn_04('-123');
fn_04('123.456');

fn_05(null);          //expected-error{{invalid argument type, expected integer, have NULL}}
fn_05(true);
fn_05(false);
fn_05(123);
fn_05(123.456);       //expected-warning{{implicit conversion from 'double' to 'integer'}}
fn_05('qwe');         
fn_05('123qwe');
fn_05('123');
fn_05('-123');
fn_05('123.456');


// implicit cast to double
fn_06(null);          //expected-error{{invalid argument type, expected double, have NULL}}
fn_06(true);
fn_06(false);
fn_06(123);
fn_06(123.456);
fn_06('qwe');         
fn_06('123qwe');
fn_06('123');
fn_06('-123');
fn_06('123.456');

fn_07(null);          //expected-error{{invalid argument type, expected double, have NULL}}
fn_07(true);
fn_07(false);
fn_07(123);
fn_07(123.456);
fn_07('qwe');         
fn_07('123qwe');
fn_07('123');
fn_07('-123');
fn_07('123.456');

fn_08(null);          //expected-error{{invalid argument type, expected double, have NULL}}
fn_08(true);
fn_08(false);
fn_08(123);
fn_08(123.456);
fn_08('qwe');         
fn_08('123qwe');
fn_08('123');
fn_08('-123');
fn_08('123.456');


// implicit cast to string
fn_09(null);          //expected-error{{invalid argument type, expected string, have NULL}}
fn_09(true);
fn_09(false);
fn_09(123);
fn_09(123.456);
fn_09('qwe');
fn_09('123qwe');
fn_09('123');
fn_09('-123');
fn_09('123.456');

fn_10(null);          //expected-error{{invalid argument type, expected string, have NULL}}
fn_10(true);
fn_10(false);
fn_10(123);
fn_10(123.456);
fn_10('qwe');
fn_10('123qwe');
fn_10('123');
fn_10('-123');
fn_10('123.456');


//--------------------------------------------------------------------
// call function with forward declaration
//--------------------------------------------------------------------
//forward(); //TODO: emit errors
forward(1);
//forward(1,2,3); //TODO: emit errors
function forward($a) {
  echo "forward!\n";
}
forward();      // expected-error{{too few arguments in function call, expected 1, have 0}}
forward(1);
forward(1,2,3); // expected-error{{too many arguments in function call, expected 1, have 3}}


//--------------------------------------------------------------------
// call function with forward declaration with type hints
//--------------------------------------------------------------------
//forward_args(1, 3.14, null, true, 123); //TODO: emit errors
function forward_args($a, int $b, bool $c, string $d, float $e2) {
  echo "forward_args!\n";
}

//--------------------------------------------------------------------
// call variadic function with type hints
//--------------------------------------------------------------------
function fn_500(        ...$a) { echo "fn_500!\n"; }
function fn_501(bool    ...$a) { echo "fn_501!\n"; }
function fn_502(boolean ...$a) { echo "fn_502!\n"; }
function fn_503(int     ...$a) { echo "fn_503!\n"; }
function fn_504(long    ...$a) { echo "fn_504!\n"; }
function fn_505(integer ...$a) { echo "fn_505!\n"; }
function fn_506(float   ...$a) { echo "fn_506!\n"; }
function fn_507(real    ...$a) { echo "fn_507!\n"; }
function fn_508(double  ...$a) { echo "fn_508!\n"; }
function fn_509(string  ...$a) { echo "fn_509!\n"; }
function fn_510(binary  ...$a) { echo "fn_510!\n"; }

fn_500(true, false, null, 1, 1.1, "qwe");
fn_501(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected boolean, have NULL}}
fn_502(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected boolean, have NULL}}
fn_503(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected integer, have NULL}} \
                                             expected-warning{{implicit conversion from 'double' to 'integer'}} 
fn_504(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected integer, have NULL}} \
                                             expected-warning{{implicit conversion from 'double' to 'integer'}} 
fn_505(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected integer, have NULL}} \
                                             expected-warning{{implicit conversion from 'double' to 'integer'}} 
fn_506(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected double, have NULL}}
fn_507(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected double, have NULL}}
fn_508(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected double, have NULL}}
fn_509(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected string, have NULL}}
fn_510(true, false, null, 1, 1.1, "qwe"); // expected-error{{invalid argument type, expected string, have NULL}}


//--------------------------------------------------------------------
// call variadic function with type hints references
//--------------------------------------------------------------------                                         
function fn_600(        &...$a) { echo "fn_600!\n"; }
function fn_601(bool    &...$a) { echo "fn_601!\n"; }
function fn_602(boolean &...$a) { echo "fn_602!\n"; }
function fn_603(int     &...$a) { echo "fn_603!\n"; }
function fn_604(long    &...$a) { echo "fn_604!\n"; }
function fn_605(integer &...$a) { echo "fn_605!\n"; }
function fn_606(float   &...$a) { echo "fn_606!\n"; }
function fn_607(real    &...$a) { echo "fn_607!\n"; }
function fn_608(double  &...$a) { echo "fn_608!\n"; }
function fn_609(string  &...$a) { echo "fn_609!\n"; }
function fn_610(binary  &...$a) { echo "fn_610!\n"; }
//function fn_611(array   &...$a) { echo "fn_611!\n"; }

fn_600();
fn_600(123, $v_6002, $v_6003);              // expected-error{{expected l-value}}

fn_601();
fn_601($v_6011, true, $v_6013, $v_6014);    // expected-error{{expected l-value}}

fn_602();
fn_602($v_6021, $v_6022, false);            // expected-error{{expected l-value}}

fn_603();
fn_603($v_6031, 123, $v_6033, $v_6034);     // expected-error{{expected l-value}}

fn_604();
fn_604(345, $v_6042, $v_6043);              // expected-error{{expected l-value}}

fn_605();
fn_605($v_6051, $v_6052, $v_6053, 456);     // expected-error{{expected l-value}}

fn_606();
fn_606($v_6061, 1.1, $v_6063);              // expected-error{{expected l-value}}

fn_607();
fn_607(2.2, $v_6072, $v_6073, $v_6074);     // expected-error{{expected l-value}}

fn_608();
fn_608($v_6081, $v_6082, 3.3);              // expected-error{{expected l-value}}

fn_609();
fn_609($v_6091, "qwe", $v_6093, $v_6094);   // expected-error{{expected l-value}}

fn_610();
fn_610($v_6101, $v_6102, "qwe");            // expected-error{{expected l-value}}

//--------------------------------------------------------------------
// parameter redeclaration
//-------------------------------------------------------------------- 
function fn_err_1($a, $a, int $a) {} // expected-error{{parameter with this name is already defined}} \
                                        expected-note{{previous declaration is here}} \
                                        expected-error{{parameter with this name is already defined}} \
                                        expected-note{{previous declaration is here}}

$b = 123;
function fn_okay_1($a, $b) {}

?>