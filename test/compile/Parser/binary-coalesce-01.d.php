// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '??'
//-------------------------------------------------------------------
static_assert((null    ?? []) === []     );
static_assert((true    ?? []) === true   );
static_assert((false   ?? []) === false  );
static_assert((123     ?? []) === 123    );
static_assert((0       ?? []) === 0      );
static_assert((1.2     ?? []) === 1.2    );
static_assert((0.0     ?? []) === 0.0    );
static_assert((''      ?? []) === ''     );
static_assert(("qwe"   ?? []) === "qwe"  );
static_assert(([]      ?? []) === []     );
static_assert(([1,2,3] ?? []) === [1,2,3]);
static_assert((true    ?? $a) === true   );


static_assert((null ?? null   ) === null   );
static_assert((null ?? true   ) === true   );
static_assert((null ?? false  ) === false  );
static_assert((null ?? 123    ) === 123    );
static_assert((null ?? 0      ) === 0      );
static_assert((null ?? 1.2    ) === 1.2    );
static_assert((null ?? 0.0    ) === 0.0    );
static_assert((null ?? ''     ) === ''     );
static_assert((null ?? "qwe"  ) === "qwe"  );
static_assert((null ?? []     ) === []     );
static_assert((null ?? [1,2,3]) === [1,2,3]);

