// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}



//-------------------------------------------------------------------
// binary 'or'
//-------------------------------------------------------------------

//----- null with types ---------------------------------------------
static_assert((null or null   ) === false);
static_assert((null or true   ) === true );
static_assert((null or false  ) === false);
static_assert((null or 123    ) === true );
static_assert((null or 0      ) === false);
static_assert((null or 1.2    ) === true );
static_assert((null or 0.0    ) === false);
static_assert((null or ''     ) === false);
static_assert((null or "qwe"  ) === true );
static_assert((null or []     ) === false);
static_assert((null or [1,2,3]) === true );

static_assert((null    or null) === false);
static_assert((true    or null) === true );
static_assert((false   or null) === false);
static_assert((123     or null) === true );
static_assert((0       or null) === false);
static_assert((1.2     or null) === true );
static_assert((0.0     or null) === false);
static_assert((''      or null) === false);
static_assert(("qwe"   or null) === true );
static_assert(([]      or null) === false);
static_assert(([1,2,3] or null) === true );


//----- false with types --------------------------------------------
static_assert((false or null   ) === false);
static_assert((false or true   ) === true );
static_assert((false or false  ) === false);
static_assert((false or 123    ) === true );
static_assert((false or 0      ) === false);
static_assert((false or 1.2    ) === true );
static_assert((false or 0.0    ) === false);
static_assert((false or ''     ) === false);
static_assert((false or "qwe"  ) === true );
static_assert((false or []     ) === false);
static_assert((false or [1,2,3]) === true );

static_assert((null    or false) === false);
static_assert((true    or false) === true );
static_assert((false   or false) === false);
static_assert((123     or false) === true );
static_assert((0       or false) === false);
static_assert((1.2     or false) === true );
static_assert((0.0     or false) === false);
static_assert((''      or false) === false);
static_assert(("qwe"   or false) === true );
static_assert(([]      or false) === false);
static_assert(([1,2,3] or false) === true );


//----- true with types ---------------------------------------------
static_assert((true or null   ) === true);
static_assert((true or true   ) === true);
static_assert((true or false  ) === true);
static_assert((true or 123    ) === true);
static_assert((true or 0      ) === true);
static_assert((true or 1.2    ) === true);
static_assert((true or 0.0    ) === true);
static_assert((true or ''     ) === true);
static_assert((true or "qwe"  ) === true);
static_assert((true or []     ) === true);
static_assert((true or [1,2,3]) === true);
static_assert((true or $a     ) === true);

static_assert((null    or true) === true);
static_assert((true    or true) === true);
static_assert((false   or true) === true);
static_assert((123     or true) === true);
static_assert((0       or true) === true);
static_assert((1.2     or true) === true);
static_assert((0.0     or true) === true);
static_assert((''      or true) === true);
static_assert(("qwe"   or true) === true);
static_assert(([]      or true) === true);
static_assert(([1,2,3] or true) === true);


//----- integer with types ------------------------------------------
static_assert((123 or null   ) === true);
static_assert((123 or true   ) === true);
static_assert((123 or false  ) === true);
static_assert((123 or 123    ) === true);
static_assert((123 or 0      ) === true);
static_assert((123 or 1.2    ) === true);
static_assert((123 or 0.0    ) === true);
static_assert((123 or ''     ) === true);
static_assert((123 or "qwe"  ) === true);
static_assert((123 or []     ) === true);
static_assert((123 or [1,2,3]) === true);
static_assert((123 or $a     ) === true);

static_assert((null    or 123) === true);
static_assert((true    or 123) === true);
static_assert((false   or 123) === true);
static_assert((123     or 123) === true);
static_assert((0       or 123) === true);
static_assert((1.2     or 123) === true);
static_assert((0.0     or 123) === true);
static_assert((''      or 123) === true);
static_assert(("qwe"   or 123) === true);
static_assert(([]      or 123) === true);
static_assert(([1,2,3] or 123) === true);

static_assert((0 or null   ) === false);
static_assert((0 or true   ) === true );
static_assert((0 or false  ) === false);
static_assert((0 or 123    ) === true );
static_assert((0 or 0      ) === false);
static_assert((0 or 1.2    ) === true );
static_assert((0 or 0.0    ) === false);
static_assert((0 or ''     ) === false);
static_assert((0 or "qwe"  ) === true );
static_assert((0 or []     ) === false);
static_assert((0 or [1,2,3]) === true );

static_assert((null    or 0) === false);
static_assert((true    or 0) === true );
static_assert((false   or 0) === false);
static_assert((123     or 0) === true );
static_assert((0       or 0) === false);
static_assert((1.2     or 0) === true );
static_assert((0.0     or 0) === false);
static_assert((''      or 0) === false);
static_assert(("qwe"   or 0) === true );
static_assert(([]      or 0) === false);
static_assert(([1,2,3] or 0) === true );


//----- double with types -------------------------------------------
static_assert((1.3 or null   ) === true);
static_assert((1.3 or true   ) === true);
static_assert((1.3 or false  ) === true);
static_assert((1.3 or 123    ) === true);
static_assert((1.3 or 0      ) === true);
static_assert((1.3 or 1.2    ) === true);
static_assert((1.3 or 0.0    ) === true);
static_assert((1.3 or ''     ) === true);
static_assert((1.3 or "qwe"  ) === true);
static_assert((1.3 or []     ) === true);
static_assert((1.3 or [1,2,3]) === true);
static_assert((1.3 or $a     ) === true);

static_assert((null    or 1.3) === true);
static_assert((true    or 1.3) === true);
static_assert((false   or 1.3) === true);
static_assert((123     or 1.3) === true);
static_assert((0       or 1.3) === true);
static_assert((1.2     or 1.3) === true);
static_assert((0.0     or 1.3) === true);
static_assert((''      or 1.3) === true);
static_assert(("qwe"   or 1.3) === true);
static_assert(([]      or 1.3) === true);
static_assert(([1,2,3] or 1.3) === true);

static_assert((0.0 or null   ) === false);
static_assert((0.0 or true   ) === true );
static_assert((0.0 or false  ) === false);
static_assert((0.0 or 123    ) === true );
static_assert((0.0 or 0      ) === false);
static_assert((0.0 or 1.2    ) === true );
static_assert((0.0 or 0.0    ) === false);
static_assert((0.0 or ''     ) === false);
static_assert((0.0 or "qwe"  ) === true );
static_assert((0.0 or []     ) === false);
static_assert((0.0 or [1,2,3]) === true );

static_assert((null    or 0.0) === false);
static_assert((true    or 0.0) === true );
static_assert((false   or 0.0) === false);
static_assert((123     or 0.0) === true );
static_assert((0       or 0.0) === false);
static_assert((1.2     or 0.0) === true );
static_assert((0.0     or 0.0) === false);
static_assert((''      or 0.0) === false);
static_assert(("qwe"   or 0.0) === true );
static_assert(([]      or 0.0) === false);
static_assert(([1,2,3] or 0.0) === true );


//----- string with types -------------------------------------------
static_assert(("qwe" or null   ) === true);
static_assert(("qwe" or true   ) === true);
static_assert(("qwe" or false  ) === true);
static_assert(("qwe" or 123    ) === true);
static_assert(("qwe" or 0      ) === true);
static_assert(("qwe" or 1.2    ) === true);
static_assert(("qwe" or 0.0    ) === true);
static_assert(("qwe" or ''     ) === true);
static_assert(("qwe" or "qwe"  ) === true);
static_assert(("qwe" or []     ) === true);
static_assert(("qwe" or [1,2,3]) === true);
static_assert(("qwe" or $a     ) === true);

static_assert((null    or "qwe") === true);
static_assert((true    or "qwe") === true);
static_assert((false   or "qwe") === true);
static_assert((123     or "qwe") === true);
static_assert((0       or "qwe") === true);
static_assert((1.2     or "qwe") === true);
static_assert((0.0     or "qwe") === true);
static_assert((''      or "qwe") === true);
static_assert(("qwe"   or "qwe") === true);
static_assert(([]      or "qwe") === true);
static_assert(([1,2,3] or "qwe") === true);

static_assert(('' or null   ) === false);
static_assert(('' or true   ) === true );
static_assert(('' or false  ) === false);
static_assert(('' or 123    ) === true );
static_assert(('' or 0      ) === false);
static_assert(('' or 1.2    ) === true );
static_assert(('' or 0.0    ) === false);
static_assert(('' or ''     ) === false);
static_assert(('' or "qwe"  ) === true );
static_assert(('' or []     ) === false);
static_assert(('' or [1,2,3]) === true );

static_assert((null    or '') === false);
static_assert((true    or '') === true );
static_assert((false   or '') === false);
static_assert((123     or '') === true );
static_assert((0       or '') === false);
static_assert((1.2     or '') === true );
static_assert((0.0     or '') === false);
static_assert((''      or '') === false);
static_assert(("qwe"   or '') === true );
static_assert(([]      or '') === false);
static_assert(([1,2,3] or '') === true );

static_assert(("0" or null   ) === false);
static_assert(("0" or true   ) === true );
static_assert(("0" or false  ) === false);
static_assert(("0" or 123    ) === true );
static_assert(("0" or 0      ) === false);
static_assert(("0" or 1.2    ) === true );
static_assert(("0" or 0.0    ) === false);
static_assert(("0" or ''     ) === false);
static_assert(("0" or "qwe"  ) === true );
static_assert(("0" or []     ) === false);
static_assert(("0" or [1,2,3]) === true );

static_assert((null    or "0") === false);
static_assert((true    or "0") === true );
static_assert((false   or "0") === false);
static_assert((123     or "0") === true );
static_assert((0       or "0") === false);
static_assert((1.2     or "0") === true );
static_assert((0.0     or "0") === false);
static_assert((''      or "0") === false);
static_assert(("qwe"   or "0") === true );
static_assert(([]      or "0") === false);
static_assert(([1,2,3] or "0") === true );


//----- array with types --------------------------------------------
static_assert(([1, 2, 3] or null   ) === true);
static_assert(([1, 2, 3] or true   ) === true);
static_assert(([1, 2, 3] or false  ) === true);
static_assert(([1, 2, 3] or 123    ) === true);
static_assert(([1, 2, 3] or 0      ) === true);
static_assert(([1, 2, 3] or 1.2    ) === true);
static_assert(([1, 2, 3] or 0.0    ) === true);
static_assert(([1, 2, 3] or ''     ) === true);
static_assert(([1, 2, 3] or "qwe"  ) === true);
static_assert(([1, 2, 3] or []     ) === true);
static_assert(([1, 2, 3] or [1,2,3]) === true);
static_assert(([1, 2, 3] or $a     ) === true);

static_assert((null    or [1, 2, 3]) === true);
static_assert((true    or [1, 2, 3]) === true);
static_assert((false   or [1, 2, 3]) === true);
static_assert((123     or [1, 2, 3]) === true);
static_assert((0       or [1, 2, 3]) === true);
static_assert((1.2     or [1, 2, 3]) === true);
static_assert((0.0     or [1, 2, 3]) === true);
static_assert((''      or [1, 2, 3]) === true);
static_assert(("qwe"   or [1, 2, 3]) === true);
static_assert(([]      or [1, 2, 3]) === true);
static_assert(([1,2,3] or [1, 2, 3]) === true);

static_assert(([] or null   ) === false);
static_assert(([] or true   ) === true );
static_assert(([] or false  ) === false);
static_assert(([] or 123    ) === true );
static_assert(([] or 0      ) === false);
static_assert(([] or 1.2    ) === true );
static_assert(([] or 0.0    ) === false);
static_assert(([] or ''     ) === false);
static_assert(([] or "qwe"  ) === true );
static_assert(([] or []     ) === false);
static_assert(([] or [1,2,3]) === true );


static_assert((null    or []) === false);
static_assert((true    or []) === true );
static_assert((false   or []) === false);
static_assert((123     or []) === true );
static_assert((0       or []) === false);
static_assert((1.2     or []) === true );
static_assert((0.0     or []) === false);
static_assert((''      or []) === false);
static_assert(("qwe"   or []) === true );
static_assert(([]      or []) === false);
static_assert(([1,2,3] or []) === true );

