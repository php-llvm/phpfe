// RUN: %clang_php %s -verify
<?php 


//-------------------------------------------------------------------
// unary '-'
//-------------------------------------------------------------------
static_assert(-[] == []);  // expected-error{{operation cannot be applied to arrays}}


//-------------------------------------------------------------------
// unary '+'
//-------------------------------------------------------------------
static_assert(+[] == []);  // expected-error{{operation cannot be applied to arrays}}


//-------------------------------------------------------------------
// unary '~'
//-------------------------------------------------------------------
echo min(~null, 123);   // expected-warning{{'null' implicitly converted to 'zero'}}
echo min(~true, 123);   // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
echo min(~[], 123);     // expected-error{{operation cannot be applied to arrays}}
//TODO: add string test


//-------------------------------------------------------------------
// unary '++', '--'
//-------------------------------------------------------------------
function incdec_bool(bool $a) {
  $a++; // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
  ++$a; // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
  $a--; // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
  --$a; // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
}
function incdec_array(array $a) {
  $a++; // expected-error{{operation cannot be applied to arrays}}
  ++$a; // expected-error{{operation cannot be applied to arrays}}
  $a--; // expected-error{{operation cannot be applied to arrays}}
  --$a; // expected-error{{operation cannot be applied to arrays}}
}

?>
