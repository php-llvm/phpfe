// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// logical 'and'
//-------------------------------------------------------------------

//----- null with types ---------------------------------------------
static_assert((null and null   ) === false);
static_assert((null and true   ) === false);
static_assert((null and false  ) === false);
static_assert((null and 123    ) === false);
static_assert((null and 0      ) === false);
static_assert((null and 1.2    ) === false);
static_assert((null and 0.0    ) === false);
static_assert((null and ''     ) === false);
static_assert((null and "qwe"  ) === false);
static_assert((null and []     ) === false);
static_assert((null and [1,2,3]) === false);
static_assert((null and $a     ) === false);

static_assert((null    and null) === false);
static_assert((true    and null) === false);
static_assert((false   and null) === false);
static_assert((123     and null) === false);
static_assert((0       and null) === false);
static_assert((1.2     and null) === false);
static_assert((0.0     and null) === false);
static_assert((''      and null) === false);
static_assert(("qwe"   and null) === false);
static_assert(([]      and null) === false);
static_assert(([1,2,3] and null) === false);


//----- false with types --------------------------------------------
static_assert((false and null   ) === false);
static_assert((false and true   ) === false);
static_assert((false and false  ) === false);
static_assert((false and 123    ) === false);
static_assert((false and 0      ) === false);
static_assert((false and 1.2    ) === false);
static_assert((false and 0.0    ) === false);
static_assert((false and ''     ) === false);
static_assert((false and "qwe"  ) === false);
static_assert((false and []     ) === false);
static_assert((false and [1,2,3]) === false);
static_assert((false and $a     ) === false);

static_assert((null    and false) === false);
static_assert((true    and false) === false);
static_assert((false   and false) === false);
static_assert((123     and false) === false);
static_assert((0       and false) === false);
static_assert((1.2     and false) === false);
static_assert((0.0     and false) === false);
static_assert((''      and false) === false);
static_assert(("qwe"   and false) === false);
static_assert(([]      and false) === false);
static_assert(([1,2,3] and false) === false);


//----- true with types ---------------------------------------------
static_assert((true and null   ) === false);
static_assert((true and true   ) === true );
static_assert((true and false  ) === false);
static_assert((true and 123    ) === true );
static_assert((true and 0      ) === false);
static_assert((true and 1.2    ) === true );
static_assert((true and 0.0    ) === false);
static_assert((true and ''     ) === false);
static_assert((true and "qwe"  ) === true );
static_assert((true and []     ) === false);
static_assert((true and [1,2,3]) === true );

static_assert((null    and true) === false);
static_assert((true    and true) === true );
static_assert((false   and true) === false);
static_assert((123     and true) === true );
static_assert((0       and true) === false);
static_assert((1.2     and true) === true );
static_assert((0.0     and true) === false);
static_assert((''      and true) === false);
static_assert(("qwe"   and true) === true );
static_assert(([]      and true) === false);
static_assert(([1,2,3] and true) === true );


//----- integer with types ------------------------------------------
static_assert((123 and null   ) === false);
static_assert((123 and true   ) === true );
static_assert((123 and false  ) === false);
static_assert((123 and 123    ) === true );
static_assert((123 and 0      ) === false);
static_assert((123 and 1.2    ) === true );
static_assert((123 and 0.0    ) === false);
static_assert((123 and ''     ) === false);
static_assert((123 and "qwe"  ) === true );
static_assert((123 and []     ) === false);
static_assert((123 and [1,2,3]) === true );

static_assert((null    and 123) === false);
static_assert((true    and 123) === true );
static_assert((false   and 123) === false);
static_assert((123     and 123) === true );
static_assert((0       and 123) === false);
static_assert((1.2     and 123) === true );
static_assert((0.0     and 123) === false);
static_assert((''      and 123) === false);
static_assert(("qwe"   and 123) === true );
static_assert(([]      and 123) === false);
static_assert(([1,2,3] and 123) === true );

static_assert((0 and null   ) === false);
static_assert((0 and true   ) === false);
static_assert((0 and false  ) === false);
static_assert((0 and 123    ) === false);
static_assert((0 and 0      ) === false);
static_assert((0 and 1.2    ) === false);
static_assert((0 and 0.0    ) === false);
static_assert((0 and ''     ) === false);
static_assert((0 and "qwe"  ) === false);
static_assert((0 and []     ) === false);
static_assert((0 and [1,2,3]) === false);
static_assert((0 and $a     ) === false);

static_assert((null    and 0) === false);
static_assert((true    and 0) === false);
static_assert((false   and 0) === false);
static_assert((123     and 0) === false);
static_assert((0       and 0) === false);
static_assert((1.2     and 0) === false);
static_assert((0.0     and 0) === false);
static_assert((''      and 0) === false);
static_assert(("qwe"   and 0) === false);
static_assert(([]      and 0) === false);
static_assert(([1,2,3] and 0) === false);


//----- double with types -------------------------------------------
static_assert((1.3 and null   ) === false);
static_assert((1.3 and true   ) === true );
static_assert((1.3 and false  ) === false);
static_assert((1.3 and 123    ) === true );
static_assert((1.3 and 0      ) === false);
static_assert((1.3 and 1.2    ) === true );
static_assert((1.3 and 0.0    ) === false);
static_assert((1.3 and ''     ) === false);
static_assert((1.3 and "qwe"  ) === true );
static_assert((1.3 and []     ) === false);
static_assert((1.3 and [1,2,3]) === true );

static_assert((null    and 1.3) === false);
static_assert((true    and 1.3) === true );
static_assert((false   and 1.3) === false);
static_assert((123     and 1.3) === true );
static_assert((0       and 1.3) === false);
static_assert((1.2     and 1.3) === true );
static_assert((0.0     and 1.3) === false);
static_assert((''      and 1.3) === false);
static_assert(("qwe"   and 1.3) === true );
static_assert(([]      and 1.3) === false);
static_assert(([1,2,3] and 1.3) === true );

static_assert((0.0 and null   ) === false);
static_assert((0.0 and true   ) === false);
static_assert((0.0 and false  ) === false);
static_assert((0.0 and 123    ) === false);
static_assert((0.0 and 0      ) === false);
static_assert((0.0 and 1.2    ) === false);
static_assert((0.0 and 0.0    ) === false);
static_assert((0.0 and ''     ) === false);
static_assert((0.0 and "qwe"  ) === false);
static_assert((0.0 and []     ) === false);
static_assert((0.0 and [1,2,3]) === false);
static_assert((0.0 and $a     ) === false);

static_assert((null    and 0.0) === false);
static_assert((true    and 0.0) === false);
static_assert((false   and 0.0) === false);
static_assert((123     and 0.0) === false);
static_assert((0       and 0.0) === false);
static_assert((1.2     and 0.0) === false);
static_assert((0.0     and 0.0) === false);
static_assert((''      and 0.0) === false);
static_assert(("qwe"   and 0.0) === false);
static_assert(([]      and 0.0) === false);
static_assert(([1,2,3] and 0.0) === false);


//----- string with types -------------------------------------------
static_assert(("qwe" and null   ) === false);
static_assert(("qwe" and true   ) === true );
static_assert(("qwe" and false  ) === false);
static_assert(("qwe" and 123    ) === true );
static_assert(("qwe" and 0      ) === false);
static_assert(("qwe" and 1.2    ) === true );
static_assert(("qwe" and 0.0    ) === false);
static_assert(("qwe" and ''     ) === false);
static_assert(("qwe" and "qwe"  ) === true );
static_assert(("qwe" and []     ) === false);
static_assert(("qwe" and [1,2,3]) === true );

static_assert((null    and "qwe") === false);
static_assert((true    and "qwe") === true );
static_assert((false   and "qwe") === false);
static_assert((123     and "qwe") === true );
static_assert((0       and "qwe") === false);
static_assert((1.2     and "qwe") === true );
static_assert((0.0     and "qwe") === false);
static_assert((''      and "qwe") === false);
static_assert(("qwe"   and "qwe") === true );
static_assert(([]      and "qwe") === false);
static_assert(([1,2,3] and "qwe") === true );

static_assert(('' and null   ) === false);
static_assert(('' and true   ) === false);
static_assert(('' and false  ) === false);
static_assert(('' and 123    ) === false);
static_assert(('' and 0      ) === false);
static_assert(('' and 1.2    ) === false);
static_assert(('' and 0.0    ) === false);
static_assert(('' and ''     ) === false);
static_assert(('' and "qwe"  ) === false);
static_assert(('' and []     ) === false);
static_assert(('' and [1,2,3]) === false);
static_assert(('' and $a     ) === false);

static_assert((null    and '') === false);
static_assert((true    and '') === false);
static_assert((false   and '') === false);
static_assert((123     and '') === false);
static_assert((0       and '') === false);
static_assert((1.2     and '') === false);
static_assert((0.0     and '') === false);
static_assert((''      and '') === false);
static_assert(("qwe"   and '') === false);
static_assert(([]      and '') === false);
static_assert(([1,2,3] and '') === false);

static_assert(("0" and null   ) === false);
static_assert(("0" and true   ) === false);
static_assert(("0" and false  ) === false);
static_assert(("0" and 123    ) === false);
static_assert(("0" and 0      ) === false);
static_assert(("0" and 1.2    ) === false);
static_assert(("0" and 0.0    ) === false);
static_assert(("0" and ''     ) === false);
static_assert(("0" and "qwe"  ) === false);
static_assert(("0" and []     ) === false);
static_assert(("0" and [1,2,3]) === false);
static_assert(("0" and $a     ) === false);

static_assert((null    and "0") === false);
static_assert((true    and "0") === false);
static_assert((false   and "0") === false);
static_assert((123     and "0") === false);
static_assert((0       and "0") === false);
static_assert((1.2     and "0") === false);
static_assert((0.0     and "0") === false);
static_assert((''      and "0") === false);
static_assert(("qwe"   and "0") === false);
static_assert(([]      and "0") === false);
static_assert(([1,2,3] and "0") === false);



//----- array with types --------------------------------------------
static_assert(([1, 2, 3] and null   ) === false);
static_assert(([1, 2, 3] and true   ) === true );
static_assert(([1, 2, 3] and false  ) === false);
static_assert(([1, 2, 3] and 123    ) === true );
static_assert(([1, 2, 3] and 0      ) === false);
static_assert(([1, 2, 3] and 1.2    ) === true );
static_assert(([1, 2, 3] and 0.0    ) === false);
static_assert(([1, 2, 3] and ''     ) === false);
static_assert(([1, 2, 3] and "qwe"  ) === true );
static_assert(([1, 2, 3] and []     ) === false);
static_assert(([1, 2, 3] and [1,2,3]) === true );

static_assert((null    and [1, 2, 3]) === false);
static_assert((true    and [1, 2, 3]) === true );
static_assert((false   and [1, 2, 3]) === false);
static_assert((123     and [1, 2, 3]) === true );
static_assert((0       and [1, 2, 3]) === false);
static_assert((1.2     and [1, 2, 3]) === true );
static_assert((0.0     and [1, 2, 3]) === false);
static_assert((''      and [1, 2, 3]) === false);
static_assert(("qwe"   and [1, 2, 3]) === true );
static_assert(([]      and [1, 2, 3]) === false);
static_assert(([1,2,3] and [1, 2, 3]) === true );

static_assert(([] and null   ) === false);
static_assert(([] and true   ) === false);
static_assert(([] and false  ) === false);
static_assert(([] and 123    ) === false);
static_assert(([] and 0      ) === false);
static_assert(([] and 1.2    ) === false);
static_assert(([] and 0.0    ) === false);
static_assert(([] and ''     ) === false);
static_assert(([] and "qwe"  ) === false);
static_assert(([] and []     ) === false);
static_assert(([] and [1,2,3]) === false);
static_assert(([] and $a     ) === false);

static_assert((null    and []) === false);
static_assert((true    and []) === false);
static_assert((false   and []) === false);
static_assert((123     and []) === false);
static_assert((0       and []) === false);
static_assert((1.2     and []) === false);
static_assert((0.0     and []) === false);
static_assert((''      and []) === false);
static_assert(("qwe"   and []) === false);
static_assert(([]      and []) === false);
static_assert(([1,2,3] and []) === false);


