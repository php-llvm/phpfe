// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// logical '&&'
//-------------------------------------------------------------------

//----- null with types ---------------------------------------------
static_assert((null && null   ) === false);
static_assert((null && true   ) === false);
static_assert((null && false  ) === false);
static_assert((null && 123    ) === false);
static_assert((null && 0      ) === false);
static_assert((null && 1.2    ) === false);
static_assert((null && 0.0    ) === false);
static_assert((null && ''     ) === false);
static_assert((null && "qwe"  ) === false);
static_assert((null && []     ) === false);
static_assert((null && [1,2,3]) === false);
static_assert((null && $a     ) === false);

static_assert((null    && null) === false);
static_assert((true    && null) === false);
static_assert((false   && null) === false);
static_assert((123     && null) === false);
static_assert((0       && null) === false);
static_assert((1.2     && null) === false);
static_assert((0.0     && null) === false);
static_assert((''      && null) === false);
static_assert(("qwe"   && null) === false);
static_assert(([]      && null) === false);
static_assert(([1,2,3] && null) === false);


//----- false with types --------------------------------------------
static_assert((false && null   ) === false);
static_assert((false && true   ) === false);
static_assert((false && false  ) === false);
static_assert((false && 123    ) === false);
static_assert((false && 0      ) === false);
static_assert((false && 1.2    ) === false);
static_assert((false && 0.0    ) === false);
static_assert((false && ''     ) === false);
static_assert((false && "qwe"  ) === false);
static_assert((false && []     ) === false);
static_assert((false && [1,2,3]) === false);
static_assert((false && $a     ) === false);

static_assert((null    && false) === false);
static_assert((true    && false) === false);
static_assert((false   && false) === false);
static_assert((123     && false) === false);
static_assert((0       && false) === false);
static_assert((1.2     && false) === false);
static_assert((0.0     && false) === false);
static_assert((''      && false) === false);
static_assert(("qwe"   && false) === false);
static_assert(([]      && false) === false);
static_assert(([1,2,3] && false) === false);


//----- true with types ---------------------------------------------
static_assert((true && null   ) === false);
static_assert((true && true   ) === true );
static_assert((true && false  ) === false);
static_assert((true && 123    ) === true );
static_assert((true && 0      ) === false);
static_assert((true && 1.2    ) === true );
static_assert((true && 0.0    ) === false);
static_assert((true && ''     ) === false);
static_assert((true && "qwe"  ) === true );
static_assert((true && []     ) === false);
static_assert((true && [1,2,3]) === true );

static_assert((null    && true) === false);
static_assert((true    && true) === true );
static_assert((false   && true) === false);
static_assert((123     && true) === true );
static_assert((0       && true) === false);
static_assert((1.2     && true) === true );
static_assert((0.0     && true) === false);
static_assert((''      && true) === false);
static_assert(("qwe"   && true) === true );
static_assert(([]      && true) === false);
static_assert(([1,2,3] && true) === true );


//----- integer with types ------------------------------------------
static_assert((123 && null   ) === false);
static_assert((123 && true   ) === true );
static_assert((123 && false  ) === false);
static_assert((123 && 123    ) === true );
static_assert((123 && 0      ) === false);
static_assert((123 && 1.2    ) === true );
static_assert((123 && 0.0    ) === false);
static_assert((123 && ''     ) === false);
static_assert((123 && "qwe"  ) === true );
static_assert((123 && []     ) === false);
static_assert((123 && [1,2,3]) === true );

static_assert((null    && 123) === false);
static_assert((true    && 123) === true );
static_assert((false   && 123) === false);
static_assert((123     && 123) === true );
static_assert((0       && 123) === false);
static_assert((1.2     && 123) === true );
static_assert((0.0     && 123) === false);
static_assert((''      && 123) === false);
static_assert(("qwe"   && 123) === true );
static_assert(([]      && 123) === false);
static_assert(([1,2,3] && 123) === true );

static_assert((0 && null   ) === false);
static_assert((0 && true   ) === false);
static_assert((0 && false  ) === false);
static_assert((0 && 123    ) === false);
static_assert((0 && 0      ) === false);
static_assert((0 && 1.2    ) === false);
static_assert((0 && 0.0    ) === false);
static_assert((0 && ''     ) === false);
static_assert((0 && "qwe"  ) === false);
static_assert((0 && []     ) === false);
static_assert((0 && [1,2,3]) === false);
static_assert((0 && $a     ) === false);

static_assert((null    && 0) === false);
static_assert((true    && 0) === false);
static_assert((false   && 0) === false);
static_assert((123     && 0) === false);
static_assert((0       && 0) === false);
static_assert((1.2     && 0) === false);
static_assert((0.0     && 0) === false);
static_assert((''      && 0) === false);
static_assert(("qwe"   && 0) === false);
static_assert(([]      && 0) === false);
static_assert(([1,2,3] && 0) === false);


//----- double with types -------------------------------------------
static_assert((1.3 && null   ) === false);
static_assert((1.3 && true   ) === true );
static_assert((1.3 && false  ) === false);
static_assert((1.3 && 123    ) === true );
static_assert((1.3 && 0      ) === false);
static_assert((1.3 && 1.2    ) === true );
static_assert((1.3 && 0.0    ) === false);
static_assert((1.3 && ''     ) === false);
static_assert((1.3 && "qwe"  ) === true );
static_assert((1.3 && []     ) === false);
static_assert((1.3 && [1,2,3]) === true );

static_assert((null    && 1.3) === false);
static_assert((true    && 1.3) === true );
static_assert((false   && 1.3) === false);
static_assert((123     && 1.3) === true );
static_assert((0       && 1.3) === false);
static_assert((1.2     && 1.3) === true );
static_assert((0.0     && 1.3) === false);
static_assert((''      && 1.3) === false);
static_assert(("qwe"   && 1.3) === true );
static_assert(([]      && 1.3) === false);
static_assert(([1,2,3] && 1.3) === true );

static_assert((0.0 && null   ) === false);
static_assert((0.0 && true   ) === false);
static_assert((0.0 && false  ) === false);
static_assert((0.0 && 123    ) === false);
static_assert((0.0 && 0      ) === false);
static_assert((0.0 && 1.2    ) === false);
static_assert((0.0 && 0.0    ) === false);
static_assert((0.0 && ''     ) === false);
static_assert((0.0 && "qwe"  ) === false);
static_assert((0.0 && []     ) === false);
static_assert((0.0 && [1,2,3]) === false);
static_assert((0.0 && $a     ) === false);

static_assert((null    && 0.0) === false);
static_assert((true    && 0.0) === false);
static_assert((false   && 0.0) === false);
static_assert((123     && 0.0) === false);
static_assert((0       && 0.0) === false);
static_assert((1.2     && 0.0) === false);
static_assert((0.0     && 0.0) === false);
static_assert((''      && 0.0) === false);
static_assert(("qwe"   && 0.0) === false);
static_assert(([]      && 0.0) === false);
static_assert(([1,2,3] && 0.0) === false);


//----- string with types -------------------------------------------
static_assert(("qwe" && null   ) === false);
static_assert(("qwe" && true   ) === true );
static_assert(("qwe" && false  ) === false);
static_assert(("qwe" && 123    ) === true );
static_assert(("qwe" && 0      ) === false);
static_assert(("qwe" && 1.2    ) === true );
static_assert(("qwe" && 0.0    ) === false);
static_assert(("qwe" && ''     ) === false);
static_assert(("qwe" && "qwe"  ) === true );
static_assert(("qwe" && []     ) === false);
static_assert(("qwe" && [1,2,3]) === true );

static_assert((null    && "qwe") === false);
static_assert((true    && "qwe") === true );
static_assert((false   && "qwe") === false);
static_assert((123     && "qwe") === true );
static_assert((0       && "qwe") === false);
static_assert((1.2     && "qwe") === true );
static_assert((0.0     && "qwe") === false);
static_assert((''      && "qwe") === false);
static_assert(("qwe"   && "qwe") === true );
static_assert(([]      && "qwe") === false);
static_assert(([1,2,3] && "qwe") === true );

static_assert(('' && null   ) === false);
static_assert(('' && true   ) === false);
static_assert(('' && false  ) === false);
static_assert(('' && 123    ) === false);
static_assert(('' && 0      ) === false);
static_assert(('' && 1.2    ) === false);
static_assert(('' && 0.0    ) === false);
static_assert(('' && ''     ) === false);
static_assert(('' && "qwe"  ) === false);
static_assert(('' && []     ) === false);
static_assert(('' && [1,2,3]) === false);
static_assert(('' && $a     ) === false);

static_assert((null    && '') === false);
static_assert((true    && '') === false);
static_assert((false   && '') === false);
static_assert((123     && '') === false);
static_assert((0       && '') === false);
static_assert((1.2     && '') === false);
static_assert((0.0     && '') === false);
static_assert((''      && '') === false);
static_assert(("qwe"   && '') === false);
static_assert(([]      && '') === false);
static_assert(([1,2,3] && '') === false);

static_assert(("0" && null   ) === false);
static_assert(("0" && true   ) === false);
static_assert(("0" && false  ) === false);
static_assert(("0" && 123    ) === false);
static_assert(("0" && 0      ) === false);
static_assert(("0" && 1.2    ) === false);
static_assert(("0" && 0.0    ) === false);
static_assert(("0" && ''     ) === false);
static_assert(("0" && "qwe"  ) === false);
static_assert(("0" && []     ) === false);
static_assert(("0" && [1,2,3]) === false);
static_assert(("0" && $a     ) === false);

static_assert((null    && "0") === false);
static_assert((true    && "0") === false);
static_assert((false   && "0") === false);
static_assert((123     && "0") === false);
static_assert((0       && "0") === false);
static_assert((1.2     && "0") === false);
static_assert((0.0     && "0") === false);
static_assert((''      && "0") === false);
static_assert(("qwe"   && "0") === false);
static_assert(([]      && "0") === false);
static_assert(([1,2,3] && "0") === false);



//----- array with types --------------------------------------------
static_assert(([1, 2, 3] && null   ) === false);
static_assert(([1, 2, 3] && true   ) === true );
static_assert(([1, 2, 3] && false  ) === false);
static_assert(([1, 2, 3] && 123    ) === true );
static_assert(([1, 2, 3] && 0      ) === false);
static_assert(([1, 2, 3] && 1.2    ) === true );
static_assert(([1, 2, 3] && 0.0    ) === false);
static_assert(([1, 2, 3] && ''     ) === false);
static_assert(([1, 2, 3] && "qwe"  ) === true );
static_assert(([1, 2, 3] && []     ) === false);
static_assert(([1, 2, 3] && [1,2,3]) === true );

static_assert((null    && [1, 2, 3]) === false);
static_assert((true    && [1, 2, 3]) === true );
static_assert((false   && [1, 2, 3]) === false);
static_assert((123     && [1, 2, 3]) === true );
static_assert((0       && [1, 2, 3]) === false);
static_assert((1.2     && [1, 2, 3]) === true );
static_assert((0.0     && [1, 2, 3]) === false);
static_assert((''      && [1, 2, 3]) === false);
static_assert(("qwe"   && [1, 2, 3]) === true );
static_assert(([]      && [1, 2, 3]) === false);
static_assert(([1,2,3] && [1, 2, 3]) === true );

static_assert(([] && null   ) === false);
static_assert(([] && true   ) === false);
static_assert(([] && false  ) === false);
static_assert(([] && 123    ) === false);
static_assert(([] && 0      ) === false);
static_assert(([] && 1.2    ) === false);
static_assert(([] && 0.0    ) === false);
static_assert(([] && ''     ) === false);
static_assert(([] && "qwe"  ) === false);
static_assert(([] && []     ) === false);
static_assert(([] && [1,2,3]) === false);
static_assert(([] && $a     ) === false);

static_assert((null    && []) === false);
static_assert((true    && []) === false);
static_assert((false   && []) === false);
static_assert((123     && []) === false);
static_assert((0       && []) === false);
static_assert((1.2     && []) === false);
static_assert((0.0     && []) === false);
static_assert((''      && []) === false);
static_assert(("qwe"   && []) === false);
static_assert(([]      && []) === false);
static_assert(([1,2,3] && []) === false);


