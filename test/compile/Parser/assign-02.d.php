// RUN: %clang_php %s -verify
<?php

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_bool_01(bool $a) {
  $a = [1,2,3];            // expected-warning{{implicit conversion from 'array' to 'boolean'}}
  $a = get_array();        // expected-warning{{implicit conversion from 'array' to 'boolean'}}
  $a = get_array_ref();    // expected-warning{{implicit conversion from 'array' to 'boolean'}}
}
check_assign_to_bool_01(true);


function check_assign_to_bool_02(bool &$a) {
  $a = [1,2,3];            // expected-warning{{implicit conversion from 'array' to 'boolean'}}
  $a = get_array();        // expected-warning{{implicit conversion from 'array' to 'boolean'}}
  $a = get_array_ref();    // expected-warning{{implicit conversion from 'array' to 'boolean'}}
}
check_assign_to_bool_02($a);


function check_assign_to_int_01(int $a) {
  $a = [1,2,3];            // expected-error{{cannot implicitly convert array to 'integer'}}
  $a = get_array();        // expected-error{{cannot implicitly convert array to 'integer'}}
  $a = get_array_ref();    // expected-error{{cannot implicitly convert array to 'integer'}}
}
check_assign_to_int_01(123);


function check_assign_to_int_02(int &$a) {
  $a = [1,2,3];            // expected-error{{cannot implicitly convert array to 'integer'}}
  $a = get_array();        // expected-error{{cannot implicitly convert array to 'integer'}}
  $a = get_array_ref();    // expected-error{{cannot implicitly convert array to 'integer'}}
}
check_assign_to_int_02($a);


function check_assign_to_double_01(float $a) {
  $a = [1,2,3];            // expected-error{{cannot implicitly convert array to 'double'}}
  $a = get_array();        // expected-error{{cannot implicitly convert array to 'double'}}
  $a = get_array_ref();    // expected-error{{cannot implicitly convert array to 'double'}}
}
check_assign_to_double_01(123.456);


function check_assign_to_double_02(float &$a) {
  $a = [1,2,3];            // expected-error{{cannot implicitly convert array to 'double'}}
  $a = get_array();        // expected-error{{cannot implicitly convert array to 'double'}}
  $a = get_array_ref();    // expected-error{{cannot implicitly convert array to 'double'}}
}
check_assign_to_double_02($a);


function check_assign_to_string_01(string $a) {
  $a = [1,2,3];            // expected-error{{cannot implicitly convert array to 'string'}}
  $a = get_array();        // expected-error{{cannot implicitly convert array to 'string'}}
  $a = get_array_ref();    // expected-error{{cannot implicitly convert array to 'string'}}
}
check_assign_to_string_01('qwe');


function check_assign_to_string_02(string &$a) {
  assert($a === 'qwe');

  $a = [1,2,3];            // expected-error{{cannot implicitly convert array to 'string'}}
  $a = get_array();        // expected-error{{cannot implicitly convert array to 'string'}}
  $a = get_array_ref();    // expected-error{{cannot implicitly convert array to 'string'}}
}
check_assign_to_string_02($a);


function check_assign_to_array_01(array $a) {
//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;  // expected-error{{cannot implicitly convert 'NULL' to array}}
  $a = false; // expected-error{{cannot implicitly convert 'boolean' to array}}
  $a = 123;   // expected-error{{cannot implicitly convert 'integer' to array}}
  $a = 12.34; // expected-error{{cannot implicitly convert 'double' to array}}
  $a = "qwe"; // expected-error{{cannot implicitly convert 'string' to array}}
  $a = [1,2,3];

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   // expected-error{{cannot implicitly convert 'NULL' to array}}
  $a = get_bool();   // expected-error{{cannot implicitly convert 'boolean' to array}}
  $a = get_int();    // expected-error{{cannot implicitly convert 'integer' to array}}
  $a = get_double(); // expected-error{{cannot implicitly convert 'double' to array}}
  $a = get_string(); // expected-error{{cannot implicitly convert 'string' to array}}
  $a = get_array();

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();     // expected-error{{cannot implicitly convert 'boolean' to array}}
  $a = get_int_ref();      // expected-error{{cannot implicitly convert 'integer' to array}}
  $a = get_double_ref();   // expected-error{{cannot implicitly convert 'double' to array}}
  $a = get_string_ref();   // expected-error{{cannot implicitly convert 'string' to array}}
  $a = get_array_ref();
}
check_assign_to_array_01([1,2,3]);


function check_assign_to_array_02(array &$a) {
//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;  // expected-error{{cannot implicitly convert 'NULL' to array}}
  $a = false; // expected-error{{cannot implicitly convert 'boolean' to array}}
  $a = 123;   // expected-error{{cannot implicitly convert 'integer' to array}}
  $a = 12.34; // expected-error{{cannot implicitly convert 'double' to array}}
  $a = "qwe"; // expected-error{{cannot implicitly convert 'string' to array}}
  $a = [1,2,3];

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   // expected-error{{cannot implicitly convert 'NULL' to array}}
  $a = get_bool();   // expected-error{{cannot implicitly convert 'boolean' to array}}
  $a = get_int();    // expected-error{{cannot implicitly convert 'integer' to array}}
  $a = get_double(); // expected-error{{cannot implicitly convert 'double' to array}}
  $a = get_string(); // expected-error{{cannot implicitly convert 'string' to array}}
  $a = get_array();

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();   // expected-error{{cannot implicitly convert 'boolean' to array}}
  $a = get_int_ref();    // expected-error{{cannot implicitly convert 'integer' to array}}
  $a = get_double_ref(); // expected-error{{cannot implicitly convert 'double' to array}}
  $a = get_string_ref(); // expected-error{{cannot implicitly convert 'string' to array}}
  $a = get_array_ref();
}
check_assign_to_array_02($a);

?>