<?php
// RUN: %clang_php -triple x86_64-unknown-linux-gnu -verify %s


//TODO:function static_assert($a) { assert($a); } //expected - warning{{static_assert has special treatment that cannot be redefined}}

//-------------------------------------------------------------------
// unary '-'
//-------------------------------------------------------------------


// Assignments to a variable.

$a = -null;                              // expected-warning{{'null' implicitly converted to 'zero'}}
$a = -true;                              // expected-warning{{implicit conversion from 'boolean' to 'integer'}}

$a = -123;
$a = -123456789012345678901234567890;    // expected-warning{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}
$a = -"123";                             // expected-warning{{implicit conversion from 'string' to 'integer'}}
$a = -"123q";                            // expected-warning{{implicit conversion from 'string' to 'integer'}}
$a = -"123456789012345678901234567890";  // expected-warning{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
                                         // expected-warning@-1{{implicit conversion from 'string' to 'double'}}

$a = -12.3;
$a = -12.3e1000;                         // expected-warning{{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}}
$a = -"12.3";                            // expected-warning{{implicit conversion from 'string' to 'double'}}
$a = -"12.3e10";                         // expected-warning{{implicit conversion from 'string' to 'double'}}
$a = -"12.3e1000";                       // expected-warning{{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}}
                                         // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
$a = -"12.3q";                           // expected-warning{{implicit conversion from 'string' to 'double'}}
$a = -"qqq";                             // expected-warning{{string does not contain a number}}


// -null
static_assert(-null === 0);              // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert(-(-null) === 0);           // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert(is_integer(-null));        // expected-warning{{'null' implicitly converted to 'zero'}}


// -boolean
static_assert(-true  === -1);            // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(-false ===  0);            // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(-(true)  === -1);          // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(-(false) ===  0);          // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(-(-true)  === 1);          // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(-(-false) ===  0);         // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(is_integer(-true));        // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert(is_integer(-false));       // expected-warning{{implicit conversion from 'boolean' to 'integer'}}


// -integer
static_assert(-1 === -1);
static_assert(-1 === -(1));
static_assert(-(-1) === 1);
static_assert("123" == 123);             // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert("-123" == -123);           // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(-"123" === -123);          // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(-"-123" === 123);          // expected-warning{{implicit conversion from 'string' to 'integer'}}
//
static_assert(is_integer(-"123"));       // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(is_integer(-"-123"));      // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(!is_integer("-123"));
static_assert(is_numeric("-123"));
//
static_assert(-"-123q" === 123);         // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(-"123q" === -123);         // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(is_integer(-"123q"));      // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(!is_integer("-123q"));
static_assert(!is_numeric("-123q"));
//
static_assert(is_integer(-9223372036854775807));
static_assert(is_integer(-9223372036854775808));
static_assert(is_float(-9223372036854775809));
                                         // expected-warning@-1{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}
static_assert(is_float(-(-9223372036854775808)));
static_assert(is_float(-123456789012345678901234567890));
                                         // expected-warning@-1{{literal cannot be represented as integer (min is -9223372036854775808), converted to float}}
//
static_assert(is_float(-"123456789012345678901234567890"));
                                         // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
                                         // expected-warning@-2{{literal cannot be represented as integer (max is 9223372036854775807), converted to float}}
static_assert(!is_float("-123456789012345678901234567890"));
static_assert(is_numeric("-123456789012345678901234567890"));


// -float
static_assert(-1.2 === -1.2);
static_assert(-(1.2) === -1.2);
static_assert(-"123.456" === -123.456);  // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert("-123.456" == -123.456);   // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(-"2e10000" < 0);           // expected-warning{{implicit conversion from 'string' to 'double'}}
                                         // expected-warning@-1{{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}}
static_assert("-2e10000" < 0);           // expected-warning{{implicit conversion from 'string' to 'double'}}
                                         // expected-warning@-1{{magnitude of floating-point constant too large for type 'double'; maximum is 1.7976931348623157E+308}}

static_assert(is_float(-12.3));
static_assert(is_float(-"12.3"));        // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(!is_float("-12.3"));
static_assert(is_numeric("-12.3"));
static_assert(is_float(-"12.3q"));       // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(is_float(-"12.3e10"));     // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(is_float(-"12.3e1000"));   // expected-warning{{implicit conversion from 'string' to 'double'}}
                                         // expected-warning@-1{{magnitude of floating-point constant too large for type 'double';}}


static_assert(-"qwe" === 0);             // expected-warning{{string does not contain a number}}
