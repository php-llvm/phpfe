// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '==='
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
static_assert((false === null   ) === false);
static_assert((true  === null   ) === false);
static_assert((false === false  ) === true );
static_assert((false === true   ) === false);
static_assert((true  === false  ) === false);
static_assert((true  === true   ) === true );
static_assert((true  === 123    ) === false);
static_assert((false === 123    ) === false);
static_assert((true  === 123.456) === false);
static_assert((false === 123.456) === false);
static_assert((true  === ''     ) === false);
static_assert((false === ''     ) === false);
static_assert((true  === "0"    ) === false);
static_assert((false === "0"    ) === false);
static_assert((true  === "qwe"  ) === false);
static_assert((false === "qwe"  ) === false);
static_assert((false === []     ) === false);
static_assert((true  === []     ) === false);
static_assert((false === [0]    ) === false);
static_assert((true  === [0]    ) === false);
static_assert((false === [123]  ) === false);
static_assert((true  === [123]  ) === false);

static_assert((null    === false) === false);
static_assert((null    === true ) === false);
static_assert((123     === true ) === false);
static_assert((123     === false) === false);
static_assert((123.456 === true ) === false);
static_assert((123.456 === false) === false);
static_assert((''      === true ) === false);
static_assert((''      === false) === false);
static_assert(("0"     === true ) === false);
static_assert(("0"     === false) === false);
static_assert(("qwe"   === true ) === false);
static_assert(("qwe"   === false) === false);
static_assert(([]      === false) === false);
static_assert(([]      === true ) === false);
static_assert(([0]     === false) === false);
static_assert(([0]     === true ) === false);
static_assert(([123]   === false) === false);
static_assert(([123]   === true ) === false);


//----- null with types ---------------------------------------------
static_assert((null === null   ) === true );
static_assert((null === ''     ) === false);
static_assert((null === "0"    ) === false);
static_assert((null === "qwe"  ) === false);
static_assert((null === "123"  ) === false);
static_assert((null === 123    ) === false);
static_assert((null === -123   ) === false);
static_assert((null === 123.123) === false);
static_assert((null === []     ) === false);
static_assert((null === [0]    ) === false);
static_assert((null === [123]  ) === false);

static_assert((null    === null) === true );
static_assert((''      === null) === false);
static_assert(("0"     === null) === false);
static_assert(("qwe"   === null) === false);
static_assert(("123"   === null) === false);
static_assert((123     === null) === false);
static_assert((-123    === null) === false);
static_assert((123.123 === null) === false);
static_assert(([]      === null) === false);
static_assert(([0]     === null) === false);
static_assert(([123]   === null) === false);


//----- integer with types ------------------------------------------
static_assert((123 === 123.4) === false);
static_assert((123 === 456  ) === false);
static_assert((456 === 123  ) === false);
static_assert((123 === 123  ) === true );
static_assert((123 === 123.0) === false);
static_assert((123 === ''   ) === false);
static_assert((123 === "qwe") === false);
static_assert((123 === "456") === false);
static_assert((123 === []   ) === false);
static_assert((123 === [0]  ) === false);
static_assert((123 === [123]) === false);

static_assert((123.4 === 123) === false);
static_assert((456   === 123) === false);
static_assert((123   === 456) === false);
static_assert((123   === 123) === true );
static_assert((123.0 === 123) === false);
static_assert((''    === 123) === false);
static_assert(("qwe" === 123) === false);
static_assert(("456" === 123) === false);
static_assert(([]    === 123) === false);
static_assert(([0]   === 123) === false);
static_assert(([123] === 123) === false);


//----- double with types -------------------------------------------
static_assert((1.2 === 1.2   ) === true );
static_assert((1.3 === 1.2   ) === false);
static_assert((1.2 === 1.3   ) === false);
static_assert((-.2 === ''    ) === false);
static_assert((1.2 === "qwe" ) === false);
static_assert((1.2 === "1e10") === false);
static_assert((1.2 === []    ) === false);
static_assert((1.2 === [0]   ) === false);
static_assert((1.2 === [123] ) === false);

static_assert((1.2    === 1.3) === false);
static_assert((1.3    === 1.2) === false);
static_assert((''     === -.2) === false);
static_assert(("qwe"  === 1.2) === false);
static_assert(("1e10" === 1.2) === false);
static_assert(([]     === 1.2) === false);
static_assert(([0]    === 1.2) === false);
static_assert(([123]  === 1.2) === false);


//----- string with types -------------------------------------------
static_assert(("123"   === "0456" ) === false);
static_assert(("qwe"   === "0456" ) === false);
static_assert(("0456"  === "qwe"  ) === false);
static_assert(("qwe"   === "qwe"  ) === true );
static_assert(("100"   === "1e2"  ) === false);
static_assert(("abcde" === "aacd" ) === false);
static_assert(("qwe"   === []     ) === false);
static_assert(("qwe"   === [0]    ) === false);
static_assert(("qwe"   === [123]  ) === false);
static_assert(("-123"    === ".123"   ) === false);
static_assert(("+123"    === ".123"   ) === false);
static_assert(("+123"    === ' .123'  ) === false);
static_assert((' +123'   === ".123"   ) === false);
static_assert(("+123asd" === ".123qwe") === false);

static_assert(("0456"  === "123"  ) === false);
static_assert(("0456"  === "qwe"  ) === false);
static_assert(("qwe"   === "0456" ) === false);
static_assert(("qwe"   === "qwe"  ) === true );
static_assert(("1e2"   === "100"  ) === false);
static_assert(("1e2"   === "100.0") === false);
static_assert(("aacd"  === "abcde") === false);
static_assert(("abcde" === "aacd" ) === false);
static_assert(("abcde" === "aacd" ) === false);
static_assert(([]      === "qwe"  ) === false);
static_assert(([0]     === "qwe"  ) === false);
static_assert(([123]   === "qwe"  ) === false);
static_assert((".123"    === "-123"   ) === false);
static_assert((".123"    === "+123"   ) === false);
static_assert((' .123'   === "+123"   ) === false);
static_assert((".123"    === ' +123'  ) === false);
static_assert((".123qwe" === "+123asd") === false);


//----- array with types --------------------------------------------
static_assert(([]      === []     )  === true );
static_assert(([1,2,3] === []     )  === false);
static_assert(([]      === [1,2,3])  === false);
static_assert(([3,4]   === [1,2,3])  === false);
static_assert(([1,2]   === [1,2]  )  === true );

static_assert((["a" => 1] === ["b" => 1])  === false);
static_assert((["b" => 1] === ["a" => 1])  === false);

static_assert(([3 => 1] === [7 => 1])  === false);
static_assert(([7 => 1] === [3 => 1])  === false);

static_assert((["a" => 1] === ["a" => 2])  === false);
static_assert((["a" => 2] === ["a" => 1])  === false);


