// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '!=='
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
static_assert((false !== null   ) === true );
static_assert((true  !== null   ) === true );
static_assert((false !== false  ) === false);
static_assert((false !== true   ) === true );
static_assert((true  !== false  ) === true );
static_assert((true  !== true   ) === false);
static_assert((true  !== 123    ) === true );
static_assert((false !== 123    ) === true );
static_assert((true  !== 123.456) === true );
static_assert((false !== 123.456) === true );
static_assert((true  !== ''     ) === true );
static_assert((false !== ''     ) === true );
static_assert((true  !== "0"    ) === true );
static_assert((false !== "0"    ) === true );
static_assert((true  !== "qwe"  ) === true );
static_assert((false !== "qwe"  ) === true );
static_assert((false !== []     ) === true );
static_assert((true  !== []     ) === true );
static_assert((false !== [0]    ) === true );
static_assert((true  !== [0]    ) === true );
static_assert((false !== [123]  ) === true );
static_assert((true  !== [123]  ) === true );

static_assert((null    !== false) === true);
static_assert((null    !== true ) === true);
static_assert((123     !== true ) === true);
static_assert((123     !== false) === true);
static_assert((123.456 !== true ) === true);
static_assert((123.456 !== false) === true);
static_assert((''      !== true ) === true);
static_assert((''      !== false) === true);
static_assert(("0"     !== true ) === true);
static_assert(("0"     !== false) === true);
static_assert(("qwe"   !== true ) === true);
static_assert(("qwe"   !== false) === true);
static_assert(([]      !== false) === true);
static_assert(([]      !== true ) === true);
static_assert(([0]     !== false) === true);
static_assert(([0]     !== true ) === true);
static_assert(([123]   !== false) === true);
static_assert(([123]   !== true ) === true);


//----- null with types ---------------------------------------------
static_assert((null !== null   ) === false);
static_assert((null !== ''     ) === true );
static_assert((null !== "0"    ) === true );
static_assert((null !== "qwe"  ) === true );
static_assert((null !== "123"  ) === true );
static_assert((null !== 123    ) === true );
static_assert((null !== -123   ) === true );
static_assert((null !== 123.123) === true );
static_assert((null !== []     ) === true );
static_assert((null !== [0]    ) === true );
static_assert((null !== [123]  ) === true );

static_assert((null    !== null) === false);
static_assert((''      !== null) === true );
static_assert(("0"     !== null) === true );
static_assert(("qwe"   !== null) === true );
static_assert(("123"   !== null) === true );
static_assert((123     !== null) === true );
static_assert((-123    !== null) === true );
static_assert((123.123 !== null) === true );
static_assert(([]      !== null) === true );
static_assert(([0]     !== null) === true );
static_assert(([123]   !== null) === true );


//----- integer with types ------------------------------------------
static_assert((123 !== 123.4) === true );
static_assert((123 !== 456  ) === true );
static_assert((456 !== 123  ) === true );
static_assert((123 !== 123  ) === false);
static_assert((123 !== 123.0) === true );
static_assert((123 !== ''   ) === true );
static_assert((123 !== "qwe") === true );
static_assert((123 !== "456") === true );
static_assert((123 !== []   ) === true );
static_assert((123 !== [0]  ) === true );
static_assert((123 !== [123]) === true );

static_assert((123.4 !== 123) === true );
static_assert((456   !== 123) === true );
static_assert((123   !== 456) === true );
static_assert((123   !== 123) === false);
static_assert((123.0 !== 123) === true );
static_assert((''    !== 123) === true );
static_assert(("qwe" !== 123) === true );
static_assert(("456" !== 123) === true );
static_assert(([]    !== 123) === true );
static_assert(([0]   !== 123) === true );
static_assert(([123] !== 123) === true );


//----- double with types -------------------------------------------
static_assert((1.2 !== 1.2   ) === false);
static_assert((1.3 !== 1.2   ) === true );
static_assert((1.2 !== 1.3   ) === true );
static_assert((-.2 !== ''    ) === true );
static_assert((1.2 !== "qwe" ) === true );
static_assert((1.2 !== "1e10") === true );
static_assert((1.2 !== []    ) === true );
static_assert((1.2 !== [0]   ) === true );
static_assert((1.2 !== [123] ) === true );

static_assert((1.2    !== 1.3) === true);
static_assert((1.3    !== 1.2) === true);
static_assert((''     !== -.2) === true);
static_assert(("qwe"  !== 1.2) === true);
static_assert(("1e10" !== 1.2) === true);
static_assert(([]     !== 1.2) === true);
static_assert(([0]    !== 1.2) === true);
static_assert(([123]  !== 1.2) === true);


//----- string with types -------------------------------------------
static_assert(("123"   !== "0456" ) === true );
static_assert(("qwe"   !== "0456" ) === true );
static_assert(("0456"  !== "qwe"  ) === true );
static_assert(("qwe"   !== "qwe"  ) === false);
static_assert(("100"   !== "1e2"  ) === true );
static_assert(("abcde" !== "aacd" ) === true );
static_assert(("qwe"   !== []     ) === true );
static_assert(("qwe"   !== [0]    ) === true );
static_assert(("qwe"   !== [123]  ) === true );
static_assert(("-123"    !== ".123"   ) === true );
static_assert(("+123"    !== ".123"   ) === true );
static_assert(("+123"    !== ' .123'  ) === true );
static_assert((' +123'   !== ".123"   ) === true );
static_assert(("+123asd" !== ".123qwe") === true );

static_assert(("0456"  !== "123"  ) === true );
static_assert(("0456"  !== "qwe"  ) === true );
static_assert(("qwe"   !== "0456" ) === true );
static_assert(("qwe"   !== "qwe"  ) === false);
static_assert(("1e2"   !== "100"  ) === true );
static_assert(("1e2"   !== "100.0") === true );
static_assert(("aacd"  !== "abcde") === true );
static_assert(("abcde" !== "aacd" ) === true );
static_assert(("abcde" !== "aacd" ) === true );
static_assert(([]      !== "qwe"  ) === true );
static_assert(([0]     !== "qwe"  ) === true );
static_assert(([123]   !== "qwe"  ) === true );
static_assert((".123"    !== "-123"   ) === true );
static_assert((".123"    !== "+123"   ) === true );
static_assert((' .123'   !== "+123"   ) === true );
static_assert((".123"    !== ' +123'  ) === true );
static_assert((".123qwe" !== "+123asd") === true );


//----- array with types --------------------------------------------
static_assert(([]      !== []     )  === false);
static_assert(([1,2,3] !== []     )  === true );
static_assert(([]      !== [1,2,3])  === true );
static_assert(([3,4]   !== [1,2,3])  === true );
static_assert(([1,2]   !== [1,2]  )  === false);

static_assert((["a" => 1] !== ["b" => 1])  === true);
static_assert((["b" => 1] !== ["a" => 1])  === true);

static_assert(([3 => 1] !== [7 => 1])  === true);
static_assert(([7 => 1] !== [3 => 1])  === true);

static_assert((["a" => 1] !== ["a" => 2])  === true);
static_assert((["a" => 2] !== ["a" => 1])  === true);
