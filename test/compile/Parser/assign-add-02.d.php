// RUN: %clang_php %s -verify
<?php

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $x = true;    return $x; }
function &get_int_ref    () : int    { $x = 123;     return $x; }
function &get_double_ref () : float  { $x = 12.34;   return $x; }
function &get_string_ref () : string { $x = 'qwe';   return $x; }
function &get_array_ref  () : array  { $x = [1,2,3]; return $x; }


function check_assign_to_bool_01(bool $a) {
  assert($a === false);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += [];                           // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();                       // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref();                       // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a += $b; assert($a === false);
  $b = true;    $a += $b; assert($a === true);
  $b = 123;     $a += $b; assert($a === true);
  $b = 12.45;   $a += $b; assert($a === true);
  $b = 'qwe';   $a += $b; assert($a === true);
  $b = [1,2,3]; $a += $b; assert($a === false);
}
check_assign_to_bool_01(false);


function check_assign_to_bool_02(bool &$a) {
  assert($a === false);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += [];                           // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();                       // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref();                       // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a += $b; assert($a === false);
  $b = true;    $a += $b; assert($a === true);
  $b = 123;     $a += $b; assert($a === true);
  $b = 12.45;   $a += $b; assert($a === true);
  $b = 'qwe';   $a += $b; assert($a === true);
  $b = [1,2,3]; $a += $b; assert($a === false); 
}
$a = false;
check_assign_to_bool_02($a);








function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a += 456;  assert($a === 579);
  $a += -12;  assert($a === 567);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += [1,2,3];                     // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();                       // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref(); assert($a === 0);    // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a += $b; assert($a === 0);
  $b = true;  $a += $b; assert($a === 975);
  $b = 123;   $a += $b; assert($a === 1098);
  $b = 12.45; $a += $b; assert($a === 1110);
  $b = 'qwe';   $a += $b; assert($a === 0);
  $b = [1,2,3]; $a += $b; assert($a === false);
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a += 456;  assert($a === 579);
  $a += -12;  assert($a === 567);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += [1,2,3];                     // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();                       // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref(); assert($a === 0);    // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a += $b; assert($a === 0);
  $b = true;  $a += $b; assert($a === 975);
  $b = 123;   $a += $b; assert($a === 1098);
  $b = 12.45; $a += $b; assert($a === 1110);
  $b = 'qwe';   $a += $b; assert($a === 0);
  $b = [1,2,3]; $a += $b; assert($a === false);
}
$a = 123;
check_assign_to_int_02($a);








function eq(double $a, double $b) : bool { return abs($a - $b) < 1e-10; }

function check_assign_to_double_01(float $a) {
  assert($a === 123.456);

  $a += 456.5;   assert($a === 579.956);
  $a += -12.258; assert($a === 567.698);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += [];                              // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();                          // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref();                       // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a += $b; assert($a === 0);
  $b = true;    $a += $b; assert(eq($a, 976.718));
  $b = 123;     $a += $b; assert(eq($a, 1099.718));
  $b = 12.45;   $a += $b; assert(eq($a, 1112.168));
  $b = 'qwe';   $a += $b; assert($a === 0);
  $b = [1,2,3]; $a += $b; assert($a === false);
}

check_assign_to_double_01(123.456);

function check_assign_to_double_02(float &$a) {
  assert($a === 123.456);

  $a += 456.5;   assert($a === 579.956);
  $a += -12.258; assert($a === 567.698);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += [];                              // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_array();                          // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_array_ref();                       // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a += $b; assert($a === 0);
  $b = true;    $a += $b; assert(eq($a, 976.718));
  $b = 123;     $a += $b; assert(eq($a, 1099.718));
  $b = 12.45;   $a += $b; assert(eq($a, 1112.168));
  $b = 'qwe';   $a += $b; assert($a === 0);
  $b = [1,2,3]; $a += $b; assert($a === false);
}
$a = 123.456;
check_assign_to_double_02($a);








function check_assign_to_string_02(string &$a) {
  assert($a === 'qwe');

  $a += get_array();  // expected-error{{in array addition both arguments must be arrays}}
  $a += get_array_ref();  // expected-error{{in array addition both arguments must be arrays}}

}
$a = 'qwe';
check_assign_to_string_02($a);






function check_assign_to_array_01(array $a) {
  assert($a === [1,2,3]);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;    // expected-error{{in array addition both arguments must be arrays}}
  $a += false;   // expected-error{{in array addition both arguments must be arrays}}
  $a += 123;     // expected-error{{in array addition both arguments must be arrays}}
  $a += 12.34;   // expected-error{{in array addition both arguments must be arrays}}
  $a += "qwe";   // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   // expected-error{{in array addition both arguments must be arrays}}
  $a += get_bool();   // expected-error{{in array addition both arguments must be arrays}}
  $a += get_int();    // expected-error{{in array addition both arguments must be arrays}}
  $a += get_double(); // expected-error{{in array addition both arguments must be arrays}}
  $a += get_string(); // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   // expected-error{{in array addition both arguments must be arrays}}
  $a += get_int_ref();    // expected-error{{in array addition both arguments must be arrays}}
  $a += get_double_ref(); // expected-error{{in array addition both arguments must be arrays}}
  $a += get_string_ref(); // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a += $b;
  $b = true;    $a += $b;
  $b = 123;     $a += $b;
  $b = 12.45;   $a += $b;
  $b = 'qwe';   $a += $b;
  $b = [1,2,3]; $a += $b;
}
check_assign_to_array_01([1,2,3]);

function check_assign_to_array_02(array &$a) {
  assert($a === [1,2,3]);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a += null;    // expected-error{{in array addition both arguments must be arrays}}
  $a += false;   // expected-error{{in array addition both arguments must be arrays}}
  $a += 123;     // expected-error{{in array addition both arguments must be arrays}}
  $a += 12.34;   // expected-error{{in array addition both arguments must be arrays}}
  $a += "qwe";   // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a += get_void();   // expected-error{{in array addition both arguments must be arrays}}
  $a += get_bool();   // expected-error{{in array addition both arguments must be arrays}}
  $a += get_int();    // expected-error{{in array addition both arguments must be arrays}}
  $a += get_double(); // expected-error{{in array addition both arguments must be arrays}}
  $a += get_string(); // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a += get_bool_ref();   // expected-error{{in array addition both arguments must be arrays}}
  $a += get_int_ref();    // expected-error{{in array addition both arguments must be arrays}}
  $a += get_double_ref(); // expected-error{{in array addition both arguments must be arrays}}
  $a += get_string_ref(); // expected-error{{in array addition both arguments must be arrays}}

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a += $b;
  $b = true;    $a += $b;
  $b = 123;     $a += $b;
  $b = 12.45;   $a += $b;
  $b = 'qwe';   $a += $b;
  $b = [1,2,3]; $a += $b;
}
$a = [1,2,3];
check_assign_to_array_02($a);

?>