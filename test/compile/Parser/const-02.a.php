// RUN: %clang_php %s
<?php

// Literals as constant values.
const ABCD = 12;
const VVV = ABCD;
static_assert(ABCD === 12);
static_assert(VVV === 12);
static_assert(VVV === ABCD);
static_assert(is_int(ABCD));
static_assert(!is_float(ABCD));

const AA_1 = 'abcd';
const VV_1 = AA_1;
static_assert(AA_1 === 'abcd');
static_assert(VV_1 === 'abcd');
static_assert(AA_1 === VV_1);
static_assert(is_string(AA_1));
//TODO: static_assert(is_int(AA_1));

const AA_2 = true;
const VV_2 = AA_2;
static_assert(AA_2 === true);
static_assert(VV_2 === true);
static_assert(AA_2 === VV_2);
static_assert(is_bool(AA_2));

const AA_3 = 12.3;
const VV_3 = AA_3;
static_assert(AA_3 === 12.3);
static_assert(VV_3 === 12.3);
static_assert(AA_3 === VV_3);
static_assert(is_float(AA_3));

const AA_4 = null;
const VV_4 = AA_4;
static_assert(AA_4 === null);
static_assert(VV_4 === null);
static_assert(AA_4 === VV_4);
static_assert(is_null(AA_4));

const AA_5 = [1,2,3];
const VV_5 = AA_5;
static_assert(AA_5 === [1,2,3]);
static_assert(VV_5 === [1,2,3]);
static_assert(AA_5 === VV_5);
static_assert(is_array(AA_5));


// Expressions as constant values.
const AA_6a = 12;
const AA_6 = AA_6a + 600;
static_assert(AA_6 === 612);
static_assert(is_int(AA_6));
