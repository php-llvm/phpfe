// RUN: %clang_php -ast-dump %s | FileCheck %s
<?php

//--------------------------------------------------------------------
// function return type hint
//--------------------------------------------------------------------

function fn00()           {}
// CHECK-LABEL: FunctionDecl {{.*}} fn00 'class phprt::Box (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt

function fn01(): bool     { return true; }
// CHECK-LABEL: FunctionDecl {{.*}} fn01 '_Bool (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: CXXBoolLiteralExpr

function fn02(): boolean  { return true; }
// CHECK-LABEL: FunctionDecl {{.*}} fn02 '_Bool (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: CXXBoolLiteralExpr

function fn03(): int      { return 0; }
// CHECK-LABEL: FunctionDecl {{.*}} fn03 'long (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: IntegerLiteral

function fn04(): integer  { return 0; }
// CHECK-LABEL: FunctionDecl {{.*}} fn04 'long (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: IntegerLiteral

function fn05(): long     { return 0; }
// CHECK-LABEL: FunctionDecl {{.*}} fn05 'long (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: IntegerLiteral

function fn06(): float    { return 0.0; }
// CHECK-LABEL: FunctionDecl {{.*}} fn06 'double (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: FloatingLiteral

function fn07(): real     { return 0.0; }
// CHECK-LABEL: FunctionDecl {{.*}} fn07 'double (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: FloatingLiteral

function fn08(): double   { return 0.0; }
// CHECK-LABEL: FunctionDecl {{.*}} fn08 'double (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: FloatingLiteral

function fn09(): string   { return "qwe"; }
// CHECK-LABEL: FunctionDecl {{.*}} fn09 'class phprt::StringBox (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: CXXFunctionalCastExpr
// CHECK-NEXT: CXXBindTemporaryExpr
// CHECK-NEXT: CXXConstructExpr
// CHECK-NEXT: ImplicitCastExpr
// CHECK-NEXT: StringLiteral

function fn10(): binary   { return "qwe"; }
// CHECK-LABEL: FunctionDecl {{.*}} fn10 'class phprt::StringBox (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-NEXT: ReturnStmt
// CHECK-NEXT: CXXFunctionalCastExpr
// CHECK-NEXT: CXXBindTemporaryExpr
// CHECK-NEXT: CXXConstructExpr
// CHECK-NEXT: ImplicitCastExpr
// CHECK-NEXT: StringLiteral

//TODO: function fn11(): callable {}

//TODO: add arrays
// function fn12(): array    {}
// _CHECK-LABEL: FunctionDecl {{.*}} fn12
// _CHECK-SAME: 'class phprt::IArray (void)'
// _CHECK-NEXT: CompoundStmt



//--------------------------------------------------------------------
// function return type hint reference
//--------------------------------------------------------------------
function &fr00()           { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr00 'RefBox<class phprt::Box> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr01(): bool     { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr01 'RefBox<_Bool> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr02(): boolean  { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr02 'RefBox<_Bool> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr03(): int      { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr03 'RefBox<long> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr04(): integer  { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr04 'RefBox<long> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr05(): long     { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr05 'RefBox<long> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr06(): float    { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr06 'RefBox<double> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr07(): real     { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr07 'RefBox<double> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr08(): double   { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr08 'RefBox<double> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr09(): string   { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr09 'RefBox<class phprt::StringBox> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

function &fr10(): binary   { return $a; }
// CHECK-LABEL: FunctionDecl {{.*}} fr10 'RefBox<class phprt::StringBox> (void)'
// CHECK-NEXT: CompoundStmt
// CHECK-LABEL: ReturnStmt

//function &fr11(): callable {}

//TODO: add arrays
//function &fr12(): array    {}
// _CHECK-LABEL: FunctionDecl
// _CHECK-SAME: fr12
// _CHECK-SAME: 'Boxed<class phprt::IArray> (void)'
// _CHECK-NEXT: CompoundStmt


//--------------------------------------------------------------------
// function ellipsis argument type hint
//--------------------------------------------------------------------
function fn_000(...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_000
// CHECK-SAME: 'class phprt::Box (ArgPack<class phprt::Box>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<class phprt::Box>'
// CHECK-NEXT: CompoundStmt

function fn_001(bool ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_001
// CHECK-SAME: 'class phprt::Box (ArgPack<_Bool>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<_Bool>'
// CHECK-NEXT: CompoundStmt

function fn_002(boolean ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_002
// CHECK-SAME: 'class phprt::Box (ArgPack<_Bool>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<_Bool>'
// CHECK-NEXT: CompoundStmt

function fn_003(int ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_003
// CHECK-SAME: 'class phprt::Box (ArgPack<long>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<intvalue_t>'
// CHECK-NEXT: CompoundStmt

function fn_004(long ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_004
// CHECK-SAME: 'class phprt::Box (ArgPack<long>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<intvalue_t>'
// CHECK-NEXT: CompoundStmt

function fn_005(integer ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_005
// CHECK-SAME: 'class phprt::Box (ArgPack<long>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<intvalue_t>'
// CHECK-NEXT: CompoundStmt

function fn_006(float ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_006
// CHECK-SAME: 'class phprt::Box (ArgPack<double>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<double>'
// CHECK-NEXT: CompoundStmt

function fn_007(real ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_007
// CHECK-SAME: 'class phprt::Box (ArgPack<double>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<double>'
// CHECK-NEXT: CompoundStmt

function fn_008(double ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_008
// CHECK-SAME: 'class phprt::Box (ArgPack<double>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<double>'
// CHECK-NEXT: CompoundStmt

function fn_009(string ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_009
// CHECK-SAME: 'class phprt::Box (ArgPack<class phprt::StringBox>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<class phprt::StringBox>'
// CHECK-NEXT: CompoundStmt

function fn_010(binary ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_010
// CHECK-SAME: 'class phprt::Box (ArgPack<class phprt::StringBox>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<class phprt::StringBox>'
// CHECK-NEXT: CompoundStmt

function fn_011(array ...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl {{.*}} fn_011
// CHECK-SAME: 'class phprt::Box (ArgPack<class phprt::ArrayBox>)'
// CHECK-NEXT: ParmVarDecl {{.*}} ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<class phprt::ArrayBox>'
// CHECK-NEXT: CompoundStmt





function fn_100(&...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_100
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<class phprt::Box *>'
// CHECK-NEXT: CompoundStmt

function fn_101(bool &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_101
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<_Bool *>'
// CHECK-NEXT: CompoundStmt

function fn_102(boolean &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_102
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<_Bool *>'
// CHECK-NEXT: CompoundStmt

function fn_103(int &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_103
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<intvalue_t *>'
// CHECK-NEXT: CompoundStmt

function fn_104(long &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_104
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<intvalue_t *>'
// CHECK-NEXT: CompoundStmt

function fn_105(integer &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_105
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<intvalue_t *>'
// CHECK-NEXT: CompoundStmt

function fn_106(float &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_106
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<double *>'
// CHECK-NEXT: CompoundStmt

function fn_107(real &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_107
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<double *>'
// CHECK-NEXT: CompoundStmt

function fn_108(double &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_108
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<double *>'
// CHECK-NEXT: CompoundStmt

function fn_109(string &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_109
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<class phprt::StringBox *>'
// CHECK-NEXT: CompoundStmt

function fn_110(binary &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_110
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<class phprt::StringBox *>'
// CHECK-NEXT: CompoundStmt

function fn_111(array &...$ellipsis_arg) {}
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fn_111
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: ellipsis_arg
// CHECK-SAME: 'struct phprt::ArgPack<class phprt::ArrayBox *>'
// CHECK-NEXT: CompoundStmt


//--------------------------------------------------------------------
// function arguments type hint
//--------------------------------------------------------------------
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fa00
function fa00(
			  bool $a0, 
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a0
// CHECK-SAME: '_Bool'

           boolean $a1,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a1
// CHECK-SAME: '_Bool'

               int $a2,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a2
// CHECK-SAME: 'long'

           integer $a3,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a3
// CHECK-SAME: 'long'

              long $a4,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a4
// CHECK-SAME: 'long'

             float $a5,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a5
// CHECK-SAME: 'double'

              real $a6,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a6
// CHECK-SAME: 'double'

            double $a7,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a7
// CHECK-SAME: 'double'

            string $a8,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a8
// CHECK-SAME: 'class phprt::StringBox'

            binary $a9,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a9
// CHECK-SAME: 'class phprt::StringBox'

        //callable $a10,

             array $a11
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a11
// CHECK-SAME: 'class phprt::ArrayBox'
) {}
// CHECK-NEXT: CompoundStmt


//--------------------------------------------------------------------
// function arguments with default values & type hints
//--------------------------------------------------------------------
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fa01
function fa01(bool $a0  = 1, 
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a0
// CHECK-SAME: '_Bool'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1

           boolean $a1  = 2,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a1
// CHECK-SAME: '_Bool'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 2

               int $a2  = 3,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a2
// CHECK-SAME: 'long'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 3

           integer $a3  = 4,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a3
// CHECK-SAME: 'long'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 4

              long $a4  = 5,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a4
// CHECK-SAME: 'long'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 5

             float $a5  = 6.6,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a5
// CHECK-SAME: 'double'
// CHECK-NEXT: FloatingLiteral
// CHECK-SAME: 'double' 6.6

              real $a6  = 7.7,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a6
// CHECK-SAME: 'double'
// CHECK-NEXT: FloatingLiteral
// CHECK-SAME: 'double' 7.7

            double $a7  = 2e10,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a7
// CHECK-SAME: 'double'
// CHECK-NEXT: FloatingLiteral
// CHECK-SAME: 'double' 2.000000e+10

            string $a8  = "qwe",
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a8
// CHECK-SAME: 'class phprt::StringBox'
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

            binary $a9  = "qwe",
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a9
// CHECK-SAME: 'class phprt::StringBox'
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

        //callable $a10 = "fn0",

             array $a11 = [12]
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a11
// CHECK-SAME: 'class phprt::ArrayBox'
// CHECK-NEXT: PhpArrayExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 12
) {}
// CHECK-NEXT: CompoundStmt


//--------------------------------------------------------------------
// function arguments type hint reference
//--------------------------------------------------------------------
// CHECK-LABEL: FunctionDecl
// CHECK-SAME: fa02
function fa02(
        bool &$a0, 
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a0
// CHECK-SAME: '_Bool &'

           boolean &$a1,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a1
// CHECK-SAME: '_Bool &'

               int &$a2,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a2
// CHECK-SAME: 'long &'

           integer &$a3,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a3
// CHECK-SAME: 'long &'

              long &$a4,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a4
// CHECK-SAME: 'long &'

             float &$a5,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a5
// CHECK-SAME: 'double &'

              real &$a6,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a6
// CHECK-SAME: 'double &'

            double &$a7,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a7
// CHECK-SAME: 'double &'

            string &$a8,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a8
// CHECK-SAME: 'class phprt::StringBox &'

            binary &$a9,
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a9
// CHECK-SAME: 'class phprt::StringBox &'

        //callable &$a10,

             array &$a11
// CHECK-NEXT: ParmVarDecl
// CHECK-SAME: a11
// CHECK-SAME: 'class phprt::ArrayBox &'
) {}
// CHECK-NEXT: CompoundStmt

//--------------------------------------------------------------------

?>