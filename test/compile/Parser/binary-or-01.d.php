// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '|'
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
static_assert((false | null   ) === 0);   // expected-warning{{'null' implicitly converted to 'zero'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  | null   ) === 1);   // expected-warning{{'null' implicitly converted to 'zero'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false | false  ) === 0);   // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((false | true   ) === 1);   // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  | false  ) === 1);   // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  | true   ) === 1);   // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  | 123    ) === 123); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false | 123    ) === 123); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  | 123.456) === 123); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false | 123.456) === 123); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  | ''     ) === 1);   // expected-warning{{string does not contain a number}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false | ''     ) === 0);   // expected-warning{{string does not contain a number}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  | "0"    ) === 1);   // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false | "0"    ) === 0);   // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  | "qwe"  ) === 1);   // expected-warning{{string does not contain a number}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false | "qwe"  ) === 0);   // expected-warning{{string does not contain a number}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false | []     ) === 0);   // expected-error{{operation cannot be applied to arrays}}
static_assert(([]    | false  ) === 0);   // expected-error{{operation cannot be applied to arrays}} 

static_assert((null    | false) === 0);   // expected-warning{{'null' implicitly converted to 'zero'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((null    | true ) === 1);   // expected-warning{{'null' implicitly converted to 'zero'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123     | true ) === 123); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123     | false) === 123); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123.456 | true ) === 123); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123.456 | false) === 123); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((''      | true ) === 1);   // expected-warning{{string does not contain a number}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((''      | false) === 0);   // expected-warning{{string does not contain a number}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("0"     | true ) === 1);   // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("0"     | false) === 0);   // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("qwe"   | true ) === 1);   // expected-warning{{string does not contain a number}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("qwe"   | false) === 0);   // expected-warning{{string does not contain a number}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}


//----- null with types ---------------------------------------------
static_assert((null | null   ) === 0);    // expected-warning 2 {{'null' implicitly converted to 'zero'}}
static_assert((null | ''     ) === 0);    // expected-warning{{'null' implicitly converted to 'zero'}}
                                          // expected-warning@-1{{string does not contain a number}}
static_assert((null | "0"    ) === 0);    // expected-warning{{'null' implicitly converted to 'zero'}}
                                          // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((null | "qwe"  ) === 0);    // expected-warning{{'null' implicitly converted to 'zero'}}
                                          // expected-warning@-1{{string does not contain a number}}
static_assert((null | "123"  ) === 123);  // expected-warning{{'null' implicitly converted to 'zero'}}
                                          // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((null | 123    ) === 123);  // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null | -123   ) === -123); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null | 123.123) === 123);  // expected-warning{{'null' implicitly converted to 'zero'}}
                                          // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}

static_assert((''      | null) === 0); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{string does not contain a number}}
static_assert(("0"     | null) === 0); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"   | null) === 0); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{string does not contain a number}}
static_assert(("123"   | null) === 123); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((123     | null) === 123); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((-123    | null) === -123); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((123.123 | null) === 123); // expected-warning{{'null' implicitly converted to 'zero'}} expected-warning{{implicit conversion from 'double' to 'integer'}}


//----- integer with types ------------------------------------------
static_assert((123 | 123.4) === 123 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
static_assert((123 | 456  ) === 507 );
static_assert((456 | 123  ) === 507 );
static_assert((123 | 123  ) === 123 );
static_assert((123 | ''   ) === 123 ); // expected-warning{{string does not contain a number}}
static_assert((123 | "qwe") === 123 ); // expected-warning{{string does not contain a number}}
static_assert((123 | "456") === 507 ); // expected-warning{{implicit conversion from 'string' to 'integer'}}

static_assert((123.4 | 123) === 123 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
static_assert((''    | 123) === 123 ); // expected-warning{{string does not contain a number}}
static_assert(("qwe" | 123) === 123 ); // expected-warning{{string does not contain a number}}
static_assert(("456" | 123) === 507 ); // expected-warning{{implicit conversion from 'string' to 'integer'}}


//----- double with types -------------------------------------------
static_assert((4.2 | 1.2   ) === 5 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                      // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}
static_assert((1.3 | 5.2   ) === 5 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                      // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}
static_assert((-.2 | ''    ) === 0 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                      // expected-warning@-1{{string does not contain a number}}
static_assert((1.2 | "qwe" ) === 1 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                      // expected-warning@-1{{string does not contain a number}}
static_assert((6.2 | "1e4" ) === 10006 ); // expected-warning 2 {{implicit conversion from 'double' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'string' to 'double'}}

static_assert((''     | -.2) === 0 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                      // expected-warning@-1{{string does not contain a number}}
static_assert(("qwe"  | 1.2) === 1 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                      // expected-warning@-1{{string does not contain a number}}
static_assert(("1e4"  | 1.2) === 10001 ); // expected-warning 2 {{implicit conversion from 'double' to 'integer'}}
                                          // expected-warning@-1{{implicit conversion from 'string' to 'double'}}


//----- string with types -------------------------------------------
static_assert(("01234568" | "12345679") === "13375779");
static_assert(("qweasdzxc" | "asd") === "qweasdzxc");
static_assert(("zc" | "qeasdzxc") === "{gasdzxc");

static_assert(([]      | []     )  === 0); // expected-error{{operation cannot be applied to arrays}}
static_assert(([1,2,3] | []     )  === 1); // expected-error{{operation cannot be applied to arrays}}
static_assert(([3,4]   | [1,2,3])  === 1); // expected-error{{operation cannot be applied to arrays}}
static_assert(([1,2]   | [1,2]  )  === 1); // expected-error{{operation cannot be applied to arrays}}
