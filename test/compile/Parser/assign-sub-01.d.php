// RUN: %clang_php %s -verify
<?php

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_bool_01(bool $a) {
  assert($a === true);

  $a -= false; assert($a === true);       // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a -= true;  assert($a === false);      // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                          // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a -= null;    assert($a === true);  // expected-warning{{'null' implicitly converted to 'zero'}}
                                                  // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= 123;     assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= 12.34;   assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a = true; $a -= "qwe";   assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
                                                  // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a -= get_void();   assert($a === true);  // expected-warning{{'null' implicitly converted to 'zero'}}
                                                       // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= get_bool();   assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                                       // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a = true; $a -= get_int();    assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= get_double(); assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a = true; $a -= get_string(); assert($a === false); // expected-warning{{implicit conversion from 'string' to 'number'}}
                                                       // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a -= get_bool_ref();   assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                                           // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a = true; $a -= get_int_ref();    assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= get_double_ref(); assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a = true; $a -= get_string_ref(); assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
                                                           // expected-warning@-1{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a = true; $a -= $b; assert($a === true);
  $b = true;    $a = true; $a -= $b; assert($a === false);
  $b = 123;     $a = true; $a -= $b; assert($a === false);
  $b = 12.45;   $a = true; $a -= $b; assert($a === false);
  $b = 'qwe';   $a = true; $a -= $b; assert($a === true);
  $b = [1,2,3]; $a = true; $a -= $b; assert($a === false);
}
check_assign_to_bool_01(true);


function check_assign_to_bool_02(bool &$a) {
  assert($a === true);

  $a -= false; assert($a === true);  // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                     // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a -= true;  assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                     // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = true; $a -= null;    assert($a === true);  // expected-warning{{'null' implicitly converted to 'zero'}}
                                                  // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= 123;     assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= 12.34;   assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a = true; $a -= "qwe";   assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
                                                  // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = true; $a -= get_void();   assert($a === true);  // expected-warning{{'null' implicitly converted to 'zero'}}
                                                       // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= get_bool();   assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                                       // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a = true; $a -= get_int();    assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= get_double(); assert($a === false); // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a = true; $a -= get_string(); assert($a === false); // expected-warning{{implicit conversion from 'string' to 'number'}}
                                                       // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = true; $a -= get_bool_ref();   assert($a === false);  // expected-warning{{operation requires conversion from 'boolean' to 'integer' and back}}
                                                            // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
  $a = true; $a -= get_int_ref();    assert($a === false);  // expected-warning{{operation requires conversion from 'boolean' to 'number' and back}}
  $a = true; $a -= get_double_ref(); assert($a === false);  // expected-warning{{operation requires conversion from 'boolean' to 'double' and back}}
  $a = true; $a -= get_string_ref(); assert($a === false);  // expected-warning{{implicit conversion from 'string' to 'number'}}
                                                            // expected-warning@-1{{operation requires conversion from 'boolean' to 'number' and back}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a = true; $a -= $b; assert($a === true);
  $b = true;    $a = true; $a -= $b; assert($a === false);
  $b = 123;     $a = true; $a -= $b; assert($a === false);
  $b = 12.45;   $a = true; $a -= $b; assert($a === false);
  $b = 'qwe';   $a = true; $a -= $b; assert($a === true);
  $b = [1,2,3]; $a = true; $a -= $b; assert($a === false);
}
$a = true;
check_assign_to_bool_02($a);








function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a -= 456;  assert($a === -333);
  $a -= -12;  assert($a === -321);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;  assert($a === -321);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a -= false; assert($a === -321);
  $a -= 123;   assert($a === -444);
  $a -= 12.34; assert($a === -456);   // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a -= "qwe"; assert($a === -456);   // expected-warning{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   assert($a === -456);  // expected-warning{{'null' implicitly converted to 'zero'}}
  $a -= get_bool();   assert($a === -457);
  $a -= get_int();    assert($a === -579);
  $a -= get_double(); assert($a === -591);  // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a -= get_string(); assert($a === -591);  // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   assert($a === -592);
  $a -= get_int_ref();    assert($a === -715);
  $a -= get_double_ref(); assert($a === -727);  // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a -= get_string_ref(); assert($a === -727);  // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a -= $b; assert($a === -727);
  $b = true;  $a -= $b; assert($a === -728);
  $b = 123;   $a -= $b; assert($a === -851);
  $b = 12.45; $a -= $b; assert($a === -863);
  $b = 'qwe';   $a -= $b; assert($a === -863);
  $b = [1,2,3]; $a -= $b;
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a -= 456;  assert($a === -333);
  $a -= -12;  assert($a === -321);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;  assert($a === -321);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a -= false; assert($a === -321);
  $a -= 123;   assert($a === -444);
  $a -= 12.34; assert($a === -456);   // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a -= "qwe"; assert($a === -456);   // expected-warning{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   assert($a === -456);  // expected-warning{{'null' implicitly converted to 'zero'}}
  $a -= get_bool();   assert($a === -457);
  $a -= get_int();    assert($a === -579);
  $a -= get_double(); assert($a === -591);  // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a -= get_string(); assert($a === -591);  // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   assert($a === -592);
  $a -= get_int_ref();    assert($a === -715);
  $a -= get_double_ref(); assert($a === -727);  // expected-warning{{operation requires conversion from 'integer' to 'double' and back}}
  $a -= get_string_ref(); assert($a === -727);  // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a -= $b; assert($a === -727);
  $b = true;  $a -= $b; assert($a === -728);
  $b = 123;   $a -= $b; assert($a === -851);
  $b = 12.45; $a -= $b; assert($a === -863);
  $b = 'qwe';   $a -= $b; assert($a === -863);
  $b = [1,2,3]; $a -= $b;
}
$a = 123;
check_assign_to_int_02($a);








function eq(double $a, double $b) : bool { return abs($a - $b) < 1e-10; }

function check_assign_to_double_01(float $a) {
  assert($a === 123.456);

  $a -= 456.5;   assert($a === -333.044);
  $a -= -12.258; assert($a === -345.302);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;  assert($a === -345.302);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a -= false; assert($a === -345.302);   // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a -= 123;   assert($a === -468.302);
  $a -= 12.34; assert($a === -480.642);
  $a -= "qwe"; assert($a === -480.642);   // expected-warning{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   assert($a === -480.642); // expected-warning{{'null' implicitly converted to 'zero'}}
  $a -= get_bool();   assert($a === -481.642); // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a -= get_int();    assert($a === -604.642);
  $a -= get_double(); assert($a === -616.982);
  $a -= get_string(); assert($a === -616.982); // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   assert($a === -617.982);  // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a -= get_int_ref();    assert($a === -740.982);  // expected-warning{{implicit conversion from 'integer' to 'double'}}
  $a -= get_double_ref(); assert(eq($a, -753.322));
  $a -= get_string_ref(); assert($a === -753.322);  // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a -= $b; assert($a === -753.322);
  $b = true;    $a -= $b; assert(eq($a, -754.322));
  $b = 123;     $a -= $b; assert(eq($a, -877.322));
  $b = 12.45;   $a -= $b; assert(eq($a, -889.772));
  $b = 'qwe';   $a -= $b; assert($a === -889.772);
  $b = [1,2,3]; $a -= $b;
}

check_assign_to_double_01(123.456);

function check_assign_to_double_02(float &$a) {
  assert($a === 123.456);

  $a -= 456.5;   assert($a === -333.044);
  $a -= -12.258; assert($a === -345.302);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;  assert($a === -345.302);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a -= false; assert($a === -345.302);   // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a -= 123;   assert($a === -468.302);
  $a -= 12.34; assert($a === -480.642);
  $a -= "qwe"; assert($a === -480.642);   // expected-warning{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   assert($a === -480.642); // expected-warning{{'null' implicitly converted to 'zero'}}
  $a -= get_bool();   assert($a === -481.642); // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a -= get_int();    assert($a === -604.642);
  $a -= get_double(); assert($a === -616.982);
  $a -= get_string(); assert($a === -616.982); // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   assert($a === -617.982);  // expected-warning{{implicit conversion from 'boolean' to 'double'}}
  $a -= get_int_ref();    assert($a === -740.982);  // expected-warning{{implicit conversion from 'integer' to 'double'}}
  $a -= get_double_ref(); assert(eq($a, -753.322));
  $a -= get_string_ref(); assert($a === -753.322);  // expected-warning{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to float
//-------------------------------------------------------------------
  $b = null;    $a -= $b; assert($a === -753.322);
  $b = true;    $a -= $b; assert(eq($a, -754.322));
  $b = 123;     $a -= $b; assert(eq($a, -877.322));
  $b = 12.45;   $a -= $b; assert(eq($a, -889.772));
  $b = 'qwe';   $a -= $b; assert($a === -889.772);
  $b = [1,2,3]; $a -= $b;
}
$a = 123.456;
check_assign_to_double_02($a);








//TODO: add warninigs for <string> -= anything?
function check_assign_to_string_01(string $a) {
  assert($a === 'qwe');

  $a -= 'zxc';   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}
  $a -= '';      // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;    // expected-warning{{'null' implicitly converted to 'zero'}}
                 // expected-warning@-1{{operation requires conversion from 'string' to 'number' and back}}
  $a -= false;   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= 123;     // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= 12.34;   // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a -= "qwe";   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   // expected-warning{{'null' implicitly converted to 'zero'}}
                      // expected-warning@-1{{operation requires conversion from 'string' to 'number' and back}}
  $a -= get_bool();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= get_int();    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= get_double(); // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a -= get_string(); // expected-warning{{implicit conversion from 'string' to 'number'}}
                      // expected-warning@-1{{operation requires conversion from 'string' to 'number' and back}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= get_int_ref();    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= get_double_ref(); // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a -= get_string_ref(); // expected-warning{{implicit conversion from 'string' to 'number'}}
                          // expected-warning@-1{{operation requires conversion from 'string' to 'number' and back}}

//-------------------------------------------------------------------
// assign box to string
//-------------------------------------------------------------------
  $b = null;    $a -= $b;
  $b = true;    $a -= $b;
  $b = 123;     $a -= $b;
  $b = 12.45;   $a -= $b;
  $b = 'qwe';   $a -= $b;
  $b = [1,2,3]; $a -= $b;
}
check_assign_to_string_01('qwe');

function check_assign_to_string_02(string &$a) {
  assert($a === 'qwe');

  $a -= 'zxc';   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}
  $a -= '';      // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a -= null;    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{'null' implicitly converted to 'zero'}}
  $a -= false;   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= 123;     // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= 12.34;   // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a -= "qwe";   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                 // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a -= get_void();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                      // expected-warning@-1{{'null' implicitly converted to 'zero'}}
  $a -= get_bool();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= get_int();    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= get_double(); // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a -= get_string(); // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                      // expected-warning@-1{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a -= get_bool_ref();   // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= get_int_ref();    // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
  $a -= get_double_ref(); // expected-warning{{operation requires conversion from 'string' to 'double' and back}}
  $a -= get_string_ref(); // expected-warning{{operation requires conversion from 'string' to 'number' and back}}
                          // expected-warning@-1{{implicit conversion from 'string' to 'number'}}

//-------------------------------------------------------------------
// assign box to string
//-------------------------------------------------------------------
  $b = null;    $a -= $b;
  $b = true;    $a -= $b;
  $b = 123;     $a -= $b;
  $b = 12.45;   $a -= $b;
  $b = 'qwe';   $a -= $b;
  $b = [1,2,3]; $a -= $b;
}
$a = 'qwe';
check_assign_to_string_02($a);






function check_assign_to_array_01(array $a) {
  assert($a === [1,2,3]);

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a -= $b;
  $b = true;    $a -= $b;
  $b = 123;     $a -= $b;
  $b = 12.45;   $a -= $b;
  $b = 'qwe';   $a -= $b;
  $b = [1,2,3]; $a -= $b;
}
check_assign_to_array_01([1,2,3]);

function check_assign_to_array_02(array &$a) {
  assert($a === [1,2,3]);

//-------------------------------------------------------------------
// assign box to array
//-------------------------------------------------------------------
  $b = null;    $a -= $b;
  $b = true;    $a -= $b;
  $b = 123;     $a -= $b;
  $b = 12.45;   $a -= $b;
  $b = 'qwe';   $a -= $b;
  $b = [1,2,3]; $a -= $b;
}
$a = [1,2,3];
check_assign_to_array_02($a);

?>