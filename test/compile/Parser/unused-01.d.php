// RUN: %clang_php %s 2>&1 | %ErrorCheck %s 
<?php

//--------------------------------------------------------------------

// Nulls do not produce unused result message.
null;

true;
// CHECK: : warning: expression result unused
// CHECK: true;
// CHECK: ^~~~

123;
// CHECK: : warning: expression result unused
// CHECK: 123;
// CHECK: ^~~

3.14;
// CHECK: : warning: expression result unused
// CHECK: 3.14;
// CHECK: ^~~~

'abcd';
// CHECK: : warning: expression result unused
// CHECK: 'abcd';
// CHECK: ^~~~~~

//"qwer";  //TODO: include quotes in range
//$a;     //TODO: include $ in range

//11 + 11;//TODO: include entire expr in range

/*
//TODO: $ must be included into range
$abc + 22;


+1;     // expected-warning{{expression result unused}}
-$a;    // expected-warning{{expression result unused}}
(11+11);  // expected-warning{{expression result unused}}

$a = 123;
//$a += 123;

[1,2,3];         // expected-warning{{expression result unused}}
["qwe" => 3.14]; // expected-warning{{expression result unused}}
array(11, 22, 33);

function fn() {}
[1,2,3,fn()];

min(1,2,3);        // expected-warning{{expression result unused}}
max(1,1+1,1+1+1);  // expected-warning{{expression result unused}}
*/

$a = $bbb xor 111;
// CHECK: warning: expression result unused
// CHECK: $a = $bbb xor 111;
// CHECK:  ~~~~~~~~ ^   ~~~
//TODO: $ must be included into range

$a = $bbb or 111;
// CHECK: warning: expression result unused
// CHECK: $a = $bbb or 111;
// CHECK:  ~~~~~~~~ ^  ~~~
//TODO: $ must be included into range

$a = $bbb and 111;
// CHECK: warning: expression result unused
// CHECK: $a = $bbb and 111;
// CHECK:  ~~~~~~~~ ^   ~~~
//TODO: $ must be included into range

?>