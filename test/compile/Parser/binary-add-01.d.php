// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '+'
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
static_assert((false + null   ) === 0       ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  + null   ) === 1       ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false + false  ) === 0       ); // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((false + true   ) === 1       ); // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  + false  ) === 1       ); // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  + true   ) === 2       ); // expected-warning 2 {{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  + 123    ) === 124     ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false + 123    ) === 123     ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  + 123.456) === 124.456 ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false + 123.456) === 123.456 ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  + ''     ) === 1       ); // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false + ''     ) === 0       ); // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  + "0"    ) === 1       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false + "0"    ) === 0       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  + "qwe"  ) === 1       ); // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false + "qwe"  ) === 0       ); // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert( false + []                   ); // expected-error{{in array addition both arguments must be arrays}}
static_assert( []    + false                ); // expected-error{{in array addition both arguments must be arrays}}

static_assert((null    + false) === 0       ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((null    + true ) === 1       ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123     + true ) === 124     ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123     + false) === 123     ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123.456 + true ) === 124.456 ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123.456 + false) === 123.456 ); // expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((''      + true ) === 1       ); // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((''      + false) === 0       ); // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("0"     + true ) === 1       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("0"     + false) === 0       ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("qwe"   + true ) === 1       ); // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("qwe"   + false) === 0       ); // expected-warning{{string does not contain a number}}
                                               // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}


//----- null with types ---------------------------------------------
static_assert((null + null   ) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                             // expected-warning@-1{{'null' implicitly converted to 'zero'}}
static_assert((null + ''     ) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                             // expected-warning@-1{{string does not contain a number}}
static_assert((null + "0"    ) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                             // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((null + "qwe"  ) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                             // expected-warning@-1{{string does not contain a number}}
static_assert((null + "123"  ) === 123    ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                             // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((null + 123    ) === 123    ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null + -123   ) === -123   ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null + 123.123) === 123.123); // expected-warning{{'null' implicitly converted to 'zero'}}

static_assert((''      + null) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                             // expected-warning@-1{{string does not contain a number}}
static_assert(("0"     + null) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                             // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"   + null) === 0      ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                             // expected-warning@-1{{string does not contain a number}}
static_assert(("123"   + null) === 123    ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                             // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((123     + null) === 123    ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((-123    + null) === -123   ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((123.123 + null) === 123.123); // expected-warning{{'null' implicitly converted to 'zero'}}


//----- integer with types ------------------------------------------
static_assert((123 + 123.4) === 246.4 );
static_assert((123 + 456  ) === 579   );
static_assert((456 + 123  ) === 579   );
static_assert((123 + 123  ) === 246   );
static_assert((123 + ''   ) === 123   ); // expected-warning{{string does not contain a number}}
static_assert((123 + "qwe") === 123   ); // expected-warning{{string does not contain a number}}
static_assert((123 + "456") === 579   ); // expected-warning{{implicit conversion from 'string' to 'integer'}}

static_assert((123.4 + 123) === 246.4 );
static_assert((456   + 123) === 579   );
static_assert((123   + 456) === 579   );
static_assert((123   + 123) === 246   );
static_assert((''    + 123) === 123   ); // expected-warning{{string does not contain a number}}
static_assert(("qwe" + 123) === 123   ); // expected-warning{{string does not contain a number}}
static_assert(("456" + 123) === 579   ); // expected-warning{{implicit conversion from 'string' to 'integer'}}


//----- double with types -------------------------------------------
static_assert((1.2 + 1.2   ) === 2.4           );
static_assert((1.3 + 1.2   ) === 2.5           );
static_assert((1.2 + 1.3   ) === 2.5           );
static_assert((-.2 + ''    ) === -0.2          ); // expected-warning{{string does not contain a number}}
static_assert((1.2 + "qwe" ) === 1.2           ); // expected-warning{{string does not contain a number}}
static_assert((1.2 + "1e10") === 10000000001.2 ); // expected-warning{{implicit conversion from 'string' to 'double'}}

static_assert((''     + -.2) === -0.2          ); // expected-warning{{string does not contain a number}}
static_assert(("qwe"  + 1.2) === 1.2           ); // expected-warning{{string does not contain a number}}
static_assert(("1e10" + 1.2) === 10000000001.2 ); // expected-warning{{implicit conversion from 'string' to 'double'}}


//----- string with types -------------------------------------------
static_assert(("123"     + "0456"   ) === 579      ); // expected-warning 2 {{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"     + "0456"   ) === 456      ); // expected-warning{{string does not contain a number}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert(("0456"    + "qwe"    ) === 456      ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                      // expected-warning@-1{{string does not contain a number}}
static_assert(("qwe"     + "qwe"    ) === 0        ); // expected-warning 2 {{string does not contain a number}}
static_assert(("-123"    + ".123"   ) === -122.877 ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    + ".123"   ) === 123.123  ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    + ' .123'  ) === 123.123  ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((' +123'   + ".123"   ) === 123.123  ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert(("+123asd" + ".123qwe") === 123.123  ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'double'}}

static_assert(("0456"  + "123"  ) === 579          ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert(("0456"  + "qwe"  ) === 456          ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                      // expected-warning@-1{{string does not contain a number}}
static_assert(("qwe"   + "0456" ) === 456          ); // expected-warning{{string does not contain a number}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"   + "qwe"  ) === 0            ); // expected-warning{{string does not contain a number}}
                                                      // expected-warning@-1{{string does not contain a number}}
static_assert((".123"    + "-123"   ) === -122.877 ); // expected-warning{{implicit conversion from 'string' to 'double'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((".123"    + "+123"   ) === 123.123  ); // expected-warning{{implicit conversion from 'string' to 'double'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((' .123'   + "+123"   ) === 123.123  ); // expected-warning{{implicit conversion from 'string' to 'double'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((".123"    + ' +123'  ) === 123.123  ); // expected-warning{{implicit conversion from 'string' to 'double'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((".123qwe" + "+123asd") === 123.123  ); // expected-warning{{implicit conversion from 'string' to 'double'}}
                                                      // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}

static_assert(([]      + []     )  === []);
static_assert(([1,2,3] + []     )  === [1,2,3]);
static_assert(([]      + [1,2,3])  === [1,2,3]);
static_assert(([3,4]   + [1,2,3])  === [3,4,3]);
static_assert(([1,2]   + [1,2]  )  === [1,2]);

static_assert((["a" => 1] + ["b" => 1])  === ["a" => 1, "b" => 1]);
static_assert((["b" => 1] + ["a" => 1])  === ["b" => 1, "a" => 1]);

static_assert(([3 => 1] + [7 => 1])  === [3 => 1, 7 => 1]);
static_assert(([7 => 1] + [3 => 1])  === [7 => 1, 3 => 1]);

static_assert((["a" => 1] + ["a" => 2])  === ["a" => 1]);
static_assert((["a" => 2] + ["a" => 1])  === ["a" => 2]);
