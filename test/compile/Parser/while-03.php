// RUN: %clang_php %s -verify
<?php
$a = 5;
while  ($a < 10): {
    echo "echo";  
}
endwhile // expected-error {{expected ';' after endwhile statement}}
  $a=1; 
?>