<?php
// RUN: %clang_php %s -verify

namespace ABCD { // expected-note{{previous namespace defined here}}
}

namespace DEF; // expected-error{{cannot mix bracketed namespaces with simple}}

namespace A1 {}

namespace AA2 {  // expected-note{{outer namespace starts here}}
  namespace BB2 {  // expected-error{{namespace nesting is not allowed}}
  }
}

namespace =A {  // expected-error{{expected namespace name}}
}

namespace =A;  // expected-error{{expected namespace name}}

namespace A/B {  // expected-error{{expected '{' or ';'}}
}

namespace ;  // expected-error{{expected namespace name}}

namespace A\B {
}

