// RUN: %clang_php %s -verify
<?php

//--------------------------------------------------------------------
// Legalize box to specified type
//--------------------------------------------------------------------
function fn_100(          $a) { echo "fn_100!\n"; }
function fn_101(bool      $a) { echo "fn_101!\n"; }
function fn_102(boolean   $a) { echo "fn_102!\n"; }
function fn_103(int       $a) { echo "fn_103!\n"; }
function fn_104(long      $a) { echo "fn_104!\n"; }
function fn_105(integer   $a) { echo "fn_105!\n"; }
function fn_106(real      $a) { echo "fn_106!\n"; }
function fn_107(float     $a) { echo "fn_107!\n"; }
function fn_108(double    $a) { echo "fn_108!\n"; }
function fn_109(string    $a) { echo "fn_109!\n"; }
function fn_110(binary    $a) { echo "fn_110!\n"; }

fn_100($fn_100);
fn_101($fn_101);
fn_102($fn_102);
fn_103($fn_103);
fn_104($fn_104);
fn_105($fn_105);
fn_106($fn_106);
fn_107($fn_107);
fn_108($fn_108);
fn_109($fn_109);
fn_110($fn_110);

//--------------------------------------------------------------------
// Legalize box to specified type reference
//--------------------------------------------------------------------
function fn_200(         &$a) { echo "fn_200!\n"; }
function fn_201(bool     &$a) { echo "fn_201!\n"; }
function fn_202(boolean  &$a) { echo "fn_202!\n"; }
function fn_203(int      &$a) { echo "fn_203!\n"; }
function fn_204(long     &$a) { echo "fn_204!\n"; }
function fn_205(integer  &$a) { echo "fn_205!\n"; }
function fn_206(real     &$a) { echo "fn_206!\n"; }
function fn_207(float    &$a) { echo "fn_207!\n"; }
function fn_208(double   &$a) { echo "fn_208!\n"; }
function fn_209(string   &$a) { echo "fn_209!\n"; }
function fn_210(binary   &$a) { echo "fn_210!\n"; }

fn_200($fn_200);
fn_201($fn_201);
fn_202($fn_202);
fn_203($fn_203);
fn_204($fn_204);
fn_205($fn_205);
fn_206($fn_206);
fn_207($fn_207);
fn_208($fn_208);
fn_209($fn_209);
fn_210($fn_210);

echo fn_200($fn_200);

//--------------------------------------------------------------------
// Legalize return value
//--------------------------------------------------------------------
function fn_300()           { echo "fn_300!\n"; return $fn_300; }
function fn_301(): bool     { echo "fn_301!\n"; return $fn_301; }
function fn_302(): boolean  { echo "fn_302!\n"; return $fn_302; }
function fn_303(): int      { echo "fn_303!\n"; return $fn_303; }
function fn_304(): long     { echo "fn_304!\n"; return $fn_304; }
function fn_305(): integer  { echo "fn_305!\n"; return $fn_305; }
function fn_306(): float    { echo "fn_306!\n"; return $fn_306; }
function fn_307(): real     { echo "fn_307!\n"; return $fn_307; }
function fn_308(): double   { echo "fn_308!\n"; return $fn_308; }
function fn_309(): string   { echo "fn_309!\n"; return $fn_309; }
function fn_310(): binary   { echo "fn_310!\n"; return $fn_310; }
//function fn_311(): array    { echo "fn_311!\n"; return $fn_311; }

echo fn_300();
echo fn_301();
echo fn_302();
echo fn_303();
echo fn_304();
echo fn_305();
echo fn_306();
echo fn_307();
echo fn_308();
echo fn_309();
echo fn_310();

echo fn_100(fn_300());
echo fn_101(fn_301());
echo fn_102(fn_302());
echo fn_103(fn_303());
echo fn_104(fn_304());
echo fn_105(fn_305());
echo fn_106(fn_306());
echo fn_107(fn_307());
echo fn_108(fn_308());
echo fn_109(fn_309());
echo fn_110(fn_310());

//--------------------------------------------------------------------
// Legalize return value reference
//--------------------------------------------------------------------
function &fn_400()           { echo "fn_400!\n"; return $fn_400; }
function &fn_401(): bool     { echo "fn_401!\n"; return $fn_401; }
function &fn_402(): boolean  { echo "fn_402!\n"; return $fn_402; }
function &fn_403(): int      { echo "fn_403!\n"; return $fn_403; }
function &fn_404(): long     { echo "fn_404!\n"; return $fn_404; }
function &fn_405(): integer  { echo "fn_405!\n"; return $fn_405; }
function &fn_406(): float    { echo "fn_406!\n"; return $fn_406; }
function &fn_407(): real     { echo "fn_407!\n"; return $fn_407; }
function &fn_408(): double   { echo "fn_408!\n"; return $fn_408; }
function &fn_409(): string   { echo "fn_409!\n"; return $fn_409; }
function &fn_410(): binary   { echo "fn_410!\n"; return $fn_410; }
function &fn_411(): array    { echo "fn_411!\n"; return $fn_411; }

fn_400();
fn_401();
fn_402();
fn_403();
fn_404();
fn_405();
fn_406();
fn_407();
fn_408();
fn_409();
fn_410();

echo fn_400();
echo fn_401();
echo fn_402();
echo fn_403();
echo fn_404();
echo fn_405();
echo fn_406();
echo fn_407();
echo fn_408();
echo fn_409();
echo fn_410();

echo fn_100(fn_400());
echo fn_101(fn_401());
echo fn_102(fn_402());
echo fn_103(fn_403());
echo fn_104(fn_404());
echo fn_105(fn_405());
echo fn_106(fn_406());
echo fn_107(fn_407());
echo fn_108(fn_408());
echo fn_109(fn_409());
echo fn_110(fn_410());

echo fn_200(fn_400());
echo fn_201(fn_401());
echo fn_202(fn_402());
echo fn_203(fn_403());
echo fn_204(fn_404());
echo fn_205(fn_405());
echo fn_206(fn_406());
echo fn_207(fn_407());
echo fn_208(fn_408());
echo fn_209(fn_409());
echo fn_210(fn_410());

//--------------------------------------------------------------------
// Legalize variadic arguments
//--------------------------------------------------------------------
function fn_500(        ...$a) { echo "fn_500!\n"; }
function fn_501(bool    ...$a) { echo "fn_501!\n"; }
function fn_502(boolean ...$a) { echo "fn_502!\n"; }
function fn_503(int     ...$a) { echo "fn_503!\n"; }
function fn_504(long    ...$a) { echo "fn_504!\n"; }
function fn_505(integer ...$a) { echo "fn_505!\n"; }
function fn_506(float   ...$a) { echo "fn_506!\n"; }
function fn_507(real    ...$a) { echo "fn_507!\n"; }
function fn_508(double  ...$a) { echo "fn_508!\n"; }
function fn_509(string  ...$a) { echo "fn_509!\n"; }
function fn_510(binary  ...$a) { echo "fn_510!\n"; }
//function fn_511(array   ...$a) { echo "fn_511!\n"; }


fn_500();
fn_500(1,"qwe",3.14);
fn_500(1,true,3,"asd",null);

fn_501();
fn_501(true, false, true);
fn_501(true, false, true, true, true);

fn_502();
fn_502(true, false);
fn_502(true, false, false, false);

fn_503();
fn_503(1,2,3);
fn_503(1,2,3,4,5);

fn_504();
fn_504(1);
fn_504(1,2,3,4,5);

fn_505();
fn_505(-1, +1);
fn_505(1,2,3,4,5);

fn_506();
fn_506(-1.234);
fn_506(1.1, 1.1 + 0.2, 3.3, 4.4);

fn_507();
fn_507(1.2, 2.3, 4.5);
fn_507(5.5, 6.6);

fn_508();
fn_508(1.2, 6.543, 8.7);
fn_508(7.7);

fn_509();
fn_509("qwe", "asd", "zxc", "rty", "fgh");
fn_509("q", "a", "z");

fn_510();
fn_510("qaz");
fn_510("qwe", "q", "w", "e");

//--------------------------------------------------------------------
// Legalize variadic arguments with references
//--------------------------------------------------------------------
function fn_600(        &...$a) { echo "fn_600!\n"; }
function fn_601(bool    &...$a) { echo "fn_601!\n"; }
function fn_602(boolean &...$a) { echo "fn_602!\n"; }
function fn_603(int     &...$a) { echo "fn_603!\n"; }
function fn_604(long    &...$a) { echo "fn_604!\n"; }
function fn_605(integer &...$a) { echo "fn_605!\n"; }
function fn_606(float   &...$a) { echo "fn_606!\n"; }
function fn_607(real    &...$a) { echo "fn_607!\n"; }
function fn_608(double  &...$a) { echo "fn_608!\n"; }
function fn_609(string  &...$a) { echo "fn_609!\n"; }
function fn_610(binary  &...$a) { echo "fn_610!\n"; }
//function fn_611(array   &...$a) { echo "fn_611!\n"; }

fn_600();
fn_600($v_6001, $v_6002, $v_6003);

fn_601();
fn_601($v_6011, $v_6012, $v_6013, $v_6014);

fn_602();
fn_602($v_6021, $v_6022, $v_6023);

fn_603();
fn_603($v_6031, $v_6032, $v_6033, $v_6034);

fn_604();
fn_604($v_6041, $v_6042, $v_6043);

fn_605();
fn_605($v_6051, $v_6052, $v_6053, $v_6054);

fn_606();
fn_606($v_6061, $v_6062, $v_6063);

fn_607();
fn_607($v_6071, $v_6072, $v_6073, $v_6074);

fn_608();
fn_608($v_6081, $v_6082, $v_6083);

fn_609();
fn_609($v_6091, $v_6092, $v_6093, $v_6094);

fn_610();
fn_610($v_6101, $v_6102, $v_6103);

//--------------------------------------------------------------------
// Legalize returning & receiving return values with type hints
//--------------------------------------------------------------------
function &fn_o100()          { return fn_o100(); }
function &fn_o101(): bool    { return fn_o101(); }
function &fn_o102(): boolean { return fn_o102(); }
function &fn_o103(): int     { return fn_o103(); }
function &fn_o104(): long    { return fn_o104(); }
function &fn_o105(): integer { return fn_o105(); }
function &fn_o106(): float   { return fn_o106(); }
function &fn_o107(): real    { return fn_o107(); }
function &fn_o108(): double  { return fn_o108(); }
function &fn_o109(): string  { return fn_o109(); }
function &fn_o110(): binary  { return fn_o110(); }
//function &fn_o111(): array   { return fn_o111(); }

function  fn_o200()          { return fn_o200(); }
function  fn_o201(): bool    { return fn_o201(); }
function  fn_o202(): boolean { return fn_o202(); }
function  fn_o203(): int     { return fn_o203(); }
function  fn_o204(): long    { return fn_o204(); }
function  fn_o205(): integer { return fn_o205(); }
function  fn_o206(): float   { return fn_o206(); }
function  fn_o207(): real    { return fn_o207(); }
function  fn_o208(): double  { return fn_o208(); }
function  fn_o209(): string  { return fn_o209(); }
function  fn_o210(): binary  { return fn_o210(); }
//function  fn_o211(): array   { return fn_o211(); }

function  fn_o300()          { return fn_o100(); }
function  fn_o301(): bool    { return fn_o101(); }
function  fn_o302(): boolean { return fn_o102(); }
function  fn_o303(): int     { return fn_o103(); }
function  fn_o304(): long    { return fn_o104(); }
function  fn_o305(): integer { return fn_o105(); }
function  fn_o306(): float   { return fn_o106(); }
function  fn_o307(): real    { return fn_o107(); }
function  fn_o308(): double  { return fn_o108(); }
function  fn_o309(): string  { return fn_o109(); }
function  fn_o310(): binary  { return fn_o110(); }
//function  fn_o311(): array   { return fn_o111(); }

function  fn_i100(         $a) {}
function  fn_i101(bool     $a) {}
function  fn_i102(boolean  $a) {}
function  fn_i103(int      $a) {}
function  fn_i104(long     $a) {}
function  fn_i105(integer  $a) {}
function  fn_i106(float    $a) {}
function  fn_i107(real     $a) {}
function  fn_i108(double   $a) {}
function  fn_i109(string   $a) {}
function  fn_i110(binary   $a) {}
//function  fn_i111(array    $a) {}

function  fn_i200(        &$a) {}
function  fn_i201(bool    &$a) {}
function  fn_i202(boolean &$a) {}
function  fn_i203(int     &$a) {}
function  fn_i204(long    &$a) {}
function  fn_i205(integer &$a) {}
function  fn_i206(float   &$a) {}
function  fn_i207(real    &$a) {}
function  fn_i208(double  &$a) {}
function  fn_i209(string  &$a) {}
function  fn_i210(binary  &$a) {}
//function  fn_i211(array   &$a) {}


// reference -> non-reference
fn_i100(fn_o100());
fn_i101(fn_o101());
fn_i102(fn_o102());
fn_i103(fn_o103());
fn_i104(fn_o104());
fn_i105(fn_o105());
fn_i106(fn_o106());
fn_i107(fn_o107());
fn_i108(fn_o108());
fn_i109(fn_o109());
fn_i110(fn_o110());
//fn_i111(fn_o111());


// non-reference -> non-reference
fn_i100(fn_o200());
fn_i101(fn_o201());
fn_i102(fn_o202());
fn_i103(fn_o203());
fn_i104(fn_o204());
fn_i105(fn_o205());
fn_i106(fn_o206());
fn_i107(fn_o207());
fn_i108(fn_o208());
fn_i109(fn_o209());
fn_i110(fn_o210());
//fn_i111(fn_o211());


// reference -> reference
fn_i200(fn_o100());
fn_i201(fn_o101());
fn_i202(fn_o102());
fn_i203(fn_o103());
fn_i204(fn_o104());
fn_i205(fn_o105());
fn_i206(fn_o106());
fn_i207(fn_o107());
fn_i208(fn_o108());
fn_i209(fn_o109());
fn_i210(fn_o110());
//fn_i211(fn_o111());


// non-reference -> reference
//TODO: must bind without errors.
fn_i200(fn_o200());
fn_i201(fn_o201());		// expected-error{{expected l-value}}
fn_i202(fn_o202());		// expected-error{{expected l-value}}
fn_i203(fn_o203());		// expected-error{{expected l-value}}
fn_i204(fn_o204());		// expected-error{{expected l-value}}
fn_i205(fn_o205());		// expected-error{{expected l-value}}
fn_i206(fn_o206());		// expected-error{{expected l-value}}
fn_i207(fn_o207());		// expected-error{{expected l-value}}
fn_i208(fn_o208());		// expected-error{{expected l-value}}
fn_i209(fn_o209());
fn_i210(fn_o210());

?>