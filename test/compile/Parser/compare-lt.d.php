// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '<'
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
static_assert((false < null   ) === false); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((true  < null   ) === false); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((false < false  ) === false);
static_assert((false < true   ) === true );
static_assert((true  < false  ) === false);
static_assert((true  < true   ) === false);
static_assert((true  < 123    ) === false); // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
static_assert((false < 123    ) === true ); // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
static_assert((true  < 123.456) === false); // expected-warning{{implicit conversion from 'double' to 'boolean'}}
static_assert((false < 123.456) === true ); // expected-warning{{implicit conversion from 'double' to 'boolean'}}
static_assert((true  < ''     ) === false); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((false < ''     ) === false); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((true  < "0"    ) === false); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((false < "0"    ) === false); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((true  < "qwe"  ) === false); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((false < "qwe"  ) === true ); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((false < []     ) === false); // expected-warning{{comparing array with non-array}}
static_assert((true  < []     ) === false); // expected-warning{{comparing array with non-array}}
static_assert((false < [0]    ) === true ); // expected-warning{{comparing array with non-array}}
static_assert((true  < [0]    ) === false); // expected-warning{{comparing array with non-array}}
static_assert((false < [123]  ) === true ); // expected-warning{{comparing array with non-array}}
static_assert((true  < [123]  ) === false); // expected-warning{{comparing array with non-array}}

static_assert((null    < false) === false); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((null    < true ) === true ); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((123     < true ) === false); // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
static_assert((123     < false) === false); // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
static_assert((123.456 < true ) === false); // expected-warning{{implicit conversion from 'double' to 'boolean'}}
static_assert((123.456 < false) === false); // expected-warning{{implicit conversion from 'double' to 'boolean'}}
static_assert((''      < true ) === true ); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert((''      < false) === false); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(("0"     < true ) === true ); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(("0"     < false) === false); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(("qwe"   < true ) === false); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(("qwe"   < false) === false); // expected-warning{{implicit conversion from 'string' to 'boolean'}}
static_assert(([]      < false) === false); // expected-warning{{comparing array with non-array}}
static_assert(([]      < true ) === true ); // expected-warning{{comparing array with non-array}}
static_assert(([0]     < false) === false); // expected-warning{{comparing array with non-array}}
static_assert(([0]     < true ) === false); // expected-warning{{comparing array with non-array}}
static_assert(([123]   < false) === false); // expected-warning{{comparing array with non-array}}
static_assert(([123]   < true ) === false); // expected-warning{{comparing array with non-array}}


//----- null with types ---------------------------------------------
static_assert((null < null   ) === false);
static_assert((null < ''     ) === false); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((null < "0"    ) === true ); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((null < "qwe"  ) === true ); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((null < "123"  ) === true ); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((null < 123    ) === true ); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((null < -123   ) === false); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((null < 123.123) === true ); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((null < []     ) === false); // expected-warning{{comparing array with non-array}}
static_assert((null < [0]    ) === true ); // expected-warning{{comparing array with non-array}}
static_assert((null < [123]  ) === true ); // expected-warning{{comparing array with non-array}}

static_assert((null    < null) === false);
static_assert((''      < null) === false); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert(("0"     < null) === false); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert(("qwe"   < null) === false); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert(("123"   < null) === false); // expected-warning{{'null' implicitly converted to 'empty string'}}
static_assert((123     < null) === false); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((-123    < null) === true ); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert((123.123 < null) === false); // expected-warning{{'null' implicitly converted to 'false'}}
static_assert(([]      < null) === false); // expected-warning{{comparing array with non-array}}
static_assert(([0]     < null) === false); // expected-warning{{comparing array with non-array}}
static_assert(([123]   < null) === false); // expected-warning{{comparing array with non-array}}


//----- integer with types ------------------------------------------
static_assert((123 < 123.4) === true );
static_assert((123 < 456  ) === true );
static_assert((456 < 123  ) === false);
static_assert((123 < 123  ) === false);
static_assert((123 < ''   ) === false); // expected-warning{{string does not contain a number}}
static_assert((123 < "qwe") === false); // expected-warning{{string does not contain a number}}
static_assert((123 < "456") === true ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert((123 < []   ) === true ); // expected-warning{{comparing array with non-array}}
static_assert((123 < [0]  ) === true ); // expected-warning{{comparing array with non-array}}
static_assert((123 < [123]) === true ); // expected-warning{{comparing array with non-array}}

static_assert((123.4 < 123) === false);
static_assert((456   < 123) === false);
static_assert((123   < 456) === true );
static_assert((123   < 123) === false);
static_assert((''    < 123) === true ); // expected-warning{{string does not contain a number}}
static_assert(("qwe" < 123) === true ); // expected-warning{{string does not contain a number}}
static_assert(("456" < 123) === false); // expected-warning{{implicit conversion from 'string' to 'integer'}}
static_assert(([]    < 123) === false); // expected-warning{{comparing array with non-array}}
static_assert(([0]   < 123) === false); // expected-warning{{comparing array with non-array}}
static_assert(([123] < 123) === false); // expected-warning{{comparing array with non-array}}


//----- double with types -------------------------------------------
static_assert((1.2 < 1.2   ) === false);
static_assert((1.3 < 1.2   ) === false);
static_assert((1.2 < 1.3   ) === true );
static_assert((-.2 < ''    ) === true ); // expected-warning{{string does not contain a number}}
static_assert((1.2 < "qwe" ) === false); // expected-warning{{string does not contain a number}}
static_assert((1.2 < "1e10") === true ); // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert((1.2 < []    ) === true ); // expected-warning{{comparing array with non-array}}
static_assert((1.2 < [0]   ) === true ); // expected-warning{{comparing array with non-array}}
static_assert((1.2 < [123] ) === true ); // expected-warning{{comparing array with non-array}}

static_assert((1.2    < 1.2) === false);
static_assert((1.2    < 1.3) === true );
static_assert((1.3    < 1.2) === false);
static_assert((''     < -.2) === false); // expected-warning{{string does not contain a number}}
static_assert(("qwe"  < 1.2) === true ); // expected-warning{{string does not contain a number}}
static_assert(("1e10" < 1.2) === false); // expected-warning{{implicit conversion from 'string' to 'double'}}
static_assert(([]     < 1.2) === false); // expected-warning{{comparing array with non-array}}
static_assert(([0]    < 1.2) === false); // expected-warning{{comparing array with non-array}}
static_assert(([123]  < 1.2) === false); // expected-warning{{comparing array with non-array}}


//----- string with types -------------------------------------------
static_assert(("123"   < "0456" ) === true ); // expected-warning 2 {{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"   < "0456" ) === false);
static_assert(("0456"  < "qwe"  ) === true );
static_assert(("qwe"   < "qwe"  ) === false);
static_assert(("abcde" < "aacd" ) === false);
static_assert(("aacd"  < "abcde") === true );
static_assert(("aacd"  < "abcde") === true );
static_assert(("qwe"   < []     ) === true ); // expected-warning{{comparing array with non-array}}
static_assert(("qwe"   < [0]    ) === true ); // expected-warning{{comparing array with non-array}}
static_assert(("qwe"   < [123]  ) === true ); // expected-warning{{comparing array with non-array}}
static_assert(("-123"    < ".123"   ) === true ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                  // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    < ".123"   ) === false); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                  // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert(("+123"    < ' .123'  ) === false); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                  // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((' +123'   < ".123"   ) === false); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                  // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert(("+123asd" < ".123qwe") === true );

static_assert(("0456"  < "123"  ) === false); // expected-warning 2 {{implicit conversion from 'string' to 'integer'}}
static_assert(("0456"  < "qwe"  ) === true );
static_assert(("qwe"   < "0456" ) === false);
static_assert(("qwe"   < "qwe"  ) === false);
static_assert(("aacd"  < "abcde") === true );
static_assert(("abcde" < "aacd" ) === false);
static_assert(("abcde" < "aacd" ) === false);
static_assert(([]      < "qwe"  ) === false); // expected-warning{{comparing array with non-array}}
static_assert(([0]     < "qwe"  ) === false); // expected-warning{{comparing array with non-array}}
static_assert(([123]   < "qwe"  ) === false); // expected-warning{{comparing array with non-array}}
static_assert((".123"    < "-123"   ) === false); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                  // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((".123"    < "+123"   ) === true ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                  // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((' .123'   < "+123"   ) === true ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                  // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((".123"    < ' +123'  ) === true ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                  // expected-warning@-1{{implicit conversion from 'string' to 'double'}}
static_assert((".123qwe" < "+123asd") === false);


//----- array with types --------------------------------------------
static_assert(([]      < []     )  === false);
static_assert(([1,2,3] < []     )  === false);
static_assert(([]      < [1,2,3])  === true );
static_assert(([3,4]   < [1,2,3])  === true );
static_assert(([1,2]   < [1,2]  )  === false);

static_assert((["a" => 1] < ["b" => 1])  === false);
static_assert((["b" => 1] < ["a" => 1])  === false);

static_assert(([3 => 1] < [7 => 1])  === false);
static_assert(([7 => 1] < [3 => 1])  === false);

static_assert((["a" => 1] < ["a" => 2])  === true );
static_assert((["a" => 2] < ["a" => 1])  === false);



