// RUN: %clang_php %s -verify
<?php

function static_assert($a) { assert($a); }// expected-warning{{static_assert has special treatment that cannot be redefined}}

//-------------------------------------------------------------------
// null to types
//-------------------------------------------------------------------
static_assert(((unset)  null) === null);
static_assert(((bool)   null) === false);
static_assert(((boolean)null) === false);
static_assert(((int)    null) === 0);
static_assert(((integer)null) === 0);
static_assert(((real)   null) === 0.0);
static_assert(((float)  null) === 0.0);
static_assert(((double) null) === 0.0);
static_assert(((string) null) === '');
static_assert(((binary) null) === '');
static_assert(((array)  null) === []);
//var_dump((object)null);


static_assert(((UNSET)  null) === null);
static_assert(((BOOL)   null) === false);
static_assert(((BOOLEAN)null) === false);
static_assert(((INT)    null) === 0);
static_assert(((INTEGER)null) === 0);
static_assert(((REAL)   null) === 0.0);
static_assert(((FLOAT)  null) === 0.0);
static_assert(((DOUBLE) null) === 0.0);
static_assert(((STRING) null) === '');
static_assert(((BINARY) null) === '');
static_assert(((ARRAY)  null) === []);
//var_dump((OBJECT)null);

//-------------------------------------------------------------------
// boolean to types
//-------------------------------------------------------------------
static_assert(((unset)  false) === null);
static_assert(((bool)   false) === false);
static_assert(((boolean)false) === false);
static_assert(((int)    false) === 0);
static_assert(((integer)false) === 0);
static_assert(((real)   false) === 0.0);
static_assert(((float)  false) === 0.0);
static_assert(((double) false) === 0.0);
static_assert(((string) false) === '');
static_assert(((binary) false) === '');
static_assert(((array)  false) === [false]);
//var_dump((object)false);

static_assert(((unset)  true) === null);
static_assert(((bool)   true) === true);
static_assert(((boolean)true) === true);
static_assert(((int)    true) === 1);
static_assert(((integer)true) === 1);
static_assert(((real)   true) === 1.0);
static_assert(((float)  true) === 1.0);
static_assert(((double) true) === 1.0);
static_assert(((string) true) === "1");
static_assert(((binary) true) === "1");
static_assert(((array)  true) === [true]);
//var_dump((object)true);

//-------------------------------------------------------------------
// integer  to types
//-------------------------------------------------------------------
static_assert(((unset)  123) === null);
static_assert(((bool)   123) === true);
static_assert(((boolean)123) === true);
static_assert(((int)    123) === 123);
static_assert(((integer)123) === 123);
static_assert(((real)   123) === 123.0);
static_assert(((float)  123) === 123.0);
static_assert(((double) 123) === 123.0);
static_assert(((string) 123) === "123");
static_assert(((binary) 123) === "123");
static_assert(((array)  123) === [123]);
//var_dump((object)123);

//-------------------------------------------------------------------
// float to types
//-------------------------------------------------------------------
static_assert(((unset)  123.456) === null);
static_assert(((bool)   123.456) === true);
static_assert(((boolean)123.456) === true);
static_assert(((int)    123.456) === 123);
static_assert(((integer)123.456) === 123);
static_assert(((real)   123.456) === 123.456);
static_assert(((float)  123.456) === 123.456);
static_assert(((double) 123.456) === 123.456);
static_assert(((string) 123.456) === "123.456");
static_assert(((binary) 123.456) === "123.456");
static_assert(((array)  123.456) === [123.456]);
//var_dump((object)123.456);

//-------------------------------------------------------------------
// string to types
//-------------------------------------------------------------------
static_assert(((unset)  "qwe") === null);
static_assert(((bool)   "qwe") === true);
static_assert(((boolean)"qwe") === true);
static_assert(((int)    "qwe") === 0);    // expected-warning{{string does not contain a number}}
static_assert(((integer)"qwe") === 0);    // expected-warning{{string does not contain a number}}
static_assert(((real)   "qwe") === 0.0);  // expected-warning{{string does not contain a number}}
static_assert(((float)  "qwe") === 0.0);  // expected-warning{{string does not contain a number}}
static_assert(((double) "qwe") === 0.0);  // expected-warning{{string does not contain a number}}
static_assert(((string) "qwe") === "qwe");
static_assert(((binary) "qwe") === "qwe");
static_assert(((array)  "qwe") === ["qwe"]);
//var_dump((object)"qwe");

static_assert(((unset)  "123") === null);
static_assert(((bool)   "123") === true);
static_assert(((boolean)"123") === true);
static_assert(((int)    "123") === 123);
static_assert(((integer)"123") === 123);
static_assert(((real)   "123") === 123.0);
static_assert(((float)  "123") === 123.0);
static_assert(((double) "123") === 123.0);
static_assert(((string) "123") === "123");
static_assert(((binary) "123") === "123");
static_assert(((array)  "123") === ["123"]);
//var_dump((object)"123");

static_assert(((unset)  '') === null);
static_assert(((bool)   '') === false);
static_assert(((boolean)'') === false);
static_assert(((int)    '') === 0);    // expected-warning{{string does not contain a number}}
static_assert(((integer)'') === 0);    // expected-warning{{string does not contain a number}}
static_assert(((real)   '') === 0.0);  // expected-warning{{string does not contain a number}}
static_assert(((float)  '') === 0.0);  // expected-warning{{string does not contain a number}}
static_assert(((double) '') === 0.0);  // expected-warning{{string does not contain a number}}
static_assert(((string) '') === '');
static_assert(((binary) '') === '');
static_assert(((array)  '') === ['']);
//var_dump((object)'');

static_assert(((unset)  '0') === null);
static_assert(((bool)   '0') === false);
static_assert(((boolean)'0') === false);
static_assert(((int)    '0') === 0);
static_assert(((integer)'0') === 0);
static_assert(((real)   '0') === 0.0);
static_assert(((float)  '0') === 0.0);
static_assert(((double) '0') === 0.0);
static_assert(((string) '0') === '0');
static_assert(((binary) '0') === '0');
static_assert(((array)  '0') === ['0']);
//var_dump((object)"0");

//-------------------------------------------------------------------
// array to types
//-------------------------------------------------------------------
static_assert(((unset)  []) === null);
static_assert(((bool)   []) === false);
static_assert(((boolean)[]) === false);
static_assert(((int)    []) === 0);    // expected-warning{{implicit conversion of array to number}}
static_assert(((integer)[]) === 0);    // expected-warning{{implicit conversion of array to number}}
static_assert(((real)   []) === 0.0);  // expected-warning{{implicit conversion of array to number}}
static_assert(((float)  []) === 0.0);  // expected-warning{{implicit conversion of array to number}}
static_assert(((double) []) === 0.0);  // expected-warning{{implicit conversion of array to number}}
static_assert(((string) []) === 'Array');  //TODO: must be warning
static_assert(((binary) []) === 'Array');
static_assert(((array)  []) === []);
//var_dump((object)[]);

static_assert(((unset)  [0]) === null);
static_assert(((bool)   [0]) === true);
static_assert(((boolean)[0]) === true);
static_assert(((int)    [0]) === 1);    // expected-warning{{implicit conversion of array to number}}
static_assert(((integer)[0]) === 1);    // expected-warning{{implicit conversion of array to number}}
static_assert(((real)   [0]) === 1.0);  // expected-warning{{implicit conversion of array to number}}
static_assert(((float)  [0]) === 1.0);  // expected-warning{{implicit conversion of array to number}}
static_assert(((double) [0]) === 1.0);  // expected-warning{{implicit conversion of array to number}}
static_assert(((string) [0]) === 'Array'); //TODO: must be warning
static_assert(((binary) [0]) === 'Array');
static_assert(((array)  [0]) === [0]);
//var_dump((object)[0]);

static_assert(((unset)  [3,2,1]) === null);
static_assert(((bool)   [3,2,1]) === true);
static_assert(((boolean)[3,2,1]) === true);
static_assert(((int)    [3,2,1]) === 1);    // expected-warning{{implicit conversion of array to number}}
static_assert(((integer)[3,2,1]) === 1);    // expected-warning{{implicit conversion of array to number}}
static_assert(((real)   [3,2,1]) === 1.0);  // expected-warning{{implicit conversion of array to number}}
static_assert(((float)  [3,2,1]) === 1.0);  // expected-warning{{implicit conversion of array to number}}
static_assert(((double) [3,2,1]) === 1.0);  // expected-warning{{implicit conversion of array to number}}
static_assert(((string) [3,2,1]) === 'Array'); //TODO: must be warning
static_assert(((binary) [3,2,1]) === 'Array');
static_assert(((array)  [3,2,1]) === [3,2,1]);
//var_dump((object)[1,2,3]);



//-------------------------------------------------------------------
// cast chain
//-------------------------------------------------------------------
static_assert(((bool)(int)   123) === true);
static_assert(((bool)(double)123) === true);
static_assert(((bool)(string)123) === true);
static_assert(((bool)(array) 123) === true);

static_assert(((int)(bool)  123) === 1);
static_assert(((int)(double)123) === 123);
static_assert(((int)(string)123) === 123);
static_assert(((int)(array) 123) === 1);       // expected-warning{{implicit conversion of array to number}}

static_assert(((double)(bool)  123) === 1.0);
static_assert(((double)(int)   123) === 123.0);
static_assert(((double)(string)123) === 123.0);
static_assert(((double)(array) 123) === 1.0);  // expected-warning{{implicit conversion of array to number}}

static_assert(((string)(bool)  123) === '1');
static_assert(((string)(int)   123.5) === '123');
static_assert(((string)(double)123) === '123');
static_assert(((string)(array) 123) === 'Array');

static_assert(((array)(bool)  123) === [true]);
static_assert(((array)(int)   123) === [123]);
static_assert(((array)(double)123) === [123.0]);
static_assert(((array)(string)123) === ['123']);


//-------------------------------------------------------------------
// no extra warnings
//-------------------------------------------------------------------
function fn_01(): bool {
  echo "function fn_01 called!\n";
  return true;
}

assert(((unset)   fn_01()) === null);
assert(((bool)    fn_01()) === true);
assert(((boolean) fn_01()) === true);
assert(((int)     fn_01()) === 1);
assert(((integer) fn_01()) === 1);
assert(((real)    fn_01()) === 1.0);
assert(((float)   fn_01()) === 1.0);
assert(((double)  fn_01()) === 1.0);
assert(((string)  fn_01()) === '1');
assert(((binary)  fn_01()) === '1');

?>