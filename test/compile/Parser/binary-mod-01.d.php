// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '%'
//-------------------------------------------------------------------

//----- boolean with types ------------------------------------------
static_assert((false % true   ) === 0 ); //expected-warning 2{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  % true   ) === 0 ); //expected-warning 2{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  % 2      ) === 1 ); //expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((false % 123    ) === 0 ); //expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((true  % 4      ) === 1 ); //expected-warning{{implicit conversion from 'boolean' to 'integer'}}

static_assert((false % 123.456) === 0 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                         // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}

static_assert((null    % true ) === 0 ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                         // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123     % true ) === 0 ); //expected-warning{{implicit conversion from 'boolean' to 'integer'}}
static_assert((123.456 % true ) === 0 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                         // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert((''      % true ) === 0 ); // expected-warning{{string does not contain a number}}
                                         // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("0"     % true ) === 0 ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                         // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}
static_assert(("qwe"   % true ) === 0 ); // expected-warning{{string does not contain a number}}
                                         // expected-warning@-1{{implicit conversion from 'boolean' to 'integer'}}


//----- null with types ---------------------------------------------
static_assert((null % "123"  ) === 0 ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                        // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((null % 123    ) === 0 ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null % -123   ) === 0 ); // expected-warning{{'null' implicitly converted to 'zero'}}
static_assert((null % 123.123) === 0 ); // expected-warning{{'null' implicitly converted to 'zero'}}
                                        // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}


//----- integer with types ------------------------------------------
static_assert(( 123 % 123.4) ===  0   ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
static_assert(( 5   % 3    ) ===  2   );
static_assert((-5   % 3    ) === -2   );
static_assert(( 6   % 3    ) ===  0   );
static_assert(( 123 % 123  ) ===  0   );
static_assert(( 123 % "456") ===  123 ); // expected-warning{{implicit conversion from 'string' to 'integer'}}

static_assert((123.4  % 123) === 0 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
static_assert((''     % 123) === 0 ); // expected-warning{{string does not contain a number}}
static_assert(("qwe"  % 123) === 0 ); // expected-warning{{string does not contain a number}}
static_assert(("1024" % 32 ) === 0 ); // expected-warning{{implicit conversion from 'string' to 'integer'}}


//----- double with types -------------------------------------------
static_assert((1.2 % 1.2   ) === 0 ); // expected-warning 2 {{implicit conversion from 'double' to 'integer'}}
static_assert((2.4 % 3.2   ) === 2 ); // expected-warning 2 {{implicit conversion from 'double' to 'integer'}}
static_assert((1.2 % 2.4   ) === 1 ); // expected-warning 2 {{implicit conversion from 'double' to 'integer'}}
static_assert((1.2 % "1e5" ) === 1 ); // expected-warning{{implicit conversion from 'double' to 'integer'}}
                                      // expected-warning@-1{{implicit conversion from 'string' to 'double'}}

static_assert(("qwe"  % 1.2) === 0 ); // expected-warning{{string does not contain a number}}
                                      // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}
static_assert(("1e5"  % 5.2) === 0 ); // expected-warning{{implicit conversion from 'string' to 'double'}}
                                      // expected-warning@-1{{implicit conversion from 'double' to 'integer'}}


//----- string with types -------------------------------------------
static_assert(("123"     % "246"    ) ===  123 ); // expected-warning 2 {{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"     % "0456"   ) ===  0   ); // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                                  // expected-warning@-1{{string does not contain a number}}

static_assert(("246"     % "123"    ) ===  0  ); // expected-warning 2 {{implicit conversion from 'string' to 'integer'}}
static_assert(("qwe"     % "0456"   ) ===  0  ); // expected-warning{{string does not contain a number}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
static_assert((".123"    % "-123"   ) ===  0  ); // expected-warning{{implicit conversion from 'string' to 'double'}}
                                                 // expected-warning@-1{{implicit conversion from 'string' to 'integer'}}
