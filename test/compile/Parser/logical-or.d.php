// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary '||'
//-------------------------------------------------------------------

//----- null with types ---------------------------------------------
static_assert((null || null   ) === false);
static_assert((null || true   ) === true );
static_assert((null || false  ) === false);
static_assert((null || 123    ) === true );
static_assert((null || 0      ) === false);
static_assert((null || 1.2    ) === true );
static_assert((null || 0.0    ) === false);
static_assert((null || ''     ) === false);
static_assert((null || "qwe"  ) === true );
static_assert((null || []     ) === false);
static_assert((null || [1,2,3]) === true );

static_assert((null    || null) === false);
static_assert((true    || null) === true );
static_assert((false   || null) === false);
static_assert((123     || null) === true );
static_assert((0       || null) === false);
static_assert((1.2     || null) === true );
static_assert((0.0     || null) === false);
static_assert((''      || null) === false);
static_assert(("qwe"   || null) === true );
static_assert(([]      || null) === false);
static_assert(([1,2,3] || null) === true );


//----- false with types --------------------------------------------
static_assert((false || null   ) === false);
static_assert((false || true   ) === true );
static_assert((false || false  ) === false);
static_assert((false || 123    ) === true );
static_assert((false || 0      ) === false);
static_assert((false || 1.2    ) === true );
static_assert((false || 0.0    ) === false);
static_assert((false || ''     ) === false);
static_assert((false || "qwe"  ) === true );
static_assert((false || []     ) === false);
static_assert((false || [1,2,3]) === true );

static_assert((null    || false) === false);
static_assert((true    || false) === true );
static_assert((false   || false) === false);
static_assert((123     || false) === true );
static_assert((0       || false) === false);
static_assert((1.2     || false) === true );
static_assert((0.0     || false) === false);
static_assert((''      || false) === false);
static_assert(("qwe"   || false) === true );
static_assert(([]      || false) === false);
static_assert(([1,2,3] || false) === true );


//----- true with types ---------------------------------------------
static_assert((true || null   ) === true);
static_assert((true || true   ) === true);
static_assert((true || false  ) === true);
static_assert((true || 123    ) === true);
static_assert((true || 0      ) === true);
static_assert((true || 1.2    ) === true);
static_assert((true || 0.0    ) === true);
static_assert((true || ''     ) === true);
static_assert((true || "qwe"  ) === true);
static_assert((true || []     ) === true);
static_assert((true || [1,2,3]) === true);
static_assert((true || $a     ) === true);

static_assert((null    || true) === true);
static_assert((true    || true) === true);
static_assert((false   || true) === true);
static_assert((123     || true) === true);
static_assert((0       || true) === true);
static_assert((1.2     || true) === true);
static_assert((0.0     || true) === true);
static_assert((''      || true) === true);
static_assert(("qwe"   || true) === true);
static_assert(([]      || true) === true);
static_assert(([1,2,3] || true) === true);


//----- integer with types ------------------------------------------
static_assert((123 || null   ) === true);
static_assert((123 || true   ) === true);
static_assert((123 || false  ) === true);
static_assert((123 || 123    ) === true);
static_assert((123 || 0      ) === true);
static_assert((123 || 1.2    ) === true);
static_assert((123 || 0.0    ) === true);
static_assert((123 || ''     ) === true);
static_assert((123 || "qwe"  ) === true);
static_assert((123 || []     ) === true);
static_assert((123 || [1,2,3]) === true);
static_assert((123 || $a     ) === true);

static_assert((null    || 123) === true);
static_assert((true    || 123) === true);
static_assert((false   || 123) === true);
static_assert((123     || 123) === true);
static_assert((0       || 123) === true);
static_assert((1.2     || 123) === true);
static_assert((0.0     || 123) === true);
static_assert((''      || 123) === true);
static_assert(("qwe"   || 123) === true);
static_assert(([]      || 123) === true);
static_assert(([1,2,3] || 123) === true);

static_assert((0 || null   ) === false);
static_assert((0 || true   ) === true );
static_assert((0 || false  ) === false);
static_assert((0 || 123    ) === true );
static_assert((0 || 0      ) === false);
static_assert((0 || 1.2    ) === true );
static_assert((0 || 0.0    ) === false);
static_assert((0 || ''     ) === false);
static_assert((0 || "qwe"  ) === true );
static_assert((0 || []     ) === false);
static_assert((0 || [1,2,3]) === true );

static_assert((null    || 0) === false);
static_assert((true    || 0) === true );
static_assert((false   || 0) === false);
static_assert((123     || 0) === true );
static_assert((0       || 0) === false);
static_assert((1.2     || 0) === true );
static_assert((0.0     || 0) === false);
static_assert((''      || 0) === false);
static_assert(("qwe"   || 0) === true );
static_assert(([]      || 0) === false);
static_assert(([1,2,3] || 0) === true );


//----- double with types -------------------------------------------
static_assert((1.3 || null   ) === true);
static_assert((1.3 || true   ) === true);
static_assert((1.3 || false  ) === true);
static_assert((1.3 || 123    ) === true);
static_assert((1.3 || 0      ) === true);
static_assert((1.3 || 1.2    ) === true);
static_assert((1.3 || 0.0    ) === true);
static_assert((1.3 || ''     ) === true);
static_assert((1.3 || "qwe"  ) === true);
static_assert((1.3 || []     ) === true);
static_assert((1.3 || [1,2,3]) === true);
static_assert((1.3 || $a     ) === true);

static_assert((null    || 1.3) === true);
static_assert((true    || 1.3) === true);
static_assert((false   || 1.3) === true);
static_assert((123     || 1.3) === true);
static_assert((0       || 1.3) === true);
static_assert((1.2     || 1.3) === true);
static_assert((0.0     || 1.3) === true);
static_assert((''      || 1.3) === true);
static_assert(("qwe"   || 1.3) === true);
static_assert(([]      || 1.3) === true);
static_assert(([1,2,3] || 1.3) === true);

static_assert((0.0 || null   ) === false);
static_assert((0.0 || true   ) === true );
static_assert((0.0 || false  ) === false);
static_assert((0.0 || 123    ) === true );
static_assert((0.0 || 0      ) === false);
static_assert((0.0 || 1.2    ) === true );
static_assert((0.0 || 0.0    ) === false);
static_assert((0.0 || ''     ) === false);
static_assert((0.0 || "qwe"  ) === true );
static_assert((0.0 || []     ) === false);
static_assert((0.0 || [1,2,3]) === true );

static_assert((null    || 0.0) === false);
static_assert((true    || 0.0) === true );
static_assert((false   || 0.0) === false);
static_assert((123     || 0.0) === true );
static_assert((0       || 0.0) === false);
static_assert((1.2     || 0.0) === true );
static_assert((0.0     || 0.0) === false);
static_assert((''      || 0.0) === false);
static_assert(("qwe"   || 0.0) === true );
static_assert(([]      || 0.0) === false);
static_assert(([1,2,3] || 0.0) === true );


//----- string with types -------------------------------------------
static_assert(("qwe" || null   ) === true);
static_assert(("qwe" || true   ) === true);
static_assert(("qwe" || false  ) === true);
static_assert(("qwe" || 123    ) === true);
static_assert(("qwe" || 0      ) === true);
static_assert(("qwe" || 1.2    ) === true);
static_assert(("qwe" || 0.0    ) === true);
static_assert(("qwe" || ''     ) === true);
static_assert(("qwe" || "qwe"  ) === true);
static_assert(("qwe" || []     ) === true);
static_assert(("qwe" || [1,2,3]) === true);
static_assert(("qwe" || $a     ) === true);

static_assert((null    || "qwe") === true);
static_assert((true    || "qwe") === true);
static_assert((false   || "qwe") === true);
static_assert((123     || "qwe") === true);
static_assert((0       || "qwe") === true);
static_assert((1.2     || "qwe") === true);
static_assert((0.0     || "qwe") === true);
static_assert((''      || "qwe") === true);
static_assert(("qwe"   || "qwe") === true);
static_assert(([]      || "qwe") === true);
static_assert(([1,2,3] || "qwe") === true);

static_assert(('' || null   ) === false);
static_assert(('' || true   ) === true );
static_assert(('' || false  ) === false);
static_assert(('' || 123    ) === true );
static_assert(('' || 0      ) === false);
static_assert(('' || 1.2    ) === true );
static_assert(('' || 0.0    ) === false);
static_assert(('' || ''     ) === false);
static_assert(('' || "qwe"  ) === true );
static_assert(('' || []     ) === false);
static_assert(('' || [1,2,3]) === true );

static_assert((null    || '') === false);
static_assert((true    || '') === true );
static_assert((false   || '') === false);
static_assert((123     || '') === true );
static_assert((0       || '') === false);
static_assert((1.2     || '') === true );
static_assert((0.0     || '') === false);
static_assert((''      || '') === false);
static_assert(("qwe"   || '') === true );
static_assert(([]      || '') === false);
static_assert(([1,2,3] || '') === true );

static_assert(("0" || null   ) === false);
static_assert(("0" || true   ) === true );
static_assert(("0" || false  ) === false);
static_assert(("0" || 123    ) === true );
static_assert(("0" || 0      ) === false);
static_assert(("0" || 1.2    ) === true );
static_assert(("0" || 0.0    ) === false);
static_assert(("0" || ''     ) === false);
static_assert(("0" || "qwe"  ) === true );
static_assert(("0" || []     ) === false);
static_assert(("0" || [1,2,3]) === true );

static_assert((null    || "0") === false);
static_assert((true    || "0") === true );
static_assert((false   || "0") === false);
static_assert((123     || "0") === true );
static_assert((0       || "0") === false);
static_assert((1.2     || "0") === true );
static_assert((0.0     || "0") === false);
static_assert((''      || "0") === false);
static_assert(("qwe"   || "0") === true );
static_assert(([]      || "0") === false);
static_assert(([1,2,3] || "0") === true );


//----- array with types --------------------------------------------
static_assert(([1, 2, 3] || null   ) === true);
static_assert(([1, 2, 3] || true   ) === true);
static_assert(([1, 2, 3] || false  ) === true);
static_assert(([1, 2, 3] || 123    ) === true);
static_assert(([1, 2, 3] || 0      ) === true);
static_assert(([1, 2, 3] || 1.2    ) === true);
static_assert(([1, 2, 3] || 0.0    ) === true);
static_assert(([1, 2, 3] || ''     ) === true);
static_assert(([1, 2, 3] || "qwe"  ) === true);
static_assert(([1, 2, 3] || []     ) === true);
static_assert(([1, 2, 3] || [1,2,3]) === true);
static_assert(([1, 2, 3] || $a     ) === true);

static_assert((null    || [1, 2, 3]) === true);
static_assert((true    || [1, 2, 3]) === true);
static_assert((false   || [1, 2, 3]) === true);
static_assert((123     || [1, 2, 3]) === true);
static_assert((0       || [1, 2, 3]) === true);
static_assert((1.2     || [1, 2, 3]) === true);
static_assert((0.0     || [1, 2, 3]) === true);
static_assert((''      || [1, 2, 3]) === true);
static_assert(("qwe"   || [1, 2, 3]) === true);
static_assert(([]      || [1, 2, 3]) === true);
static_assert(([1,2,3] || [1, 2, 3]) === true);

static_assert(([] || null   ) === false);
static_assert(([] || true   ) === true );
static_assert(([] || false  ) === false);
static_assert(([] || 123    ) === true );
static_assert(([] || 0      ) === false);
static_assert(([] || 1.2    ) === true );
static_assert(([] || 0.0    ) === false);
static_assert(([] || ''     ) === false);
static_assert(([] || "qwe"  ) === true );
static_assert(([] || []     ) === false);
static_assert(([] || [1,2,3]) === true );


static_assert((null    || []) === false);
static_assert((true    || []) === true );
static_assert((false   || []) === false);
static_assert((123     || []) === true );
static_assert((0       || []) === false);
static_assert((1.2     || []) === true );
static_assert((0.0     || []) === false);
static_assert((''      || []) === false);
static_assert(("qwe"   || []) === true );
static_assert(([]      || []) === false);
static_assert(([1,2,3] || []) === true );
