<?php 
// RUN: %clang_php -ast-dump -dump-stage=parse %s | FileCheck %s

//-------------------------------------------------------------------
// Tests for PHP expression AST generation.
//-------------------------------------------------------------------



//-------------------------------------------------------------------
// Unary operators
//-------------------------------------------------------------------
echo -123;
// CHECK:      PhpEchoStmt
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: -123

echo +123;
// CHECK:      PhpEchoStmt
// CHECK-NEXT: UnaryOperator
// CHECK-SAME: prefix '+'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123

echo !123;
// CHECK:      PhpEchoStmt
// CHECK-NEXT: UnaryOperator
// CHECK-SAME: prefix '!'
// CHECK-NEXT: CXXBoolLiteralExpr {{.*}} '_Bool' true

echo ~123;
// CHECK:      PhpEchoStmt
// CHECK-NEXT: UnaryOperator
// CHECK-SAME: prefix '~'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123

//--$a;
//++$a;
//$a--;
//$a++;
//&$a;

@"qwe";
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"

@123 + 456;
// CHECK-NEXT: BinaryOperator
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

1 + @@@@2;
// CHECK-NEXT: BinaryOperator
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 2

@"qwe"[1];
// CHECK-NEXT: PhpSuppressExpr
// CHECK-NEXT: ArraySubscriptExpr
// CHECK-NEXT: StringLiteral
// CHECK-SAME: "qwe"
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 'long' 1


//-------------------------------------------------------------------
// Binary operators
//-------------------------------------------------------------------

123 + 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '+'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 - 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '-'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 * 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '*'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 / 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '/'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 % 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '%'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 << 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '<<'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 >> 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '>>'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 <= 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '<='
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 <> 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '!='
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 < 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '<'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 >= 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '>='
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 > 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '>'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 != 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '!='
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 == 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '=='
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 & 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '&'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 ^ 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '^'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 | 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '|'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 && 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '&&'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 || 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '||'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 xor 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: 'xor'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 or 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: 'or'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 and 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: 'and'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 . 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '.'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 === 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '==='
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 !== 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '!=='
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 ** 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '**'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 ?? 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '??'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

123 <=> 456;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '<=>'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 123
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456


//-------------------------------------------------------------------
// Postfix operators
//-------------------------------------------------------------------

$xxx = 456;
// CHECK:      BinaryOperator
// CHECK-SAME: '='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx *= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '*='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx /= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '/='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx %= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '%='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx += 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '+='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx -= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '-='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx <<= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '<<='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx >>= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '>>='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx &= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '&='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx ^= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '^='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx |= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '|='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx .= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '.='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456

$xxx **= 456;
// CHECK-NEXT: CompoundAssignOperator
// CHECK-SAME: '**='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'xxx'
// CHECK-NEXT: IntegerLiteral
// CHECK-SAME: 456


//-------------------------------------------------------------------
// Binary operator precedence
//-------------------------------------------------------------------


//-------------------------------------------------------------------
// Binary operator associativity
//-------------------------------------------------------------------

echo $aaa + $bbb + $ccc;
// CHECK-NEXT: PhpEchoStmt
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '+'
// CHECK-NEXT: BinaryOperator
// CHECK_SAME: '+'
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'aaa'
// CHECK-NEXT: DeclRefExpr
// CHECK_SAME: 'bbb'
// CHECK-NEXT: DeclRefExpr
// CHECK_SAME: 'ccc'

echo $aaa ** $bbb ** $ccc;
// CHECK-NEXT: PhpEchoStmt
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '**'
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'aaa'
// CHECK-NEXT: BinaryOperator
// CHECK_SAME: '**'
// CHECK-NEXT: DeclRefExpr
// CHECK_SAME: 'bbb'
// CHECK-NEXT: DeclRefExpr
// CHECK_SAME: 'ccc'

echo "===";
// CHECK-NEXT: PhpEchoStmt
// CHECK-NEXT: StringLiteral

$aa1 = $bb1 = 444;
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '='
// CHECK-NEXT: DeclRefExpr
// CHECK-SAME: 'aa1'
// CHECK-NEXT: BinaryOperator
// CHECK-SAME: '='
// CHECK-NEXT: DeclRefExpr
// CHECK_SAME: 'bb1'
// CHECK_NEXT: IntegerLiteral
// _CHECK-SAME: 444

?>