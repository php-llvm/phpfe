// RUN: %clang_php %s -verify
<?php

function get_void   () : void   {                 }
function get_bool   () : bool   { return true;    }
function get_int    () : int    { return 123;     }
function get_double () : float  { return 12.34;   }
function get_string () : string { return 'qwe';   }
function get_array  () : array  { return [1,2,3]; }


function &get_bool_ref   () : bool   { $a = true;    return $a; }
function &get_int_ref    () : int    { $a = 123;     return $a; }
function &get_double_ref () : float  { $a = 12.34;   return $a; }
function &get_string_ref () : string { $a = 'qwe';   return $a; }
function &get_array_ref  () : array  { $a = [1,2,3]; return $a; }


function check_assign_to_bool_01(bool $a) {
  assert($a === true);

  $a = false; assert($a === false);
  $a = true;  assert($a === true);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;    assert($a === false); // expected-warning{{'null' implicitly converted to 'false'}}
  $a = 123;     assert($a === true);  // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
  $a = 12.34;   assert($a === true);  // expected-warning{{implicit conversion from 'double' to 'boolean'}}
  $a = "qwe";   assert($a === true);  // expected-warning{{implicit conversion from 'string' to 'boolean'}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   assert($a === false); // expected-warning{{'null' implicitly converted to 'false'}}
  $a = get_bool();   assert($a === true);
  $a = get_int();    assert($a === true);  // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
  $a = get_double(); assert($a === true);  // expected-warning{{implicit conversion from 'double' to 'boolean'}}
  $a = get_string(); assert($a === true);  // expected-warning{{implicit conversion from 'string' to 'boolean'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();   assert($a === true);
  $a = get_int_ref();    assert($a === true);  // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
  $a = get_double_ref(); assert($a === true);  // expected-warning{{implicit conversion from 'double' to 'boolean'}}
  $a = get_string_ref(); assert($a === true);  // expected-warning{{implicit conversion from 'string' to 'boolean'}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a = $b; assert($a === false);
  $b = true;    $a = $b; assert($a === true);
  $b = 123;     $a = $b; assert($a === true);
  $b = 12.45;   $a = $b; assert($a === true);
  $b = 'qwe';   $a = $b; assert($a === true);
  $b = [1,2,3]; $a = $b; assert($a === false);
}
check_assign_to_bool_01(true);


function check_assign_to_bool_02(bool &$a) {
  assert($a === true);

  $a = false; assert($a === false);
  $a = true;  assert($a === true);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;    assert($a === false); // expected-warning{{'null' implicitly converted to 'false'}}
  $a = 123;     assert($a === true);  // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
  $a = 12.34;   assert($a === true);  // expected-warning{{implicit conversion from 'double' to 'boolean'}}
  $a = "qwe";   assert($a === true);  // expected-warning{{implicit conversion from 'string' to 'boolean'}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   assert($a === false); // expected-warning{{'null' implicitly converted to 'false'}}
  $a = get_bool();   assert($a === true);
  $a = get_int();    assert($a === true);  // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
  $a = get_double(); assert($a === true);  // expected-warning{{implicit conversion from 'double' to 'boolean'}}
  $a = get_string(); assert($a === true);  // expected-warning{{implicit conversion from 'string' to 'boolean'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();   assert($a === true);
  $a = get_int_ref();    assert($a === true); // expected-warning{{implicit conversion from 'integer' to 'boolean'}}
  $a = get_double_ref(); assert($a === true); // expected-warning{{implicit conversion from 'double' to 'boolean'}}
  $a = get_string_ref(); assert($a === true); // expected-warning{{implicit conversion from 'string' to 'boolean'}}

//-------------------------------------------------------------------
// assign box to bool
//-------------------------------------------------------------------
  $b = null;    $a = $b; assert($a === false);
  $b = true;    $a = $b; assert($a === true);
  $b = 123;     $a = $b; assert($a === true);
  $b = 12.45;   $a = $b; assert($a === true);
  $b = 'qwe';   $a = $b; assert($a === true);
  $b = [1,2,3]; $a = $b; assert($a === false);
}
$a = true;
check_assign_to_bool_02($a);








function check_assign_to_int_01(int $a) {
  assert($a === 123);

  $a = 456;  assert($a === 456);
  $a = -12;  assert($a === -12);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;  assert($a === 0);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a = false; assert($a === 0);
  $a = 123;   assert($a === 123);
  $a = 12.34; assert($a === 12);  // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = "qwe"; assert($a === 0);   // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                  // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   assert($a === 0);  // expected-warning{{'null' implicitly converted to 'zero'}}
  $a = get_bool();   assert($a === 1);
  $a = get_int();    assert($a === 123);
  $a = get_double(); assert($a === 12); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = get_string(); assert($a === 0);  // expected-warning{{implicit conversion from 'string' to 'integer'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();   assert($a === 1);
  $a = get_int_ref();    assert($a === 123);
  $a = get_double_ref(); assert($a === 12); // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = get_string_ref(); assert($a === 0);  // expected-warning{{implicit conversion from 'string' to 'integer'}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a = $b; assert($a === 0);
  $b = true;  $a = $b; assert($a === 1);
  $b = 123;   $a = $b; assert($a === 123);
  $b = 12.45; $a = $b; assert($a === 12);
  $b = 'qwe';   $a = $b; assert($a === 0);
  $b = [1,2,3]; $a = $b; assert($a === false);
}

check_assign_to_int_01(123);

function check_assign_to_int_02(int &$a) {
  assert($a === 123);

  $a = 456;  assert($a === 456);
  $a = -12;  assert($a === -12);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;  assert($a === 0);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a = false; assert($a === 0);
  $a = 123;   assert($a === 123);
  $a = 12.34; assert($a === 12);  // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = "qwe"; assert($a === 0);   // expected-warning{{implicit conversion from 'string' to 'integer'}}
                                  // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   assert($a === 0);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a = get_bool();   assert($a === 1);
  $a = get_int();    assert($a === 123);
  $a = get_double(); assert($a === 12);  // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = get_string(); assert($a === 0);   // expected-warning{{implicit conversion from 'string' to 'integer'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();   assert($a === 1);
  $a = get_int_ref();    assert($a === 123);
  $a = get_double_ref(); assert($a === 12);  // expected-warning{{implicit conversion from 'double' to 'integer'}}
  $a = get_string_ref(); assert($a === 0);   // expected-warning{{implicit conversion from 'string' to 'integer'}}

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null; $a = $b; assert($a === 0);
  $b = true;  $a = $b; assert($a === 1);
  $b = 123;   $a = $b; assert($a === 123);
  $b = 12.45; $a = $b; assert($a === 12);
  $b = 'qwe';   $a = $b; assert($a === 0);
  $b = [1,2,3]; $a = $b; assert($a === false);
}
$a = 123;
check_assign_to_int_02($a);









function check_assign_to_double_01(float $a) {
  assert($a === 123.456);

  $a = 456.5;   assert($a === 456.5);
  $a = -12.258; assert($a === -12.258);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;  assert($a === 0.0);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a = false; assert($a === 0.0);
  $a = 123;   assert($a === 123.0);
  $a = 12.34; assert($a === 12.34);
  $a = "qwe"; assert($a === 0.0);   // expected-warning{{implicit conversion from 'string' to 'double'}}
                                    // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   assert($a === 0.0);  // expected-warning{{'null' implicitly converted to 'zero'}}
  $a = get_bool();   assert($a === 1.0);
  $a = get_int();    assert($a === 123.0);
  $a = get_double(); assert($a === 12.34);
  $a = get_string(); assert($a === 0.0);  // expected-warning{{implicit conversion from 'string' to 'double'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();   assert($a === 1.0);
  $a = get_int_ref();    assert($a === 123.0);
  $a = get_double_ref(); assert($a === 12.34);
  $a = get_string_ref(); assert($a === 0.0);  // expected-warning{{implicit conversion from 'string' to 'double'}}


//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null;    $a = $b; assert($a === 0);
  $b = true;    $a = $b; assert($a === 1.0);
  $b = 123;     $a = $b; assert($a === 123.0);
  $b = 12.45;   $a = $b; assert($a === 12.45);
  $b = 'qwe';   $a = $b; assert($a === 0);
  $b = [1,2,3]; $a = $b; assert($a === false);
}

check_assign_to_double_01(123.456);

function check_assign_to_double_02(float &$a) {
  assert($a === 123.456);

  $a = 456.5;   assert($a === 456.5);
  $a = -12.258; assert($a === -12.258);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;  assert($a === 0.0);   // expected-warning{{'null' implicitly converted to 'zero'}}
  $a = false; assert($a === 0.0);
  $a = 123;   assert($a === 123.0);
  $a = 12.34; assert($a === 12.34);
  $a = "qwe"; assert($a === 0.0);   // expected-warning{{implicit conversion from 'string' to 'double'}}
                                    // expected-warning@-1{{string does not contain a number}}

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   assert($a === 0.0);  // expected-warning{{'null' implicitly converted to 'zero'}}
  $a = get_bool();   assert($a === 1.0);
  $a = get_int();    assert($a === 123.0);
  $a = get_double(); assert($a === 12.34);
  $a = get_string(); assert($a === 0.0);  // expected-warning{{implicit conversion from 'string' to 'double'}}

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();   assert($a === 1.0);
  $a = get_int_ref();    assert($a === 123.0);
  $a = get_double_ref(); assert($a === 12.34);
  $a = get_string_ref(); assert($a === 0.0);  // expected-warning{{implicit conversion from 'string' to 'double'}}


//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null;    $a = $b; assert($a === 0);
  $b = true;    $a = $b; assert($a === 1.0);
  $b = 123;     $a = $b; assert($a === 123.0);
  $b = 12.45;   $a = $b; assert($a === 12.45);
  $b = 'qwe';   $a = $b; assert($a === 0);
  $b = [1,2,3]; $a = $b; assert($a === false);
}
$a = 123.456;
check_assign_to_double_02($a);









function check_assign_to_string_01(string $a) {
  assert($a === 'qwe');

  $a = 'zxc';   assert($a === 'zxc');
  $a = '';      assert($a === '');

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;    assert($a === '');      // expected-warning{{'null' implicitly converted to 'empty string'}}
  $a = false;   assert($a === '');      // expected-warning{{implicit conversion from 'boolean' to 'string'}}
  $a = 123;     assert($a === '123');   // expected-warning{{implicit conversion from 'integer' to 'string'}}
  $a = 12.34;   assert($a === '12.34'); // expected-warning{{implicit conversion from 'double' to 'string'}}
  $a = "qwe";   assert($a === 'qwe');

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   assert($a === '');       // expected-warning{{'null' implicitly converted to 'empty string'}}
  $a = get_bool();   assert($a === '1');      // expected-warning{{implicit conversion from 'boolean' to 'string'}}
  $a = get_int();    assert($a === '123');    // expected-warning{{implicit conversion from 'integer' to 'string'}}
  $a = get_double(); assert($a === '12.34');  // expected-warning{{implicit conversion from 'double' to 'string'}}
  $a = get_string(); assert($a === 'qwe');

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();   assert($a === '1');      // expected-warning{{implicit conversion from 'boolean' to 'string'}}
  $a = get_int_ref();    assert($a === '123');    // expected-warning{{implicit conversion from 'integer' to 'string'}}
  $a = get_double_ref(); assert($a === '12.34');  // expected-warning{{implicit conversion from 'double' to 'string'}}
  $a = get_string_ref(); assert($a === 'qwe');

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null;    $a = $b; assert($a === '');
  $b = true;    $a = $b; assert($a === '1');
  $b = 123;     $a = $b; assert($a === '123');
  $b = 12.45;   $a = $b; assert($a === '12.45');
  $b = 'qwe';   $a = $b; assert($a === 'qwe');
  $b = [1,2,3]; $a = $b; assert($a === 'Array');
}

check_assign_to_string_01('qwe');

function check_assign_to_string_02(string &$a) {
  assert($a === 'qwe');

  $a = 'zxc';   assert($a === 'zxc');
  $a = '';      assert($a === '');

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = null;    assert($a === '');      // expected-warning{{}}
  $a = false;   assert($a === '');      // expected-warning{{implicit conversion from 'boolean' to 'string'}}
  $a = 123;     assert($a === '123');   // expected-warning{{implicit conversion from 'integer' to 'string'}}
  $a = 12.34;   assert($a === '12.34'); // expected-warning{{implicit conversion from 'double' to 'string'}}
  $a = "qwe";   assert($a === 'qwe');

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_void();   assert($a === '');       // expected-warning{{}}
  $a = get_bool();   assert($a === '1');      // expected-warning{{implicit conversion from 'boolean' to 'string'}}
  $a = get_int();    assert($a === '123');    // expected-warning{{implicit conversion from 'integer' to 'string'}}
  $a = get_double(); assert($a === '12.34');  // expected-warning{{implicit conversion from 'double' to 'string'}}
  $a = get_string(); assert($a === 'qwe');

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_bool_ref();   assert($a === '1');      // expected-warning{{implicit conversion from 'boolean' to 'string'}}
  $a = get_int_ref();    assert($a === '123');    // expected-warning{{implicit conversion from 'integer' to 'string'}}
  $a = get_double_ref(); assert($a === '12.34');  // expected-warning{{implicit conversion from 'double' to 'string'}}
  $a = get_string_ref(); assert($a === 'qwe');

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null;    $a = $b; assert($a === '');
  $b = true;    $a = $b; assert($a === '1');
  $b = 123;     $a = $b; assert($a === '123');
  $b = 12.45;   $a = $b; assert($a === '12.45');
  $b = 'qwe';   $a = $b; assert($a === 'qwe');
  $b = [1,2,3]; $a = $b; assert($a === 'Array');
}
$a = 'qwe';
check_assign_to_string_02($a);






function check_assign_to_array_01(array $a) {
  assert($a === [1,2,3]);

  $a = [4,5,6]; assert($a === [4,5,6]);

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null;    $a = $b;
  $b = true;    $a = $b;
  $b = 123;     $a = $b;
  $b = 12.45;   $a = $b;
  $b = 'qwe';   $a = $b;
  $b = [1,2,3]; $a = $b; assert($a === [1,2,3]);
}

check_assign_to_array_01([1,2,3]);

function check_assign_to_array_02(array &$a) {
  assert($a === [1,2,3]);

  $a = [4,5,6]; assert($a === [4,5,6]);

//-------------------------------------------------------------------
// assign constant values
//-------------------------------------------------------------------
  $a = [1,2,3]; assert($a === [1,2,3]);

//-------------------------------------------------------------------
// assign non constant values
//-------------------------------------------------------------------
  $a = get_array();  assert($a === [1,2,3]);

//-------------------------------------------------------------------
// assign references
//-------------------------------------------------------------------
  $a = get_array_ref();  assert($a === [1,2,3]);

//-------------------------------------------------------------------
// assign box to int
//-------------------------------------------------------------------
  $b = null;    $a = $b;
  $b = true;    $a = $b;
  $b = 123;     $a = $b;
  $b = 12.45;   $a = $b;
  $b = 'qwe';   $a = $b;
  $b = [1,2,3]; $a = $b; assert($a === [1,2,3]);
}
$a = [1,2,3];
check_assign_to_array_02($a);

?>