// RUN: %clang_php %s -verify
<?php 

function static_assert($a) { assert($a); } //expected-warning{{static_assert has special treatment that cannot be redefined}}


//-------------------------------------------------------------------
// binary 'xor'
//-------------------------------------------------------------------

//----- null with types ---------------------------------------------
static_assert((null xor null   ) === false);
static_assert((null xor true   ) === true );
static_assert((null xor false  ) === false);
static_assert((null xor 123    ) === true );
static_assert((null xor 0      ) === false);
static_assert((null xor 1.2    ) === true );
static_assert((null xor 0.0    ) === false);
static_assert((null xor ''     ) === false);
static_assert((null xor "qwe"  ) === true );
static_assert((null xor []     ) === false);
static_assert((null xor [1,2,3]) === true );

static_assert((null    xor null) === false);
static_assert((true    xor null) === true );
static_assert((false   xor null) === false);
static_assert((123     xor null) === true );
static_assert((0       xor null) === false);
static_assert((1.2     xor null) === true );
static_assert((0.0     xor null) === false);
static_assert((''      xor null) === false);
static_assert(("qwe"   xor null) === true );
static_assert(([]      xor null) === false);
static_assert(([1,2,3] xor null) === true );


//----- false with types --------------------------------------------
static_assert((false xor null   ) === false);
static_assert((false xor true   ) === true );
static_assert((false xor false  ) === false);
static_assert((false xor 123    ) === true );
static_assert((false xor 0      ) === false);
static_assert((false xor 1.2    ) === true );
static_assert((false xor 0.0    ) === false);
static_assert((false xor ''     ) === false);
static_assert((false xor "qwe"  ) === true );
static_assert((false xor []     ) === false);
static_assert((false xor [1,2,3]) === true );

static_assert((null    xor false) === false);
static_assert((true    xor false) === true );
static_assert((false   xor false) === false);
static_assert((123     xor false) === true );
static_assert((0       xor false) === false);
static_assert((1.2     xor false) === true );
static_assert((0.0     xor false) === false);
static_assert((''      xor false) === false);
static_assert(("qwe"   xor false) === true );
static_assert(([]      xor false) === false);
static_assert(([1,2,3] xor false) === true );


//----- true with types ---------------------------------------------
static_assert((true xor null   ) === true );
static_assert((true xor true   ) === false);
static_assert((true xor false  ) === true );
static_assert((true xor 123    ) === false);
static_assert((true xor 0      ) === true );
static_assert((true xor 1.2    ) === false);
static_assert((true xor 0.0    ) === true );
static_assert((true xor ''     ) === true );
static_assert((true xor "qwe"  ) === false);
static_assert((true xor []     ) === true );
static_assert((true xor [1,2,3]) === false);

static_assert((null    xor true) === true );
static_assert((true    xor true) === false);
static_assert((false   xor true) === true );
static_assert((123     xor true) === false);
static_assert((0       xor true) === true );
static_assert((1.2     xor true) === false);
static_assert((0.0     xor true) === true );
static_assert((''      xor true) === true );
static_assert(("qwe"   xor true) === false);
static_assert(([]      xor true) === true );
static_assert(([1,2,3] xor true) === false);


//----- integer with types ------------------------------------------
static_assert((123 xor null   ) === true );
static_assert((123 xor true   ) === false);
static_assert((123 xor false  ) === true );
static_assert((123 xor 123    ) === false);
static_assert((123 xor 0      ) === true );
static_assert((123 xor 1.2    ) === false);
static_assert((123 xor 0.0    ) === true );
static_assert((123 xor ''     ) === true );
static_assert((123 xor "qwe"  ) === false);
static_assert((123 xor []     ) === true );
static_assert((123 xor [1,2,3]) === false);

static_assert((null    xor 123) === true );
static_assert((true    xor 123) === false);
static_assert((false   xor 123) === true );
static_assert((123     xor 123) === false);
static_assert((0       xor 123) === true );
static_assert((1.2     xor 123) === false);
static_assert((0.0     xor 123) === true );
static_assert((''      xor 123) === true );
static_assert(("qwe"   xor 123) === false);
static_assert(([]      xor 123) === true );
static_assert(([1,2,3] xor 123) === false);

static_assert((0 xor null   ) === false);
static_assert((0 xor true   ) === true );
static_assert((0 xor false  ) === false);
static_assert((0 xor 123    ) === true );
static_assert((0 xor 0      ) === false);
static_assert((0 xor 1.2    ) === true );
static_assert((0 xor 0.0    ) === false);
static_assert((0 xor ''     ) === false);
static_assert((0 xor "qwe"  ) === true );
static_assert((0 xor []     ) === false);
static_assert((0 xor [1,2,3]) === true );

static_assert((null    xor 0) === false);
static_assert((true    xor 0) === true );
static_assert((false   xor 0) === false);
static_assert((123     xor 0) === true );
static_assert((0       xor 0) === false);
static_assert((1.2     xor 0) === true );
static_assert((0.0     xor 0) === false);
static_assert((''      xor 0) === false);
static_assert(("qwe"   xor 0) === true );
static_assert(([]      xor 0) === false);
static_assert(([1,2,3] xor 0) === true );


//----- double with types -------------------------------------------
static_assert((1.3 xor null   ) === true );
static_assert((1.3 xor true   ) === false);
static_assert((1.3 xor false  ) === true );
static_assert((1.3 xor 123    ) === false);
static_assert((1.3 xor 0      ) === true );
static_assert((1.3 xor 1.2    ) === false);
static_assert((1.3 xor 0.0    ) === true );
static_assert((1.3 xor ''     ) === true );
static_assert((1.3 xor "qwe"  ) === false);
static_assert((1.3 xor []     ) === true );
static_assert((1.3 xor [1,2,3]) === false);

static_assert((null    xor 1.3) === true );
static_assert((true    xor 1.3) === false);
static_assert((false   xor 1.3) === true );
static_assert((123     xor 1.3) === false);
static_assert((0       xor 1.3) === true );
static_assert((1.2     xor 1.3) === false);
static_assert((0.0     xor 1.3) === true );
static_assert((''      xor 1.3) === true );
static_assert(("qwe"   xor 1.3) === false);
static_assert(([]      xor 1.3) === true );
static_assert(([1,2,3] xor 1.3) === false);

static_assert((0.0 xor null   ) === false);
static_assert((0.0 xor true   ) === true );
static_assert((0.0 xor false  ) === false);
static_assert((0.0 xor 123    ) === true );
static_assert((0.0 xor 0      ) === false);
static_assert((0.0 xor 1.2    ) === true );
static_assert((0.0 xor 0.0    ) === false);
static_assert((0.0 xor ''     ) === false);
static_assert((0.0 xor "qwe"  ) === true );
static_assert((0.0 xor []     ) === false);
static_assert((0.0 xor [1,2,3]) === true );

static_assert((null    xor 0.0) === false);
static_assert((true    xor 0.0) === true );
static_assert((false   xor 0.0) === false);
static_assert((123     xor 0.0) === true );
static_assert((0       xor 0.0) === false);
static_assert((1.2     xor 0.0) === true );
static_assert((0.0     xor 0.0) === false);
static_assert((''      xor 0.0) === false);
static_assert(("qwe"   xor 0.0) === true );
static_assert(([]      xor 0.0) === false);
static_assert(([1,2,3] xor 0.0) === true );


//----- string with types -------------------------------------------
static_assert(("qwe" xor null   ) === true );
static_assert(("qwe" xor true   ) === false);
static_assert(("qwe" xor false  ) === true );
static_assert(("qwe" xor 123    ) === false);
static_assert(("qwe" xor 0      ) === true );
static_assert(("qwe" xor 1.2    ) === false);
static_assert(("qwe" xor 0.0    ) === true );
static_assert(("qwe" xor ''     ) === true );
static_assert(("qwe" xor "qwe"  ) === false);
static_assert(("qwe" xor []     ) === true );
static_assert(("qwe" xor [1,2,3]) === false);

static_assert((null    xor "qwe") === true );
static_assert((true    xor "qwe") === false);
static_assert((false   xor "qwe") === true );
static_assert((123     xor "qwe") === false);
static_assert((0       xor "qwe") === true );
static_assert((1.2     xor "qwe") === false);
static_assert((0.0     xor "qwe") === true );
static_assert((''      xor "qwe") === true );
static_assert(("qwe"   xor "qwe") === false);
static_assert(([]      xor "qwe") === true );
static_assert(([1,2,3] xor "qwe") === false);

static_assert(('' xor null   ) === false);
static_assert(('' xor true   ) === true );
static_assert(('' xor false  ) === false);
static_assert(('' xor 123    ) === true );
static_assert(('' xor 0      ) === false);
static_assert(('' xor 1.2    ) === true );
static_assert(('' xor 0.0    ) === false);
static_assert(('' xor ''     ) === false);
static_assert(('' xor "qwe"  ) === true );
static_assert(('' xor []     ) === false);
static_assert(('' xor [1,2,3]) === true );

static_assert((null    xor '') === false);
static_assert((true    xor '') === true );
static_assert((false   xor '') === false);
static_assert((123     xor '') === true );
static_assert((0       xor '') === false);
static_assert((1.2     xor '') === true );
static_assert((0.0     xor '') === false);
static_assert((''      xor '') === false);
static_assert(("qwe"   xor '') === true );
static_assert(([]      xor '') === false);
static_assert(([1,2,3] xor '') === true );

static_assert(("0" xor null   ) === false);
static_assert(("0" xor true   ) === true );
static_assert(("0" xor false  ) === false);
static_assert(("0" xor 123    ) === true );
static_assert(("0" xor 0      ) === false);
static_assert(("0" xor 1.2    ) === true );
static_assert(("0" xor 0.0    ) === false);
static_assert(("0" xor ''     ) === false);
static_assert(("0" xor "qwe"  ) === true );
static_assert(("0" xor []     ) === false);
static_assert(("0" xor [1,2,3]) === true );

static_assert((null    xor "0") === false);
static_assert((true    xor "0") === true );
static_assert((false   xor "0") === false);
static_assert((123     xor "0") === true );
static_assert((0       xor "0") === false);
static_assert((1.2     xor "0") === true );
static_assert((0.0     xor "0") === false);
static_assert((''      xor "0") === false);
static_assert(("qwe"   xor "0") === true );
static_assert(([]      xor "0") === false);
static_assert(([1,2,3] xor "0") === true );



//----- array with types --------------------------------------------
static_assert(([1, 2, 3] xor null   ) === true );
static_assert(([1, 2, 3] xor true   ) === false);
static_assert(([1, 2, 3] xor false  ) === true );
static_assert(([1, 2, 3] xor 123    ) === false);
static_assert(([1, 2, 3] xor 0      ) === true );
static_assert(([1, 2, 3] xor 1.2    ) === false);
static_assert(([1, 2, 3] xor 0.0    ) === true );
static_assert(([1, 2, 3] xor ''     ) === true );
static_assert(([1, 2, 3] xor "qwe"  ) === false);
static_assert(([1, 2, 3] xor []     ) === true );
static_assert(([1, 2, 3] xor [1,2,3]) === false);

static_assert((null    xor [1, 2, 3]) === true );
static_assert((true    xor [1, 2, 3]) === false);
static_assert((false   xor [1, 2, 3]) === true );
static_assert((123     xor [1, 2, 3]) === false);
static_assert((0       xor [1, 2, 3]) === true );
static_assert((1.2     xor [1, 2, 3]) === false);
static_assert((0.0     xor [1, 2, 3]) === true );
static_assert((''      xor [1, 2, 3]) === true );
static_assert(("qwe"   xor [1, 2, 3]) === false);
static_assert(([]      xor [1, 2, 3]) === true );
static_assert(([1,2,3] xor [1, 2, 3]) === false);

static_assert(([] xor null   ) === false);
static_assert(([] xor true   ) === true );
static_assert(([] xor false  ) === false);
static_assert(([] xor 123    ) === true );
static_assert(([] xor 0      ) === false);
static_assert(([] xor 1.2    ) === true );
static_assert(([] xor 0.0    ) === false);
static_assert(([] xor ''     ) === false);
static_assert(([] xor "qwe"  ) === true );
static_assert(([] xor []     ) === false);
static_assert(([] xor [1,2,3]) === true );

static_assert((null    xor []) === false);
static_assert((true    xor []) === true );
static_assert((false   xor []) === false);
static_assert((123     xor []) === true );
static_assert((0       xor []) === false);
static_assert((1.2     xor []) === true );
static_assert((0.0     xor []) === false);
static_assert((''      xor []) === false);
static_assert(("qwe"   xor []) === true );
static_assert(([]      xor []) === false);
static_assert(([1,2,3] xor []) === true );

