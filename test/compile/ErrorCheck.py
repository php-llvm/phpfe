#!/usr/bin/env python

# We need this checker because in some cases we need to compare compiler output
# char-by-char. For instance, when checking compiler messages we need to check
# caret position, arguments underline, these cannot be checked by clang builting
# diagnostic consumer.

import sys
import os
import mmap

if (len(sys.argv) < 2):
    print("Error: no file specified")
    exit(2)

filename = sys.argv[1];

if not os.path.isfile(filename):
    print("Error: file not found")
    exit(3)


# Read test file.
SourceFile = open(filename, "r").read();


# Collect all expected strings.
check_string = "CHECK: "
expected_outputs = []
pos = SourceFile.find(check_string, 0)
while pos != -1:
    end_pos = SourceFile.find('\n', pos)
    if end_pos == pos:
        continue
    if end_pos == -1:
        expected_string = SourceFile[pos:]
    else:
        expected_string = SourceFile[pos+len(check_string):end_pos]
    expected_outputs.append(expected_string.rstrip())
    if end_pos == -1:
        break
    pos = SourceFile.find(check_string, end_pos)

error = False


for diag in expected_outputs:
    input_line = sys.stdin.readline().rstrip()
    if input_line == '':
        continue
    if input_line.find(diag) == -1:
        print("Error: diagnostic does not match expected output")
        print "Expected:",
        print(diag)
        print "Actual output:",
        print(input_line)
        error = True

if error:
    exit(4)