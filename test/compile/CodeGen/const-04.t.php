<?php
// RUN: %clang_php -ast-dump -dump-stage=parse -phpmode=module -triple x86_64-linux-gnu %s | FileCheck %s
// RUN: %clang_php -S -phpmode=module -triple x86_64-linux-gnu %s -o - | FileCheck --check-prefix=X64 %s

//------------------------------------------------------------------------------
// The test checks that in module mode compile time constant definition:
// - is placed into namespace 'php::__Const',
// - is placed into section which name contains 'phpd',
// - is initialized by proper literal after parser,
// - in target code it is represented by mutable object.
//------------------------------------------------------------------------------

namespace SPQR {

const ABCD = 1122;

}

// CHECK: NamespaceDecl {{.*}} php
// CHECK: NamespaceDecl {{.*}} SPQR
// CHECK: NamespaceDecl {{.*}} __Const
// CHECK: VarDecl {{.*}} used ABCD 'struct phprt::ValueInfo' cinit
// CHECK: IntegerLiteral {{.*}} 'long' 1122
// CHECK: SectionAttr {{.*}} section "__phpdc"
// CHECK: WeakAttr

// CHECK-LABEL: FunctionDecl {{.*}} 'class phprt::Box (struct phprt::UnitState *)'
// CHECK      : `-CallExpr
// CHECK-NEXT :   |-ImplicitCastExpr {{.*}} 'void (*)(struct phprt::ValueInfo &, class phprt::Box)' <FunctionToPointerDecay>
// CHECK-NEXT :   | `-DeclRefExpr {{.*}} 'void (struct phprt::ValueInfo &, class phprt::Box)' Function {{.*}} 'init_constant'
// CHECK-NEXT :   |-DeclRefExpr {{.*}} 'struct phprt::ValueInfo' lvalue Var {{.*}} 'ABCD' 'struct phprt::ValueInfo'
// CHECK      :       `-CXXConstructExpr {{.*}} 'class phprt::Box' 'void (intvalue_t) __attribute__((thiscall))'
// CHECK-NEXT :         `-IntegerLiteral {{.*}} 1122


// X64:      .L.str.1:
// X64-NEXT: .asciz	"SPQR\\ABCD"
// X64-NEXT: .size	.L.str.1, 10
// X64:      .type _ZN3php4SPQR7__Const4ABCDE,@object
// X64-NEXT: .section __phpdc
// X64-NEXT: .weak _ZN3php4SPQR7__Const4ABCDE
// X64-NEXT: .align
// X64-NEXT: _ZN3php4SPQR7__Const4ABCDE:
// X64-NEXT:	.quad	.L.str.1
// X64-NEXT:	.long	9
// X64-NEXT:	.zero	4
// X64-NEXT:	.byte	2
// X64-NEXT:	.byte	0
// X64-NEXT:	.byte	0
// X64-NEXT:	.byte	0
// X64-NEXT:	.zero	4
// X64-NEXT:	.zero	16
// X64-NEXT:	.quad	0
// X64-NEXT:	.size	_ZN3php4SPQR7__Const4ABCDE, 48

