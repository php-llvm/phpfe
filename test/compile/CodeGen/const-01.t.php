<?php
// RUN: %clang_php -ast-dump -dump-stage=parse -phpmode=emodule -triple x86_64-linux-gnu %s | FileCheck %s
// RUN: %clang_php -S -phpmode=emodule -triple x86_64-linux-gnu %s -o - | FileCheck --check-prefix=X64 %s

//------------------------------------------------------------------------------
// The test checks that in emodule mode compile time constant definition:
// - is placed into namespace 'php::__Const',
// - is placed into section which name contains 'phpc',
// - is initialized by proper integer literal after parser,
// - in target code it is represented by constant object.
//TODO: Check section attributes are read-only
//------------------------------------------------------------------------------


const ABCD = 1122;

// CHECK: NamespaceDecl {{.*}} php
// CHECK: NamespaceDecl {{.*}} __Const
// CHECK: VarDecl {{.*}} used ABCD 'const struct phprt::ValueInfo' cinit
// CHECK: IntegerLiteral {{.*}} 'long' 1122
// CHECK: SectionAttr {{.*}} section "__phpcc"
// CHECK-NOT: WeakAttr
// CHECK-NOT: ABCD

// X64:      .type _ZN3php7__ConstL4ABCDE,@object
// X64-NEXT: .section __phpc
// X64-NEXT: .align
// X64-NEXT: _ZN3php7__ConstL4ABCDE:
// X64-NEXT:	.quad	.L.str
// X64-NEXT:	.long	4
// X64-NEXT:	.zero	4
// X64-NEXT:	.byte	3
// X64-NEXT:	.byte	0
// X64-NEXT:	.byte	0
// X64-NEXT:	.byte	0
// X64-NEXT:	.zero	4
// X64-NEXT:	.quad	1122
// X64-NEXT:	.byte	0
// X64-NEXT:	.byte	0
// X64-NEXT:	.byte	0
// X64-NEXT:	.byte	3
// X64-NEXT:	.long	0
// X64-NEXT:	.quad	0
// X64-NEXT:	.size	_ZN3php7__ConstL4ABCDE, 48
