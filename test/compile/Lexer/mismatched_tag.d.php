// RUN: %clang_php %s -verify
<?php 
// expected-warning@+1 {{closing tag does not match opening tag}}
</script>

<?php 
// expected-warning@+1 {{closing tag does not match opening tag}}
%>

<?php 
// expected-warning@+1 {{closing tag does not match opening tag}}
%>

<%
// expected-warning@+1 {{closing tag does not match opening tag}}
?>

<script language='php'>
// expected-warning@+1 {{closing tag does not match opening tag}}
?>
