<?php

// RUN: %clang_php %s -rewrite-php -o comment-func-01.out.php
// RUN: diff comment-func-01.out.php %s.expect
$not_function_comment;

function fn_1() {}

  function &fn_1r() { return $a; }

      function fn_2(): bool { return true; }

function fn_3($a, int $b, string $c, &$d, double &$e) {}



/**
 *  Test function description
 *
 *  @return nothing
 */
function fn_4($a, $b) {}

//--------------------------------------------------------------------

?>
