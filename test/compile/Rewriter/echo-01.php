<html><?php

// RUN: %clang_php %s -rewrite-php -o echo-01.out.php
// RUN: diff echo-01.out.php %s.expect

?><?php

//--------------------------------------------------------------------

echo("Hello!\n");
echo 1,2,3,4;

//--------------------------------------------------------------------

?>
</html>
