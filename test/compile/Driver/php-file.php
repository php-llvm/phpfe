// Tests how driver processes source files of different kind


// clang -c abc.php must call PHP front end
//
// RUN: %clang -c %s -### 2>&1 | FileCheck -check-prefix=CLANG_COMP %s
// CLANG_COMP: "-php"


// phlang -c abc.php must call PHP front end
//
// RUN: phlang -c %s -### 2>&1 | FileCheck -check-prefix=PHLANG_COMP %s
// PHLANG_COMP: "-php"


// phl -c aaa.cpp must call PHP front end
//
// RUN: phlang -c %S/aaa.cpp -### 2>&1 | FileCheck -check-prefix=PHLANG_COMP_CXX %s
// PHLANG_COMP_CXX: "-php"{{.*}} "-x" "c++" {{.*}}aaa.cpp
